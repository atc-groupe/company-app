# API

## Initialization procedure

### NPM
run
```bash
npm install
```

### JWT RSA Keys
Generate JWT RSA Keys:

In the terminal, go into `server` folder execute the following commands:

```bash
mkdir rsa
cd rsa
ssh-keygen -t rsa -b 4096 -m PEM -f key
ssh-keygen -e -m PEM -f key > key.pub
```

### Data init
Initialize super admin user

#### Initialize app settings document
App settings is a single MongoDB document. To initialize it, execute the following command in a terminal:

```bash
curl -k -X GET https://localhost:5010/company-api/app-settings/initialize
```
