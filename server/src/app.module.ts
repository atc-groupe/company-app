import { Module } from '@nestjs/common';
import {
  CoreModule,
  LoggerModule,
  SecurityConfigModule,
} from './shared/modules';
import { MpModule } from './mp/mp.module';
import { ResourcesModule } from './resources/resources.module';
import { EventDispatchersModule } from './shared/modules/event-dispatchers.module';
import { ScheduleModule } from '@nestjs/schedule';
import { EventEmitterModule } from '@nestjs/event-emitter';

@Module({
  imports: [
    LoggerModule,
    MpModule,
    CoreModule,
    SecurityConfigModule,
    ResourcesModule,
    EventDispatchersModule,
    ScheduleModule.forRoot(),
    EventEmitterModule.forRoot(),
  ],
})
export class AppModule {}
