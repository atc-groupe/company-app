import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import { ValidationPipe } from '@nestjs/common';
import cookieParser = require('cookie-parser');

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const configService = app.get(ConfigService);

  const cookieParserSecret = configService.getOrThrow('COOKIE_PARSER_SECRET');

  app.setGlobalPrefix('company-api');
  app.enableCors();
  app.use(cookieParser(cookieParserSecret));
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      whitelist: true,
      transformOptions: { enableImplicitConversion: true },
    }),
  );

  await app.init();
  await app.listen(configService.get('API_PORT') ?? 80);
}
bootstrap();
