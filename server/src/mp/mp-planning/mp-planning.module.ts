import { Module } from '@nestjs/common';
import { MpHttpModule } from '../../shared/modules';
import { MpPlanningGetSetupRepository } from './mp-planning-get-setup.repository';
import { MpPlanningGetLinesRepository } from './mp-planning-get-lines.repository';
import { MpPlanningPushJobRepository } from './mp-planning-push-job.repository';

@Module({
  imports: [MpHttpModule],
  providers: [
    MpPlanningGetLinesRepository,
    MpPlanningGetSetupRepository,
    MpPlanningPushJobRepository,
  ],
  exports: [
    MpPlanningGetLinesRepository,
    MpPlanningGetSetupRepository,
    MpPlanningPushJobRepository,
  ],
})
export class MpPlanningModule {}
