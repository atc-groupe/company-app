export enum MpPlanningDepartmentEnum {
  prepress,
  offset,
  digital,
  finishing,
  subcontractors,
}
