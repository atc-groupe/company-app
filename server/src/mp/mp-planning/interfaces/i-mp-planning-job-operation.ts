import { MpPlanningDepartmentEnum } from '../enum';

export interface IMpPlanningJobOperation {
  operation: string;
  start_date: string;
  department: MpPlanningDepartmentEnum;
  calculated_time: number; // in hours
  production_time: number; // in hours
  ordering: number;
  employee: string; // Mp employee name
  information: string;
  printing_colors: string;
  extra_info: string;
  material: string;
  splited: string;
  assistances: number;
  machine: string;
  machine_id: number;
  machine_order: number;
}
