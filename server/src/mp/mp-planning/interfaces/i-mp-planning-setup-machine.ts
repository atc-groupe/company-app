import { IMpPlanningSetupMachineOperation } from './i-mp-planning-setup-operation';

export interface IMpPlanningSetupMachine {
  id: number;
  name: string;
  ordering: number;
  color: string;
  remark: string;
  operations: IMpPlanningSetupMachineOperation[];
}
