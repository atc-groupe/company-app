import { IMpPlanningCardDay } from './i-mp-planning-card-day';

export interface IMpPlanningCard {
  machine: {
    name: string;
    id: number;
  };
  start_date: Date;
  production_time: number;
  calculated_time: number;
  employee: string;
  extra_info: string;
  information: string;
  completed: boolean;
  job: {
    operation: string;
    job_number: number;
    company: string;
    contact_name: string;
    phone: string;
    email: string;
    description: string;
    job_status: string;
    job_status_number: number;
    delivery_date: Date;
    sending_date: Date;
    artwork_date: Date;
    creator: string;
    calculator: string;
    jobmanager: string;
    salesman: string;
    technical: string;
    product_part: string;
    material: string;
    quality: string;
    paper_ordered: boolean;
    paper_available: boolean;
    paper_cut: boolean;
    plates_ready: boolean;
    not_planned: boolean;
    files_ready: boolean;
  };
  days: IMpPlanningCardDay[];
}
