import { IMpPlanningSetupMachine } from './i-mp-planning-setup-machine';

export interface IMpPlanningSetupGroup {
  name: string;
  machine: IMpPlanningSetupMachine[];
}
