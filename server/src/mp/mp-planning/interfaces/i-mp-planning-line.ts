export interface IMpPlanningLine {
  date: Date;
  machine: {
    name: string;
    id: number;
  };
  description: string;
  time: number;
  ordering: number;
  id: number;
}
