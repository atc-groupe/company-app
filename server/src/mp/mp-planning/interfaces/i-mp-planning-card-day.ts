export interface IMpPlanningCardDay {
  date: Date;
  time: number;
  fixed_time_active: boolean;
  fixed_time: number;
  ordering: number;
}
