import { Injectable } from '@nestjs/common';
import { MpHttpAdapter } from '../../shared/adapters';
import { IMpPlanningSetupDepartment } from './interfaces';
import { Observable } from 'rxjs';

@Injectable()
export class MpPlanningGetSetupRepository {
  constructor(private _http: MpHttpAdapter) {}

  public getPlanningSetup(): Observable<IMpPlanningSetupDepartment[]> {
    return this._http.get<IMpPlanningSetupDepartment[]>('/planning/getSetup');
  }
}
