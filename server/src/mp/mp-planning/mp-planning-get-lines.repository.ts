import { Injectable, NotFoundException } from '@nestjs/common';
import { MpHttpAdapter } from '../../shared/adapters';
import { IMpPlanningGetLinesDto } from './dto';
import { catchError, Observable } from 'rxjs';
import { IMpPlanningCard, IMpPlanningLine } from './interfaces';
import { MpApiErrorException } from '../../shared/exceptions';
import { MpApiPlanningErrorEnum } from '../../shared/enum';

@Injectable()
export class MpPlanningGetLinesRepository {
  constructor(private _http: MpHttpAdapter) {}

  public getLines(dto: IMpPlanningGetLinesDto): Observable<IMpPlanningLine[]> {
    return this._http.get<IMpPlanningLine[]>('/planning/getLines', {
      params: dto,
    });
  }

  public getCard(id: number): Observable<IMpPlanningCard> {
    return this._http
      .get<IMpPlanningCard>('/planning/getLines', { params: { id } })
      .pipe(
        catchError((err) => {
          if (
            err instanceof MpApiErrorException &&
            err.errNumber === MpApiPlanningErrorEnum.PLANNING_LINE_NOT_FOUND
          ) {
            throw new NotFoundException(
              `La carte de planning ID: ${id} n'a pas été trouvée`,
            );
          }

          throw err;
        }),
      );
  }
}
