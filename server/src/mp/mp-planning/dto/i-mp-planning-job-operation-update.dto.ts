export interface IMpPlanningJobOperationUpdateDto {
  operation: string; // Can be updated
  start_date: Date | string; // Can be updated. string format: YYYY-MM-DD
  production_time: number; // Can be updated
  employee: string; // Mp employee name
  information: string;
  printing_colors: string;
  extra_info: string;
  material: string;
}
