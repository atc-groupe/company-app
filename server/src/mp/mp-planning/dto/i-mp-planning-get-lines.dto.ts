import { MpPlanningDepartmentEnum } from '../enum';

export interface IMpPlanningGetLinesDto {
  startdate: string; // Format YYYY-MM-DD
  stopdate?: string; // Format YYYY-MM-DD
  department?: MpPlanningDepartmentEnum;
  group?: string;
}
