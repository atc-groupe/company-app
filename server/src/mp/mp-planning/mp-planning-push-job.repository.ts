import {
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { MpHttpAdapter } from '../../shared/adapters';
import { catchError, EMPTY, Observable } from 'rxjs';
import { IMpPlanningJobOperation } from './interfaces/i-mp-planning-job-operation';
import { IMpPlanningJobOperationUpdateDto } from './dto';
import { MpApiErrorException } from '../../shared/exceptions';
import { MpApiPlanningErrorEnum } from '../../shared/enum';

@Injectable()
export class MpPlanningPushJobRepository {
  constructor(private _http: MpHttpAdapter) {}

  getJobPlanOperations(
    jobNumber: number,
  ): Observable<IMpPlanningJobOperation[]> {
    return this._http
      .get<IMpPlanningJobOperation[]>('/planning/pushJob', {
        params: { job_number: jobNumber },
      })
      .pipe(
        catchError((err) => {
          this._handlePushJobError(err, jobNumber);
          return EMPTY;
        }),
      );
  }

  planJob(jobNumber: number): Observable<void> {
    return this._http
      .post<void>('/planning/pushJob', {
        params: { job_number: jobNumber, not_planned: false },
      })
      .pipe(
        catchError((err) => {
          this._handlePushJobError(err, jobNumber);
          return EMPTY;
        }),
      );
  }

  unPlanJob(jobNumber: number): Observable<void> {
    return this._http
      .post<void>('/planning/pushJob', {
        params: { job_number: jobNumber, not_planned: true },
      })
      .pipe(
        catchError((err) => {
          this._handlePushJobError(err, jobNumber);
          return EMPTY;
        }),
      );
  }

  updateJobOperationData(
    jobNumber: number,
    dto: IMpPlanningJobOperationUpdateDto,
  ): Observable<void> {
    return this._http
      .post<void>('/planning/pushJob', {
        params: { job_number: jobNumber, data: JSON.stringify(dto) },
      })
      .pipe(
        catchError((err) => {
          this._handlePushJobError(err, jobNumber);
          return EMPTY;
        }),
      );
  }

  private _handlePushJobError(err: any, jobNumber: number): void {
    if (
      err instanceof MpApiErrorException &&
      err.errNumber === MpApiPlanningErrorEnum.JOB_IN_USE
    ) {
      throw new InternalServerErrorException(
        this._getLockedMessage(err.errText),
      );
    }

    if (
      err instanceof MpApiErrorException &&
      err.errNumber === MpApiPlanningErrorEnum.JOB_NOT_FOUND
    ) {
      throw new NotFoundException(`Le job n°${jobNumber} n' a pas été trouvé`);
    }

    throw err;
  }

  private _getLockedMessage(error: string): string {
    const split = error.split('"');
    const name = split[split.length - 2];

    return `Le job est utilisé par ${name}`;
  }
}
