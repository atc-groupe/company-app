export interface IMpEmployeeListItem {
  id: string;
  employee_number: number;
  name: string;
  department: string;
}
