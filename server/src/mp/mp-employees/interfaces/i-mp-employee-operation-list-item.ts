export interface IMpEmployeeOperationListItem {
  operation_number: number;
  operation: string;
  type: number;
  standard: boolean;
}
