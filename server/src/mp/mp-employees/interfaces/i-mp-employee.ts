import { IMpEmployeeOperationListItem } from './i-mp-employee-operation-list-item';

export interface IMpEmployee {
  id: string;
  name: string;
  address: string;
  zipcode: number;
  city: string;
  phone: string;
  hireddate: string;
  birthdate: string;
  position: string;
  employee_number: number;
  department: string;
  internal_phone: string;
  address_number: string;
  outofservicedate: string;
  bank_account: string;
  bankname: string;
  email: string;
  gender: string;
  mobile_phone: string;
  production_plant: string;
  free_choice1: string; // Configured in MP for AD account
  free_choice2: string;
  free_choice3: string;
  country: string;
  country_code: string;
  extra_address_1: string;
  extra_address_2: string;
  extra_address_3: string;
  extra_address_4: string;
  extra_address_5: string;
  holding: string;
  timestamp: string;
  bank_iban: string;
  bank_bic: string;
  birthdate_mmdd: number;
  operations: IMpEmployeeOperationListItem[];
}
