export interface IMpEmployeeOperation {
  operation: string;
  operation_number: number;
  ask_material: boolean;
  ask_cost: boolean;
  production: boolean;
  active: boolean;
  job_status?: string;
  job_status_number?: number;
}
