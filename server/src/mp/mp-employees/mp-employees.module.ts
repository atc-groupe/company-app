import { Module } from '@nestjs/common';
import { MpEmployeesListRepository } from './mp-employees-list.repository';
import { MpHttpModule, HelperModule } from '../../shared/modules';
import { MpEmployeesRepository } from './mp-employees.repository';
import { MpEmployeeWorksheetRepository } from './mp-employee-worksheet.repository';
import { MpEmployeesOperationsRepository } from './mp-employees-operations.repository';

@Module({
  imports: [MpHttpModule, HelperModule],
  providers: [
    MpEmployeesListRepository,
    MpEmployeesRepository,
    MpEmployeeWorksheetRepository,
    MpEmployeesOperationsRepository,
  ],
  exports: [
    MpEmployeesRepository,
    MpEmployeesListRepository,
    MpEmployeeWorksheetRepository,
    MpEmployeesOperationsRepository,
  ],
})
export class MpEmployeesModule {}
