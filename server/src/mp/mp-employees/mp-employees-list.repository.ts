import { Injectable } from '@nestjs/common';
import { map, Observable } from 'rxjs';
import { IMpEmployeeList, IMpEmployeeListItem } from './interfaces';
import { DataSorter } from '../../shared/services';
import { MpHttpAdapter } from '../../shared/adapters';

@Injectable()
export class MpEmployeesListRepository {
  constructor(
    private _http: MpHttpAdapter,
    private _dataSorter: DataSorter,
  ) {}

  /**
   * Returns MultiPress actives users list
   */
  findAll(): Observable<IMpEmployeeListItem[]> {
    return this._http.get<IMpEmployeeList>('/employees/getEmployeesList').pipe(
      map(({ employees }): IMpEmployeeListItem[] => {
        return this._dataSorter.sort<IMpEmployeeListItem>(
          employees,
          (item: IMpEmployeeListItem) => item.name.toUpperCase(),
        );
      }),
    );
  }
}
