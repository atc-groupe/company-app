import { Injectable } from '@nestjs/common';
import { catchError, Observable, of } from 'rxjs';
import { IMpEmployee } from './interfaces';
import { MpApiErrorException } from '../../shared/exceptions';
import { MpApiEmployeesErrorEnum } from '../../shared/enum';
import { MpHttpAdapter } from '../../shared/adapters';

@Injectable()
export class MpEmployeesRepository {
  constructor(private _http: MpHttpAdapter) {}

  findOneByNumber(employeeNumber: number): Observable<IMpEmployee | null> {
    return this._http
      .get<IMpEmployee>('/employees/getEmployeeInfo', {
        params: { employee_number: employeeNumber },
      })
      .pipe(
        catchError((err) => {
          if (
            err instanceof MpApiErrorException &&
            err.errNumber === MpApiEmployeesErrorEnum.RECORD_NOT_FOUND
          ) {
            return of(null);
          }

          throw err;
        }),
      );
  }
}
