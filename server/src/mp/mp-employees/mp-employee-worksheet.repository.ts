import {
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { MpHttpAdapter } from '../../shared/adapters';
import { catchError, map, Observable } from 'rxjs';
import { IMpEmployeeWorksheet } from './interfaces';
import { MpApiErrorException } from '../../shared/exceptions';
import {
  JobWorksheetCreateDto,
  JobWorksheetUpdateDto,
} from '../../resources/jobs/dto';

@Injectable()
export class MpEmployeeWorksheetRepository {
  constructor(private _http: MpHttpAdapter) {}

  public getWorksheet(id: string): Observable<IMpEmployeeWorksheet | null> {
    return this._http
      .get<IMpEmployeeWorksheet>(`/employees/handleWorkSheets`, {
        params: { id },
      })
      .pipe(
        map((worksheet) => {
          if (Array.isArray(worksheet) && worksheet.length === 0) {
            return null;
          }

          return worksheet;
        }),
        catchError(() => {
          throw new InternalServerErrorException(
            `Impossible de récupérer la fiche de travail. ID: ${id}`,
          );
        }),
      );
  }

  public createWorksheet(dto: JobWorksheetCreateDto): Observable<string> {
    return this._http
      .put<{ id: string; result: string }>(
        `/employees/handleWorkSheets`,
        {
          data: JSON.stringify(dto),
        },
        {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          },
        },
      )
      .pipe(
        map((result) => result.id),
        catchError(() => {
          throw new InternalServerErrorException(
            'Impossible de créer la fiche de travail',
          );
        }),
      );
  }

  public updateWorksheet(
    id: string,
    data: JobWorksheetUpdateDto,
  ): Observable<void> {
    return this._http
      .post<void>(
        `/employees/handleWorkSheets`,
        {
          id,
          data: JSON.stringify(data),
        },
        {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          },
        },
      )
      .pipe(
        catchError((err) => {
          if (err instanceof MpApiErrorException) {
            if (err.errNumber === 802) {
              throw new NotFoundException('Fiche de travail introuvable');
            }

            if (err.errNumber === 803) {
              throw new InternalServerErrorException(
                this._getLockedMessage(err.errText),
              );
            }
          }

          throw new InternalServerErrorException(
            'Impossible de modifier la fiche de travail',
          );
        }),
      );
  }

  public deleteWorksheet(id: string): Observable<void> {
    return this._http
      .delete<void>(`/employees/handleWorkSheets`, {
        params: { id },
      })
      .pipe(
        catchError((err) => {
          if (err instanceof MpApiErrorException) {
            if (err.errNumber === 802) {
              throw new NotFoundException('Worksheet not found');
            }

            if (err.errNumber === 803) {
              throw new InternalServerErrorException(
                this._getLockedMessage(err.errText),
              );
            }
          }

          throw new InternalServerErrorException(
            'Impossible de supprimer la fiche de travail',
          );
        }),
      );
  }

  private _getLockedMessage(error: string): string {
    const split = error.split('"');
    const name = split[split.length - 2];

    return `La fiche de travail est utilisée par ${name}`;
  }
}
