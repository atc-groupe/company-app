import { Injectable } from '@nestjs/common';
import { MpHttpAdapter } from '../../shared/adapters';
import { map, Observable } from 'rxjs';
import { IMpEmployeeOperation } from './interfaces';

@Injectable()
export class MpEmployeesOperationsRepository {
  constructor(private _http: MpHttpAdapter) {}

  public getOperationList(): Observable<IMpEmployeeOperation[]> {
    return this._http
      .get<{
        operations: IMpEmployeeOperation[];
      }>('/employees/getOperationsList')
      .pipe(map(({ operations }) => operations));
  }
}
