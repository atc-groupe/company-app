export interface IMpProductListGroupItem {
  product_id: number;
  product_description: string;
}
