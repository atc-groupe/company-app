export interface IMpStockReservation {
  product_type: number;
  product_id: number;
  job_number: number;
  company: string;
  description: string;
  delivery_date: Date;
  reserved: number;
  unit_id: number;
  unit_txt: string;
  stock: number;
  minimum: number;
  article_number: string;
  type: string;
  extra_info: string;
  size: string;
  weight: number;
  color: string;
  grain: string;
  remark: string;
}
