export interface IMpStockMutationListItem {
  mutation_id: number;
  job_number: number;
  subtract: number;
  add: number;
  description: string;
}
