import { IMpProductListGroupItem } from './i-mp-product-list-group-item';

export interface IMpProductListGroup {
  group_type: number;
  group_description: string;
  articles: IMpProductListGroupItem[];
}
