export interface IMpStockMutationResponse {
  mutation_id: number;
  result: string;
}
