export interface IMpProductSupplier {
  name: string;
  number: number;
  standard: boolean;
  article: string;
  alternative: string;
  delivery_days: number;
  intrastat: string;
  delivery_unit: string;
  delivery_amount: number;
  minimum_amount: number;
  maximum_amount: number;
  unit_amount: number;
  documentation: string;
  information: string;
}
