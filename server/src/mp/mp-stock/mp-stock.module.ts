import { Module } from '@nestjs/common';
import { MpStockGetArticleListRepository } from './mp-stock-get-article-list.repository';
import { MpHttpModule } from '../../shared/modules';
import { MpStockHandleArticleDetailsRepository } from './mp-stock-handle-article-details.repository';
import { MpStockHandleMutationRepository } from './mp-stock-handle-mutation.repository';

@Module({
  imports: [MpHttpModule],
  providers: [
    MpStockGetArticleListRepository,
    MpStockHandleArticleDetailsRepository,
    MpStockHandleMutationRepository,
  ],
  exports: [
    MpStockGetArticleListRepository,
    MpStockHandleArticleDetailsRepository,
    MpStockHandleMutationRepository,
  ],
})
export class MpStockModule {}
