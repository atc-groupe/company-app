import { Injectable } from '@nestjs/common';
import { MpHttpAdapter } from '../../shared/adapters';
import { ProductTypeEnum } from './enums';
import { catchError, Observable, of } from 'rxjs';
import { IMpArticle, IMpCustomerStock, IMpPaper } from './interfaces';
import { MpApiErrorException } from '../../shared/exceptions';
import { MpApiStockErrorEnum } from '../../shared/enum';

@Injectable()
export class MpStockHandleArticleDetailsRepository {
  constructor(private _http: MpHttpAdapter) {}

  public getArticleDetails(articleId: number): Observable<IMpArticle | null> {
    return this._getProductDetails<IMpArticle>(
      articleId,
      ProductTypeEnum.Article,
    );
  }

  public getCustomerStockDetails(
    id: number,
  ): Observable<IMpCustomerStock | null> {
    return this._getProductDetails<IMpCustomerStock>(
      id,
      ProductTypeEnum.CustomerProduct,
    );
  }

  public getPaperDetails(paperId: number): Observable<IMpPaper | null> {
    return this._getProductDetails<IMpPaper>(paperId, ProductTypeEnum.Paper);
  }

  private _getProductDetails<
    T extends IMpArticle | IMpCustomerStock | IMpPaper,
  >(productId: number, productType: ProductTypeEnum): Observable<T | null> {
    return this._http
      .get<T>('/stock/handleArticleDetails', {
        params: {
          product_id: productId,
          product_type: productType,
        },
      })
      .pipe(
        catchError((err) => {
          if (
            err instanceof MpApiErrorException &&
            err.errNumber === MpApiStockErrorEnum.ARTICLE_NOT_FOUND
          ) {
            return of(null);
          }

          throw err;
        }),
      );
  }
}
