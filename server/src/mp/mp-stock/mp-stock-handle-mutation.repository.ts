import {
  BadRequestException,
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { MpHttpAdapter } from '../../shared/adapters';
import { StockMutationCreateDto } from './dto';
import { catchError, map, Observable } from 'rxjs';
import {
  IMpStockMutation,
  IMpStockMutationListItem,
  IMpStockMutationResponse,
} from './interfaces';
import { MpApiErrorException } from '../../shared/exceptions';
import { MpApiStockErrorEnum } from '../../shared/enum';

@Injectable()
export class MpStockHandleMutationRepository {
  constructor(private _http: MpHttpAdapter) {}

  public createMutation(
    productType: number,
    productId: number,
    dto: StockMutationCreateDto,
  ): Observable<IMpStockMutationResponse> {
    return this._http
      .put<IMpStockMutationResponse>(
        `/stock/handleStockMutation`,
        {
          product_type: productType,
          product_id: productId,
          employee_number: dto.employee_number,
          data: JSON.stringify(dto.data),
        },
        {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          },
        },
      )
      .pipe(
        catchError((err) => {
          this._handleSetMutationError(err, dto);
          throw err;
        }),
      );
  }

  public updateMutation(
    productType: number,
    productId: number,
    mutationId: number,
    dto: StockMutationCreateDto,
  ): Observable<IMpStockMutationResponse> {
    return this._http
      .put<IMpStockMutationResponse>(
        '/stock/handleStockMutation',
        {
          mutation_id: mutationId,
          product_type: productType,
          product_id: productId,
          employee_number: dto.employee_number,
          data: JSON.stringify(dto.data),
        },
        {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          },
        },
      )
      .pipe(
        catchError((err) => {
          this._handleSetMutationError(err, dto);
          throw err;
        }),
      );
  }

  public getMutation(
    productType: number,
    productId: number,
    mutationId: number,
  ) {
    return this._http
      .get<IMpStockMutation>('/stock/handleStockMutation', {
        params: {
          product_type: productType,
          product_id: productId,
          mutation_id: mutationId,
        },
      })
      .pipe(
        catchError((err) => {
          this._handleBaseError(err);
          throw err;
        }),
      );
  }

  public getMutations(
    productType: number,
    productId: number,
    startDate: string,
    stopDate?: string,
  ) {
    const params: any = {
      startdate: startDate,
      product_type: productType,
      product_id: productId,
    };

    if (stopDate) {
      params.stopDate = stopDate;
    }

    return this._http
      .get<IMpStockMutationListItem[]>('/stock/handleStockMutation', { params })
      .pipe(
        map((list) => {
          return list.sort((a, b) => {
            const nameA = a.mutation_id;
            const nameB = b.mutation_id;
            if (nameA > nameB) {
              return -1;
            }
            if (nameA < nameB) {
              return 1;
            }

            return 0;
          });
        }),
        catchError((err) => {
          this._handleBaseError(err);
          throw err;
        }),
      );
  }

  private _handleBaseError(err: any): void {
    if (
      err.errNumber === MpApiStockErrorEnum.ARTICLE_NOT_FOUND ||
      err.errNumber === MpApiStockErrorEnum.NO_ARTICLE_ENTERED
    ) {
      throw new NotFoundException(err.errText);
    }

    if (
      err.errNumber === MpApiStockErrorEnum.ARTICLE_IN_USE ||
      err.errNumber === MpApiStockErrorEnum.MUTATION_IN_USE
    ) {
      throw new ConflictException(this._getLockedMessage(err.errText));
    }
  }

  private _handleSetMutationError(err: any, dto: StockMutationCreateDto): void {
    if (err instanceof MpApiErrorException) {
      this._handleBaseError(err);

      if (
        err.errNumber === MpApiStockErrorEnum.INVALID_AND_OR_INSUFFICIENT_DATA
      ) {
        throw new BadRequestException(err.errText);
      }

      if (err.errNumber === MpApiStockErrorEnum.JOB_NOT_FOUND_OR_USED) {
        throw new BadRequestException(
          `Le job ${dto.data.job_number} n'existe pas ou est en cours d'utilisation.`,
        );
      }
    }
  }

  private _getLockedMessage(error: string): string {
    const split = error.split('"');
    const name = split[split.length - 2];

    return `L'article est utilisé par ${name}`;
  }
}
