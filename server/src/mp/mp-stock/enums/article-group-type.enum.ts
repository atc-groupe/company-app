export enum ArticleGroupTypeEnum {
  All = 0,
  OtherFinishes = 8,
  LargeFormat = 12,
  Ink = 90,
}
