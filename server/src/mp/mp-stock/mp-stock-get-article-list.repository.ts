import {
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { MpHttpAdapter } from '../../shared/adapters';
import {
  ArticleGroupTypeEnum,
  PaperGroupTypeEnum,
  ProductTypeEnum,
} from './enums';
import { IMpProductListGroup } from './interfaces';
import { catchError } from 'rxjs';
import { MpApiErrorException } from '../../shared/exceptions';
import { MpApiStockErrorEnum } from '../../shared/enum';
import { CustomerStockGroupTypeEnum } from './enums/customer-stock-group-type.enum';

@Injectable()
export class MpStockGetArticleListRepository {
  constructor(private _http: MpHttpAdapter) {}

  public getArticleList(
    groupType: ArticleGroupTypeEnum = ArticleGroupTypeEnum.All,
    search?: string,
  ) {
    return this._getStockList(ProductTypeEnum.Article, groupType, search);
  }

  public getCustomerProductList(
    groupType:
      | CustomerStockGroupTypeEnum
      | number = CustomerStockGroupTypeEnum.All,
    search?: string,
  ) {
    return this._getStockList(
      ProductTypeEnum.CustomerProduct,
      groupType,
      search,
    );
  }

  public getPaperList(
    groupType: PaperGroupTypeEnum = PaperGroupTypeEnum.All,
    search?: string,
  ) {
    return this._getStockList(ProductTypeEnum.Paper, groupType, search);
  }

  private _getStockList(
    productType: ProductTypeEnum,
    groupType: number,
    search?: string,
  ) {
    const searchParams: any = [
      { field: 'status', comparator: '=', value: 'Actif' },
    ];

    if (
      productType === ProductTypeEnum.Paper &&
      groupType === PaperGroupTypeEnum.Sheets
    ) {
      searchParams.push({
        conjenction: 'and',
        field: 'attribute_8',
        comparator: '=',
        value: '',
      });
    }

    if (search && productType === ProductTypeEnum.Article) {
      searchParams.push({
        conjenction: 'and',
        field: 'description',
        comparator: '=',
        value: `@${search}@`,
      });
    }

    if (search && productType === ProductTypeEnum.Paper) {
      searchParams.push({
        conjenction: 'and',
        field: 'extra_info',
        comparator: '=',
        value: `@${search}@`,
      });
    }

    return this._http
      .get<IMpProductListGroup[]>('/stock/getArticleList', {
        params: {
          product_type: productType,
          group_type: groupType,
          filter: JSON.stringify(searchParams),
        },
      })
      .pipe(
        catchError((err) => {
          if (
            err instanceof MpApiErrorException &&
            err.errNumber === MpApiStockErrorEnum.NO_ARTICLE_ENTERED
          ) {
            throw new NotFoundException(
              "Impossible de récupérer la liste d'articles",
            );
          }

          throw new InternalServerErrorException(
            'Un probleme est survenu lors de la récupération des articles',
          );
        }),
      );
  }
}
