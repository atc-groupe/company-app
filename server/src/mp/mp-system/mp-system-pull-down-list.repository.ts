import { Injectable } from '@nestjs/common';
import { defer, Observable } from 'rxjs';
import { IMpPullDownList } from './interfaces';
import { MpPullDownListEnum } from './enum';
import { MpHttpAdapter } from '../../shared/adapters';

@Injectable()
export class MpSystemPullDownListRepository {
  constructor(private _http: MpHttpAdapter) {}

  public getList(id: MpPullDownListEnum): Observable<IMpPullDownList> {
    return defer(() =>
      this._http.get<IMpPullDownList>(`system/getPulldownlist`, {
        params: { id },
      }),
    );
  }
}
