import { Module } from '@nestjs/common';
import { MpSystemPullDownListRepository } from './mp-system-pull-down-list.repository';
import { MpHttpModule } from '../../shared/modules/mp-http.module';

@Module({
  imports: [MpHttpModule],
  providers: [MpSystemPullDownListRepository],
  exports: [MpSystemPullDownListRepository],
})
export class MpSystemModule {}
