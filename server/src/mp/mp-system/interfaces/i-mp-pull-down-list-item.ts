export interface IMpPullDownListItem {
  id: string;
  description: string;
  text: string;
  ordering: number;
}
