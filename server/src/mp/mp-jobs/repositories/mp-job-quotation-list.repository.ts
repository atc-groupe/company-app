import { Injectable } from '@nestjs/common';
import { map, Observable } from 'rxjs';
import { IMpJobQuotationList, IMpJobQuotationListItem } from '../interfaces';
import { MpHttpAdapter } from '../../../shared/adapters';

@Injectable()
export class MpJobQuotationListRepository {
  constructor(protected _http: MpHttpAdapter) {}

  public findActiveJobs(): Observable<IMpJobQuotationListItem[] | null> {
    return this._http
      .get<IMpJobQuotationList>('/jobs/getJobsQuotations', {
        params: {
          type: 0,
          filter: JSON.stringify({
            field: 'job_status_number',
            comparator: '<',
            value: 1000,
          }),
        },
      })
      .pipe(
        map((list) => {
          if (!list.jobs.length) {
            return null;
          }

          return list.jobs;
        }),
      );
  }
}
