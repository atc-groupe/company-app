import { Injectable } from '@nestjs/common';
import { catchError, map, Observable, of } from 'rxjs';
import { IMpDeliveryAddress } from '../interfaces';
import { MpApiErrorException } from '../../../shared/exceptions';
import { MpApiJobsErrorEnum } from '../../../shared/enum';
import { MpHttpAdapter } from '../../../shared/adapters';

@Injectable()
export class MpDeliveryAddressRepository {
  constructor(protected _http: MpHttpAdapter) {}

  findOneById(id: number): Observable<IMpDeliveryAddress | null> {
    return this._http
      .get<IMpDeliveryAddress | null>('/jobs/handleDeliveryAddressDetails', {
        params: { id },
      })
      .pipe(
        catchError((err) => {
          if (
            err instanceof MpApiErrorException &&
            err.errNumber === MpApiJobsErrorEnum.JOB_OR_QUOTATION_NOT_FOUND
          ) {
            return of(null);
          }

          throw err;
        }),
      );
  }
}
