import { Injectable } from '@nestjs/common';
import { catchError, Observable, of } from 'rxjs';
import { IMpJobQuotation } from '../interfaces';
import { MpApiErrorException } from '../../../shared/exceptions';
import { MpApiJobsErrorEnum } from '../../../shared/enum';
import { MpHttpAdapter } from '../../../shared/adapters';

@Injectable()
export class MpJobsQuotationRepository {
  constructor(protected _http: MpHttpAdapter) {}

  public findOneById(id: number): Observable<IMpJobQuotation | null> {
    return this._findOneBy({ id });
  }

  public findOneByNumber(number: number): Observable<IMpJobQuotation | null> {
    return this._findOneBy({ job_number: number });
  }

  private _findOneBy(params: any): Observable<IMpJobQuotation | null> {
    return this._http
      .get<IMpJobQuotation | null>('/jobs/handleJobsQuotationsDetails', {
        params,
      })
      .pipe(
        catchError((err) => {
          if (
            err instanceof MpApiErrorException &&
            err.errNumber === MpApiJobsErrorEnum.JOB_OR_QUOTATION_NOT_FOUND
          ) {
            return of(null);
          }

          throw err;
        }),
      );
  }
}
