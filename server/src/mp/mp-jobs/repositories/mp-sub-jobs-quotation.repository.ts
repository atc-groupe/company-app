import { Injectable } from '@nestjs/common';
import { catchError, Observable, of } from 'rxjs';
import { IMpSubJobQuotation } from '../interfaces';
import { MpApiErrorException } from '../../../shared/exceptions';
import { MpApiJobsErrorEnum } from '../../../shared/enum';
import { MpHttpAdapter } from '../../../shared/adapters';

@Injectable()
export class MpSubJobsQuotationRepository {
  constructor(protected _http: MpHttpAdapter) {}

  findOne(id: number): Observable<IMpSubJobQuotation | null> {
    return this._http
      .get<IMpSubJobQuotation | null>('/jobs/handleSubJobsQuotationsDetails', {
        params: { id },
      })
      .pipe(
        catchError((err) => {
          if (
            err instanceof MpApiErrorException &&
            err.errNumber === MpApiJobsErrorEnum.SUB_JOB_OR_QUOTATION_NOT_FOUND
          ) {
            return of(null);
          }

          throw err;
        }),
      );
  }
}
