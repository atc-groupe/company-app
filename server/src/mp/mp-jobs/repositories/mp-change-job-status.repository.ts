import { BadRequestException, Injectable } from '@nestjs/common';
import { catchError, Observable } from 'rxjs';
import {
  MpApiErrorException,
  SubJobLockedErrorException,
} from '../../../shared/exceptions';
import { MpApiJobsErrorEnum } from '../../../shared/enum';
import { MpHttpAdapter } from '../../../shared/adapters';
import { JobsStatusChangeDto } from '../../../resources/jobs/dto';

@Injectable()
export class MpChangeJobStatusRepository {
  constructor(protected _http: MpHttpAdapter) {}

  changeStatus(
    mpNumber: number,
    dto: JobsStatusChangeDto,
  ): Observable<unknown> {
    return this._http
      .post(
        '/jobs/changeJobStatus',
        {
          job_number: mpNumber,
          job_status_number: dto.statusNumber,
          reason: dto.reason,
        },
        {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          },
        },
      )
      .pipe(
        catchError((err) => {
          if (
            err instanceof MpApiErrorException &&
            err.errNumber === MpApiJobsErrorEnum.JOB_OR_QUOTATION_NOT_FOUND
          ) {
            throw new BadRequestException(
              `Le job est introuvable. Numéro de job: ${mpNumber}`,
            );
          }

          if (
            err instanceof MpApiErrorException &&
            (err.errNumber === MpApiJobsErrorEnum.JOB_OR_QUOTATION_IN_USE ||
              err.errNumber === MpApiJobsErrorEnum.SUB_JOB_OR_QUOTATION_IN_USE)
          ) {
            throw new SubJobLockedErrorException(
              this._getLockedMessage(err.errText),
            );
          }

          throw err;
        }),
      );
  }

  private _getLockedMessage(error: string): string {
    const split = error.split('"');
    const name = split[split.length - 2];

    return `Le job est utilisé par ${name}`;
  }
}
