import { IMpJobQuotationListItem } from './i-mp-job-quotation-list-item';

export interface IMpJobQuotationList {
  jobs: IMpJobQuotationListItem[];
}
