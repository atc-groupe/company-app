export interface IMpDeliveryAddress {
  company: string;
  address: string;
  zipcode: string;
  city: string;
  contact_name: string;
  run: number;
  packed_per: number;
  description: string;
  starting_number: number;
  packages: number;
  remark: string;
  branch: string;
  country: string;
  relation: string;
  address_number: string;
  relation_number: number;
  delivery_method: string;
  phone: string;
  stepping_number: number;
  country_code: string;
  delivery_date: string;
  fax: string;
  ordering: number;
  sending_date: string;
  extra_info: string;
  extra_address_1: string;
  extra_address_2: string;
  extra_address_3: string;
  extra_address_4: string;
  extra_address_5: string;
  email: string;
  weight: number;
  status: string;
  timestamp: string;
  parent: number; // Job id
}
