import { IMpJobQuotationSubJobsItem } from './i-mp-job-quotation-sub-jobs-item';
import { IMpJobQuotationDeliveryAddressesItem } from './i-mp-job-quotation-delivery-addresses-item';

export interface IMpJobQuotation {
  quotation_number: number; // -
  quotation_date: string; // -
  company: string;
  contact_name: string;
  description: string;
  fax: string;
  phone: string;
  price: number;
  timestamp: string;
  job_status_number: number;
  quotation_price1: number;
  quotation_price2: number;
  quotation_price3: number;
  job_number: number;
  job_status: string;
  job_date: string;
  delivery_date: string;
  reference: string;
  invoice_status: 'OUI' | 'NON';
  info_digital: string;
  delivery_week: number;
  relation_number: number;
  search_code: string;
  salesman: string; // Commercial
  quotation_status: string;
  email: string; // Contact
  jobmanager: string;
  info_general: string;
  info_prepress: string;
  info_print: string;
  info_finishing: string;
  info_subcontract: string;
  info_expedition: string;
  delivery_date_locked: boolean;
  job_status_history: string;
  planned: boolean;
  delivered: string; // Date of the delivery
  calculator: string;
  recall_date: string;
  info_planning: string;
  deviating_plan_date: string;
  artwork_date: string;
  proof_date: string;
  holding: string;
  return_date: string;
  creator: string; // Personne qui a créé le job.
  quotation_status_history: string;
  invoice_address: string;
  invoice_company: string;
  invoice_number: number;
  sending_date: string;
  paper_certificate: string;
  production_plant: string;
  invoice_data: string;
  free_choice1: string;
  free_choice2: string;
  free_choice3: string;
  files_delivered: boolean;
  type: number;
  id: number;
  sub_jobs: IMpJobQuotationSubJobsItem[];
  delivery_address: IMpJobQuotationDeliveryAddressesItem[];
}
