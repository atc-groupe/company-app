export interface IMpSubJobQuotationVersion {
  version: string;
  quantity: number;
}
