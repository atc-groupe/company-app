export interface IMpJobQuotationDeliveryAddressesItem {
  id: number;
  description: string;
}
