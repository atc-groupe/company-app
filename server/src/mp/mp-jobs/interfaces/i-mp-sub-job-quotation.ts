import { IMpSubJobQuotationVersion } from './i-mp-sub-job-quotation-version';

export interface IMpSubJobQuotation {
  run_01: number;
  run_02: number;
  run_03: number;
  text_01: string; // description
  text_02: string; // format
  text_03: string; // pao
  text_04: string; // print
  text_05: string; // material
  text_06: string; // finishing
  text_07: string; // packaging
  text_08: string; // delivery
  text_09: string; // application
  text_10: string;
  text_11: string;
  text_12: string; // remark
  number_of_addresses: number;
  price_1: number;
  price_2: number;
  price_3: number;
  standard_quotation: number; // Id of the sub-job
  total_1: number;
  total_2: number;
  total_3: number;
  additional_runs: number;
  description: string;
  discount_percentage_1: number;
  discount_percentage_2: number;
  discount_percentage_3: number;
  job_additional_price: number;
  additional_price_1: number;
  additional_price_2: number;
  job_price: number;
  delivered_runs: number;
  last_job_price: number;
  transport_costs_1: number;
  transport_costs_2: number;
  transport_costs_3: number;
  last_job_run: number;
  fee_percentage: number;
  fee_price_1: number;
  fee_price_2: number;
  fee_price_3: number;
  transport_distance: number;
  bruto_weight_1: number;
  bruto_weight_2: number;
  bruto_weight_3: number;
  checklist: number;
  last_job_number: number;
  product_number: number;
  product_type: string;
  article_number: string;
  article_type: string;
  internal_remark_1: string;
  internal_remark_2: string;
  unit_price_1: number;
  unit_price_2: number;
  unit_price_3: number;
  unit_price_job: number;
  reprint: string;
  filing_code: string;
  last_filing_code: string;
  cost_price_1: number;
  cost_price_2: number;
  cost_price_3: number;
  fixed_costs: number;
  hold_price: boolean;
  last_job_date: string;
  free_choice1: string;
  free_choice2: string;
  free_choice3: string;
  unit_factor: number;
  original_sub_quotation: number;
  netto_weight_1: number; // Poid
  netto_weight_2: number;
  netto_weight_3: number;
  variable_costs_1: number;
  variable_costs_2: number;
  variable_costs_3: number;
  variable_costs_job: number;
  paper_claim: string;
  alternative: boolean;
  parent: number; // Parent job id
  project_number: number;
  versions?: IMpSubJobQuotationVersion[];
}
