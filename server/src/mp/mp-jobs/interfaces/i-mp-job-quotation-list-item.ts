export interface IMpJobQuotationListItem {
  id: number;
  job_number: number;
  description: string;
  job_status: string;
  job_date: Date;
  quotation_date: Date;
  quotation_number: number;
  quotation_status: string;
}
