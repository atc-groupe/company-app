import { Module } from '@nestjs/common';
import {
  MpJobsQuotationRepository,
  MpDeliveryAddressRepository,
  MpSubJobsQuotationRepository,
  MpJobQuotationListRepository,
  MpChangeJobStatusRepository,
} from './repositories';
import { MpHttpModule } from '../../shared/modules/mp-http.module';

@Module({
  imports: [MpHttpModule],
  providers: [
    MpChangeJobStatusRepository,
    MpDeliveryAddressRepository,
    MpJobsQuotationRepository,
    MpSubJobsQuotationRepository,
    MpJobQuotationListRepository,
  ],
  exports: [
    MpChangeJobStatusRepository,
    MpDeliveryAddressRepository,
    MpJobsQuotationRepository,
    MpSubJobsQuotationRepository,
    MpJobQuotationListRepository,
  ],
})
export class MpJobsModule {}
