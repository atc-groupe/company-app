import { Module } from '@nestjs/common';
import { MpEmployeesModule } from './mp-employees/mp-employees.module';
import { MpJobsModule } from './mp-jobs/mp-jobs.module';
import { MpWorkflowModule } from './mp-workflow/mp-workflow.module';
import { MpSystemModule } from './mp-system/mp-system.module';
import { MpPlanningModule } from './mp-planning/mp-planning.module';
import { MpStockModule } from './mp-stock/mp-stock.module';

@Module({
  imports: [
    MpEmployeesModule,
    MpJobsModule,
    MpWorkflowModule,
    MpSystemModule,
    MpPlanningModule,
    MpStockModule,
  ],
})
export class MpModule {}
