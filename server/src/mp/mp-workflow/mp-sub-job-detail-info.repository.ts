import { Injectable } from '@nestjs/common';
import { catchError, Observable, of, switchMap } from 'rxjs';
import { IMpSubJobDetailInfo } from './interfaces';
import {
  MpApiErrorException,
  SubJobLockedErrorException,
} from '../../shared/exceptions';
import { MpApiWorkflowErrorEnum } from '../../shared/enum';
import { MpHttpAdapter } from '../../shared/adapters';

@Injectable()
export class MpSubJobDetailInfoRepository {
  constructor(private _http: MpHttpAdapter) {}

  public getData(id: number): Observable<IMpSubJobDetailInfo | null> {
    return this._http
      .delete<void>('/workflow/handleJobDetailInfo', {
        params: { id, fullreset: true },
      })
      .pipe(
        switchMap(() =>
          this._http.get<IMpSubJobDetailInfo | null>(
            '/workflow/handleJobDetailInfo',
            {
              params: { id },
            },
          ),
        ),

        catchError((err) => {
          if (
            err instanceof MpApiErrorException &&
            err.errNumber ===
              MpApiWorkflowErrorEnum.SUB_JOB_OR_QUOTATION_NOT_FOUND
          ) {
            return of(null);
          }

          if (
            err instanceof MpApiErrorException &&
            err.errNumber === MpApiWorkflowErrorEnum.SUB_JOB_OR_QUOTATION_IN_USE
          ) {
            throw new SubJobLockedErrorException(
              this._getLockedMessage(err.errText),
            );
          }

          throw err;
        }),
      );
  }

  private _getLockedMessage(error: string): string {
    const split = error.split('"');
    const name = split[split.length - 2];

    return `Un sous job est utilisé par ${name}`;
  }
}
