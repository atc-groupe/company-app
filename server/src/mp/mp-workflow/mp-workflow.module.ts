import { Module } from '@nestjs/common';
import { MpSubJobDetailInfoRepository } from './mp-sub-job-detail-info.repository';
import { MpHttpModule } from '../../shared/modules/mp-http.module';

@Module({
  imports: [MpHttpModule],
  providers: [MpSubJobDetailInfoRepository],
  exports: [MpSubJobDetailInfoRepository],
})
export class MpWorkflowModule {}
