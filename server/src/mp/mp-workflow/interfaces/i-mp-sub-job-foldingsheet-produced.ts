export interface IMpSubJobFoldingsheetProduced {
  id: string;
  versiontext: string;
  status: string;
  amount: number;
  needed: number;
  originate: string;
  pallets: string;
}
