export interface IMpSubJobSubcontractor {
  relation: string;
  relation_number: number;
  contact_name: string;
  description: string;
  remark: string;
  run: number;
  purchaseprice: number;
  salesprice: number;
  ordered: string;
  sending_date: string;
  delivery_date: string;
}
