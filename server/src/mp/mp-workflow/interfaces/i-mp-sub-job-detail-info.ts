import { IMpSubJobPrintingSheet } from './i-mp-sub-job-printing-sheet';
import { IMpSubJobFoldingSheet } from './i-mp-sub-job-folding-sheet';
import { IMpSubJobSubcontractor } from './i-mp-sub-job-subcontractor';
import { IMpSubJobLargeFormatFinishing } from './i-mp-sub-job-large-format-finishing';
import { IMpSubJobLaminationFinishing } from './i-mp-sub-job-lamination-finishing';

export interface IMpSubJobDetailInfo {
  printingsheets: IMpSubJobPrintingSheet[];
  foldingsheets: IMpSubJobFoldingSheet[];
  finishing: (IMpSubJobLaminationFinishing | IMpSubJobLargeFormatFinishing)[];
  subcontractors: IMpSubJobSubcontractor[];
}
