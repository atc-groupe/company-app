import { Injectable } from '@nestjs/common';
import { IMeState } from './i-me-state';
import { Socket } from 'socket.io';
import { UserDocument } from '../users/schema';

@Injectable()
export class MeStateService {
  private _usersStates: IMeState[] = [];

  public addState(user: UserDocument, socket: Socket): void {
    const state = this._usersStates.find((item) => item.user._id === user._id);

    if (state) {
      state.socket = socket;
    } else {
      this._usersStates.push({ user, socket });
    }
  }

  public removeState(socketId: string): void {
    this._usersStates = this._usersStates.filter(
      (item) => item.socket.id !== socketId,
    );
  }

  public get usersStates(): IMeState[] {
    return this._usersStates;
  }
}
