import {
  OnGatewayConnection,
  OnGatewayDisconnect,
  WebSocketGateway,
} from '@nestjs/websockets';
import { MeStateService } from './me-state.service';
import { Socket } from 'socket.io';
import { User } from '../users/schema';
import { UserRepository } from '../users/user.repository';
import { firstValueFrom } from 'rxjs';

@WebSocketGateway({
  transports: ['websocket'],
  namespace: 'me',
})
export class MeGateway implements OnGatewayConnection, OnGatewayDisconnect {
  constructor(
    private _states: MeStateService,
    private _userRepository: UserRepository,
  ) {}

  async handleConnection(client: Socket): Promise<void> {
    try {
      const user = await firstValueFrom(
        this._userRepository.findOne(client.handshake.auth.id),
      );

      if (!user) {
        return;
      }

      this._states.addState(user, client);
    } catch {}
  }

  handleDisconnect(client: any): any {
    this._states.removeState(client.id);
  }

  public emitUpdate(socket: Socket, user: User): void {
    socket.emit('update', user);
  }
}
