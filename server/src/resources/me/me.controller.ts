import { Body, Controller, Get, Patch, Req } from '@nestjs/common';
import { MeService } from './me.service';
import { IAppRequest } from '../../shared/interfaces';
import { Observable } from 'rxjs';
import { User } from '../users/schema';
import { UserSettingsUpdateDto } from './user-settings-update.dto';
import { UsersService } from '../users/services';

@Controller('me')
export class MeController {
  constructor(
    private _meService: MeService,
    private _usersService: UsersService,
  ) {}

  @Get()
  public getAuthenticatedUser(
    @Req() req: IAppRequest,
  ): Observable<User> | null {
    return req.user?._id
      ? this._usersService.findOne(req.user._id.toString())
      : null;
  }

  @Patch('settings')
  public updateMySettings(
    @Req() req: IAppRequest,
    @Body() dto: UserSettingsUpdateDto,
  ): Observable<User> {
    return this._meService.updateSettings(req.user._id.toString(), dto);
  }
}
