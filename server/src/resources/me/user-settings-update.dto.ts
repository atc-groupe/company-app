import {
  PlanningModeEnum,
  PlanningPeriodEnum,
  UiModeEnum,
} from '../users/enum';
import { IsBoolean, IsEnum, IsOptional, IsString } from 'class-validator';
import { Types } from 'mongoose';

export class UserSettingsUpdateDto {
  @IsEnum(UiModeEnum)
  @IsOptional()
  uiMode: UiModeEnum;

  @IsEnum(PlanningModeEnum)
  @IsOptional()
  planningMode: PlanningModeEnum;

  @IsEnum(PlanningPeriodEnum)
  @IsOptional()
  planningPeriod: PlanningPeriodEnum;

  @IsString()
  planningView: string | Types.ObjectId;

  @IsBoolean()
  @IsOptional()
  planningHideNotPlanned: boolean;

  @IsBoolean()
  @IsOptional()
  planningHideDone: boolean;

  @IsBoolean()
  @IsOptional()
  planningMergeJobOperationCards: boolean;
}
