import { Injectable } from '@nestjs/common';
import { IEventListener } from '../../shared/interfaces';
import { UsersEventDispatcher } from '../../shared/event-dispatchers/users.event-dispatcher';
import { UserDocument } from '../users/schema';
import { MeStateService } from './me-state.service';
import { MeGateway } from './me.gateway';

@Injectable()
export class UserEventListener implements IEventListener {
  constructor(
    eventDispatcher: UsersEventDispatcher,
    private _states: MeStateService,
    private _gateway: MeGateway,
  ) {
    eventDispatcher.addListener(this);
  }

  on(name: string, user: UserDocument): any {
    if (name === 'create') {
      return;
    }

    this._states.usersStates.forEach((state) => {
      if (state.user.ad.userPrincipalName === user.ad.userPrincipalName) {
        state.user = user;
        this._gateway.emitUpdate(state.socket, user);
      }
    });
  }
}
