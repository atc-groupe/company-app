import { Injectable } from '@nestjs/common';
import { RolesEventDispatcher } from '../../shared/event-dispatchers';
import { IEventListener } from '../../shared/interfaces';
import { Role } from '../roles/role.schema';
import { MeStateService } from './me-state.service';
import { MeGateway } from './me.gateway';
import { firstValueFrom } from 'rxjs';
import { UsersService } from '../users/services';

@Injectable()
export class RolesEventListener implements IEventListener {
  constructor(
    eventDispatcher: RolesEventDispatcher,
    private _states: MeStateService,
    private _gateway: MeGateway,
    private _usersService: UsersService,
  ) {
    eventDispatcher.addListener(this);
  }

  async on(name: string, role: Role): Promise<void> {
    if (name === 'create') {
      return;
    }

    const filteredStates = this._states.usersStates.filter(
      (state) =>
        state.user.roles.find((item) => item.label === role.label) !==
        undefined,
    );

    for (const state of filteredStates) {
      try {
        const freshUser = await firstValueFrom(
          this._usersService.findOne(state.user._id.toString()),
        );

        this._gateway.emitUpdate(state.socket, freshUser);
        state.user = freshUser;
      } catch {}
    }
  }
}
