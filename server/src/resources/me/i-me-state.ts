import { Socket } from 'socket.io';
import { UserDocument } from '../users/schema';

export interface IMeState {
  user: UserDocument;
  socket: Socket;
}
