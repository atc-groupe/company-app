import { Module } from '@nestjs/common';
import { MeService } from './me.service';
import { MeController } from './me.controller';
import { UsersModule } from '../users/users.module';
import { MeGateway } from './me.gateway';
import { MeStateService } from './me-state.service';
import { EventDispatchersModule } from '../../shared/modules/event-dispatchers.module';
import { UserEventListener } from './user.event-listener';
import { RolesEventListener } from './roles.event-listener';

@Module({
  imports: [UsersModule, UsersModule, EventDispatchersModule],
  providers: [
    MeService,
    MeGateway,
    MeStateService,
    UserEventListener,
    RolesEventListener,
  ],
  controllers: [MeController],
})
export class MeModule {}
