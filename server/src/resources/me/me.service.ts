import {
  HttpException,
  Injectable,
  InternalServerErrorException,
  LoggerService,
  NotFoundException,
} from '@nestjs/common';
import { UserSettingsUpdateDto } from './user-settings-update.dto';
import { catchError, map, Observable } from 'rxjs';
import { User } from '../users/schema';
import { UserRepository } from '../users/user.repository';
import { LoggerFactory } from '../../shared/factories/logger.factory';
import { LogContextEnum } from '../../shared/enum';
import { Types } from 'mongoose';

@Injectable()
export class MeService {
  private _logger: LoggerService;

  constructor(
    loggerFactory: LoggerFactory,
    private _userRepository: UserRepository,
  ) {
    this._logger = loggerFactory.get(LogContextEnum.me);
  }

  public updateSettings(
    id: string,
    dto: UserSettingsUpdateDto,
  ): Observable<User> {
    return this._userRepository
      .updateOne(id, {
        settings: {
          ...dto,
          planningView: new Types.ObjectId(dto.planningView),
        },
      })
      .pipe(
        map((user) => {
          if (!user) {
            throw new NotFoundException(`L'utilisateur est introuvable`);
          }

          return user;
        }),
        catchError((err) => {
          let message = `Une erreur est survenue pendant la mise à jour de l'utilisateur. _id: ${id}`;

          if (err.message) {
            message = `${message}. Message: ${err.message}`;
          }

          this._logger.error(`[MeService] ${message}`);

          if (err instanceof HttpException) {
            throw err;
          }

          throw new InternalServerErrorException(message);
        }),
      );
  }
}
