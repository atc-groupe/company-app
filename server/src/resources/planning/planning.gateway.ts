import {
  ConnectedSocket,
  MessageBody,
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketGateway,
  WsException,
} from '@nestjs/websockets';
import { Socket } from 'socket.io';
import { PlanningDataService, PlanningStateService } from './services';
import { AppWsResponse, AppWsSuccessResponse } from '../../shared/classes';
import { AppWsInternalServerErrorException } from '../../shared/exceptions';
import { IPlanningCard, IPlanningCardRemove } from './interfaces';
import { PlanningDataQueryParamsDto } from './dto';
import { UseGuards } from '@nestjs/common';
import { AuthGatewayGuard } from '../auth/auth-gateway.guard';

@UseGuards(AuthGatewayGuard)
@WebSocketGateway({ transports: ['websocket'], namespace: 'planning' })
export class PlanningGateway implements OnGatewayDisconnect {
  constructor(
    private _stateService: PlanningStateService,
    private _dataService: PlanningDataService,
  ) {}
  handleDisconnect(client: Socket): void {
    this._stateService.removeSocket(client);
  }

  @SubscribeMessage('data.subscribe')
  public async onSubscribeData(
    @MessageBody() dto: PlanningDataQueryParamsDto,
    @ConnectedSocket() client: Socket,
  ): Promise<AppWsResponse | WsException> {
    try {
      const data = await this._dataService.getData(dto);
      this._stateService.pushState(dto, data, client);

      return new AppWsSuccessResponse<IPlanningCard[]>(data);
    } catch (err) {
      return new AppWsInternalServerErrorException(
        'Une erreur est survenue lors de la récupération des données du planning',
      );
    }
  }

  public emitDataUpdates(
    sockets: Socket[],
    cards: (IPlanningCard | IPlanningCardRemove)[],
  ) {
    sockets.forEach((socket) => {
      socket.emit('data.update', cards);
    });
  }
}
