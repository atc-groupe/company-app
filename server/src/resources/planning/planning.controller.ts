import { Controller, Get } from '@nestjs/common';
import { PlanningSetupService } from './services';
import { Observable } from 'rxjs';
import { TPlanningSetup } from './types';

@Controller('planning')
export class PlanningController {
  constructor(private _setupService: PlanningSetupService) {}

  @Get('setup')
  getSetup(): Observable<TPlanningSetup> {
    return this._setupService.getSetup();
  }
}
