import { Injectable } from '@nestjs/common';
import { PlanningGateway } from '../planning.gateway';
import { PlanningDataService, PlanningStateService } from '../services';
import { IEventListener } from '../../../shared/interfaces';
import { JobEventDispatcher } from '../../../shared/event-dispatchers';
import { JobDocument } from '../../jobs/schemas';
import { IPlanningCard, IPlanningStateDataItem } from '../interfaces';

@Injectable()
export class JobEventListener implements IEventListener {
  constructor(
    eventDispatcher: JobEventDispatcher,
    private _gateway: PlanningGateway,
    private _stateService: PlanningStateService,
    private _planningDataService: PlanningDataService,
  ) {
    eventDispatcher.addListener(this);
  }

  async on(name: string, job: JobDocument): Promise<void> {
    if (!this._stateService.states.length) {
      return;
    }

    for (const state of this._stateService.states) {
      const stateItems = state.data.filter(
        (item) => item.jobNumber === job.mp.number,
      );

      if (!stateItems.length) {
        continue;
      }

      const jobCards = await this._planningDataService.getJobCards(
        state.queryParams.department,
        state.queryParams.group,
        job,
      );

      if (!jobCards?.length) {
        continue;
      }

      const cardsToUpdate: IPlanningCard[] = [];
      const stateItemsToUpdate: IPlanningStateDataItem[] = [];
      stateItems.forEach((stateItem) => {
        const jobCard = jobCards.find(
          (item) => item.computedId === stateItem.cardComputedId,
        );

        if (jobCard) {
          jobCard.syncAction = 'update';
          cardsToUpdate.push(jobCard);

          stateItemsToUpdate.push(
            this._stateService.getMappedDataItem(jobCard),
          );
        }
      });

      if (cardsToUpdate.length) {
        this._gateway.emitDataUpdates(state.sockets, cardsToUpdate);
        state.data = state.data.map((item) => {
          const newItem = stateItemsToUpdate.find(
            (el) => el.cardComputedId === item.cardComputedId,
          );

          return newItem ?? item;
        });
      }
    }
  }
}
