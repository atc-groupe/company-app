import { MpPlanningDepartmentEnum } from '../../../mp/mp-planning/enum';
import { IsEnum, IsOptional, IsString } from 'class-validator';

export class PlanningDataQueryParamsDto {
  @IsEnum(MpPlanningDepartmentEnum)
  department: MpPlanningDepartmentEnum;

  @IsString()
  group: string;

  @IsString()
  startDate: string;

  @IsString()
  @IsOptional()
  endDate: string;

  @IsString()
  @IsOptional()
  term: string;
}
