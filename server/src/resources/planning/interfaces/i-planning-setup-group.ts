import { IPlanningSetupGroupMachine } from './i-planning-setup-group-machine';
import { MpPlanningDepartmentEnum } from '../../../mp/mp-planning/enum';

export interface IPlanningSetupGroup {
  department: MpPlanningDepartmentEnum;
  name: string;
  machines: IPlanningSetupGroupMachine[];
}
