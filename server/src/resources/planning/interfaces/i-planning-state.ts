import { Socket } from 'socket.io';
import { IPlanningStateDataItem } from './i-planning-state-data-item';
import { PlanningDataQueryParamsDto } from '../dto';

export interface IPlanningState {
  queryParams: PlanningDataQueryParamsDto;
  data: IPlanningStateDataItem[];
  sockets: Socket[];
}
