export interface IPlanningStateDataItem {
  cardComputedId: string;
  jobNumber: number;
  hash: string;
}
