export interface IPlanningCardRemove {
  computedId: string;
  syncAction?: 'add' | 'update' | 'remove';
}
