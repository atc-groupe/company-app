import { JobSync } from '../../jobs/schemas';
import { JobCommentsPlanning } from '../../jobs/schemas/job-comments-planning.schema';

export interface IPlanningCardJobInfo {
  _id: string;
  number: number;
  statusNumber: number;
  company: string;
  description: string;
  machines: string[];
  filesCheckStatusIndex: number;
  sendingDate: string | null;
  comment: JobCommentsPlanning | null;
  sync: JobSync;
}
