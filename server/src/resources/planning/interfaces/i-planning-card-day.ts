export interface IPlanningCardDay {
  date: Date;
  time: number;
  ordering: number;
}
