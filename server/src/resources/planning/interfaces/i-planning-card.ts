import { IPlanningCardJobInfo } from './i-planning-card-job-info';
import { IPlanningCardDay } from './i-planning-card-day';
import { IPlanningCardJobOperationUserInfo } from './i-planning-card-job-operation-user-info';

export interface IPlanningCard {
  id: number;
  computedId: string;
  startDate: Date;
  machineId: number;
  machineName: string;
  operation: string;
  operationUserInfo: IPlanningCardJobOperationUserInfo | null;
  operationTime: number | null;
  productionTime: number;
  calculatedTime: number;
  employee: string | null;
  isPlanned: boolean;
  completed: boolean;
  ordering: number;
  syncAction?: 'add' | 'update' | 'remove';
  jobInfo: IPlanningCardJobInfo;
  days: IPlanningCardDay[];
}
