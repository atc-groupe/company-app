import { IPlanningSetupGroupMachineOperation } from './i-planning-setup-group-machine-operation';

export interface IPlanningSetupGroupMachine {
  id: number;
  name: string;
  ordering: number;
  operations: IPlanningSetupGroupMachineOperation[];
}
