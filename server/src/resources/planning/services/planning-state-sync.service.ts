import { Injectable, LoggerService } from '@nestjs/common';
import { PlanningStateService } from './planning-state.service';
import { LoggerFactory } from '../../../shared/factories/logger.factory';
import { LogContextEnum } from '../../../shared/enum';
import { IPlanningCard, IPlanningCardRemove } from '../interfaces';
import { PlanningDataService } from './planning-data.service';
import { PlanningGateway } from '../planning.gateway';

@Injectable()
export class PlanningStateSyncService {
  private _logger: LoggerService;
  constructor(
    loggerFactory: LoggerFactory,
    private _stateService: PlanningStateService,
    private _dataService: PlanningDataService,
    private _gateway: PlanningGateway,
  ) {
    this._logger = loggerFactory.get(LogContextEnum.planning);
  }

  public async syncStates(): Promise<void> {
    if (!this._stateService.states.length) {
      return;
    }

    const socketIds = this._stateService.states.reduce((acc: string, state) => {
      state.sockets.forEach((socket) => {
        acc = acc === '' ? socket.id : `${acc}, ${socket.id}`;
      });

      return acc;
    }, '');

    this._logger.log(
      `[PlanningStateSyncService] sync plannings start. Nb of states: ${this._stateService.states.length}. Sockets: ${socketIds}`,
    );

    for (const state of this._stateService.states) {
      try {
        const cardsToUpdate: (IPlanningCard | IPlanningCardRemove)[] = [];
        const newData = await this._dataService.getData(state.queryParams);

        newData.forEach((newCard) => {
          const stateItemIndex = state.data.findIndex(
            (item) => item.cardComputedId === newCard.computedId,
          );
          const newDataItem = this._stateService.getMappedDataItem(newCard);

          if (stateItemIndex === -1) {
            newCard.syncAction = 'add';
            cardsToUpdate.push(newCard);
            state.data.push(newDataItem);

            return;
          } else if (newDataItem.hash !== state.data[stateItemIndex].hash) {
            newCard.syncAction = 'update';
            cardsToUpdate.push(newCard);
            state.data[stateItemIndex] = newDataItem;
          }
        });

        state.data = state.data.filter((stateItem) => {
          const newCard = newData.find(
            (item) => item.computedId === stateItem.cardComputedId,
          );

          if (!newCard) {
            cardsToUpdate.push({
              computedId: stateItem.cardComputedId,
              syncAction: 'remove',
            });

            return false;
          }

          return true;
        });

        if (cardsToUpdate.length) {
          this._gateway.emitDataUpdates(state.sockets, cardsToUpdate);
        }
      } catch (err) {
        const message =
          '[PlanningStateService] Une erreur est survenue lors de la synchronisation du planning';

        this._logger.error(
          err.message ? `${message}. Message: ${err.message}` : message,
        );
      }
    }
  }
}
