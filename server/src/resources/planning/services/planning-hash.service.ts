import { Injectable } from '@nestjs/common';
import { IPlanningCard } from '../interfaces';
import { createHash } from 'node:crypto';

@Injectable()
export class PlanningHashService {
  public getCardHash(card: IPlanningCard): string {
    const data = { ...card };
    delete data.syncAction;

    return createHash('sha256').update(JSON.stringify(data)).digest('hex');
  }
}
