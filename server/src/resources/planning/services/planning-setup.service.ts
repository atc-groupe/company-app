import {
  HttpException,
  Injectable,
  InternalServerErrorException,
  LoggerService,
} from '@nestjs/common';
import {
  IPlanningSetupGroup,
  IPlanningSetupGroupMachine,
  IPlanningSetupGroupMachineOperation,
  IPlanningSetupGroupMachineOperationEmployee,
} from '../interfaces';
import { catchError, map, Observable } from 'rxjs';
import { MpPlanningGetSetupRepository } from '../../../mp/mp-planning/mp-planning-get-setup.repository';
import {
  IMpPlanningSetupGroup,
  IMpPlanningSetupMachine,
  IMpPlanningSetupMachineOperation,
  IMpPlanningSetupMachineOperationEmployee,
} from '../../../mp/mp-planning/interfaces';
import { TPlanningSetup } from '../types';
import { MpPlanningDepartmentEnum } from '../../../mp/mp-planning/enum';
import { LoggerFactory } from '../../../shared/factories/logger.factory';
import { LogContextEnum } from '../../../shared/enum';

@Injectable()
export class PlanningSetupService {
  private _logger: LoggerService;
  constructor(
    loggerFactory: LoggerFactory,
    private _mpPlanningSetupRepository: MpPlanningGetSetupRepository,
  ) {
    this._logger = loggerFactory.get(LogContextEnum.planning);
  }

  getSetup(): Observable<TPlanningSetup> {
    return this._mpPlanningSetupRepository.getPlanningSetup().pipe(
      map((setup) =>
        setup.reduce((acc: TPlanningSetup, department) => {
          department.group.forEach((group) => {
            acc.push(this._getMappedGroup(department.department, group));
          });

          return acc;
        }, []),
      ),
      catchError((err) => {
        const message =
          'Impossible de récupérer le setup du planning Multipress';

        this._logger.error(
          err.message
            ? `[PlanningSetupService] ${message}. Message: ${err.message}`
            : `[PlanningSetupService] ${message}`,
        );
        if (err instanceof HttpException) {
          throw err;
        }

        throw new InternalServerErrorException(message);
      }),
    );
  }

  private _getMappedGroup(
    department: MpPlanningDepartmentEnum,
    mpGroup: IMpPlanningSetupGroup,
  ): IPlanningSetupGroup {
    return {
      department,
      name: mpGroup.name,
      machines: mpGroup.machine.map((machine) =>
        this._getMappedMachine(machine),
      ),
    };
  }

  private _getMappedMachine(
    mpMachine: IMpPlanningSetupMachine,
  ): IPlanningSetupGroupMachine {
    return {
      id: mpMachine.id,
      name: mpMachine.name,
      ordering: mpMachine.ordering,
      operations: mpMachine.operations.map((operation) =>
        this._getMappedOperations(operation),
      ),
    };
  }

  private _getMappedOperations(
    mpOperation: IMpPlanningSetupMachineOperation,
  ): IPlanningSetupGroupMachineOperation {
    return {
      name: mpOperation.name,
      employees: mpOperation.employees.map((employee) =>
        this._getMappedEmployee(employee),
      ),
    };
  }

  private _getMappedEmployee(
    mpEmployee: IMpPlanningSetupMachineOperationEmployee,
  ): IPlanningSetupGroupMachineOperationEmployee {
    return {
      name: mpEmployee.name,
      id: mpEmployee.id,
    };
  }
}
