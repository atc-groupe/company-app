import { Injectable } from '@nestjs/common';
import {
  IPlanningCard,
  IPlanningState,
  IPlanningStateDataItem,
} from '../interfaces';
import { Socket } from 'socket.io';
import { PlanningDataQueryParamsDto } from '../dto';
import { PlanningHashService } from './planning-hash.service';

@Injectable()
export class PlanningStateService {
  private _states: IPlanningState[] = [];

  constructor(private _hashService: PlanningHashService) {}

  public get states(): IPlanningState[] {
    return this._states;
  }

  public pushState(
    queryParams: PlanningDataQueryParamsDto,
    planningCards: IPlanningCard[],
    socket: Socket,
  ): void {
    this.removeSocket(socket);
    const state = this._getStateFromQueryParams(queryParams);

    if (state) {
      state.sockets.push(socket);
    } else {
      const data: IPlanningStateDataItem[] = planningCards.map(
        (card: IPlanningCard) => this.getMappedDataItem(card),
      );

      this._states.push({
        queryParams,
        data,
        sockets: [socket],
      });
    }
  }

  public removeSocket(socket: Socket): void {
    this._states = this._states.filter((state) => {
      state.sockets = state.sockets.filter((item) => item.id !== socket.id);

      return state.sockets.length > 0;
    });
  }

  public getMappedDataItem(card: IPlanningCard): IPlanningStateDataItem {
    return {
      cardComputedId: card.computedId,
      jobNumber: card.jobInfo.number,
      hash: this._hashService.getCardHash(card),
    };
  }

  private _getStateFromQueryParams(
    dto: PlanningDataQueryParamsDto,
  ): IPlanningState | null {
    const state = this._states.find(
      ({ queryParams }) => JSON.stringify(queryParams) === JSON.stringify(dto),
    );

    return state ? state : null;
  }
}
