import {
  HttpException,
  Injectable,
  InternalServerErrorException,
  LoggerService,
} from '@nestjs/common';
import { PlanningCardMappingService } from './planning-card-mapping.service';
import { LoggerFactory } from '../../../shared/factories/logger.factory';
import { MpPlanningGetLinesRepository } from '../../../mp/mp-planning/mp-planning-get-lines.repository';
import { LogContextEnum } from '../../../shared/enum';
import { JobsService } from '../../jobs/services';
import { JobSyncService } from '../../jobs/sync';
import { PlanningDataQueryParamsDto } from '../dto';
import { IPlanningCard } from '../interfaces';
import { IMpPlanningGetLinesDto } from '../../../mp/mp-planning/dto';
import { firstValueFrom } from 'rxjs';
import { JobDocument } from '../../jobs/schemas';

@Injectable()
export class PlanningDataService {
  private _logger: LoggerService;
  constructor(
    loggerFactory: LoggerFactory,
    private _mpPlanningLinesRepository: MpPlanningGetLinesRepository,
    private _jobsService: JobsService,
    private _jobSyncService: JobSyncService,
    private _cardMappingService: PlanningCardMappingService,
  ) {
    this._logger = loggerFactory.get(LogContextEnum.planning);
  }

  public async getData(
    dto: PlanningDataQueryParamsDto,
  ): Promise<IPlanningCard[]> {
    return dto.term ? this._getSearchData(dto) : this._getData(dto);
  }

  public async getJobCards(
    department: number,
    group: string,
    job: JobDocument,
  ): Promise<IPlanningCard[] | null> {
    try {
      const maxDates = this._getPlanningLinesSearchDates();
      const lines = await firstValueFrom(
        this._mpPlanningLinesRepository.getLines({
          startdate: maxDates.startDate,
          stopdate: maxDates.endDate,
          department,
          group,
        }),
      );

      const jobLines = lines.filter(
        (line) =>
          this._getJobNumberFromLineDescription(line.description) ===
          job.mp.number,
      );

      if (!jobLines.length) {
        return null;
      }

      const cards: IPlanningCard[] = [];
      for (const line of jobLines) {
        const mpCard = await firstValueFrom(
          this._mpPlanningLinesRepository.getCard(line.id),
        );

        if (!mpCard) {
          this._logger.warn(
            `[PlanningDataService] La carte de planning est introuvable. ID: ${line.id}`,
          );

          return null;
        }

        cards.push(
          this._cardMappingService.getMappedData(group, line, mpCard, job),
        );
      }

      return cards;
    } catch (err) {
      this._logger.warn(
        `[PlanningDataService] Une erreur est survenue pendant la synchronisation des cartes de planning du job ${job.mp.number}. Erreur: ${err.error.message}`,
      );

      return null;
    }
  }

  private async _getData(
    dto: PlanningDataQueryParamsDto,
  ): Promise<IPlanningCard[]> {
    const maxDates = this._getPlanningLinesSearchDates();
    const startDate = dto.startDate ? dto.startDate : maxDates.startDate;

    const mpDto: IMpPlanningGetLinesDto = {
      startdate: startDate,
      department: dto.department,
      group: dto.group,
    };

    if (dto.endDate) {
      mpDto.stopdate = dto.endDate;
    }

    if (!dto.startDate) {
      mpDto.stopdate = maxDates.endDate;
    }

    try {
      const lines = await firstValueFrom(
        this._mpPlanningLinesRepository.getLines(mpDto),
      );

      if (!lines.length) {
        return [];
      }

      const jobNumbers = lines.map((line) =>
        this._getJobNumberFromLineDescription(line.description),
      );

      const jobs = await firstValueFrom(
        this._jobsService.findByMpNumbers(jobNumbers),
      );

      const jobsMap = new Map<number, JobDocument>();
      const jobsToSync: number[] = [];

      for (const jobNumber of jobNumbers) {
        const job = jobs.find((item) => item.mp.number === jobNumber);

        job ? jobsMap.set(job.mp.number, job) : jobsToSync.push(jobNumber);
      }

      for (const jobNumber of jobsToSync) {
        try {
          const job = await firstValueFrom(
            this._jobSyncService.syncOneByNumber(jobNumber),
          );

          jobsMap.set(job.mp.number, job);
        } catch {
          this._logger.error(
            `[PlanningDataService] Impossible de synchroniser le job n°${jobNumber}`,
          );
        }
      }

      const filteredLines = lines.filter((line) => {
        const job = jobsMap.get(
          this._getJobNumberFromLineDescription(line.description),
        );

        return !!job && job.mp.statusNumber >= -1 && job.mp.statusNumber < 720;
      });

      const cards: IPlanningCard[] = [];
      for (const line of filteredLines) {
        const mpCard = await firstValueFrom(
          this._mpPlanningLinesRepository.getCard(line.id),
        );

        if (!mpCard) {
          this._logger.warn(
            `[PlanningDataService] Une carte de planning est introuvable. ID: ${line.id}`,
          );

          continue;
        }

        const job = jobsMap.get(mpCard.job.job_number);
        if (!job) {
          continue;
        }

        const card = this._cardMappingService.getMappedData(
          dto.group,
          line,
          mpCard,
          job,
        );

        cards.push(card);
      }

      return cards;
    } catch (err) {
      const message =
        'Une erreur est survenue lors de la récupération des données du planning';

      this._logger.error(
        err.message
          ? `[PlanningDataService] ${message}. Message: ${err.message}`
          : `[PlanningDataService] ${message}`,
      );

      if (err instanceof HttpException) {
        throw err;
      }

      throw new InternalServerErrorException(message);
    }
  }

  private async _getSearchData(
    dto: PlanningDataQueryParamsDto,
  ): Promise<IPlanningCard[]> {
    try {
      const jobs = await firstValueFrom(
        this._jobsService.find({ fullSearch: dto.term }),
      );

      const dates = this._getPlanningLinesSearchDates();
      const lines = await firstValueFrom(
        this._mpPlanningLinesRepository.getLines({
          startdate: dates.startDate,
          stopdate: dates.endDate,
          department: dto.department,
          group: dto.group,
        }),
      );

      const cards: IPlanningCard[] = [];
      for (const job of jobs) {
        const line = lines.find((item) =>
          item.description.startsWith(job.mp.numberSearch),
        );

        if (!line) {
          continue;
        }

        const mpCard = await firstValueFrom(
          this._mpPlanningLinesRepository.getCard(line.id),
        );

        cards.push(
          this._cardMappingService.getMappedData(dto.group, line, mpCard, job),
        );
      }

      return cards;
    } catch (err) {
      const message = 'Une erreur est survenue lors de la recherche';
      this._logger.error(
        err.message
          ? `[PlanningDataSearchService] ${message}. Message: ${err.message}`
          : `[PlanningDataSearchService] ${message}`,
      );

      throw new InternalServerErrorException(message);
    }
  }

  private _getPlanningLinesSearchDates(): {
    startDate: string;
    endDate: string;
  } {
    const startDate = new Date();
    const interval = 183;
    startDate.setDate(startDate.getDate() - interval);

    const endDate = new Date();
    endDate.setDate(endDate.getDate() + interval);

    return {
      startDate: startDate.toLocaleDateString('fr-FR'),
      endDate: endDate.toLocaleDateString('fr-FR'),
    };
  }

  private _getJobNumberFromLineDescription(description: string): number {
    return parseInt(description.split(' ')[0]);
  }
}
