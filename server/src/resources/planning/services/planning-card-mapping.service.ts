import { Injectable } from '@nestjs/common';
import {
  IMpPlanningCard,
  IMpPlanningLine,
} from '../../../mp/mp-planning/interfaces';
import { JobDocument, JobWorksheet } from '../../jobs/schemas';
import {
  IPlanningCard,
  IPlanningCardJobOperationUserInfo,
} from '../interfaces';

@Injectable()
export class PlanningCardMappingService {
  public getMappedData(
    group: string,
    line: IMpPlanningLine,
    card: IMpPlanningCard,
    job: JobDocument,
  ): IPlanningCard {
    const worksheets = job.worksheets?.filter(
      (item) => item.operation === card.job.operation,
    );
    const comment = job.comments.planning.find(
      (item) => item.planningGroup === group,
    );

    return {
      id: line.id,
      computedId: `${line.id}-${line.date}`,
      startDate: line.date,
      machineId: card.machine.id,
      machineName: card.machine.name,
      operation: card.job.operation,
      operationUserInfo: this._getOperationUserInfo(worksheets),
      operationTime: this._getTime(worksheets),
      productionTime: card.production_time,
      calculatedTime: card.calculated_time,
      employee: this._getEmployee(card.employee),
      isPlanned: !card.job.not_planned,
      completed: card.completed,
      ordering: line.ordering,
      jobInfo: {
        _id: job._id.toString(),
        number: job.mp.number,
        statusNumber: job.mp.statusNumber,
        company: job.mp.company,
        description: job.mp.description,
        machines: job.meta.productionTypes,
        filesCheckStatusIndex: job.prePress.filesCheckStatusIndex,
        sendingDate: job.mp.dates.sending,
        comment: comment ?? null,
        sync: job.sync,
      },
      days: card.days.map((item) => {
        return {
          date: item.date,
          time: item.time,
          ordering: item.ordering,
        };
      }),
    };
  }

  private _getOperationUserInfo(
    worksheets?: JobWorksheet[],
  ): IPlanningCardJobOperationUserInfo | null {
    if (!worksheets || !worksheets.length) {
      return null;
    }
    const lastWorksheet = worksheets[worksheets.length - 1];

    return {
      employee: lastWorksheet.employee,
      active: !lastWorksheet.totalTime,
    };
  }

  private _getEmployee(employee: string): string | null {
    if (employee === '-' || employee === '_ _') {
      return null;
    }

    return employee;
  }

  private _getTime(worksheets?: JobWorksheet[]): number | null {
    if (!worksheets || !worksheets.length) {
      return null;
    }

    const time = worksheets.reduce((acc: number, ws) => {
      acc += ws.totalTime;

      return acc;
    }, 0);

    return time > 0 ? time : null;
  }
}
