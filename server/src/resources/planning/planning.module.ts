import { Module } from '@nestjs/common';
import {
  PlanningSetupService,
  PlanningCardMappingService,
  PlanningDataService,
  PlanningStateService,
  PlanningHashService,
  PlanningStateSyncService,
} from './services';
import { PlanningController } from './planning.controller';
import { MpPlanningModule } from '../../mp/mp-planning/mp-planning.module';
import { JobsModule } from '../jobs/jobs.module';
import { PlanningSyncTask } from './tasks/planning-sync.task';
import { JobEventListener } from './event-listeners/job-event-listener';
import { EventDispatchersModule } from '../../shared/modules/event-dispatchers.module';
import { PlanningGateway } from './planning.gateway';

@Module({
  imports: [MpPlanningModule, JobsModule, EventDispatchersModule],
  providers: [
    PlanningCardMappingService,
    PlanningDataService,
    PlanningHashService,
    PlanningSetupService,
    PlanningStateService,
    PlanningStateSyncService,
    PlanningSyncTask,
    JobEventListener,
    PlanningGateway,
  ],
  controllers: [PlanningController],
})
export class PlanningModule {}
