import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { PlanningStateService, PlanningStateSyncService } from '../services';
import { ConfigService } from '@nestjs/config';
import { LoggerFactory } from '../../../shared/factories/logger.factory';
import { Logger } from '../../../shared/services';
import { LogContextEnum } from '../../../shared/enum';

@Injectable()
export class PlanningSyncTask {
  private _logger: Logger;
  private _disabled = false;
  constructor(
    _loggerFactory: LoggerFactory,
    private _stateSyncService: PlanningStateSyncService,
    private _stateService: PlanningStateService,
    private _configService: ConfigService,
  ) {
    this._logger = _loggerFactory.get(LogContextEnum.planning);
    const ALL_TASKS = this._configService.get('ALL_TASKS');
    const PLANNING_TASKS = this._configService.get('PLANNING_TASKS');

    if (ALL_TASKS === 'off' || PLANNING_TASKS === 'off') {
      this._disabled = true;
    }
  }

  @Cron('0 */1 6-20 * * *')
  public async syncCards(): Promise<void> {
    if (!this._stateService.states.length) {
      return;
    }

    if (this._disabled) {
      this._logger.log(
        `[PlanningSyncTask.syncCards] task skipped. See .env file`,
      );

      return;
    }

    await this._stateSyncService.syncStates();
  }
}
