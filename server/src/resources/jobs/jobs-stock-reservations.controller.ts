import {
  Body,
  Controller,
  Delete,
  Param,
  Patch,
  Post,
  Put,
  Req,
} from '@nestjs/common';
import { JobStockReservationsService } from './services';
import {
  JobStockReservationDto,
  JobStockReservationHandledDto,
  JobStockReservationStatusDto,
} from './dto';
import { Observable } from 'rxjs';
import { JobDocument } from './schemas';
import { JobStockReservationStatusEnum } from './enum';
import { IAppRequest } from '../../shared/interfaces';
import { Authorizations } from '../auth/authorization.decorator';
import { AuthActionsJobsEnum, AuthSubjectsEnum } from '../../shared/enum';

@Controller('jobs')
export class JobsStockReservationsController {
  constructor(private readonly _service: JobStockReservationsService) {}

  @Post(':jobNumber/stock-reservations')
  @Authorizations(
    {
      subject: AuthSubjectsEnum.Jobs,
      action: AuthActionsJobsEnum.CreateStockReservations,
    },
    {
      subject: AuthSubjectsEnum.Jobs,
      action: AuthActionsJobsEnum.ManageStockReservations,
    },
  )
  public createOne(
    @Param('jobNumber') jobNumber: number,
    @Body() dto: JobStockReservationDto,
    @Req() req: IAppRequest,
  ): Observable<JobDocument> {
    dto.createUsername = `${req.user.ad.firstName} ${req.user.ad.lastName}`;

    return this._service.createOne(jobNumber, dto);
  }

  @Patch(':jobNumber/stock-reservations/status')
  @Authorizations(
    {
      subject: AuthSubjectsEnum.Jobs,
      action: AuthActionsJobsEnum.CreateStockReservations,
    },
    {
      subject: AuthSubjectsEnum.Jobs,
      action: AuthActionsJobsEnum.HandleStockReservations,
    },
    {
      subject: AuthSubjectsEnum.Jobs,
      action: AuthActionsJobsEnum.ManageStockReservations,
    },
  )
  public changeAllStatuses(
    @Param('jobNumber') jobNumber: number,
    @Body() dto: JobStockReservationStatusDto,
    @Req() req: IAppRequest,
  ): Observable<JobDocument> {
    dto.updateUsername = `${req.user.ad.firstName} ${req.user.ad.lastName}`;

    return this._service.changeAllStatuses(jobNumber, dto);
  }

  @Put(':jobNumber/stock-reservations/:id')
  @Authorizations(
    {
      subject: AuthSubjectsEnum.Jobs,
      action: AuthActionsJobsEnum.CreateStockReservations,
    },
    {
      subject: AuthSubjectsEnum.Jobs,
      action: AuthActionsJobsEnum.ManageStockReservations,
    },
  )
  public updateOne(
    @Param('jobNumber') jobNumber: number,
    @Param('id') id: string,
    @Body() dto: JobStockReservationDto,
    @Req() req: IAppRequest,
  ): Observable<JobDocument> {
    dto.updateUsername = `${req.user.ad.firstName} ${req.user.ad.lastName}`;

    return this._service.updateOne(jobNumber, id, dto);
  }

  @Patch(':jobNumber/stock-reservations/:id/status')
  @Authorizations(
    {
      subject: AuthSubjectsEnum.Jobs,
      action: AuthActionsJobsEnum.CreateStockReservations,
    },
    {
      subject: AuthSubjectsEnum.Jobs,
      action: AuthActionsJobsEnum.HandleStockReservations,
    },
    {
      subject: AuthSubjectsEnum.Jobs,
      action: AuthActionsJobsEnum.ManageStockReservations,
    },
  )
  public changeStatus(
    @Param('jobNumber') jobNumber: number,
    @Param('id') id: string,
    @Body('status') status: JobStockReservationStatusEnum,
    @Req() req: IAppRequest,
  ): Observable<JobDocument> {
    return this._service.changeStatus(
      jobNumber,
      id,
      status,
      `${req.user.ad.firstName} ${req.user.ad.lastName}`,
    );
  }

  @Patch(':jobNumber/stock-reservations/:id/handle')
  @Authorizations(
    {
      subject: AuthSubjectsEnum.Jobs,
      action: AuthActionsJobsEnum.HandleStockReservations,
    },
    {
      subject: AuthSubjectsEnum.Jobs,
      action: AuthActionsJobsEnum.ManageStockReservations,
    },
  )
  public handleOne(
    @Param('jobNumber') jobNumber: number,
    @Param('id') id: string,
    @Body() dto: JobStockReservationHandledDto,
    @Req() req: IAppRequest,
  ): Observable<JobDocument> {
    dto.action = 'create';

    return this._service.handleOne(jobNumber, id, { ...dto, user: req.user });
  }

  @Patch(':jobNumber/stock-reservations/:id/cancel')
  @Authorizations(
    {
      subject: AuthSubjectsEnum.Jobs,
      action: AuthActionsJobsEnum.CreateStockReservations,
    },
    {
      subject: AuthSubjectsEnum.Jobs,
      action: AuthActionsJobsEnum.HandleStockReservations,
    },
    {
      subject: AuthSubjectsEnum.Jobs,
      action: AuthActionsJobsEnum.ManageStockReservations,
    },
  )
  public cancelOne(
    @Param('jobNumber') jobNumber: number,
    @Param('id') id: string,
    @Body() dto: JobStockReservationHandledDto,
    @Req() req: IAppRequest,
  ): Observable<JobDocument> {
    dto.action = 'cancel';

    return this._service.handleOne(jobNumber, id, { ...dto, user: req.user });
  }

  @Delete(':jobNumber/stock-reservations')
  @Authorizations(
    {
      subject: AuthSubjectsEnum.Jobs,
      action: AuthActionsJobsEnum.CreateStockReservations,
    },
    {
      subject: AuthSubjectsEnum.Jobs,
      action: AuthActionsJobsEnum.ManageStockReservations,
    },
  )
  public deleteAll(
    @Param('jobNumber') jobNumber: number,
  ): Observable<JobDocument> {
    return this._service.removeAll(jobNumber);
  }

  @Delete(':jobNumber/stock-reservations/:id')
  @Authorizations(
    {
      subject: AuthSubjectsEnum.Jobs,
      action: AuthActionsJobsEnum.CreateStockReservations,
    },
    {
      subject: AuthSubjectsEnum.Jobs,
      action: AuthActionsJobsEnum.ManageStockReservations,
    },
  )
  public deleteOne(
    @Param('jobNumber') jobNumber: number,
    @Param('id') id: string,
  ): Observable<JobDocument> {
    return this._service.removeOne(jobNumber, id);
  }
}
