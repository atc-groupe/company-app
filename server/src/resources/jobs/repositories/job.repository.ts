import { Injectable } from '@nestjs/common';
import { defer, filter, Observable, tap } from 'rxjs';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IJobCreateDto, IJobUpdateDto } from '../dto';
import { Job, JobDocument } from '../schemas';
import { JobEventDispatcher } from '../../../shared/event-dispatchers';
import { JOB_POPULATE } from './job-populate.constant';

@Injectable()
export class JobRepository {
  constructor(
    @InjectModel(Job.name) protected _jobModel: Model<Job>,
    private _eventDispatcher: JobEventDispatcher,
  ) {}

  public insertOne(dto: IJobCreateDto): Observable<JobDocument> {
    return defer(() => this._jobModel.create(dto)).pipe(
      tap((job) => this._eventDispatcher.dispatchEvent('create', job)),
    );
  }

  public findOneById(id: string): Observable<JobDocument | null> {
    return defer(() => this._jobModel.findById(id).exec());
  }

  public findOneByMpNumber(mpNumber: number): Observable<JobDocument | null> {
    return defer(() =>
      this._jobModel
        .findOne({ 'mp.number': mpNumber })
        .populate(JOB_POPULATE)
        .exec(),
    );
  }

  public updateOne(
    id: string,
    dto: IJobUpdateDto,
  ): Observable<JobDocument | null> {
    return defer(() =>
      this._jobModel
        .findByIdAndUpdate(
          id,
          {
            $set: {
              mp: dto.mp,
              subJobs: dto.subJobs,
              deliveryAddresses: dto.deliveryAddresses,
              subcontractors: dto.subcontractors,
              meta: dto.meta,
              operations: dto.operations,
              prePress: dto.prePress,
              sync: dto.sync,
              mappedStatus: dto.mappedStatus,
            },
          },
          { new: true, runValidators: true },
        )
        .populate(JOB_POPULATE)
        .exec(),
    ).pipe(
      filter((v) => v !== undefined && v !== null),
      tap((job) => this._eventDispatcher.dispatchEvent('update', job)),
    );
  }

  public updateStatus(
    mpNumber: number,
    statusNumber: number,
  ): Observable<JobDocument | null> {
    return defer(() =>
      this._jobModel
        .findOneAndUpdate(
          { 'mp.number': mpNumber },
          { 'mp.statusNumber': statusNumber },
          { new: true, runValidators: true },
        )
        .populate(JOB_POPULATE)
        .exec(),
    ).pipe(
      filter((v) => v !== undefined && v !== null),
      tap((job) => this._eventDispatcher.dispatchEvent('update', job)),
    );
  }

  public findSyncError(): Promise<JobDocument[]> {
    return this._jobModel.find({ 'sync.status': 'error' }).exec();
  }

  public findActives(): Promise<JobDocument[]> {
    return this._jobModel.find({ 'mp.statusNumber': { $lt: 1000 } }).exec();
  }

  public removeOneByMpId(mpId: number): Observable<JobDocument | null> {
    return defer(() =>
      this._jobModel.findOneAndRemove({ 'mp.id': mpId }).exec(),
    ).pipe(tap((job) => this._eventDispatcher.dispatchEvent('delete', job)));
  }
}
