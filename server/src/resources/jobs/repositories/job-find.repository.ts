import { Injectable } from '@nestjs/common';
import { defer, Observable } from 'rxjs';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { JobsQueryParamDto } from '../dto';
import { Job, JobDocument } from '../schemas';
import { JobStockReservationStatusEnum } from '../enum';

@Injectable()
export class JobFindRepository {
  constructor(@InjectModel(Job.name) protected _jobModel: Model<Job>) {}

  public find(queryParams: JobsQueryParamDto): Observable<JobDocument[]> {
    return defer(() =>
      this._jobModel
        .find(this._getQueryFilters(queryParams))
        .sort({ 'mp.number': 1 })
        .exec(),
    );
  }

  public findByNumbers(numbers: number[]): Observable<JobDocument[]> {
    return defer(() =>
      this._jobModel.find({ 'mp.number': { $in: numbers } }).exec(),
    );
  }

  private _getQueryFilters(queryParams: JobsQueryParamDto) {
    if (Object.keys(queryParams).length === 0) {
      return {};
    }

    if (queryParams.jobEndNumber) {
      return {
        'mp.number': {
          $regex: new RegExp(`^[0-9]*${queryParams.jobEndNumber}$`),
        },
      };
    }

    if (queryParams.fullSearch) {
      const regexp = new RegExp(queryParams.fullSearch, 'i');

      return {
        $or: [
          { 'mp.numberSearch': { $regex: regexp } },
          { 'mp.company': { $regex: regexp } },
          { 'mp.description': { $regex: regexp } },
        ],
      };
    }

    const searchParams: any[] = [];

    /*
      status number filters:
      - if "status_number" is set, other status number filters are ignored
      - then if "status_number_list" is set "start_status_number" and "end_status_number" are ignored
      - then "start_status_number" and "end_status_number" are used.
     */
    if (queryParams.statusNumber) {
      searchParams.push({
        'mp.statusNumber': queryParams.statusNumber,
      });
    } else if (queryParams.statusNumberList) {
      searchParams.push({
        'mp.statusNumber': { $in: queryParams.statusNumberList },
      });
    } else {
      if (queryParams.startStatusNumber) {
        searchParams.push({
          'mp.statusNumber': {
            $gte: queryParams.startStatusNumber,
          },
        });
      }

      if (queryParams.endStatusNumber) {
        searchParams.push({
          'mp.statusNumber': {
            $lte: queryParams.endStatusNumber,
          },
        });
      }
    }

    if (queryParams.sendingDate) {
      searchParams.push({
        $or: [
          { 'mp.dates.sending': { $eq: queryParams.sendingDate } },
          {
            'meta.deliverySendingDates': {
              $eq: queryParams.sendingDate,
              $exists: true,
              $ne: [],
            },
          },
        ],
      });
    } else {
      if (queryParams.startSendingDate) {
        searchParams.push({
          $or: [
            { 'mp.dates.sending': { $gte: queryParams.startSendingDate } },
            {
              'meta.deliverySendingDates': {
                $gte: queryParams.startSendingDate,
                $exists: true,
                $ne: [],
              },
            },
          ],
        });
      }

      if (queryParams.endSendingDate) {
        searchParams.push({
          $or: [
            { 'mp.dates.sending': { $lte: queryParams.endSendingDate } },
            {
              'meta.deliverySendingDates': {
                $lte: queryParams.endSendingDate,
                $exists: true,
                $ne: [],
              },
            },
          ],
        });
      }

      if (queryParams.withStockReservation) {
        searchParams.push({
          $and: [
            { stockReservations: { $ne: null } },
            { stockReservations: { $ne: [] } },
            {
              'stockReservations.status': {
                $ne: JobStockReservationStatusEnum.Brouillon,
              },
            },
          ],
        });
      }
    }

    return { $and: searchParams };
  }
}
