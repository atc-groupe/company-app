import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Job, JobDocument, JobWorksheet } from '../schemas';
import { Model } from 'mongoose';
import { JobEventDispatcher } from '../../../shared/event-dispatchers';
import { defer, Observable, tap } from 'rxjs';
import { JOB_POPULATE } from './job-populate.constant';

@Injectable()
export class JobWorksheetsRepository {
  constructor(
    @InjectModel(Job.name) protected _jobModel: Model<Job>,
    private _eventDispatcher: JobEventDispatcher,
  ) {}

  public addOne(
    jobId: string,
    dto: JobWorksheet,
  ): Observable<JobDocument | null> {
    return defer(() =>
      this._jobModel
        .findOneAndUpdate(
          { _id: jobId },
          { $push: { worksheets: dto } },
          { runValidators: true, new: true },
        )
        .populate(JOB_POPULATE)
        .exec(),
    ).pipe(
      tap((job) => {
        if (job) {
          this._eventDispatcher.dispatchEvent('update.worksheet', job);
        }
      }),
    );
  }

  public updateOne(
    jobId: string,
    worksheetId: string,
    dto: JobWorksheet,
  ): Observable<JobDocument | null> {
    return defer(() =>
      this._jobModel
        .findOneAndUpdate(
          {
            _id: jobId,
            'worksheets.id': worksheetId,
          },
          { $set: { 'worksheets.$': dto } },
          {
            runValidators: true,
            new: true,
          },
        )
        .populate(JOB_POPULATE)
        .exec(),
    ).pipe(
      tap((job) => {
        if (job) {
          this._eventDispatcher.dispatchEvent('update.worksheet', job);
        }
      }),
    );
  }

  public removeOne(
    jobId: string,
    worksheetId: string,
  ): Observable<JobDocument | null> {
    return defer(() =>
      this._jobModel
        .findOneAndUpdate(
          { _id: jobId },
          { $pull: { worksheets: { id: worksheetId } } },
          { runValidators: true, new: true },
        )
        .populate(JOB_POPULATE)
        .exec(),
    ).pipe(
      tap((job) => {
        if (job) {
          this._eventDispatcher.dispatchEvent('update.worksheet', job);
        }
      }),
    );
  }
}
