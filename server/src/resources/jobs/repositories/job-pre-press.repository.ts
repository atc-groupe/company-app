import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Job, JobDocument } from '../schemas';
import { Model } from 'mongoose';
import { JobEventDispatcher } from '../../../shared/event-dispatchers';
import { JobPrePressUpdateDto } from '../dto';
import { defer, filter, Observable, tap } from 'rxjs';
import { JOB_POPULATE } from './job-populate.constant';

@Injectable()
export class JobPrePressRepository {
  constructor(
    @InjectModel(Job.name) protected _jobModel: Model<Job>,
    private _eventDispatcher: JobEventDispatcher,
  ) {}

  public updatePrePress(
    id: string,
    dto: JobPrePressUpdateDto,
  ): Observable<JobDocument | null> {
    return defer(() =>
      this._jobModel
        .findByIdAndUpdate(
          id,
          {
            $set: {
              'prePress.note': dto.note,
              'prePress.designNote': dto.designNote,
              'prePress.graphicDesignNote': dto.graphicDesignNote,
              'prePress.filesCheck': dto.filesCheck,
              'prePress.filesCheckStatusIndex': dto.filesCheckStatusIndex,
            },
          },
          { new: true, runValidators: true },
        )
        .populate(JOB_POPULATE)
        .exec(),
    ).pipe(
      filter((v) => v !== undefined && v !== null),
      tap((job) =>
        this._eventDispatcher.dispatchEvent('update.pre-press', job),
      ),
    );
  }
}
