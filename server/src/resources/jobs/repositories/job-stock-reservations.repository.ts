import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Job, JobDocument } from '../schemas';
import { Model } from 'mongoose';
import { JobEventDispatcher } from '../../../shared/event-dispatchers';
import {
  JobStockReservationDto,
  JobStockReservationHandledDto,
  JobStockReservationStatusDto,
} from '../dto';
import { defer, Observable, tap } from 'rxjs';
import { JobStockReservationStatusEnum } from '../enum';
import { JOB_POPULATE } from './job-populate.constant';

@Injectable()
export class JobStockReservationsRepository {
  constructor(
    @InjectModel(Job.name) protected _jobModel: Model<Job>,
    private _eventDispatcher: JobEventDispatcher,
  ) {}

  public createOne(
    jobNumber: number,
    dto: JobStockReservationDto,
  ): Observable<JobDocument | null> {
    return defer(() =>
      this._jobModel
        .findOneAndUpdate(
          { 'mp.number': jobNumber },
          {
            $push: {
              stockReservations: dto,
            },
          },
          { new: true, runValidators: true },
        )
        .populate(JOB_POPULATE)
        .exec(),
    ).pipe(
      tap((job) => {
        if (job) {
          this._eventDispatcher.dispatchEvent('create.stock-reservation', job);
        }
      }),
    );
  }

  public updateOne(
    jobNumber: number,
    reservationId: string,
    dto: JobStockReservationDto,
  ): Observable<JobDocument | null> {
    return defer(() =>
      this._jobModel
        .findOneAndUpdate(
          { 'mp.number': jobNumber, 'stockReservations._id': reservationId },
          {
            $set: { 'stockReservations.$': { ...dto, updatedAt: new Date() } },
          },
          { new: true, runValidators: true },
        )
        .populate(JOB_POPULATE)
        .exec(),
    ).pipe(
      tap((job) => {
        if (job) {
          this._eventDispatcher.dispatchEvent('update.stock-reservation', job);
        }
      }),
    );
  }

  public changeStatus(
    jobNumber: number,
    reservationId: string,
    status: JobStockReservationStatusEnum,
    updateUsername: string,
  ): Observable<JobDocument | null> {
    return defer(() =>
      this._jobModel
        .findOneAndUpdate(
          { 'mp.number': jobNumber, 'stockReservations._id': reservationId },
          {
            $set: {
              'stockReservations.$.status': status,
              'stockReservations.$.updateUsername': updateUsername,
              'stockReservations.$.updatedAt': new Date(),
            },
          },
          { new: true, runValidators: true },
        )
        .populate(JOB_POPULATE)
        .exec(),
    ).pipe(
      tap((job) => {
        if (job) {
          this._eventDispatcher.dispatchEvent('update.stock-reservation', job);
        }
      }),
    );
  }

  public handleOne(
    jobNumber: number,
    reservationId: string,
    dto: JobStockReservationHandledDto,
  ): Observable<JobDocument | null> {
    const status =
      dto.action === 'create'
        ? JobStockReservationStatusEnum.Traitee
        : JobStockReservationStatusEnum.ATraiter;
    const handleUsername =
      dto.action === 'create'
        ? `${dto.user.ad.firstName} ${dto.user.ad.lastName}`
        : null;
    const subtractQuantity = dto.action === 'create' ? dto.quantity : null;
    const mutationDate = dto.action === 'create' ? new Date() : null;

    return defer(() =>
      this._jobModel
        .findOneAndUpdate(
          { 'mp.number': jobNumber, 'stockReservations._id': reservationId },
          {
            $set: {
              'stockReservations.$.paper': dto.paperId,
              'stockReservations.$.status': status,
              'stockReservations.$.handleUsername': handleUsername,
              'stockReservations.$.subtractQuantity': subtractQuantity,
              'stockReservations.$.mutationDate': mutationDate,
            },
          },
          { new: true, runValidators: true },
        )
        .populate(JOB_POPULATE)
        .exec(),
    ).pipe(
      tap((job) => {
        if (job) {
          this._eventDispatcher.dispatchEvent('update.stock-reservation', job);
        }
      }),
    );
  }

  public changeAllStatuses(
    jobNumber: number,
    dto: JobStockReservationStatusDto,
  ): Observable<JobDocument | null> {
    console.log(dto);
    return defer(() =>
      this._jobModel
        .findOneAndUpdate(
          { 'mp.number': jobNumber },
          {
            $set: {
              'stockReservations.$[elem].status': dto.to,
              'stockReservations.$[elem].updateUsername': dto.updateUsername,
              'stockReservations.$[elem].updatedAt': new Date(),
            },
          },
          {
            arrayFilters: [{ 'elem.status': { $eq: dto.from } }],
            new: true,
            runValidators: true,
          },
        )
        .populate(JOB_POPULATE)
        .exec(),
    ).pipe(
      tap((job) => {
        if (job) {
          this._eventDispatcher.dispatchEvent('update.stock-reservation', job);
        }
      }),
    );
  }

  public removeOne(
    jobNumber: number,
    reservationId: string,
  ): Observable<JobDocument | null> {
    return defer(() =>
      this._jobModel
        .findOneAndUpdate(
          { 'mp.number': jobNumber },
          {
            $pull: { stockReservations: { _id: reservationId } },
          },
          { new: true, runValidators: true },
        )
        .populate(JOB_POPULATE)
        .exec(),
    ).pipe(
      tap((job) => {
        if (job) {
          this._eventDispatcher.dispatchEvent('delete.stock-reservation', job);
        }
      }),
    );
  }

  public removeAll(jobNumber: number): Observable<JobDocument | null> {
    return defer(() =>
      this._jobModel.findOneAndUpdate(
        { 'mp.number': jobNumber },
        {
          $set: { stockReservations: [] },
        },
        { new: true, runValidators: true },
      ),
    ).pipe(
      tap((job) => {
        if (job) {
          this._eventDispatcher.dispatchEvent('delete.stock-reservation', job);
        }
      }),
    );
  }
}
