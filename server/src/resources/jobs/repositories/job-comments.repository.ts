import { Injectable } from '@nestjs/common';
import { defer, Observable, tap } from 'rxjs';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IJobPlanningCommentCreateDto } from '../dto';
import { Job, JobDocument } from '../schemas';
import { JobEventDispatcher } from '../../../shared/event-dispatchers';
import { JOB_POPULATE } from './job-populate.constant';

@Injectable()
export class JobCommentsRepository {
  constructor(
    @InjectModel(Job.name) protected _jobModel: Model<Job>,
    private _eventDispatcher: JobEventDispatcher,
  ) {}

  public updateDelivery(
    id: string,
    comment: string | null,
  ): Observable<JobDocument | null> {
    return defer(() =>
      this._jobModel
        .findByIdAndUpdate(
          id,
          { 'comments.delivery': comment },
          { new: true, runValidators: true },
        )
        .populate(JOB_POPULATE)
        .exec(),
    ).pipe(
      tap((job) =>
        this._eventDispatcher.dispatchEvent('update:comment.delivery', job),
      ),
    );
  }

  addPlanningComment(
    id: string,
    dto: IJobPlanningCommentCreateDto,
  ): Observable<JobDocument | null> {
    return defer(() =>
      this._jobModel
        .findByIdAndUpdate(
          id,
          { $addToSet: { 'comments.planning': dto } },
          { new: true, runValidators: true },
        )
        .populate(JOB_POPULATE)
        .exec(),
    ).pipe(
      tap((job) =>
        this._eventDispatcher.dispatchEvent('create:comment.planning', job),
      ),
    );
  }

  updatePlanningComment(
    id: string,
    commentId: string,
    comment: string,
  ): Observable<JobDocument | null> {
    return defer(() =>
      this._jobModel
        .findOneAndUpdate(
          { _id: id, 'comments.planning._id': commentId },
          {
            $set: {
              'comments.planning.$.comment': comment,
              'comments.planning.$.updatedAt': new Date(),
            },
          },
          {
            new: true,
            runValidators: true,
            timestamps: { createdAt: false },
          },
        )
        .populate(JOB_POPULATE)
        .exec(),
    ).pipe(
      tap((job) =>
        this._eventDispatcher.dispatchEvent('update:comment.planning', job),
      ),
    );
  }

  removePlanningComment(
    id: string,
    commentId: string,
  ): Observable<JobDocument | null> {
    return defer(() =>
      this._jobModel
        .findByIdAndUpdate(
          id,
          { $pull: { 'comments.planning': { _id: commentId } } },
          { new: true, runValidators: true },
        )
        .populate(JOB_POPULATE)
        .exec(),
    ).pipe(
      tap((job) =>
        this._eventDispatcher.dispatchEvent('remove:comment.planning', job),
      ),
    );
  }
}
