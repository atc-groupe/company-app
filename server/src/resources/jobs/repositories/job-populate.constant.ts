export const JOB_POPULATE = {
  path: 'stockReservations',
  populate: [
    { path: 'paperGenericName', model: 'ProductGenericName' },
    { path: 'paper', model: 'Paper' },
  ],
};
