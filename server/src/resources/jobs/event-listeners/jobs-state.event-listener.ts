import { Injectable } from '@nestjs/common';
import { IEventListener } from '../../../shared/interfaces';
import { JobEventDispatcher } from '../../../shared/event-dispatchers';
import {
  JobPlanningDataService,
  JobsHashService,
  JobsStateService,
} from '../services';
import { JobsGateway } from '../jobs.gateway';
import { JobDocument } from '../schemas';

@Injectable()
export class JobsStateEventListener implements IEventListener {
  constructor(
    eventDispatcher: JobEventDispatcher,
    private _statesService: JobsStateService,
    private _gateway: JobsGateway,
    private _planningDataService: JobPlanningDataService,
    private _hashService: JobsHashService,
  ) {
    eventDispatcher.addListener(this);
  }

  async on(name: string, job: JobDocument): Promise<void> {
    if (this._statesService.isEmpty) {
      return;
    }

    try {
      const state = this._statesService.states.find(
        (state) => state.number === job.mp.number,
      );

      if (state) {
        if (name === 'update.worksheet') {
          job.planningData = await this._planningDataService.getJobCards(job);
          state.planningData = job.planningData;
          state.planningDataHash = this._hashService.getPlanningDataHash(
            job.planningData,
          );
        } else {
          job.planningData = state.planningData;
        }
        this._gateway.emitJobUpdate(state.sockets, job);
      }
    } catch {}
  }
}
