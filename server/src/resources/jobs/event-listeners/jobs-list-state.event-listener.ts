import { Injectable } from '@nestjs/common';
import { JobEventDispatcher } from '../../../shared/event-dispatchers';
import { JobsGateway } from '../jobs.gateway';
import { JobsListStateService } from '../services/jobs-list-state.service';
import { IEventListener } from '../../../shared/interfaces';
import { JobDocument } from '../schemas';
import { JobLineMappingService } from '../mapping/job-line-mapping.service';

@Injectable()
export class JobsListStateEventListener implements IEventListener {
  constructor(
    eventDispatcher: JobEventDispatcher,
    private _listStates: JobsListStateService,
    private _lineMapping: JobLineMappingService,
    private _gateway: JobsGateway,
  ) {
    eventDispatcher.addListener(this);
  }

  async on(name: string, job: JobDocument): Promise<void> {
    if (this._listStates.isEmpty) {
      return;
    }

    const newLine = this._lineMapping.getMappedData(job);
    const newItem = this._listStates.getMappedDataItem(newLine);

    for (const state of this._listStates.states) {
      const stateItemIndex = state.data.findIndex(
        (item) => item.number === newLine.number,
      );

      if (stateItemIndex === -1) {
        newLine.syncAction = 'add';
        this._gateway.emitListDataChanges(state.sockets, newLine);
        state.data.push(newItem);
      } else {
        newLine.syncAction = 'update';
        this._gateway.emitListDataChanges(state.sockets, newLine);
        state.data[stateItemIndex] = newItem;
      }
    }
  }
}
