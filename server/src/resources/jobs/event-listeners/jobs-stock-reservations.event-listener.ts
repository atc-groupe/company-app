import { Injectable } from '@nestjs/common';
import {
  StockReservationsDataService,
  StockReservationsStateService,
} from '../services';
import { StockReservationsGateway } from '../stock-reservations.gateway';
import { IEventListener } from '../../../shared/interfaces';
import { JobEventDispatcher } from '../../../shared/event-dispatchers';
import { JobDocument } from '../schemas';
import { StockReservationsLineMappingService } from '../mapping';

@Injectable()
export class JobsStockReservationsEventListener implements IEventListener {
  constructor(
    eventDispatcher: JobEventDispatcher,
    private _stateService: StockReservationsStateService,
    private _mappingService: StockReservationsLineMappingService,
    private _dataService: StockReservationsDataService,
    private _gateway: StockReservationsGateway,
  ) {
    eventDispatcher.addListener(this);
  }

  async on(name: string, job: JobDocument): Promise<void> {
    if (this._stateService.hasNoSocket) {
      return;
    }

    const newLines = await this._dataService.getReservationsLines();

    if (this._stateService.hasNoData && !newLines.length) {
      return;
    }

    const newLine = this._mappingService.getMappedData(job);
    const newStateItem = this._stateService.getMappedDataItem(newLine);

    const isInNewLines =
      newLines.findIndex((item) => item.jobNumber === newLine.jobNumber) !== -1;
    const stateItemIndex = this._stateService.state.data.findIndex(
      (item) => item.jobNumber === newLine.jobNumber,
    );
    const isInState = stateItemIndex !== -1;
    let hasToEmitUpdate = false;

    if (isInNewLines && !isInState) {
      newLine.syncAction = 'add';
      hasToEmitUpdate = true;
      this._stateService.state.data.push(newStateItem);
    }

    if (!isInNewLines && isInState) {
      newLine.syncAction = 'remove';
      hasToEmitUpdate = true;
      this._stateService.state.data = this._stateService.state.data.splice(
        stateItemIndex,
        1,
      );
    }

    if (
      isInNewLines &&
      isInState &&
      newStateItem.hash !== this._stateService.state.data[stateItemIndex].hash
    ) {
      newLine.syncAction = 'update';
      hasToEmitUpdate = true;
      this._stateService.state.data[stateItemIndex] = newStateItem;
    }

    if (hasToEmitUpdate) {
      this._gateway.emitLineUpdate(this._stateService.state.sockets, newLine);
    }
  }
}
