import {
  ConnectedSocket,
  MessageBody,
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketGateway,
  WsException,
} from '@nestjs/websockets';
import {
  JobsService,
  JobsListStateService,
  JobsStateService,
  JobPlanningDataService,
} from './services';
import { JobsQueryParamDto } from './dto';
import { Socket } from 'socket.io';
import { AppWsResponse, AppWsSuccessResponse } from '../../shared/classes';
import { AppWsInternalServerErrorException } from '../../shared/exceptions';
import { firstValueFrom } from 'rxjs';
import { JobLineMappingService } from './mapping/job-line-mapping.service';
import { IJobLine } from './interfaces';
import { JobSyncService } from './sync';
import { JobDocument } from './schemas';
import { UseGuards } from '@nestjs/common';
import { AuthGatewayGuard } from '../auth/auth-gateway.guard';

@UseGuards(AuthGatewayGuard)
@WebSocketGateway({
  transports: ['websocket'],
  namespace: 'jobs',
})
export class JobsGateway implements OnGatewayDisconnect {
  constructor(
    private _jobsService: JobsService,
    private _jobSyncService: JobSyncService,
    private _listStateService: JobsListStateService,
    private _jobsStateService: JobsStateService,
    private _lineMappingService: JobLineMappingService,
    private _jobPlanningDataService: JobPlanningDataService,
  ) {}

  handleDisconnect(client: any): any {
    this._listStateService.removeSocket(client.id);
    this._jobsStateService.removeSocket(client.id);
  }

  @SubscribeMessage('list.subscribe')
  public async onSubscribeData(
    @MessageBody() dto: JobsQueryParamDto,
    @ConnectedSocket() client: Socket,
  ): Promise<AppWsResponse<IJobLine[]> | WsException> {
    try {
      const jobs = await firstValueFrom(this._jobsService.find(dto));
      const lines = jobs.map((job) =>
        this._lineMappingService.getMappedData(job),
      );
      this._listStateService.pushState(dto, lines, client);

      return new AppWsSuccessResponse<IJobLine[]>(lines);
    } catch (err) {
      return new AppWsInternalServerErrorException(
        'Une erreur est survenue lors de la récupération de la liste des jobs',
      );
    }
  }

  public emitListDataChanges(sockets: Socket[], line: IJobLine) {
    sockets.forEach((socket) => {
      socket.emit('list.update', line);
    });
  }

  @SubscribeMessage('job.subscribe')
  public async onSubscribeJob(
    @MessageBody('mpNumber') mpNumber: number,
    @ConnectedSocket() client: Socket,
  ): Promise<AppWsSuccessResponse<JobDocument> | WsException> {
    try {
      const job = await firstValueFrom(
        this._jobSyncService.syncOneByNumber(mpNumber),
      );
      const planningData = await this._jobPlanningDataService.getJobCards(job);
      job.planningData = planningData;

      this._jobsStateService.pushState(mpNumber, planningData, client);
      return new AppWsSuccessResponse<JobDocument>(job);
    } catch (err) {
      return new AppWsInternalServerErrorException(
        `Une erreur est survenue lors de la récupération du job n°${mpNumber}`,
      );
    }
  }

  public emitJobUpdate(sockets: Socket[], job: JobDocument): void {
    sockets.forEach((socket) => {
      socket.emit('job.update', job);
    });
  }
}
