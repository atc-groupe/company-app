import { Injectable } from '@nestjs/common';
import { JobsService } from '../services';
import {
  MpDeliveryAddressRepository,
  MpJobsQuotationRepository,
} from '../../../mp/mp-jobs/repositories';
import { DeliveryAddressMappingService } from '../mapping';
import { LoggerFactory } from '../../../shared/factories/logger.factory';
import { LogContextEnum } from '../../../shared/enum';
import { firstValueFrom } from 'rxjs';
import { IMpDeliveryAddress } from '../../../mp/mp-jobs/interfaces';
import { JobSyncOneService } from './job-sync-one.service';

@Injectable()
export class JobSyncByDeliveryAddressesService {
  private _logger;
  constructor(
    loggerFactory: LoggerFactory,
    private _jobsService: JobsService,
    private _jobSyncOneService: JobSyncOneService,
    private _mpJobsRepository: MpJobsQuotationRepository,
    private _mpAddressesRepository: MpDeliveryAddressRepository,
    private _addressesMapping: DeliveryAddressMappingService,
  ) {
    this._logger = loggerFactory.get(LogContextEnum.jobSync);
  }

  public async syncByDeliveryAddresses(mpNumber: number): Promise<void> {
    try {
      const job = await firstValueFrom(
        this._jobsService.findOneByMpNumber(mpNumber),
      );
      const mpJob = await firstValueFrom(
        this._mpJobsRepository.findOneById(job.mp.id),
      );

      if (!mpJob) {
        return;
      }

      const mpAddressesPromises: Promise<IMpDeliveryAddress | null>[] =
        mpJob.delivery_address.map((address) =>
          firstValueFrom(this._mpAddressesRepository.findOneById(address.id)),
        );

      if (!mpAddressesPromises.length) {
        return;
      }

      const mpAddresses = await Promise.all(mpAddressesPromises);

      const freshAddresses =
        this._addressesMapping.getMappedCollection(mpAddresses);

      if (
        JSON.stringify(freshAddresses) !== JSON.stringify(job.deliveryAddresses)
      ) {
        await firstValueFrom(this._jobSyncOneService.syncOne(job.mp.id));
        this._logger.log(
          `Les adresses du job ${job.mp.number} ont été mises à jour`,
        );
      }
    } catch (err) {
      let message = `Une erreur est survenue lors de la resynchronisation des adresses de livraison du job ${mpNumber}.`;
      if (err?.message) {
        message = `${message}. Message: ${err.message}`;
      }

      this._logger.error(`[JobReSyncService] ${message}`);
    }
  }
}
