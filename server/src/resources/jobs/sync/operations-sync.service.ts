import { Injectable } from '@nestjs/common';
import { LoggerFactory } from '../../../shared/factories/logger.factory';
import { Logger } from '../../../shared/services';
import { LogContextEnum } from '../../../shared/enum';
import { map, Observable, of } from 'rxjs';
import { JobOperation } from '../schemas';
import { JobDocument } from '../schemas';
import { MpPlanningPushJobRepository } from '../../../mp/mp-planning/mp-planning-push-job.repository';
import { JobOperationMappingService } from '../mapping';

@Injectable()
export class OperationsSyncService {
  private _logger: Logger;
  constructor(
    loggerFactory: LoggerFactory,
    private _mpPlanningPushJobRepository: MpPlanningPushJobRepository,
    private _operationMapping: JobOperationMappingService,
  ) {
    this._logger = loggerFactory.get(LogContextEnum.jobSync);
  }

  public getSyncOperations(
    job: JobDocument | null,
  ): Observable<JobOperation[] | null> {
    if (!job) {
      return of(null);
    }

    if (job.mp.statusNumber === 1000) {
      return of(job.operations);
    }

    return this._mpPlanningPushJobRepository
      .getJobPlanOperations(job.mp.number)
      .pipe(
        map((mpOperations) =>
          this._operationMapping.getMappedCollection(mpOperations),
        ),
      );
  }
}
