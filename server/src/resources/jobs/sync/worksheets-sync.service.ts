import {
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { MpEmployeeWorksheetRepository } from '../../../mp/mp-employees/mp-employee-worksheet.repository';
import { JobWorksheetMappingService } from '../mapping';
import { JobsService } from '../services';
import { firstValueFrom } from 'rxjs';
import { JobWorksheetsRepository } from '../repositories';
import { Logger } from '../../../shared/services';
import { LoggerFactory } from '../../../shared/factories/logger.factory';
import { LogContextEnum } from '../../../shared/enum';

@Injectable()
export class WorksheetsSyncService {
  private _logger: Logger;
  constructor(
    _loggerFactory: LoggerFactory,
    private _mpEmployeeWorksheetRepository: MpEmployeeWorksheetRepository,
    private _mappingService: JobWorksheetMappingService,
    private _jobsService: JobsService,
    private _jobWorksheetsRepository: JobWorksheetsRepository,
  ) {
    this._logger = _loggerFactory.get(LogContextEnum.jobSync);
  }

  public async syncJobWorksheets(jobId: string): Promise<void> {
    try {
      const job = await firstValueFrom(this._jobsService.findOneById(jobId));

      if (!job) {
        throw new NotFoundException('Job introuvable');
      }

      if (
        !job.worksheets ||
        !job.worksheets.length ||
        job.mp.statusNumber === 1000
      ) {
        return;
      }

      for (const worksheet of job.worksheets) {
        try {
          const mpWorksheet = await firstValueFrom(
            this._mpEmployeeWorksheetRepository.getWorksheet(worksheet.id),
          );

          if (!mpWorksheet) {
            await firstValueFrom(
              this._jobWorksheetsRepository.removeOne(
                job._id.toString(),
                worksheet.id,
              ),
            );

            this._logger.warn(
              `[WorksheetsSyncService] a worksheet has been deleted because it has been not found in MultiPress. ID ${worksheet.id}`,
            );
          } else {
            await firstValueFrom(
              this._jobWorksheetsRepository.updateOne(
                jobId,
                worksheet.id,
                this._mappingService.getMappedData(mpWorksheet),
              ),
            );
          }
        } catch (err) {
          this._logger.error(
            `[WorksheetsSyncService] syncJobWorksheet failed: ${err}`,
          );
        }
      }
    } catch (err) {
      throw new InternalServerErrorException(
        'Impossible de synchroniser les fiches de travail du job',
      );
    }
  }

  public async syncJobsWorksheets(): Promise<void> {
    try {
      this._logger.log(`[WorksheetsSyncService] worksheets sync start`);
      const jobs = await firstValueFrom(
        this._jobsService.find({ endStatusNumber: 999 }),
      );

      for (const job of jobs) {
        try {
          await this.syncJobWorksheets(job._id.toString());
        } catch {}
      }
      this._logger.log(`[WorksheetsSyncService] worksheets sync stop`);
    } catch {}
  }
}
