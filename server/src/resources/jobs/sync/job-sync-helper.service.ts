import { Injectable } from '@nestjs/common';
import { IMpJobQuotation } from '../../../mp/mp-jobs/interfaces';
import { MpJobTypeEnum } from '../../../mp/mp-jobs/enum';

@Injectable()
export class JobSyncHelperService {
  public isArchivedJob(mpJob: IMpJobQuotation): boolean {
    return (
      mpJob.type === MpJobTypeEnum.ArchivedJob ||
      mpJob.job_status_number === 1000
    );
  }
}
