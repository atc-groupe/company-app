import { Injectable } from '@nestjs/common';

@Injectable()
export class JobSyncProcessService {
  private _processingMpIds = new Set<number>();

  public setProcessingMpId(id: number): void {
    this._processingMpIds.add(id);
  }

  public clearProcessingMpId(id: number): void {
    this._processingMpIds.delete(id);
  }

  public isAlreadyProcessingMpId(id: number): boolean {
    return this._processingMpIds.has(id);
  }
}
