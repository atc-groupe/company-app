import { Injectable } from '@nestjs/common';
import { IMpJobQuotation } from '../../../mp/mp-jobs/interfaces';
import { DeliveryAddress } from '../schemas';
import { DeliveryAddressMappingService } from '../mapping';
import { forkJoin, map, Observable, of } from 'rxjs';
import { MpDeliveryAddressRepository } from '../../../mp/mp-jobs/repositories';
import { LoggerFactory } from '../../../shared/factories/logger.factory';
import { Logger } from '../../../shared/services';
import { LogContextEnum } from '../../../shared/enum';

@Injectable()
export class DeliveryAddressesSyncService {
  private _logger: Logger;
  constructor(
    loggerFactory: LoggerFactory,
    private _addressesMapping: DeliveryAddressMappingService,
    private _mpAddressRepository: MpDeliveryAddressRepository,
  ) {
    this._logger = loggerFactory.get(LogContextEnum.jobSync);
  }

  public getSyncAddresses(
    mpJob: IMpJobQuotation,
  ): Observable<DeliveryAddress[] | null> {
    if (!mpJob.delivery_address.length) {
      this._logger.warn(
        `No delivery address found. Job Number: ${mpJob.job_number}`,
      );

      return of(null);
    }

    return forkJoin(
      mpJob.delivery_address.map((item) =>
        this._mpAddressRepository.findOneById(item.id),
      ),
    ).pipe(
      map((mpAddresses) =>
        this._addressesMapping.getMappedCollection(mpAddresses),
      ),
    );
  }
}
