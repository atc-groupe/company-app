import {
  HttpException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { MpJobsQuotationRepository } from '../../../mp/mp-jobs/repositories';
import {
  catchError,
  forkJoin,
  map,
  Observable,
  of,
  switchMap,
  tap,
} from 'rxjs';
import { LogContextEnum } from '../../../shared/enum';
import {
  JobMetaMappingService,
  JobMpMappingService,
  JobSubcontractorMappingService,
  MappingProcessService,
} from '../mapping';

import { JobRepository } from '../repositories';
import { JobDocument, JobSubcontractor } from '../schemas';
import { JobSyncStatusEnum } from '../enum';
import { LoggerFactory } from '../../../shared/factories/logger.factory';
import { IJobCreateDto } from '../dto';
import { IMpJobQuotation } from '../../../mp/mp-jobs/interfaces';
import { Settings } from '../../settings/schemas';
import { DeliveryAddressesSyncService } from './delivery-addresses-sync.service';
import { SubJobsSyncService } from './sub-jobs-sync.service';
import { OperationsSyncService } from './operations-sync.service';
import { SettingsService } from '../../settings/services';

@Injectable()
export class JobSyncOneService {
  private _logger;

  constructor(
    loggerFactory: LoggerFactory,
    private _mappingProcessService: MappingProcessService,
    private _mpJobRepository: MpJobsQuotationRepository,
    private _mpMapping: JobMpMappingService,
    private _subcontractorMapping: JobSubcontractorMappingService,
    private _metaMapping: JobMetaMappingService,
    private _jobRepository: JobRepository,
    private _addressesSync: DeliveryAddressesSyncService,
    private _subJobsSync: SubJobsSyncService,
    private _operationSync: OperationsSyncService,
    private _settingsService: SettingsService,
  ) {
    this._logger = loggerFactory.get(LogContextEnum.jobSync);
  }

  public syncOne(id: number): Observable<JobDocument> {
    this._mappingProcessService.processingJobMpId = id;
    this._logger.log(
      `[JobSyncOneService] Démarrage de la synchronisation du job mpID ${id}`,
    );

    let job: JobDocument | null;
    let mpJob: IMpJobQuotation;
    let settings: Settings;

    return this._mpJobRepository.findOneById(id).pipe(
      switchMap((data) => {
        if (!data) {
          throw new NotFoundException(
            `Impossible de trouver le job depuis l'API MultiPress par son ID. mpID: ${id}`,
          );
        }

        mpJob = data;
        const job$ = this._getJob$(mpJob.job_number);
        const settings$ = this._getAppSettings$();

        return forkJoin([job$, settings$]);
      }),

      switchMap(([jobData, settingsData]) => {
        job = jobData;
        settings = settingsData;

        const addresses$ = this._addressesSync.getSyncAddresses(mpJob);
        const operations$ = this._operationSync.getSyncOperations(job);
        const subJobs$ = this._subJobsSync.getSyncSubJobs(
          mpJob,
          job ? job.subJobs : null,
          settings,
        );

        return forkJoin([addresses$, subJobs$, operations$]);
      }),

      switchMap(([deliveryAddresses, subJobs, operations]) => {
        const mp = this._mpMapping.getMappedData(mpJob);

        let subcontractors: JobSubcontractor[] | null = null;
        if (subJobs) {
          subcontractors =
            this._subcontractorMapping.getMappedCollection(subJobs);
        }

        const meta = this._metaMapping.getMappedData(
          subJobs,
          deliveryAddresses,
          settings,
        );

        let job$: Observable<JobDocument | null>;
        let action$: Observable<'create' | 'update'>;
        const dto: IJobCreateDto = {
          mp,
          subJobs,
          deliveryAddresses,
          subcontractors,
          meta,
          operations,
          sync: { status: JobSyncStatusEnum.SUCCESS, timestamp: Date.now() },
        };

        if (job) {
          action$ = of('update');
          job$ = this._jobRepository.updateOne(job._id.toString(), dto);
        } else {
          action$ = of('create');
          job$ = this._jobRepository.insertOne(dto);
        }

        return forkJoin([action$, job$]);
      }),

      map(([action, job]) => {
        if (!job) {
          throw new InternalServerErrorException(
            `Une erreur est survenue lors de l'enregistrement du job dans la DB. mpID: ${this._mappingProcessService.processingJobMpId}`,
          );
        }

        this._logger.log(
          `[JobSyncOneService] Le job n°${job.mp.number} a été ${
            action === 'update' ? 'mis à jour' : 'créé'
          } avec success`,
        );

        this._mappingProcessService.clearData();

        return job;
      }),

      catchError((err) => {
        let message = `Une erreur est survenue lors de la synchronisation du job. mpID: ${id}`;
        let error = err;

        if (err instanceof HttpException) {
          message = `${message}. Message: ${err.message}`;
        } else {
          error = new InternalServerErrorException(message);
        }

        this._logger.error(`[JobSyncOneService] ${message}`);
        const jobId = this._mappingProcessService.processingJobId;
        this._mappingProcessService.clearData();

        if (jobId) {
          return this._saveJobError(jobId, err.message ? err.message : null);
        }

        throw error;
      }),
    );
  }

  private _getJob$(mpNumber: number): Observable<JobDocument | null> {
    return this._jobRepository.findOneByMpNumber(mpNumber).pipe(
      tap((job) => {
        if (job) {
          this._mappingProcessService.processingJobId = job._id.toString();
        }
      }),
    );
  }

  private _getAppSettings$(): Observable<Settings> {
    return this._settingsService.findUnique().pipe(
      map((settings) => {
        if (!settings) {
          this._logger.error(
            "[JobSyncOneService] Impossible de synchroniser le job car les paramètres de l'app ne sont pas initialisés",
          );

          throw new InternalServerErrorException(
            "Les paramètres de l'app ne sont pas initialisés",
          );
        }

        return settings;
      }),
    );
  }

  private _saveJobError(id: string, message: string): Observable<JobDocument> {
    return this._jobRepository
      .updateOne(id, {
        sync: {
          status: JobSyncStatusEnum.ERROR,
          timestamp: Date.now(),
          errorMessage: message,
        },
      })
      .pipe(
        map((job) => {
          if (!job) {
            throw new NotFoundException('Job introuvable dans la DB');
          }

          return job;
        }),
      );
  }
}
