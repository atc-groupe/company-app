import {
  HttpException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import {
  catchError,
  firstValueFrom,
  map,
  Observable,
  switchMap,
  tap,
} from 'rxjs';
import { JobSyncOneService } from './job-sync-one.service';
import { LogContextEnum } from '../../../shared/enum';
import {
  MpJobQuotationListRepository,
  MpJobsQuotationRepository,
} from '../../../mp/mp-jobs/repositories';
import { JobRepository } from '../repositories';
import { JobSyncProcessService } from './job-sync-process.service';
import { LoggerFactory } from '../../../shared/factories/logger.factory';
import { JobAlreadyInSyncErrorException } from '../../../shared/exceptions';
import { JobDocument } from '../schemas';
import { IActivesJobsSyncResult } from '../interfaces';

@Injectable()
export class JobSyncService {
  private _logger;

  constructor(
    loggerFactory: LoggerFactory,
    private _syncOneService: JobSyncOneService,
    private _mpJobListRepository: MpJobQuotationListRepository,
    private _mpJobRepository: MpJobsQuotationRepository,
    private _jobRepository: JobRepository,
    private _syncProcess: JobSyncProcessService,
  ) {
    this._logger = loggerFactory.get(LogContextEnum.jobSync);
  }

  public syncOne(id: number): Observable<JobDocument> {
    if (this._syncProcess.isAlreadyProcessingMpId(id)) {
      const message = `Le job ${id} est déjà en cours de synchronisation`;

      this._logError(message);
      throw new JobAlreadyInSyncErrorException(message);
    }

    this._syncProcess.setProcessingMpId(id);

    return this._syncOneService.syncOne(id).pipe(
      tap(() => {
        this._syncProcess.clearProcessingMpId(id);
      }),
      catchError((err) => {
        this._syncProcess.clearProcessingMpId(id);
        this._logError(
          `Une erreur est survenue lors de la synchronisation du job ID: ${id}`,
          err,
        );

        if (err instanceof NotFoundException) {
          return this._jobRepository.removeOneByMpId(id).pipe(
            map(() => {
              throw new NotFoundException("Le job n'a pas été trouvé");
            }),
          );
        }

        throw err;
      }),
    );
  }

  public syncOneByNumber(mpNumber: number): Observable<JobDocument> {
    return this._mpJobRepository.findOneByNumber(mpNumber).pipe(
      switchMap((mpJob) => {
        if (!mpJob) {
          throw new NotFoundException(
            `Le job n° ${mpNumber} n'a pas été trouvé`,
          );
        }

        return this.syncOne(mpJob.id);
      }),

      catchError((err) => {
        const message = `Une erreur est survenue lors de la synchronisation du job n°: ${mpNumber}`;
        this._logError(message, err);

        if (err instanceof HttpException) {
          throw err;
        }

        throw new InternalServerErrorException(message);
      }),
    );
  }

  public async syncActives(): Promise<IActivesJobsSyncResult> {
    let syncCount = 0;
    const start = Date.now();
    try {
      const list = await firstValueFrom(
        this._mpJobListRepository.findActiveJobs(),
      );

      if (!list) {
        return {
          status: 'success',
          jobsSyncCount: 0,
          syncTime: null,
        };
      }

      for (const item of list) {
        try {
          await firstValueFrom(this._syncOneService.syncOne(item.id));
          syncCount++;
        } catch {}
      }

      return {
        status: 'success',
        jobsSyncCount: syncCount,
        syncTime: `${Math.floor((Date.now() - start) / 1000)} secondes`,
      };
    } catch (err) {
      const message =
        'Une erreur est survenue lors de la synchronisation des jobs actifs';
      this._logError(message, err);

      const time = Math.floor((Date.now() - start) / 1000);
      const minutes = Math.floor(time / 60);
      const seconds = time % 60;

      return {
        status: 'error',
        jobsSyncCount: syncCount,
        syncTime: `${minutes} minutes et ${seconds} secondes`,
        error: err.message,
      };
    }
  }

  private _logError(message: string, err?: any) {
    if (err?.message) {
      message = `${message}. Message: ${err.message}`;
    }

    this._logger.error(`[JobSyncService] ${message}`);
  }
}
