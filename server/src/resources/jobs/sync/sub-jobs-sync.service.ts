import { Injectable } from '@nestjs/common';
import { IMpJobQuotation } from '../../../mp/mp-jobs/interfaces';
import { forkJoin, map, Observable, of } from 'rxjs';
import { SubJob } from '../schemas';
import { Logger } from '../../../shared/services';
import { LoggerFactory } from '../../../shared/factories/logger.factory';
import { MpSubJobsQuotationRepository } from '../../../mp/mp-jobs/repositories';
import { MpSubJobDetailInfoRepository } from '../../../mp/mp-workflow/mp-sub-job-detail-info.repository';
import { LogContextEnum } from '../../../shared/enum';
import { SubJobMappingService } from '../mapping';
import { Settings } from '../../settings/schemas';

@Injectable()
export class SubJobsSyncService {
  private _logger: Logger;

  constructor(
    loggerFactory: LoggerFactory,
    private _mpSubJobRepository: MpSubJobsQuotationRepository,
    private _mpSubJobWorkflowRepository: MpSubJobDetailInfoRepository,
    private _subJobsMapping: SubJobMappingService,
  ) {
    this._logger = loggerFactory.get(LogContextEnum.jobSync);
  }
  public getSyncSubJobs(
    mpJob: IMpJobQuotation,
    subJobs: SubJob[] | null,
    settings: Settings,
  ): Observable<SubJob[] | null> {
    if (!mpJob.sub_jobs.length) {
      this._logger.warn(`No sub-job found. Job Number: ${mpJob.job_number}`);

      return of(null);
    }

    return forkJoin(
      mpJob.sub_jobs.map((item) =>
        forkJoin([
          of(item.id),
          this._mpSubJobRepository.findOne(item.id),
          this._mpSubJobWorkflowRepository.getData(item.id),
        ]),
      ),
    ).pipe(
      map((mpSubJobsData) =>
        this._subJobsMapping.getMappedCollection(
          mpSubJobsData,
          subJobs,
          settings,
        ),
      ),
    );
  }
}
