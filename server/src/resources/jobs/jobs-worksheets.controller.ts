import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { JobWorksheetsService } from './services';
import { JobWorksheetCreateDto, JobWorksheetUpdateDto } from './dto';
import { Observable } from 'rxjs';
import { JobDocument } from './schemas';
import { WorksheetsSyncService } from './sync/worksheets-sync.service';
import { Authorizations } from '../auth/authorization.decorator';
import { AuthActionsJobsEnum, AuthSubjectsEnum } from '../../shared/enum';

@Controller('jobs')
export class JobsWorksheetsController {
  constructor(
    private _worksheetsService: JobWorksheetsService,
    private _worksheetsSyncService: WorksheetsSyncService,
  ) {}

  @Post(':id/worksheets')
  @Authorizations(
    {
      subject: AuthSubjectsEnum.Jobs,
      action: AuthActionsJobsEnum.CreateWorksheet,
    },
    {
      subject: AuthSubjectsEnum.Jobs,
      action: AuthActionsJobsEnum.ManageWorksheets,
    },
  )
  public addWorksheet(
    @Param('id') id: string,
    @Body() dto: JobWorksheetCreateDto,
  ): Observable<JobDocument> {
    return this._worksheetsService.addOne(id, dto);
  }

  @Get(':id/worksheets/sync')
  public async syncWorksheets(@Param('id') id: string): Promise<void> {
    await this._worksheetsSyncService.syncJobWorksheets(id);
  }

  @Delete(':id/worksheets/:worksheetId')
  @Authorizations(
    {
      subject: AuthSubjectsEnum.Jobs,
      action: AuthActionsJobsEnum.DeleteMyWorksheets,
    },
    {
      subject: AuthSubjectsEnum.Jobs,
      action: AuthActionsJobsEnum.ManageWorksheets,
    },
  )
  public removeWorksheet(
    @Param('id') id: string,
    @Param('worksheetId') worksheetId: string,
  ): Observable<JobDocument> {
    return this._worksheetsService.removeOne(id, worksheetId);
  }

  @Patch(':id/worksheets/:worksheetId')
  @Authorizations(
    {
      subject: AuthSubjectsEnum.Jobs,
      action: AuthActionsJobsEnum.UpdateMyWorksheets,
    },
    {
      subject: AuthSubjectsEnum.Jobs,
      action: AuthActionsJobsEnum.ManageWorksheets,
    },
  )
  public updateWorksheet(
    @Param('id') id: string,
    @Param('worksheetId') worksheetId: string,
    @Body() dto: JobWorksheetUpdateDto,
  ): Observable<JobDocument> {
    return this._worksheetsService.updateOne(id, worksheetId, dto);
  }
}
