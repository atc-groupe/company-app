import {
  ConnectedSocket,
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketGateway,
  WsException,
} from '@nestjs/websockets';
import { Socket } from 'socket.io';
import { AppWsResponse, AppWsSuccessResponse } from '../../shared/classes';
import { AppWsInternalServerErrorException } from '../../shared/exceptions';
import { IStockReservationLine } from './interfaces';
import {
  StockReservationsDataService,
  StockReservationsStateService,
} from './services';

@WebSocketGateway({
  transports: ['websocket'],
  namespace: 'job.stock-reservations',
})
export class StockReservationsGateway implements OnGatewayDisconnect {
  constructor(
    private _statesService: StockReservationsStateService,
    private _dataService: StockReservationsDataService,
  ) {}

  handleDisconnect(client: Socket): void {
    this._statesService.removeSocket(client.id);
  }

  @SubscribeMessage('list.subscribe')
  public async onSubscribeData(
    @ConnectedSocket() client: Socket,
  ): Promise<AppWsResponse | WsException> {
    try {
      const lines = await this._dataService.getReservationsLines();
      this._statesService.pushState(lines, client);

      return new AppWsSuccessResponse<IStockReservationLine[]>(lines);
    } catch (err) {
      return new AppWsInternalServerErrorException(
        'Une erreur est survenue lors de la récupération de la liste des jobs',
      );
    }
  }

  public emitLineUpdate(sockets: Socket[], line: IStockReservationLine) {
    sockets.forEach((socket) => {
      socket.emit('item.update', line);
    });
  }
}
