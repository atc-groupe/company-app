import { Body, Controller, Delete, Param, Patch, Post } from '@nestjs/common';
import { Observable } from 'rxjs';
import { JobsCommentsService } from './services';
import {
  JobDeliveryCommentDto,
  JobPlanningCommentCreateDto,
  JobPlanningCommentUpdateDto,
} from './dto';
import { Job } from './schemas';
import { Authorizations } from '../auth/authorization.decorator';
import { AuthActionsJobsEnum, AuthSubjectsEnum } from '../../shared/enum';

@Controller('jobs')
export class JobsCommentsController {
  constructor(private _commentsService: JobsCommentsService) {}

  @Patch(':id/comments/delivery')
  @Authorizations({
    subject: AuthSubjectsEnum.Jobs,
    action: AuthActionsJobsEnum.UpdateDeliveryPlanningComment,
  })
  public updateDelivery(
    @Param('id') id: string,
    @Body() dto: JobDeliveryCommentDto,
  ): Observable<Job> {
    return this._commentsService.updateDelivery(id, dto);
  }

  @Post(':id/comments/planning')
  @Authorizations({
    subject: AuthSubjectsEnum.Jobs,
    action: AuthActionsJobsEnum.UpdatePlanningComment,
  })
  public addPlanningComment(
    @Param('id') id: string,
    @Body() dto: JobPlanningCommentCreateDto,
  ): Observable<Job> {
    return this._commentsService.addPlanningComment(id, dto);
  }

  @Patch(':id/comments/planning/:commentId')
  @Authorizations({
    subject: AuthSubjectsEnum.Jobs,
    action: AuthActionsJobsEnum.UpdatePlanningComment,
  })
  public updatePlanningComment(
    @Param('id') id: string,
    @Param('commentId') commentId: string,
    @Body() dto: JobPlanningCommentUpdateDto,
  ): Observable<Job> {
    return this._commentsService.updatePlanningComment(
      id,
      commentId,
      dto.comment,
    );
  }
  @Delete(':id/comments/planning/:commentId')
  @Authorizations({
    subject: AuthSubjectsEnum.Jobs,
    action: AuthActionsJobsEnum.UpdatePlanningComment,
  })
  public removePlanningComment(
    @Param('id') id: string,
    @Param('commentId') commentId: string,
  ): Observable<Job> {
    return this._commentsService.removePlanningComment(id, commentId);
  }
}
