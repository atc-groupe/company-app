import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({ _id: false })
export class SubJobMeta {
  @Prop()
  surface: number;

  @Prop()
  hasSheetPrinting: boolean;

  @Prop({ type: [{ type: String }], default: null })
  deliveryServiceFinishes: string[];

  @Prop()
  hasNumericScoreCutting: boolean;

  @Prop()
  hasZundCutting: boolean;

  @Prop()
  hasKits: boolean;
}

export const SubJobMetaSchema = SchemaFactory.createForClass(SubJobMeta);
