import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({ _id: false })
export class FinishingLayerSides {
  @Prop()
  left: boolean;

  @Prop()
  right: boolean;

  @Prop()
  top: boolean;

  @Prop()
  bottom: boolean;
}

export const FinishingLayerSidesSchema =
  SchemaFactory.createForClass(FinishingLayerSides);
