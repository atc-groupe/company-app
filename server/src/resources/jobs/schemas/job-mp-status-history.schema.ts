import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({ _id: false })
export class JobMpStatusHistory {
  @Prop()
  date: string;

  @Prop()
  time: string;

  @Prop()
  status: string;

  @Prop()
  reason: string;
}

export const JobMpStatusHistorySchema =
  SchemaFactory.createForClass(JobMpStatusHistory);
