import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import {
  FinishingLayerOperation,
  FinishingLayerOperationSchema,
} from './finishing-layer-operation.schema';
import {
  FinishingLayerMaterial,
  FinishingLayerMaterialSchema,
} from './finishing-layer-material.schema';
import {
  FinishingLayerSides,
  FinishingLayerSidesSchema,
} from './finishing-layer-sides.schema';
import { MpSubJobFinishingTypeEnum } from '../../../mp/mp-workflow/enums/mp-sub-job-finishing-type.enum';

@Schema({ _id: false })
export class SubJobFinishingLayer {
  @Prop({ type: String, enum: MpSubJobFinishingTypeEnum })
  finishingType: MpSubJobFinishingTypeEnum;

  @Prop({ type: FinishingLayerOperationSchema })
  operation: FinishingLayerOperation;

  @Prop({ type: FinishingLayerMaterialSchema, default: null })
  material: FinishingLayerMaterial | null;

  @Prop({ type: FinishingLayerSidesSchema, default: undefined })
  sides?: FinishingLayerSides;

  @Prop()
  displayValue: string;
}

export const SubJobFinishingLayerSchema =
  SchemaFactory.createForClass(SubJobFinishingLayer);
