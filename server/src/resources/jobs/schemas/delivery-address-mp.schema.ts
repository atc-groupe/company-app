import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({ _id: false })
export class DeliveryAddressMp {
  @Prop()
  company: string;

  @Prop()
  address: string;

  @Prop()
  zipCode: string;

  @Prop()
  city: string;

  @Prop()
  country: string;

  @Prop()
  countryCode: string;

  @Prop()
  contactName: string;

  @Prop()
  phone: string;

  @Prop()
  email: string;

  @Prop()
  relation: string;

  @Prop()
  addressNumber: string;

  @Prop()
  relationNumber: number;

  @Prop()
  deliveryMethod: string;

  @Prop()
  sendingDate: string;

  @Prop()
  deliveryDate: string;

  @Prop({ type: [{ type: String }], default: null })
  extraAddress: string[] | null;
}

export const DeliveryAddressMpSchema =
  SchemaFactory.createForClass(DeliveryAddressMp);
