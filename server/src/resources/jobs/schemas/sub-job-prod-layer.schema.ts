import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import {
  SubJobNumericLayer,
  SubJobNumericLayerSchema,
} from './sub-job-numeric-layer.schema';
import {
  SubJobFinishingLayer,
  SubJobFinishingLayerSchema,
} from './sub-job-finishing-layer.schema';

@Schema({ _id: false })
export class SubJobProdLayer {
  @Prop({ type: SubJobNumericLayerSchema })
  numeric: SubJobNumericLayer;

  @Prop({ type: [{ type: SubJobFinishingLayerSchema }], default: null })
  finishes: SubJobFinishingLayer[] | null;
}

export const SubJobProdLayerSchema =
  SchemaFactory.createForClass(SubJobProdLayer);
