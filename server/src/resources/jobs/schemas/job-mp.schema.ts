import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { JobInfo, JobInfoSchema } from './job-info.schema';
import { JobDates, JobDatesSchema } from './job-dates.schema';
import {
  JobMpStatusHistory,
  JobMpStatusHistorySchema,
} from './job-mp-status-history.schema';

@Schema({ _id: false })
export class JobMp {
  @Prop() id: number;

  @Prop() number: number;

  @Prop() numberSearch: string;

  @Prop() type: number;

  @Prop() company: string;

  @Prop() holding: string;

  @Prop() relationNumber: number;

  @Prop() contactName: string;

  @Prop() description: string;

  @Prop() reference: string;

  @Prop() phone: string;

  @Prop() email: string;

  @Prop() creator: string;

  @Prop() calculator: string;

  @Prop() salesMan: string;

  @Prop() jobManager: string;

  @Prop() price: number;

  @Prop() statusNumber: number;

  @Prop({ type: [{ type: JobMpStatusHistorySchema }] })
  statusHistory: JobMpStatusHistory[];

  @Prop() invoiceStatus: 'OUI' | 'NON';

  @Prop() planned: boolean;

  @Prop() delivered: string;

  @Prop() filesDelivered: boolean;

  @Prop({ type: JobInfoSchema }) info: JobInfo;

  @Prop({ type: JobDatesSchema }) dates: JobDates;
}

export const JobMpSchema = SchemaFactory.createForClass(JobMp);
