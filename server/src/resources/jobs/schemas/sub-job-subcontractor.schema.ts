import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import {
  SubJobSubcontractorItem,
  SubJobSubcontractorItemSchema,
} from './sub-job-subcontractor-item.schema';

@Schema({ _id: false })
export class SubJobSubcontractor {
  @Prop()
  relation: string;

  @Prop()
  relationNumber: number;

  @Prop({ type: [{ type: SubJobSubcontractorItemSchema }] })
  items: SubJobSubcontractorItem[];
}

export const SubJobSubcontractorSchema =
  SchemaFactory.createForClass(SubJobSubcontractor);
