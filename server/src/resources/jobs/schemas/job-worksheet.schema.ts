import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({ _id: false })
export class JobWorksheet {
  @Prop()
  id: string;

  @Prop()
  date: string;

  @Prop()
  employee: string;

  @Prop()
  startTime: number;

  @Prop()
  stopTime: number;

  @Prop()
  timeProduction: number;

  @Prop()
  employeeNumber: number;

  @Prop()
  timeCustomer: number;

  @Prop()
  operation: string;

  @Prop()
  operationNumber: number;

  @Prop()
  totalTime: number;

  @Prop()
  remark: string;

  @Prop()
  countRuns: number;

  @Prop()
  operationReady: boolean;
}

export const JobWorksheetSchema = SchemaFactory.createForClass(JobWorksheet);
