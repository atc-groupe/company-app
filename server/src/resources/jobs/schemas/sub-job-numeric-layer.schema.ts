import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({ _id: false })
export class SubJobNumericLayer {
  @Prop()
  uniqueId: number;

  @Prop()
  layerName: string;

  @Prop()
  layerIndex: number;

  @Prop()
  remark: string;

  @Prop()
  device: string;

  @Prop()
  deviceDisplayName: string;

  @Prop()
  printMode: string;

  @Prop()
  roll: boolean;

  @Prop()
  width: number;

  @Prop()
  height: number;

  @Prop()
  quantity: number;

  @Prop()
  paperName: string;

  @Prop()
  paperWeight: number;

  @Prop()
  paperThickness: number;

  @Prop()
  paperCode: string;

  @Prop()
  paperId: number;
}

export const SubJobNumericLayerSchema =
  SchemaFactory.createForClass(SubJobNumericLayer);
