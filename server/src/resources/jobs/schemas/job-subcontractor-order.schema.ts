import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import {
  JobSubcontractorOrderSubJob,
  JobSubcontractorOrderSubJobSchema,
} from './job-subcontractor-order-sub-job.schema';

@Schema({ _id: false })
export class JobSubcontractorOrder {
  @Prop({ type: String })
  contactName: string | null;

  @Prop({ type: String })
  sendingDate: string | null;

  @Prop({ type: String })
  deliveryDate: string | null;

  @Prop({ type: [{ type: JobSubcontractorOrderSubJobSchema }] })
  subJobs: JobSubcontractorOrderSubJob[];
}

export const JobSubcontractorOrderSchema = SchemaFactory.createForClass(
  JobSubcontractorOrder,
);
