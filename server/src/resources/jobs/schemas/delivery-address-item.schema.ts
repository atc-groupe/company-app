import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({ _id: false })
export class DeliveryAddressItem {
  @Prop()
  ordering: number;

  @Prop()
  quantity: number;

  @Prop()
  packedPer: number;

  @Prop()
  packages: number;

  @Prop()
  description: string;

  @Prop()
  remark: string;

  @Prop()
  extraInfo: string;

  @Prop()
  weight: number;

  @Prop()
  status: string;
}

export const DeliveryAddressItemSchema =
  SchemaFactory.createForClass(DeliveryAddressItem);
