import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({ _id: false })
export class JobSubcontractorOrderSubJobItem {
  @Prop()
  description: string;

  @Prop({ type: String })
  remark: string | null;

  @Prop()
  quantity: number;

  @Prop()
  ordered: boolean;
}

export const JobSubcontractorOrderSubJobItemSchema =
  SchemaFactory.createForClass(JobSubcontractorOrderSubJobItem);
