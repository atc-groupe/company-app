import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { JobSyncStatusEnum } from '../../../resources/jobs/enum/job-sync-status.enum';

@Schema({ _id: false })
export class JobSync {
  @Prop({ type: String })
  status: JobSyncStatusEnum;

  @Prop({ type: Number, default: Date.now() })
  timestamp: number;

  @Prop({ type: String })
  errorMessage?: string;
}

export const JobSyncSchema = SchemaFactory.createForClass(JobSync);
