import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({ _id: false })
export class FinishingLayerMaterial {
  @Prop()
  name: string;

  @Prop()
  id: number;
}

export const FinishingLayerMaterialSchema = SchemaFactory.createForClass(
  FinishingLayerMaterial,
);
