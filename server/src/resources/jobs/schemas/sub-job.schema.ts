import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { SubJobMeta, SubJobMetaSchema } from './sub-job-meta.schema';
import { SubJobMp, SubJobMpSchema } from './sub-job-mp.schema';
import {
  SubJobProdLayer,
  SubJobProdLayerSchema,
} from './sub-job-prod-layer.schema';
import {
  SubJobFinishingLayer,
  SubJobFinishingLayerSchema,
} from './sub-job-finishing-layer.schema';
import {
  SubJobSubcontractor,
  SubJobSubcontractorSchema,
} from './sub-job-subcontractor.schema';

@Schema({ _id: false })
export class SubJob {
  @Prop({ type: Number, default: -1 })
  referenceIndex: number;

  @Prop({ type: String, default: '' })
  reference: string;

  @Prop({ type: SubJobMpSchema })
  mp: SubJobMp;

  @Prop({ type: [{ type: SubJobProdLayerSchema }], default: null })
  prodLayers: SubJobProdLayer[] | null;

  @Prop({ type: [{ type: SubJobFinishingLayerSchema }], default: null })
  unlinkedFinishingLayers: SubJobFinishingLayer[] | null;

  @Prop({ type: [{ type: SubJobSubcontractorSchema }], default: null })
  subcontractors: SubJobSubcontractor[] | null;

  @Prop({ type: SubJobMetaSchema, default: null })
  meta: SubJobMeta | null;
}

export const SubJobSchema = SchemaFactory.createForClass(SubJob);
