import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { SubJobMp, SubJobMpSchema } from './sub-job-mp.schema';
import {
  JobSubcontractorOrderSubJobItem,
  JobSubcontractorOrderSubJobItemSchema,
} from './job-subcontractor-order-sub-job-item.schema';

@Schema({ _id: false })
export class JobSubcontractorOrderSubJob {
  @Prop()
  reference: string;

  @Prop({ type: SubJobMpSchema })
  subJobMp: SubJobMp;

  @Prop({ type: [{ type: JobSubcontractorOrderSubJobItemSchema }] })
  items: JobSubcontractorOrderSubJobItem[];
}

export const JobSubcontractorOrderSubJobSchema = SchemaFactory.createForClass(
  JobSubcontractorOrderSubJob,
);
