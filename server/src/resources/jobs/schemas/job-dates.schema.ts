import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({ _id: false })
export class JobDates {
  @Prop({ type: String, default: null })
  date: string | null;

  @Prop({ type: String, default: null })
  sending: string | null;

  @Prop({ type: String, default: null })
  delivery: string | null;

  @Prop()
  deliveryWeek: number;

  @Prop()
  deliveryDateLocked: boolean;

  @Prop({ type: String, default: null })
  atWork: string | null;

  @Prop({ type: String, default: null })
  proof: string | null;

  @Prop({ type: String, default: null })
  return: string | null;
}

export const JobDatesSchema = SchemaFactory.createForClass(JobDates);
