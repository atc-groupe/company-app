import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({ _id: false })
export class JobMeta {
  @Prop()
  printingSurface: number;

  @Prop()
  modelCount: number;

  @Prop()
  manufacturedPiecesCount: number;

  @Prop({ type: [{ type: String }] })
  productionTypes: string[];

  @Prop({ type: [{ type: String }] })
  deliveryServiceFinishes: string[];

  @Prop()
  hasSheetProduction: boolean;

  @Prop()
  hasNumericScoreCutting: boolean;

  @Prop()
  hasZundCutting: boolean;

  @Prop()
  hasKits: boolean;

  @Prop()
  hasSubcontractingProduction: boolean;

  @Prop()
  addressCount: number;

  @Prop({ type: [{ type: String }] })
  addressZipCodes: string[];

  @Prop({ type: [{ type: String }] })
  deliveryMethods: string[];

  @Prop({ type: [{ type: String }] })
  deliverySendingDates: string[];
}

export const JobMetaSchema = SchemaFactory.createForClass(JobMeta);
