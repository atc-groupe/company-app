import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({ _id: false })
export class JobPrePress {
  @Prop()
  note: string;

  @Prop()
  graphicDesignNote: string;

  @Prop()
  designNote: string;

  @Prop()
  filesCheck: string;

  @Prop({ type: Number, default: 1 })
  filesCheckStatusIndex: number;
}

export const JobPrePressSchema = SchemaFactory.createForClass(JobPrePress);
