import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { SubJob, SubJobSchema } from './sub-job.schema';
import { HydratedDocument } from 'mongoose';
import {
  DeliveryAddress,
  DeliveryAddressSchema,
} from './delivery-address.schema';
import { JobMeta, JobMetaSchema } from './job-meta.schema';
import { JobMp, JobMpSchema } from './job-mp.schema';
import { JobSync, JobSyncSchema } from './job-sync.schema';
import { JobComments, JobCommentsSchema } from './job-comments.schema';
import { JobPrePress, JobPrePressSchema } from './job-pre-press.schema';
import {
  JobSubcontractor,
  JobSubcontractorSchema,
} from './job-subcontractor.schema';
import { JobOperation, JobOperationSchema } from './job-operation.schema';
import {
  JobStockReservation,
  JobStockReservationSchema,
} from './job-stock-reservation.schema';
import { JobWorksheet, JobWorksheetSchema } from './job-worksheet.schema';
import { IJobPlanningCard } from '../interfaces';

@Schema()
export class Job {
  @Prop({ type: JobMpSchema })
  mp: JobMp;

  @Prop({ type: [{ type: SubJobSchema }], default: null })
  subJobs: SubJob[] | null;

  @Prop({ type: [{ type: DeliveryAddressSchema }], default: null })
  deliveryAddresses: DeliveryAddress[] | null;

  @Prop({ type: [{ type: JobSubcontractorSchema }] })
  subcontractors: JobSubcontractor[] | null;

  @Prop({ type: [{ type: JobOperationSchema }], default: null })
  operations: JobOperation[] | null;

  @Prop({ type: [{ type: JobWorksheetSchema }], default: null })
  worksheets: JobWorksheet[] | null;

  @Prop({ type: [{ type: JobStockReservationSchema }], default: null })
  stockReservations: JobStockReservation[] | null;

  @Prop({ type: JobMetaSchema })
  meta: JobMeta;

  @Prop({ type: JobSyncSchema, required: true })
  sync: JobSync;

  @Prop({ type: JobCommentsSchema, default: {} })
  comments: JobComments;

  @Prop({ type: JobPrePressSchema, default: {} })
  prePress: JobPrePress;

  @Prop()
  planningData?: IJobPlanningCard[];
}

export const JobSchema = SchemaFactory.createForClass(Job);
export type JobDocument = HydratedDocument<Job>;
