import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({ _id: false })
export class JobOperation {
  @Prop()
  operation: string;

  @Prop({ type: String })
  startDate: string | null;

  @Prop()
  department: number;

  @Prop()
  calculatedTime: number;

  @Prop()
  productionTime: number;

  @Prop()
  ordering: number;

  @Prop()
  employee: string;

  @Prop()
  information: string;

  @Prop()
  extraInfo: string;

  @Prop()
  machine: string;

  @Prop()
  machineId: number;
}

export const JobOperationSchema = SchemaFactory.createForClass(JobOperation);
