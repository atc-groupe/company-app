import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { JobStockReservationStatusEnum } from '../enum';
import { ProductGenericName } from '../../stock/schemas/product-generic-name.schema';
import { HydratedDocument, Types } from 'mongoose';
import { Paper } from '../../stock/schemas';

@Schema()
export class JobStockReservation {
  @Prop({
    type: String,
    enum: JobStockReservationStatusEnum,
    default: JobStockReservationStatusEnum.Brouillon,
  })
  status: JobStockReservationStatusEnum;

  @Prop({ type: Types.ObjectId, ref: 'ProductGenericName', default: null })
  paperGenericName: ProductGenericName | null;

  @Prop({ type: String, default: null })
  paperName: string | null;

  @Prop({ type: Number, ref: 'Paper', default: null })
  paper: Paper | null;

  @Prop({ type: Number, default: null })
  width: number | null;

  @Prop({ type: Number, default: null })
  height: number | null;

  @Prop()
  imperativeFormat: boolean;

  @Prop()
  quantity: number;

  @Prop()
  printSides: string;

  @Prop()
  destination: string;

  @Prop({ type: String, default: null })
  comment: string | null;

  @Prop({ type: Number, default: null })
  subtractQuantity: number | null;

  @Prop({ type: Date, default: null })
  mutationDate: Date | null;

  @Prop({ type: String })
  createUsername: string;

  @Prop({ type: String, default: null })
  updateUsername: string | null;

  @Prop({ type: String, default: null })
  handleUsername: string | null;

  @Prop({ type: Date, default: new Date() })
  createdAt: Date;

  @Prop({ type: Date, default: new Date() })
  updatedAt: Date;
}

export const JobStockReservationSchema =
  SchemaFactory.createForClass(JobStockReservation);
export type JobStockReservationDocument = HydratedDocument<JobStockReservation>;
