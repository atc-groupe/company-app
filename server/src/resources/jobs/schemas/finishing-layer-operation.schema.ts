import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({ _id: false })
export class FinishingLayerOperation {
  @Prop()
  name: string;

  @Prop()
  type?: string;

  @Prop()
  width?: number;

  @Prop()
  height?: number;

  @Prop()
  doubleSided?: boolean;
}

export const FinishingLayerOperationSchema = SchemaFactory.createForClass(
  FinishingLayerOperation,
);
