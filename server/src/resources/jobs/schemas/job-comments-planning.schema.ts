import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema()
export class JobCommentsPlanning {
  @Prop({ type: String, isRequired: true })
  comment: string;

  @Prop()
  userId: string;

  @Prop()
  userInitials: string;

  @Prop()
  planningGroup: string;

  @Prop({ type: Date, default: new Date() })
  createdAt: Date;

  @Prop({ type: Date, default: new Date() })
  updatedAt: Date;
}

export const JobCommentsPlanningSchema =
  SchemaFactory.createForClass(JobCommentsPlanning);
