import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { SubJobSubcontractorItemMp } from './sub-job-subcontractor-item-mp.schema';

@Schema({ _id: false })
export class SubJobSubcontractorItem {
  @Prop({ type: SubJobSubcontractorItemMp })
  mp: SubJobSubcontractorItemMp;
}

export const SubJobSubcontractorItemSchema = SchemaFactory.createForClass(
  SubJobSubcontractorItem,
);
