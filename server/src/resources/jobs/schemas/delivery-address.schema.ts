import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import {
  DeliveryAddressItem,
  DeliveryAddressItemSchema,
} from './delivery-address-item.schema';
import {
  DeliveryAddressMp,
  DeliveryAddressMpSchema,
} from './delivery-address-mp.schema';

@Schema({ _id: false })
export class DeliveryAddress {
  @Prop({ type: DeliveryAddressMpSchema })
  mp: DeliveryAddressMp;

  @Prop({ type: [{ type: DeliveryAddressItemSchema }] })
  items: DeliveryAddressItem[];
}

export const DeliveryAddressSchema =
  SchemaFactory.createForClass(DeliveryAddress);
