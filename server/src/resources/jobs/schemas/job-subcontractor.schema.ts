import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import {
  JobSubcontractorOrder,
  JobSubcontractorOrderSchema,
} from './job-subcontractor-order.schema';

@Schema({ _id: false })
export class JobSubcontractor {
  @Prop()
  relationNumber: number;

  @Prop()
  relation: string;

  @Prop({ type: [{ type: JobSubcontractorOrderSchema }] })
  orders: JobSubcontractorOrder[];
}

export const JobSubcontractorSchema =
  SchemaFactory.createForClass(JobSubcontractor);
