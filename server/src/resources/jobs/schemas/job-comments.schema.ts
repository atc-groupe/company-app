import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import {
  JobCommentsPlanning,
  JobCommentsPlanningSchema,
} from './job-comments-planning.schema';

@Schema({ _id: false })
export class JobComments {
  @Prop({ type: String, default: null })
  delivery: string;

  @Prop({ type: [{ type: JobCommentsPlanningSchema }], default: [] })
  planning: JobCommentsPlanning[];
}

export const JobCommentsSchema = SchemaFactory.createForClass(JobComments);
