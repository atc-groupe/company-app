import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({ _id: false })
export class SubJobSubcontractorItemMp {
  @Prop()
  contactName: string;

  @Prop()
  description: string;

  @Prop()
  remark: string;

  @Prop()
  quantity: number;

  @Prop()
  ordered: boolean;

  @Prop({ type: String, default: null })
  sendingDate: string | null;

  @Prop({ type: String, default: null })
  deliveryDate: string | null;
}

export const SubJobSubcontractorItemMpSchema = SchemaFactory.createForClass(
  SubJobSubcontractorItemMp,
);
