import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({ _id: false })
export class JobInfo {
  @Prop()
  general: string;

  @Prop()
  prePress: string;

  @Prop()
  digital: string;

  @Prop()
  finishing: string;

  @Prop()
  subcontract: string;

  @Prop()
  expeditions: string;

  @Prop()
  application: string;
}

export const JobInfoSchema = SchemaFactory.createForClass(JobInfo);
