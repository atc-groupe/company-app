import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import {
  SubJobCheckList,
  SubJobCheckListSchema,
} from './sub-job-check-list.schema';

@Schema({ _id: false })
export class SubJobMp {
  @Prop()
  id: number;

  @Prop()
  productNumber: number;

  @Prop()
  productType: string;

  @Prop()
  articleNumber: string;

  @Prop()
  articleType: string;

  @Prop()
  description: string;

  @Prop()
  quantity: number;

  @Prop()
  deliveredQuantity: number;

  @Prop()
  weight: number;

  @Prop()
  paoCheckRemark: string;

  @Prop()
  paoRemark: string;

  @Prop({ type: SubJobCheckListSchema })
  checklist: SubJobCheckList;
}

export const SubJobMpSchema = SchemaFactory.createForClass(SubJobMp);
