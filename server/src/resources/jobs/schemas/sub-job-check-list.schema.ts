import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({ _id: false })
export class SubJobCheckList {
  @Prop({ type: String, default: null })
  description: string | null;

  @Prop({ type: String, default: null })
  format: string | null;

  @Prop({ type: String, default: null })
  pao: string | null;

  @Prop({ type: String, default: null })
  print: string | null;

  @Prop({ type: String, default: null })
  material: string | null;

  @Prop({ type: String, default: null })
  finishing: string | null;

  @Prop({ type: String, default: null })
  packaging: string | null;

  @Prop({ type: String, default: null })
  delivery: string | null;

  @Prop({ type: String, default: null })
  application: string | null;

  @Prop({ type: String, default: null })
  remark: string | null;
}

export const SubJobCheckListSchema =
  SchemaFactory.createForClass(SubJobCheckList);
