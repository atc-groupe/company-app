import { Injectable } from '@nestjs/common';
import { IJobLine, IJobsListState, IJobsListStateItem } from '../interfaces';
import { JobsHashService } from './jobs-hash.service';
import { Socket } from 'socket.io';
import { JobsQueryParamDto } from '../dto';

@Injectable()
export class JobsListStateService {
  private _states: IJobsListState[] = [];

  constructor(private _hashService: JobsHashService) {}

  public get isEmpty(): boolean {
    return this._states.length === 0;
  }

  public get states(): IJobsListState[] {
    return this._states;
  }

  public getMappedDataItem(jobLine: IJobLine): IJobsListStateItem {
    return {
      number: jobLine.number,
      hash: this._hashService.getLineHash(jobLine),
    };
  }

  public pushState(
    queryParams: JobsQueryParamDto,
    lines: IJobLine[],
    socket: Socket,
  ): void {
    this.removeSocket(socket);

    const state = this._getStateFromQueryParams(queryParams);

    if (state) {
      state.sockets.push(socket);
    } else {
      const data = lines.map((item) => this.getMappedDataItem(item));

      this._states.push({
        queryParams,
        data,
        sockets: [socket],
      });
    }
  }

  public removeSocket(socket: Socket): void {
    this._states = this._states.filter((state) => {
      state.sockets = state.sockets.filter((item) => item.id !== socket.id);

      return state.sockets.length > 0;
    });
  }

  private _getStateFromQueryParams(
    dto: JobsQueryParamDto,
  ): IJobsListState | null {
    const state = this._states.find(
      ({ queryParams }) => JSON.stringify(queryParams) === JSON.stringify(dto),
    );

    return state ? state : null;
  }
}
