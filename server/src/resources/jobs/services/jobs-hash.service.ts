import { Injectable } from '@nestjs/common';
import { createHash } from 'node:crypto';
import { ISyncElement } from '../../../shared/interfaces';
import { IJobPlanningCard } from '../interfaces';

@Injectable()
export class JobsHashService {
  public getLineHash(element: ISyncElement): string {
    const data = { ...element };
    delete data.syncAction;

    return this._getHash(data);
  }

  public getPlanningDataHash(cards: IJobPlanningCard[]): string {
    return this._getHash(cards);
  }

  private _getHash(data: any): string {
    return createHash('sha256').update(JSON.stringify(data)).digest('hex');
  }
}
