import { Injectable } from '@nestjs/common';
import { Socket } from 'socket.io';
import {
  IStockReservationLine,
  IStockReservationsState,
  IStockReservationsStateItem,
} from '../interfaces';
import { JobsHashService } from './jobs-hash.service';

@Injectable()
export class StockReservationsStateService {
  private _state: IStockReservationsState = { sockets: [], data: [] };

  constructor(private _hashService: JobsHashService) {}

  public get hasNoData(): boolean {
    return this._state.data.length === 0;
  }

  public get hasNoSocket(): boolean {
    return this._state.sockets.length === 0;
  }

  public get state(): IStockReservationsState {
    return this._state;
  }

  public pushState(data: IStockReservationLine[], socket: Socket): void {
    this.removeSocket(socket.id);

    if (!this._state.data.length) {
      this._state.data = data.map((line) => this.getMappedDataItem(line));
    }

    this._state.sockets.push(socket);
  }

  public removeSocket(socketId: string): void {
    this._state.sockets = this._state.sockets.filter(
      (item) => item.id !== socketId,
    );

    if (!this._state.sockets.length) {
      this._state.data = [];
    }
  }

  public getMappedDataItem(
    line: IStockReservationLine,
  ): IStockReservationsStateItem {
    return {
      jobNumber: line.jobNumber,
      hash: this._hashService.getLineHash(line),
    };
  }
}
