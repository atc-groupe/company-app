import { Injectable } from '@nestjs/common';
import { JobsGateway } from '../jobs.gateway';
import { JobPlanningDataService } from './job-planning-data.service';
import { JobsService } from './jobs.service';
import { firstValueFrom } from 'rxjs';
import { JobsStateService } from './jobs-state.service';
import { JobsHashService } from './jobs-hash.service';

@Injectable()
export class JobStateSyncService {
  constructor(
    private _statesService: JobsStateService,
    private _jobsService: JobsService,
    private _jobPlanningDataService: JobPlanningDataService,
    private _hashService: JobsHashService,
    private _gateway: JobsGateway,
  ) {}

  public async syncStates(): Promise<void> {
    if (this._statesService.isEmpty) {
      return;
    }

    for (const state of this._statesService.states) {
      const job = await firstValueFrom(
        this._jobsService.findOneByMpNumber(state.number),
      );

      const planningData = await this._jobPlanningDataService.getJobCards(job);
      job.planningData = planningData;
      const newHash = this._hashService.getPlanningDataHash(planningData);

      if (newHash !== state.planningDataHash) {
        state.planningDataHash = newHash;
        this._gateway.emitJobUpdate(state.sockets, job);
      }
    }
  }
}
