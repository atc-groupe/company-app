import { Injectable } from '@nestjs/common';
import { IJobPlanningCard, IJobsState } from '../interfaces';
import { Socket } from 'socket.io';
import { JobsHashService } from './jobs-hash.service';

@Injectable()
export class JobsStateService {
  private _states: IJobsState[] = [];
  constructor(private _hashService: JobsHashService) {}

  public get isEmpty(): boolean {
    return this._states.length === 0;
  }

  public get states(): IJobsState[] {
    return this._states;
  }

  public pushState(
    jobNumber: number,
    planningData: IJobPlanningCard[],
    socket: Socket,
  ): void {
    this.removeSocket(socket);

    const state = this._states.find((item) => item.number === jobNumber);

    if (state) {
      state.sockets.push(socket);
    } else {
      this._states.push({
        number: jobNumber,
        planningData,
        planningDataHash: this._hashService.getPlanningDataHash(planningData),
        sockets: [socket],
      });
    }
  }

  public removeSocket(socket: Socket): void {
    this._states = this._states.filter((state) => {
      state.sockets = state.sockets.filter((item) => item.id !== socket.id);

      return state.sockets.length > 0;
    });
  }
}
