import { Injectable } from '@nestjs/common';
import { JobsService } from './jobs.service';
import { StockReservationsLineMappingService } from '../mapping';
import { IStockReservationLine } from '../interfaces';
import { firstValueFrom } from 'rxjs';

@Injectable()
export class StockReservationsDataService {
  constructor(
    private _jobsService: JobsService,
    private _mappingService: StockReservationsLineMappingService,
  ) {}

  public async getReservationsLines(): Promise<IStockReservationLine[]> {
    const jobs = await firstValueFrom(
      this._jobsService.find({
        startStatusNumber: -1,
        endStatusNumber: 720,
        withStockReservation: true,
      }),
    );

    return jobs.map((job) => this._mappingService.getMappedData(job));
  }
}
