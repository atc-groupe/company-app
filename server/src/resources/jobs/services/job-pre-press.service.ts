import {
  HttpException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { JobPrePressRepository } from '../repositories';
import { JobPrePressUpdateDto } from '../dto';
import { catchError, map, Observable } from 'rxjs';
import { Job } from '../schemas';

@Injectable()
export class JobPrePressService {
  constructor(private _prePressRepository: JobPrePressRepository) {}

  public updatePrePress(
    id: string,
    dto: JobPrePressUpdateDto,
  ): Observable<Job> {
    return this._prePressRepository.updatePrePress(id, dto).pipe(
      map((job) => {
        if (!job) {
          throw new NotFoundException(`Le job n'a pas été trouvé'`);
        }

        return job;
      }),
      catchError((err) => {
        if (err instanceof HttpException) {
          throw err;
        }

        throw new InternalServerErrorException(
          'Une erreur est survenue lors de la mise à jour du job',
        );
      }),
    );
  }
}
