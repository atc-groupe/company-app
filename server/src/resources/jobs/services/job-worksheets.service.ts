import {
  HttpException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { JobWorksheetsRepository } from '../repositories';
import { JobDocument } from '../schemas';
import { catchError, map, Observable, switchMap } from 'rxjs';
import { JobWorksheetCreateDto, JobWorksheetUpdateDto } from '../dto';
import { MpEmployeeWorksheetRepository } from '../../../mp/mp-employees/mp-employee-worksheet.repository';
import { JobWorksheetMappingService } from '../mapping';

@Injectable()
export class JobWorksheetsService {
  constructor(
    private _mpWorksheetRepository: MpEmployeeWorksheetRepository,
    private _worksheetMapping: JobWorksheetMappingService,
    private _jobWorksheetsRepository: JobWorksheetsRepository,
  ) {}

  public addOne(
    jobId: string,
    dto: JobWorksheetCreateDto,
  ): Observable<JobDocument> {
    return this._mpWorksheetRepository.createWorksheet(dto).pipe(
      switchMap((id) => this._mpWorksheetRepository.getWorksheet(id)),
      switchMap((mpWorksheet) => {
        if (!mpWorksheet) {
          throw new InternalServerErrorException();
        }

        const mappedData = this._worksheetMapping.getMappedData(mpWorksheet);

        return this._jobWorksheetsRepository.addOne(jobId, mappedData);
      }),
      map((job) => {
        if (!job) {
          throw new NotFoundException("Le job n'a pas été trouvé");
        }

        return job;
      }),
      catchError((err) => {
        if (err instanceof HttpException) {
          throw err;
        }

        throw new InternalServerErrorException(
          'Impossible de créer la fiche de travail',
        );
      }),
    );
  }

  public updateOne(
    jobId: string,
    worksheetId: string,
    dto: JobWorksheetUpdateDto,
  ): Observable<JobDocument> {
    return this._mpWorksheetRepository.updateWorksheet(worksheetId, dto).pipe(
      switchMap(() => this._mpWorksheetRepository.getWorksheet(worksheetId)),
      switchMap((mpWorksheet) => {
        if (!mpWorksheet) {
          throw new InternalServerErrorException();
        }

        const mappedData = this._worksheetMapping.getMappedData(mpWorksheet);

        return this._jobWorksheetsRepository.updateOne(
          jobId,
          worksheetId,
          mappedData,
        );
      }),
      map((job) => {
        if (!job) {
          throw new NotFoundException("Le job n'a pas été trouvé");
        }

        return job;
      }),
      catchError((err) => {
        if (err instanceof HttpException) {
          throw err;
        }

        throw new InternalServerErrorException(
          'Impossible de modifier la fiche de travail',
        );
      }),
    );
  }

  public removeOne(
    jobId: string,
    worksheetId: string,
  ): Observable<JobDocument> {
    return this._mpWorksheetRepository.deleteWorksheet(worksheetId).pipe(
      switchMap(() =>
        this._jobWorksheetsRepository.removeOne(jobId, worksheetId),
      ),
      map((job) => {
        if (!job) {
          throw new NotFoundException("Le job n'a pas été trouvé");
        }

        return job;
      }),
      catchError((err) => {
        if (err instanceof HttpException) {
          throw err;
        }

        throw new InternalServerErrorException(
          'Impossible de supprimer la fiche de travail',
        );
      }),
    );
  }
}
