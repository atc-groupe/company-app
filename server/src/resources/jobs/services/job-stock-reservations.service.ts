import {
  HttpException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { JobStockReservationsRepository } from '../repositories';
import {
  JobStockReservationDto,
  JobStockReservationHandledDto,
  JobStockReservationStatusDto,
} from '../dto';
import { JobDocument } from '../schemas';
import { catchError, map, Observable, of, switchMap } from 'rxjs';
import { JobStockReservationStatusEnum } from '../enum';
import { StockMutationsService } from '../../stock/services';
import { ProductTypeEnum } from '../../../mp/mp-stock/enums';

@Injectable()
export class JobStockReservationsService {
  constructor(
    private _repository: JobStockReservationsRepository,
    private _stockMutationsService: StockMutationsService,
  ) {}

  public createOne(
    jobNumber: number,
    dto: JobStockReservationDto,
  ): Observable<JobDocument> {
    return this._repository.createOne(jobNumber, dto).pipe(
      map((job) => {
        if (!job) {
          throw new NotFoundException('Job introuvable');
        }

        return job;
      }),
      catchError((err) => {
        console.log(err);
        if (err instanceof HttpException) {
          throw err;
        }

        throw new InternalServerErrorException(
          `'Une erreur est survenue lors de l'ajout de la résa matière`,
        );
      }),
    );
  }

  public updateOne(
    jobNumber: number,
    reservationId: string,
    dto: JobStockReservationDto,
  ): Observable<JobDocument> {
    return this._repository.updateOne(jobNumber, reservationId, dto).pipe(
      map((job) => {
        if (!job) {
          throw new NotFoundException('Job introuvable');
        }

        return job;
      }),
      catchError((err) => {
        if (err instanceof HttpException) {
          throw err;
        }

        throw new InternalServerErrorException(
          `'Une erreur est survenue lors de la mise à jour de la résa matière`,
        );
      }),
    );
  }

  public changeStatus(
    jobNumber: number,
    reservationId: string,
    status: JobStockReservationStatusEnum,
    updateUsername: string,
  ): Observable<JobDocument> {
    return this._repository
      .changeStatus(jobNumber, reservationId, status, updateUsername)
      .pipe(
        map((job) => {
          if (!job) {
            throw new NotFoundException('Job introuvable');
          }

          return job;
        }),
        catchError((err) => {
          if (err instanceof HttpException) {
            throw err;
          }

          throw new InternalServerErrorException(
            `'Une erreur est survenue lors de la mise à jour de la résa matière`,
          );
        }),
      );
  }

  public changeAllStatuses(
    jobNumber: number,
    dto: JobStockReservationStatusDto,
  ): Observable<JobDocument> {
    return this._repository.changeAllStatuses(jobNumber, dto).pipe(
      map((job) => {
        if (!job) {
          throw new NotFoundException('Job introuvable');
        }

        return job;
      }),
      catchError((err) => {
        if (err instanceof HttpException) {
          throw err;
        }

        throw new InternalServerErrorException(
          `'Une erreur est survenue lors de la mise à jour de la résa matière`,
        );
      }),
    );
  }

  public removeOne(
    jobNumber: number,
    reservationId: string,
  ): Observable<JobDocument> {
    return this._repository.removeOne(jobNumber, reservationId).pipe(
      map((job) => {
        if (!job) {
          throw new NotFoundException('Job introuvable');
        }

        return job;
      }),
      catchError((err) => {
        if (err instanceof HttpException) {
          throw err;
        }

        throw new InternalServerErrorException(
          `'Une erreur est survenue lors de la suppression de la résa matière`,
        );
      }),
    );
  }

  public removeAll(jobNumber: number): Observable<JobDocument> {
    return this._repository.removeAll(jobNumber).pipe(
      map((job) => {
        if (!job) {
          throw new NotFoundException('Job introuvable');
        }

        return job;
      }),
      catchError((err) => {
        if (err instanceof HttpException) {
          throw err;
        }

        throw new InternalServerErrorException(
          `'Une erreur est survenue lors de la suppression des résa matières`,
        );
      }),
    );
  }

  public handleOne(
    jobNumber: number,
    reservationId: string,
    dto: JobStockReservationHandledDto,
  ): Observable<JobDocument> {
    const actionMessage = dto.action === 'create' ? 'Traitement' : 'Annulation';

    const action$: Observable<any> =
      dto.quantity === 0
        ? of(true)
        : this._stockMutationsService.createMutation(
            ProductTypeEnum.Paper,
            dto.paperId,
            {
              employee_number: dto.user.mp.employeeNumber,
              data: {
                job_number: jobNumber,
                description: `${actionMessage} résa matière par ${dto.user.ad.firstName} ${dto.user.ad.lastName}`,
                add: dto.action === 'cancel' ? dto.quantity : 0,
                subtract: dto.action === 'create' ? dto.quantity : 0,
              },
            },
          );

    return action$.pipe(
      switchMap(() => {
        return this._repository.handleOne(jobNumber, reservationId, dto);
      }),
      map((job) => {
        if (!job) {
          throw new NotFoundException('Job introuvable');
        }

        return job;
      }),
      catchError((err) => {
        if (err instanceof HttpException) {
          throw err;
        }

        throw new InternalServerErrorException(
          'Une erreur est survenue pendant le traitement',
        );
      }),
    );
  }
}
