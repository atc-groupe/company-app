import {
  HttpException,
  Injectable,
  InternalServerErrorException,
  LoggerService,
  NotFoundException,
} from '@nestjs/common';
import { catchError, forkJoin, map, Observable, of, switchMap } from 'rxjs';
import { IJobMappedStatus, IJobStatus } from '../interfaces';
import { MpSystemPullDownListRepository } from '../../../mp/mp-system/mp-system-pull-down-list.repository';
import { MpPullDownListEnum } from '../../../mp/mp-system/enum';
import { JobFindRepository, JobRepository } from '../repositories';
import { JobsQueryParamDto, JobsStatusChangeDto } from '../dto';
import { MpChangeJobStatusRepository } from '../../../mp/mp-jobs/repositories';
import { JobDocument } from '../schemas';
import { LoggerFactory } from '../../../shared/factories/logger.factory';
import { LogContextEnum } from '../../../shared/enum';
import { SettingsService } from '../../settings/services';

@Injectable()
export class JobsService {
  private _logger: LoggerService;

  constructor(
    loggerFactory: LoggerFactory,
    private _mpPullDownListRepository: MpSystemPullDownListRepository,
    private _jobRepository: JobRepository,
    private _jobFindRepository: JobFindRepository,
    private _mpJobStatusRepository: MpChangeJobStatusRepository,
    private _settingsService: SettingsService,
  ) {
    this._logger = loggerFactory.get(LogContextEnum.jobs);
  }

  public find(queryParams: JobsQueryParamDto): Observable<JobDocument[]> {
    return this._jobFindRepository.find(queryParams).pipe(
      catchError((err) => {
        const message =
          'Une erreur est survenue lors de la récupération des jobs';
        this._logError(message, err);

        throw new InternalServerErrorException(message);
      }),
    );
  }

  public findByMpNumbers(numbers: number[]): Observable<JobDocument[]> {
    return this._jobFindRepository.findByNumbers(numbers).pipe(
      catchError((err) => {
        const message =
          'Une erreur est survenue lors de la récupération des jobs';
        this._logError(message, err);

        throw new InternalServerErrorException(message);
      }),
    );
  }

  public findOneById(id: string): Observable<JobDocument> {
    return this._jobRepository.findOneById(id).pipe(
      map((job) => {
        if (!job) {
          throw new NotFoundException(`Aucun job trouvé avec l'ID' ${id}`);
        }

        return job;
      }),

      catchError((err) => {
        const message = `Une erreur est survenue pendant la lecture du job ID ${id}`;
        this._logError(message, err);

        if (err instanceof HttpException) {
          throw err;
        }

        throw new InternalServerErrorException(
          `Une erreur est survenue pendant la lecture du job ID ${id}`,
        );
      }),
    );
  }

  public findOneByMpNumber(mpNumber: number): Observable<JobDocument> {
    return this._jobRepository.findOneByMpNumber(mpNumber).pipe(
      map((job) => {
        if (!job) {
          throw new NotFoundException(
            `Aucun job trouvé avec le numéro ${mpNumber}`,
          );
        }

        return job;
      }),

      catchError((err) => {
        const message = `Une erreur est survenue pendant la lecture du job ${mpNumber}`;
        this._logError(message, err);

        if (err instanceof HttpException) {
          throw err;
        }

        throw new InternalServerErrorException(
          `Une erreur est survenue pendant la lecture du job ${mpNumber}`,
        );
      }),
    );
  }

  public getStatusesList(): Observable<IJobStatus[]> {
    return this._mpPullDownListRepository.getList(MpPullDownListEnum.Job).pipe(
      map((list) => {
        return list.values.map((item) => {
          return {
            number: item.ordering,
            label: item.text,
          };
        });
      }),
      catchError((err) => {
        const message =
          'Une erreur est survenue lors de la récupération de la liste de statuts de jobs';
        this._logError(message, err);

        throw new InternalServerErrorException(message);
      }),
    );
  }

  public getMappedStatuses(): Observable<IJobMappedStatus[]> {
    const settings$ = this._settingsService.findUnique();
    const statuses$ = this.getStatusesList();

    return forkJoin([settings$, statuses$]).pipe(
      map(([settings, statuses]) => {
        return statuses.map((status) => {
          const mappedStatus = settings.job.statuses.items.find(
            (item) => item.statusNumber === status.number,
          );

          return mappedStatus
            ? mappedStatus
            : {
                statusNumber: status.number,
                label: status.label,
                textColor: 'black',
                bgColor: settings.job.statuses.defaultColor,
              };
        });
      }),
    );
  }

  public changeStatus(
    mpNumber: number,
    dto: JobsStatusChangeDto,
  ): Observable<unknown> {
    return this._mpJobStatusRepository.changeStatus(mpNumber, dto).pipe(
      switchMap((res: { result: string }) => {
        if (res.result === 'succes') {
          console.log('update status yep !!!');
          return this._jobRepository.updateStatus(mpNumber, dto.statusNumber);
        }

        return of(res);
      }),
      catchError((err) => {
        const message = `Une erreur est survenue lors du changement de statut du job n°${mpNumber}`;
        this._logError(message, err);

        if (err instanceof HttpException) {
          throw err;
        }

        throw new InternalServerErrorException(message);
      }),
    );
  }

  private _logError(message: string, err: any) {
    if (err.message) {
      message = `${message}. Message: ${err.message}`;
    }

    this._logger.error(`[JobService] ${message}`);
  }
}
