import { Injectable } from '@nestjs/common';
import { MpPlanningGetLinesRepository } from '../../../mp/mp-planning/mp-planning-get-lines.repository';
import { JobDocument } from '../schemas';
import { firstValueFrom } from 'rxjs';
import { IMpPlanningLine } from '../../../mp/mp-planning/interfaces';
import { IJobPlanningCard } from '../interfaces';
import { JobPlanningCardMappingService } from '../mapping';

@Injectable()
export class JobPlanningDataService {
  private _departments: number[] = [0, 2, 3];
  constructor(
    private _mpPlanningLinesRepository: MpPlanningGetLinesRepository,
    private _mappingService: JobPlanningCardMappingService,
  ) {}

  public async getJobCards(job: JobDocument): Promise<IJobPlanningCard[]> {
    const jobLines: IMpPlanningLine[] = [];
    try {
      const maxDates = this._getPlanningLinesSearchDates();
      for (const department of this._departments) {
        const departmentLines = await firstValueFrom(
          this._mpPlanningLinesRepository.getLines({
            startdate: maxDates.startDate,
            stopdate: maxDates.endDate,
            department,
          }),
        );

        jobLines.push(
          ...departmentLines.filter(
            (item) =>
              job.mp.number ===
              this._getJobNumberFromLineDescription(item.description),
          ),
        );
      }

      if (!jobLines.length) {
        return [];
      }

      const uniqueJobLines = jobLines.reduce((acc: IMpPlanningLine[], line) => {
        if (acc.find((item) => item.id === line.id) === undefined) {
          acc.push(line);
        }

        return acc;
      }, []);

      const cards: IJobPlanningCard[] = [];
      for (const line of uniqueJobLines) {
        const mpCard = await firstValueFrom(
          this._mpPlanningLinesRepository.getCard(line.id),
        );

        if (!mpCard) {
          continue;
        }

        cards.push(this._mappingService.getMappedCard(line.id, mpCard, job));
      }

      return cards;
    } catch {
      return [];
    }
  }

  private _getPlanningLinesSearchDates(): {
    startDate: string;
    endDate: string;
  } {
    const startDate = new Date();
    const interval = 183;
    startDate.setDate(startDate.getDate() - interval);

    const endDate = new Date();
    endDate.setDate(endDate.getDate() + interval);

    return {
      startDate: startDate.toLocaleDateString('fr-FR'),
      endDate: endDate.toLocaleDateString('fr-FR'),
    };
  }

  private _getJobNumberFromLineDescription(description: string): number {
    return parseInt(description.split(' ')[0]);
  }
}
