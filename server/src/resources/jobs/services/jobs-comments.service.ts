import {
  HttpException,
  Injectable,
  InternalServerErrorException,
  LoggerService,
  NotFoundException,
} from '@nestjs/common';
import { JobCommentsRepository } from '../repositories';
import { catchError, map, Observable } from 'rxjs';
import { JobDeliveryCommentDto, JobPlanningCommentCreateDto } from '../dto';
import { Job } from '../schemas';
import { LoggerFactory } from '../../../shared/factories/logger.factory';
import { LogContextEnum } from '../../../shared/enum';

@Injectable()
export class JobsCommentsService {
  private _logger: LoggerService;

  constructor(
    loggerFactory: LoggerFactory,
    private _commentsRepository: JobCommentsRepository,
  ) {
    this._logger = loggerFactory.get(LogContextEnum.jobs);
  }

  public updateDelivery(
    id: string,
    dto: JobDeliveryCommentDto,
  ): Observable<Job> {
    return this._commentsRepository.updateDelivery(id, dto.comment).pipe(
      map((job) => {
        if (!job) {
          throw new NotFoundException(`Job _id:${id} introuvable`);
        }

        return job;
      }),
      catchError((err) => {
        const message = `Une erreur est survenue lors de la mise à jour du commentaire d'expéditions. _id: ${id}`;
        this._logError(message, err);

        if (err instanceof HttpException) {
          throw err;
        }

        throw new InternalServerErrorException(message);
      }),
    );
  }

  public addPlanningComment(
    id: string,
    dto: JobPlanningCommentCreateDto,
  ): Observable<Job> {
    return this._commentsRepository.addPlanningComment(id, dto).pipe(
      map((job) => {
        if (!job) {
          throw new NotFoundException(`Job _id:${id} introuvable`);
        }

        return job;
      }),
      catchError((err) => {
        const message = `Une erreur est survenue lors de l'enregistrement du commentaire. _id: ${id}`;
        this._logError(message, err);

        if (err instanceof HttpException) {
          throw err;
        }

        throw new InternalServerErrorException(message);
      }),
    );
  }

  public updatePlanningComment(
    id: string,
    commentId: string,
    comment: string,
  ): Observable<Job> {
    return this._commentsRepository
      .updatePlanningComment(id, commentId, comment)
      .pipe(
        map((job) => {
          if (!job) {
            throw new NotFoundException(`Job _id:${id} introuvable`);
          }

          return job;
        }),
        catchError((err) => {
          const message = `Une erreur est survenue lors de la modification du commentaire. _id: ${id}`;
          this._logError(message, err);

          if (err instanceof HttpException) {
            throw err;
          }

          throw new InternalServerErrorException(message);
        }),
      );
  }

  public removePlanningComment(id: string, commentId: string): Observable<Job> {
    return this._commentsRepository.removePlanningComment(id, commentId).pipe(
      map((job) => {
        if (!job) {
          throw new NotFoundException(`Job _id:${id} introuvable`);
        }

        return job;
      }),
      catchError((err) => {
        const message = `Une erreur est survenue lors de la suppression du commentaire. _id: ${id}`;
        this._logError(message, err);

        if (err instanceof HttpException) {
          throw err;
        }

        throw new InternalServerErrorException(message);
      }),
    );
  }

  private _logError(message: string, err?: any) {
    if (err?.message) {
      message = `${message}. Message: ${err.message}`;
    }

    this._logger.error(`[JobCommentService] ${message}`);
  }
}
