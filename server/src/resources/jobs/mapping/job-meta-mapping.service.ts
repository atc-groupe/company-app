import { Injectable } from '@nestjs/common';
import { Settings } from '../../settings/schemas';
import { DeliveryAddress, JobMeta, SubJob } from '../schemas';

@Injectable()
export class JobMetaMappingService {
  private _settings: Settings;

  getMappedData(
    subJobs: SubJob[] | null,
    addresses: DeliveryAddress[] | null,
    settings: Settings,
  ): JobMeta {
    this._settings = settings;

    const meta: JobMeta = {
      printingSurface: 0,
      modelCount: 0,
      manufacturedPiecesCount: 0,
      productionTypes: [],
      deliveryServiceFinishes: [],
      hasSheetProduction: false,
      hasNumericScoreCutting: false,
      hasZundCutting: false,
      hasKits: false,
      hasSubcontractingProduction: false,
      addressCount: 0,
      addressZipCodes: [],
      deliveryMethods: [],
      deliverySendingDates: [],
    };

    if (subJobs?.length) {
      meta.modelCount += subJobs.length;

      subJobs.forEach((subJob) => {
        if (subJob.meta) {
          meta.printingSurface += subJob.meta.surface;

          subJob.prodLayers?.forEach((prodLayer) => {
            const mappedDeviceName = this._getMappedDeviceName(
              prodLayer.numeric.device,
            );

            if (!meta.productionTypes.includes(mappedDeviceName)) {
              meta.productionTypes.push(mappedDeviceName);
            }
          });

          subJob.meta.deliveryServiceFinishes.forEach((item) => {
            if (!meta.deliveryServiceFinishes.includes(item)) {
              meta.deliveryServiceFinishes.push(item);
            }
          });

          if (subJob.meta.hasSheetPrinting) {
            meta.hasSheetProduction = true;
          }

          if (subJob.meta.hasNumericScoreCutting) {
            meta.hasNumericScoreCutting = true;
          }

          if (subJob.meta.hasZundCutting) {
            meta.hasZundCutting = true;
          }

          if (subJob.meta.hasKits) {
            meta.hasKits = true;
          }
        }

        if (
          !meta.hasSubcontractingProduction &&
          subJob.subcontractors?.length &&
          subJob.subcontractors.some((sub) =>
            this._hasProductionSubcontracting(sub.relation),
          )
        ) {
          meta.hasSubcontractingProduction = true;
        }

        meta.manufacturedPiecesCount += subJob.mp.quantity;
      });
    }

    if (addresses) {
      addresses.forEach((address) => {
        if (this._isShippingDeliveryMethod(address.mp.deliveryMethod)) {
          meta.addressCount += 1;
        }

        if (!meta.addressZipCodes.includes(address.mp.zipCode)) {
          meta.addressZipCodes.push(address.mp.zipCode);
        }

        if (!meta.deliveryMethods.includes(address.mp.deliveryMethod)) {
          meta.deliveryMethods.push(address.mp.deliveryMethod);
        }

        if (!meta.deliverySendingDates.includes(address.mp.sendingDate)) {
          meta.deliverySendingDates.push(address.mp.sendingDate);
        }
      });
    }

    return meta;
  }

  private _getMappedDeviceName(device: string): string {
    const mappedDeviceName = this._settings.devices.mapping.names.find((item) =>
      item.mpName.toLowerCase().trim().includes(device.toLowerCase().trim()),
    );

    return mappedDeviceName ? mappedDeviceName.displayName : device;
  }

  private _isShippingDeliveryMethod(deliveryMethod: string): boolean {
    return this._settings.job.mapping.metaFilters.shippingDeliveryMethods.items.includes(
      deliveryMethod,
    );
  }

  private _hasProductionSubcontracting(subcontractorName: string): boolean {
    return !this._settings.job.mapping.metaFilters.skippedSubcontractors.items.includes(
      subcontractorName,
    );
  }
}
