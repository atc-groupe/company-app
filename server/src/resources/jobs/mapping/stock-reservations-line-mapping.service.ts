import { Injectable } from '@nestjs/common';
import { JobDocument, JobStockReservation } from '../schemas';
import { IStockReservationLine } from '../interfaces';
import { JobStockReservationStatusEnum } from '../enum';

@Injectable()
export class StockReservationsLineMappingService {
  public getMappedData(job: JobDocument): IStockReservationLine {
    if (job.stockReservations === null) {
      throw new Error('reservations cannot be null');
    }

    return {
      jobNumber: job.mp.number,
      jobStatusNumber: job.mp.statusNumber,
      company: job.mp.company,
      sendingDate: job.mp.dates.sending,
      globalStatus: this._getGlobalStatus(job.stockReservations),
      itemsCount: job.stockReservations.length,
    };
  }

  private _getGlobalStatus(
    reservations: JobStockReservation[],
  ): JobStockReservationStatusEnum {
    if (
      reservations.every(
        (item) => item.status === JobStockReservationStatusEnum.Brouillon,
      )
    ) {
      return JobStockReservationStatusEnum.Brouillon;
    }

    if (
      reservations.every(
        (item) => item.status === JobStockReservationStatusEnum.ATraiter,
      )
    ) {
      return JobStockReservationStatusEnum.ATraiter;
    }

    if (
      reservations.every(
        (item) => item.status === JobStockReservationStatusEnum.EnCours,
      )
    ) {
      return JobStockReservationStatusEnum.EnCours;
    }

    if (
      reservations.every(
        (item) =>
          item.status === JobStockReservationStatusEnum.AttenteLivraison,
      )
    ) {
      return JobStockReservationStatusEnum.AttenteLivraison;
    }

    if (
      reservations.every(
        (item) => item.status === JobStockReservationStatusEnum.Traitee,
      )
    ) {
      return JobStockReservationStatusEnum.Traitee;
    }

    return JobStockReservationStatusEnum.EnCours;
  }
}
