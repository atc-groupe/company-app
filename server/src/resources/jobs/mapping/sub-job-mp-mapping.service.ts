import { Injectable } from '@nestjs/common';
import { IMpSubJobQuotation } from '../../../mp/mp-jobs/interfaces';
import { SubJobMp } from '../schemas';

@Injectable()
export class SubJobMpMappingService {
  public getMappedItem(id: number, info: IMpSubJobQuotation): SubJobMp {
    return {
      id,
      productNumber: info.product_number,
      productType: info.product_type,
      articleNumber: info.article_number,
      articleType: info.article_type,
      description: info.description,
      quantity: info.run_01,
      deliveredQuantity: info.delivered_runs,
      weight: info.netto_weight_1,
      paoRemark: this._getParserString(info.internal_remark_1),
      paoCheckRemark: this._getParserString(info.internal_remark_2),
      checklist: {
        description: this._getParsedStringOrNull(info.text_01),
        format: this._getParsedStringOrNull(info.text_02),
        pao: this._getParsedStringOrNull(info.text_03),
        print: this._getParsedStringOrNull(info.text_04),
        material: this._getParsedStringOrNull(info.text_05),
        finishing: this._getParsedStringOrNull(info.text_06),
        packaging: this._getParsedStringOrNull(info.text_07),
        delivery: this._getParsedStringOrNull(info.text_08),
        application: this._getParsedStringOrNull(info.text_09),
        remark: this._getParsedStringOrNull(info.text_12),
      },
    };
  }

  private _getParserString(text: string): string {
    return text.replaceAll('\r', '\n');
  }

  private _getParsedStringOrNull(text?: string): string | null {
    return text ? this._getParserString(text) : null;
  }
}
