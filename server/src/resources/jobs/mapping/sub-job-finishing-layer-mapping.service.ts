import { Injectable } from '@nestjs/common';
import {
  IMpSubJobLaminationFinishing,
  IMpSubJobLargeFormatFinishing,
} from '../../../mp/mp-workflow/interfaces';
import { SubJobLargeFormatFinishingMappingService } from './sub-job-large-format-finishing-mapping.service';
import { SubJobLaminationFinishingLayerMappingService } from './sub-job-lamination-finishing-layer-mapping.service';
import { MpSubJobFinishingTypeEnum } from '../../../mp/mp-workflow/enums/mp-sub-job-finishing-type.enum';
import { Logger } from '../../../shared/services';
import { LogContextEnum } from '../../../shared/enum';
import { MappingProcessService } from './mapping-process.service';
import { LoggerFactory } from '../../../shared/factories/logger.factory';
import { SubJobFinishingLayer } from '../schemas';

@Injectable()
export class SubJobFinishingLayerMappingService {
  private _logger: Logger;

  constructor(
    loggerFactory: LoggerFactory,
    private _mappingProcess: MappingProcessService,
    private _largeFormatMapping: SubJobLargeFormatFinishingMappingService,
    private _laminationMapping: SubJobLaminationFinishingLayerMappingService,
  ) {
    this._logger = loggerFactory.get(LogContextEnum.jobSync);
  }

  public getMappedCollection(
    data: (IMpSubJobLaminationFinishing | IMpSubJobLargeFormatFinishing)[],
    numericId?: number,
  ): SubJobFinishingLayer[] | null {
    const layers = data
      .filter((item) => {
        if (numericId && item.reference?.unique === numericId) {
          return true;
        }

        if (!numericId && !item.reference) {
          this._logger.warn(
            `Finishing layer has no reference to numeric layer. Job id: ${this._mappingProcess.processingJobMpId}, subJob id: ${this._mappingProcess.processingSubJobMpId}`,
          );
          return true;
        }

        return false;
      })
      .map((item): SubJobFinishingLayer => {
        switch (item.type) {
          case MpSubJobFinishingTypeEnum.largeFormat:
            return this._largeFormatMapping.getMappedItem(item);
          case MpSubJobFinishingTypeEnum.lamination:
            return this._laminationMapping.getMappedItem(item);
        }
      });

    return layers.length ? layers : null;
  }
}
