import { Injectable } from '@nestjs/common';
import { SettingsJobMappingMetaFilters } from '../../settings/schemas';
import {
  SubJobFinishingLayer,
  SubJobMeta,
  SubJobNumericLayer,
  SubJobProdLayer,
} from '../schemas';

@Injectable()
export class SubJobMetaMappingService {
  private _metaFilters: SettingsJobMappingMetaFilters;

  public getMappedData(
    prodLayers: SubJobProdLayer[],
    metaFilters: SettingsJobMappingMetaFilters,
  ): SubJobMeta {
    this._metaFilters = metaFilters;

    const initObject: SubJobMeta = {
      surface: 0,
      hasSheetPrinting: false,
      deliveryServiceFinishes: [],
      hasKits: false,
      hasNumericScoreCutting: false,
      hasZundCutting: false,
    };

    return prodLayers.reduce((acc: SubJobMeta, { numeric, finishes }) => {
      acc.surface += this.getPrintingSurface(numeric);

      if (this._hasSheetPrinting(numeric)) {
        acc.hasSheetPrinting = true;
      }

      if (!finishes) {
        return acc;
      }

      const deliveryFinishes = this._getDeliveryFinishes(finishes);
      deliveryFinishes.forEach((item) => {
        if (!acc.deliveryServiceFinishes.includes(item)) {
          acc.deliveryServiceFinishes.push(item);
        }
      });

      if (this._hasKitFinishing(finishes)) {
        acc.hasKits = true;
      }

      if (this._hasNumericScoreCutting(finishes)) {
        acc.hasNumericScoreCutting = true;
      }

      if (this._hasZundFinishing(finishes)) {
        acc.hasZundCutting = true;
      }

      return acc;
    }, initObject);
  }

  private getPrintingSurface(numeric: SubJobNumericLayer): number {
    if (this._isMaterialDevice(numeric.device)) {
      return 0;
    }

    return (numeric.width / 1000) * (numeric.height / 1000);
  }

  private _hasSheetPrinting(numeric: SubJobNumericLayer): boolean {
    if (this._isMaterialDevice(numeric.device)) {
      return true;
    }

    return !numeric.roll;
  }

  private _getDeliveryFinishes(finishes: SubJobFinishingLayer[]): string[] {
    return finishes.reduce((acc: string[], layer) => {
      const operationName = layer.operation.name;

      if (
        this._isDeliveryFinishing(layer.operation.name) &&
        !acc.includes(operationName)
      ) {
        acc.push(operationName);
      }

      return acc;
    }, []);
  }

  private _hasNumericScoreCutting(finishes: SubJobFinishingLayer[]): boolean {
    return finishes.some((finishing) =>
      this._isScoreCuttingFinishing(finishing.operation.name),
    );
  }

  private _hasKitFinishing(finishes: SubJobFinishingLayer[]): boolean {
    return finishes.some((finishing) =>
      this._isKitFinishing(finishing.operation.name),
    );
  }

  private _hasZundFinishing(finishes: SubJobFinishingLayer[]): boolean {
    return finishes.some((finishing) =>
      this._isZundCutting(finishing.operation.name),
    );
  }

  private _isMaterialDevice(device: string): boolean {
    return this._metaFilters.materialDevices.items.includes(device);
  }

  private _isDeliveryFinishing(operationName: string): boolean {
    return this._metaFilters.deliveryServiceFinishes.items.includes(
      operationName,
    );
  }

  private _isKitFinishing(operationName: string): boolean {
    return this._metaFilters.kitFinishes.items.includes(operationName);
  }

  private _isScoreCuttingFinishing(operationName: string): boolean {
    return this._metaFilters.scoreFinishes.items.includes(operationName);
  }

  private _isZundCutting(operationName: string): boolean {
    return this._metaFilters.zundCuttingFinishes.items.includes(operationName);
  }
}
