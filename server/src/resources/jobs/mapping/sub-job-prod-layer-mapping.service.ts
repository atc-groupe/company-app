import { Injectable } from '@nestjs/common';
import { IMpSubJobDetailInfo } from '../../../mp/mp-workflow/interfaces';
import { SubJobNumericLayerMappingService } from './sub-job-numeric-layer-mapping.service';
import { SubJobFinishingLayerMappingService } from './sub-job-finishing-layer-mapping.service';
import { SubJobProdLayer } from '../schemas';
import { SettingsDeviceMappingName } from '../../settings/schemas';

@Injectable()
export class SubJobProdLayerMappingService {
  constructor(
    private _numericMapping: SubJobNumericLayerMappingService,
    private _finishesMapping: SubJobFinishingLayerMappingService,
  ) {}
  public getMappedCollection(
    data: IMpSubJobDetailInfo,
    deviceMappingNames: SettingsDeviceMappingName[],
  ): SubJobProdLayer[] | null {
    const printingSheets = data.printingsheets;
    const finishing = data.finishing;

    if (!printingSheets.length) {
      return null;
    }

    return printingSheets.reduce((acc: SubJobProdLayer[], printingSheet) => {
      acc.push({
        numeric: this._numericMapping.getMappedItem(
          printingSheet,
          deviceMappingNames,
        ),
        finishes: this._finishesMapping.getMappedCollection(
          finishing,
          printingSheet.unique,
        ),
      });
      return acc;
    }, []);
  }
}
