import { Injectable } from '@nestjs/common';
import { JobOperation } from '../schemas';
import { IMpPlanningJobOperation } from '../../../mp/mp-planning/interfaces/i-mp-planning-job-operation';
import { DateHelper } from '../../../shared/services';

@Injectable()
export class JobOperationMappingService {
  constructor(private _helper: DateHelper) {}

  public getMappedCollection(
    mpOperations: IMpPlanningJobOperation[],
  ): JobOperation[] | null {
    if (!mpOperations.length) {
      return [];
    }

    return mpOperations
      .map((mpOperation) => {
        return {
          operation: mpOperation.operation,
          startDate: this._helper.getDateOrNull(mpOperation.start_date),
          department: mpOperation.department,
          calculatedTime: mpOperation.calculated_time,
          productionTime: mpOperation.production_time,
          ordering: mpOperation.ordering,
          employee: mpOperation.employee,
          information: mpOperation.information,
          extraInfo: mpOperation.extra_info,
          machine: mpOperation.machine,
          machineId: mpOperation.machine_id,
        };
      })
      .sort((a, b) => (a.ordering < b.ordering ? -1 : 1));
  }
}
