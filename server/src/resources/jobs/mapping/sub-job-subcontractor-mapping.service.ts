import { Injectable } from '@nestjs/common';
import { IMpSubJobSubcontractor } from '../../../mp/mp-workflow/interfaces';
import { DateHelper } from '../../../shared/services';
import { SubJobSubcontractor, SubJobSubcontractorItemMp } from '../schemas';

@Injectable()
export class SubJobSubcontractorMappingService {
  constructor(private _dateHelper: DateHelper) {}

  public getMappedCollection(
    data: IMpSubJobSubcontractor[],
  ): SubJobSubcontractor[] {
    return data.reduce((acc: SubJobSubcontractor[], item) => {
      const subcontractor = this._getSubcontractor(acc, item);
      const mappedItemMp = this._getMappedItemMp(item);

      if (subcontractor) {
        subcontractor.items.push({
          mp: mappedItemMp,
        });
      } else {
        const mappedSubcontractor = this._getMappedSubcontractor(item);
        mappedSubcontractor.items.push({ mp: mappedItemMp });
        acc.push(mappedSubcontractor);
      }
      return acc;
    }, []);
  }

  private _getMappedSubcontractor(
    data: IMpSubJobSubcontractor,
  ): SubJobSubcontractor {
    return {
      relation: data.relation,
      relationNumber: data.relation_number,
      items: [],
    };
  }

  private _getSubcontractor(
    subcontractors: SubJobSubcontractor[],
    mpSubcontractor: IMpSubJobSubcontractor,
  ): SubJobSubcontractor | null {
    if (!subcontractors.length) {
      return null;
    }

    const subcontractor = subcontractors.find(
      (item) => item.relationNumber === mpSubcontractor.relation_number,
    );

    return subcontractor ? subcontractor : null;
  }

  private _getMappedItemMp(
    data: IMpSubJobSubcontractor,
  ): SubJobSubcontractorItemMp {
    return {
      contactName: data.contact_name,
      description: data.description,
      remark: data.remark,
      quantity: data.run,
      ordered: data.ordered !== 'NON',
      sendingDate: this._dateHelper.getDateOrNull(data.sending_date),
      deliveryDate: this._dateHelper.getDateOrNull(data.delivery_date),
    };
  }
}
