import { Injectable } from '@nestjs/common';
import { IMpSubJobLargeFormatFinishing } from '../../../mp/mp-workflow/interfaces';
import { MpSubJobFinishingTypeEnum } from '../../../mp/mp-workflow/enums/mp-sub-job-finishing-type.enum';
import {
  FinishingLayerMaterial,
  FinishingLayerOperation,
  SubJobFinishingLayer,
} from '../schemas';

@Injectable()
export class SubJobLargeFormatFinishingMappingService {
  public getMappedItem(
    data: IMpSubJobLargeFormatFinishing,
  ): SubJobFinishingLayer {
    const operation = this._getMappedOperation(data);
    const material = this._getMappedMaterial(data);

    return {
      finishingType: MpSubJobFinishingTypeEnum.largeFormat,
      operation,
      sides: {
        left: data.sides.left,
        right: data.sides.right,
        top: data.sides.top,
        bottom: data.sides.bottom,
      },
      material,
      displayValue: this._getDisplayValue(operation, material),
    };
  }

  private _getMappedOperation(
    data: IMpSubJobLargeFormatFinishing,
  ): FinishingLayerOperation {
    return {
      name: this._getParsedType(data.operation.name),
      type: this._getParsedType(data.operation.type),
    };
  }

  private _getMappedMaterial(
    data: IMpSubJobLargeFormatFinishing,
  ): FinishingLayerMaterial | null {
    if (!data.material) {
      return null;
    }

    return {
      id: data.material.id,
      name: data.material.name,
    };
  }

  private _getParsedType(type: string): string {
    return type.replace(/^[0-9]+\./, '');
  }

  private _getDisplayValue(
    operation: FinishingLayerOperation,
    material: FinishingLayerMaterial | null,
  ): string {
    return `${operation.name} > ${operation.type} ${material ? ' > ' + material.name : ''}`;
  }
}
