import { Injectable } from '@nestjs/common';

@Injectable()
export class MappingProcessService {
  private _processingJobMpId: number | null;
  private _processingSubJobMpId: number | null;
  private _processingJobId: string | null;

  get processingJobMpId(): number | null {
    return this._processingJobMpId;
  }

  set processingJobMpId(value: number | null) {
    this._processingJobMpId = value;
  }

  get processingSubJobMpId(): number | null {
    return this._processingSubJobMpId;
  }

  set processingSubJobMpId(value: number | null) {
    this._processingSubJobMpId = value;
  }

  get processingJobId(): string | null {
    return this._processingJobId;
  }

  set processingJobId(value: string | null) {
    this._processingJobId = value;
  }

  public clearData(): void {
    this._processingJobId = null;
    this._processingJobMpId = null;
    this._processingSubJobMpId = null;
  }
}
