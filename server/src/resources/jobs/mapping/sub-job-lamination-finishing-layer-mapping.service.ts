import { Injectable } from '@nestjs/common';
import { IMpSubJobLaminationFinishing } from '../../../mp/mp-workflow/interfaces';
import { MpSubJobFinishingTypeEnum } from '../../../mp/mp-workflow/enums/mp-sub-job-finishing-type.enum';
import { Logger } from '../../../shared/services';
import { LogContextEnum } from '../../../shared/enum';
import { MappingProcessService } from './mapping-process.service';
import { LoggerFactory } from '../../../shared/factories/logger.factory';
import { FinishingLayerMaterial, SubJobFinishingLayer } from '../schemas';

@Injectable()
export class SubJobLaminationFinishingLayerMappingService {
  private _logger: Logger;
  constructor(
    _loggerFactory: LoggerFactory,
    private _mappingProcess: MappingProcessService,
  ) {
    this._logger = _loggerFactory.get(LogContextEnum.jobSync);
  }

  public getMappedItem(
    data: IMpSubJobLaminationFinishing,
  ): SubJobFinishingLayer {
    return {
      finishingType: MpSubJobFinishingTypeEnum.lamination,
      operation: {
        name: data.operation.name,
        width: data.operation.width,
        height: data.operation.height,
        doubleSided: data.operation.doublesided,
      },
      material: this._getMaterial(data),
      displayValue: this._getDisplayValue(data),
    };
  }

  private _getMaterial(
    data: IMpSubJobLaminationFinishing,
  ): FinishingLayerMaterial | null {
    if (!data.material) {
      this._logger.log(
        `Lamination finishing has no material defined. JobId: ${this._mappingProcess.processingJobMpId}, subJobId: ${this._mappingProcess.processingSubJobMpId}`,
      );

      return null;
    }

    return {
      id: data.material.id,
      name: data.material.name,
    };
  }

  private _getDisplayValue(data: IMpSubJobLaminationFinishing): string {
    const doubleSided = data.operation.doublesided ? 'R/V' : 'Recto';
    const materialName = data.material ? data.material.name : '[NON DÉFINIE]';

    return `${data.operation.name} ${materialName} - ${doubleSided}`;
  }
}
