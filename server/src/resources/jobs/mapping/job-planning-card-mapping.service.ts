import { Injectable } from '@nestjs/common';
import { IMpPlanningCard } from '../../../mp/mp-planning/interfaces';
import { IJobPlanningCard } from '../interfaces';
import { Job, JobWorksheet } from '../schemas';
import { IPlanningCardJobOperationUserInfo } from '../../planning/interfaces';

@Injectable()
export class JobPlanningCardMappingService {
  public getMappedCard(
    id: number,
    mpCard: IMpPlanningCard,
    job: Job,
  ): IJobPlanningCard {
    const jobWorksheets = job.worksheets?.filter(
      (ws) => ws.operation === mpCard.job.operation,
    );

    return {
      id,
      machineId: mpCard.machine.id,
      machineName: mpCard.machine.name,
      startDate: mpCard.start_date,
      calculatedTime: mpCard.calculated_time,
      productionTime: mpCard.production_time,
      employee: this._getEmployee(mpCard.employee),
      extraInfo: mpCard.extra_info,
      information: mpCard.information,
      completed: mpCard.completed,
      operation: mpCard.job.operation,
      operationUserInfo: this._getOperationUserInfo(jobWorksheets),
      operationTime: this._getTime(jobWorksheets),
      isPlanned: !mpCard.job.not_planned,
      material: mpCard.job.material,
      quality: mpCard.job.quality,
      days: mpCard.days.map((item) => {
        return {
          date: item.date,
          time: item.time,
          ordering: item.ordering,
        };
      }),
    };
  }

  private _getOperationUserInfo(
    worksheets?: JobWorksheet[],
  ): IPlanningCardJobOperationUserInfo | null {
    if (!worksheets || !worksheets.length) {
      return null;
    }
    const lastWorksheet = worksheets[worksheets.length - 1];

    return {
      employee: lastWorksheet.employee,
      active: !lastWorksheet.totalTime,
    };
  }

  private _getEmployee(employee: string): string | null {
    if (employee === '-' || employee === '_ _') {
      return null;
    }

    return employee;
  }

  private _getTime(worksheets?: JobWorksheet[]): number | null {
    if (!worksheets || !worksheets.length) {
      return null;
    }

    const time = worksheets.reduce((acc: number, ws) => {
      acc += ws.totalTime;

      return acc;
    }, 0);

    return time > 0 ? time : null;
  }
}
