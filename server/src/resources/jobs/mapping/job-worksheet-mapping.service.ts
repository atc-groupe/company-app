import { Injectable } from '@nestjs/common';
import { IMpEmployeeWorksheet } from '../../../mp/mp-employees/interfaces';
import { JobWorksheet } from '../schemas';
import { DateHelper } from '../../../shared/services';

@Injectable()
export class JobWorksheetMappingService {
  constructor(private _dateHelper: DateHelper) {}

  public getMappedData(mpWorksheet: IMpEmployeeWorksheet): JobWorksheet {
    return {
      id: mpWorksheet.id,
      date: this._dateHelper.getDate(mpWorksheet.date),
      employee: mpWorksheet.employee,
      startTime: this._dateHelper.extractTime(mpWorksheet.start_time),
      stopTime: this._dateHelper.extractTime(mpWorksheet.stop_time),
      timeProduction: mpWorksheet.time_production,
      employeeNumber: mpWorksheet.employee_number,
      timeCustomer: mpWorksheet.time_customer,
      operation: mpWorksheet.operation,
      operationNumber: mpWorksheet.operation_number,
      totalTime: mpWorksheet.total_time,
      remark: mpWorksheet.remark.replaceAll('\r', '\n'),
      countRuns: mpWorksheet.count_runs,
      operationReady: mpWorksheet.operation_ready,
    };
  }
}
