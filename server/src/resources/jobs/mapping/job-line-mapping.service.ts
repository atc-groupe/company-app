import { Injectable } from '@nestjs/common';
import { JobDocument } from '../schemas';
import { IJobLine } from '../interfaces';

@Injectable()
export class JobLineMappingService {
  public getMappedData(job: JobDocument): IJobLine {
    return {
      number: job.mp.number,
      statusNumber: job.mp.statusNumber,
      company: job.mp.company,
      description: job.mp.description,
      devices: job.meta.productionTypes,
      deliveryDate: job.mp.dates.delivery,
      sync: job.sync,
    };
  }
}
