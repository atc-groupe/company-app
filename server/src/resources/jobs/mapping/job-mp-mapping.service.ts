import { Injectable } from '@nestjs/common';
import { IMpJobQuotation } from '../../../mp/mp-jobs/interfaces';
import { DateHelper } from '../../../shared/services';
import { JobMp, JobMpStatusHistory } from '../schemas';

@Injectable()
export class JobMpMappingService {
  constructor(private _dateHelper: DateHelper) {}

  public getMappedData(mpJob: IMpJobQuotation): JobMp {
    return {
      id: mpJob.id,
      number: mpJob.job_number,
      numberSearch: mpJob.job_number.toString(),
      type: mpJob.type,
      company: mpJob.company,
      holding: mpJob.holding,
      relationNumber: mpJob.relation_number,
      contactName: mpJob.contact_name,
      description: mpJob.description,
      reference: mpJob.reference,
      phone: mpJob.phone,
      email: mpJob.email,
      creator: mpJob.creator,
      calculator: mpJob.calculator,
      salesMan: mpJob.salesman,
      jobManager: mpJob.jobmanager,
      price: mpJob.price,
      statusNumber: mpJob.job_status_number,
      statusHistory: this._getMappedStatusHistory(mpJob.job_status_history),
      invoiceStatus: mpJob.invoice_status,
      planned: mpJob.planned,
      delivered: mpJob.delivered,
      filesDelivered: mpJob.files_delivered,
      info: {
        general: this._getMappedInfo(mpJob.info_general),
        prePress: this._getMappedInfo(mpJob.info_prepress),
        digital: this._getMappedInfo(mpJob.info_digital),
        finishing: this._getMappedInfo(mpJob.info_finishing),
        subcontract: this._getMappedInfo(mpJob.info_subcontract),
        expeditions: this._getMappedInfo(mpJob.info_expedition),
        application: this._getMappedInfo(mpJob.info_planning),
      },
      dates: {
        date: this._dateHelper.getDateOrNull(mpJob.job_date),
        sending: this._dateHelper.getDateOrNull(mpJob.sending_date),
        delivery: this._dateHelper.getDateOrNull(mpJob.delivery_date),
        deliveryWeek: mpJob.delivery_week,
        deliveryDateLocked: mpJob.delivery_date_locked,
        atWork: this._dateHelper.getDateOrNull(mpJob.artwork_date),
        proof: this._dateHelper.getDateOrNull(mpJob.proof_date),
        return: this._dateHelper.getDateOrNull(mpJob.return_date),
      },
    };
  }

  private _getMappedStatusHistory(
    historyAsString: string,
  ): JobMpStatusHistory[] {
    const lines = historyAsString.split('\r').filter((v) => v !== '');

    return lines
      .filter((v) => v !== '')
      .map((line) => {
        const data = Array.from(
          line.matchAll(
            /^(\d{2}\/\d{2}\/\d{4}) (\d{2}:\d{2}) ([a-zA-Zéàè°ê\- ]+): +(.*)/g,
          ),
        )[0];

        if (!data || !data[1] || !data[2] || !data[3] || !data[4]) {
          return {
            date: '',
            time: '',
            status: '',
            reason: '',
          };
        }

        return {
          date: data[1],
          time: data[2],
          status: data[3],
          reason: data[4],
        };
      });
  }

  private _getMappedInfo(info: string): string {
    return info.replaceAll('\r', '\n');
  }
}
