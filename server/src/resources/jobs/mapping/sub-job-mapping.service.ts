import { Injectable } from '@nestjs/common';
import { IMpSubJobQuotation } from '../../../mp/mp-jobs/interfaces';
import { IMpSubJobDetailInfo } from '../../../mp/mp-workflow/interfaces';
import { SubJobMpMappingService } from './sub-job-mp-mapping.service';
import { SubJobProdLayerMappingService } from './sub-job-prod-layer-mapping.service';
import { SubJobFinishingLayerMappingService } from './sub-job-finishing-layer-mapping.service';
import { SubJobSubcontractorMappingService } from './sub-job-subcontractor-mapping.service';
import { SubJobMetaMappingService } from './sub-job-meta-mapping.service';
import { MappingProcessService } from './mapping-process.service';
import { Logger } from '../../../shared/services';
import { LogContextEnum } from '../../../shared/enum';
import { Settings } from '../../settings/schemas';
import { TSubJobMappingData } from '../types';
import { LoggerFactory } from '../../../shared/factories/logger.factory';
import { ISubJobMapping } from '../interfaces';
import { SubJob } from '../schemas';

@Injectable()
export class SubJobMappingService {
  private _logger: Logger;
  private _references = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

  constructor(
    loggerFactory: LoggerFactory,
    private _mappingProcess: MappingProcessService,
    private _mpMapping: SubJobMpMappingService,
    private _prodMapping: SubJobProdLayerMappingService,
    private _finishMapping: SubJobFinishingLayerMappingService,
    private _subcontractorMapping: SubJobSubcontractorMappingService,
    private _metaMapping: SubJobMetaMappingService,
  ) {
    this._logger = loggerFactory.get(LogContextEnum.jobSync);
  }

  public getMappedCollection(
    mpSubJobsData: TSubJobMappingData[],
    subJobs: SubJob[] | null,
    settings: Settings,
  ): ISubJobMapping[] {
    const mappedSubJobs = mpSubJobsData
      .filter(([subJobId, info]) => {
        if (!info) {
          this._logger.warn(
            `Sub-job quotation not found. Job ID: ${this._mappingProcess.processingJobMpId}. Sub-job id: ${subJobId}`,
          );
        }

        return !!info;
      })
      .map(([subJobId, info, workflowData]) => {
        return this._getMappedData(
          subJobId,
          info!,
          workflowData,
          settings,
          subJobs?.find((item) => item.mp.id === subJobId),
        );
      });

    this._mapReferences(mappedSubJobs);

    return mappedSubJobs;
  }

  private _getMappedData(
    id: number,
    info: IMpSubJobQuotation,
    workflowData: IMpSubJobDetailInfo | null,
    settings: Settings,
    subJob?: SubJob,
  ): ISubJobMapping {
    this._mappingProcess.processingSubJobMpId = id;

    const mp = this._mpMapping.getMappedItem(id, info);

    const prodLayers = workflowData
      ? this._prodMapping.getMappedCollection(
          workflowData,
          settings.devices.mapping.names,
        )
      : subJob
        ? subJob.prodLayers
        : null;
    const unlinkedFinishingLayers = workflowData
      ? this._finishMapping.getMappedCollection(workflowData.finishing)
      : null;
    const subcontractors = workflowData
      ? this._subcontractorMapping.getMappedCollection(
          workflowData.subcontractors,
        )
      : subJob
        ? subJob.subcontractors
        : null;
    const meta = prodLayers
      ? this._metaMapping.getMappedData(
          prodLayers,
          settings.job.mapping.metaFilters,
        )
      : subJob
        ? subJob.meta
        : null;

    this._mappingProcess.processingSubJobMpId = null;

    return {
      referenceIndex:
        subJob && subJob.referenceIndex !== -1 ? subJob.referenceIndex : -1,
      reference: subJob ? subJob.reference : '',
      mp,
      prodLayers,
      unlinkedFinishingLayers,
      subcontractors,
      meta,
    };
  }

  private _mapReferences(mappedSubJobs: ISubJobMapping[]): ISubJobMapping[] {
    const items: number[] = mappedSubJobs
      .filter((v) => v.referenceIndex !== -1)
      .map((v) => v.referenceIndex!)
      .sort((a, b) => a - b);

    mappedSubJobs.forEach((subJob) => {
      if (subJob.referenceIndex === -1) {
        subJob.referenceIndex = items.length ? items[items.length - 1] + 1 : 0;
        subJob.reference = this._getReferenceFromIndex(subJob.referenceIndex);
        items.push(subJob.referenceIndex);
      }
    });

    return mappedSubJobs;
  }

  private _getReferenceFromIndex(index: number): string {
    const letterIndex = index % 26;
    const zCount = Math.floor(index / 26);
    let reference = '';
    for (let i = 0; i < zCount; i++) {
      reference += 'Z';
    }

    return `${reference}${this._references[letterIndex]}`;
  }
}
