import { Injectable } from '@nestjs/common';
import { IMpDeliveryAddress } from '../../../mp/mp-jobs/interfaces';
import { DateHelper } from '../../../shared/services';
import {
  DeliveryAddress,
  DeliveryAddressItem,
  DeliveryAddressMp,
} from '../schemas';

@Injectable()
export class DeliveryAddressMappingService {
  constructor(private _dateHelper: DateHelper) {}

  public getMappedCollection(
    mpAddresses: (IMpDeliveryAddress | null)[],
  ): DeliveryAddress[] {
    return mpAddresses.reduce((acc: DeliveryAddress[], mpAddress) => {
      if (!mpAddress || this._isEmptyAddress(mpAddress)) {
        return acc;
      }

      const mappedAddressMp = this._getMappedAddressMp(mpAddress);
      const mappedItem = this._getMappedItem(mpAddress);
      const address = this._getItemAddress(acc, mappedAddressMp);

      if (address) {
        address.items.push(mappedItem);
      } else {
        acc.push({
          mp: mappedAddressMp,
          items: [mappedItem],
        });
      }

      return acc;
    }, []);
  }

  private _isEmptyAddress(mpAddress: IMpDeliveryAddress): boolean {
    return mpAddress.delivery_method === 'Non';
  }

  private _getMappedAddressMp(
    mpAddress: IMpDeliveryAddress,
  ): DeliveryAddressMp {
    return {
      company: mpAddress.company,
      address: mpAddress.address,
      zipCode: mpAddress.zipcode,
      city: mpAddress.city,
      country: mpAddress.country,
      countryCode: mpAddress.country_code,
      contactName: mpAddress.contact_name,
      phone: mpAddress.phone,
      email: mpAddress.email,
      relation: mpAddress.relation,
      addressNumber: mpAddress.address_number,
      relationNumber: mpAddress.relation_number,
      deliveryMethod: mpAddress.delivery_method,
      sendingDate: this._dateHelper.getDate(mpAddress.sending_date),
      deliveryDate: this._dateHelper.getDate(mpAddress.delivery_date),
      extraAddress: this._getExtraAddress(mpAddress),
    };
  }

  private _getExtraAddress(mpAddress: IMpDeliveryAddress): string[] | null {
    const extraAddress: string[] = [];

    if (mpAddress.extra_address_1) extraAddress.push(mpAddress.extra_address_1);
    if (mpAddress.extra_address_2) extraAddress.push(mpAddress.extra_address_2);
    if (mpAddress.extra_address_3) extraAddress.push(mpAddress.extra_address_3);
    if (mpAddress.extra_address_4) extraAddress.push(mpAddress.extra_address_4);
    if (mpAddress.extra_address_5) extraAddress.push(mpAddress.extra_address_5);

    return extraAddress.length ? extraAddress : null;
  }

  private _getItemAddress(
    addresses: DeliveryAddress[],
    addressMp: DeliveryAddressMp,
  ): DeliveryAddress | null {
    if (!addresses.length) {
      return null;
    }

    const itemAddress = addresses.find((item) => {
      return (
        item.mp.address === addressMp.address &&
        item.mp.zipCode === addressMp.zipCode &&
        item.mp.city === addressMp.city &&
        item.mp.deliveryMethod === addressMp.deliveryMethod &&
        item.mp.sendingDate === addressMp.sendingDate &&
        item.mp.deliveryDate === addressMp.deliveryDate
      );
    });

    return itemAddress ? itemAddress : null;
  }

  private _getMappedItem(mpAddress: IMpDeliveryAddress): DeliveryAddressItem {
    return {
      ordering: mpAddress.ordering,
      quantity: mpAddress.run,
      packedPer: mpAddress.packed_per,
      packages: mpAddress.packages,
      description: mpAddress.description,
      remark: mpAddress.remark,
      extraInfo: mpAddress.extra_info,
      weight: mpAddress.weight,
      status: mpAddress.status,
    };
  }
}
