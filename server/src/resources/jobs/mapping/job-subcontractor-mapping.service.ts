import { Injectable } from '@nestjs/common';
import {
  JobSubcontractor,
  JobSubcontractorOrder,
  JobSubcontractorOrderSubJobItem,
  SubJob,
  SubJobSubcontractorItemMp,
  SubJobSubcontractorItem,
} from '../schemas';

@Injectable()
export class JobSubcontractorMappingService {
  public getMappedCollection(subJobs: SubJob[]): JobSubcontractor[] | null {
    const itemsBySubcontractor = this._getItemsBySubcontractor(subJobs);

    if (!itemsBySubcontractor.length) {
      return null;
    }

    return itemsBySubcontractor.reduce((acc: JobSubcontractor[], subItem) => {
      acc.push({
        relation: subItem.subcontractor.relation,
        relationNumber: subItem.subcontractor.relationNumber,
        orders: this._getItemsByOrder(subItem.items),
      });

      return acc;
    }, []);
  }

  private _getItemsBySubcontractor(subJobs: SubJob[]): {
    subcontractor: { relation: string; relationNumber: number };
    items: { item: SubJobSubcontractorItem; subJob: SubJob }[];
  }[] {
    return subJobs.reduce(
      (
        acc: {
          subcontractor: { relation: string; relationNumber: number };
          items: { item: SubJobSubcontractorItem; subJob: SubJob }[];
        }[],
        subJob,
      ) => {
        if (!subJob.subcontractors) {
          return acc;
        }

        subJob.subcontractors.forEach((sub) => {
          const subByRelationItem = acc.find(
            (item) => item.subcontractor.relationNumber === sub.relationNumber,
          );

          const mappedItems = sub.items.map((item) => ({ item, subJob }));

          if (!subByRelationItem) {
            acc.push({
              subcontractor: {
                relation: sub.relation,
                relationNumber: sub.relationNumber,
              },
              items: mappedItems,
            });
          } else {
            subByRelationItem.items = [
              ...subByRelationItem.items,
              ...mappedItems,
            ];
          }
        });

        return acc;
      },
      [],
    );
  }

  private _getItemsByOrder(
    items: { item: SubJobSubcontractorItem; subJob: SubJob }[],
  ): JobSubcontractorOrder[] {
    return items.reduce((acc: JobSubcontractorOrder[], subJobItem) => {
      const order = acc.find(
        (item) =>
          item.contactName === subJobItem.item.mp.contactName &&
          item.deliveryDate === subJobItem.item.mp.deliveryDate &&
          item.sendingDate === subJobItem.item.mp.sendingDate,
      );

      if (order) {
        const subJob = order.subJobs.find(
          (item) => item.subJobMp.id === subJobItem.subJob.mp.id,
        );

        if (subJob) {
          subJob.items.push(this._getMappedItem(subJobItem.item.mp));
        } else {
          order.subJobs.push({
            reference: subJobItem.subJob.reference,
            subJobMp: subJobItem.subJob.mp,
            items: [this._getMappedItem(subJobItem.item.mp)],
          });
        }
      } else {
        acc.push({
          contactName: subJobItem.item.mp.contactName,
          sendingDate: subJobItem.item.mp.sendingDate,
          deliveryDate: subJobItem.item.mp.deliveryDate,
          subJobs: [
            {
              reference: subJobItem.subJob.reference,
              subJobMp: subJobItem.subJob.mp,
              items: [this._getMappedItem(subJobItem.item.mp)],
            },
          ],
        });
      }

      return acc;
    }, []);
  }

  private _getMappedItem(
    subMp: SubJobSubcontractorItemMp,
  ): JobSubcontractorOrderSubJobItem {
    return {
      description: subMp.description,
      quantity: subMp.quantity,
      remark: subMp.remark,
      ordered: subMp.ordered,
    };
  }
}
