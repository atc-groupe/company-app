import { Injectable } from '@nestjs/common';
import { IMpSubJobPrintingSheet } from '../../../mp/mp-workflow/interfaces';
import { SubJobNumericLayer } from '../schemas';
import { SettingsDeviceMappingName } from '../../settings/schemas';

@Injectable()
export class SubJobNumericLayerMappingService {
  public getMappedItem(
    data: IMpSubJobPrintingSheet,
    mappingNames: SettingsDeviceMappingName[],
  ): SubJobNumericLayer {
    const mappingName = mappingNames.find((mapping) =>
      mapping.mpName
        .toLowerCase()
        .trim()
        .includes(data.device.toLowerCase().trim()),
    );

    return {
      uniqueId: data.unique,
      layerName: data.sheetname,
      layerIndex: data.sheetnumber,
      remark: data.remark,
      device: data.device,
      deviceDisplayName: mappingName ? mappingName.displayName : data.device,
      printMode: data.colors.choice.replace(/[0-9]+\.\s?/, ''),
      roll: data.roll,
      width: data.modelwidth,
      height: data.modelheight,
      quantity: data.prints,
      paperName: data.papername,
      paperWeight: data.paperweight,
      paperThickness: data.paperthickness,
      paperCode: data.papercode,
      paperId: data.paperid,
    };
  }
}
