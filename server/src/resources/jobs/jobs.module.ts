import { Module } from '@nestjs/common';
import { JobsController } from './jobs.controller';
import { MpSystemModule } from '../../mp/mp-system/mp-system.module';
import {
  JobsStateService,
  JobsCommentsService,
  JobStockReservationsService,
  StockReservationsDataService,
  StockReservationsStateService,
  JobWorksheetsService,
  JobPlanningDataService,
  JobStateSyncService,
  JobsService,
} from './services';
import { JobsCommentsController } from './jobs-comments.controller';
import { MpJobsModule } from '../../mp/mp-jobs/mp-jobs.module';
import { MongooseModule } from '@nestjs/mongoose';
import { Job, JobSchema } from './schemas';
import {
  JobCommentsRepository,
  JobFindRepository,
  JobPrePressRepository,
  JobRepository,
  JobStockReservationsRepository,
} from './repositories';
import {
  DeliveryAddressMappingService,
  JobMetaMappingService,
  JobMpMappingService,
  JobOperationMappingService,
  JobPlanningCardMappingService,
  JobSubcontractorMappingService,
  JobWorksheetMappingService,
  MappingProcessService,
  StockReservationsLineMappingService,
  SubJobFinishingLayerMappingService,
  SubJobLaminationFinishingLayerMappingService,
  SubJobLargeFormatFinishingMappingService,
  SubJobMappingService,
  SubJobMetaMappingService,
  SubJobMpMappingService,
  SubJobNumericLayerMappingService,
  SubJobProdLayerMappingService,
  SubJobSubcontractorMappingService,
} from './mapping';
import { MpWorkflowModule } from '../../mp/mp-workflow/mp-workflow.module';
import { HelperModule } from '../../shared/modules';
import { SettingsModule } from '../settings/settings.module';
import { EventDispatchersModule } from '../../shared/modules/event-dispatchers.module';
import {
  JobsStateEventListener,
  JobsStockReservationsEventListener,
} from './event-listeners';
import { JobsPrePressController } from './jobs-pre-press.controller';
import { JobPrePressService } from './services';
import { MpPlanningModule } from '../../mp/mp-planning/mp-planning.module';
import { MpEmployeesModule } from '../../mp/mp-employees/mp-employees.module';
import {
  JobsSyncTaskService,
  JobSyncHelperService,
  JobSyncOneService,
  JobSyncProcessService,
  JobSyncService,
  OperationsSyncService,
  DeliveryAddressesSyncService,
  SubJobsSyncService,
} from './sync';
import { JobsStockReservationsController } from './jobs-stock-reservations.controller';
import { StockModule } from '../stock/stock.module';
import { JobsHashService } from './services/jobs-hash.service';
import { JobsListStateService } from './services/jobs-list-state.service';
import { JobsGateway } from './jobs.gateway';
import { JobLineMappingService } from './mapping/job-line-mapping.service';
import { StockReservationsGateway } from './stock-reservations.gateway';
import { JobWorksheetsRepository } from './repositories/job-worksheets.repository';
import { JobsWorksheetsSyncTaskService } from './tasks/jobs-worksheets-sync-task.service';
import { WorksheetsSyncService } from './sync/worksheets-sync.service';
import { JobsWorksheetsController } from './jobs-worksheets.controller';
import { JobSyncByDeliveryAddressesService } from './sync/job-sync-by-delivery-addresses.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Job.name, schema: JobSchema }]),
    MpSystemModule,
    MpWorkflowModule,
    SettingsModule,
    MpJobsModule,
    HelperModule,
    EventDispatchersModule,
    MpPlanningModule,
    MpEmployeesModule,
    StockModule,
  ],
  providers: [
    JobsService,
    JobsCommentsService,
    JobRepository,
    JobFindRepository,
    JobCommentsRepository,
    JobSyncService,
    JobSyncProcessService,
    JobSyncOneService,
    JobsSyncTaskService,
    DeliveryAddressMappingService,
    SubJobMappingService,
    SubJobMpMappingService,
    SubJobProdLayerMappingService,
    SubJobNumericLayerMappingService,
    SubJobFinishingLayerMappingService,
    SubJobLaminationFinishingLayerMappingService,
    SubJobLargeFormatFinishingMappingService,
    SubJobSubcontractorMappingService,
    SubJobMetaMappingService,
    JobOperationMappingService,
    JobWorksheetMappingService,
    MappingProcessService,
    JobMpMappingService,
    JobMetaMappingService,
    JobSubcontractorMappingService,
    JobPrePressService,
    JobPrePressRepository,
    OperationsSyncService,
    JobSyncHelperService,
    DeliveryAddressesSyncService,
    SubJobsSyncService,
    JobStockReservationsRepository,
    JobStockReservationsService,
    JobsHashService,
    JobsListStateService,
    JobsStateService,
    JobsGateway,
    JobLineMappingService,
    StockReservationsStateService,
    StockReservationsLineMappingService,
    StockReservationsDataService,
    StockReservationsGateway,
    JobsStockReservationsEventListener,
    JobsStateEventListener,
    JobWorksheetsRepository,
    JobWorksheetsService,
    WorksheetsSyncService,
    JobsWorksheetsSyncTaskService,
    JobPlanningDataService,
    JobPlanningCardMappingService,
    JobStateSyncService,
    JobSyncByDeliveryAddressesService,
  ],
  controllers: [
    JobsController,
    JobsCommentsController,
    JobsPrePressController,
    JobsStockReservationsController,
    JobsWorksheetsController,
  ],
  exports: [JobsService, JobSyncService, JobSyncByDeliveryAddressesService],
})
export class JobsModule {}
