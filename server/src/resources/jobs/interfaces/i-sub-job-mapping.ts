import {
  SubJobFinishingLayer,
  SubJobMeta,
  SubJobMp,
  SubJobProdLayer,
  SubJobSubcontractor,
} from '../schemas';

export interface ISubJobMapping {
  referenceIndex: number;
  reference: string;
  mp: SubJobMp;
  prodLayers: SubJobProdLayer[] | null;
  unlinkedFinishingLayers: SubJobFinishingLayer[] | null;
  subcontractors: SubJobSubcontractor[] | null;
  meta: SubJobMeta | null;
}
