import { IJobPlanningCardDay } from './i-job-planning-card-day';
import { IPlanningCardJobOperationUserInfo } from '../../planning/interfaces';

export interface IJobPlanningCard {
  id: number;
  machineId: number;
  machineName: string;
  startDate: Date;
  calculatedTime: number;
  productionTime: number;
  employee: string | null;
  extraInfo: string;
  information: string;
  completed: boolean;
  operation: string;
  operationUserInfo: IPlanningCardJobOperationUserInfo | null;
  operationTime: number | null;
  isPlanned: boolean;
  material: string;
  quality: string;
  days: IJobPlanningCardDay[];
}
