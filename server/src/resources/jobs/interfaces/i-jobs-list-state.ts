import { JobsQueryParamDto } from '../dto';
import { IJobsListStateItem } from './i-jobs-list-state-item';
import { Socket } from 'socket.io';

export interface IJobsListState {
  queryParams: JobsQueryParamDto;
  data: IJobsListStateItem[];
  sockets: Socket[];
}
