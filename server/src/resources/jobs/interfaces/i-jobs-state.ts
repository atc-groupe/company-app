import { Socket } from 'socket.io';
import { IJobPlanningCard } from './i-job-planning-card';

export interface IJobsState {
  number: number;
  planningData: IJobPlanningCard[];
  planningDataHash: string;
  sockets: Socket[];
}
