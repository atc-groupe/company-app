import { JobSync } from '../schemas';
import { ISyncElement } from '../../../shared/interfaces';

export interface IJobLine extends ISyncElement {
  number: number;
  statusNumber: number;
  company: string;
  description: string;
  devices: string[];
  deliveryDate: string | null;
  sync: JobSync;
}
