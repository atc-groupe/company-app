import { ISyncElement } from '../../../shared/interfaces';
import { JobStockReservationStatusEnum } from '../enum';

export interface IStockReservationLine extends ISyncElement {
  jobNumber: number;
  jobStatusNumber: number;
  company: string;
  sendingDate: string | null;
  globalStatus: JobStockReservationStatusEnum;
  itemsCount: number;
}
