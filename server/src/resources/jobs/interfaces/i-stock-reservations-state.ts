import { Socket } from 'socket.io';
import { IStockReservationsStateItem } from './i-stock-reservations-state-item';

export interface IStockReservationsState {
  sockets: Socket[];
  data: IStockReservationsStateItem[];
}
