export interface IStockReservationsStateItem {
  jobNumber: number;
  hash: string;
}
