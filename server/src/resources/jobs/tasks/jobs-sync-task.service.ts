import { Injectable, LoggerService } from '@nestjs/common';
import { JobRepository } from '../repositories';
import { JobSyncOneService } from '../sync';
import { firstValueFrom } from 'rxjs';
import { LoggerFactory } from '../../../shared/factories/logger.factory';
import { LogContextEnum } from '../../../shared/enum';
import { Cron } from '@nestjs/schedule';
import { JobStateSyncService } from '../services/job-state-sync.service';
import { ConfigService } from '@nestjs/config';
import { JobsStateService } from '../services';

@Injectable()
export class JobsSyncTaskService {
  private _logger: LoggerService;
  private _disabled = false;

  constructor(
    loggerFactory: LoggerFactory,
    private _jobRepository: JobRepository,
    private _jobSyncOneService: JobSyncOneService,
    private _jobsStateSyncService: JobStateSyncService,
    private _jobsStateService: JobsStateService,
    private _configService: ConfigService,
  ) {
    this._logger = loggerFactory.get(LogContextEnum.jobSync);
    const ALL_TASKS = this._configService.get('ALL_TASKS');
    const JOBS_TASKS = this._configService.get('JOBS_TASKS');

    if (ALL_TASKS === 'off' || JOBS_TASKS === 'off') {
      this._disabled = true;
    }
  }

  @Cron('0 */1 6-20 * * *')
  public async syncJobsInError(): Promise<void> {
    try {
      const errJobs = await this._jobRepository.findSyncError();

      if (!errJobs.length) {
        return;
      }

      for (const job of errJobs) {
        try {
          await firstValueFrom(this._jobSyncOneService.syncOne(job.mp.id));
        } catch (err) {
          this._logError(
            `Une erreur est survenue lors de la re-synchronisation du job ${job.mp.id}`,
            err,
          );
        }
      }
    } catch (err) {
      this._logError(
        "Une erreur est survenue lors de la re-synchronisation d'un job.",
        err,
      );
    }
  }

  @Cron('0/30 * 6-20 * * *')
  public async syncJobsStates(): Promise<void> {
    if (this._jobsStateService.isEmpty) {
      return;
    }

    if (this._disabled) {
      this._logger.log(
        `[JobsSyncTaskService.syncJobsStates] task skipped. See .env file`,
      );

      return;
    }

    await this._jobsStateSyncService.syncStates();
  }

  private _logError(message: string, err?: any) {
    if (err?.message) {
      message = `${message}. Message: ${err.message}`;
    }

    this._logger.error(`[JobReSyncService] ${message}`);
  }
}
