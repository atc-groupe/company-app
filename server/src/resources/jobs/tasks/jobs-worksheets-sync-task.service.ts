import { Injectable } from '@nestjs/common';
import { WorksheetsSyncService } from '../sync/worksheets-sync.service';
import { Cron } from '@nestjs/schedule';
import { ConfigService } from '@nestjs/config';
import { LoggerFactory } from '../../../shared/factories/logger.factory';
import { Logger } from '../../../shared/services';
import { LogContextEnum } from '../../../shared/enum';

@Injectable()
export class JobsWorksheetsSyncTaskService {
  private _logger: Logger;
  private _disabled = false;

  constructor(
    _loggerFactory: LoggerFactory,
    private _syncService: WorksheetsSyncService,
    private _configService: ConfigService,
  ) {
    this._logger = _loggerFactory.get(LogContextEnum.jobSync);
    const ALL_TASKS = this._configService.get('ALL_TASKS');
    const JOBS_TASKS = this._configService.get('JOBS_TASKS');

    if (ALL_TASKS === 'off' || JOBS_TASKS === 'off') {
      this._disabled = true;
    }
  }

  @Cron('0 0 23 * * *')
  async syncJobsWorksheets(): Promise<void> {
    if (this._disabled) {
      this._logger.log(
        `[JobsWorksheetsSyncTaskService.syncJobsWorksheets] task skipped. See .env file`,
      );

      return;
    }

    await this._syncService.syncJobsWorksheets();
  }
}
