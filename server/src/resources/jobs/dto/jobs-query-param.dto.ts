import {
  IsArray,
  IsBoolean,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';
import { StatusNumberListTransformer } from '../data-transformer/status-number-list.transformer';
import { Transform } from 'class-transformer';

export class JobsQueryParamDto {
  @IsString()
  @IsOptional()
  sendingDate?: string;

  @IsString()
  @IsOptional()
  startSendingDate?: string;

  @IsString()
  @IsOptional()
  endSendingDate?: string;

  @IsNumber()
  @IsOptional()
  statusNumber?: number;

  @IsArray()
  @IsOptional()
  @Transform(StatusNumberListTransformer)
  statusNumberList?: number[];

  @IsNumber()
  @IsOptional()
  startStatusNumber?: number;

  @IsNumber()
  @IsOptional()
  endStatusNumber?: number;

  @IsNumber()
  @IsOptional()
  jobEndNumber?: string;

  @IsString()
  @IsOptional()
  fullSearch?: string;

  @IsBoolean()
  @IsOptional()
  withStockReservation?: boolean;
}
