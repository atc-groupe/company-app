import { JobStockReservationStatusEnum } from '../enum';
import {
  IsBoolean,
  IsDate,
  IsEnum,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

export class JobStockReservationDto {
  @IsEnum(JobStockReservationStatusEnum)
  status: JobStockReservationStatusEnum;

  @IsString()
  @IsOptional()
  paperGenericName: string;

  @IsString()
  @IsOptional()
  paperName: string;

  @IsNumber()
  @IsOptional()
  paper: number;

  @IsNumber()
  @IsOptional()
  width: number;

  @IsNumber()
  @IsOptional()
  height: number;

  @IsBoolean()
  imperativeFormat: boolean;

  @IsNumber()
  quantity: number;

  @IsString()
  printSides: string;

  @IsString()
  destination: string;

  @IsString()
  @IsOptional()
  comment: string;

  @IsNumber()
  @IsOptional()
  subtractQuantity: number;

  @IsDate()
  @IsOptional()
  mutationDate: Date;

  @IsString()
  @IsOptional()
  createUsername: string;

  @IsString()
  @IsOptional()
  updateUsername: string;

  @IsString()
  @IsOptional()
  handleUsername: string;
}
