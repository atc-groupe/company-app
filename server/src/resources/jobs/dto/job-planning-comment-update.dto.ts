import { IsString } from 'class-validator';

export class JobPlanningCommentUpdateDto {
  @IsString()
  comment: string;
}
