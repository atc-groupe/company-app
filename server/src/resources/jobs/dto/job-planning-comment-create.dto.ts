import { IsString } from 'class-validator';

export class JobPlanningCommentCreateDto {
  @IsString()
  userId: string;

  @IsString()
  userInitials: string;

  @IsString()
  planningGroup: string;

  @IsString()
  comment: string;
}
