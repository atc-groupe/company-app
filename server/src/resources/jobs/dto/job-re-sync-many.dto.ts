import { IsNumber } from 'class-validator';

export class JobReSyncManyDto {
  @IsNumber({ maxDecimalPlaces: 0 }, { each: true })
  mpIds: number[];
}
