import { IsNumber, IsOptional, IsString } from 'class-validator';

export class JobPrePressUpdateDto {
  @IsString()
  @IsOptional()
  note: string;

  @IsString()
  @IsOptional()
  graphicDesignNote: string;

  @IsString()
  @IsOptional()
  designNote: string;

  @IsString()
  @IsOptional()
  filesCheck: string;

  @IsNumber()
  @IsOptional()
  filesCheckStatusIndex: number;
}
