import { IsNumber } from 'class-validator';
import { User } from '../../users/schema';

export class JobStockReservationHandledDto {
  @IsNumber()
  paperId: number;

  @IsNumber()
  quantity: number;

  user: User;

  action: 'create' | 'cancel';
}
