import { IsBoolean, IsNumber, IsOptional, IsString } from 'class-validator';

export class JobWorksheetUpdateDto {
  @IsString()
  @IsOptional()
  date: string;

  @IsString()
  @IsOptional()
  start_time: string;

  @IsString()
  @IsOptional()
  stop_time: string;

  @IsNumber()
  @IsOptional()
  time_production: number;

  @IsNumber()
  @IsOptional()
  time_customer: number;

  @IsString()
  operation: string;

  @IsNumber()
  @IsOptional()
  operation_number: number;

  @IsString()
  @IsOptional()
  remark?: string;

  @IsNumber()
  @IsOptional()
  count_runs: number;

  @IsBoolean()
  @IsOptional()
  operation_ready: boolean;
}
