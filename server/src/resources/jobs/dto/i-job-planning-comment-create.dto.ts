export interface IJobPlanningCommentCreateDto {
  userId: string;
  userInitials: string;
  planningGroup: string;
  comment: string;
}
