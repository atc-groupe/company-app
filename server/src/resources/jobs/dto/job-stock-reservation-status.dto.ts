import { JobStockReservationStatusEnum } from '../enum';
import { IsEnum, IsOptional, IsString } from 'class-validator';

export class JobStockReservationStatusDto {
  @IsEnum(JobStockReservationStatusEnum)
  from: JobStockReservationStatusEnum;

  @IsEnum(JobStockReservationStatusEnum)
  to: JobStockReservationStatusEnum;

  @IsString()
  @IsOptional()
  updateUsername: string;
}
