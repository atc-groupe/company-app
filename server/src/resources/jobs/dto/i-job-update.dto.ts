import {
  DeliveryAddress,
  JobMeta,
  JobMp,
  JobOperation,
  JobPrePress,
  JobSubcontractor,
  JobSync,
  SubJob,
} from '../schemas';
import { SettingsJobStatus } from '../../settings/schemas';

export interface IJobUpdateDto {
  mp?: JobMp;
  subJobs?: SubJob[] | null;
  deliveryAddresses?: DeliveryAddress[] | null;
  subcontractors?: JobSubcontractor[] | null;
  meta?: JobMeta;
  sync?: JobSync;
  prePress?: JobPrePress;
  operations?: JobOperation[] | null;
  mappedStatus?: SettingsJobStatus;
}
