import { IsBoolean, IsNumber, IsOptional, IsString } from 'class-validator';

export class JobWorksheetCreateDto {
  @IsNumber()
  job_number: number;

  @IsString()
  date: string;

  @IsString()
  start_time: string;

  @IsString()
  @IsOptional()
  stop_time: string;

  @IsNumber()
  @IsOptional()
  time_production: number;

  @IsNumber()
  employee_number: number;

  @IsNumber()
  @IsOptional()
  time_customer: number;

  @IsNumber()
  operation_number: number;

  @IsNumber()
  @IsOptional()
  total_time: number;

  @IsString()
  @IsOptional()
  remark?: string;

  @IsNumber()
  @IsOptional()
  count_runs: number;

  @IsBoolean()
  operation_ready: boolean;
}
