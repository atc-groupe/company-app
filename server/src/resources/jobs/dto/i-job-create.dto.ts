import {
  DeliveryAddress,
  JobMeta,
  JobMp,
  JobOperation,
  JobSubcontractor,
  JobSync,
  SubJob,
} from '../schemas';

export interface IJobCreateDto {
  mp: JobMp;
  subJobs: SubJob[] | null;
  deliveryAddresses: DeliveryAddress[] | null;
  subcontractors: JobSubcontractor[] | null;
  operations: JobOperation[] | null;
  meta: JobMeta;
  sync: JobSync;
}
