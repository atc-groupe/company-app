import { IsString } from 'class-validator';

export class JobWorksheetDeleteDto {
  @IsString()
  operation: string;

  @IsString()
  worksheetId: string;
}
