import { IsOptional, IsString } from 'class-validator';

export class JobDeliveryCommentDto {
  @IsString()
  @IsOptional()
  comment: string;
}
