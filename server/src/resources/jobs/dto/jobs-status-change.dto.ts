import { IsNumber, IsString } from 'class-validator';

export class JobsStatusChangeDto {
  @IsNumber()
  statusNumber: number;

  @IsString()
  reason: string;
}
