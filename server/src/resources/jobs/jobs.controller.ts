import { Body, Controller, Get, Param, Patch, Query } from '@nestjs/common';
import { JobsService } from './services';
import { Observable } from 'rxjs';
import {
  IActivesJobsSyncResult,
  IJobMappedStatus,
  IJobStatus,
} from './interfaces';
import { JobsQueryParamDto, JobsStatusChangeDto } from './dto';
import { Job } from './schemas';
import { JobSyncService } from './sync';
import { Authorizations } from '../auth/authorization.decorator';
import { AuthActionsJobsEnum, AuthSubjectsEnum } from '../../shared/enum';

@Controller('jobs')
export class JobsController {
  constructor(
    private _jobsService: JobsService,
    private _jobSyncService: JobSyncService,
  ) {}

  @Get()
  public find(@Query() queryParams: JobsQueryParamDto): Observable<Job[]> {
    return this._jobsService.find(queryParams);
  }

  @Get('statuses')
  public getStatuses(): Observable<IJobStatus[]> {
    return this._jobsService.getStatusesList();
  }

  @Get('mapped-statuses')
  public getMappedStatuses(): Observable<IJobMappedStatus[]> {
    return this._jobsService.getMappedStatuses();
  }

  @Get('sync/actives')
  @Authorizations({
    subject: AuthSubjectsEnum.Jobs,
    action: AuthActionsJobsEnum.SyncAllJobs,
  })
  public async syncActives(): Promise<IActivesJobsSyncResult> {
    return this._jobSyncService.syncActives();
  }

  @Get('sync/:mpNumber')
  public syncOne(@Param('mpNumber') mpNumber: number): Observable<Job> {
    return this._jobSyncService.syncOneByNumber(mpNumber);
  }

  @Get(':mpNumber')
  public findOneByMpNumber(
    @Param('mpNumber') mpNumber: number,
  ): Observable<Job> {
    return this._jobsService.findOneByMpNumber(mpNumber);
  }

  @Patch(':mpNumber/status')
  @Authorizations({
    subject: AuthSubjectsEnum.Jobs,
    action: AuthActionsJobsEnum.UpdateStatus,
  })
  public changeStatus(
    @Param('mpNumber') mpNumber: number,
    @Body() dto: JobsStatusChangeDto,
  ): Observable<unknown> {
    return this._jobsService.changeStatus(mpNumber, dto);
  }
}
