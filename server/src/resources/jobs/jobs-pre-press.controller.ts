import { Body, Controller, Param, Patch } from '@nestjs/common';
import { JobPrePressUpdateDto } from './dto';
import { Job } from './schemas';
import { Observable } from 'rxjs';
import { JobPrePressService } from './services/job-pre-press.service';
import { Authorizations } from '../auth/authorization.decorator';
import { AuthActionsJobsEnum, AuthSubjectsEnum } from '../../shared/enum';

@Controller('jobs')
export class JobsPrePressController {
  constructor(private _prePressService: JobPrePressService) {}

  @Patch(':id/pre-press')
  @Authorizations({
    subject: AuthSubjectsEnum.Jobs,
    action: AuthActionsJobsEnum.UpdateFilesCheckInfos,
  })
  public updateFilesCheck(
    @Body() dto: JobPrePressUpdateDto,
    @Param('id') id: string,
  ): Observable<Job> {
    return this._prePressService.updatePrePress(id, dto);
  }
}
