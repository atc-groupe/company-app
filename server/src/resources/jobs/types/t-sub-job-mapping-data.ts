import { IMpSubJobQuotation } from '../../../mp/mp-jobs/interfaces';
import { IMpSubJobDetailInfo } from '../../../mp/mp-workflow/interfaces';

export type TSubJobMappingData = [
  number,
  IMpSubJobQuotation | null,
  IMpSubJobDetailInfo | null,
];
