import { Controller, Get } from '@nestjs/common';
import { Observable } from 'rxjs';
import { EmployeesService } from './employees.service';
import { IEmployeeListItem } from './i-employee-list-item';
import { IEmployeeOperation } from './i-employee-operation';
import { EmployeesOperationsService } from './employees-operations.service';

@Controller('employees')
export class EmployeesController {
  constructor(
    private _employeesService: EmployeesService,
    private _operationsService: EmployeesOperationsService,
  ) {}
  @Get()
  public findAll(): Observable<IEmployeeListItem[]> {
    return this._employeesService.findAll();
  }

  @Get('not-registered')
  public findNotRegistered(): Observable<IEmployeeListItem[]> {
    return this._employeesService.findNotRegistered();
  }

  @Get('operations')
  public getOperations(): Observable<IEmployeeOperation[]> {
    return this._operationsService.getOperations();
  }
}
