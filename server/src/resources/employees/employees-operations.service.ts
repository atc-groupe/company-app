import { Injectable } from '@nestjs/common';
import { MpEmployeesOperationsRepository } from '../../mp/mp-employees/mp-employees-operations.repository';
import { map, Observable } from 'rxjs';
import { IEmployeeOperation } from './i-employee-operation';

@Injectable()
export class EmployeesOperationsService {
  constructor(private _operationsRepository: MpEmployeesOperationsRepository) {}

  public getOperations(): Observable<IEmployeeOperation[]> {
    return this._operationsRepository.getOperationList().pipe(
      map((operations) =>
        operations
          .filter((operation) => operation.active)
          .map((operation) => {
            return {
              operation: operation.operation,
              operationNumber: operation.operation_number,
              jobStatus: operation.job_status,
              jobStatusNumber: operation.job_status_number,
            };
          }),
      ),
    );
  }
}
