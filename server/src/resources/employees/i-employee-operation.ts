export interface IEmployeeOperation {
  operation: string;
  operationNumber: number;
  jobStatus?: string;
  jobStatusNumber?: number;
}
