import { Injectable } from '@nestjs/common';
import { IMpEmployeeListItem } from '../../mp/mp-employees/interfaces';
import { IEmployeeListItem } from './i-employee-list-item';

@Injectable()
export class EmployeesMappingService {
  public getMappedItem(mpEmployee: IMpEmployeeListItem): IEmployeeListItem {
    return {
      employeeNumber: mpEmployee.employee_number,
      name: mpEmployee.name,
    };
  }
}
