import {
  HttpException,
  Injectable,
  InternalServerErrorException,
  LoggerService,
} from '@nestjs/common';
import { catchError, map, Observable, switchMap } from 'rxjs';
import { MpEmployeesListRepository } from '../../mp/mp-employees/mp-employees-list.repository';
import { IEmployeeListItem } from './i-employee-list-item';
import { EmployeesMappingService } from './employees-mapping.service';
import { UserRepository } from '../users/user.repository';
import { LoggerFactory } from '../../shared/factories/logger.factory';
import { LogContextEnum } from '../../shared/enum';

@Injectable()
export class EmployeesService {
  private _logger: LoggerService;
  constructor(
    loggerFactory: LoggerFactory,
    private _mpEmployeesListRepository: MpEmployeesListRepository,
    private _usersRepository: UserRepository,
    private _mappingService: EmployeesMappingService,
  ) {
    this._logger = loggerFactory.get(LogContextEnum.employees);
  }

  public findAll(): Observable<IEmployeeListItem[]> {
    return this._mpEmployeesListRepository.findAll().pipe(
      map((employees) =>
        employees.map((item) => this._mappingService.getMappedItem(item)),
      ),
      catchError((err) => {
        const message =
          'Une erreur est survenue lors de la récupération des employés';
        this._logError(message, err);

        if (err instanceof HttpException) {
          throw err;
        }

        throw new InternalServerErrorException(message);
      }),
    );
  }

  public findNotRegistered(): Observable<IEmployeeListItem[]> {
    return this._mpEmployeesListRepository.findAll().pipe(
      switchMap((items) =>
        this._usersRepository.findAll().pipe(
          map((users) => {
            return items
              .filter(
                (item) =>
                  !users.find(
                    (user) => user.mp.employeeNumber === item.employee_number,
                  ),
              )
              .map((item) => this._mappingService.getMappedItem(item));
          }),
        ),
      ),
      catchError((err) => {
        const message =
          'Une erreur est survenue lors de la récupération des employés';
        this._logError(message, err);

        if (err instanceof HttpException) {
          throw err;
        }

        throw new InternalServerErrorException(message);
      }),
    );
  }

  private _logError(message: string, err?: any) {
    if (err?.message) {
      message = `${message}. Message: ${err.message}`;
    }

    this._logger.error(`[EmployeesService] ${message}`);
  }
}
