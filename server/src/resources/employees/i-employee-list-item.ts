export interface IEmployeeListItem {
  employeeNumber: number;
  name: string;
}
