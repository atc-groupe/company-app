import { Module } from '@nestjs/common';
import { MpEmployeesModule } from '../../mp/mp-employees/mp-employees.module';
import { EmployeesController } from './employees.controller';
import { EmployeesService } from './employees.service';
import { EmployeesMappingService } from './employees-mapping.service';
import { UsersModule } from '../users/users.module';
import { EmployeesOperationsService } from './employees-operations.service';

@Module({
  imports: [MpEmployeesModule, UsersModule],
  controllers: [EmployeesController],
  providers: [
    EmployeesService,
    EmployeesMappingService,
    EmployeesOperationsService,
  ],
})
export class EmployeesModule {}
