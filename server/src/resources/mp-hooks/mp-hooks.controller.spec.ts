import { Test, TestingModule } from '@nestjs/testing';
import { MpHooksController } from './mp-hooks.controller';

describe('MpHooksController', () => {
  let controller: MpHooksController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MpHooksController],
    }).compile();

    controller = module.get<MpHooksController>(MpHooksController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
