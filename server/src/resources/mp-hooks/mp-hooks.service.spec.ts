import { Test, TestingModule } from '@nestjs/testing';
import { MpHooksService } from './mp-hooks.service';

describe('MpHooksService', () => {
  let service: MpHooksService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MpHooksService],
    }).compile();

    service = module.get<MpHooksService>(MpHooksService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
