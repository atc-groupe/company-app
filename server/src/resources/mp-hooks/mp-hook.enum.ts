export enum MpHookEnum {
  'jobStatus' = 1,
  'quotationStatus' = 2,
  'relation' = 3,
  'employee' = 4,
  'newFile' = 5,
  'internetLineProcessed' = 6,
  'job' = 7,
  'quotation' = 8,
  'projects' = 9,
  'deliveryStatus' = 10,
}
