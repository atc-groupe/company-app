import { Body, Controller, Ip, Post } from '@nestjs/common';
import { MpHooksService } from './mp-hooks.service';
import { Public } from '../auth/public.decorator';
import { IMpHookEvent } from './i-mp-hook-event';

@Controller('mp-hooks')
export class MpHooksController {
  constructor(private _mpHooksService: MpHooksService) {}

  @Public()
  @Post()
  async sync(@Body() mpHookEvent: IMpHookEvent): Promise<void> {
    return this._mpHooksService.syncResource(mpHookEvent);
  }
}
