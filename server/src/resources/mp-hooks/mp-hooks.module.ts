import { Module } from '@nestjs/common';
import { MpHooksController } from './mp-hooks.controller';
import { MpHooksService } from './mp-hooks.service';
import { JobsModule } from '../jobs/jobs.module';

@Module({
  imports: [JobsModule],
  controllers: [MpHooksController],
  providers: [MpHooksService],
})
export class MpHooksModule {}
