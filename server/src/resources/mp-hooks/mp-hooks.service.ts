import { Injectable } from '@nestjs/common';
import { Logger } from '../../shared/services';
import { LogContextEnum } from '../../shared/enum';
import { IMpHookEvent } from './i-mp-hook-event';
import { MpHookEnum } from './mp-hook.enum';
import { catchError, EMPTY } from 'rxjs';
import { LoggerFactory } from '../../shared/factories/logger.factory';
import { JobSyncService } from '../jobs/sync';

@Injectable()
export class MpHooksService {
  private _logger: Logger;

  constructor(
    loggerFactory: LoggerFactory,
    private _jobSyncService: JobSyncService,
  ) {
    this._logger = loggerFactory.get(LogContextEnum.mpHook);
  }

  syncResource(mpHookEvent: IMpHookEvent): Promise<void> | void {
    this._logger.log(JSON.stringify(mpHookEvent), false);

    switch (mpHookEvent.event) {
      case MpHookEnum.job: {
        if (mpHookEvent.sub) {
          return;
        }

        this._jobSyncService
          .syncOne(mpHookEvent.id as number)
          .pipe(catchError(() => EMPTY))
          .subscribe();
        return;
      }
      default:
        return;
    }
  }
}
