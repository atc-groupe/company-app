import { MpHookEnum } from './mp-hook.enum';

export interface IMpHookEvent {
  id: number | string;
  type: string;
  event: MpHookEnum;
  sub: number;
}
