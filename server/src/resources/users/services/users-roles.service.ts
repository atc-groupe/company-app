import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { catchError, map, Observable } from 'rxjs';
import { User } from '../schema';
import { UserRepository } from '../user.repository';

@Injectable()
export class UsersRolesService {
  constructor(private _userRepository: UserRepository) {}

  public addRole(id: string, roleId: string): Observable<User> {
    return this._userRepository.addRole(id, roleId).pipe(
      map((user) => {
        if (!user) {
          throw new BadRequestException('Utilisateur introuvable');
        }

        return user;
      }),
      catchError((err) => {
        if (err instanceof BadRequestException) {
          throw err;
        }

        throw new InternalServerErrorException(
          'Une erreur est survenue lors de la suppression du role utilisateur',
        );
      }),
    );
  }

  public removeRole(id: string, roleId: string): Observable<User> {
    return this._userRepository.removeRole(id, roleId).pipe(
      map((user) => {
        if (!user) {
          throw new BadRequestException('Utilisateur introuvable');
        }

        return user;
      }),
      catchError((err) => {
        if (err instanceof BadRequestException) {
          throw err;
        }

        throw new InternalServerErrorException(
          'Une erreur est survenue lors de la suppression du role utilisateur',
        );
      }),
    );
  }
}
