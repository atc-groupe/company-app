import { Injectable } from '@nestjs/common';
import { IAdUser } from '../../active-directory/i-ad-user';
import {
  IMpEmployee,
  IMpEmployeeOperationListItem,
} from '../../../mp/mp-employees/interfaces';
import { UserAd, UserMp, UserMpOperation } from '../schema';

@Injectable()
export class UserSyncMappingService {
  public getMappedUserAd(adUser: IAdUser): UserAd {
    return {
      userPrincipalName: adUser.userPrincipalName,
      firstName: adUser.givenName,
      lastName: adUser.sn,
    };
  }

  public getMappedUserMp(mpEmployee: IMpEmployee): UserMp {
    return {
      employeeNumber: mpEmployee.employee_number,
      name: mpEmployee.name,
      department: mpEmployee.department,
      operations: mpEmployee.operations.map((operation) =>
        this._getMappedUserOperation(operation),
      ),
    };
  }

  private _getMappedUserOperation(
    operation: IMpEmployeeOperationListItem,
  ): UserMpOperation {
    return {
      number: operation.operation_number,
      label: operation.operation,
      type: operation.type,
    };
  }
}
