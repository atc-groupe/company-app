import {
  BadRequestException,
  HttpException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { catchError, map, Observable, of, switchMap } from 'rxjs';
import { Logger } from '../../../shared/services';
import { LogContextEnum } from '../../../shared/enum';
import { UserRepository } from '../user.repository';
import {
  UserCreateDto,
  UserUpdateDto,
  IUserUpdateDto,
  FindAllQueryParamsDto,
} from '../dto';
import { UserDocument } from '../schema';
import { UserSyncService } from './user-sync.service';
import { LoggerFactory } from '../../../shared/factories/logger.factory';

@Injectable()
export class UsersService {
  private _logger: Logger;

  constructor(
    _loggerFactory: LoggerFactory,
    private _userRepository: UserRepository,
    private _userSyncService: UserSyncService,
  ) {
    this._logger = _loggerFactory.get(LogContextEnum.users);
  }

  public findOne(id: string): Observable<UserDocument> {
    return this._userRepository.findOne(id).pipe(
      map((user) => {
        if (!user) {
          throw new NotFoundException('Utilisateur introuvable');
        }

        return user;
      }),
      catchError((err) => {
        if (err instanceof HttpException) {
          throw err;
        }

        throw new InternalServerErrorException(
          "Une erreur est survenue lors de la récupération de l'utilisateurs",
          { description: err },
        );
      }),
    );
  }

  public findAll(query: FindAllQueryParamsDto): Observable<UserDocument[]> {
    return this._userRepository.findAll(query).pipe(
      catchError((err) => {
        if (err instanceof HttpException) {
          throw err;
        }

        throw new InternalServerErrorException(
          'Une erreur est survenue lors de la récupération des utilisateurs',
        );
      }),
    );
  }

  public createOne(dto: UserCreateDto): Observable<UserDocument> {
    const user$ = this._userSyncService
      .getSyncData(dto.userPrincipalName, dto.employeeNumber)
      .pipe(
        switchMap((syncData) =>
          this._userRepository.createOne({
            isAdmin: dto.isAdmin,
            externalAccess: dto.externalAccess,
            ...syncData,
          }),
        ),
      );

    return user$.pipe(
      catchError((err) => {
        if (err.code === 11000) {
          throw new BadRequestException('Cet utilisateur est déjà créé');
        }

        if (err instanceof HttpException) {
          this._logger.error(err.message);
          throw err;
        }

        this._logger.error(
          `Une erreur est survenue lors de la création de l'utilisateur. userPrincipalName: ${dto.userPrincipalName}, employeeNumber: ${dto.employeeNumber}`,
        );

        throw new InternalServerErrorException(
          "Une erreur est survenue lors de la création de l'utilisateur",
        );
      }),
    );
  }

  public updateOne(id: string, dto: UserUpdateDto): Observable<UserDocument> {
    let userUpdateDto$: Observable<IUserUpdateDto>;
    const { userPrincipalName, ...updateDto } = dto;

    if (userPrincipalName) {
      userUpdateDto$ = this._userSyncService
        .getSyncUserAd(dto.userPrincipalName)
        .pipe(
          map((userAd) => {
            return { ...updateDto, ad: userAd };
          }),
        );
    } else {
      userUpdateDto$ = of(updateDto);
    }

    const user$ = userUpdateDto$.pipe(
      switchMap((updateDto) => this._userRepository.updateOne(id, updateDto)),
    );

    return user$.pipe(
      map((user) => {
        if (!user) {
          throw new NotFoundException('Utilisateur introuvable');
        }

        return user;
      }),
      catchError((err) => {
        if (err instanceof HttpException) {
          throw err;
        }

        throw new InternalServerErrorException(
          "Un problème est survenu lors de la modification de l'utilisateur",
        );
      }),
    );
  }

  public removeOne(id: string): Observable<UserDocument> {
    return this._userRepository.removeOne(id).pipe(
      map((user) => {
        if (!user) {
          throw new NotFoundException('Utilisateur introuvable');
        }

        return user;
      }),
      catchError((err) => {
        if (err instanceof HttpException) {
          throw err;
        }

        throw new InternalServerErrorException(
          "Un problème est survenu lors de la suppression de l'utilisateur",
        );
      }),
    );
  }

  private _logError(message: string, err?: any) {
    if (err?.message) {
      message = `${message}. Message: ${err.message}`;
    }

    this._logger.error(`[JobSyncService] ${message}`);
  }
}
