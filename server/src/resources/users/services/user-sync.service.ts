import {
  HttpException,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { Logger } from '../../../shared/services';
import { UserRepository } from '../user.repository';
import { MpEmployeesRepository } from '../../../mp/mp-employees/mp-employees.repository';
import { ActiveDirectoryService } from '../../active-directory/active-directory.service';
import { LogContextEnum } from '../../../shared/enum';
import {
  catchError,
  combineLatestWith,
  map,
  Observable,
  switchMap,
} from 'rxjs';
import { UserAd, UserDocument, UserMp } from '../schema';
import { UserSyncMappingService } from './user-sync-mapping.service';
import { IUserSync } from '../interfaces';
import { LoggerFactory } from '../../../shared/factories/logger.factory';

@Injectable()
export class UserSyncService {
  private _logger: Logger;

  constructor(
    _loggerFactory: LoggerFactory,
    private _userRepository: UserRepository,
    private _mpEmployeesRepository: MpEmployeesRepository,
    private _adService: ActiveDirectoryService,
    private _usersMapping: UserSyncMappingService,
  ) {
    this._logger = _loggerFactory.get(LogContextEnum.userSync);
  }

  public getSyncUserAd(userPrincipalName: string): Observable<UserAd> {
    return this._adService.findUser(userPrincipalName).pipe(
      map((adUser) => {
        if (!adUser) {
          const message = `Utilisateur introuvable dans l'active directory. userPrincipalName: ${userPrincipalName}`;
          this._logger.error(`[${this.constructor.name}] ${message}`);

          throw new InternalServerErrorException(message);
        }

        return this._usersMapping.getMappedUserAd(adUser);
      }),
    );
  }

  public getSyncUserMp(employeeNumber: number): Observable<UserMp> {
    return this._mpEmployeesRepository.findOneByNumber(employeeNumber).pipe(
      map((mpEmployee) => {
        if (!mpEmployee) {
          const message = `Utilisateur introuvable dans MultiPress. No d'employé: ${employeeNumber}`;
          this._logger.error(`[${this.constructor.name}] ${message}`);

          throw new InternalServerErrorException(message);
        }

        return this._usersMapping.getMappedUserMp(mpEmployee);
      }),
    );
  }

  public getSyncData(
    userPrincipalName: string,
    employeeNumber: number,
  ): Observable<IUserSync> {
    const userAd$ = this.getSyncUserAd(userPrincipalName);
    const userMp$ = this.getSyncUserMp(employeeNumber);

    return userAd$.pipe(
      combineLatestWith(userMp$),
      map(([userAd, userMp]) => {
        return {
          ad: userAd,
          mp: userMp,
        };
      }),
    );
  }

  public syncOne(id: string): Observable<UserDocument> {
    return this._userRepository.findOne(id).pipe(
      switchMap((user) => {
        if (!user) {
          const message = `Utilisateur introuvable dans l'application. ID: ${id}`;
          throw new InternalServerErrorException(message);
        }

        return this.getSyncData(
          user.ad.userPrincipalName,
          user.mp.employeeNumber,
        ).pipe(
          switchMap((dto) => {
            return this._userRepository.updateOne(id, dto);
          }),
        );
      }),

      map((user) => {
        if (!user) {
          const message = `Un problème est survenu lors de la synchronisation de l'utilisateur`;
          throw new InternalServerErrorException(message);
        }

        return user;
      }),

      catchError((err) => {
        const message = `Un problème est survenu lors de la synchronisation de l'utilisateur`;
        const logMessage = `[${this.constructor.name}] ${message}`;

        if (err instanceof HttpException) {
          this._logger.error(`${logMessage}. Message: ${err.message}`);

          throw err;
        }

        this._logger.error(logMessage);

        throw new InternalServerErrorException(message);
      }),
    );
  }
}
