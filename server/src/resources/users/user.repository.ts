import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserDocument } from './schema';
import { FilterQuery, Model, Types } from 'mongoose';
import { defer, forkJoin, Observable, of, switchMap, tap } from 'rxjs';
import { FindAllQueryParamsDto, IUserCreateDto, IUserUpdateDto } from './dto';
import { UsersEventDispatcher } from '../../shared/event-dispatchers/users.event-dispatcher';

@Injectable()
export class UserRepository {
  constructor(
    @InjectModel(User.name) private _userModel: Model<User>,
    private _eventDispatcher: UsersEventDispatcher,
  ) {}

  public findAll(query?: FindAllQueryParamsDto): Observable<UserDocument[]> {
    const filter: FilterQuery<User> = {};

    if (query?.roleId) {
      filter.roles = query.roleId;
    }

    if (query?.isActive) {
      filter.isActive = query.isActive;
    }

    return defer(() =>
      this._userModel
        .find(filter)
        .sort('ad.firstName ad.lastName')
        .populate('roles')
        .select('-__v')
        .exec(),
    );
  }

  public findOne(id: string): Observable<UserDocument | null> {
    return defer(() =>
      this._userModel.findById(id).populate('roles').select('-__v').exec(),
    );
  }

  public findOneByAdPrincipalName(
    userPrincipalName: string,
  ): Observable<UserDocument | null> {
    return defer(() =>
      this._userModel
        .findOne({ 'ad.userPrincipalName': userPrincipalName })
        .populate('roles')
        .select('-__v')
        .exec(),
    );
  }

  public findOneByEmployeeNumber(
    employeeNumber: number,
  ): Observable<UserDocument | null> {
    return defer(() =>
      this._userModel
        .findOne({ employeeNumber })
        .select('-__v')
        .populate('roles')
        .exec(),
    );
  }

  public createOne(dto: IUserCreateDto): Observable<UserDocument> {
    return defer(() => this._userModel.create({ ...dto, settings: {} })).pipe(
      tap((user) => this._eventDispatcher.dispatchEvent('create', user)),
    );
  }

  public updateOne(
    id: string,
    dto: IUserUpdateDto,
  ): Observable<UserDocument | null> {
    return defer(() =>
      this._userModel
        .findByIdAndUpdate(
          id,
          { $set: dto },
          { new: true, runValidators: true },
        )
        .populate('roles')
        .select('-__v')
        .exec(),
    ).pipe(
      tap((user) => {
        if (user) {
          this._eventDispatcher.dispatchEvent('update', user);
        }
      }),
    );
  }

  public removeOne(id: string): Observable<UserDocument | null> {
    return defer(() =>
      this._userModel.findByIdAndRemove(id).select('-__v').exec(),
    ).pipe(
      tap((user) => {
        if (user) {
          this._eventDispatcher.dispatchEvent('remove', user);
        }
      }),
    );
  }

  public addRole(id: string, roleId: string): Observable<UserDocument | null> {
    return defer(() =>
      this._userModel
        .findByIdAndUpdate(
          id,
          {
            $addToSet: { roles: new Types.ObjectId(roleId) },
          },
          { new: true, runValidators: true },
        )
        .select('-__v')
        .populate('roles')
        .exec(),
    ).pipe(
      tap((user) => {
        if (user) {
          this._eventDispatcher.dispatchEvent('add:roles', user);
        }
      }),
    );
  }

  public removeRole(
    id: string,
    roleId: string,
  ): Observable<UserDocument | null> {
    return defer(() =>
      this._userModel
        .findByIdAndUpdate(
          id,
          {
            $pull: { roles: new Types.ObjectId(roleId) },
          },
          { new: true, runValidators: true },
        )
        .populate('roles')
        .select('-__v')
        .exec(),
    ).pipe(
      tap((user) => {
        if (user) {
          this._eventDispatcher.dispatchEvent('remove:roles', user);
        }
      }),
    );
  }

  public removeRoleForAll(roleId: string): Observable<UserDocument[]> {
    const roleObjectId = new Types.ObjectId(roleId);
    return defer(() =>
      this._userModel.find({ roles: { $in: [roleObjectId] } }).exec(),
    ).pipe(
      switchMap((users) => {
        const removeRole$ = defer(() =>
          this._userModel
            .updateMany(
              { roles: { $in: [roleObjectId] } },
              { $pull: { roles: roleObjectId } },
            )
            .exec(),
        );

        const updatedUsersIds$ = of(users.map((user) => user._id));

        return forkJoin([removeRole$, updatedUsersIds$]);
      }),
      switchMap(([, updatedUserIds]) =>
        this._userModel
          .find({ _id: { $in: [updatedUserIds] } })
          .populate('roles')
          .select('-__v')
          .exec(),
      ),
      tap((users) => {
        users.forEach((user) => {
          this._eventDispatcher.dispatchEvent('remove:roles', user);
        });
      }),
    );
  }
}
