import { UserAd, UserMp } from '../schema';
import { UserSettingsUpdateDto } from '../../me/user-settings-update.dto';

export interface IUserUpdateDto {
  isActive?: boolean;
  isAdmin?: boolean;
  externalAccess?: boolean;
  ad?: UserAd;
  mp?: UserMp;
  settings?: UserSettingsUpdateDto;
}
