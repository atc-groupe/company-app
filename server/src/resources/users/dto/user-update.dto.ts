import { IsBoolean, IsOptional, IsString } from 'class-validator';

export class UserUpdateDto {
  @IsOptional()
  @IsString()
  userPrincipalName: string;

  @IsOptional()
  @IsBoolean()
  isActive: boolean;

  @IsBoolean()
  @IsOptional()
  isAdmin: boolean;

  @IsOptional()
  @IsBoolean()
  externalAccess: boolean;
}
