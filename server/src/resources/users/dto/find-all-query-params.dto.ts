import { IsBoolean, IsOptional, IsString } from 'class-validator';

export class FindAllQueryParamsDto {
  @IsString()
  @IsOptional()
  roleId?: string;

  @IsBoolean()
  @IsOptional()
  isActive?: boolean;
}
