import { IsBoolean, IsNumber, IsString } from 'class-validator';

export class UserCreateDto {
  @IsNumber({ maxDecimalPlaces: 0 })
  employeeNumber: number;

  @IsString()
  userPrincipalName: string;

  @IsBoolean()
  isAdmin: boolean;

  @IsBoolean()
  externalAccess: boolean;
}
