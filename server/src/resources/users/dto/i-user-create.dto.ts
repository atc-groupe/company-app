import { UserAd, UserMp } from '../schema';

export interface IUserCreateDto {
  isAdmin: boolean;
  externalAccess: boolean;
  ad: UserAd;
  mp: UserMp;
}
