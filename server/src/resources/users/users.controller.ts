import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { UsersService } from './services/users.service';
import { Observable } from 'rxjs';
import { UsersRolesService } from './services/users-roles.service';
import { FindAllQueryParamsDto, UserCreateDto, UserUpdateDto } from './dto';
import { User } from './schema';
import { Authorizations } from '../auth/authorization.decorator';
import { AuthActionsEnum, AuthSubjectsEnum } from '../../shared/enum';

@Controller('users')
export class UsersController {
  constructor(
    private _usersService: UsersService,
    private _usersRolesService: UsersRolesService,
  ) {}

  @Get()
  public findAll(@Query() query: FindAllQueryParamsDto): Observable<User[]> {
    return this._usersService.findAll(query);
  }

  @Post()
  @Authorizations({
    subject: AuthSubjectsEnum.Users,
    action: AuthActionsEnum.Manage,
  })
  public createOne(@Body() dto: UserCreateDto): Observable<User> {
    return this._usersService.createOne(dto);
  }

  @Get(':id')
  public findOne(@Param('id') id: string): Observable<User> {
    return this._usersService.findOne(id);
  }
  @Patch(':id')
  @Authorizations({
    subject: AuthSubjectsEnum.Users,
    action: AuthActionsEnum.Manage,
  })
  public updateOne(
    @Param('id') id: string,
    @Body() dto: UserUpdateDto,
  ): Observable<User> {
    return this._usersService.updateOne(id, dto);
  }

  @Delete(':id')
  @Authorizations({
    subject: AuthSubjectsEnum.Users,
    action: AuthActionsEnum.Manage,
  })
  public deleteOne(@Param('id') id: string): Observable<User> {
    return this._usersService.removeOne(id);
  }

  @Post(':id/roles/:roleId')
  @Authorizations({
    subject: AuthSubjectsEnum.Users,
    action: AuthActionsEnum.Manage,
  })
  public addRole(
    @Param('id') id: string,
    @Param('roleId') roleId: string,
  ): Observable<User> {
    return this._usersRolesService.addRole(id, roleId);
  }

  @Delete(':id/roles/:roleId')
  @Authorizations({
    subject: AuthSubjectsEnum.Users,
    action: AuthActionsEnum.Manage,
  })
  public removeRole(
    @Param('id') id: string,
    @Param('roleId') roleId: string,
  ): Observable<User> {
    return this._usersRolesService.removeRole(id, roleId);
  }
}
