import { UserAd, UserMp } from '../schema';

export interface IUserSync {
  ad: UserAd;
  mp: UserMp;
}
