import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({ _id: false })
export class UserAd {
  @Prop({ type: String, isRequired: true, unique: true })
  userPrincipalName: string;

  @Prop()
  firstName: string;

  @Prop()
  lastName: string;
}

export const UserAdSchema = SchemaFactory.createForClass(UserAd);
