import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({ _id: false })
export class UserMpOperation {
  @Prop()
  number: number;

  @Prop()
  label: string;

  @Prop()
  type: number;
}

export const UserMpOperationSchema =
  SchemaFactory.createForClass(UserMpOperation);
