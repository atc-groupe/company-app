import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import {
  UserMpOperation,
  UserMpOperationSchema,
} from './user-mp-operation.schema';

@Schema({ _id: false })
export class UserMp {
  @Prop({ type: Number, isRequired: true, unique: true })
  employeeNumber: number;

  @Prop()
  name: string;

  @Prop()
  department: string;

  @Prop([{ type: UserMpOperationSchema }])
  operations: UserMpOperation[];
}

export const UserMpSchema = SchemaFactory.createForClass(UserMp);
