import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument, Schema as MongooseSchema } from 'mongoose';
import { UserSettings, userSettingsSchema } from './user-settings.schema';
import { Role } from '../../roles/role.schema';
import { UserAd, UserAdSchema } from './user-ad.schema';
import { UserMp, UserMpSchema } from './user-mp.schema';

@Schema()
export class User {
  @Prop({ type: Boolean, default: false })
  isAdmin: boolean;

  @Prop({ type: Boolean, default: true })
  isActive: boolean;

  @Prop({ type: Boolean, default: false })
  externalAccess: boolean;

  @Prop({ type: userSettingsSchema })
  settings: UserSettings;

  @Prop({ type: [{ type: MongooseSchema.Types.ObjectId, ref: Role.name }] })
  roles: Role[];

  @Prop({ type: UserAdSchema })
  ad: UserAd;

  @Prop({ type: UserMpSchema })
  mp: UserMp;
}

export const UserSchema = SchemaFactory.createForClass(User);
export type UserDocument = HydratedDocument<User>;
