import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { PlanningModeEnum, PlanningPeriodEnum, UiModeEnum } from '../enum';
import { Types } from 'mongoose';
import { SettingsPlanningView } from '../../settings/schemas';

@Schema({ _id: false })
export class UserSettings {
  @Prop({ type: String, enum: ['dark', 'light'], default: UiModeEnum.Dark })
  uiMode: UiModeEnum;

  @Prop({
    type: String,
    enum: PlanningModeEnum,
    default: PlanningModeEnum.List,
  })
  planningMode: PlanningModeEnum;

  @Prop({
    type: String,
    enum: PlanningPeriodEnum,
    default: PlanningPeriodEnum.Day,
  })
  planningPeriod: PlanningPeriodEnum;

  @Prop({ type: Types.ObjectId, ref: SettingsPlanningView.name, default: null })
  planningView: SettingsPlanningView;

  @Prop({ type: Boolean, default: false })
  planningHideNotPlanned: boolean;

  @Prop({ type: Boolean, default: true })
  planningHideDone: boolean;

  @Prop({ type: Boolean, default: true })
  planningMergeJobOperationCards: boolean;
}

export const userSettingsSchema = SchemaFactory.createForClass(UserSettings);
