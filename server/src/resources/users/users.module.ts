import { Global, Module } from '@nestjs/common';
import { MpEmployeesModule } from '../../mp/mp-employees/mp-employees.module';
import { ActiveDirectoryModule } from '../active-directory/active-directory.module';
import { UsersController } from './users.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from './schema';
import { UserRepository } from './user.repository';
import {
  UsersRolesService,
  UsersService,
  UserSyncMappingService,
  UserSyncService,
} from './services';
import { EventDispatchersModule } from '../../shared/modules/event-dispatchers.module';

@Global()
@Module({
  imports: [
    MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
    MpEmployeesModule,
    ActiveDirectoryModule,
    EventDispatchersModule,
  ],
  providers: [
    UsersService,
    UsersRolesService,
    UserRepository,
    UserSyncService,
    UserSyncMappingService,
  ],
  controllers: [UsersController],
  exports: [UsersService, UserRepository, UserSyncService],
})
export class UsersModule {}
