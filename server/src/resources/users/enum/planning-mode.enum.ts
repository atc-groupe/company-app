export enum PlanningModeEnum {
  List = 'list',
  Kanban = 'kanban',
}
