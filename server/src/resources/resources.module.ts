import { Module } from '@nestjs/common';
import { UsersModule } from './users/users.module';
import { RolesModule } from './roles/roles.module';
import { MpHooksModule } from './mp-hooks/mp-hooks.module';
import { MeModule } from './me/me.module';
import { LogsModule } from './logs/logs.module';
import { JobsModule } from './jobs/jobs.module';
import { EmployeesModule } from './employees/employees.module';
import { AuthModule } from './auth/auth.module';
import { ActiveDirectoryModule } from './active-directory/active-directory.module';
import { SettingsModule } from './settings/settings.module';
import { StockModule } from './stock/stock.module';
import { PlanningModule } from './planning/planning.module';
import { ExpeditionsModule } from './expeditions/expeditions.module';
import { InitModule } from './init/init.module';
import { AppFilesModule } from './app-files/app-files.module';

@Module({
  imports: [
    ActiveDirectoryModule,
    AuthModule,
    EmployeesModule,
    JobsModule,
    LogsModule,
    MeModule,
    MpHooksModule,
    PlanningModule,
    RolesModule,
    UsersModule,
    SettingsModule,
    StockModule,
    ExpeditionsModule,
    InitModule,
    AppFilesModule,
  ],
})
export class ResourcesModule {}
