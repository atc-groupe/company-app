import { SetMetadata } from '@nestjs/common';
import { IAuthorization } from '../../shared/interfaces';

export const AUTHORIZATIONS_KEY = 'authorizations';
export const Authorizations = (...authorizations: IAuthorization[]) =>
  SetMetadata(AUTHORIZATIONS_KEY, authorizations);
