import { IsNotEmpty } from 'class-validator';

export class CredentialsDto {
  @IsNotEmpty({ message: "L'identifiant est requis" })
  identifier: string;

  @IsNotEmpty({ message: 'Le mot de passe est requis' })
  password: string;
}
