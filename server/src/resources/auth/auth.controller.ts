import {
  Body,
  Controller,
  HttpCode,
  HttpStatus,
  Post,
  Req,
} from '@nestjs/common';
import { CredentialsDto } from './credentials.dto';
import { Public } from './public.decorator';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { Request } from 'express';

@Controller('auth')
export class AuthController {
  constructor(private _authService: AuthService) {}

  @Public()
  @HttpCode(HttpStatus.OK)
  @Post('sign-in')
  signIn(
    @Body() credentials: CredentialsDto,
    @Req() request: Request,
  ): Observable<{ accessToken: string }> {
    console.log(request.headers);
    return this._authService.signIn(credentials);
  }
}
