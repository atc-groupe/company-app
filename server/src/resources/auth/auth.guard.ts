import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Reflector } from '@nestjs/core';
import { IS_PUBLIC_KEY } from './public.decorator';
import { catchError, defer, map, Observable, of, switchMap } from 'rxjs';
import { UsersService } from '../users/services';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private _jwtService: JwtService,
    private reflector: Reflector,
    private _usersService: UsersService,
  ) {}

  canActivate(context: ExecutionContext): Observable<boolean> {
    const isPublic = this.reflector.getAllAndOverride<boolean>(IS_PUBLIC_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);

    if (isPublic) {
      return of(true);
    }

    const request = context.switchToHttp().getRequest();
    const token = this.extractTokenFromHeader(request);

    if (!token) {
      throw new UnauthorizedException();
    }

    return defer(() => this._jwtService.verifyAsync(token, {})).pipe(
      switchMap((payload) => this._usersService.findOne(payload.sub)),
      map((user) => {
        if (!user.isActive) {
          throw new UnauthorizedException();
        }

        request['user'] = user;

        return true;
      }),
      catchError(() => {
        throw new UnauthorizedException();
      }),
    );
  }

  private extractTokenFromHeader(request: any): string | undefined {
    const [type, token] = request.headers.authorization?.split(' ') ?? [];
    return type === 'Bearer' ? token : undefined;
  }
}
