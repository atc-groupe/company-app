import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';
import { AUTHORIZATIONS_KEY } from './authorization.decorator';

@Injectable()
export class AuthorizationsGuard implements CanActivate {
  constructor(private _reflector: Reflector) {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const requiredAuthorizations = this._reflector.getAllAndOverride<string[]>(
      AUTHORIZATIONS_KEY,
      [context.getHandler(), context.getClass()],
    );

    if (!requiredAuthorizations) {
      return true;
    }

    const { user } = context.switchToHttp().getRequest();

    if (user.isAdmin) {
      return true;
    }

    return user.roles?.some((role: any) =>
      requiredAuthorizations.some((authorization: any) =>
        role.authorizations?.includes(
          `${authorization.subject}.${authorization.action}`,
        ),
      ),
    );
  }
}
