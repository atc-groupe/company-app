import { Module } from '@nestjs/common';
import { ActiveDirectoryModule } from '../active-directory/active-directory.module';
import { SecurityConfigModule } from '../../shared/modules';
import { UsersModule } from '../users/users.module';
import { AuthService } from './auth.service';
import { AuthGuard } from './auth.guard';
import { AuthController } from './auth.controller';
import { AuthorizationsGuard } from './authorizations.guard';

@Module({
  imports: [ActiveDirectoryModule, SecurityConfigModule, UsersModule],
  providers: [AuthService, AuthGuard, AuthorizationsGuard],
  controllers: [AuthController],
})
export class AuthModule {}
