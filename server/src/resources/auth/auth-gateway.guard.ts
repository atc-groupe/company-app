import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../users/services';
import { catchError, defer, map, Observable, switchMap } from 'rxjs';
import { AppWsUnauthorizedErrorException } from '../../shared/exceptions';

@Injectable()
export class AuthGatewayGuard implements CanActivate {
  constructor(
    private _jwtService: JwtService,
    private _usersService: UsersService,
  ) {}

  canActivate(context: ExecutionContext): Observable<boolean> {
    const client = context.switchToWs().getClient();
    const token = client.handshake.auth.accessToken;

    if (!token) {
      throw new AppWsUnauthorizedErrorException('Accès refusé');
    }

    return defer(() => this._jwtService.verifyAsync(token, {})).pipe(
      switchMap((payload) => this._usersService.findOne(payload.sub)),
      map((user) => {
        if (!user.isActive) {
          throw new AppWsUnauthorizedErrorException(
            'Accès refusé: Utilisateur désactivé',
          );
        }

        return true;
      }),
      catchError(() => {
        throw new AppWsUnauthorizedErrorException('Accès refusé');
      }),
    );
  }
}
