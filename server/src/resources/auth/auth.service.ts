import {
  HttpException,
  Injectable,
  InternalServerErrorException,
  UnauthorizedException,
} from '@nestjs/common';
import { CredentialsDto } from './credentials.dto';
import { JwtService } from '@nestjs/jwt';
import { ActiveDirectoryService } from '../active-directory/active-directory.service';
import { catchError, map, Observable, switchMap, withLatestFrom } from 'rxjs';
import { Logger } from '../../shared/services';
import { LogContextEnum } from '../../shared/enum';
import { UserRepository } from '../users/user.repository';
import { UserSyncService } from '../users/services';
import { LoggerFactory } from '../../shared/factories/logger.factory';

@Injectable()
export class AuthService {
  private _logger: Logger;

  constructor(
    loggerFactory: LoggerFactory,
    private _jwtService: JwtService,
    private _userRepository: UserRepository,
    private _adService: ActiveDirectoryService,
    private _userSyncService: UserSyncService,
  ) {
    this._logger = loggerFactory.get(LogContextEnum.auth);
  }

  public signIn(
    credentials: CredentialsDto,
  ): Observable<{ accessToken: string }> {
    return this._userRepository
      .findOneByAdPrincipalName(credentials.identifier)
      .pipe(
        switchMap((user) => {
          if (!user) {
            this._logger.error(
              `[AuthService] SignIn: user not found in app DB; Windows account: ${credentials.identifier}`,
            );

            throw new UnauthorizedException('Identifiants incorrects');
          }

          if (!user.isActive) {
            throw new UnauthorizedException('Ce compte est désactivé');
          }

          return this._userSyncService.syncOne(user._id.toString());
        }),

        withLatestFrom(
          this._adService.isPasswordValid(
            credentials.identifier,
            credentials.password,
          ),
        ),

        map(([user, isPasswordValid]) => {
          if (!isPasswordValid) {
            this._logger.error(
              `[AuthService] SignIn: invalid credentials. identifier: ${credentials.identifier}`,
            );

            throw new UnauthorizedException('Identifiants incorrects');
          }

          const payload = {
            identifier: credentials.identifier,
            sub: user!._id,
          };

          this._logger.log(
            `[AuthService] SignIn: ${credentials.identifier} has logged-in successfully`,
          );

          return {
            accessToken: this._jwtService.sign(payload),
          };
        }),

        catchError((err) => {
          if (err instanceof UnauthorizedException) {
            this._logger.error(err.message);
            throw err;
          }

          if (err instanceof HttpException) {
            const message = 'Identifiants incorrects';
            this._logger.error(
              `[AuthService] ${message}. Identifiant: ${credentials.identifier}`,
            );

            throw new UnauthorizedException('Identifiants incorrects');
          }

          const message =
            "Oups! un problème est survenu. Veuillez re-essayer. Si le problème persiste, contactez l'administrateur";
          this._logger.error(
            `[AuthService] ${message}. Identifiant: ${credentials.identifier}`,
          );

          throw new InternalServerErrorException(message);
        }),
      );
  }
}
