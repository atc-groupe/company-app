import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Role, RoleDocument } from './role.schema';
import { Model } from 'mongoose';
import { defer, Observable, tap } from 'rxjs';
import { RoleDto } from './role.dto';
import { AuthorizationDto } from './authorization.dto';
import { RolesEventDispatcher } from '../../shared/event-dispatchers';

@Injectable()
export class RoleRepository {
  constructor(
    @InjectModel(Role.name) private _dbRoleModel: Model<Role>,
    private _eventDispatcher: RolesEventDispatcher,
  ) {}

  public findAll(): Observable<RoleDocument[]> {
    return defer(() =>
      this._dbRoleModel.find().select('-__v').sort({ label: 1 }).exec(),
    );
  }

  public findOne(id: string): Observable<RoleDocument | null> {
    return defer(() => this._dbRoleModel.findById(id).select('-__v').exec());
  }

  public insertOne(dto: RoleDto): Observable<RoleDocument> {
    return defer(() => this._dbRoleModel.create(dto)).pipe(
      tap((role) => this._eventDispatcher.dispatchEvent('create', role)),
    );
  }

  public updateOne(id: string, dto: RoleDto): Observable<RoleDocument | null> {
    return defer(() =>
      this._dbRoleModel
        .findByIdAndUpdate(id, dto, { new: true, runValidators: true })
        .select('-__v')
        .exec(),
    ).pipe(
      tap((role) => {
        if (role) {
          this._eventDispatcher.dispatchEvent('update', role);
        }
      }),
    );
  }

  public deleteOne(id: string): Observable<RoleDocument | null> {
    return defer(() =>
      this._dbRoleModel.findByIdAndRemove(id).select('-__v').exec(),
    ).pipe(
      tap((role) => {
        if (role) {
          this._eventDispatcher.dispatchEvent('delete', role);
        }
      }),
    );
  }

  public addAuthorization(
    id: string,
    dto: AuthorizationDto,
  ): Observable<RoleDocument | null> {
    return defer(() =>
      this._dbRoleModel
        .findByIdAndUpdate(
          id,
          {
            $addToSet: { authorizations: dto.name },
          },
          { new: true, runValidators: true },
        )
        .select('-__v')
        .exec(),
    ).pipe(
      tap((role) => {
        if (role) {
          this._eventDispatcher.dispatchEvent('create:authorization', role);
        }
      }),
    );
  }

  public removeAuthorization(
    id: string,
    dto: AuthorizationDto,
  ): Observable<RoleDocument | null> {
    return defer(() =>
      this._dbRoleModel
        .findByIdAndUpdate(
          id,
          {
            $pull: { authorizations: dto.name },
          },
          { new: true, runValidators: true },
        )
        .select('-__v')
        .exec(),
    ).pipe(
      tap((role) => {
        if (role) {
          this._eventDispatcher.dispatchEvent('delete:authorization', role);
        }
      }),
    );
  }
}
