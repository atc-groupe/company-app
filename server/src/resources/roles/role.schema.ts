import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

@Schema()
export class Role {
  @Prop({ type: String, isRequired: true, unique: true })
  label: string;

  @Prop()
  description: string;

  @Prop()
  authorizations: string[];
}

export const RoleSchema = SchemaFactory.createForClass(Role);
export type RoleDocument = HydratedDocument<Role>;
