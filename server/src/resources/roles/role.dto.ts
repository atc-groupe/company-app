import { IsOptional, IsString } from 'class-validator';

export class RoleDto {
  @IsString()
  label: string;

  @IsString()
  @IsOptional()
  description: string;
}
