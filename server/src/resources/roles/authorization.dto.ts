import { IsString } from 'class-validator';

export class AuthorizationDto {
  @IsString()
  name: string;
}
