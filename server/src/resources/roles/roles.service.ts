import {
  BadRequestException,
  HttpException,
  Injectable,
  InternalServerErrorException,
  LoggerService,
  NotFoundException,
} from '@nestjs/common';
import { RoleRepository } from './role.repository';
import { RoleDto } from './role.dto';
import { catchError, map, Observable, switchMap } from 'rxjs';
import { UserRepository } from '../users/user.repository';
import { RoleDocument } from './role.schema';
import { LoggerFactory } from '../../shared/factories/logger.factory';
import { LogContextEnum } from '../../shared/enum';

@Injectable()
export class RolesService {
  private _logger: LoggerService;
  constructor(
    loggerFactory: LoggerFactory,
    private _roleRepository: RoleRepository,
    private _usersRepository: UserRepository,
  ) {
    this._logger = loggerFactory.get(LogContextEnum.roles);
  }

  public findAll(): Observable<RoleDocument[]> {
    return this._roleRepository.findAll().pipe(
      catchError((err) => {
        const message = 'Une erreur est survenue lors de la lecture des roles';
        this._logError(message, err);

        if (err instanceof HttpException) {
          throw err;
        }

        throw new InternalServerErrorException(message);
      }),
    );
  }

  public findOne(id: string): Observable<RoleDocument> {
    return this._roleRepository.findOne(id).pipe(
      map((role) => {
        if (!role) {
          throw new NotFoundException('Role introuvable');
        }

        return role;
      }),
      catchError((err) => {
        const message = `Une erreur est survenue lors de la lecture du role _id: ${id}`;
        this._logError(message, err);

        if (err instanceof HttpException) {
          throw err;
        }

        throw new InternalServerErrorException(message);
      }),
    );
  }

  public createOne(dto: RoleDto): Observable<RoleDocument> {
    return this._roleRepository.insertOne(dto).pipe(
      catchError((err) => {
        if (err.code === 11000) {
          const message = 'Un role avec ce nom existe déjà';
          this._logError(`${message}. Nom: ${dto.label}`, err);

          throw new BadRequestException(message);
        }

        const message = `Une erreur est survenue lors de la création du rôle`;
        this._logError(`${message} ${dto.label}`, err);

        if (err instanceof HttpException) {
          throw err;
        }

        throw new InternalServerErrorException(message);
      }),
    );
  }

  public updateOne(id: string, dto: RoleDto): Observable<RoleDocument> {
    return this._roleRepository.updateOne(id, dto).pipe(
      map((role) => {
        if (!role) {
          throw new BadRequestException(`Rôle introuvable avec l'id ${id}`);
        }

        return role;
      }),
      catchError((err) => {
        if (err.code === 11000) {
          const message = 'Un role avec ce nom existe déjà';
          this._logError(`${message}. Nom: ${dto.label}`, err);

          throw new BadRequestException(message);
        }

        const message = `Une erreur est survenue lors de la modification du rôle`;
        this._logError(`${message} ${dto.label}. _id: ${id}`, err);

        if (err instanceof HttpException) {
          throw err;
        }

        throw new InternalServerErrorException(message);
      }),
    );
  }

  public deleteOne(id: string): Observable<RoleDocument> {
    return this._roleRepository.deleteOne(id).pipe(
      map((role) => {
        if (!role) {
          throw new BadRequestException(`Rôle introuvable avec l'id ${id}`);
        }

        return role;
      }),
      switchMap((role) =>
        this._usersRepository.removeRoleForAll(id).pipe(map(() => role)),
      ),
      catchError((err) => {
        const message = `Une erreur est survenue lors de la suppression du rôle`;
        this._logError(`${message}. _id: ${id}`, err);

        if (err instanceof HttpException) {
          throw err;
        }

        throw new InternalServerErrorException(message);
      }),
    );
  }

  private _logError(message: string, err?: any) {
    if (err?.message) {
      message = `${message}. Message: ${err.message}`;
    }

    this._logger.error(`[RolesService] ${message}`);
  }
}
