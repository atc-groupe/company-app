import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { RolesService } from './roles.service';
import { Observable } from 'rxjs';
import { RoleDto } from './role.dto';
import { AuthorizationDto } from './authorization.dto';
import { RolesAuthorizationsService } from './roles-authorizations.service';
import { Role } from './role.schema';
import { Authorizations } from '../auth/authorization.decorator';
import {
  AuthActionsEnum,
  AuthActionsJobsEnum,
  AuthSubjectsEnum,
} from '../../shared/enum';

@Controller('roles')
export class RolesController {
  constructor(
    private _rolesService: RolesService,
    private _roleAuthorizationsService: RolesAuthorizationsService,
  ) {}

  @Get()
  public findAll(): Observable<Role[]> {
    return this._rolesService.findAll();
  }

  @Get(':id')
  public findOne(@Param('id') id: string): Observable<Role> {
    return this._rolesService.findOne(id);
  }

  @Post()
  @Authorizations({
    subject: AuthSubjectsEnum.Roles,
    action: AuthActionsEnum.Manage,
  })
  public createOne(@Body() dto: RoleDto): Observable<Role> {
    return this._rolesService.createOne(dto);
  }

  @Patch(':id')
  @Authorizations({
    subject: AuthSubjectsEnum.Roles,
    action: AuthActionsEnum.Manage,
  })
  public updateOne(
    @Param('id') id: string,
    @Body() dto: RoleDto,
  ): Observable<Role> {
    return this._rolesService.updateOne(id, dto);
  }

  @Delete(':id')
  @Authorizations({
    subject: AuthSubjectsEnum.Roles,
    action: AuthActionsEnum.Manage,
  })
  public deleteOne(@Param('id') id: string): Observable<Role> {
    return this._rolesService.deleteOne(id);
  }

  @Patch(':id/authorizations/add')
  @Authorizations({
    subject: AuthSubjectsEnum.Roles,
    action: AuthActionsEnum.Manage,
  })
  public addAuthorization(
    @Param('id') id: string,
    @Body() dto: AuthorizationDto,
  ): Observable<Role> {
    return this._roleAuthorizationsService.addAuthorization(id, dto);
  }

  @Patch(':id/authorizations/remove')
  @Authorizations({
    subject: AuthSubjectsEnum.Roles,
    action: AuthActionsEnum.Manage,
  })
  public removeAuthorization(
    @Param('id') id: string,
    @Body() dto: AuthorizationDto,
  ): Observable<Role> {
    return this._roleAuthorizationsService.removeAuthorization(id, dto);
  }
}
