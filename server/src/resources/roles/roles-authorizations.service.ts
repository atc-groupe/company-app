import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  LoggerService,
} from '@nestjs/common';
import { RoleRepository } from './role.repository';
import { AuthorizationDto } from './authorization.dto';
import { catchError, map, Observable } from 'rxjs';
import { Role } from './role.schema';
import { LoggerFactory } from '../../shared/factories/logger.factory';
import { LogContextEnum } from '../../shared/enum';

@Injectable()
export class RolesAuthorizationsService {
  private _logger: LoggerService;

  constructor(
    loggerFactory: LoggerFactory,
    private _dbRoleRepository: RoleRepository,
  ) {
    this._logger = loggerFactory.get(LogContextEnum.roles);
  }

  public addAuthorization(
    roleId: string,
    dto: AuthorizationDto,
  ): Observable<Role> {
    return this._dbRoleRepository.addAuthorization(roleId, dto).pipe(
      map((role) => {
        if (!role) {
          throw new BadRequestException('Role introuvable');
        }

        return role;
      }),
      catchError((err) => {
        const message = `Une erreur est survenue lors de l'ajout de l'autorisation pour le role _id ${roleId}`;
        this._logError(message, err);

        if (err instanceof BadRequestException) {
          throw err;
        }

        throw new InternalServerErrorException(message);
      }),
    );
  }

  public removeAuthorization(
    roleId: string,
    dto: AuthorizationDto,
  ): Observable<Role> {
    return this._dbRoleRepository.removeAuthorization(roleId, dto).pipe(
      map((role) => {
        if (!role) {
          throw new BadRequestException('Role introuvable');
        }

        return role;
      }),
      catchError((err) => {
        const message = `Une erreur est survenue lors de la suppression de l'autorisation pour le role _id ${roleId}`;
        this._logError(message, err);

        if (err instanceof BadRequestException) {
          throw err;
        }

        throw new InternalServerErrorException(message);
      }),
    );
  }

  private _logError(message: string, err?: any) {
    if (err?.message) {
      message = `${message}. Message: ${err.message}`;
    }

    this._logger.error(`[RolesAuthorizationsService] ${message}`);
  }
}
