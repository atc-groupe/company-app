import { Module } from '@nestjs/common';
import { RolesService } from './roles.service';
import { RolesController } from './roles.controller';
import { RolesAuthorizationsService } from './roles-authorizations.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Role, RoleSchema } from './role.schema';
import { RoleRepository } from './role.repository';
import { UsersModule } from '../users/users.module';
import { EventDispatchersModule } from '../../shared/modules/event-dispatchers.module';

@Module({
  imports: [
    UsersModule,
    MongooseModule.forFeature([{ name: Role.name, schema: RoleSchema }]),
    EventDispatchersModule,
  ],
  providers: [RolesService, RolesAuthorizationsService, RoleRepository],
  controllers: [RolesController],
})
export class RolesModule {}
