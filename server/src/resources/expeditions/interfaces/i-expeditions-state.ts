import { ExpeditionsQueryParamsDto } from '../dto/expeditions-query-params.dto';
import { IExpeditionsStateDataItem } from './i-expeditions-state-data-item';
import { Socket } from 'socket.io';

export interface IExpeditionsState {
  queryParams: ExpeditionsQueryParamsDto;
  data: IExpeditionsStateDataItem[];
  sockets: Socket[];
}
