export interface IExpeditionsStateDataItem {
  jobNumber: number;
  hash: string;
}
