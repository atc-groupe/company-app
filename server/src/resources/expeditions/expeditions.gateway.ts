import {
  ConnectedSocket,
  MessageBody,
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketGateway,
  WsException,
} from '@nestjs/websockets';
import { Socket } from 'socket.io';
import { AppWsResponse, AppWsSuccessResponse } from '../../shared/classes';
import { AppWsInternalServerErrorException } from '../../shared/exceptions';
import { ExpeditionsDataService, ExpeditionsStateService } from './services';
import { ExpeditionsQueryParamsDto } from './dto/expeditions-query-params.dto';
import { IExpeditionLine } from './interfaces';
import { UseGuards } from '@nestjs/common';
import { AuthGatewayGuard } from '../auth/auth-gateway.guard';

@UseGuards(AuthGatewayGuard)
@WebSocketGateway({ transports: ['websocket'], namespace: 'expeditions' })
export class ExpeditionsGateway implements OnGatewayDisconnect {
  constructor(
    private _stateService: ExpeditionsStateService,
    private _dataService: ExpeditionsDataService,
  ) {}

  handleDisconnect(client: any): any {
    this._stateService.removeSocket(client);
  }

  @SubscribeMessage('data.subscribe')
  public async onSubscribeData(
    @MessageBody() dto: ExpeditionsQueryParamsDto,
    @ConnectedSocket() client: Socket,
  ): Promise<AppWsResponse | WsException> {
    try {
      const data = await this._dataService.getPlanningLines(dto);
      this._stateService.pushState(dto, data, client);

      return new AppWsSuccessResponse<IExpeditionLine[]>(data);
    } catch (err) {
      return new AppWsInternalServerErrorException(
        'Une erreur est survenue lors de la récupération des données du planning',
      );
    }
  }

  public emitLineUpdate(sockets: Socket[], line: IExpeditionLine): void {
    sockets.forEach((socket) => socket.emit('data.update', line));
  }
}
