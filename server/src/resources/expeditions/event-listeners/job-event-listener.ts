import { Injectable } from '@nestjs/common';
import { JobEventDispatcher } from '../../../shared/event-dispatchers';
import { IEventListener } from '../../../shared/interfaces';
import { JobDocument } from '../../jobs/schemas';
import {
  ExpeditionLineMappingService,
  ExpeditionsStateService,
} from '../services';
import { ExpeditionsGateway } from '../expeditions.gateway';

@Injectable()
export class JobEventListener implements IEventListener {
  constructor(
    eventDispatcher: JobEventDispatcher,
    private _stateService: ExpeditionsStateService,
    private _mappingService: ExpeditionLineMappingService,
    private _gateway: ExpeditionsGateway,
  ) {
    eventDispatcher.addListener(this);
  }

  async on(name: string, job: JobDocument): Promise<void> {
    if (this._stateService.isEmpty) {
      return;
    }

    for (const state of this._stateService.states) {
      const newLine = this._mappingService.getMappedData(job);
      const newStateDataItem = this._stateService.getMappedDataItem(newLine);

      const isMatchingDate =
        job.mp.dates.sending === state.queryParams.sendingDate ||
        job.meta.deliverySendingDates.includes(state.queryParams.sendingDate);
      const stateDataItemIndex = state.data.findIndex(
        (item) => item.jobNumber === job.mp.number,
      );
      const isInState = stateDataItemIndex !== -1;
      const hasSearch = !!state.queryParams.fullSearch;
      const isArchived = job.mp.statusNumber > 710;
      const isDifferent =
        newStateDataItem.hash !== state.data[stateDataItemIndex]?.hash;

      if (hasSearch && isInState) {
        newLine.syncAction = 'update';
        this._gateway.emitLineUpdate(state.sockets, newLine);
        state.data[stateDataItemIndex] = newStateDataItem;

        continue;
      }

      if ((!isMatchingDate && isInState) || !job.deliveryAddresses?.length) {
        newLine.syncAction = 'remove';
        this._gateway.emitLineUpdate(state.sockets, newLine);

        state.data.splice(stateDataItemIndex, 1);

        continue;
      }

      if (isMatchingDate && !isInState && !isArchived) {
        newLine.syncAction = 'add';
        this._gateway.emitLineUpdate(state.sockets, newLine);
        state.data.push(newStateDataItem);

        continue;
      }

      if (isMatchingDate && isDifferent) {
        if (isArchived) {
          newLine.syncAction = 'remove';
          this._gateway.emitLineUpdate(state.sockets, newLine);

          state.data.splice(stateDataItemIndex, 1);
        } else {
          newLine.syncAction = 'update';
          this._gateway.emitLineUpdate(state.sockets, newLine);
          state.data[stateDataItemIndex] = newStateDataItem;
        }
      }
    }
  }
}
