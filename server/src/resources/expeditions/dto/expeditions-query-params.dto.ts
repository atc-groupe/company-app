import { IsOptional, IsString } from 'class-validator';

export class ExpeditionsQueryParamsDto {
  @IsOptional()
  @IsString()
  sendingDate: string;

  @IsOptional()
  @IsString()
  fullSearch: string;

  endStatusNumber?: number;
}
