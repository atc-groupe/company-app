import { Injectable } from '@nestjs/common';
import { ExpeditionsStateService } from '../services';
import { JobSyncByDeliveryAddressesService } from '../../jobs/sync/job-sync-by-delivery-addresses.service';
import { Cron } from '@nestjs/schedule';
import { ConfigService } from '@nestjs/config';
import { Logger } from '../../../shared/services';
import { LoggerFactory } from '../../../shared/factories/logger.factory';
import { LogContextEnum } from '../../../shared/enum';

@Injectable()
export class ExpeditionsSyncTaskService {
  private _disabled = false;
  private _logger: Logger;

  constructor(
    _loggerFactory: LoggerFactory,
    private _statesService: ExpeditionsStateService,
    private _jobDeliverySyncService: JobSyncByDeliveryAddressesService,
    private _configService: ConfigService,
  ) {
    this._logger = _loggerFactory.get(LogContextEnum.expeditions);
    const ALL_TASKS = this._configService.get('ALL_TASKS');
    const EXPEDITIONS_TASKS = this._configService.get('EXPEDITIONS_TASKS');

    if (ALL_TASKS === 'off' || EXPEDITIONS_TASKS === 'off') {
      this._disabled = true;
    }
  }

  @Cron('0/30 * 6-20 * * *')
  public async syncJobsDeliveryAddresses(): Promise<void> {
    if (this._statesService.isEmpty) {
      return;
    }

    if (this._disabled) {
      this._logger.log(
        `[ExpeditionsSyncTaskService.syncJobsDeliveryAddresses] task skipped. See .env file`,
      );

      return;
    }

    const mpNumbers = new Set<number>();
    this._statesService.states.forEach((state) => {
      state.data.forEach((el) => mpNumbers.add(el.jobNumber));
    });

    for (const mpNumber of mpNumbers.values()) {
      await this._jobDeliverySyncService.syncByDeliveryAddresses(mpNumber);
    }
  }
}
