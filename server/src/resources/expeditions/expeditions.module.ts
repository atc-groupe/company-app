import { Module } from '@nestjs/common';
import {
  ExpeditionLineMappingService,
  ExpeditionsDataService,
  ExpeditionsHashService,
  ExpeditionsStateService,
} from './services';
import { ExpeditionsGateway } from './expeditions.gateway';
import { JobsModule } from '../jobs/jobs.module';
import { JobEventListener } from './event-listeners/job-event-listener';
import { EventDispatchersModule } from '../../shared/modules/event-dispatchers.module';
import { ExpeditionsSyncTaskService } from './tasks/expeditions-sync-task.service';

@Module({
  imports: [JobsModule, EventDispatchersModule],
  providers: [
    ExpeditionLineMappingService,
    ExpeditionsDataService,
    ExpeditionsHashService,
    ExpeditionsStateService,
    ExpeditionsGateway,
    JobEventListener,
    ExpeditionsSyncTaskService,
  ],
})
export class ExpeditionsModule {}
