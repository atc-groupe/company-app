import { Injectable } from '@nestjs/common';
import { createHash } from 'node:crypto';
import { IExpeditionLine } from '../interfaces';

@Injectable()
export class ExpeditionsHashService {
  public getLineHash(line: IExpeditionLine): string {
    const data = { ...line };
    delete data.syncAction;

    return createHash('sha256').update(JSON.stringify(data)).digest('hex');
  }
}
