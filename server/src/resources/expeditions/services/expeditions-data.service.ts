import { Injectable } from '@nestjs/common';
import { JobsService } from '../../jobs/services';
import { ExpeditionsQueryParamsDto } from '../dto/expeditions-query-params.dto';
import { firstValueFrom, map } from 'rxjs';
import { ExpeditionLineMappingService } from './expedition-line-mapping.service';
import { IExpeditionLine } from '../interfaces';

@Injectable()
export class ExpeditionsDataService {
  constructor(
    private _jobsService: JobsService,
    private _mappingService: ExpeditionLineMappingService,
  ) {}

  public async getPlanningLines(
    dto: ExpeditionsQueryParamsDto,
  ): Promise<IExpeditionLine[]> {
    if (!dto.fullSearch) {
      dto.endStatusNumber = 710;
    }

    return await firstValueFrom(
      this._jobsService.find(dto).pipe(
        map((jobs) =>
          jobs.filter(
            (job) => job.deliveryAddresses && job.deliveryAddresses.length > 0,
          ),
        ),
        map((jobs) =>
          jobs.map((job) => this._mappingService.getMappedData(job)),
        ),
      ),
    );
  }
}
