import { Injectable } from '@nestjs/common';
import {
  IExpeditionLine,
  IExpeditionsState,
  IExpeditionsStateDataItem,
} from '../interfaces';
import { ExpeditionsQueryParamsDto } from '../dto/expeditions-query-params.dto';
import { Socket } from 'socket.io';
import { ExpeditionsHashService } from './expeditions-hash.service';

@Injectable()
export class ExpeditionsStateService {
  private _states: IExpeditionsState[] = [];

  constructor(private _hashService: ExpeditionsHashService) {}

  public get isEmpty(): boolean {
    return this._states.length === 0;
  }

  public get states(): IExpeditionsState[] {
    return this._states;
  }

  public pushState(
    queryParams: ExpeditionsQueryParamsDto,
    lines: IExpeditionLine[],
    socket: Socket,
  ): void {
    this.removeSocket(socket);

    const state = this._getStateFromQueryParams(queryParams);

    if (state) {
      state.sockets.push(socket);
    } else {
      const data = lines.map((item) => this.getMappedDataItem(item));

      this._states.push({
        queryParams,
        data,
        sockets: [socket],
      });
    }
  }

  public removeSocket(socket: Socket): void {
    this._states = this._states.filter((state) => {
      state.sockets = state.sockets.filter((item) => item.id !== socket.id);

      return state.sockets.length > 0;
    });
  }

  public getMappedDataItem(line: IExpeditionLine): IExpeditionsStateDataItem {
    return {
      jobNumber: line.jobNumber,
      hash: this._hashService.getLineHash(line),
    };
  }

  private _getStateFromQueryParams(
    dto: ExpeditionsQueryParamsDto,
  ): IExpeditionsState | null {
    const state = this._states.find(
      ({ queryParams }) => JSON.stringify(queryParams) === JSON.stringify(dto),
    );

    return state ? state : null;
  }
}
