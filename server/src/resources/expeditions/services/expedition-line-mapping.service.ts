import { Injectable } from '@nestjs/common';
import { JobDocument } from '../../jobs/schemas';
import { IExpeditionLine } from '../interfaces';

@Injectable()
export class ExpeditionLineMappingService {
  public getMappedData(job: JobDocument): IExpeditionLine {
    return {
      _id: job._id.toString(),
      jobNumber: job.mp.number,
      jobStatusNumber: job.mp.statusNumber,
      company: job.mp.company,
      description: job.mp.description,
      jobManager: job.mp.jobManager,
      devices: job.meta.productionTypes,
      printingSurface: job.meta.printingSurface,
      manufacturedPiecesCount: job.meta.manufacturedPiecesCount,
      hasSheetProduction: job.meta.hasSheetProduction,
      hasSubcontractingProduction: job.meta.hasSubcontractingProduction,
      deliveryServiceFinishs: job.meta.deliveryServiceFinishes,
      deliverySendingDates: job.meta.deliverySendingDates,
      addressZipCodes: job.meta.addressZipCodes,
      deliveryMethods: job.meta.deliveryMethods,
      comment: job.comments.delivery,
      sync: job.sync,
    };
  }
}
