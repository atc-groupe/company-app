import { IsNumber, IsString } from 'class-validator';

export class SettingsJobStatusDto {
  @IsNumber()
  statusNumber: number;

  @IsString()
  label: string;

  @IsString()
  bgColor: string;

  @IsString()
  textColor: string;
}
