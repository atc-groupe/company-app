import { IsNumber, IsString } from 'class-validator';

export class SettingsPlanningViewDto {
  @IsString()
  name: string;

  @IsNumber()
  planningDepartment: number;

  @IsString()
  planningGroup: string;
}
