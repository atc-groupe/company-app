import { IsString } from 'class-validator';

export class SettingsJobMappingMetaFilterDto {
  @IsString()
  name: string;

  @IsString()
  value: string;
}
