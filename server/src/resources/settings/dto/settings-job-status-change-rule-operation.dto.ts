import { IsNumber, IsString } from 'class-validator';

export class SettingsJobStatusChangeRuleOperationDto {
  @IsString()
  operation: string;

  @IsNumber()
  operationNumber: number;
}
