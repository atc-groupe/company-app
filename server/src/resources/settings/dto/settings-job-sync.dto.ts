import { IsNumber, IsOptional } from 'class-validator';

export class SettingsJobSyncDto {
  @IsNumber()
  @IsOptional()
  syncInterval: number;
}
