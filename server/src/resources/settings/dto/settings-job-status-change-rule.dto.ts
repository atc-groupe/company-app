import { IsBoolean, IsEnum, IsNumber, IsString } from 'class-validator';
import { JobStatusChangeRuleTypeEnum } from '../enums/job-status-change-rule-type.enum';

export class SettingsJobStatusChangeRuleDto {
  @IsEnum(JobStatusChangeRuleTypeEnum)
  type: JobStatusChangeRuleTypeEnum;

  @IsString()
  fromStatusText: string;

  @IsNumber()
  fromStatusNumber: number;

  @IsBoolean()
  canCancel: boolean;
}
