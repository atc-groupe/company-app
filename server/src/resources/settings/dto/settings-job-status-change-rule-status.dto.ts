import { IsNumber, IsString } from 'class-validator';

export class SettingsJobStatusChangeRuleStatusDto {
  @IsString()
  statusText: string;

  @IsNumber()
  statusNumber: number;
}
