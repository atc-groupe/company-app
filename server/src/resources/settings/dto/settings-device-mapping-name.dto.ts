import { IsString } from 'class-validator';

export class SettingsDeviceMappingNameDto {
  @IsString()
  mpName: string;

  @IsString()
  displayName: string;
}
