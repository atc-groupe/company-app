import { WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Server } from 'socket.io';
import { Settings } from './schemas';
import { UseGuards } from '@nestjs/common';
import { AuthGatewayGuard } from '../auth/auth-gateway.guard';

@UseGuards(AuthGatewayGuard)
@WebSocketGateway({ transports: ['websocket'], namespace: 'app-settings' })
export class SettingsGateway {
  @WebSocketServer() private _server: Server;

  emitUpdate(settings: Settings | null): void {
    if (settings) {
      this._server.emit('update', settings);
    }
  }
}
