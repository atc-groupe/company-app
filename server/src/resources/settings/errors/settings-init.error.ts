import { Error } from 'mongoose';

export class SettingsInitError extends Error {
  constructor(message: string) {
    super(message);
  }
}
