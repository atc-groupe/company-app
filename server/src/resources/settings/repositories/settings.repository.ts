import { Injectable } from '@nestjs/common';
import { Settings, SettingsDocument } from '../schemas';
import { defer, Observable, switchMap } from 'rxjs';
import { SettingsInitError } from '../errors/settings-init.error';
import { AbstractSettingsRepository } from './abstract-settings.repository';

@Injectable()
export class SettingsRepository extends AbstractSettingsRepository {
  public findUnique(): Observable<SettingsDocument | null> {
    return defer(() =>
      this._settingsModel
        .findOne({ identifier: Settings.id })
        .select('-__v')
        .exec(),
    );
  }

  public initialize(): Observable<SettingsDocument> {
    return this.findUnique().pipe(
      switchMap((settings) => {
        if (settings) {
          throw new SettingsInitError(
            'App settings has already been initialized',
          );
        }

        return defer(() =>
          this._settingsModel.create({
            job: { statuses: {}, mapping: { metaFilters: {} } },
            devices: { mapping: {} },
            planning: {},
          }),
        );
      }),
    );
  }
}
