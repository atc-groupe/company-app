import { Injectable } from '@nestjs/common';
import { Settings, SettingsDocument } from '../schemas';
import { defer, Observable, tap } from 'rxjs';
import { SettingsJobStatusDto } from '../dto';
import { AbstractSettingsRepository } from './abstract-settings.repository';

@Injectable()
export class SettingsJobStatusesRepository extends AbstractSettingsRepository {
  public setDefaultColor(color: string): Observable<SettingsDocument | null> {
    return defer(() =>
      this._settingsModel
        .findOneAndUpdate(
          { identifier: Settings.id },
          { 'job.statuses.defaultColor': color },
          { new: true, runValidators: true },
        )
        .select('-__v')
        .exec(),
    ).pipe(
      tap((settings) =>
        this._eventDispatcher.dispatchEvent(
          'update:job.statuses.defaultColor',
          settings,
        ),
      ),
    );
  }

  public addStatus(
    dto: SettingsJobStatusDto,
  ): Observable<SettingsDocument | null> {
    return defer(() =>
      this._settingsModel
        .findOneAndUpdate(
          { identifier: Settings.id },
          { $addToSet: { 'job.statuses.items': dto } },
          { new: true, runValidators: true },
        )
        .select('-__v')
        .exec(),
    ).pipe(
      tap((settings) =>
        this._eventDispatcher.dispatchEvent(
          'create:job.statuses.items',
          settings,
        ),
      ),
    );
  }

  public updateStatus(
    id: string,
    dto: SettingsJobStatusDto,
  ): Observable<SettingsDocument | null> {
    return defer(() =>
      this._settingsModel
        .findOneAndUpdate(
          { identifier: Settings.id, 'job.statuses.items._id': id },
          { $set: { 'job.statuses.items.$': dto } },
          { new: true, runValidators: true },
        )
        .select('-__v')
        .exec(),
    ).pipe(
      tap((settings) =>
        this._eventDispatcher.dispatchEvent(
          'update:job.statuses.items',
          settings,
        ),
      ),
    );
  }

  public removeStatus(id: string): Observable<SettingsDocument | null> {
    return defer(() =>
      this._settingsModel
        .findOneAndUpdate(
          { identifier: Settings.id },
          { $pull: { 'job.statuses.items': { _id: id } } },
          { new: true, runValidators: true },
        )
        .select('-__v')
        .exec(),
    ).pipe(
      tap((settings) =>
        this._eventDispatcher.dispatchEvent(
          'delete:job.statuses.items',
          settings,
        ),
      ),
    );
  }
}
