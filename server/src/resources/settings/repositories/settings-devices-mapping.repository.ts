import { Injectable } from '@nestjs/common';
import { Settings, SettingsDocument } from '../schemas';
import { SettingsDeviceMappingNameDto } from '../dto';
import { defer, Observable, tap } from 'rxjs';
import { AbstractSettingsRepository } from './abstract-settings.repository';

@Injectable()
export class SettingsDevicesMappingRepository extends AbstractSettingsRepository {
  public addName(
    dto: SettingsDeviceMappingNameDto,
  ): Observable<SettingsDocument | null> {
    return defer(() =>
      this._settingsModel.findOneAndUpdate(
        { identifier: Settings.id },
        { $addToSet: { 'devices.mapping.names': dto } },
        { new: true, runValidators: true },
      ),
    ).pipe(
      tap((settings) =>
        this._eventDispatcher.dispatchEvent(
          'create:devices.mapping.names',
          settings,
        ),
      ),
    );
  }

  public removeName(mpName: string): Observable<SettingsDocument | null> {
    return defer(() =>
      this._settingsModel.findOneAndUpdate(
        { identifier: Settings.id },
        { $pull: { 'devices.mapping.names': { mpName } } },
        { new: true, runValidators: true },
      ),
    ).pipe(
      tap((settings) =>
        this._eventDispatcher.dispatchEvent(
          'delete:devices.mapping.names',
          settings,
        ),
      ),
    );
  }
}
