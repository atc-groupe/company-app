import { Injectable } from '@nestjs/common';
import { AbstractSettingsRepository } from './abstract-settings.repository';
import { Settings, SettingsDocument } from '../schemas';
import { defer, Observable, tap } from 'rxjs';
import { SettingsPlanningViewDto } from '../dto';

@Injectable()
export class SettingsPlanningViewsRepository extends AbstractSettingsRepository {
  public createOne(
    dto: SettingsPlanningViewDto,
  ): Observable<SettingsDocument | null> {
    return defer(() =>
      this._settingsModel.findOneAndUpdate(
        { identifier: Settings.id },
        {
          $push: {
            'planning.views': {
              $each: [dto],
              $sort: { planningGroup: 1, name: 1 },
            },
          },
        },
        { new: true, runValidators: true },
      ),
    ).pipe(
      tap((settings) =>
        this._eventDispatcher.dispatchEvent('create:planning.view', settings),
      ),
    );
  }

  public updateOne(
    id: string,
    dto: SettingsPlanningViewDto,
  ): Observable<SettingsDocument | null> {
    return defer(() =>
      this._settingsModel.findOneAndUpdate(
        { identifier: Settings.id, 'planning.views._id': id },
        {
          $set: {
            'planning.views.$.name': dto.name,
            'planning.views.$.planningDepartment': dto.planningDepartment,
            'planning.views.$.planningGroup': dto.planningGroup,
          },
        },
        { new: true, runValidators: true },
      ),
    ).pipe(
      tap((settings) =>
        this._eventDispatcher.dispatchEvent('update:planning.view', settings),
      ),
    );
  }

  public removeOne(id: string): Observable<SettingsDocument | null> {
    return defer(() =>
      this._settingsModel.findOneAndUpdate(
        { identifier: Settings.id },
        {
          $pull: {
            'planning.views': { _id: id },
          },
        },
        { new: true, runValidators: true },
      ),
    ).pipe(
      tap((settings) =>
        this._eventDispatcher.dispatchEvent('delete:planning.view', settings),
      ),
    );
  }

  public addMachineId(
    id: string,
    machineId: number,
  ): Observable<SettingsDocument | null> {
    return defer(() =>
      this._settingsModel.findOneAndUpdate(
        { identifier: Settings.id, 'planning.views._id': id },
        { $addToSet: { 'planning.views.$.machineIds': machineId } },
        { new: true, runValidators: true },
      ),
    ).pipe(
      tap((settings) =>
        this._eventDispatcher.dispatchEvent(
          'create:planning.view.machineId',
          settings,
        ),
      ),
    );
  }

  public removeMachineId(
    id: string,
    machineId: number,
  ): Observable<SettingsDocument | null> {
    return defer(() =>
      this._settingsModel.findOneAndUpdate(
        { identifier: Settings.id, 'planning.views._id': id },
        { $pull: { 'planning.views.$.machineIds': machineId } },
        { new: true, runValidators: true },
      ),
    ).pipe(
      tap((settings) =>
        this._eventDispatcher.dispatchEvent(
          'delete:planning.view.machineId',
          settings,
        ),
      ),
    );
  }
}
