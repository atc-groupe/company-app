import { InjectModel } from '@nestjs/mongoose';
import { Settings } from '../schemas';
import { Model } from 'mongoose';
import { SettingsEventDispatcher } from '../../../shared/event-dispatchers';

export abstract class AbstractSettingsRepository {
  constructor(
    @InjectModel(Settings.name) protected _settingsModel: Model<Settings>,
    protected _eventDispatcher: SettingsEventDispatcher,
  ) {}
}
