import { Injectable } from '@nestjs/common';
import { AbstractSettingsRepository } from './abstract-settings.repository';
import { defer, Observable, tap } from 'rxjs';
import { Settings, SettingsDocument } from '../schemas';
import {
  SettingsJobStatusChangeRuleDto,
  SettingsJobStatusChangeRuleOperationDto,
  SettingsJobStatusChangeRuleStatusDto,
} from '../dto';

@Injectable()
export class SettingsAutomationRepository extends AbstractSettingsRepository {
  public createJobStatusChangeRule(
    dto: SettingsJobStatusChangeRuleDto,
  ): Observable<SettingsDocument | null> {
    return defer(() =>
      this._settingsModel.findOneAndUpdate(
        { identifier: Settings.id },
        {
          $push: { 'automation.jobStatusChangeRules': dto },
        },
        { runValidators: true, new: true },
      ),
    ).pipe(
      tap((settings) =>
        this._eventDispatcher.dispatchEvent(
          'create:automation.jobStatusChangeRule',
          settings,
        ),
      ),
    );
  }

  public updateJobStatusChangeRule(
    ruleId: string,
    dto: SettingsJobStatusChangeRuleDto,
  ): Observable<SettingsDocument | null> {
    return defer(() =>
      this._settingsModel.findOneAndUpdate(
        {
          identifier: Settings.id,
          'automation.jobStatusChangeRules._id': ruleId,
        },
        {
          $set: {
            'automation.jobStatusChangeRules.$.type': dto.type,
            'automation.jobStatusChangeRules.$.fromStatusText':
              dto.fromStatusText,
            'automation.jobStatusChangeRules.$.fromStatusNumber':
              dto.fromStatusNumber,
            'automation.jobStatusChangeRules.$.canCancel': dto.canCancel,
          },
        },
        { runValidators: true, new: true },
      ),
    ).pipe(
      tap((settings) =>
        this._eventDispatcher.dispatchEvent(
          'create:automation.jobStatusChangeRule',
          settings,
        ),
      ),
    );
  }

  public deleteJobStatusChangeRule(
    ruleId: string,
  ): Observable<SettingsDocument | null> {
    return defer(() =>
      this._settingsModel.findOneAndUpdate(
        {
          identifier: Settings.id,
        },
        {
          $pull: { 'automation.jobStatusChangeRules': { _id: ruleId } },
        },
        { runValidators: true, new: true },
      ),
    ).pipe(
      tap((settings) =>
        this._eventDispatcher.dispatchEvent(
          'delete:automation.jobStatusChangeRule',
          settings,
        ),
      ),
    );
  }

  public createJobStatusChangeRuleOperation(
    ruleId: string,
    dto: SettingsJobStatusChangeRuleOperationDto,
  ): Observable<SettingsDocument | null> {
    return defer(() =>
      this._settingsModel.findOneAndUpdate(
        {
          identifier: Settings.id,
          'automation.jobStatusChangeRules._id': ruleId,
        },
        {
          $push: {
            'automation.jobStatusChangeRules.$.operations': {
              $each: [dto],
              $sort: { operationNumber: 1 },
            },
          },
        },
        { runValidators: true, new: true },
      ),
    ).pipe(
      tap((settings) =>
        this._eventDispatcher.dispatchEvent(
          'create:automation.jobStatusChangeRule.operation',
          settings,
        ),
      ),
    );
  }

  public deleteJobStatusChangeRuleOperation(
    ruleId: string,
    operationId: string,
  ): Observable<SettingsDocument | null> {
    return defer(() =>
      this._settingsModel.findOneAndUpdate(
        {
          identifier: Settings.id,
          'automation.jobStatusChangeRules._id': ruleId,
        },
        {
          $pull: {
            'automation.jobStatusChangeRules.$.operations': {
              _id: operationId,
            },
          },
        },
        { runValidators: true, new: true },
      ),
    ).pipe(
      tap((settings) =>
        this._eventDispatcher.dispatchEvent(
          'delete:automation.jobStatusChangeRule.operation',
          settings,
        ),
      ),
    );
  }

  public addJobStatusChangeRulesStatus(
    ruleId: string,
    dto: SettingsJobStatusChangeRuleStatusDto,
  ): Observable<SettingsDocument | null> {
    return defer(() =>
      this._settingsModel.findOneAndUpdate(
        {
          identifier: Settings.id,
          'automation.jobStatusChangeRules._id': ruleId,
        },
        {
          $push: {
            'automation.jobStatusChangeRules.$.toStatuses': {
              $each: [dto],
              $sort: { statusNumber: 1 },
            },
          },
        },
        { runValidators: true, new: true },
      ),
    ).pipe(
      tap((settings) =>
        this._eventDispatcher.dispatchEvent(
          'create:automation.jobStatusChangeRule.status',
          settings,
        ),
      ),
    );
  }

  public removeJobStatusChangeRuleStatus(
    ruleId: string,
    statusId: string,
  ): Observable<SettingsDocument | null> {
    return defer(() =>
      this._settingsModel.findOneAndUpdate(
        {
          identifier: Settings.id,
          'automation.jobStatusChangeRules._id': ruleId,
        },
        {
          $pull: {
            'automation.jobStatusChangeRules.$.toStatuses': {
              _id: statusId,
            },
          },
        },
        { runValidators: true, new: true },
      ),
    ).pipe(
      tap((settings) =>
        this._eventDispatcher.dispatchEvent(
          'delete:automation.jobStatusChangeRule.status',
          settings,
        ),
      ),
    );
  }
}
