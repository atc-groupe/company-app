import { Injectable } from '@nestjs/common';
import { Settings, SettingsDocument } from '../schemas';
import { SettingsJobSyncDto } from '../dto';
import { defer, Observable, tap } from 'rxjs';
import { AbstractSettingsRepository } from './abstract-settings.repository';

@Injectable()
export class SettingsJobSyncRepository extends AbstractSettingsRepository {
  public update(dto: SettingsJobSyncDto): Observable<SettingsDocument | null> {
    return defer(() =>
      this._settingsModel
        .findOneAndUpdate(
          { identifier: Settings.id },
          { 'job.sync': dto },
          {
            new: true,
            runValidators: true,
          },
        )
        .select('-__v')
        .exec(),
    ).pipe(
      tap((settings) =>
        this._eventDispatcher.dispatchEvent('update:job.sync', settings),
      ),
    );
  }
}
