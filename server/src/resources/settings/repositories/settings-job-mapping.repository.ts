import { Injectable } from '@nestjs/common';
import { Settings, SettingsDocument } from '../schemas';
import { SettingsJobMappingMetaFilterDto } from '../dto';
import { defer, Observable, tap } from 'rxjs';
import { AbstractSettingsRepository } from './abstract-settings.repository';

@Injectable()
export class SettingsJobMappingRepository extends AbstractSettingsRepository {
  public addMetaFilter(
    dto: SettingsJobMappingMetaFilterDto,
  ): Observable<SettingsDocument | null> {
    return defer(() =>
      this._settingsModel
        .findOneAndUpdate(
          { identifier: Settings.id },
          {
            $push: {
              [`job.mapping.metaFilters.${dto.name}.items`]: dto.value,
            },
          },
          { new: true, runValidators: true },
        )
        .select('-__v')
        .exec(),
    ).pipe(
      tap((settings) =>
        this._eventDispatcher.dispatchEvent(
          'create:job.mapping.metaFilters',
          settings,
        ),
      ),
    );
  }

  public removeMetaFilter(
    dto: SettingsJobMappingMetaFilterDto,
  ): Observable<SettingsDocument | null> {
    return defer(() =>
      this._settingsModel
        .findOneAndUpdate(
          { identifier: Settings.id },
          {
            $pull: { [`job.mapping.metaFilters.${dto.name}.items`]: dto.value },
          },
          { new: true, runValidators: true },
        )
        .select('-__v')
        .exec(),
    ).pipe(
      tap((settings) =>
        this._eventDispatcher.dispatchEvent(
          'delete:job.mapping.metaFilters',
          settings,
        ),
      ),
    );
  }
}
