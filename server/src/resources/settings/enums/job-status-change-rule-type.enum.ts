export enum JobStatusChangeRuleTypeEnum {
  onStartFirst = 'onStartFirst',
  onStartOne = 'onStartOne',
  onCompleteLast = 'onCompleteLast',
  onStopOne = 'onStopOne',
  onDeleteOne = 'onDeleteOne',
  onDeleteUnique = 'onDeleteUnique',
}
