import { Injectable } from '@nestjs/common';
import { IEventListener } from '../../shared/interfaces';
import { SettingsEventDispatcher } from '../../shared/event-dispatchers';
import { SettingsGateway } from './settings.gateway';
import { Settings } from './schemas';

@Injectable()
export class SettingsEventListener implements IEventListener {
  constructor(
    eventDispatcher: SettingsEventDispatcher,
    private _gateway: SettingsGateway,
  ) {
    eventDispatcher.addListener(this);
  }

  on(name: string, settings: Settings): any {
    this._gateway.emitUpdate(settings);
  }
}
