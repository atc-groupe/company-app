import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema()
export class SettingsPlanningView {
  @Prop()
  name: string;

  @Prop()
  planningDepartment: number;

  @Prop()
  planningGroup: string;

  @Prop({ type: [{ type: Number }], default: [] })
  machineIds: number[];
}

export const SettingsPlanningViewSchema =
  SchemaFactory.createForClass(SettingsPlanningView);
