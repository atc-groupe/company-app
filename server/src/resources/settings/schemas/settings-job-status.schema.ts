import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema()
export class SettingsJobStatus {
  @Prop()
  statusNumber: number;

  @Prop()
  label: string;

  @Prop()
  bgColor: string;

  @Prop()
  textColor: 'white' | 'black';
}

export const SettingsJobStatusSchema =
  SchemaFactory.createForClass(SettingsJobStatus);
