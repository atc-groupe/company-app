import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import {
  SettingsJobStatuses,
  SettingsJobStatusesSchema,
} from './settings-job-statuses.schema';
import {
  SettingsJobMapping,
  SettingsJobMappingSchema,
} from './settings-job-mapping.schema';
import {
  SettingsJobSync,
  SettingsJobSyncSchema,
} from './settings-job-sync.schema';

@Schema({ _id: false })
export class SettingsJob {
  @Prop({ type: SettingsJobStatusesSchema })
  statuses: SettingsJobStatuses;

  @Prop({ type: SettingsJobMappingSchema })
  mapping: SettingsJobMapping;

  @Prop({ type: SettingsJobSyncSchema, default: {} })
  sync: SettingsJobSync;
}

export const SettingsJobSchema = SchemaFactory.createForClass(SettingsJob);
