import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({ _id: false })
export class SettingsJobSync {
  @Prop({ type: Number, default: 60 })
  syncInterval: number; // In seconds
}

export const SettingsJobSyncSchema =
  SchemaFactory.createForClass(SettingsJobSync);
