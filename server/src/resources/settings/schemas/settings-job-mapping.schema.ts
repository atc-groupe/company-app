import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import {
  SettingsJobMappingMetaFilters,
  SettingsJobMappingMetaFiltersSchema,
} from './settings-job-mapping-meta-filters.schema';

@Schema({ _id: false })
export class SettingsJobMapping {
  @Prop({ type: SettingsJobMappingMetaFiltersSchema })
  metaFilters: SettingsJobMappingMetaFilters;
}

export const SettingsJobMappingSchema =
  SchemaFactory.createForClass(SettingsJobMapping);
