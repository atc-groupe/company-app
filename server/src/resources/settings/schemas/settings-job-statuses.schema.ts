import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import {
  SettingsJobStatus,
  SettingsJobStatusSchema,
} from './settings-job-status.schema';

@Schema({ _id: false })
export class SettingsJobStatuses {
  @Prop({ type: String, default: '#888888' })
  defaultColor: string;

  @Prop({ type: [{ type: SettingsJobStatusSchema }], default: [] })
  items: SettingsJobStatus[];
}

export const SettingsJobStatusesSchema =
  SchemaFactory.createForClass(SettingsJobStatuses);
