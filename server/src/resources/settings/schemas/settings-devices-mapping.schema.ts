import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import {
  SettingsDeviceMappingName,
  SettingsDevicesMappingNameSchema,
} from './settings-devices-mapping-name.schema';

@Schema({ _id: false })
export class SettingsDevicesMapping {
  @Prop({
    type: [{ type: SettingsDevicesMappingNameSchema }],
    default: [
      { mpName: 'Kudu – Plaques', displayName: 'Kudu' },
      { mpName: 'Kudu – Rouleaux', displayName: 'Kudu' },
      { mpName: 'Vutek 5R - 1370', displayName: 'Vutek 5R' },
      { mpName: 'Vutek 5R - 5000', displayName: 'Vutek 5R' },
    ],
  })
  names: SettingsDeviceMappingName[];
}

export const SettingsDevicesMappingSchema = SchemaFactory.createForClass(
  SettingsDevicesMapping,
);
