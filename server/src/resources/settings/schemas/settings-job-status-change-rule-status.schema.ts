import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema()
export class SettingsJobStatusChangeRuleStatus {
  @Prop()
  statusText: string;

  @Prop()
  statusNumber: number;
}

export const SettingsJobStatusChangeRuleStatusSchema =
  SchemaFactory.createForClass(SettingsJobStatusChangeRuleStatus);
