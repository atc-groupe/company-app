import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import {
  SettingsPlanningView,
  SettingsPlanningViewSchema,
} from './settings-planning-view.schema';

@Schema({ _id: false })
export class SettingsPlanning {
  @Prop({ type: [{ type: SettingsPlanningViewSchema }], default: [] })
  views: SettingsPlanningView[];
}

export const SettingsPlanningSchema =
  SchemaFactory.createForClass(SettingsPlanning);
