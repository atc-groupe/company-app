import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { SettingsJob, SettingsJobSchema } from './settings-job.schema';
import { HydratedDocument } from 'mongoose';
import {
  SettingsDevices,
  SettingsDevicesSchema,
} from './settings-devices.schema';
import {
  SettingsAutomation,
  SettingsAutomationSchema,
} from './settings-automation.schema';
import {
  SettingsPlanning,
  SettingsPlanningSchema,
} from './settings-planning.schema';

@Schema()
export class Settings {
  public static id = 'SETTINGS';

  @Prop({ type: String, default: Settings.id, unique: true })
  identifier: string;

  @Prop({ type: SettingsAutomationSchema, default: {} })
  automation: SettingsAutomation;

  @Prop({ type: SettingsJobSchema })
  job: SettingsJob;

  @Prop({ type: SettingsDevicesSchema })
  devices: SettingsDevices;

  @Prop({ type: SettingsPlanningSchema, default: {} })
  planning: SettingsPlanning;
}

export const SettingsSchema = SchemaFactory.createForClass(Settings);
export type SettingsDocument = HydratedDocument<Settings>;
