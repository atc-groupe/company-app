import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({ _id: false })
export class SettingsDeviceMappingName {
  @Prop()
  mpName: string;

  @Prop()
  displayName: string;
}

export const SettingsDevicesMappingNameSchema = SchemaFactory.createForClass(
  SettingsDeviceMappingName,
);
