import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import {
  settingsJobStatusChangeRuleOperation,
  SettingsJobStatusChangeRuleOperationSchema,
} from './settings-job-status-change-rule-operation.schema';
import {
  SettingsJobStatusChangeRuleStatus,
  SettingsJobStatusChangeRuleStatusSchema,
} from './settings-job-status-change-rule-status.schema';
import { JobStatusChangeRuleTypeEnum } from '../enums/job-status-change-rule-type.enum';

@Schema()
export class SettingsJobStatusChangeRule {
  @Prop({ type: String, enum: JobStatusChangeRuleTypeEnum })
  type: JobStatusChangeRuleTypeEnum;

  @Prop()
  fromStatusText: string;

  @Prop()
  fromStatusNumber: number;

  @Prop({
    type: [{ type: SettingsJobStatusChangeRuleStatusSchema }],
    default: [],
  })
  toStatuses: SettingsJobStatusChangeRuleStatus[];

  @Prop({
    type: [{ type: SettingsJobStatusChangeRuleOperationSchema }],
    default: [],
  })
  operations: settingsJobStatusChangeRuleOperation[];

  @Prop()
  canCancel: boolean;
}

export const SettingsJobStatusChangeRuleSchema = SchemaFactory.createForClass(
  SettingsJobStatusChangeRule,
);
