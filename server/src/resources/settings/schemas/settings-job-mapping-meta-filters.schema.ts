import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import {
  SettingsJobMappingMetaFilter,
  SettingsJobMappingMetaFilterSchema,
} from './settings-job-mapping-meta-filter.schema';

@Schema({ _id: false })
export class SettingsJobMappingMetaFilters {
  @Prop({
    type: SettingsJobMappingMetaFilterSchema,
    default: {
      title: 'Finitions service expéditions',
      items: ['Perçages', 'Pose bandes fixation', 'Pose double-face'],
    },
  })
  deliveryServiceFinishes: SettingsJobMappingMetaFilter;

  @Prop({
    type: SettingsJobMappingMetaFilterSchema,
    default: {
      title: 'Manipulations découpe zund',
      items: ['Rouleau - ZUND', 'Plaque - Coupe', 'Plaque - Perçage'],
    },
  })
  zundCuttingFinishes: SettingsJobMappingMetaFilter;

  @Prop({
    type: SettingsJobMappingMetaFilterSchema,
    default: {
      title: 'Manipulations découpe Summa',
      items: ['Semi découpe'],
    },
  })
  scoreFinishes: SettingsJobMappingMetaFilter;

  @Prop({
    type: SettingsJobMappingMetaFilterSchema,
    default: {
      title: 'Manipulations de mise sous kit',
      items: ['Mise sous Kit'],
    },
  })
  kitFinishes: SettingsJobMappingMetaFilter;

  @Prop({
    type: SettingsJobMappingMetaFilterSchema,
    default: {
      title: 'Devices de type support',
      description:
        'Devices utilisés comme support pour un autre layer numérique et dont le format ne doit donc pas être compté dans le format final pour le calcul de surface.',
      items: ['Support/matière', 'Contrecollage'],
    },
  })
  materialDevices: SettingsJobMappingMetaFilter;

  @Prop({
    type: SettingsJobMappingMetaFilterSchema,
    default: {
      title: 'Méthodes de livraison avec expédition',
      description:
        "Utilisé pour exclure les lignes d'expédition dont la méthode ne comporte pas d'expédition. Utilisé pour compter le nombre réel d'adresses de livraison",
      items: [
        'Affrêtement',
        'Course',
        'Expédition par sous traitant',
        'Express',
        'La Poste',
        'Messagerie',
      ],
    },
  })
  shippingDeliveryMethods: SettingsJobMappingMetaFilter;

  @Prop({
    type: SettingsJobMappingMetaFilterSchema,
    default: {
      title: 'Sous-traitants à ignorer',
      description:
        "Permet de mettre en évidence les jobs ayant de la sous-traitance en ignorant les sous-traitants de type transporteur ou autres n'ayant pas de rapport avec la production. Utilisé dans le résumé de la fabrication d'un job.",
      items: ['FRANCE EXPRESS GEODIS', 'PACK SERVICES.TOP SERVICES'],
    },
  })
  skippedSubcontractors: SettingsJobMappingMetaFilter;
}

export const SettingsJobMappingMetaFiltersSchema = SchemaFactory.createForClass(
  SettingsJobMappingMetaFilters,
);
