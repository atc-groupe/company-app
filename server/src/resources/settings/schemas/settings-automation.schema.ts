import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import {
  SettingsJobStatusChangeRule,
  SettingsJobStatusChangeRuleSchema,
} from './settings-job-status-change-rule.schema';

@Schema({ _id: false })
export class SettingsAutomation {
  @Prop({ type: [{ type: SettingsJobStatusChangeRuleSchema }], default: [] })
  jobStatusChangeRules: SettingsJobStatusChangeRule[];
}

export const SettingsAutomationSchema =
  SchemaFactory.createForClass(SettingsAutomation);
