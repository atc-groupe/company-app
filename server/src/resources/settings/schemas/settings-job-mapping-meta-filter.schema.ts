import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({ _id: false })
export class SettingsJobMappingMetaFilter {
  @Prop()
  title: string;

  @Prop()
  description: string;

  @Prop({ type: [String] })
  items: string[];
}

export const SettingsJobMappingMetaFilterSchema = SchemaFactory.createForClass(
  SettingsJobMappingMetaFilter,
);
