import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema()
export class settingsJobStatusChangeRuleOperation {
  @Prop()
  operation: string;

  @Prop({ type: Number, unique: true })
  operationNumber: number;
}

export const SettingsJobStatusChangeRuleOperationSchema =
  SchemaFactory.createForClass(settingsJobStatusChangeRuleOperation);
