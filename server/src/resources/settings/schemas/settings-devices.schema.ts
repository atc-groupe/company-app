import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import {
  SettingsDevicesMapping,
  SettingsDevicesMappingSchema,
} from './settings-devices-mapping.schema';

@Schema({ _id: false })
export class SettingsDevices {
  @Prop({ type: SettingsDevicesMappingSchema })
  mapping: SettingsDevicesMapping;
}

export const SettingsDevicesSchema =
  SchemaFactory.createForClass(SettingsDevices);
