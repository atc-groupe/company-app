import { Module } from '@nestjs/common';
import {
  SettingsAutomationService,
  SettingsDevicesMappingService,
  SettingsJobMappingService,
  SettingsJobStatusesService,
  SettingsJobSyncService,
  SettingsPlanningViewsService,
  SettingsService,
} from './services';
import {
  SettingsAutomationController,
  SettingsController,
  SettingsDevicesMappingController,
  SettingsJobMappingController,
  SettingsJobStatusesController,
  SettingsJobSyncController,
} from './controllers';
import { MongooseModule } from '@nestjs/mongoose';
import { Settings, SettingsSchema } from './schemas';
import {
  SettingsAutomationRepository,
  SettingsDevicesMappingRepository,
  SettingsJobMappingRepository,
  SettingsJobStatusesRepository,
  SettingsJobSyncRepository,
  SettingsPlanningViewsRepository,
  SettingsRepository,
} from './repositories';
import { SettingsGateway } from './settings.gateway';
import { EventDispatchersModule } from '../../shared/modules/event-dispatchers.module';
import { SettingsHelperService } from './services/settings-helper.service';
import { SettingsEventListener } from './settings.event-listener';
import { SettingsPlanningViewsController } from './controllers/settings-planning-views.controller';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Settings.name, schema: SettingsSchema },
    ]),
    EventDispatchersModule,
  ],
  providers: [
    SettingsService,
    SettingsJobStatusesService,
    SettingsJobMappingService,
    SettingsDevicesMappingService,
    SettingsJobSyncService,
    SettingsRepository,
    SettingsDevicesMappingRepository,
    SettingsJobMappingRepository,
    SettingsJobStatusesRepository,
    SettingsJobSyncRepository,
    SettingsGateway,
    SettingsHelperService,
    SettingsEventListener,
    SettingsAutomationRepository,
    SettingsAutomationService,
    SettingsPlanningViewsRepository,
    SettingsPlanningViewsService,
  ],
  controllers: [
    SettingsController,
    SettingsAutomationController,
    SettingsJobStatusesController,
    SettingsJobMappingController,
    SettingsDevicesMappingController,
    SettingsJobSyncController,
    SettingsPlanningViewsController,
  ],
  exports: [SettingsService],
})
export class SettingsModule {}
