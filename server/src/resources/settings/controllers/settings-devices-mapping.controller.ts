import { Body, Controller, Delete, Param, Post } from '@nestjs/common';
import { SettingsDevicesMappingService } from '../services';
import { Observable } from 'rxjs';
import { Settings } from '../schemas';
import { SettingsDeviceMappingNameDto } from '../dto';
import { Authorizations } from '../../auth/authorization.decorator';
import {
  AuthActionsSettingsEnum,
  AuthSubjectsEnum,
} from '../../../shared/enum';

@Controller('app-settings/devices/mapping')
@Authorizations({
  subject: AuthSubjectsEnum.Settings,
  action: AuthActionsSettingsEnum.ManageMappingParameters,
})
export class SettingsDevicesMappingController {
  constructor(private _mappingService: SettingsDevicesMappingService) {}

  @Post('names')
  public addName(
    @Body() dto: SettingsDeviceMappingNameDto,
  ): Observable<Settings> {
    return this._mappingService.addName(dto);
  }

  @Delete('names/:mpName')
  public removeName(@Param('mpName') mpName: string) {
    return this._mappingService.removeName(mpName);
  }
}
