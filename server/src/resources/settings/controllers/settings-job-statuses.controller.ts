import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { SettingsJobStatusesService } from '../services';
import { Settings } from '../schemas';
import { Observable } from 'rxjs';
import { SettingsJobStatusDto } from '../dto';
import { Authorizations } from '../../auth/authorization.decorator';
import {
  AuthActionsSettingsEnum,
  AuthSubjectsEnum,
} from '../../../shared/enum';

@Controller('app-settings/job/statuses')
@Authorizations({
  subject: AuthSubjectsEnum.Settings,
  action: AuthActionsSettingsEnum.ManageMappingParameters,
})
export class SettingsJobStatusesController {
  constructor(private _statusesService: SettingsJobStatusesService) {}
  @Patch('default-color')
  public setDefaultColor(@Body('color') color: string): Observable<Settings> {
    if (!color) {
      throw new BadRequestException('La couleur est requise');
    }

    return this._statusesService.setDefaultColor(color);
  }

  @Post()
  public addStatus(@Body() dto: SettingsJobStatusDto): Observable<Settings> {
    return this._statusesService.addStatus(dto);
  }

  @Patch(':id')
  public updateStatus(
    @Param('id') id: string,
    @Body() dto: SettingsJobStatusDto,
  ) {
    return this._statusesService.updateStatus(id, dto);
  }

  @Delete(':id')
  public removeStatus(@Param('id') id: string): Observable<Settings> {
    return this._statusesService.removeStatus(id);
  }
}
