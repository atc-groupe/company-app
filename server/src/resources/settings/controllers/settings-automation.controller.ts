import { Body, Controller, Delete, Param, Post, Put } from '@nestjs/common';
import {
  SettingsJobStatusChangeRuleDto,
  SettingsJobStatusChangeRuleOperationDto,
  SettingsJobStatusChangeRuleStatusDto,
} from '../dto';
import { Observable } from 'rxjs';
import { SettingsDocument } from '../schemas';
import { SettingsAutomationService } from '../services';
import { Authorizations } from '../../auth/authorization.decorator';
import {
  AuthActionsSettingsEnum,
  AuthSubjectsEnum,
} from '../../../shared/enum';

@Controller('app-settings/automation/job-status-change-rules')
@Authorizations({
  subject: AuthSubjectsEnum.Settings,
  action: AuthActionsSettingsEnum.ManageJobsAutomationRules,
})
export class SettingsAutomationController {
  constructor(private _automationService: SettingsAutomationService) {}
  @Post()
  public addJobStatusChangeRule(
    @Body() dto: SettingsJobStatusChangeRuleDto,
  ): Observable<SettingsDocument> {
    return this._automationService.addJobStatusChangeRule(dto);
  }

  @Put(':id')
  public updateJobStatusChangeRule(
    @Param('id') id: string,
    @Body() dto: SettingsJobStatusChangeRuleDto,
  ): Observable<SettingsDocument> {
    return this._automationService.updateJobStatusChangeRule(id, dto);
  }

  @Delete(':id')
  public removeJobStatusChangeRule(
    @Param('id') id: string,
  ): Observable<SettingsDocument> {
    return this._automationService.removeJobStatusChangeRule(id);
  }

  @Post(':id/operations')
  public addJobStatusChangeRuleOperation(
    @Param('id') id: string,
    @Body() dto: SettingsJobStatusChangeRuleOperationDto,
  ): Observable<SettingsDocument> {
    return this._automationService.addJobStatusChangeRuleOperation(id, dto);
  }

  @Delete(':id/operations/:operationId')
  public removeJobStatusChangeRuleOperation(
    @Param('id') id: string,
    @Param('operationId') operationId: string,
  ): Observable<SettingsDocument> {
    return this._automationService.removeJobStatusChangeRuleOperation(
      id,
      operationId,
    );
  }

  @Post(':id/to-statuses')
  public addJobStatusChangeRuleStartStatus(
    @Param('id') id: string,
    @Body() dto: SettingsJobStatusChangeRuleStatusDto,
  ): Observable<SettingsDocument> {
    return this._automationService.addJobStatusChangeRuleStatus(id, dto);
  }

  @Delete(':id/to-statuses/:statusId')
  public removeJobStatusChangeRuleStartStatus(
    @Param('id') id: string,
    @Param('statusId') statusId: string,
  ): Observable<SettingsDocument> {
    return this._automationService.removeJobStatusChangeRuleStatus(
      id,
      statusId,
    );
  }
}
