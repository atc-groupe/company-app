import { Controller, Get, Ip, UnauthorizedException } from '@nestjs/common';
import { Observable } from 'rxjs';
import { SettingsService } from '../services';
import { Settings } from '../schemas';
import { Public } from '../../auth/public.decorator';

@Controller('app-settings')
export class SettingsController {
  constructor(private _appSettingsService: SettingsService) {}

  @Get()
  public findUnique(): Observable<Settings> {
    return this._appSettingsService.findUnique();
  }

  @Public()
  @Get('initialize')
  public initialize(@Ip() ip: string): Observable<Settings> {
    if (!ip.endsWith('127.0.0.1')) {
      throw new UnauthorizedException(
        'App settings initialization must be done from server',
      );
    }

    return this._appSettingsService.initialize();
  }
}
