import { Body, Controller, Delete, Param, Patch, Post } from '@nestjs/common';
import { SettingsPlanningViewsService } from '../services';
import { SettingsPlanningViewDto } from '../dto';
import { Observable } from 'rxjs';
import { SettingsDocument } from '../schemas';
import { Authorizations } from '../../auth/authorization.decorator';
import {
  AuthActionsSettingsEnum,
  AuthSubjectsEnum,
} from '../../../shared/enum';

@Controller('app-settings/planning/views')
@Authorizations({
  subject: AuthSubjectsEnum.Settings,
  action: AuthActionsSettingsEnum.ManagePlanningViews,
})
export class SettingsPlanningViewsController {
  constructor(private _viewService: SettingsPlanningViewsService) {}

  @Post()
  public createOne(
    @Body() dto: SettingsPlanningViewDto,
  ): Observable<SettingsDocument> {
    return this._viewService.createOne(dto);
  }

  @Patch(':id')
  public updateOne(
    @Param('id') id: string,
    @Body() dto: SettingsPlanningViewDto,
  ): Observable<SettingsDocument> {
    return this._viewService.updateOne(id, dto);
  }

  @Delete(':id')
  public removeOne(@Param('id') id: string) {
    return this._viewService.removeOne(id);
  }

  @Post(':id/machine-ids')
  public addMachineId(
    @Param('id') id: string,
    @Body('machineId') machineId: number,
  ): Observable<SettingsDocument> {
    return this._viewService.addMachineId(id, machineId);
  }

  @Delete(':id/machine-ids/:machineId')
  public removeMachineId(
    @Param('id') id: string,
    @Param('machineId') machineId: number,
  ): Observable<SettingsDocument> {
    return this._viewService.removeMachineId(id, machineId);
  }
}
