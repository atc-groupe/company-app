import { Body, Controller, Patch } from '@nestjs/common';
import { Observable } from 'rxjs';
import { Settings } from '../schemas';
import { SettingsJobSyncService } from '../services';
import { SettingsJobSyncDto } from '../dto';

@Controller('app-settings/job/sync')
export class SettingsJobSyncController {
  constructor(private _settingsJobSync: SettingsJobSyncService) {}

  @Patch()
  public update(@Body() dto: SettingsJobSyncDto): Observable<Settings> {
    return this._settingsJobSync.update(dto);
  }
}
