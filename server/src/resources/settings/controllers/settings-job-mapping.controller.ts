import { Body, Controller, Delete, Post } from '@nestjs/common';
import { SettingsJobMappingMetaFilterDto } from '../dto';
import { Observable } from 'rxjs';
import { Settings } from '../schemas';
import { SettingsJobMappingService } from '../services';
import { Authorizations } from '../../auth/authorization.decorator';
import {
  AuthActionsSettingsEnum,
  AuthSubjectsEnum,
} from '../../../shared/enum';

@Controller('app-settings/job/mapping')
@Authorizations({
  subject: AuthSubjectsEnum.Settings,
  action: AuthActionsSettingsEnum.ManageMappingParameters,
})
export class SettingsJobMappingController {
  constructor(private _mappingService: SettingsJobMappingService) {}

  @Post('meta-filters')
  public addMetaFilter(
    @Body() dto: SettingsJobMappingMetaFilterDto,
  ): Observable<Settings> {
    return this._mappingService.addMetaFilter(dto);
  }

  @Delete('meta-filters')
  public removeMetaFilter(
    @Body() dto: SettingsJobMappingMetaFilterDto,
  ): Observable<Settings> {
    return this._mappingService.removeMetaFilter(dto);
  }
}
