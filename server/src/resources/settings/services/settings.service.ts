import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { SettingsRepository } from '../repositories';
import { catchError, map, Observable } from 'rxjs';
import { Settings } from '../schemas';
import { SettingsInitError } from '../errors/settings-init.error';
import { SettingsHelperService } from './settings-helper.service';

@Injectable()
export class SettingsService {
  constructor(
    private _settingsRepository: SettingsRepository,
    private _helper: SettingsHelperService,
  ) {}

  public initialize(): Observable<Settings> {
    return this._settingsRepository.initialize().pipe(
      catchError((err) => {
        if (err instanceof SettingsInitError) {
          const message =
            "Les paramètres de l'application ont déjà été initialisés";
          this._helper.logError('SettingsService', err, message);

          throw new BadRequestException(message);
        }

        const message =
          "Une erreur est survenue lors de l'initialisation des paramètres de l'application";
        this._helper.logError('SettingsService', err, message);

        throw new InternalServerErrorException(message);
      }),
    );
  }

  public findUnique(): Observable<Settings> {
    return this._settingsRepository.findUnique().pipe(
      map((settings) => {
        if (!settings) {
          const message = `Les paramètres de l'application n'ont pas été initialisés`;
          this._helper.logError('SettingsService', null, message);
          throw new InternalServerErrorException(message);
        }

        return settings;
      }),
    );
  }
}
