import { Injectable } from '@nestjs/common';
import { SettingsDevicesMappingRepository } from '../repositories';
import { SettingsDeviceMappingNameDto } from '../dto';
import { catchError, map, Observable } from 'rxjs';
import { Settings } from '../schemas';
import { SettingsHelperService } from './settings-helper.service';

@Injectable()
export class SettingsDevicesMappingService {
  constructor(
    private _mappingRepository: SettingsDevicesMappingRepository,
    private _helper: SettingsHelperService,
  ) {}

  public addName(dto: SettingsDeviceMappingNameDto): Observable<Settings> {
    return this._mappingRepository.addName(dto).pipe(
      map((settings) => this._helper.checkSettings(settings)),
      catchError((err) => {
        this._helper.logError(this.constructor.name, err);
        return this._helper.handleError(err);
      }),
    );
  }

  public removeName(mpName: string): Observable<Settings> {
    return this._mappingRepository.removeName(mpName).pipe(
      map((settings) => this._helper.checkSettings(settings)),
      catchError((err) => {
        this._helper.logError(this.constructor.name, err);
        return this._helper.handleError(err);
      }),
    );
  }
}
