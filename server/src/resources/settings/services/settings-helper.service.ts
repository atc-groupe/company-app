import {
  HttpException,
  Injectable,
  InternalServerErrorException,
  LoggerService,
} from '@nestjs/common';
import { SettingsDocument } from '../schemas';
import { Observable } from 'rxjs';
import { LoggerFactory } from '../../../shared/factories/logger.factory';
import { LogContextEnum } from '../../../shared/enum';

@Injectable()
export class SettingsHelperService {
  private _logger: LoggerService;

  constructor(loggerFactory: LoggerFactory) {
    this._logger = loggerFactory.get(LogContextEnum.settings);
  }
  public checkSettings(settings: SettingsDocument | null): SettingsDocument {
    if (!settings) {
      throw new InternalServerErrorException(
        `Les paramètres de l'application n'ont pas été initialisés`,
      );
    }

    return settings;
  }

  public handleError(err: any): Observable<never> {
    if (err instanceof HttpException) {
      throw err;
    }

    throw new InternalServerErrorException(
      'Une erreur est survenue lors de la mise à jour des paramètres',
    );
  }

  public logError(context: string, err?: any, message?: string) {
    if (!message) {
      message = `[${context}] Une erreur est survenue lors de la mise à jour des paramètres`;
    }

    if (err?.message) {
      message = `${message}. Message: ${err.message}`;
    }

    this._logger.error(message);
  }
}
