import { Injectable } from '@nestjs/common';
import { SettingsPlanningViewsRepository } from '../repositories';
import { SettingsPlanningViewDto } from '../dto';
import { catchError, map, Observable } from 'rxjs';
import { SettingsDocument } from '../schemas';
import { SettingsHelperService } from './settings-helper.service';

@Injectable()
export class SettingsPlanningViewsService {
  constructor(
    private _repository: SettingsPlanningViewsRepository,
    private _helper: SettingsHelperService,
  ) {}

  public createOne(dto: SettingsPlanningViewDto): Observable<SettingsDocument> {
    return this._repository.createOne(dto).pipe(
      map((settings) => this._helper.checkSettings(settings)),
      catchError((err) => {
        this._helper.logError(this.constructor.name, err);
        return this._helper.handleError(err);
      }),
    );
  }

  public updateOne(
    id: string,
    dto: SettingsPlanningViewDto,
  ): Observable<SettingsDocument> {
    return this._repository.updateOne(id, dto).pipe(
      map((settings) => this._helper.checkSettings(settings)),
      catchError((err) => {
        this._helper.logError(this.constructor.name, err);
        return this._helper.handleError(err);
      }),
    );
  }

  public removeOne(id: string): Observable<SettingsDocument> {
    return this._repository.removeOne(id).pipe(
      map((settings) => this._helper.checkSettings(settings)),
      catchError((err) => {
        this._helper.logError(this.constructor.name, err);
        return this._helper.handleError(err);
      }),
    );
  }

  public addMachineId(
    id: string,
    machineId: number,
  ): Observable<SettingsDocument> {
    return this._repository.addMachineId(id, machineId).pipe(
      map((settings) => this._helper.checkSettings(settings)),
      catchError((err) => {
        this._helper.logError(this.constructor.name, err);
        return this._helper.handleError(err);
      }),
    );
  }

  public removeMachineId(
    id: string,
    machineId: number,
  ): Observable<SettingsDocument> {
    return this._repository.removeMachineId(id, machineId).pipe(
      map((settings) => this._helper.checkSettings(settings)),
      catchError((err) => {
        this._helper.logError(this.constructor.name, err);
        return this._helper.handleError(err);
      }),
    );
  }
}
