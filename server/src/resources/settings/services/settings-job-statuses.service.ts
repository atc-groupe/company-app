import { Injectable } from '@nestjs/common';
import { SettingsJobStatusesRepository } from '../repositories';
import { catchError, map, Observable } from 'rxjs';
import { Settings } from '../schemas';
import { SettingsJobStatusDto } from '../dto';
import { SettingsHelperService } from './settings-helper.service';

@Injectable()
export class SettingsJobStatusesService {
  constructor(
    private _statusesRepository: SettingsJobStatusesRepository,
    private _helper: SettingsHelperService,
  ) {}

  public setDefaultColor(color: string): Observable<Settings> {
    return this._statusesRepository.setDefaultColor(color).pipe(
      map((settings) => this._helper.checkSettings(settings)),
      catchError((err) => {
        this._helper.logError(this.constructor.name, err);
        return this._helper.handleError(err);
      }),
    );
  }

  public addStatus(dto: SettingsJobStatusDto): Observable<Settings> {
    return this._statusesRepository.addStatus(dto).pipe(
      map((settings) => this._helper.checkSettings(settings)),
      catchError((err) => {
        this._helper.logError(this.constructor.name, err);
        return this._helper.handleError(err);
      }),
    );
  }

  public updateStatus(
    id: string,
    dto: SettingsJobStatusDto,
  ): Observable<Settings> {
    return this._statusesRepository.updateStatus(id, dto).pipe(
      map((settings) => this._helper.checkSettings(settings)),
      catchError((err) => {
        this._helper.logError(this.constructor.name, err);
        return this._helper.handleError(err);
      }),
    );
  }

  public removeStatus(id: string): Observable<Settings> {
    return this._statusesRepository.removeStatus(id).pipe(
      map((settings) => this._helper.checkSettings(settings)),
      catchError((err) => {
        this._helper.logError(this.constructor.name, err);
        return this._helper.handleError(err);
      }),
    );
  }
}
