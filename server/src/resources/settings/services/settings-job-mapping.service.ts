import { Injectable } from '@nestjs/common';
import { SettingsJobMappingRepository } from '../repositories';
import { SettingsJobMappingMetaFilterDto } from '../dto';
import { catchError, map, Observable } from 'rxjs';
import { Settings } from '../schemas';
import { SettingsHelperService } from './settings-helper.service';

@Injectable()
export class SettingsJobMappingService {
  constructor(
    private _mappingRepository: SettingsJobMappingRepository,
    private _helper: SettingsHelperService,
  ) {}

  public addMetaFilter(
    dto: SettingsJobMappingMetaFilterDto,
  ): Observable<Settings> {
    return this._mappingRepository.addMetaFilter(dto).pipe(
      map((settings) => this._helper.checkSettings(settings)),
      catchError((err) => {
        this._helper.logError(this.constructor.name, err);
        return this._helper.handleError(err);
      }),
    );
  }

  public removeMetaFilter(
    dto: SettingsJobMappingMetaFilterDto,
  ): Observable<Settings> {
    return this._mappingRepository.removeMetaFilter(dto).pipe(
      map((settings) => this._helper.checkSettings(settings)),
      catchError((err) => {
        this._helper.logError(this.constructor.name, err);
        return this._helper.handleError(err);
      }),
    );
  }
}
