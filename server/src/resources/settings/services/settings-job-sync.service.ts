import { Injectable } from '@nestjs/common';
import { SettingsJobSyncRepository } from '../repositories';
import { catchError, map, Observable } from 'rxjs';
import { Settings } from '../schemas';
import { SettingsJobSyncDto } from '../dto';
import { SettingsHelperService } from './settings-helper.service';

@Injectable()
export class SettingsJobSyncService {
  constructor(
    private _syncRepository: SettingsJobSyncRepository,
    private _helper: SettingsHelperService,
  ) {}

  public update(dto: SettingsJobSyncDto): Observable<Settings> {
    return this._syncRepository.update(dto).pipe(
      map((settings) => this._helper.checkSettings(settings)),
      catchError((err) => {
        this._helper.logError(this.constructor.name, err);
        return this._helper.handleError(err);
      }),
    );
  }
}
