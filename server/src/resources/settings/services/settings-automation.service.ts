import { Injectable } from '@nestjs/common';
import { SettingsAutomationRepository } from '../repositories';
import {
  SettingsJobStatusChangeRuleDto,
  SettingsJobStatusChangeRuleOperationDto,
  SettingsJobStatusChangeRuleStatusDto,
} from '../dto';
import { catchError, map, Observable } from 'rxjs';
import { SettingsHelperService } from './settings-helper.service';
import { SettingsDocument } from '../schemas';

@Injectable()
export class SettingsAutomationService {
  constructor(
    private _automationRepository: SettingsAutomationRepository,
    private _helper: SettingsHelperService,
  ) {}

  public addJobStatusChangeRule(
    dto: SettingsJobStatusChangeRuleDto,
  ): Observable<SettingsDocument> {
    return this._automationRepository.createJobStatusChangeRule(dto).pipe(
      map((settings) => this._helper.checkSettings(settings)),
      catchError((err) => {
        this._helper.logError(this.constructor.name, err);
        return this._helper.handleError(err);
      }),
    );
  }

  public updateJobStatusChangeRule(
    ruleId: string,
    dto: SettingsJobStatusChangeRuleDto,
  ): Observable<SettingsDocument> {
    return this._automationRepository
      .updateJobStatusChangeRule(ruleId, dto)
      .pipe(
        map((settings) => this._helper.checkSettings(settings)),
        catchError((err) => {
          this._helper.logError(this.constructor.name, err);
          return this._helper.handleError(err);
        }),
      );
  }

  public removeJobStatusChangeRule(id: string): Observable<SettingsDocument> {
    return this._automationRepository.deleteJobStatusChangeRule(id).pipe(
      map((settings) => this._helper.checkSettings(settings)),
      catchError((err) => {
        this._helper.logError(this.constructor.name, err);
        return this._helper.handleError(err);
      }),
    );
  }

  public addJobStatusChangeRuleOperation(
    ruleId: string,
    dto: SettingsJobStatusChangeRuleOperationDto,
  ): Observable<SettingsDocument> {
    return this._automationRepository
      .createJobStatusChangeRuleOperation(ruleId, dto)
      .pipe(
        map((settings) => this._helper.checkSettings(settings)),
        catchError((err) => {
          this._helper.logError(this.constructor.name, err);
          return this._helper.handleError(err);
        }),
      );
  }

  public removeJobStatusChangeRuleOperation(
    ruleId: string,
    operationId: string,
  ): Observable<SettingsDocument> {
    return this._automationRepository
      .deleteJobStatusChangeRuleOperation(ruleId, operationId)
      .pipe(
        map((settings) => this._helper.checkSettings(settings)),
        catchError((err) => {
          this._helper.logError(this.constructor.name, err);
          return this._helper.handleError(err);
        }),
      );
  }

  public addJobStatusChangeRuleStatus(
    ruleId: string,
    dto: SettingsJobStatusChangeRuleStatusDto,
  ): Observable<SettingsDocument> {
    return this._automationRepository
      .addJobStatusChangeRulesStatus(ruleId, dto)
      .pipe(
        map((settings) => this._helper.checkSettings(settings)),
        catchError((err) => {
          this._helper.logError(this.constructor.name, err);
          return this._helper.handleError(err);
        }),
      );
  }

  public removeJobStatusChangeRuleStatus(
    ruleId: string,
    statusId: string,
  ): Observable<SettingsDocument> {
    return this._automationRepository
      .removeJobStatusChangeRuleStatus(ruleId, statusId)
      .pipe(
        map((settings) => this._helper.checkSettings(settings)),
        catchError((err) => {
          this._helper.logError(this.constructor.name, err);
          return this._helper.handleError(err);
        }),
      );
  }
}
