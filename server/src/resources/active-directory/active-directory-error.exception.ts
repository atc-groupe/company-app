import { HttpException, HttpStatus } from '@nestjs/common';

export class ActiveDirectoryErrorException extends HttpException {
  constructor(message: string, code?: HttpStatus) {
    super(message, code ? code : HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
