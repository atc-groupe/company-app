import { Module } from '@nestjs/common';
import { ActiveDirectoryService } from './active-directory.service';
import { ActiveDirectoryController } from './active-directory.controller';
import { ActiveDirectoryConfig } from '../../shared/config';
import { HelperModule } from '../../shared/modules';

@Module({
  imports: [HelperModule],
  providers: [ActiveDirectoryService, ActiveDirectoryConfig],
  controllers: [ActiveDirectoryController],
  exports: [ActiveDirectoryService],
})
export class ActiveDirectoryModule {}
