import { Controller, Get } from '@nestjs/common';
import { ActiveDirectoryService } from './active-directory.service';
import { ActiveDirectoryConfig } from '../../shared/config';
import { IActiveDirectoryConfigOptions } from '../../shared/interfaces';
import { Observable } from 'rxjs';
import { IAdUser } from './i-ad-user';

@Controller('security/active-directory')
export class ActiveDirectoryController {
  private _adConfig: IActiveDirectoryConfigOptions;

  constructor(
    adConfig: ActiveDirectoryConfig,
    private _adService: ActiveDirectoryService,
  ) {
    this._adConfig = adConfig.getConfigOptions();
  }

  @Get('users')
  findAll(): Observable<IAdUser[]> {
    return this._adService.findUsersFromSecurityGroup();
  }
}
