import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { IAdUser } from './i-ad-user';
import { catchError, Observable } from 'rxjs';
import { ActiveDirectoryConfig } from '../../shared/config';
import { DataSorter, Logger } from '../../shared/services';
import { LogContextEnum } from '../../shared/enum';
import ActiveDirectory = require('activedirectory2');
import { ActiveDirectoryErrorException } from './active-directory-error.exception';
import { LoggerFactory } from '../../shared/factories/logger.factory';

@Injectable()
export class ActiveDirectoryService {
  private readonly _adSecurityUsersGroup;
  private readonly _adSecurityAdminsGroup;
  private _ad;
  private _logger: Logger;

  constructor(
    adConfig: ActiveDirectoryConfig,
    loggerFactory: LoggerFactory,
    private _sorter: DataSorter,
  ) {
    const config = adConfig.getConfigOptions();
    this._ad = new ActiveDirectory(adConfig.getConfigOptions());
    this._adSecurityUsersGroup = config.usersGroup;
    this._adSecurityAdminsGroup = config.adminsGroup;
    this._logger = loggerFactory.get(LogContextEnum.activeDirectory);
  }

  public findUsersFromSecurityGroup(): Observable<IAdUser[]> {
    return new Observable<IAdUser[]>((subscriber) => {
      this._ad.getUsersForGroup(
        this._adSecurityUsersGroup,
        function (err: object, users: IAdUser[]) {
          if (err) {
            return subscriber.error(err);
          }

          if (users === undefined) {
            return subscriber.error(
              new ActiveDirectoryErrorException(
                'AD config error. Impossible to fetch data. Please look at connection configuration',
              ),
            );
          }

          if (!users.length) {
            subscriber.next([]);
            return subscriber.complete();
          }

          const sorter = new DataSorter();
          subscriber.next(
            sorter.sort(
              users,
              (user: IAdUser) =>
                `${user.givenName.toUpperCase()} ${user.sn?.toUpperCase()}`,
            ),
          );
          subscriber.complete();
        },
      );
    }).pipe(
      catchError((err) => {
        if (err instanceof ActiveDirectoryErrorException) {
          this._logger.error(err.message, true);

          throw new InternalServerErrorException(err.message);
        }

        this._logger.error('AD error');
        throw new InternalServerErrorException('AD error', { cause: err });
      }),
    );
  }

  public findUser(name: string): Observable<IAdUser | null> {
    return new Observable<IAdUser | null>((subscriber) => {
      this._ad.findUser(name, (err: any, user: IAdUser) => {
        if (err) {
          return subscriber.error(err);
        }

        subscriber.next(user ? user : null);
        subscriber.complete();
      });
    });
  }

  public isPasswordValid(
    userPrincipalName: string,
    password: string,
  ): Observable<boolean> {
    return new Observable<boolean>((subscriber) => {
      this._ad.authenticate(
        userPrincipalName,
        password,
        (err: string, auth: boolean) => {
          if (err) {
            return subscriber.next(false);
          }

          subscriber.next(auth);
          subscriber.complete();
        },
      );
    });
  }

  public isAdmin(userPrincipalName: string): Observable<boolean> {
    return new Observable<boolean>((subscriber) => {
      this._ad.isUserMemberOf(
        userPrincipalName,
        this._adSecurityAdminsGroup,
        (err: object, isAdmin: boolean) => {
          if (err) {
            return subscriber.error(err);
          }

          subscriber.next(isAdmin);
          subscriber.complete();
        },
      );
    });
  }
}
