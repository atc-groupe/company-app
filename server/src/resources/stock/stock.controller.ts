import {
  Body,
  Controller,
  Delete,
  FileTypeValidator,
  Get,
  MaxFileSizeValidator,
  Param,
  ParseFilePipe,
  Patch,
  Post,
  Put,
  Query,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { ProductsService, StockMutationsService } from './services';
import { ProductTypeEnum } from '../../mp/mp-stock/enums';
import { IMpStockMutation } from '../../mp/mp-stock/interfaces';
import {
  ArticleStockInfoDto,
  ProductGenericNameDto,
  StockMutationDto,
} from './dto';
import { Observable } from 'rxjs';
import { ArticleDocument, PaperDocument } from './schemas';
import { TProductDocument } from './types';
import { ProductGenericName } from './schemas/product-generic-name.schema';
import { ProductsFilesService } from './services/products-files.service';
import { AppFileDto } from '../app-files/app-file.dto';
import { AppFileInfoDto } from '../app-files/app-file-info.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { Authorizations } from '../auth/authorization.decorator';
import {
  AuthActionsProductsEnum,
  AuthActionsSettingsEnum,
  AuthSubjectsEnum,
} from '../../shared/enum';

@Controller('stock')
export class StockController {
  constructor(
    private _productsService: ProductsService,
    private _productsFilesService: ProductsFilesService,
    private _stockMutationsService: StockMutationsService,
  ) {}

  @Get('sync')
  @Authorizations({
    subject: AuthSubjectsEnum.Products,
    action: AuthActionsProductsEnum.SyncAllProducts,
  })
  public async syncProducts(): Promise<void> {
    await this._productsService.syncAll();
  }

  @Get(':type/:id/sync')
  public syncOne(
    @Param('type') type: ProductTypeEnum,
    @Param('id') id: number,
  ) {
    return this._productsService.syncOne(type, id);
  }

  @Get(':type/:id/mutations')
  public getProductMutations(
    @Param('type') type: ProductTypeEnum,
    @Param('id') id: number,
    @Query('start-date') startDate: string,
    @Query('stop-date') stopDate?: string,
  ): Promise<IMpStockMutation[]> {
    return this._stockMutationsService.getMutations(
      type,
      id,
      startDate,
      stopDate,
    );
  }

  @Get('sheets/generic-names')
  public getSheetsGenericNames(): Observable<ProductGenericName[]> {
    return this._productsService.getSheetsGenericNames();
  }

  @Get('papers/:id')
  public getPaperByName(@Param('id') id: number): Observable<PaperDocument[]> {
    return this._productsService.getPapersByName(id);
  }

  @Get(':type/:groupType')
  public getProductList(
    @Param('type') type: ProductTypeEnum,
    @Param('groupType') groupType: number,
  ): Promise<TProductDocument[]> {
    return this._productsService.getProductsList(type, groupType);
  }

  @Patch('articles/:id/stock-info')
  @Authorizations({
    subject: AuthSubjectsEnum.Products,
    action: AuthActionsProductsEnum.UpdateStockSettings,
  })
  public updateArticleStockInfo(
    @Param('id') id: number,
    @Body() dto: ArticleStockInfoDto,
  ): Observable<ArticleDocument> {
    return this._productsService.updateArticleStockInfo(id, dto);
  }

  @Patch(':type/:id/generic-name')
  @Authorizations({
    subject: AuthSubjectsEnum.Products,
    action: AuthActionsProductsEnum.UpdateStockSettings,
  })
  public updateProductGenericName(
    @Param('type') type: ProductTypeEnum,
    @Param('id') id: number,
    @Body() dto: ProductGenericNameDto,
  ): Observable<TProductDocument> {
    return this._productsService.updateProductGenericName(type, id, dto);
  }

  @Post(':type/:id/mutations')
  @Authorizations(
    {
      subject: AuthSubjectsEnum.Products,
      action: AuthActionsProductsEnum.ManageStock,
    },
    {
      subject: AuthSubjectsEnum.Products,
      action: AuthActionsProductsEnum.DecreaseStock,
    },
    {
      subject: AuthSubjectsEnum.Products,
      action: AuthActionsProductsEnum.UpdateStock,
    },
  )
  public createProductMutations(
    @Param('type') type: ProductTypeEnum,
    @Param('id') id: number,
    @Body() dto: StockMutationDto,
  ): Observable<TProductDocument> {
    return this._stockMutationsService.createMutation(type, id, dto);
  }

  @Put(':type/:id/mutations/:mutationId')
  @Authorizations(
    {
      subject: AuthSubjectsEnum.Products,
      action: AuthActionsProductsEnum.ManageStock,
    },
    {
      subject: AuthSubjectsEnum.Products,
      action: AuthActionsProductsEnum.DecreaseStock,
    },
    {
      subject: AuthSubjectsEnum.Products,
      action: AuthActionsProductsEnum.UpdateStock,
    },
  )
  public updateProductMutations(
    @Param('type') type: ProductTypeEnum,
    @Param('id') id: number,
    @Param('mutationId') mutationId: number,
    @Body() dto: StockMutationDto,
  ): Observable<void> {
    return this._stockMutationsService.updateMutation(
      type,
      id,
      mutationId,
      dto,
    );
  }

  @Post(':type/:id/files')
  @Authorizations({
    subject: AuthSubjectsEnum.Products,
    action: AuthActionsProductsEnum.ManageDocuments,
  })
  @UseInterceptors(FileInterceptor('file'))
  public addFile(
    @Param('type') type: ProductTypeEnum,
    @Param('id') id: number,
    @Body() dto: AppFileDto,
    @UploadedFile(
      new ParseFilePipe({
        validators: [
          new MaxFileSizeValidator({
            maxSize: 5000000,
            message: 'La taille maximum est de 5Mo',
          }),
          new FileTypeValidator({
            fileType: /image\/(jpg|jpeg)|application\/pdf/,
          }),
        ],
      }),
    )
    file: Express.Multer.File,
  ): Promise<TProductDocument | null> {
    return this._productsFilesService.addFile(type, id, dto, file);
  }

  @Delete(':type/:id/files/:fileId')
  @Authorizations({
    subject: AuthSubjectsEnum.Products,
    action: AuthActionsProductsEnum.ManageDocuments,
  })
  public deleteFile(
    @Param('type') type: ProductTypeEnum,
    @Param('id') id: number,
    @Param('fileId') fileId: string,
  ): Promise<TProductDocument | null> {
    return this._productsFilesService.removeFile(type, id, fileId);
  }

  @Patch(':type/:id/files/:fileId/info')
  @Authorizations({
    subject: AuthSubjectsEnum.Products,
    action: AuthActionsProductsEnum.ManageDocuments,
  })
  public updateFileInfo(
    @Param('type') type: ProductTypeEnum,
    @Param('id') id: number,
    @Param('fileId') fileId: string,
    @Body() dto: AppFileInfoDto,
  ): Promise<TProductDocument | null> {
    return this._productsFilesService.updateFileInfo(type, id, fileId, dto);
  }

  @Patch(':type/:id/files/:fileId')
  @Authorizations({
    subject: AuthSubjectsEnum.Products,
    action: AuthActionsProductsEnum.ManageDocuments,
  })
  @UseInterceptors(FileInterceptor('file'))
  public updateFile(
    @Param('type') type: ProductTypeEnum,
    @Param('id') id: number,
    @Param('fileId') fileId: string,
    @UploadedFile(
      new ParseFilePipe({
        validators: [
          new MaxFileSizeValidator({
            maxSize: 5000000,
            message: 'La taille maximum est de 5Mo',
          }),
          new FileTypeValidator({
            fileType: /image\/(jpg|jpeg)|application\/pdf/,
          }),
        ],
      }),
    )
    file: Express.Multer.File,
  ): Promise<TProductDocument | null> {
    return this._productsFilesService.updateFile(type, id, fileId, file);
  }
}
