import { TProductDocument } from '../types';

export class ProductEvent {
  constructor(
    public readonly subject: 'paper' | 'article',
    public readonly action: 'create' | 'update' | 'delete',
    public readonly payload: TProductDocument,
  ) {}
}
