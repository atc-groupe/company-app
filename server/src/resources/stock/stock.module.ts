import { Module } from '@nestjs/common';
import { MpStockModule } from '../../mp/mp-stock/mp-stock.module';
import { StockController } from './stock.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Article, ArticleSchema, Paper, PaperSchema } from './schemas';
import {
  ArticleRepository,
  ProductGenericNameRepository,
} from './repositories';
import {
  ArticleMpMappingService,
  PaperMpMappingService,
  ArticleSyncService,
  PaperSyncService,
  ProductsStatesService,
  ProductsService,
  StockMutationsService,
  ProductGenericNamesService,
} from './services';
import { PaperRepository } from './repositories/paper.repository';
import { ProductsSyncTaskService } from './tasks/products-sync-task.service';
import { ProductsEventService } from './services/products-event.service';
import { ProductsGateway } from './products.gateway';
import { ProductStatesService } from './services/product-states.service';
import { ProductGateway } from './product.gateway';
import { ProductEventService } from './services/product-event.service';
import {
  ProductGenericName,
  ProductGenericNameSchema,
} from './schemas/product-generic-name.schema';
import { ProductGenericNamesController } from './product-generic-names.controller';
import { AppFilesModule } from '../app-files/app-files.module';
import { ProductsFilesService } from './services/products-files.service';

@Module({
  imports: [
    MpStockModule,
    AppFilesModule,
    MongooseModule.forFeature([
      { name: Article.name, schema: ArticleSchema },
      { name: Paper.name, schema: PaperSchema },
      { name: ProductGenericName.name, schema: ProductGenericNameSchema },
    ]),
  ],
  providers: [
    ArticleRepository,
    PaperRepository,
    ArticleSyncService,
    PaperSyncService,
    ArticleMpMappingService,
    PaperMpMappingService,
    ProductsSyncTaskService,
    ProductsEventService,
    ProductEventService,
    ProductsGateway,
    ProductGateway,
    ProductsStatesService,
    ProductStatesService,
    ProductsService,
    StockMutationsService,
    ProductGenericNameRepository,
    ProductGenericNamesService,
    ProductsFilesService,
  ],
  controllers: [StockController, ProductGenericNamesController],
  exports: [StockMutationsService],
})
export class StockModule {}
