import {
  ConnectedSocket,
  MessageBody,
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketGateway,
} from '@nestjs/websockets';
import { Socket } from 'socket.io';
import { TProductDocument } from './types';
import { ProductQueryParamsDto } from './dto';
import { ProductsService } from './services/products.service';
import { ProductStatesService } from './services/product-states.service';
import { AppWsResponse, AppWsSuccessResponse } from '../../shared/classes';
import { firstValueFrom } from 'rxjs';
import { AppWsInternalServerErrorException } from '../../shared/exceptions';
import { ProductTypeEnum } from '../../mp/mp-stock/enums';
import { UseGuards } from '@nestjs/common';
import { AuthGatewayGuard } from '../auth/auth-gateway.guard';

@UseGuards(AuthGatewayGuard)
@WebSocketGateway({ transports: ['websocket'], namespace: 'stock.product' })
export class ProductGateway implements OnGatewayDisconnect {
  constructor(
    private _productStatesService: ProductStatesService,
    private _productsService: ProductsService,
  ) {}

  handleDisconnect(client: Socket): void {
    this._productStatesService.removeSocket(client.id);
  }

  @SubscribeMessage('product.subscribe')
  public async onSubscribeProduct(
    @MessageBody() dto: ProductQueryParamsDto,
    @ConnectedSocket() client: Socket,
  ): Promise<AppWsResponse> {
    try {
      const product = await firstValueFrom(
        this._productsService.syncOne(dto.productType, dto.productId),
      );

      this._productStatesService.addState(dto.productId, client);

      return new AppWsSuccessResponse(product);
    } catch {
      return new AppWsInternalServerErrorException(
        'Une erreur est survenue lors de la récupération du produit',
      );
    }
  }

  public emitProductChange(
    event: string,
    sockets: Socket[],
    productType: ProductTypeEnum,
    action: 'create' | 'update' | 'delete',
    product: TProductDocument,
  ): void {
    sockets.forEach((socket: Socket) => {
      socket.emit('product.change', { productType, action, product });
    });
  }
}
