import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
  ProductGenericName,
  ProductGenericNameDocument,
} from '../schemas/product-generic-name.schema';
import { defer, Observable } from 'rxjs';

@Injectable()
export class ProductGenericNameRepository {
  constructor(
    @InjectModel(ProductGenericName.name)
    private productGenericNameModel: Model<ProductGenericName>,
  ) {}

  public insertOne(name: string): Observable<ProductGenericNameDocument> {
    return defer(() => this.productGenericNameModel.create({ name }));
  }

  public findAll(): Observable<ProductGenericNameDocument[]> {
    return defer(() =>
      this.productGenericNameModel.find().sort({ name: 'asc' }).exec(),
    );
  }

  public updateOne(
    id: string,
    name: string,
  ): Observable<ProductGenericNameDocument | null> {
    return defer(() =>
      this.productGenericNameModel.findByIdAndUpdate(
        id,
        { $set: { name } },
        { new: true, runValidators: true },
      ),
    );
  }

  public removeOne(id: string): Observable<ProductGenericNameDocument | null> {
    return defer(() =>
      this.productGenericNameModel.findByIdAndRemove(id).exec(),
    );
  }
}
