import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Paper, PaperDocument, PaperMp } from '../schemas';
import { Model, Types } from 'mongoose';
import { defer, Observable, tap } from 'rxjs';
import { PaperGroupTypeEnum } from '../../../mp/mp-stock/enums';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { ProductEventEnum } from '../enum';
import { ProductEvent } from '../events/product-event';
import { IProductRepository } from '../interfaces';
import { ProductGenericNameDto } from '../dto';
import { ProductGenericName } from '../schemas/product-generic-name.schema';
import { TProductDocument } from '../types';

@Injectable()
export class PaperRepository implements IProductRepository {
  constructor(
    @InjectModel(Paper.name) private _paperModel: Model<Paper>,
    private _eventEmitter: EventEmitter2,
  ) {}

  public insertOne(paper: Paper): Observable<PaperDocument> {
    return defer(() => this._paperModel.create(paper)).pipe(
      tap((paper) =>
        this._eventEmitter.emit(
          ProductEventEnum.Change,
          new ProductEvent('paper', 'create', paper),
        ),
      ),
    );
  }

  public find(): Observable<PaperDocument[]> {
    return defer(() =>
      this._paperModel.find().populate(['genericName', 'files']).exec(),
    );
  }

  public findById(id: number): Observable<PaperDocument | null> {
    return defer(() =>
      this._paperModel.findById(id).populate(['genericName', 'files']).exec(),
    );
  }

  public findByName(name: string): Observable<PaperDocument[]> {
    return defer(() =>
      this._paperModel
        .find({ 'mp.name': name, 'mp.stockManagement': true })
        .sort({ 'mp.width': 1, 'mp.length': 1 })
        .populate(['genericName', 'files'])
        .exec(),
    );
  }

  public findByGroupType(
    groupType: PaperGroupTypeEnum,
  ): Observable<PaperDocument[]> {
    return defer(() =>
      this._paperModel
        .find({
          'mp.groupType': groupType,
          'mp.stockManagement': true,
        })
        .sort({
          'mp.groupDescription': 1,
          'mp.name': 1,
          'mp.width': 1,
          'mp.length': 1,
        })
        .populate(['genericName', 'files'])
        .exec(),
    );
  }

  public findSheetsGenericNames(): Observable<ProductGenericName[]> {
    return defer(() =>
      this._paperModel.aggregate([
        {
          $match: {
            'mp.groupType': PaperGroupTypeEnum.Sheets,
            genericName: { $exists: true, $ne: null },
          },
        },
        { $project: { genericName: 1 } },
        {
          $lookup: {
            from: 'productgenericnames',
            localField: 'genericName',
            foreignField: '_id',
            as: 'genericName',
          },
        },
        {
          $addFields: {
            genericName: { $arrayElemAt: ['$genericName', 0] },
          },
        },
        {
          $group: {
            _id: 'gen',
            items: {
              $addToSet: '$genericName',
            },
          },
        },
        { $unwind: '$items' },
        {
          $addFields: {
            _id: '$items._id',
            name: '$items.name',
          },
        },
        { $project: { name: 1 } },
        { $sort: { name: 1 } },
      ]),
    );
  }

  public updateMp(id: number, mp: PaperMp): Observable<PaperDocument | null> {
    return defer(() =>
      this._paperModel
        .findByIdAndUpdate(
          id,
          { $set: { mp } },
          { new: true, runValidators: true },
        )
        .populate(['genericName', 'files'])
        .exec(),
    ).pipe(
      tap((paper) => {
        if (paper) {
          this._eventEmitter.emit(
            ProductEventEnum.Change,
            new ProductEvent('paper', 'update', paper),
          );
        }
      }),
    );
  }

  public updateGenericName(
    id: number,
    dto: ProductGenericNameDto,
  ): Observable<PaperDocument | null> {
    return defer(() =>
      this._paperModel
        .findByIdAndUpdate(
          id,
          { $set: { genericName: dto.id ? new Types.ObjectId(dto.id) : null } },
          { new: true, runValidators: true },
        )
        .populate(['genericName', 'files'])
        .exec(),
    ).pipe(
      tap((paper) => {
        if (paper) {
          this._eventEmitter.emit(
            ProductEventEnum.Change,
            new ProductEvent('paper', 'update', paper),
          );
        }
      }),
    );
  }

  public removeGenericNameForAll(genericNameId: string) {
    return defer(() =>
      this._paperModel
        .updateMany(
          { genericName: genericNameId },
          {
            $set: {
              genericName: null,
            },
          },
        )
        .populate('files')
        .exec(),
    );
  }

  public removeOne(id: number): Observable<PaperDocument | null> {
    return defer(() =>
      this._paperModel.findByIdAndRemove(id).populate('files').exec(),
    ).pipe(
      tap((paper) => {
        if (paper) {
          this._eventEmitter.emit(
            ProductEventEnum.Change,
            new ProductEvent('paper', 'delete', paper),
          );
        }
      }),
    );
  }

  public addFile(
    id: number,
    fileId: Types.ObjectId,
  ): Observable<TProductDocument | null> {
    return defer(() =>
      this._paperModel
        .findByIdAndUpdate(
          id,
          { $push: { files: fileId } },
          { runValidators: true, new: true },
        )
        .populate(['genericName', 'files'])
        .exec(),
    ).pipe(
      tap((article) => {
        if (article) {
          this._eventEmitter.emit(
            ProductEventEnum.Change,
            new ProductEvent('paper', 'update', article),
          );
        }
      }),
    );
  }

  public removeFile(
    id: number,
    fileId: string,
  ): Observable<TProductDocument | null> {
    return defer(() =>
      this._paperModel
        .findByIdAndUpdate(
          id,
          { $pull: { files: new Types.ObjectId(fileId) } },
          { runValidators: true, new: true },
        )
        .populate(['genericName', 'files'])
        .exec(),
    ).pipe(
      tap((article) => {
        if (article) {
          this._eventEmitter.emit(
            ProductEventEnum.Change,
            new ProductEvent('paper', 'update', article),
          );
        }
      }),
    );
  }
}
