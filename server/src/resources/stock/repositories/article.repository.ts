import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Article, ArticleDocument, ArticleMp } from '../schemas';
import { Model, Types } from 'mongoose';
import { ArticleGroupTypeEnum } from '../../../mp/mp-stock/enums';
import { defer, Observable, tap } from 'rxjs';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { ProductEventEnum } from '../enum';
import { ProductEvent } from '../events/product-event';
import { IProductRepository } from '../interfaces';
import { ArticleStockInfoDto, ProductGenericNameDto } from '../dto';
import { TProductDocument } from '../types';

@Injectable()
export class ArticleRepository implements IProductRepository {
  constructor(
    @InjectModel(Article.name) private _articleModel: Model<Article>,
    private _eventEmitter: EventEmitter2,
  ) {}

  public insertOne(article: Article): Observable<ArticleDocument> {
    return defer(() => this._articleModel.create(article)).pipe(
      tap((article) =>
        this._eventEmitter.emit(
          ProductEventEnum.Change,
          new ProductEvent('article', 'create', article),
        ),
      ),
    );
  }

  public find(): Observable<ArticleDocument[]> {
    return defer(() =>
      this._articleModel.find().populate(['genericName', 'files']).exec(),
    );
  }

  public findById(id: number): Observable<ArticleDocument | null> {
    return defer(() =>
      this._articleModel.findById(id).populate(['genericName', 'files']).exec(),
    );
  }

  public findByName(name: string): Observable<ArticleDocument[]> {
    return defer(() =>
      this._articleModel
        .find({ 'mp.name': name })
        .populate(['genericName', 'files'])
        .exec(),
    );
  }

  public findByGroupType(
    groupType: ArticleGroupTypeEnum,
  ): Observable<ArticleDocument[]> {
    return defer(() =>
      this._articleModel
        .find({ 'mp.groupType': groupType })
        .populate(['genericName', 'files'])
        .sort({ 'mp.groupDescription': 1, 'mp.name': 1 })
        .exec(),
    );
  }

  public updateMp(
    id: number,
    mp: ArticleMp,
  ): Observable<ArticleDocument | null> {
    return defer(() =>
      this._articleModel
        .findByIdAndUpdate(
          id,
          { $set: { mp } },
          { new: true, runValidators: true },
        )
        .populate(['genericName', 'files'])
        .exec(),
    ).pipe(
      tap((article) => {
        if (article) {
          this._eventEmitter.emit(
            ProductEventEnum.Change,
            new ProductEvent('article', 'update', article),
          );
        }
      }),
    );
  }

  public updateStockInfo(
    id: number,
    dto: ArticleStockInfoDto,
  ): Observable<ArticleDocument | null> {
    return defer(() =>
      this._articleModel
        .findByIdAndUpdate(
          id,
          {
            $set: {
              stockUnitNumber: dto.stockUnitNumber,
              stockUnitLabel: dto.stockUnitLabel,
            },
          },
          { new: true, runValidators: true },
        )
        .populate(['genericName', 'files'])
        .exec(),
    ).pipe(
      tap((article) => {
        if (article) {
          this._eventEmitter.emit(
            ProductEventEnum.Change,
            new ProductEvent('article', 'update', article),
          );
        }
      }),
    );
  }

  public updateGenericName(
    id: number,
    dto: ProductGenericNameDto,
  ): Observable<ArticleDocument | null> {
    return defer(() =>
      this._articleModel
        .findByIdAndUpdate(
          id,
          {
            $set: {
              genericName: dto.id ? new Types.ObjectId(dto.id) : null,
            },
          },
          { new: true, runValidators: true },
        )
        .populate(['genericName', 'files'])
        .exec(),
    ).pipe(
      tap((article) => {
        if (article) {
          this._eventEmitter.emit(
            ProductEventEnum.Change,
            new ProductEvent('article', 'update', article),
          );
        }
      }),
    );
  }

  public removeGenericNameForAll(genericNameId: string) {
    return defer(() =>
      this._articleModel
        .updateMany(
          { genericName: genericNameId },
          {
            $set: {
              genericName: null,
            },
          },
        )
        .populate('files')
        .exec(),
    );
  }

  public removeOne(id: number): Observable<ArticleDocument | null> {
    return defer(() => this._articleModel.findByIdAndRemove(id).exec()).pipe(
      tap((article) => {
        if (article) {
          this._eventEmitter.emit(
            ProductEventEnum.Change,
            new ProductEvent('article', 'delete', article),
          );
        }
      }),
    );
  }

  public addFile(
    id: number,
    fileId: Types.ObjectId,
  ): Observable<TProductDocument | null> {
    return defer(() =>
      this._articleModel
        .findByIdAndUpdate(
          id,
          { $push: { files: fileId } },
          { runValidators: true, new: true },
        )
        .populate(['genericName', 'files'])
        .exec(),
    ).pipe(
      tap((article) => {
        if (article) {
          this._eventEmitter.emit(
            ProductEventEnum.Change,
            new ProductEvent('article', 'update', article),
          );
        }
      }),
    );
  }

  public removeFile(
    id: number,
    fileId: string,
  ): Observable<TProductDocument | null> {
    return defer(() =>
      this._articleModel
        .findByIdAndUpdate(
          id,
          { $pull: { files: new Types.ObjectId(fileId) } },
          { runValidators: true, new: true },
        )
        .populate(['genericName', 'files'])
        .exec(),
    ).pipe(
      tap((article) => {
        if (article) {
          this._eventEmitter.emit(
            ProductEventEnum.Change,
            new ProductEvent('article', 'update', article),
          );
        }
      }),
    );
  }
}
