import { ArticleDocument, PaperDocument } from '../schemas';

export type TProductDocument = ArticleDocument | PaperDocument;
