import { Article, Paper } from '../schemas';

export type TProduct = Article | Paper;
