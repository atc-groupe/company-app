import { TProductDocument } from '../types';
import { Observable } from 'rxjs';

export interface IProductSyncService {
  syncAll(): Promise<void>;
  syncOne(id: number): Observable<TProductDocument>;
}
