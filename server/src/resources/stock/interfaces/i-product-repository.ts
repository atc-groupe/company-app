import { TProduct } from '../types/t-product';
import { TProductDocument } from '../types';
import { Observable } from 'rxjs';
import { Types } from 'mongoose';

export interface IProductRepository {
  insertOne(product: TProduct): Observable<TProductDocument>;

  findById(id: number): Observable<TProductDocument | null>;

  findByName(name: string): Observable<TProductDocument[]>;

  findByGroupType(groupType: number): Observable<TProductDocument[]>;

  removeOne(id: number): Observable<TProductDocument | null>;

  addFile(
    id: number,
    fileId: Types.ObjectId,
  ): Observable<TProductDocument | null>;

  removeFile(id: number, fileId: string): Observable<TProductDocument | null>;
}
