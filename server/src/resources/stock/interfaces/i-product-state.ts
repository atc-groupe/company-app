import { Socket } from 'socket.io';

export interface IProductState {
  productId: number;
  sockets: Socket[];
}
