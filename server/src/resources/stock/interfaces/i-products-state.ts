import { Socket } from 'socket.io';

export interface IProductsState {
  productType: number;
  groupType: number;
  sockets: Socket[];
}
