export enum ArticleEventEnum {
  Create = 'stock.article.create',
  UpdateMp = 'stock.article.updateMp',
  Delete = 'stock.article.delete',
}
