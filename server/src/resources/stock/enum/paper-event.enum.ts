export enum PaperEventEnum {
  Create = 'stock.paper.create',
  UpdateMp = 'stock.paper.updateMp',
  Delete = 'stock.paper.delete',
}
