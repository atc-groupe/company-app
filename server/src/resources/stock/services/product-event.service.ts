import { Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { ProductEvent } from '../events/product-event';
import { ProductEventEnum } from '../enum';
import { ProductGateway } from '../product.gateway';
import { ProductStatesService } from './product-states.service';
import { ProductTypeEnum } from '../../../mp/mp-stock/enums';

@Injectable()
export class ProductEventService {
  constructor(
    private _gateway: ProductGateway,
    private _productsStates: ProductStatesService,
  ) {}

  @OnEvent(ProductEventEnum.Change)
  public handlePaperCreateEvent(event: ProductEvent) {
    const state = this._productsStates.getState(event.payload._id);

    if (state) {
      this._gateway.emitProductChange(
        `${event.subject}.change`,
        state.sockets,
        event.subject === 'article'
          ? ProductTypeEnum.Article
          : ProductTypeEnum.Paper,
        event.action,
        event.payload,
      );
    }
  }
}
