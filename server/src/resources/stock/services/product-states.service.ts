import { Injectable } from '@nestjs/common';
import { IProductState } from '../interfaces';
import { Socket } from 'socket.io';

@Injectable()
export class ProductStatesService {
  private _states = new Map<number, IProductState>();

  public addState(productId: number, socket: Socket): void {
    this.removeSocket(socket.id);
    const state = this._states.get(productId);

    if (state) {
      state.sockets.push(socket);
    } else {
      this._states.set(productId, { productId, sockets: [socket] });
    }
  }

  public removeSocket(socketId: string): void {
    this._states.forEach((state) => {
      state.sockets = state.sockets.filter((socket) => socket.id !== socketId);

      if (!state.sockets.length) {
        this._states.delete(state.productId);
      }
    });
  }

  public getState(productId: number): IProductState | null {
    const state = this._states.get(productId);

    return state ? state : null;
  }
}
