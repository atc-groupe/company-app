import {
  BadRequestException,
  HttpException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { ArticleRepository } from '../repositories';
import { PaperRepository } from '../repositories/paper.repository';
import { ProductTypeEnum } from '../../../mp/mp-stock/enums';
import { TProductDocument } from '../types';
import { catchError, firstValueFrom, map, Observable, switchMap } from 'rxjs';
import { ArticleSyncService } from './article-sync.service';
import { PaperSyncService } from './paper-sync.service';
import { IProductRepository, IProductSyncService } from '../interfaces';
import { ArticleDocument, PaperDocument } from '../schemas';
import { ArticleStockInfoDto, ProductGenericNameDto } from '../dto';
import { ProductGenericName } from '../schemas/product-generic-name.schema';

@Injectable()
export class ProductsService {
  constructor(
    private _articleRepository: ArticleRepository,
    private _paperRepository: PaperRepository,
    private _articleSyncService: ArticleSyncService,
    private _paperSyncService: PaperSyncService,
  ) {}

  public async getProductsList(
    productType: ProductTypeEnum,
    groupType: number,
  ): Promise<TProductDocument[]> {
    return firstValueFrom(
      this._getProductRepository(productType).findByGroupType(groupType),
    );
  }

  public getSheetsGenericNames(): Observable<ProductGenericName[]> {
    return this._paperRepository.findSheetsGenericNames();
  }

  public getPapersByName(id: number): Observable<PaperDocument[]> {
    return this._paperRepository.findById(id).pipe(
      switchMap((paper) => {
        if (!paper) {
          throw new NotFoundException('Papier introuvable');
        }

        return this._paperRepository.findByName(paper.mp.name);
      }),
      catchError((err) => {
        if (err instanceof HttpException) {
          throw err;
        }

        throw new InternalServerErrorException(
          'Impossible de récupérer les papiers',
        );
      }),
    );
  }

  public async syncAll(): Promise<void> {
    await this._articleSyncService.syncAll();
    await this._paperSyncService.syncAll();
  }

  public syncOne(
    productType: ProductTypeEnum,
    id: number,
  ): Observable<TProductDocument> {
    return this._getSyncService(productType).syncOne(id);
  }

  public updateProductGenericName(
    type: ProductTypeEnum,
    id: number,
    dto: ProductGenericNameDto,
  ): Observable<TProductDocument> {
    let product$: Observable<TProductDocument | null>;
    switch (type) {
      case ProductTypeEnum.Article:
        product$ = this._articleRepository.updateGenericName(id, dto);
        break;
      case ProductTypeEnum.Paper:
        product$ = this._paperRepository.updateGenericName(id, dto);
        break;
      default:
        throw new InternalServerErrorException('Unsupported product type');
    }

    return product$.pipe(
      map((product) => {
        if (!product) {
          throw new NotFoundException('Produit introuvable');
        }

        return product;
      }),
      catchError((err) => {
        if (err instanceof HttpException) {
          throw err;
        }

        throw new InternalServerErrorException(
          `Une erreur est survenue lors de la mise à jour du produit ${id}`,
        );
      }),
    );
  }

  public updateArticleStockInfo(
    id: number,
    dto: ArticleStockInfoDto,
  ): Observable<ArticleDocument> {
    return this._articleRepository.updateStockInfo(id, dto).pipe(
      map((article) => {
        if (!article) {
          throw new NotFoundException(`L'article ${id} est introuvable`);
        }

        return article;
      }),
      catchError((err) => {
        if (err instanceof HttpException) {
          throw err;
        }

        throw new InternalServerErrorException(
          `Une erreur est survenue lors de la mise à jour de l'article ${id}`,
        );
      }),
    );
  }

  private _getProductRepository(
    productType: ProductTypeEnum,
  ): IProductRepository {
    switch (productType) {
      case ProductTypeEnum.Paper:
        return this._paperRepository;
      case ProductTypeEnum.Article:
        return this._articleRepository;
      default:
        throw new BadRequestException(
          `Le type "Customer product" n'est pas implémenté`,
        );
    }
  }

  private _getSyncService(productType: ProductTypeEnum): IProductSyncService {
    switch (productType) {
      case ProductTypeEnum.Paper:
        return this._paperSyncService;
      case ProductTypeEnum.Article:
        return this._articleSyncService;
      default:
        throw new BadRequestException(
          `Le type "Customer product" n'est pas implémenté`,
        );
    }
  }
}
