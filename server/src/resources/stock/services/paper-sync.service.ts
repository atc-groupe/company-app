import {
  HttpException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { MpStockGetArticleListRepository } from '../../../mp/mp-stock/mp-stock-get-article-list.repository';
import { MpStockHandleArticleDetailsRepository } from '../../../mp/mp-stock/mp-stock-handle-article-details.repository';
import {
  catchError,
  firstValueFrom,
  forkJoin,
  map,
  Observable,
  of,
  switchMap,
} from 'rxjs';
import { Logger } from '../../../shared/services';
import { LoggerFactory } from '../../../shared/factories/logger.factory';
import { LogContextEnum } from '../../../shared/enum';
import { PaperDocument, PaperMp } from '../schemas';
import { PaperRepository } from '../repositories/paper.repository';
import { PaperMpMappingService } from './paper-mp-mapping.service';
import { IProductSyncService } from '../interfaces';

@Injectable()
export class PaperSyncService implements IProductSyncService {
  private _logger: Logger;

  constructor(
    loggerFactory: LoggerFactory,
    private _mpArticleListService: MpStockGetArticleListRepository,
    private _mpArticleDetailsService: MpStockHandleArticleDetailsRepository,
    private _paperRepository: PaperRepository,
    private _paperMpMapping: PaperMpMappingService,
  ) {
    this._logger = loggerFactory.get(LogContextEnum.stock);
  }

  public async syncAll(): Promise<void> {
    try {
      const mpPapers = await firstValueFrom(
        this._mpArticleListService.getPaperList(),
      );

      const ids: number[] = [];
      mpPapers.forEach((group) => {
        group.articles.forEach((article) => ids.push(article.product_id));
      });

      for (const id of ids) {
        try {
          await firstValueFrom(this.syncOne(id));
        } catch {}
      }

      const papers = await firstValueFrom(this._paperRepository.find());
      for (const paper of papers) {
        if (!ids.includes(paper._id)) {
          await firstValueFrom(this._paperRepository.removeOne(paper._id));
        }
      }
    } catch {
      throw new InternalServerErrorException(
        'Impossible de synchroniser les articles',
      );
    }
  }

  public syncOne(id: number): Observable<PaperDocument> {
    const mpPaper$ = this._mpArticleDetailsService.getPaperDetails(id);
    const paper$ = this._paperRepository.findById(id);
    let action: 'créé' | 'modifié' | 'ignored';

    return forkJoin([mpPaper$, paper$]).pipe(
      switchMap(([mpPaper, paper]) => {
        if (!mpPaper) {
          if (paper) {
            this._logger.warn(
              `[ProductSyncService] L'article' ${id} n'existe plus dans MultiPress. Il a été supprimé.`,
            );
            return this._paperRepository.removeOne(id);
          }

          throw new NotFoundException('Article introuvable dans MultiPress');
        }

        const mappedMpData = this._paperMpMapping.getMappedData(mpPaper);

        if (paper && this._hasNotChanged(paper.mp, mappedMpData)) {
          action = 'ignored';

          return of(paper);
        }

        if (paper) {
          action = 'modifié';
          return this._paperRepository.updateMp(id, mappedMpData);
        } else {
          action = 'créé';
          return this._paperRepository.insertOne({
            _id: id,
            mp: mappedMpData,
            genericName: null,
            files: [],
          });
        }
      }),
      map((article) => {
        if (!article) {
          throw new Error();
        }

        if (action !== 'ignored') {
          this._logger.log(
            `[ProductSyncService] Le papier ${id} a été ${action} avec succès`,
          );
        }

        return article;
      }),
      catchError((err) => {
        let message = `[ProductSyncService] Une erreur est survenue lors de la synchronisation de l'article. id ${id}`;
        let error: HttpException = new InternalServerErrorException(message);

        if (err instanceof HttpException) {
          message = `${message}. Message: ${err.message}`;
          error = err;
        }

        this._logger.error(message);
        throw error;
      }),
    );
  }

  private _hasNotChanged(existing: PaperMp, fresh: PaperMp): boolean {
    return (
      existing.name === fresh.name &&
      existing.groupType === fresh.groupType &&
      existing.code === fresh.code &&
      existing.color === fresh.color &&
      existing.width === fresh.width &&
      existing.length === fresh.length &&
      existing.thickness === fresh.thickness &&
      existing.insidePrint === fresh.insidePrint &&
      existing.stockManagement === fresh.stockManagement &&
      existing.minimumStock === fresh.minimumStock &&
      existing.stock === fresh.stock &&
      existing.reserved === fresh.reserved &&
      existing.freeStock === fresh.freeStock &&
      existing.supplier === fresh.supplier &&
      existing.displayUnit === fresh.displayUnit &&
      existing.unitPrice === fresh.unitPrice &&
      existing.stockValue === fresh.stockValue
    );
  }
}
