import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { AppFilesService } from '../../app-files/app-files.service';
import { AppFileDto } from '../../app-files/app-file.dto';
import { TProductDocument } from '../types';
import { firstValueFrom } from 'rxjs';
import { ProductTypeEnum } from '../../../mp/mp-stock/enums';
import { IProductRepository } from '../interfaces';
import { ArticleRepository, PaperRepository } from '../repositories';
import { AppFileInfoDto } from '../../app-files/app-file-info.dto';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { ProductEventEnum } from '../enum';
import { ProductEvent } from '../events/product-event';

@Injectable()
export class ProductsFilesService {
  constructor(
    private _appFileService: AppFilesService,
    private _articleRepository: ArticleRepository,
    private paperRepository: PaperRepository,
    private _eventEmitter: EventEmitter2,
  ) {}

  public async addFile(
    productType: ProductTypeEnum,
    productId: number,
    dto: AppFileDto,
    file: Express.Multer.File,
  ): Promise<TProductDocument | null> {
    try {
      const fileDoc = await this._appFileService.saveOne(dto, file);
      return firstValueFrom(
        this._getRepository(productType).addFile(productId, fileDoc._id),
      );
    } catch (err) {
      throw new InternalServerErrorException("Impossible d'ajouter le fichier");
    }
  }

  public async removeFile(
    productType: ProductTypeEnum,
    productId: number,
    fileId: string,
  ): Promise<TProductDocument | null> {
    try {
      await this._appFileService.removeOne(fileId);
      return firstValueFrom(
        this._getRepository(productType).removeFile(productId, fileId),
      );
    } catch (err) {
      throw new InternalServerErrorException("Impossible d'ajouter le fichier");
    }
  }

  public async updateFile(
    productType: ProductTypeEnum,
    productId: number,
    fileId: string,
    file: Express.Multer.File,
  ): Promise<TProductDocument | null> {
    try {
      await this._appFileService.updateOne(fileId, file);
      const product = await firstValueFrom(
        this._getRepository(productType).findById(productId),
      );

      if (product) {
        this._eventEmitter.emit(
          ProductEventEnum.Change,
          new ProductEvent(
            productType === ProductTypeEnum.Article ? 'article' : 'paper',
            'update',
            product,
          ),
        );
      }

      return product;
    } catch (err) {
      throw new InternalServerErrorException("Impossible d'ajouter le fichier");
    }
  }

  public async updateFileInfo(
    productType: ProductTypeEnum,
    productId: number,
    fileId: string,
    dto: AppFileInfoDto,
  ): Promise<TProductDocument | null> {
    try {
      await this._appFileService.updateInfo(fileId, dto);
      const product = await firstValueFrom(
        this._getRepository(productType).findById(productId),
      );

      if (product) {
        this._eventEmitter.emit(
          ProductEventEnum.Change,
          new ProductEvent(
            productType === ProductTypeEnum.Article ? 'article' : 'paper',
            'update',
            product,
          ),
        );
      }

      return product;
    } catch (err) {
      throw new InternalServerErrorException(
        'Impossible de modifier les informations du fichier',
      );
    }
  }

  private _getRepository(productType: ProductTypeEnum): IProductRepository {
    switch (productType) {
      case ProductTypeEnum.Article:
        return this._articleRepository;
      case ProductTypeEnum.Paper:
        return this.paperRepository;
      default:
        throw new BadRequestException('unsupported product type');
    }
  }
}
