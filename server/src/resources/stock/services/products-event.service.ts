import { Injectable } from '@nestjs/common';
import { ProductsGateway } from '../products.gateway';
import { OnEvent } from '@nestjs/event-emitter';
import { ProductsStatesService } from './products-states.service';
import { ProductTypeEnum } from '../../../mp/mp-stock/enums';
import { ProductEvent } from '../events/product-event';
import { ProductEventEnum } from '../enum';

@Injectable()
export class ProductsEventService {
  constructor(
    private _gateway: ProductsGateway,
    private _productsStates: ProductsStatesService,
  ) {}

  @OnEvent(ProductEventEnum.Change)
  public handlePaperCreateEvent(event: ProductEvent) {
    const state = this._productsStates.getState(
      ProductTypeEnum.Paper,
      event.payload.mp.groupType,
    );

    if (state) {
      this._gateway.emitProductChange(
        `${event.subject}.change`,
        state.sockets,
        event.subject === 'article'
          ? ProductTypeEnum.Article
          : ProductTypeEnum.Paper,
        event.action,
        event.payload,
      );
    }
  }
}
