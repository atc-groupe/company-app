import {
  HttpException,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { MpStockHandleMutationRepository } from '../../../mp/mp-stock/mp-stock-handle-mutation.repository';
import { ProductTypeEnum } from '../../../mp/mp-stock/enums';
import { IMpStockMutation } from '../../../mp/mp-stock/interfaces';
import { catchError, firstValueFrom, map, Observable, switchMap } from 'rxjs';
import { StockMutationDto } from '../dto';
import { ProductsService } from './products.service';
import { LoggerFactory } from '../../../shared/factories/logger.factory';
import { Logger } from '../../../shared/services';
import { LogContextEnum } from '../../../shared/enum';
import { TProductDocument } from '../types';

@Injectable()
export class StockMutationsService {
  private _logger: Logger;

  constructor(
    loggerFactory: LoggerFactory,
    private _mpStockMutationRepository: MpStockHandleMutationRepository,
    private _productService: ProductsService,
  ) {
    this._logger = loggerFactory.get(LogContextEnum.stock);
  }

  public async getMutations(
    productType: ProductTypeEnum,
    id: number,
    startDate: string,
    stopDate?: string,
  ): Promise<IMpStockMutation[]> {
    try {
      const start = new Date(startDate);
      const stop = stopDate ? new Date(stopDate) : new Date(startDate);

      if (!stopDate) {
        stop.setFullYear(stop.getFullYear() + 1);
        stop.setDate(stop.getDate() + 1);
      }

      const list = await firstValueFrom(
        this._mpStockMutationRepository.getMutations(
          productType,
          id,
          start.toLocaleDateString('fr-FR', { timeZone: 'Europe/Paris' }),
          stop.toLocaleDateString('fr-FR', { timeZone: 'Europe/Paris' }),
        ),
      );

      const mutations: IMpStockMutation[] = [];

      for (const item of list) {
        mutations.push(
          await firstValueFrom(
            this._mpStockMutationRepository.getMutation(
              productType,
              id,
              item.mutation_id,
            ),
          ),
        );
      }

      return mutations;
    } catch {
      const message = 'Impossible de récupérer les mutations de stock';
      this._logger.error(`[${this.constructor.name}] ${message}`);

      throw new InternalServerErrorException(message);
    }
  }

  public createMutation(
    productType: ProductTypeEnum,
    id: number,
    dto: StockMutationDto,
  ): Observable<TProductDocument> {
    return this._mpStockMutationRepository
      .createMutation(productType, id, dto)
      .pipe(
        switchMap(() => {
          return this._productService.syncOne(productType, id);
        }),
        catchError((err) => {
          let message = 'Une erreur est survenue lors de la mutation de stock';
          let exception = new InternalServerErrorException(message);
          if (err instanceof HttpException) {
            exception = err;
            message = `${message}. ${err.message}`;
          }

          this._logger.error(message);
          throw exception;
        }),
      );
  }

  public updateMutation(
    productType: ProductTypeEnum,
    id: number,
    mutationId: number,
    dto: StockMutationDto,
  ): Observable<void> {
    return this._mpStockMutationRepository
      .updateMutation(productType, id, mutationId, dto)
      .pipe(
        map(() => {
          this._productService.syncOne(productType, id);
        }),
        catchError((err) => {
          let message =
            'Une erreur est survenue lors de la mise à jour de la mutation de stock';
          let exception = new InternalServerErrorException(message);
          if (err instanceof HttpException) {
            exception = err;
            message = `${message}. ${err.message}`;
          }

          this._logger.error(message);
          throw exception;
        }),
      );
  }
}
