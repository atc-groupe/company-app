import { Injectable } from '@nestjs/common';
import { IProductsState } from '../interfaces';
import { Socket } from 'socket.io';

@Injectable()
export class ProductsStatesService {
  private _states: IProductsState[] = [];

  public get isEmpty(): boolean {
    return this._states.length === 0;
  }

  public addState(
    productType: number,
    groupType: number,
    socket: Socket,
  ): void {
    const state = this._states.find(
      (item) =>
        item.productType === productType && item.groupType === groupType,
    );

    if (state) {
      state.sockets.push(socket);
    } else {
      this._states.push({ groupType, productType, sockets: [socket] });
    }
  }

  public removeSocket(socketId: string): void {
    this._states.forEach((state) => {
      state.sockets = state.sockets.filter((socket) => socket.id !== socketId);
    });

    this._states = this._states.filter((state) => state.sockets.length > 0);
  }

  public getState(
    productType: number,
    groupType: number,
  ): IProductsState | null {
    const state = this._states.find(
      (item) =>
        item.productType === productType && item.groupType === groupType,
    );

    return state ? state : null;
  }
}
