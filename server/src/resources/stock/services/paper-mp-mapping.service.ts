import { Injectable } from '@nestjs/common';
import { PaperTypeEnum } from '../enum';
import { IMpPaper } from '../../../mp/mp-stock/interfaces';
import { PaperMp } from '../schemas';

@Injectable()
export class PaperMpMappingService {
  getMappedData(mpPaperDetails: IMpPaper): PaperMp {
    let type: PaperTypeEnum;
    let displayUnit: string;
    switch (mpPaperDetails.stock_price_type) {
      case 'L':
        type = PaperTypeEnum.Roll;
        displayUnit = 'ml';
        break;
      case 'M':
      case 'C':
        type = PaperTypeEnum.Sheet;
        displayUnit = 'feuilles';
        break;
      default:
        throw new Error(
          `Invalid article type. Type: ${mpPaperDetails.stock_price_type}`,
        );
    }

    let completeName = `${mpPaperDetails.extra_info} - ${mpPaperDetails.width} x ${mpPaperDetails.length}`;
    if (type === 'sheet') {
      completeName += ' mm';
    }

    if (type === 'roll') {
      completeName += ' ml';
    }

    return {
      name: mpPaperDetails.extra_info,
      completeName,
      type,
      groupType: mpPaperDetails.group_type,
      groupDescription: mpPaperDetails.group_description,
      code: mpPaperDetails.paper_code,
      color: mpPaperDetails.color,
      width: mpPaperDetails.width,
      length: mpPaperDetails.length,
      thickness: mpPaperDetails.thickness,
      insidePrint: mpPaperDetails.inside_print !== '',
      stockManagement: mpPaperDetails.in_stock === 'OUI',
      minimumStock: mpPaperDetails.minimum_stock,
      stock: mpPaperDetails.stock,
      freeStock: mpPaperDetails.free_stock,
      reserved: mpPaperDetails.reserved,
      supplier: mpPaperDetails.supplier,
      displayUnit,
      unitPrice: mpPaperDetails.stock_price,
      stockValue: mpPaperDetails.stock_value,
    };
  }
}
