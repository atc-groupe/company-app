import {
  BadRequestException,
  HttpException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import {
  ArticleRepository,
  PaperRepository,
  ProductGenericNameRepository,
} from '../repositories';
import { catchError, forkJoin, map, Observable, switchMap } from 'rxjs';
import { ProductGenericNameDocument } from '../schemas/product-generic-name.schema';

@Injectable()
export class ProductGenericNamesService {
  constructor(
    private _productNameRepository: ProductGenericNameRepository,
    private _paperRepository: PaperRepository,
    private _articleRepository: ArticleRepository,
  ) {}

  public insertOne(name: string): Observable<ProductGenericNameDocument> {
    return this._productNameRepository.insertOne(name).pipe(
      catchError((err) => {
        if (err.code === 11000) {
          throw new BadRequestException('Ce nom existe déjà');
        }

        throw new InternalServerErrorException(
          `Une erreur est survenue lors de l'ajout du nom`,
        );
      }),
    );
  }

  public findAll(): Observable<ProductGenericNameDocument[]> {
    return this._productNameRepository.findAll().pipe(
      catchError(() => {
        throw new InternalServerErrorException(
          `Une erreur est survenue lors de la récupération des données`,
        );
      }),
    );
  }

  public updateOne(
    id: string,
    name: string,
  ): Observable<ProductGenericNameDocument> {
    return this._productNameRepository.updateOne(id, name).pipe(
      map((name) => {
        if (!name) {
          throw new NotFoundException('Item introuvable');
        }

        return name;
      }),
      catchError((err) => {
        if (err.code === 11000) {
          throw new BadRequestException('Ce nom existe déjà');
        }

        if (err instanceof HttpException) {
          throw err;
        }

        throw new InternalServerErrorException(
          `Une erreur est survenue lors de l'ajout du nom`,
        );
      }),
    );
  }

  public removeOne(id: string) {
    return this._productNameRepository.removeOne(id).pipe(
      map((name) => {
        if (!name) {
          throw new NotFoundException('Item introuvable');
        }

        return name;
      }),
      switchMap(() => {
        return forkJoin([
          this._paperRepository.removeGenericNameForAll(id),
          this._articleRepository.removeGenericNameForAll(id),
        ]);
      }),
      catchError((err) => {
        if (err instanceof HttpException) {
          throw err;
        }

        throw new InternalServerErrorException(
          `Une erreur est survenue lors de l'ajout du nom`,
        );
      }),
    );
  }
}
