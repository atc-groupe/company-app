import {
  HttpException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { MpStockGetArticleListRepository } from '../../../mp/mp-stock/mp-stock-get-article-list.repository';
import { MpStockHandleArticleDetailsRepository } from '../../../mp/mp-stock/mp-stock-handle-article-details.repository';
import {
  catchError,
  firstValueFrom,
  forkJoin,
  map,
  Observable,
  of,
  switchMap,
} from 'rxjs';
import { ArticleRepository } from '../repositories';
import { ArticleMpMappingService } from './article-mp-mapping.service';
import { Logger } from '../../../shared/services';
import { LoggerFactory } from '../../../shared/factories/logger.factory';
import { LogContextEnum } from '../../../shared/enum';
import { ArticleDocument, ArticleMp } from '../schemas';
import { IProductSyncService } from '../interfaces';

@Injectable()
export class ArticleSyncService implements IProductSyncService {
  private _logger: Logger;

  constructor(
    loggerFactory: LoggerFactory,
    private _mpArticleListService: MpStockGetArticleListRepository,
    private _mpArticleDetailsService: MpStockHandleArticleDetailsRepository,
    private _articleRepository: ArticleRepository,
    private _articleMpMappingService: ArticleMpMappingService,
  ) {
    this._logger = loggerFactory.get(LogContextEnum.stock);
  }

  public async syncAll(): Promise<void> {
    try {
      const list = await firstValueFrom(
        this._mpArticleListService.getArticleList(),
      );

      const ids: number[] = [];
      list.forEach((group) => {
        group.articles.forEach((article) => ids.push(article.product_id));
      });

      for (const id of ids) {
        try {
          await firstValueFrom(this.syncOne(id));
        } catch {}
      }

      const articles = await firstValueFrom(this._articleRepository.find());
      for (const article of articles) {
        if (!ids.includes(article._id)) {
          await firstValueFrom(this._articleRepository.removeOne(article._id));
        }
      }
    } catch {
      throw new InternalServerErrorException(
        'Impossible de synchroniser les articles',
      );
    }
  }

  public syncOne(id: number): Observable<ArticleDocument> {
    const mpArticle$ = this._mpArticleDetailsService.getArticleDetails(id);
    const article$ = this._articleRepository.findById(id);
    let action: 'créé' | 'modifié' | 'ignored';

    return forkJoin([mpArticle$, article$]).pipe(
      switchMap(([mpArticle, article]) => {
        if (!mpArticle) {
          if (article) {
            this._logger.warn(
              `[ProductSyncService] L'article' ${id} n'existe plus dans MultiPress. Il a été supprimé.`,
            );
            return this._articleRepository.removeOne(id);
          }

          throw new NotFoundException('Article introuvable dans MultiPress');
        }

        const mappedMpData =
          this._articleMpMappingService.getMappedData(mpArticle);

        if (article && this._hasNotChanged(article.mp, mappedMpData)) {
          action = 'ignored';

          return of(article);
        }

        if (article) {
          action = 'modifié';
          return this._articleRepository.updateMp(id, mappedMpData);
        } else {
          action = 'créé';
          return this._articleRepository.insertOne({
            _id: id,
            mp: mappedMpData,
            genericName: null,
            files: [],
          });
        }
      }),
      map((article) => {
        if (!article) {
          throw new Error();
        }

        if (action !== 'ignored') {
          this._logger.log(
            `[ProductSyncService] L'article ${id} a été ${action} avec succès`,
          );
        }

        return article;
      }),
      catchError((err) => {
        let message = `[ProductSyncService] Une erreur est survenue lors de la synchronisation de l'article. id ${id}`;
        let error: HttpException = new InternalServerErrorException(message);

        if (err instanceof HttpException) {
          message = `${message}. Message: ${err.message}`;
          error = err;
        }

        this._logger.error(message);
        throw error;
      }),
    );
  }

  private _hasNotChanged(existing: ArticleMp, fresh: ArticleMp): boolean {
    return (
      existing.name === fresh.name &&
      existing.groupType === fresh.groupType &&
      existing.code === fresh.code &&
      existing.color === fresh.color &&
      existing.unit === fresh.unit &&
      existing.packedPer === fresh.packedPer &&
      existing.stockManagement === fresh.stockManagement &&
      existing.minimumStock === fresh.minimumStock &&
      existing.stock === fresh.stock &&
      existing.reserved === fresh.reserved &&
      existing.freeStock === fresh.freeStock &&
      existing.supplier === fresh.supplier &&
      existing.unitPrice === fresh.unitPrice &&
      existing.stockValue === fresh.stockValue
    );
  }
}
