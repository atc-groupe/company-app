import { Injectable } from '@nestjs/common';
import { IMpArticle } from '../../../mp/mp-stock/interfaces';
import { ArticleMp } from '../schemas';

@Injectable()
export class ArticleMpMappingService {
  getMappedData(mpArticle: IMpArticle): ArticleMp {
    return {
      name: mpArticle.description,
      completeName: mpArticle.description,
      groupType: mpArticle.group_type,
      groupDescription: mpArticle.group_description,
      code: mpArticle.article_number,
      color: mpArticle.color,
      unit: mpArticle.unit,
      displayUnit:
        mpArticle.group_description === 'Encre' ? 'ex.' : mpArticle.unit,
      packedPer: mpArticle.packed_per,
      stockManagement: mpArticle.in_stock === 'OUI',
      minimumStock: mpArticle.stock_minimum,
      stock: mpArticle.stock,
      reserved: mpArticle.reserved,
      freeStock: mpArticle.free_stock,
      supplier: mpArticle.supplier,
      unitPrice: mpArticle.cost_price,
      stockValue: mpArticle.stock_value,
    };
  }
}
