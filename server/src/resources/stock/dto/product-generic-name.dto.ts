import { IsOptional, IsString } from 'class-validator';

export class ProductGenericNameDto {
  @IsString()
  @IsOptional()
  id?: string;
}
