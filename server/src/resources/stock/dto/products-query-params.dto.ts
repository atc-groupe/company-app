import { ProductTypeEnum } from '../../../mp/mp-stock/enums';
import { IsEnum, IsNumber } from 'class-validator';

export class ProductsQueryParamsDto {
  @IsEnum(ProductTypeEnum)
  productType: ProductTypeEnum;

  @IsNumber()
  groupType: number;
}
