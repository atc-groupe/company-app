import { IsEnum, IsNumber } from 'class-validator';
import { ProductTypeEnum } from '../../../mp/mp-stock/enums';

export class ProductQueryParamsDto {
  @IsEnum(ProductTypeEnum)
  productType: ProductTypeEnum;

  @IsNumber()
  productId: number;
}
