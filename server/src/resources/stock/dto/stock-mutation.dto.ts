import { IsNumber, ValidateNested } from 'class-validator';
import { StockMutationDataDto } from './stock-mutation-data.dto';

export class StockMutationDto {
  @IsNumber()
  employee_number: number;

  @ValidateNested()
  data: StockMutationDataDto;
}
