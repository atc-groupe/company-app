import { IsNumber, IsString } from 'class-validator';
import { IsNullable } from '../../../shared/class-validator';

export class ArticleStockInfoDto {
  @IsNumber()
  @IsNullable()
  stockUnitNumber: number;

  @IsString()
  @IsNullable()
  stockUnitLabel: string;
}
