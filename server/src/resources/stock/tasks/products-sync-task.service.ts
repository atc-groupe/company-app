import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { PaperSyncService, ArticleSyncService } from '../services';
import { ConfigService } from '@nestjs/config';
import { Logger } from '../../../shared/services';
import { LoggerFactory } from '../../../shared/factories/logger.factory';
import { LogContextEnum } from '../../../shared/enum';

@Injectable()
export class ProductsSyncTaskService {
  private _logger: Logger;
  private _disabled = false;

  constructor(
    _loggerFactory: LoggerFactory,
    private _paperSyncService: PaperSyncService,
    private _articleSyncService: ArticleSyncService,
    private _configService: ConfigService,
  ) {
    this._logger = _loggerFactory.get(LogContextEnum.stock);
    const ALL_TASKS = this._configService.get('ALL_TASKS');
    const STOCK_TASKS = this._configService.get('STOCK_TASKS');

    if (ALL_TASKS === 'off' || STOCK_TASKS === 'off') {
      this._disabled = true;
    }
  }
  @Cron('0 */1 5-21 * * *', { timeZone: 'Europe/Paris' })
  public async syncProducts(): Promise<void> {
    if (this._disabled) {
      this._logger.log(
        `[ProductsSyncTaskService.syncProducts] task skipped. See .env file`,
      );

      return;
    }

    await this._articleSyncService.syncAll();
    await this._paperSyncService.syncAll();
  }
}
