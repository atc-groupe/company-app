import {
  ConnectedSocket,
  MessageBody,
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketGateway,
} from '@nestjs/websockets';
import { Socket } from 'socket.io';
import { TProductDocument } from './types';
import { ProductsQueryParamsDto } from './dto';
import { ProductsService } from './services/products.service';
import { ProductsStatesService } from './services/products-states.service';
import { AppWsInternalServerErrorException } from '../../shared/exceptions';
import { AppWsSuccessResponse } from '../../shared/classes';
import { TProduct } from './types/t-product';
import { ProductTypeEnum } from '../../mp/mp-stock/enums';
import { UseGuards } from '@nestjs/common';
import { AuthGatewayGuard } from '../auth/auth-gateway.guard';

@UseGuards(AuthGatewayGuard)
@WebSocketGateway({ transports: ['websocket'], namespace: 'stock.products' })
export class ProductsGateway implements OnGatewayDisconnect {
  constructor(
    private _productsStatesService: ProductsStatesService,
    private _productsService: ProductsService,
  ) {}

  handleDisconnect(client: Socket): void {
    this._productsStatesService.removeSocket(client.id);
  }

  @SubscribeMessage('products.subscribe')
  public async onSubscribeData(
    @MessageBody() dto: ProductsQueryParamsDto,
    @ConnectedSocket() client: Socket,
  ): Promise<AppWsSuccessResponse<TProduct[]>> {
    try {
      const products = await this._productsService.getProductsList(
        dto.productType,
        dto.groupType,
      );

      this._productsStatesService.removeSocket(client.id);
      this._productsStatesService.addState(
        dto.productType,
        dto.groupType,
        client,
      );

      return new AppWsSuccessResponse(products);
    } catch (err) {
      throw new AppWsInternalServerErrorException(
        'Une erreur est survenue lors de la récupération des données du stock',
      );
    }
  }

  public emitProductChange(
    event: string,
    sockets: Socket[],
    productType: ProductTypeEnum,
    action: 'create' | 'update' | 'delete',
    product: TProductDocument,
  ): void {
    sockets.forEach((socket: Socket) => {
      socket.emit('product.change', { productType, action, product });
    });
  }
}
