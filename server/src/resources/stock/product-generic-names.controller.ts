import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { ProductGenericNamesService } from './services';
import { ProductGenericNameDocument } from './schemas/product-generic-name.schema';
import { Observable } from 'rxjs';
import { Authorizations } from '../auth/authorization.decorator';
import { AuthActionsSettingsEnum, AuthSubjectsEnum } from '../../shared/enum';

@Controller('stock/product-generic-names')
export class ProductGenericNamesController {
  constructor(
    private readonly _productGenericNamesService: ProductGenericNamesService,
  ) {}

  @Get()
  public findAll(): Observable<ProductGenericNameDocument[]> {
    return this._productGenericNamesService.findAll();
  }

  @Post()
  @Authorizations({
    subject: AuthSubjectsEnum.Settings,
    action: AuthActionsSettingsEnum.ManageProductsGenericNames,
  })
  public insertOne(
    @Body('name') name: string,
  ): Observable<ProductGenericNameDocument> {
    return this._productGenericNamesService.insertOne(name);
  }

  @Patch(':id')
  @Authorizations({
    subject: AuthSubjectsEnum.Settings,
    action: AuthActionsSettingsEnum.ManageProductsGenericNames,
  })
  public updateOne(
    @Param('id') id: string,
    @Body('name') name: string,
  ): Observable<ProductGenericNameDocument> {
    return this._productGenericNamesService.updateOne(id, name);
  }

  @Delete(':id')
  @Authorizations({
    subject: AuthSubjectsEnum.Settings,
    action: AuthActionsSettingsEnum.ManageProductsGenericNames,
  })
  public deleteOne(@Param('id') id: string) {
    return this._productGenericNamesService.removeOne(id);
  }
}
