import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { PaperTypeEnum } from '../enum';
import { PaperGroupTypeEnum } from '../../../mp/mp-stock/enums';

@Schema({ _id: false })
export class PaperMp {
  @Prop() name: string; // MP extra_info field
  @Prop() completeName: string;
  @Prop() type: PaperTypeEnum; // stock_price_type="C" => sheet - stock_price_type="L" => roll
  @Prop({ type: Number }) groupType: PaperGroupTypeEnum;
  @Prop() groupDescription: string;
  @Prop() code: string;
  @Prop() color: string;
  @Prop() width: number;
  @Prop() length: number; // roll: Ml - sheet: mm
  @Prop() thickness: number;
  @Prop() insidePrint: boolean;
  @Prop() stockManagement: boolean;
  @Prop() minimumStock: number;
  @Prop() stock: number;
  @Prop() reserved: number;
  @Prop() freeStock: number;
  @Prop() supplier: string;
  @Prop() displayUnit: string;
  @Prop() unitPrice: number;
  @Prop() stockValue: number;
}

export const PaperMpSchema = SchemaFactory.createForClass(PaperMp);
