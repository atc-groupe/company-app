import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

@Schema()
export class ProductGenericName {
  @Prop({ type: String, unique: true, required: true })
  name: string;
}

export const ProductGenericNameSchema =
  SchemaFactory.createForClass(ProductGenericName);
export type ProductGenericNameDocument = HydratedDocument<ProductGenericName>;
