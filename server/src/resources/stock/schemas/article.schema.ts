import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument, Types } from 'mongoose';
import { ArticleMp, ArticleMpSchema } from './article-mp.schema';
import { ProductGenericName } from './product-generic-name.schema';
import { AppFile } from '../../app-files/app-file.schema';

@Schema()
export class Article {
  @Prop()
  _id: number;

  @Prop({ type: ArticleMpSchema })
  mp: ArticleMp;

  @Prop({ type: Number })
  stockUnitNumber?: number;

  @Prop({ type: String })
  stockUnitLabel?: string;

  @Prop({ type: Types.ObjectId, ref: 'ProductGenericName', default: null })
  genericName: ProductGenericName | null;

  @Prop({ type: [{ type: Types.ObjectId }], ref: 'AppFile', default: [] })
  files: AppFile[];
}

export const ArticleSchema = SchemaFactory.createForClass(Article);
export type ArticleDocument = HydratedDocument<Article>;
