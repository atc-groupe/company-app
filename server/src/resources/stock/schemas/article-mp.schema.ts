import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ArticleGroupTypeEnum } from '../../../mp/mp-stock/enums';

@Schema({ _id: false })
export class ArticleMp {
  @Prop() name: string; // MP description field
  @Prop() completeName: string;
  @Prop({ type: Number }) groupType: ArticleGroupTypeEnum;
  @Prop() groupDescription: string;
  @Prop() code: string;
  @Prop() color: string;
  @Prop() unit: string;
  @Prop() displayUnit: string;
  @Prop() packedPer: number;
  @Prop() stockManagement: boolean;
  @Prop() minimumStock: number;
  @Prop() stock: number;
  @Prop() reserved: number;
  @Prop() freeStock: number;
  @Prop() supplier: string;
  @Prop() unitPrice: number;
  @Prop() stockValue: number;
}

export const ArticleMpSchema = SchemaFactory.createForClass(ArticleMp);
