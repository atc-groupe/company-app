import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument, Types } from 'mongoose';
import { PaperMp, PaperMpSchema } from './paper-mp.schema';
import { ProductGenericName } from './product-generic-name.schema';
import { AppFile } from '../../app-files/app-file.schema';

@Schema()
export class Paper {
  @Prop()
  _id: number;

  @Prop({ type: PaperMpSchema })
  mp: PaperMp;

  @Prop({ type: Types.ObjectId, ref: 'ProductGenericName', default: null })
  genericName: ProductGenericName | null;

  @Prop({ type: [{ type: Types.ObjectId }], ref: 'AppFile', default: [] })
  files: AppFile[];
}

export const PaperSchema = SchemaFactory.createForClass(Paper);
export type PaperDocument = HydratedDocument<Paper>;
