import { Global, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppFile, AppFileSchema } from './app-file.schema';
import { AppFilesService } from './app-files.service';
import { AppFilesRepository } from './app-files.repository';
import { LoggerModule } from '../../shared/modules';
import { FilesManagerService } from './files-manager.service';

@Global()
@Module({
  imports: [
    MongooseModule.forFeature([{ name: AppFile.name, schema: AppFileSchema }]),
    LoggerModule,
  ],
  providers: [AppFilesService, AppFilesRepository, FilesManagerService],
  exports: [AppFilesService],
})
export class AppFilesModule {}
