import { Injectable } from '@nestjs/common';
import { AppFileDocument } from './app-file.schema';
import { join } from 'node:path';
import { LoggerFactory } from '../../shared/factories/logger.factory';
import { Logger } from '../../shared/services';
import { LogContextEnum } from '../../shared/enum';
import { mkdir, unlink, writeFile } from 'node:fs/promises';

@Injectable()
export class FilesManagerService {
  private _logger: Logger;
  constructor(_loggerFactory: LoggerFactory) {
    this._logger = _loggerFactory.get(LogContextEnum.files);
  }

  public async saveOne(
    fileDoc: AppFileDocument,
    buffer: Buffer,
  ): Promise<boolean> {
    try {
      await mkdir(this._getFileDir(fileDoc), { recursive: true });
      await writeFile(this._getFilePath(fileDoc), buffer);

      return true;
    } catch (err) {
      this._logger.error(`[FilesManagerService]. saveOne error: ${err}`);
      return false;
    }
  }

  public async removeOne(fileDoc: AppFileDocument): Promise<boolean> {
    try {
      await unlink(this._getFilePath(fileDoc));

      return true;
    } catch (err) {
      this._logger.error(`[FilesManager]. removeOne error: ${err}`);

      return false;
    }
  }

  public getExtFromMimeType(mimeType: string): string {
    switch (mimeType) {
      case 'application/pdf':
        return '.pdf';
      case 'image/jpeg':
        return '.jpeg';
      case 'image/png':
        return '.png';
      default:
        throw new Error(`Unsupported mime type: ${mimeType}`);
    }
  }

  private _getFileDir(file: AppFileDocument): string {
    return join(
      __dirname,
      '..',
      '..',
      '..',
      '..',
      'static',
      file.module,
      ...file.path.split('/'),
    );
  }

  private _getFilePath(file: AppFileDocument): string {
    return `${this._getFileDir(file)}/${file._id}${file.extension}`;
  }
}
