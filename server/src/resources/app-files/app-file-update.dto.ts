import { IsString } from 'class-validator';

export class AppFileUpdateDto {
  @IsString()
  extension: string;

  @IsString()
  mimeType: string;
}
