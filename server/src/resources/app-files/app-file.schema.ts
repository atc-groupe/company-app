import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { AppModuleEnum } from '../../shared/enum';
import { HydratedDocument } from 'mongoose';

@Schema()
export class AppFile {
  @Prop()
  name: string;

  @Prop({ type: String, enum: AppModuleEnum })
  module: AppModuleEnum;

  @Prop()
  path: string;

  @Prop()
  extension: string;

  @Prop()
  mimeType: string;

  @Prop()
  description: string;

  @Prop()
  createdAt: Date;

  @Prop()
  updatedAt: Date;
}

export const AppFileSchema = SchemaFactory.createForClass(AppFile);
export type AppFileDocument = HydratedDocument<AppFile>;
