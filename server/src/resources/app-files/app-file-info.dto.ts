import { IsOptional, IsString } from 'class-validator';

export class AppFileInfoDto {
  @IsString()
  @IsOptional()
  name: string;

  @IsString()
  @IsOptional()
  description: string;
}
