import { IsEnum, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { AppModuleEnum } from '../../shared/enum';

export class AppFileDto {
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsEnum(AppModuleEnum)
  @IsNotEmpty()
  module: AppModuleEnum;

  @IsString()
  @IsNotEmpty()
  path: string;

  @IsString()
  @IsOptional()
  description: string;

  extension: string;

  mimeType: string;
}
