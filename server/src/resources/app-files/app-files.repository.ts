import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { AppFile, AppFileDocument } from './app-file.schema';
import { AppFileDto } from './app-file.dto';
import { AppFileUpdateDto } from './app-file-update.dto';
import { AppFileInfoDto } from './app-file-info.dto';

@Injectable()
export class AppFilesRepository {
  constructor(
    @InjectModel(AppFile.name) private _appFileModel: Model<AppFile>,
  ) {}

  public createOne(dto: AppFileDto): Promise<AppFileDocument> {
    return this._appFileModel.create({
      ...dto,
      createdAt: new Date(),
      updatedAt: new Date(),
    });
  }

  public findOne(id: string): Promise<AppFileDocument | null> {
    return this._appFileModel.findById(id).exec();
  }

  public updateFile(
    id: string | Types.ObjectId,
    dto: AppFileUpdateDto,
  ): Promise<AppFileDocument | null> {
    return this._appFileModel
      .findByIdAndUpdate(
        id,
        {
          $set: {
            extension: dto.extension,
            mimeType: dto.mimeType,
            updatedAt: new Date(),
          },
        },
        {
          new: true,
          runValidators: true,
        },
      )
      .exec();
  }

  public updateFileInfo(
    id: string | Types.ObjectId,
    dto: AppFileInfoDto,
  ): Promise<AppFileDocument | null> {
    return this._appFileModel
      .findByIdAndUpdate(
        id,
        { $set: { name: dto.name, description: dto.description } },
        {
          new: true,
          runValidators: true,
        },
      )
      .exec();
  }

  public removeOne(
    id: string | Types.ObjectId,
  ): Promise<AppFileDocument | null> {
    return this._appFileModel.findByIdAndRemove(id).exec();
  }
}
