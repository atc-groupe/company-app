import {
  HttpException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { AppFilesRepository } from './app-files.repository';
import { AppFileDto } from './app-file.dto';
import { AppFileDocument } from './app-file.schema';
import { LoggerFactory } from '../../shared/factories/logger.factory';
import { Logger } from '../../shared/services';
import { LogContextEnum } from '../../shared/enum';
import { FilesManagerService } from './files-manager.service';
import { AppFileInfoDto } from './app-file-info.dto';
import { AppFileUpdateDto } from './app-file-update.dto';

@Injectable()
export class AppFilesService {
  private _logger: Logger;

  constructor(
    _loggerFactory: LoggerFactory,
    private _repository: AppFilesRepository,
    private _filesManager: FilesManagerService,
  ) {
    this._logger = _loggerFactory.get(LogContextEnum.files);
  }

  public async saveOne(
    dto: AppFileDto,
    file: Express.Multer.File,
  ): Promise<AppFileDocument> {
    try {
      dto.mimeType = file.mimetype;
      dto.extension = this._filesManager.getExtFromMimeType(file.mimetype);

      const fileDoc = await this._repository.createOne(dto);
      const result = await this._filesManager.saveOne(fileDoc, file.buffer);

      if (!result) {
        await this._repository.removeOne(fileDoc._id);
        throw new Error();
      }

      return fileDoc;
    } catch {
      throw new InternalServerErrorException(
        `Une erreur est survenue lors de l'enregistrement du fichier`,
      );
    }
  }

  public async updateOne(
    id: string,
    file: Express.Multer.File,
  ): Promise<AppFileDocument> {
    try {
      const fileDoc = await this._repository.findOne(id);

      if (!fileDoc) {
        throw new NotFoundException('Fichier introuvable');
      }

      const deleteResult = await this._filesManager.removeOne(fileDoc);
      if (!deleteResult) {
        throw new InternalServerErrorException();
      }

      fileDoc.mimeType = file.mimetype;
      fileDoc.extension = this._getExtFromMimeType(file.mimetype);
      const saveResult = await this._filesManager.saveOne(fileDoc, file.buffer);

      if (!saveResult) {
        throw new InternalServerErrorException();
      }

      const dto: AppFileUpdateDto = {
        mimeType: file.mimetype,
        extension: this._getExtFromMimeType(file.mimetype),
      };

      const newDoc = await this._repository.updateFile(id, dto);

      return newDoc!;
    } catch (err) {
      if (err instanceof NotFoundException) {
        throw err;
      }

      throw new InternalServerErrorException(
        'Impossible de modifier le fichier',
      );
    }
  }

  public async updateInfo(
    id: string,
    dto: AppFileInfoDto,
  ): Promise<AppFileDocument> {
    try {
      const newDoc = await this._repository.updateFileInfo(id, dto);

      if (!newDoc) {
        throw new NotFoundException('Le document est introuvable');
      }

      return newDoc;
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new InternalServerErrorException(
        'Une erreur est survenue lors de la mise à jour des informations du fichier',
      );
    }
  }

  public async removeOne(id: string): Promise<AppFileDocument> {
    try {
      const fileDoc = await this._repository.findOne(id);

      if (!fileDoc) {
        throw new NotFoundException('Fichier introuvable');
      }

      const result = await this._filesManager.removeOne(fileDoc);
      if (!result) {
        throw new InternalServerErrorException();
      }

      const deletedDoc = await this._repository.removeOne(id);

      return deletedDoc!;
    } catch (err) {
      if (err instanceof NotFoundException) {
        throw err;
      }

      throw new InternalServerErrorException(
        'Impossible de supprimer le fichier',
      );
    }
  }

  private _getExtFromMimeType(mimeType: string): string {
    switch (mimeType) {
      case 'application/pdf':
        return '.pdf';
      case 'image/jpeg':
        return '.jpeg';
      case 'image/png':
        return '.png';
      default:
        throw new Error(`Unsupported mime type: ${mimeType}`);
    }
  }
}
