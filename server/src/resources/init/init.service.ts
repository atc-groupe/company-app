import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { UsersService } from '../users/services';
import { SettingsService } from '../settings/services';
import { firstValueFrom } from 'rxjs';

@Injectable()
export class InitService {
  constructor(
    private _configService: ConfigService,
    private _usersService: UsersService,
    private _settingsService: SettingsService,
  ) {}

  public async initialize(): Promise<void> {
    try {
      await firstValueFrom(this._settingsService.initialize());
      await firstValueFrom(
        this._usersService.createOne({
          employeeNumber: this._configService.getOrThrow(
            'DEFAULT_USER_EMPLOYEE_NUMBER',
          ),
          userPrincipalName: this._configService.getOrThrow(
            'DEFAULT_USER_PRINCIPAL_NAME',
          ),
          isAdmin: true,
          externalAccess: true,
        }),
      );
    } catch (err) {
      throw new InternalServerErrorException(
        `Une erreur est survenue lors de l'initialisation de l'application. Erreur: ${err}`,
      );
    }
  }
}
