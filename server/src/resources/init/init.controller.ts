import { Controller, Ip, Post, UnauthorizedException } from '@nestjs/common';
import { InitService } from './init.service';
import { Public } from '../auth/public.decorator';

@Controller('init')
export class InitController {
  constructor(private _initService: InitService) {}

  @Public()
  @Post()
  public async initialize(@Ip() ip: string) {
    if (!ip.endsWith('127.0.0.1')) {
      throw new UnauthorizedException(
        'App settings initialization must be done from server',
      );
    }

    await this._initService.initialize();
  }
}
