import { WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Server } from 'socket.io';
import { Log } from './log.schema';

@WebSocketGateway({ transports: ['websocket'], namespace: 'logs' })
export class LogsGateway {
  @WebSocketServer() private _server: Server;

  emit(log: Log): void {
    this._server.emit('log', log);
  }
}
