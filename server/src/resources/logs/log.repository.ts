import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Log, LogDocument } from './log.schema';
import { Model } from 'mongoose';
import { LogDto } from './log.dto';
import { LogQueryParamsDto } from './log-query-params.dto';
import { defer, Observable } from 'rxjs';
import { LogsGateway } from './logs.gateway';

@Injectable()
export class LogRepository {
  constructor(
    @InjectModel(Log.name)
    private logModel: Model<Log>,
    private _logsGateway: LogsGateway,
  ) {}

  public find(query: LogQueryParamsDto): Observable<LogDocument[]> {
    if (!query.level && !query.context) {
      return defer(() => this.logModel.find().exec());
    }

    let searchParams: any = {};
    let levelParam: any = null;

    if (query.level && query.level !== 'log') {
      if (query.level === 'warn') {
        levelParam = { $or: [{ level: 'warn' }, { level: 'error' }] };
      } else {
        levelParam = { level: 'error' };
      }
    }

    if (query.context && levelParam) {
      searchParams = { $and: [{ context: query.context }, levelParam] };
    } else if (query.context) {
      searchParams = { context: query.context };
    } else {
      searchParams = levelParam;
    }

    return defer(() =>
      this.logModel
        .find(searchParams)
        .sort({ createdAt: -1 })
        .select('-__v')
        .limit(1000)
        .exec(),
    );
  }

  public async insertOne(appLogDto: LogDto): Promise<LogDocument> {
    const log = await this.logModel.create(appLogDto);
    this._logsGateway.emit(log);

    return log;
  }
}
