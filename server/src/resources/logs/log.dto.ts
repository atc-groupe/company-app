import { LogContextEnum, LogLevelEnum } from '../../shared/enum';

export class LogDto {
  context: LogContextEnum;
  level: LogLevelEnum;
  message: string;
}
