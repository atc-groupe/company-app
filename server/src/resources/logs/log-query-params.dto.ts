import { LogContextEnum, LogLevelEnum } from '../../shared/enum';
import { IsEnum, IsOptional } from 'class-validator';

export class LogQueryParamsDto {
  @IsEnum(LogContextEnum)
  @IsOptional()
  context: LogContextEnum;

  @IsEnum(LogLevelEnum)
  @IsOptional()
  level: LogLevelEnum;
}
