import { Controller, Get, Query } from '@nestjs/common';
import { LogQueryParamsDto } from './log-query-params.dto';
import { LogsService } from './logs.service';
import { Log } from './log.schema';
import { Observable } from 'rxjs';

@Controller('logs')
export class LogsController {
  constructor(private _logsService: LogsService) {}

  @Get()
  public findAll(@Query() query: LogQueryParamsDto): Observable<Log[]> {
    return this._logsService.find(query);
  }
}
