import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { LogRepository } from './log.repository';
import { LogQueryParamsDto } from './log-query-params.dto';
import { catchError, Observable } from 'rxjs';
import { Log } from './log.schema';

@Injectable()
export class LogsService {
  constructor(private _logRepository: LogRepository) {}

  public find(query: LogQueryParamsDto): Observable<Log[]> {
    return this._logRepository.find(query).pipe(
      catchError(() => {
        throw new InternalServerErrorException(
          'Une erreur est survenue lors de la récupération des logs',
        );
      }),
    );
  }
}
