import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import { LogContextEnum, LogLevelEnum } from '../../shared/enum';

@Schema()
export class Log {
  @Prop({ type: String, required: true, index: true })
  context: LogContextEnum;

  @Prop({ type: String, required: true, index: true })
  level: LogLevelEnum;

  @Prop({ type: String, required: true })
  message: string;

  @Prop({ type: Date, expires: '30d', default: Date.now })
  createdAt: Date;
}

export const LogSchema = SchemaFactory.createForClass(Log);
export type LogDocument = HydratedDocument<Log>;
