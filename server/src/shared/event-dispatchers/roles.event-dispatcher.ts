import { AbstractEventDispatcher } from '../classes';
import { Injectable } from '@nestjs/common';

@Injectable()
export class RolesEventDispatcher extends AbstractEventDispatcher {}
