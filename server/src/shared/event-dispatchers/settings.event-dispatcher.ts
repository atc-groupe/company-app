import { Injectable } from '@nestjs/common';
import { AbstractEventDispatcher } from '../classes';

@Injectable()
export class SettingsEventDispatcher extends AbstractEventDispatcher {}
