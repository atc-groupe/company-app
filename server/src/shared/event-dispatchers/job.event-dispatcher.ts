import { Injectable } from '@nestjs/common';
import { AbstractEventDispatcher } from '../classes';

@Injectable()
export class JobEventDispatcher extends AbstractEventDispatcher {}
