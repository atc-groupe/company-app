import { Injectable } from '@nestjs/common';
import { AbstractEventDispatcher } from '../classes';

@Injectable()
export class UsersEventDispatcher extends AbstractEventDispatcher {}
