import { Injectable } from '@nestjs/common';
import { BehaviorSubject, Observable } from 'rxjs';
import { ISseEventData } from '../interfaces';

@Injectable()
export class SseService {
  private _event = new BehaviorSubject<MessageEvent>({
    data: null,
  } as MessageEvent);

  public pushEvent(data: ISseEventData): void {
    this._event.next({ data } as MessageEvent);
  }

  get events(): Observable<MessageEvent> {
    return this._event;
  }
}
