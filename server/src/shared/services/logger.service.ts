import { ConsoleLogger } from '@nestjs/common';
import { LogRepository } from '../../resources/logs/log.repository';
import { LogContextEnum, LogLevelEnum } from '../enum';
import { LogDto } from '../../resources/logs/log.dto';

export class Logger {
  constructor(
    private logsService: LogRepository,
    private consoleLogger: ConsoleLogger,
    private context: LogContextEnum,
  ) {}

  log(message: string, save = true, context?: LogContextEnum) {
    this.consoleLogger.log(message, this.getContext(context));

    if (save) {
      this._save(LogLevelEnum.log, message, context);
    }
  }

  warn(message: string, save = true, context?: LogContextEnum) {
    this.consoleLogger.warn(message, this.getContext(context));

    if (save) {
      this._save(LogLevelEnum.warn, message, context);
    }
  }

  error(
    message: string,
    save = true,
    context?: LogContextEnum,
    stack?: string,
  ) {
    this.consoleLogger.error(message, stack, this.getContext(context));

    if (save) {
      this._save(LogLevelEnum.error, message, context);
    }
  }

  private _save(
    level: LogLevelEnum,
    message: string,
    context?: LogContextEnum,
  ) {
    const log = new LogDto();
    log.context = context ? context : this.context;
    log.level = level;
    log.message = message;

    this.logsService.insertOne(log);
  }

  private getContext(context?: string) {
    return context ? context : this.context;
  }
}
