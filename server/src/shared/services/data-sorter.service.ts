import { Injectable } from '@nestjs/common';

@Injectable()
export class DataSorter {
  public sort<T>(collection: T[], dataComparatorFn: (item: T) => any): T[] {
    return collection.sort((a: T, b: T) => {
      const aComp = dataComparatorFn(a);
      const bComp = dataComparatorFn(b);

      if (aComp < bComp) {
        return -1;
      }
      if (aComp > bComp) {
        return 1;
      }

      return 0;
    });
  }
}
