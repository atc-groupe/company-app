import { Injectable } from '@nestjs/common';

@Injectable()
export class DateHelper {
  public getDate(rawDate: string): string {
    const date = new Date(rawDate);
    return date.toLocaleDateString('en-US');
  }

  public getDateOrNull(rawDate: string): string | null {
    if (
      rawDate === '0000-00-00T00:00:00Z' ||
      rawDate === '0000-00-00' ||
      rawDate === '0000-00-00T00:00:00'
    ) {
      return null;
    }

    return this.getDate(rawDate);
  }

  public extractTime(rawDate: string): number {
    const split = rawDate.split('T')[1].split(':');

    const hoursMs = parseInt(split[0]) * 60 * 60 * 1000;
    const minutesMs = parseInt(split[1]) * 60 * 1000;
    const secondsMs = parseInt(split[2]) * 1000;

    return hoursMs + minutesMs + secondsMs;
  }

  public getDisplayTime(timeInHours: number): string {
    const hours = Math.floor(timeInHours);
    const minutes = Math.round((timeInHours - hours) * 60);

    if (hours && minutes) {
      return `${hours}h${minutes}`;
    }

    if (hours) {
      return `${hours}h`;
    }

    return `${minutes}mn`;
  }
}
