export interface ISyncElement {
  syncAction?: 'add' | 'update' | 'remove';
}
