import { AuthSubjectsEnum } from '../enum';
import { TAuthAction } from '../types/t-auth-action';

export interface IAuthorization {
  subject: AuthSubjectsEnum;
  action: TAuthAction;
}
