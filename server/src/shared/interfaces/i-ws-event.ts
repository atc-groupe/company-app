import { WsEventActionEnum } from '../enum';

export interface IWsEvent {
  action: WsEventActionEnum;
  data: any;
}
