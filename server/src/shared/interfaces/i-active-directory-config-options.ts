export interface IActiveDirectoryConfigOptions {
  url: string;
  baseDN: string;
  username: string;
  password: string;
  usersGroup: string;
  adminsGroup: string;
  user: string[];
}
