import { Request } from 'express';
import { UserDocument } from '../../resources/users/schema';

export interface IAppRequest extends Request {
  user: UserDocument;
}
