export interface IEventListener {
  on(name: string, data: any): any;
}
