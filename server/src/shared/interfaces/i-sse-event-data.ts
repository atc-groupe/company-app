import { SseEventActionEnum, SseEventResourceEnum } from '../enum';

export interface ISseEventData {
  resource: SseEventResourceEnum;
  action: SseEventActionEnum;
  data: any;
}
