import { ConsoleLogger, Injectable } from '@nestjs/common';
import { LogRepository } from '../../resources/logs/log.repository';
import { LogContextEnum } from '../enum';
import { Logger } from '../services';

@Injectable()
export class LoggerFactory {
  constructor(
    private appLoggerService: LogRepository,
    private consoleLogger: ConsoleLogger,
  ) {}

  get(context: LogContextEnum) {
    return new Logger(this.appLoggerService, this.consoleLogger, context);
  }
}
