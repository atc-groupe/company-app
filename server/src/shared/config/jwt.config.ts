import { readFileSync } from 'fs';
import { Injectable } from '@nestjs/common';
import { JwtModuleOptions } from '@nestjs/jwt';

@Injectable()
export class JwtConfig {
  createJwtOptions(): JwtModuleOptions {
    return {
      global: true,
      publicKey: readFileSync('./rsa/key.pub'),
      privateKey: readFileSync('./rsa/key'),
      signOptions: { expiresIn: '1d', algorithm: 'RS256' },
    };
  }
}
