import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { IActiveDirectoryConfigOptions } from '../interfaces';

@Injectable()
export class ActiveDirectoryConfig {
  private readonly adUrl;
  private readonly adBaseDn;
  private readonly adUsername;
  private readonly adPassword;
  private readonly adUsersGroup;
  private readonly adAdminsGroup;

  constructor(private _configService: ConfigService) {
    this.adUrl = this._configService.getOrThrow('AD_URL');
    this.adBaseDn = this._configService.getOrThrow('AD_BASE_DN');
    this.adUsername = this._configService.getOrThrow('AD_USERNAME');
    this.adPassword = this._configService.getOrThrow('AD_PASSWORD');
    this.adUsersGroup = this._configService.getOrThrow('AD_USERS_GROUP');
    this.adAdminsGroup = this._configService.getOrThrow('AD_ADMINS_GROUP');
  }

  getConfigOptions(): IActiveDirectoryConfigOptions {
    return {
      url: this.adUrl,
      baseDN: this.adBaseDn,
      username: this.adUsername,
      password: this.adPassword,
      usersGroup: this.adUsersGroup,
      adminsGroup: this.adAdminsGroup,
      user: [
        'distinguishedName',
        'userPrincipalName',
        'sAMAccountName',
        'mail',
        'lockoutTime',
        'pwdLastSet',
        'userAccountControl',
        'employeeID',
        'sn',
        'givenName',
        'initials',
        'cn',
        'displayName',
        'comment',
        'description',
      ],
    };
  }
}
