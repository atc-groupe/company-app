import { IEventListener } from '../interfaces';

export abstract class AbstractEventDispatcher {
  public _listeners: IEventListener[] = [];

  public dispatchEvent(name: string, data: any): void {
    this._listeners.forEach((listener) => {
      listener.on(name, data);
    });
  }

  public addListener(listener: IEventListener): void {
    this._listeners.push(listener);
  }

  public removeListener(listener: IEventListener): void {
    this._listeners = this._listeners.filter(
      (item) => item.constructor.name !== listener.constructor.name,
    );
  }
}
