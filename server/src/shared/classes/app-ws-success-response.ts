import { AppWsResponse } from './app-ws-response';
import { HttpStatus } from '@nestjs/common';

export class AppWsSuccessResponse<T = any> extends AppWsResponse {
  constructor(data?: T) {
    super(HttpStatus.OK, data);
  }
}
