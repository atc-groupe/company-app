import { HttpStatus } from '@nestjs/common';

export class AppWsResponse<T = any> {
  constructor(
    public readonly status: HttpStatus,
    public readonly data?: T,
  ) {}
}
