import { Injectable } from '@nestjs/common';
import { catchError, map, Observable, ObservableInput } from 'rxjs';
import { AxiosRequestConfig } from 'axios';
import {
  MpApiErrorException,
  MpApiException,
  MpHttpAdapterException,
} from '../exceptions';
import { HttpService } from '@nestjs/axios';

@Injectable()
export class MpHttpAdapter {
  constructor(private _http: HttpService) {}

  public get<T>(url: string, config?: AxiosRequestConfig): Observable<T> {
    return this._http.get(url, config).pipe(
      map(({ data }): T => {
        if (data.errornumber) {
          throw new MpApiErrorException(data.errornumber, data.errortext);
        }

        return data;
      }),
      catchError((err) => this._handleError(err)),
    );
  }

  public post<T>(
    url: string,
    data?: any,
    config?: AxiosRequestConfig,
  ): Observable<T> {
    return this._http.post(url, data, config).pipe(
      map(({ data }): T => {
        if (data.errornumber) {
          throw new MpApiErrorException(data.errornumber, data.errortext);
        }

        return data;
      }),
      catchError((err) => this._handleError(err)),
    );
  }

  public put<T>(
    url: string,
    data?: any,
    config?: AxiosRequestConfig,
  ): Observable<T> {
    return this._http.put(url, data, config).pipe(
      map(({ data }): T => {
        if (data.errornumber) {
          throw new MpApiErrorException(data.errornumber, data.errortext);
        }

        return data;
      }),
      catchError((err) => this._handleError(err)),
    );
  }

  public delete<T>(url: string, config?: AxiosRequestConfig): Observable<T> {
    return this._http.delete(url, config).pipe(
      map(({ data }): T => {
        if (data.errornumber) {
          throw new MpApiErrorException(data.errornumber, data.errortext);
        }

        return data;
      }),
      catchError((err) => this._handleError(err)),
    );
  }

  private _handleError(err: any): ObservableInput<any> {
    if (err instanceof MpApiErrorException) {
      throw err;
    }

    if (err.code === 'ENETUNREACH') {
      throw new MpApiException(
        "La connexion au serveur MultiPress est impossible. Veuillez contacter l'administrateur",
      );
    }

    throw new MpHttpAdapterException(err);
  }
}
