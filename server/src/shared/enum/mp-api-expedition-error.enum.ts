export enum MpApiExpeditionErrorEnum {
  EXPEDITION_LINE_NOT_FOUND = 1000,
  EXPEDITION_LINE_IN_USE = 1001,
  NOT_POSSIBLE_TO_CHANGE_THIS_DATA = 1002,
  INVALID_AND_OR_INSUFFICIENT_DATA = 1003,
  EXPEDITION_LINE_ALREADY_PLANNED = 1004,
}
