export enum SseEventResourceEnum {
  AppSettings = 'AppSettings',
  Job = 'Job',
  Log = 'Log',
  Role = 'Role',
  User = 'User',
  Users = 'Users',
}
