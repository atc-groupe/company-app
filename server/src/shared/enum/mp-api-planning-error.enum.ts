export enum MpApiPlanningErrorEnum {
  PLANNING_LINE_NOT_FOUND = 900,
  JOB_NOT_FOUND = 901,
  JOB_IN_USE = 902,
}
