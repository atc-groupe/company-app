export enum WsEventActionEnum {
  CREATE = 'create',
  UPDATE = 'update',
  DELETE = 'delete',
}
