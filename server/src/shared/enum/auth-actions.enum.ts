export enum AuthActionsEnum {
  Create = 'create',
  Update = 'update',
  Delete = 'delete',
  Manage = 'manage',
}
