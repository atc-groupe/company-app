export enum SseEventActionEnum {
  CREATE = 'create',
  UPDATE = 'update',
  DELETE = 'delete',
}
