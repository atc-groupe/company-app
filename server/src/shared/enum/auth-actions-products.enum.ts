export enum AuthActionsProductsEnum {
  DecreaseStock = 'decreaseStock',
  UpdateStock = 'updateStock',
  ManageStock = 'manageStock',
  UpdateStockSettings = 'updateStockSettings',
  ManageDocuments = 'manageDocuments',
  SyncAllProducts = 'syncAllProducts',
}
