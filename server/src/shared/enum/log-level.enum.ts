export enum LogLevelEnum {
  log = 'log',
  warn = 'warn',
  error = 'error',
}
