import { Global, Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule } from '@nestjs/config';
import { JwtConfig } from '../config';
import { APP_GUARD } from '@nestjs/core';
import { UsersModule } from '../../resources/users/users.module';
import { AuthGuard } from '../../resources/auth/auth.guard';
import { AuthorizationsGuard } from '../../resources/auth/authorizations.guard';

@Global()
@Module({
  imports: [
    UsersModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useClass: JwtConfig,
    }),
  ],
  providers: [
    {
      provide: APP_GUARD,
      useClass: AuthGuard,
    },
    {
      provide: APP_GUARD,
      useClass: AuthorizationsGuard,
    },
  ],
  exports: [JwtModule],
})
export class SecurityConfigModule {}
