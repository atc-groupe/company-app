import { Module } from '@nestjs/common';
import {
  JobEventDispatcher,
  RolesEventDispatcher,
  SettingsEventDispatcher,
} from '../event-dispatchers';
import { UsersEventDispatcher } from '../event-dispatchers/users.event-dispatcher';

const DISPATCHERS = [
  JobEventDispatcher,
  RolesEventDispatcher,
  SettingsEventDispatcher,
  UsersEventDispatcher,
];

@Module({
  providers: DISPATCHERS,
  exports: DISPATCHERS,
})
export class EventDispatchersModule {}
