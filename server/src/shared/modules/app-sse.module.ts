import { Module } from '@nestjs/common';
import { SseService } from '../services';

@Module({
  providers: [SseService],
  exports: [SseService],
})
export class AppSseModule {}
