import { Module } from '@nestjs/common';
import { DataSorter, DateHelper } from '../services';

const SERVICES = [DataSorter, DateHelper];

@Module({
  providers: [...SERVICES],
  exports: [...SERVICES],
})
export class HelperModule {}
