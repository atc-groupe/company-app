import { ConsoleLogger, Global, Module } from '@nestjs/common';
import { Logger } from '../services';
import { LoggerFactory } from '../factories/logger.factory';
import { LogsModule } from '../../resources/logs/logs.module';

@Global()
@Module({
  imports: [LogsModule],
  providers: [LoggerFactory, ConsoleLogger, Logger],
  exports: [LoggerFactory],
})
export class LoggerModule {}
