import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { CoreModule } from './core.module';
import { MpHttpConfig } from '../config';
import { MpHttpAdapter } from '../adapters';

@Module({
  imports: [HttpModule.registerAsync({ useClass: MpHttpConfig }), CoreModule],
  providers: [MpHttpAdapter],
  exports: [HttpModule, MpHttpAdapter],
})
export class MpHttpModule {}
