import { HttpStatus } from '@nestjs/common';
import { AppWsException } from './app-ws.exception';

export class AppWsNotFoundErrorException extends AppWsException {
  constructor(message: string) {
    super(HttpStatus.NOT_FOUND, message);
  }
}
