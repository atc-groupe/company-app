import { HttpException } from '@nestjs/common';

export class DataMappingErrorException extends HttpException {
  constructor(message: string, code?: number) {
    super(message, code ? code : 500);
  }
}
