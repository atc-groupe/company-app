import { HttpException, HttpStatus } from '@nestjs/common';

export class MpHttpAdapterException extends HttpException {
  constructor(err: any) {
    super('MultiPress API Http Exception', HttpStatus.INTERNAL_SERVER_ERROR, {
      cause: err,
    });
  }
}
