import { HttpStatus } from '@nestjs/common';
import { WsException } from '@nestjs/websockets';

export class AppWsException extends WsException {
  constructor(
    public readonly status: HttpStatus,
    message: string,
  ) {
    super(message);
  }
}
