import { HttpException } from '@nestjs/common';

export class SubJobLockedErrorException extends HttpException {
  constructor(message: string, code?: number) {
    super(message, code ? code : 500);
  }
}
