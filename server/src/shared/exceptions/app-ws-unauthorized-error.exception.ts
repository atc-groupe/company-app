import { HttpStatus } from '@nestjs/common';
import { AppWsException } from './app-ws.exception';

export class AppWsUnauthorizedErrorException extends AppWsException {
  constructor(message: string) {
    super(HttpStatus.UNAUTHORIZED, message);
  }
}
