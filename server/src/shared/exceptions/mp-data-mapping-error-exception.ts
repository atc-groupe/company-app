import { HttpException, HttpStatus } from '@nestjs/common';

export class MpDataMappingErrorException extends HttpException {
  constructor(message: string) {
    super(message, HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
