import { HttpException } from '@nestjs/common';

export class JobArchivedErrorException extends HttpException {
  constructor(message: string) {
    super(message, 500);
  }
}
