import { AppWsException } from './app-ws.exception';
import { HttpStatus } from '@nestjs/common';

export class AppWsInternalServerErrorException extends AppWsException {
  constructor(message: string) {
    super(HttpStatus.INTERNAL_SERVER_ERROR, message);
  }
}
