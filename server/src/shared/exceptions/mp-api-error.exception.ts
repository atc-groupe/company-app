import { HttpException, HttpStatus } from '@nestjs/common';

export class MpApiErrorException extends HttpException {
  constructor(
    public errNumber: number,
    public errText: string,
  ) {
    super('MultiPress API error', HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
