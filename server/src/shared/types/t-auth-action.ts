import {
  AppModuleEnum,
  AuthActionsEnum,
  AuthActionsJobsEnum,
  AuthActionsProductsEnum,
  AuthActionsSettingsEnum,
} from '../enum';

export type TAuthAction =
  | AuthActionsEnum
  | AuthActionsJobsEnum
  | AuthActionsProductsEnum
  | AuthActionsSettingsEnum
  | AppModuleEnum;
