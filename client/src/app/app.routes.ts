import { Routes } from '@angular/router';
import { SigninComponent } from './views/signin/signin.component';
import { userDataGuard } from './shared/guards/user-data.guard';
import { authGuard } from './shared/guards/auth.guard';
import { signInGuard } from './shared/guards/sign-in.guard';
import { CoreComponent } from './views/core/core.component';
import { AppModuleEnum } from './shared/enums';
import { FeatureNavGuard } from './shared/guards/feature-nav.guard';

export const routes: Routes = [
  {
    path: 'sign-in',
    component: SigninComponent,
    canActivate: [userDataGuard, signInGuard],
  },
  {
    path: '',
    component: CoreComponent,
    canActivate: [userDataGuard, authGuard],
    canActivateChild: [FeatureNavGuard],
    children: [
      {
        path: '',
        redirectTo: AppModuleEnum.Dashboard,
        pathMatch: 'full',
      },
      {
        path: AppModuleEnum.Account,
        loadChildren: () => import('./features/account/account.routes'),
      },
      {
        path: AppModuleEnum.Admin,
        loadChildren: () => import('./features/admin/admin.routes'),
      },
      {
        path: AppModuleEnum.Dashboard,
        loadChildren: () => import('./features/dashboard/dashboard.routes'),
      },
      {
        path: AppModuleEnum.Jobs,
        loadChildren: () => import('./features/jobs/jobs.routes'),
      },
      {
        path: AppModuleEnum.Expeditions,
        loadChildren: () => import('./features/expeditions/expeditions.routes'),
      },
      {
        path: AppModuleEnum.Planning,
        loadChildren: () => import('./features/planning/planning.routes'),
      },
      {
        path: AppModuleEnum.Stock,
        loadChildren: () => import('./features/stock/stock.routes'),
      },
    ],
  },
];
