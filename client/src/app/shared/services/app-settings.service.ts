import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IAppSettings } from '../interfaces/app-settings/i-app-settings';

@Injectable({ providedIn: 'root' })
export class AppSettingsService {
  constructor(private _http: HttpClient) {}

  getAppSettings(): Observable<IAppSettings> {
    return this._http.get<IAppSettings>('/company-api/app-settings');
  }
}
