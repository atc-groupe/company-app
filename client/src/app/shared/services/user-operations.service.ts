import { Injectable } from '@angular/core';
import { User } from '../entities';
import { IJob, IJobOperation, IJobWorksheet } from '../interfaces';
import { AuthActionsJobsEnum, AuthSubjectsEnum } from '../enums';

@Injectable({ providedIn: 'root' })
export class UserOperationsService {
  public getUserOperations(job: IJob, user: User): IJobOperation[] | null {
    const operations = job.operations?.filter((operation) =>
      user.mp.operations.some((op) => op.label === operation.operation),
    );

    return operations?.length ? operations : null;
  }

  public canCreateWorksheets(user: User): boolean {
    const canCreate = user.canActivate({
      subject: AuthSubjectsEnum.Jobs,
      action: AuthActionsJobsEnum.CreateWorksheet,
    });

    return canCreate || this.canManageWorksheets(user);
  }

  public canManageWorksheets(user: User): boolean {
    return user.canActivate({
      subject: AuthSubjectsEnum.Jobs,
      action: AuthActionsJobsEnum.ManageWorksheets,
    });
  }

  public canAddWorksheet(
    operation: string,
    worksheets: IJobWorksheet[],
    user: User,
  ): boolean {
    if (!this.canCreateWorksheets(user)) {
      return false;
    }

    if (user.mp.operations.find((el) => el.label === operation) === undefined) {
      return false;
    }

    return worksheets
      .filter((ws) => ws.operation === operation)
      .every((ws) => !ws.operationReady && !!ws.totalTime);
  }

  public canUpdateWorksheet(
    user: User,
    worksheet: IJobWorksheet,
    worksheets: IJobWorksheet[],
  ): boolean {
    if (user.isAdmin) {
      return true;
    }

    if (this.canManageWorksheets(user)) {
      return true;
    }

    const canUpdate = user.canActivate({
      subject: AuthSubjectsEnum.Jobs,
      action: AuthActionsJobsEnum.UpdateMyWorksheets,
    });

    const operationWorksheets = worksheets.filter(
      (item) => item.operation === worksheet.operation,
    );

    if (!operationWorksheets.length) {
      return false;
    }

    const isLastWorksheet =
      operationWorksheets[operationWorksheets.length - 1].id === worksheet.id;

    return (
      canUpdate &&
      worksheet.employeeNumber === user.mp.employeeNumber &&
      isLastWorksheet
    );
  }

  public canSetWorksheetOperationReady(
    user: User,
    worksheet: IJobWorksheet,
    worksheets: IJobWorksheet[],
  ): boolean {
    if (this.isOperationReady(worksheet.operation, worksheets)) {
      return false;
    }

    return this.canUpdateWorksheet(user, worksheet, worksheets);
  }

  public canRemoveWorksheet(
    user: User,
    worksheet: IJobWorksheet,
    worksheets: IJobWorksheet[],
  ): boolean {
    if (user.isAdmin) {
      return true;
    }

    if (this.canManageWorksheets(user) && !worksheet.operationReady) {
      return true;
    }

    const canRemove = user.canActivate({
      subject: AuthSubjectsEnum.Jobs,
      action: AuthActionsJobsEnum.DeleteMyWorksheets,
    });

    const operationWorksheets = worksheets.filter(
      (item) => item.operation === worksheet.operation,
    );

    if (!operationWorksheets.length) {
      return false;
    }

    const isLastWorksheet =
      operationWorksheets[operationWorksheets.length - 1].id === worksheet.id;

    return (
      canRemove &&
      worksheet.employeeNumber === user.mp.employeeNumber &&
      isLastWorksheet &&
      !worksheet.operationReady
    );
  }

  public isActiveWorksheet(worksheet: IJobWorksheet): boolean {
    return !worksheet.totalTime;
  }

  public getActiveWorksheet(
    operation: string,
    worksheets: IJobWorksheet[],
  ): IJobWorksheet | null {
    const worksheet = worksheets.find(
      (ws) => ws.operation === operation && !ws.totalTime,
    );

    return worksheet ? worksheet : null;
  }

  public isOperationReady(
    operation: string,
    worksheets: IJobWorksheet[],
  ): boolean {
    return worksheets.some(
      (ws) => ws.operation === operation && ws.operationReady,
    );
  }
}
