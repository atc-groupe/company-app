import { Injectable } from '@angular/core';
import { AbstractWsService } from './abstract-ws-service';
import { map, Observable } from 'rxjs';
import { User } from '../entities';
import { IUser } from '../interfaces';

@Injectable({ providedIn: 'root' })
export class WsMeService extends AbstractWsService {
  constructor() {
    super('me');
  }

  public onUpdate(): Observable<User> {
    return this.on<IUser>('update').pipe(map((userData) => new User(userData)));
  }
}
