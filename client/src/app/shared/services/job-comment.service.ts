import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IJob } from '../interfaces';
import {
  IJobCommentPlanningCreateDto,
  IJobCommentPlanningUpdateDto,
} from '../dto';

@Injectable({ providedIn: 'root' })
export class JobCommentService {
  constructor(private _http: HttpClient) {}

  public updateDeliveryComment(
    id: string,
    comment: string | null,
  ): Observable<IJob> {
    return this._http.patch<IJob>(`/company-api/jobs/${id}/comments/delivery`, {
      comment,
    });
  }

  public addPlanningComment(
    id: string,
    dto: IJobCommentPlanningCreateDto,
  ): Observable<IJob> {
    return this._http.post<IJob>(
      `/company-api/jobs/${id}/comments/planning`,
      dto,
    );
  }

  public updatePlanningComment(
    id: string,
    commentId: string,
    dto: IJobCommentPlanningUpdateDto,
  ): Observable<IJob> {
    return this._http.patch<IJob>(
      `/company-api/jobs/${id}/comments/planning/${commentId}`,
      dto,
    );
  }

  public removePlanningComment(
    id: string,
    commentId: string,
  ): Observable<IJob> {
    return this._http.delete<IJob>(
      `/company-api/jobs/${id}/comments/planning/${commentId}`,
    );
  }
}
