import { Injectable } from '@angular/core';
import { IJobWorksheet } from '../interfaces';

@Injectable({ providedIn: 'root' })
export class WorksheetTimeHelperService {
  public getTimeFromTotalTime(totalTime: number): {
    hours: number;
    minutes: number;
  } {
    const hours = Math.floor(totalTime);
    const minutes = Math.round((totalTime - hours) * 60);

    return {
      hours,
      minutes,
    };
  }

  public getStartTime(): string {
    const now = new Date().toTimeString().slice(0, 8);

    return `0000-00-00T${now}`;
  }

  public getCalculatedTime(worksheet: IJobWorksheet): {
    hours: number;
    minutes: number;
  } {
    const now = new Date();
    if (this.isNotToday(worksheet)) {
      return { hours: 8, minutes: 0 };
    }

    const diffTime =
      this._getTime(now) - this._getTime(new Date(worksheet.startTime));

    return {
      hours: Math.floor(diffTime / 60),
      minutes: Math.round(diffTime % 60),
    };
  }

  public isNotToday(worksheet: IJobWorksheet): boolean {
    return worksheet.date !== new Date().toLocaleDateString('en-US');
  }

  private _getTime(date: Date): number {
    const timeString = date.toTimeString();
    const offset = date.getTimezoneOffset();
    const hours = parseInt(timeString.slice(0, 2));
    const minutes = parseInt(timeString.slice(3, 5));

    return hours * 60 + minutes - offset;
  }
}
