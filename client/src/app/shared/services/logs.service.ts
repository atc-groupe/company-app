import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ILog } from '../interfaces';
import { ILogsQueryParamsDto } from '../dto';

@Injectable({ providedIn: 'root' })
export class LogsService {
  constructor(private _http: HttpClient) {}

  public find(query: ILogsQueryParamsDto): Observable<ILog[]> {
    return this._http.get<ILog[]>('/company-api/logs', {
      params: { ...query },
    });
  }
}
