import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IEmployeeListItem, IEmployeeOperation } from '../interfaces';

@Injectable({ providedIn: 'root' })
export class EmployeesService {
  constructor(private _http: HttpClient) {}

  public findAll(): Observable<IEmployeeListItem[]> {
    return this._http.get<IEmployeeListItem[]>('/company-api/employees');
  }

  public findNotRegistered(): Observable<IEmployeeListItem[]> {
    return this._http.get<IEmployeeListItem[]>(
      '/company-api/employees/not-registered',
    );
  }

  public getOperations(): Observable<IEmployeeOperation[]> {
    return this._http.get<IEmployeeOperation[]>(
      '/company-api/employees/operations',
    );
  }
}
