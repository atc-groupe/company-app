import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { IUser } from '../interfaces';
import {
  IUserCreateDto,
  IUserUpdateDto,
  IUsersFindAllQueryParamsDto,
} from '../dto';
import { User } from '../entities';

@Injectable({ providedIn: 'root' })
export class UsersService {
  constructor(private _http: HttpClient) {}

  public findAll(query?: IUsersFindAllQueryParamsDto): Observable<User[]> {
    return this._http
      .get<IUser[]>('/company-api/users', { params: { ...query } })
      .pipe(map((users) => users.map((user) => new User(user))));
  }

  public findOne(id: string): Observable<User> {
    return this._http
      .get<IUser>(`/company-api/users/${id}`)
      .pipe(map((user) => new User(user)));
  }

  public createOne(dto: IUserCreateDto): Observable<User> {
    return this._http
      .post<IUser>('/company-api/users', dto)
      .pipe(map((user) => new User(user)));
  }

  public updateOne(id: string, dto: IUserUpdateDto): Observable<User> {
    return this._http
      .patch<IUser>(`/company-api/users/${id}`, dto)
      .pipe(map((user) => new User(user)));
  }

  public deleteOne(id: string): Observable<void> {
    return this._http.delete<void>(`/company-api/users/${id}`);
  }

  public addRole(id: string, roleId: string): Observable<void> {
    return this._http.post<void>(
      `/company-api/users/${id}/roles/${roleId}`,
      {},
    );
  }

  public removeRole(id: string, roleId: string): Observable<User> {
    return this._http
      .delete<User>(`/company-api/users/${id}/roles/${roleId}`)
      .pipe(map((user) => new User(user)));
  }
}
