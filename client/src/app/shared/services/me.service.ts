import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { User } from '../entities';
import { IUser } from '../interfaces';
import { IUserSettingsUpdateDto } from '../dto';

@Injectable({ providedIn: 'root' })
export class MeService {
  constructor(private _http: HttpClient) {}

  public fetchAuthenticatedUser(): Observable<User> {
    return this._http
      .get<IUser>('/company-api/me')
      .pipe(map((user) => new User(user)));
  }

  public updateSettings(dto: IUserSettingsUpdateDto): Observable<User> {
    return this._http
      .patch<IUser>(`/company-api/me/settings`, dto)
      .pipe(map((user) => new User(user)));
  }
}
