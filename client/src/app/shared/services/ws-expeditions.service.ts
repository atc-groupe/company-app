import { Injectable } from '@angular/core';
import { AbstractWsService } from './abstract-ws-service';
import { Observable } from 'rxjs';
import { IExpeditionsQueryParamsDto } from '../dto';
import { IExpeditionLine } from '../interfaces';

@Injectable({ providedIn: 'root' })
export class WsExpeditionsService extends AbstractWsService {
  constructor() {
    super('expeditions');
  }

  public onSubscribeData(
    dto: IExpeditionsQueryParamsDto,
  ): Observable<IExpeditionLine[]> {
    return this.emit<IExpeditionLine[]>('data.subscribe', dto);
  }

  public onDataUpdate(): Observable<IExpeditionLine> {
    return this.on<IExpeditionLine>('data.update');
  }
}
