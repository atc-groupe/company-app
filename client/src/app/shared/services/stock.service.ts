import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { ProductTypeEnum } from '../enums';
import { IMpStockMutation, IPaperData, IProduct } from '../interfaces';
import { IArticleStockInfoDto, IStockMutationDto } from '../dto';
import { IProductGenericName } from '../interfaces/stock/i-product-generic-name';
import { TProductData, TProductGroupType } from '../types';
import { ProductFactory } from '../factories';
import { Paper } from '../entities';

@Injectable({ providedIn: 'root' })
export class StockService {
  constructor(
    private _http: HttpClient,
    private _productFactory: ProductFactory,
  ) {}

  public syncAll(): Observable<void> {
    return this._http.get<void>(`/company-api/stock/sync`);
  }

  public syncOne(productType: ProductTypeEnum, id: number): Observable<void> {
    return this._http.get<void>(`/company-api/stock/${productType}/${id}/sync`);
  }

  public updateArticleStockInfo(
    id: number,
    dto: IArticleStockInfoDto,
  ): Observable<void> {
    return this._http.patch<void>(
      `/company-api/stock/articles/${id}/stock-info`,
      dto,
    );
  }

  public updateGenericName(
    productType: ProductTypeEnum,
    productId: number,
    genericNameId: string | null,
  ): Observable<void> {
    return this._http.patch<void>(
      `/company-api/stock/${productType}/${productId}/generic-name`,
      { id: genericNameId },
    );
  }

  public getProductMutations(
    productType: ProductTypeEnum,
    productId: number,
    startDate?: Date,
    endDate?: Date,
  ): Observable<IMpStockMutation[]> {
    if (!startDate) {
      startDate = new Date();
      startDate.setFullYear(startDate.getFullYear() - 1);
    }

    const params: any = {
      'start-date': startDate.toLocaleDateString('en-US'),
    };

    if (endDate) {
      params.endDate = endDate.toLocaleDateString('en-US');
    }

    return this._http.get<IMpStockMutation[]>(
      `/company-api/stock/${productType}/${productId}/mutations`,
      { params },
    );
  }

  public createMutation(
    productType: ProductTypeEnum,
    productId: number,
    dto: IStockMutationDto,
  ): Observable<IMpStockMutation> {
    return this._http.post<IMpStockMutation>(
      `/company-api/stock/${productType}/${productId}/mutations`,
      dto,
    );
  }

  public getSheetsGenericNames(): Observable<IProductGenericName[]> {
    return this._http.get<IProductGenericName[]>(
      '/company-api/stock/sheets/generic-names',
    );
  }

  public getProductsList(
    productType: ProductTypeEnum,
    groupType: number,
  ): Observable<IProduct[]> {
    return this._http
      .get<TProductData[]>(`/company-api/stock/${productType}/${groupType}`)
      .pipe(
        map((productsData: TProductData[]) => {
          return productsData.map((productData) =>
            this._productFactory.getProduct(productType, productData),
          );
        }),
      );
  }

  public getPapersByName(id: number): Observable<Paper[]> {
    return this._http.get<IPaperData[]>(`/company-api/stock/papers/${id}`).pipe(
      map((data) => {
        return data.map((paperData) => new Paper(paperData));
      }),
    );
  }
}
