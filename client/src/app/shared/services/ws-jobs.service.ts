import { Injectable } from '@angular/core';
import { AbstractWsService } from './abstract-ws-service';
import { IJobsQueryParamsDto } from '../dto';
import { Observable } from 'rxjs';
import { IJobLine } from '../interfaces/job/i-job-line';
import { IJob } from '../interfaces';

@Injectable({ providedIn: 'root' })
export class WsJobsService extends AbstractWsService {
  constructor() {
    super('jobs');
  }

  public onSubscribeList(dto: IJobsQueryParamsDto): Observable<IJobLine[]> {
    return this.emit<IJobLine[]>('list.subscribe', dto);
  }

  public onListItemUpdate(): Observable<IJobLine> {
    return this.on<IJobLine>('list.update');
  }

  public onSubscribeJob(mpNumber: number): Observable<IJob> {
    return this.emit<IJob>('job.subscribe', { mpNumber });
  }

  public onJobUpdate(): Observable<IJob> {
    return this.on<IJob>('job.update');
  }
}
