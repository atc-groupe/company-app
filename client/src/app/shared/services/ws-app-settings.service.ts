import { Injectable } from '@angular/core';
import { AbstractWsService } from './abstract-ws-service';
import { Observable } from 'rxjs';
import { IAppSettings } from '../interfaces/app-settings/i-app-settings';

@Injectable({ providedIn: 'root' })
export class WsAppSettingsService extends AbstractWsService {
  constructor() {
    super('app-settings');
  }

  onUpdate(): Observable<IAppSettings> {
    return this.on<IAppSettings>('update');
  }
}
