import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IAppSettingsDevicesMappingName } from '../interfaces';
import { Observable } from 'rxjs';
import { IAppSettings } from '../interfaces/app-settings/i-app-settings';

@Injectable({ providedIn: 'root' })
export class AppSettingsDevicesService {
  constructor(private _http: HttpClient) {}

  public addMappingName(
    dto: IAppSettingsDevicesMappingName,
  ): Observable<IAppSettings> {
    return this._http.post<IAppSettings>(
      '/company-api/app-settings/devices/mapping/names',
      dto,
    );
  }

  public removeMappingName(mpName: string): Observable<IAppSettings> {
    return this._http.delete<IAppSettings>(
      `/company-api/app-settings/devices/mapping/names/${mpName}`,
    );
  }
}
