import { Injectable } from '@angular/core';
import { AbstractWsService } from './abstract-ws-service';
import { Observable } from 'rxjs';
import { IJobStockReservationLine } from '../interfaces';

@Injectable({ providedIn: 'root' })
export class WSJobsStockReservationsService extends AbstractWsService {
  constructor() {
    super('job.stock-reservations');
  }

  public onSubscribeList(): Observable<IJobStockReservationLine[]> {
    return this.emit<IJobStockReservationLine[]>('list.subscribe');
  }

  public onListItemUpdate(): Observable<IJobStockReservationLine> {
    return this.on<IJobStockReservationLine>('item.update');
  }
}
