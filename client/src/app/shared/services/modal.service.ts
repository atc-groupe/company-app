import { Injectable, Type } from '@angular/core';
import { BehaviorSubject, first, Observable, Subject } from 'rxjs';
import { IModal, IModalOptions, IAlertOptions } from '../interfaces';

@Injectable({ providedIn: 'root' })
export class ModalService {
  private _modal$ = new BehaviorSubject<IModal | null>(null);
  private _afterClosed$ = new Subject<any>();

  public open(
    component: Type<any>,
    options?: { inputs?: Record<string, unknown>; options?: IModalOptions },
  ): { afterClosed$: Observable<any> } {
    this._modal$.next({
      component,
      inputs: options?.inputs,
      options: options?.options,
    });

    return { afterClosed$: this.afterClosed$ };
  }

  public close(data?: any): void {
    this._modal$.next(null);

    this._afterClosed$.next(data);
  }

  get modal$(): Observable<IModal | null> {
    return this._modal$;
  }

  get afterClosed$(): Observable<any> {
    return this._afterClosed$.pipe(first());
  }
}
