import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  ISettingsJobStatusChangeRuleDto,
  ISettingsJobStatusChangeRuleOperationDto,
  ISettingsJobStatusChangeRuleStatusDto,
} from '../dto';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class AppSettingsAutomationService {
  constructor(private _http: HttpClient) {}

  public addJobStatusChangeRule(
    dto: ISettingsJobStatusChangeRuleDto,
  ): Observable<void> {
    return this._http.post<void>(
      `/company-api/app-settings/automation/job-status-change-rules`,
      dto,
    );
  }

  public updateJobStatusChangeRule(
    id: string,
    dto: ISettingsJobStatusChangeRuleDto,
  ): Observable<void> {
    return this._http.put<void>(
      `/company-api/app-settings/automation/job-status-change-rules/${id}`,
      dto,
    );
  }

  public removeJobStatusChangeRule(id: string): Observable<void> {
    return this._http.delete<void>(
      `/company-api/app-settings/automation/job-status-change-rules/${id}`,
    );
  }

  public addJobStatusChangeRuleStatus(
    ruleId: string,
    dto: ISettingsJobStatusChangeRuleStatusDto,
  ): Observable<void> {
    return this._http.post<void>(
      `/company-api/app-settings/automation/job-status-change-rules/${ruleId}/to-statuses`,
      dto,
    );
  }

  public removeJobStatusChangeRuleStatus(
    ruleId: string,
    statusId: string,
  ): Observable<void> {
    return this._http.delete<void>(
      `/company-api/app-settings/automation/job-status-change-rules/${ruleId}/to-statuses/${statusId}`,
    );
  }

  public addJobStatusChangeRuleOperation(
    ruleId: string,
    dto: ISettingsJobStatusChangeRuleOperationDto,
  ): Observable<void> {
    return this._http.post<void>(
      `/company-api/app-settings/automation/job-status-change-rules/${ruleId}/operations`,
      dto,
    );
  }

  public removeJobStatusChangeRuleOperation(
    ruleId: string,
    operationId: string,
  ): Observable<void> {
    return this._http.delete<void>(
      `/company-api/app-settings/automation/job-status-change-rules/${ruleId}/operations/${operationId}`,
    );
  }
}
