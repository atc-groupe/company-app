import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class PlanningDateHelperService {
  public getLastWeekDay(date: Date): Date {
    const newDate = new Date(date);
    let inc = 0;
    if (newDate.getDay() === 0) {
      inc = -2;
    } else if (newDate.getDay() === 6) {
      inc = -1;
    } else {
      inc = 5 - newDate.getDay();
    }

    newDate.setDate(newDate.getDate() + inc);

    return newDate;
  }

  public getFirstWeekDay(date: Date): Date {
    const newDate = new Date(date);
    let inc = 0;
    if (newDate.getDay() === 0) {
      inc = 6;
    } else {
      inc = newDate.getDay() - 1;
    }

    newDate.setDate(newDate.getDate() - inc);

    return newDate;
  }
}
