import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  IJobStockReservationDto,
  IJobStockReservationHandleDto,
  IJobStockReservationStatusDto,
} from '../dto';
import { Observable } from 'rxjs';
import { JobStockReservationStatusEnum } from '../enums';

@Injectable({ providedIn: 'root' })
export class JobStockReservationsService {
  constructor(private _http: HttpClient) {}

  insertOne(jobNumber: number, dto: IJobStockReservationDto): Observable<void> {
    return this._http.post<void>(
      `/company-api/jobs/${jobNumber}/stock-reservations`,
      dto,
    );
  }

  updateOne(
    jobNumber: number,
    reservationId: string,
    dto: IJobStockReservationDto,
  ): Observable<void> {
    return this._http.put<void>(
      `/company-api/jobs/${jobNumber}/stock-reservations/${reservationId}`,
      dto,
    );
  }

  updateStatus(
    jobNumber: number,
    reservationId: string,
    status: JobStockReservationStatusEnum,
  ): Observable<void> {
    return this._http.patch<void>(
      `/company-api/jobs/${jobNumber}/stock-reservations/${reservationId}/status`,
      { status },
    );
  }

  handle(
    jobNumber: number,
    reservationId: string,
    dto: IJobStockReservationHandleDto,
  ): Observable<void> {
    return this._http.patch<void>(
      `/company-api/jobs/${jobNumber}/stock-reservations/${reservationId}/handle`,
      dto,
    );
  }

  cancel(
    jobNumber: number,
    reservationId: string,
    dto: IJobStockReservationHandleDto,
  ): Observable<void> {
    return this._http.patch<void>(
      `/company-api/jobs/${jobNumber}/stock-reservations/${reservationId}/cancel`,
      dto,
    );
  }

  updateAllStatus(
    jobNumber: number,
    dto: IJobStockReservationStatusDto,
  ): Observable<void> {
    return this._http.patch<void>(
      `/company-api/jobs/${jobNumber}/stock-reservations/status`,
      dto,
    );
  }

  removeOne(jobNumber: number, reservationId: string): Observable<void> {
    return this._http.delete<void>(
      `/company-api/jobs/${jobNumber}/stock-reservations/${reservationId}`,
    );
  }

  removeAll(jobNumber: number): Observable<void> {
    return this._http.delete<void>(
      `/company-api/jobs/${jobNumber}/stock-reservations`,
    );
  }
}
