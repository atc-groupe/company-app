import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { IAppSettingsJobStatusChangeRule, IJob } from '../interfaces';
import { selectAppSettings } from '../store/app-settings/app-settings.selectors';
import { JobStatusChangeRuleTypeEnum } from '../enums';

@Injectable({ providedIn: 'root' })
export class JobStatusAutomationRulesService {
  private rules: IAppSettingsJobStatusChangeRule[] | null = null;
  constructor(private _store: Store) {
    this._store.select(selectAppSettings).subscribe((settings) => {
      if (settings) {
        this.rules = settings.automation.jobStatusChangeRules;
      }
    });
  }

  public getOnStartOneRule(
    job: IJob,
    operation: string,
  ): IAppSettingsJobStatusChangeRule | null {
    return this._getRule(
      job,
      operation,
      JobStatusChangeRuleTypeEnum.onStartOne,
    );
  }

  public getOnStartFirstRule(
    job: IJob,
    operation: string,
  ): IAppSettingsJobStatusChangeRule | null {
    const rule = this._getRule(
      job,
      operation,
      JobStatusChangeRuleTypeEnum.onStartFirst,
    );

    if (!rule) {
      return null;
    }

    const hasNoWorksheet = this._hasNoWorksheet(job, rule);

    return hasNoWorksheet ? null : rule;
  }

  public getOnCompleteLastOperationRule(
    job: IJob,
    operation: string,
  ): IAppSettingsJobStatusChangeRule | null {
    const rule = this._getRule(
      job,
      operation,
      JobStatusChangeRuleTypeEnum.onCompleteLast,
    );

    if (!rule) {
      return null;
    }

    const allIsOperationsReady = rule.operations
      .filter(
        (op) =>
          op.operation !== operation &&
          job.operations?.some((jobOp) => jobOp.operation === op.operation),
      )
      .every(
        (op) =>
          job.worksheets?.some(
            (jobWs) => jobWs.operation === op.operation && jobWs.operationReady,
          ),
      );

    return allIsOperationsReady ? rule : null;
  }

  public getOnStopOneRule(
    job: IJob,
    operation: string,
  ): IAppSettingsJobStatusChangeRule | null {
    return this._getRule(job, operation, JobStatusChangeRuleTypeEnum.onStopOne);
  }

  public getOnRemoveUniqueRule(
    job: IJob,
    operation: string,
  ): IAppSettingsJobStatusChangeRule | null {
    const rule = this._getRule(
      job,
      operation,
      JobStatusChangeRuleTypeEnum.onDeleteUnique,
    );

    if (!rule) {
      return null;
    }

    if (!job.operations) {
      return null;
    }

    const worksheetsCount = job.operations
      .filter((jobOp) =>
        rule.operations.some((ruleOp) => ruleOp.operation === jobOp.operation),
      )
      .reduce((acc: number, op) => {
        if (!job.worksheets || !job.worksheets.length) {
          return acc;
        }

        acc += job.worksheets.filter(
          (item) => item.operation === op.operation,
        ).length;

        return acc;
      }, 0);

    if (worksheetsCount > 1) {
      return null;
    }

    return worksheetsCount === 1 ? rule : null;
  }

  private _getRule(
    job: IJob,
    operation: string,
    type: JobStatusChangeRuleTypeEnum,
  ): IAppSettingsJobStatusChangeRule | null {
    if (!this.rules) {
      return null;
    }

    const rule = this.rules.find(
      (rule) =>
        rule.type === type &&
        rule.fromStatusNumber === job.mp.statusNumber &&
        rule.toStatuses.length > 0 &&
        rule.operations.some((op) => op.operation === operation),
    );

    return rule ?? null;
  }

  private _hasNoWorksheet(
    job: IJob,
    rule: IAppSettingsJobStatusChangeRule,
  ): boolean {
    if (!job.worksheets || !job.worksheets.length) {
      return false;
    }

    return rule.operations.every((op) => {
      return !job.worksheets!.filter((ws) => ws.operation === op.operation)
        .length;
    });
  }
}
