import { Injectable } from '@angular/core';
import { NotificationFactory } from '../factories';
import { Store } from '@ngrx/store';
import { displayNotificationAction } from '../store/ui';
import { NotificationTypeEnum } from '../enums';

@Injectable({ providedIn: 'root' })
export class NotificationsService {
  constructor(
    private _factory: NotificationFactory,
    private _store: Store,
  ) {}

  display(type: NotificationTypeEnum, message: string) {
    this._store.dispatch(
      displayNotificationAction({
        notification: this._factory.getNotification(type, message),
      }),
    );
  }

  displayInfo(message: string) {
    this.display(NotificationTypeEnum.Info, message);
  }

  displayWarning(message: string) {
    this.display(NotificationTypeEnum.Warning, message);
  }

  displayError(message: string) {
    this.display(NotificationTypeEnum.Error, message);
  }

  displaySuccess(message: string) {
    this.display(NotificationTypeEnum.Success, message);
  }
}
