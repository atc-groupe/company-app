import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IProductGenericName } from '../interfaces/stock/i-product-generic-name';

@Injectable({ providedIn: 'root' })
export class StockProductGenericNamesService {
  constructor(private _http: HttpClient) {}

  public insertOne(name: string): Observable<IProductGenericName> {
    return this._http.post<IProductGenericName>(
      '/company-api/stock/product-generic-names',
      { name },
    );
  }

  public findAll(): Observable<IProductGenericName[]> {
    return this._http.get<IProductGenericName[]>(
      '/company-api/stock/product-generic-names',
    );
  }

  public updateOne(id: string, name: string): Observable<IProductGenericName> {
    return this._http.patch<IProductGenericName>(
      `/company-api/stock/product-generic-names/${id}`,
      { name },
    );
  }

  public removeOne(id: string): Observable<void> {
    return this._http.delete<void>(
      `/company-api/stock/product-generic-names/${id}`,
    );
  }
}
