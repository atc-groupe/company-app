import { HttpClient, HttpEvent, HttpEventType } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppModuleEnum, ProductTypeEnum } from '../enums';
import { map, Observable } from 'rxjs';
import { IAppFileDto, IAppFileInfoDto } from '../dto';
import { IAppFile } from '../interfaces/i-app-file';

@Injectable({ providedIn: 'root' })
export class StockFilesService {
  constructor(private _http: HttpClient) {}

  public addFile(
    productType: ProductTypeEnum,
    productId: number,
    dto: IAppFileDto,
  ): Observable<number | null> {
    const form = new FormData();
    form.set('name', dto.name);
    form.set('module', AppModuleEnum.Stock);
    form.set('path', productType.toString());
    form.set(
      'file',
      dto.file,
      `file.${this._getExtensionFromFileName(dto.file.name)}`,
    );

    if (dto.description !== undefined) {
      form.set('description', dto.description);
    }

    return this._http
      .post(`/company-api/stock/${productType}/${productId}/files`, form, {
        reportProgress: true,
        observe: 'events',
      })
      .pipe(
        map((event) => {
          if (event.type === HttpEventType.Sent) {
            return 0;
          }

          if (event.type === HttpEventType.UploadProgress) {
            return Math.round((event.loaded / event.total!) * 100);
          }

          if (event.type === HttpEventType.Response && event.ok) {
            return 100;
          }

          return null;
        }),
      );
  }

  public updateFileInfo(
    productType: ProductTypeEnum,
    productId: number,
    fileId: string,
    dto: IAppFileInfoDto,
  ): Observable<void> {
    return this._http.patch<void>(
      `/company-api/stock/${productType}/${productId}/files/${fileId}/info`,
      dto,
    );
  }

  public updateFile(
    productType: ProductTypeEnum,
    productId: number,
    fileId: string,
    file: File,
  ): Observable<number | null> {
    const form = new FormData();
    form.set('file', file, `file.${this._getExtensionFromFileName(file.name)}`);

    return this._http
      .patch<void>(
        `/company-api/stock/${productType}/${productId}/files/${fileId}`,
        form,
        { reportProgress: true, observe: 'events' },
      )
      .pipe(
        map((event) => {
          if (event.type === HttpEventType.Sent) {
            return 0;
          }

          if (event.type === HttpEventType.UploadProgress) {
            return Math.round((event.loaded / event.total!) * 100);
          }

          if (event.type === HttpEventType.Response && event.ok) {
            return 100;
          }

          return null;
        }),
      );
  }

  public removeFile(
    productType: ProductTypeEnum,
    productId: number,
    fileId: string,
  ): Observable<void> {
    return this._http.delete<void>(
      `/company-api/stock/${productType}/${productId}/files/${fileId}`,
    );
  }

  private _getExtensionFromFileName(name: string): string {
    const ext = name.split('.').pop();

    if (!ext) {
      throw new Error('File name has no extension');
    }

    return ext;
  }

  public getFile(file: IAppFile): Observable<Blob> {
    return this._http.get(
      `/static/${file.module}/${file.path}/${file._id}${file.extension}`,
      { responseType: 'blob' },
    );
  }
}
