import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ISettingsPlanningViewDto } from '../dto';
import { Observable } from 'rxjs';
import { IAppSettings } from '../interfaces';

@Injectable({ providedIn: 'root' })
export class AppSettingsPlanningViewsService {
  constructor(private _http: HttpClient) {}

  public createOne(dto: ISettingsPlanningViewDto): Observable<IAppSettings> {
    return this._http.post<IAppSettings>(
      `/company-api/app-settings/planning/views`,
      dto,
    );
  }

  public updateOne(
    id: string,
    dto: ISettingsPlanningViewDto,
  ): Observable<IAppSettings> {
    return this._http.patch<IAppSettings>(
      `/company-api/app-settings/planning/views/${id}`,
      dto,
    );
  }

  public removeOne(id: string): Observable<IAppSettings> {
    return this._http.delete<IAppSettings>(
      `/company-api/app-settings/planning/views/${id}`,
    );
  }

  public addMachineId(id: string, machineId: number): Observable<IAppSettings> {
    return this._http.post<IAppSettings>(
      `/company-api/app-settings/planning/views/${id}/machine-ids`,
      { machineId },
    );
  }

  public removeMachineId(
    id: string,
    machineId: number,
  ): Observable<IAppSettings> {
    return this._http.delete<IAppSettings>(
      `/company-api/app-settings/planning/views/${id}/machine-ids/${machineId}`,
    );
  }
}
