import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IJobWorksheetCreateDto } from '../dto';
import { Observable } from 'rxjs';
import { IJob } from '../interfaces';
import { IJobWorksheetUpdateDto } from '../dto/i-job-worksheet-update.dto';

@Injectable({ providedIn: 'root' })
export class JobWorksheetsService {
  constructor(private _http: HttpClient) {}

  public addOne(jobId: string, dto: IJobWorksheetCreateDto): Observable<IJob> {
    return this._http.post<IJob>(`/company-api/jobs/${jobId}/worksheets`, dto);
  }

  public updateOne(
    jobId: string,
    worksheetId: string,
    dto: IJobWorksheetUpdateDto,
  ): Observable<IJob> {
    return this._http.patch<IJob>(
      `/company-api/jobs/${jobId}/worksheets/${worksheetId}`,
      dto,
    );
  }

  public removeOne(jobId: string, worksheetId: string): Observable<IJob> {
    return this._http.delete<IJob>(
      `/company-api/jobs/${jobId}/worksheets/${worksheetId}`,
    );
  }
}
