import { Injectable } from '@angular/core';
import { IJobStockReservation } from '../interfaces/job/i-job-stock-reservation';
import {
  AuthActionsJobsEnum,
  AuthSubjectsEnum,
  JobStockReservationStatusEnum,
} from '../enums';
import { Store } from '@ngrx/store';
import { selectUser } from '../store/user/user.selectors';

@Injectable({ providedIn: 'root' })
export class JobStockReservationsHelperService {
  private _canCreate = false;
  private _canHandle = false;
  constructor(private _store: Store) {
    this._store.select(selectUser).subscribe((user) => {
      if (!user) {
        return;
      }

      const canManage = user.canActivate({
        subject: AuthSubjectsEnum.Jobs,
        action: AuthActionsJobsEnum.ManageStockReservations,
      });

      if (canManage) {
        this._canCreate = true;
        this._canHandle = true;
        return;
      }

      this._canCreate = user.canActivate({
        subject: AuthSubjectsEnum.Jobs,
        action: AuthActionsJobsEnum.CreateStockReservations,
      });

      this._canHandle = user.canActivate({
        subject: AuthSubjectsEnum.Jobs,
        action: AuthActionsJobsEnum.HandleStockReservations,
      });
    });
  }

  get canCreate(): boolean {
    return this._canCreate;
  }

  get canHandle(): boolean {
    return this._canHandle;
  }

  public getGlobalStatus(
    reservations: IJobStockReservation[],
  ): JobStockReservationStatusEnum | null {
    if (reservations.length === 0) {
      return null;
    }

    if (
      reservations.every(
        (item) => item.status === JobStockReservationStatusEnum.Brouillon,
      )
    ) {
      return JobStockReservationStatusEnum.Brouillon;
    }

    if (
      reservations.every(
        (item) => item.status === JobStockReservationStatusEnum.ATraiter,
      )
    ) {
      return JobStockReservationStatusEnum.ATraiter;
    }

    if (
      reservations.every(
        (item) => item.status === JobStockReservationStatusEnum.EnCours,
      )
    ) {
      return JobStockReservationStatusEnum.EnCours;
    }

    if (
      reservations.every(
        (item) =>
          item.status === JobStockReservationStatusEnum.AttenteLivraison,
      )
    ) {
      return JobStockReservationStatusEnum.AttenteLivraison;
    }

    if (
      reservations.every(
        (item) => item.status === JobStockReservationStatusEnum.Traitee,
      )
    ) {
      return JobStockReservationStatusEnum.Traitee;
    }

    return JobStockReservationStatusEnum.EnCours;
  }
}
