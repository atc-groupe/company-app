import { Injectable } from '@angular/core';
import { ModalService } from './modal.service';
import { IAlertOptions } from '../interfaces';
import { AlertComponent } from '../components/alert/alert.component';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class AlertService {
  constructor(private _modalService: ModalService) {}

  public open(
    title: string,
    message: string,
    options: IAlertOptions,
    yPos: 'top' | 'center' = 'center',
  ): { afterClosed$: Observable<'confirm' | 'decline' | 'dismiss'> } {
    return this._modalService.open(AlertComponent, {
      inputs: { title, message, ...options },
      options: { yPos },
    });
  }

  public dismiss() {
    this._modalService.close('dismiss');
  }

  public confirm() {
    this._modalService.close('confirm');
  }

  public decline() {
    this._modalService.close('decline');
  }
}
