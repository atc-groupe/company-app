import { Injectable } from '@angular/core';
import { AbstractWsService } from './abstract-ws-service';
import { Observable } from 'rxjs';
import { ILog } from '../interfaces';

@Injectable({ providedIn: 'root' })
export class WsLogsService extends AbstractWsService {
  constructor() {
    super('logs');
  }

  onLog(): Observable<ILog> {
    return this.on<ILog>('log');
  }
}
