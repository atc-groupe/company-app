import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IRoleDto } from '../dto';
import { Observable } from 'rxjs';
import { IRole } from '../interfaces';

@Injectable({ providedIn: 'root' })
export class RolesService {
  constructor(private _http: HttpClient) {}

  public createOne(dto: IRoleDto): Observable<IRole> {
    return this._http.post<IRole>('/company-api/roles', dto);
  }

  public updateOne(id: string, dto: IRoleDto): Observable<IRole> {
    return this._http.patch<IRole>(`/company-api/roles/${id}`, dto);
  }

  public deleteOne(id: string): Observable<IRole> {
    return this._http.delete<IRole>(`/company-api/roles/${id}`);
  }

  public findAll(): Observable<IRole[]> {
    return this._http.get<IRole[]>('/company-api/roles');
  }

  public findOne(id: string): Observable<IRole> {
    return this._http.get<IRole>(`/company-api/roles/${id}`);
  }

  public addAuthorization(id: string, name: string): Observable<IRole> {
    return this._http.patch<IRole>(
      `/company-api/roles/${id}/authorizations/add`,
      { name },
    );
  }

  public removeAuthorization(id: string, name: string): Observable<IRole> {
    return this._http.patch<IRole>(
      `/company-api/roles/${id}/authorizations/remove`,
      { name },
    );
  }
}
