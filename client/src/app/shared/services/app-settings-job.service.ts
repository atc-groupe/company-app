import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IAppSettings } from '../interfaces';
import { Observable } from 'rxjs';
import {
  IAppSettingsJobMappingMetaFilterDto,
  IAppSettingsJobStatusDto,
  IAppSettingsJobSyncDto,
} from '../dto';

@Injectable({ providedIn: 'root' })
export class AppSettingsJobService {
  constructor(private _http: HttpClient) {}

  public addStatus(dto: IAppSettingsJobStatusDto): Observable<IAppSettings> {
    return this._http.post<IAppSettings>(
      '/company-api/app-settings/job/statuses',
      dto,
    );
  }

  public updateStatus(
    id: string,
    dto: IAppSettingsJobStatusDto,
  ): Observable<IAppSettings> {
    return this._http.patch<IAppSettings>(
      `/company-api/app-settings/job/statuses/${id}`,
      dto,
    );
  }

  public removeStatus(id: string): Observable<IAppSettings> {
    return this._http.delete<IAppSettings>(
      `/company-api/app-settings/job/statuses/${id}`,
    );
  }

  public changeStatusesDefaultColor(color: string): Observable<IAppSettings> {
    return this._http.patch<IAppSettings>(
      '/company-api/app-settings/job/statuses/default-color',
      {
        color,
      },
    );
  }

  public addMappingMetadataItem(
    dto: IAppSettingsJobMappingMetaFilterDto,
  ): Observable<IAppSettings> {
    return this._http.post<IAppSettings>(
      `/company-api/app-settings/job/mapping/meta-filters`,
      dto,
    );
  }

  public removeMappingMetadataItem(
    dto: IAppSettingsJobMappingMetaFilterDto,
  ): Observable<IAppSettings> {
    return this._http.delete<IAppSettings>(
      `/company-api/app-settings/job/mapping/meta-filters`,
      { body: dto },
    );
  }

  public updateSync(dto: IAppSettingsJobSyncDto): Observable<IAppSettings> {
    return this._http.patch<IAppSettings>(
      `/company-api/app-settings/job/sync/`,
      dto,
    );
  }
}
