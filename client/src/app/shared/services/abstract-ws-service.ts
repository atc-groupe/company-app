import { Socket } from 'socket.io-client';
import { Observable } from 'rxjs';
import { io } from 'socket.io-client';
import {
  IAppWsConnectionError,
  IAppWsErrorResponse,
  IAppWsResponse,
} from '../interfaces';
import { environment } from 'src/environments/environment';
import { SecurityService } from './security.service';
import { inject } from '@angular/core';
export abstract class AbstractWsService {
  protected io: Socket;
  protected namespace: string | null = null;
  private CONNECT = 'connect';
  private CONNECT_ERROR = 'connect_error';
  private ERROR = 'exception';
  private _securityService = inject(SecurityService);

  protected constructor(namespace?: string) {
    let uri: string = environment.wsUri;

    if (namespace) {
      this.namespace = namespace;
      uri = `${uri}/${namespace}`;
    }

    this.io = io(uri, {
      autoConnect: false,
      auth: {
        accessToken: this._securityService.token,
      },
      secure: true,
      transports: ['websocket'],
    });
  }

  public connect(userId?: string): void {
    if (this.io.connected) {
      return;
    }

    if (userId) {
      this.io.auth = { id: userId };
    }

    if (this._securityService.token) {
      this.io.auth = {
        ...this.io.auth,
        accessToken: this._securityService.token,
      };
    }

    this.io.connect();
  }

  public emit<T>(event: string, data?: any): Observable<T> {
    return new Observable<T>((subscriber) => {
      this.io.emit(event, data, (result: IAppWsResponse<T>) => {
        if (result.status > 299) {
          subscriber.error({
            status: result.status,
            message: result.error,
          });
        } else {
          subscriber.next(result.data);
        }

        subscriber.complete();
      });
    });
  }

  public on<T>(eventName: string): Observable<T> {
    return new Observable<T>((subscriber) => {
      this.io.on(eventName, (data) => {
        subscriber.next(data);
      });
    });
  }

  public onConnect(): Observable<void> {
    return new Observable<void>((subscriber) => {
      this.io.on(this.CONNECT, () => {
        subscriber.next();
      });
    });
  }

  public onError(): Observable<IAppWsErrorResponse> {
    return new Observable<IAppWsErrorResponse>((subscriber) => {
      this.io.on(this.ERROR, (err) => {
        subscriber.next(err);
      });
    });
  }

  public onConnectError(): Observable<IAppWsConnectionError> {
    return new Observable((subscriber) => {
      this.io.on(this.CONNECT_ERROR, (err) => {
        const message = this.namespace
          ? `Connexion au flux ${this.namespace} impossible`
          : 'Connexion impossible';
        subscriber.next({ message });
      });
    });
  }

  public disconnect(): void {
    this.io.disconnect();
  }
}
