import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { PlanningSetup } from '../entities';
import { TPlanningSetup } from '../types';

@Injectable({ providedIn: 'root' })
export class PlanningService {
  constructor(private _http: HttpClient) {}

  public getSetup(): Observable<PlanningSetup> {
    return this._http
      .get<TPlanningSetup>('/company-api/planning/setup')
      .pipe(map((setupData) => new PlanningSetup(setupData)));
  }
}
