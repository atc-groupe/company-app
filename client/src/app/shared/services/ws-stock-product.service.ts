import { Injectable } from '@angular/core';
import { AbstractWsService } from './abstract-ws-service';
import { map, Observable } from 'rxjs';
import {
  IArticleData,
  IPaperData,
  IProduct,
  IProductChange,
  IProductChangeEvent,
} from '../interfaces';
import { ProductTypeEnum } from '../enums';
import { Article, Paper } from '../entities';
import { TProductData } from '../types';
import { ProductFactory } from '../factories';

@Injectable({ providedIn: 'root' })
export class WsStockProductService extends AbstractWsService {
  constructor(private _productFactory: ProductFactory) {
    super('stock.product');
  }

  public onSubscribeProduct(
    productType: ProductTypeEnum,
    id: number,
  ): Observable<IProduct> {
    return this.emit<TProductData>('product.subscribe', {
      productType,
      productId: id,
    }).pipe(map((data) => this._productFactory.getProduct(productType, data)));
  }

  public onSubscribeArticle(articleId: number): Observable<Article> {
    return this.emit<IArticleData>('product.subscribe', {
      productType: ProductTypeEnum.Article,
      productId: articleId,
    }).pipe(map((data) => new Article(data)));
  }

  public onSubscribePaper(paperId: number): Observable<Paper> {
    return this.emit<IPaperData>('product.subscribe', {
      productType: ProductTypeEnum.Paper,
      productId: paperId,
    }).pipe(map((data) => new Paper(data)));
  }

  public onProductChange(): Observable<IProductChange> {
    return this.on<IProductChangeEvent>('product.change').pipe(
      map((event) => {
        return {
          action: event.action,
          product: this._productFactory.getProduct(
            event.productType,
            event.product,
          ),
        };
      }),
    );
  }
}
