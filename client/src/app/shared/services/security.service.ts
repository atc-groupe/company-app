import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ICredentialsDto } from '../dto';

@Injectable({ providedIn: 'root' })
export class SecurityService {
  constructor(private _http: HttpClient) {}

  public signIn(
    credentials: ICredentialsDto,
  ): Observable<{ accessToken: string }> {
    return this._http
      .post<{ accessToken: string }>('/company-api/auth/sign-in', {
        identifier: `${credentials.identifier}@atc.local`,
        password: credentials.password,
      })
      .pipe(
        tap(({ accessToken }) => {
          this.token = accessToken;
        }),
      );
  }

  public logOut(): void {
    this.clearToken();
  }

  set token(token: string) {
    sessionStorage.setItem('token', token);
  }

  get token(): string | null {
    return sessionStorage.getItem('token');
  }

  public clearToken(): void {
    sessionStorage.removeItem('token');
  }
}
