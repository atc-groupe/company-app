import { Injectable } from '@angular/core';
import { AbstractWsService } from './abstract-ws-service';
import { IPlanningDataQueryParamsDto } from '../dto';
import { IPlanningCard, IPlanningCardDelete } from '../interfaces';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class WsPlanningService extends AbstractWsService {
  constructor() {
    super('planning');
  }

  public onSubscribeData(
    dto: IPlanningDataQueryParamsDto,
  ): Observable<IPlanningCard[]> {
    return this.emit<IPlanningCard[]>('data.subscribe', dto);
  }

  public onDataUpdate(): Observable<(IPlanningCard | IPlanningCardDelete)[]> {
    return this.on<(IPlanningCard | IPlanningCardDelete)[]>('data.update');
  }
}
