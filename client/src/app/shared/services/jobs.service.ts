import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IJobMappedStatus, IJobStatus } from '../interfaces';
import { IJobStatusChangeDto } from '../dto';

@Injectable({ providedIn: 'root' })
export class JobsService {
  constructor(private _http: HttpClient) {}

  getStatusesList(): Observable<IJobStatus[]> {
    return this._http.get<IJobStatus[]>('/company-api/jobs/statuses');
  }

  getMappedStatusesList(): Observable<IJobMappedStatus[]> {
    return this._http.get<IJobMappedStatus[]>(
      '/company-api/jobs/mapped-statuses',
    );
  }

  changeJobStatus(
    mpNumber: number,
    dto: IJobStatusChangeDto,
  ): Observable<void> {
    return this._http.patch<void>(`/company-api/jobs/${mpNumber}/status`, dto);
  }
}
