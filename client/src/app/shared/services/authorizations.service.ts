import { Injectable } from '@angular/core';
import { INavItem } from '../interfaces';
import { Store } from '@ngrx/store';
import { map, Observable } from 'rxjs';
import { UserSelectors } from '../store/user';
import { APP_NAV } from '../../app.nav';
import { AppModuleEnum, AuthSubjectsEnum } from '../enums';

@Injectable({ providedIn: 'root' })
export class AuthorizationsService {
  private _user$ = this._store.select(UserSelectors.selectUser);

  constructor(private _store: Store) {}

  public getAppFeatureNavItems(): Observable<INavItem[]> {
    return this._store.select(UserSelectors.selectUser).pipe(
      map((user) => {
        return APP_NAV.map((item) => {
          if (!user) {
            item.disabled = true;
            return item;
          }

          if (user.isAdmin) {
            item.disabled = false;
            return item;
          }

          item.disabled =
            !!item.route &&
            !user.authorizations.has(
              `${AuthSubjectsEnum.Modules}.${item.route}`,
            );

          return item;
        });
      }),
    );
  }
}
