import { Injectable } from '@angular/core';
import { AbstractWsService } from './abstract-ws-service';
import { map, Observable } from 'rxjs';
import {
  IArticleData,
  IPaperData,
  IProduct,
  IProductChange,
} from '../interfaces';
import { TProductData, TProductGroupType } from '../types';
import { Article, Paper } from '../entities';
import { ProductTypeEnum } from '../enums';
import { IProductChangeEvent } from '../interfaces';
import { ProductFactory } from '../factories';

@Injectable({ providedIn: 'root' })
export class WsStockProductsService extends AbstractWsService {
  constructor(private _productFactory: ProductFactory) {
    super('stock.products');
  }

  public onSubscribeProducts(
    productType: ProductTypeEnum,
    group: TProductGroupType,
  ): Observable<IProduct[]> {
    return this.emit<TProductData[]>(`products.subscribe`, {
      productType,
      groupType: this.getProductGroupType(productType, group),
    }).pipe(
      map((productsData) =>
        productsData.map((productData) =>
          this._productFactory.getProduct(productType, productData),
        ),
      ),
    );
  }

  public onProductChange(): Observable<IProductChange> {
    return this.on<IProductChangeEvent>('product.change').pipe(
      map((event) => {
        return {
          action: event.action,
          product: this._productFactory.getProduct(
            event.productType,
            event.product,
          ),
        };
      }),
    );
  }

  private getProductGroupType(
    productType: ProductTypeEnum,
    groupType: TProductGroupType,
  ): number {
    switch (productType) {
      case ProductTypeEnum.Article:
        switch (groupType) {
          case 'inks':
            return 90;
          case 'large-format':
            return 12;
          case 'other':
            return 8;
          default:
            throw new Error('Unsupported groupType');
        }
      case ProductTypeEnum.Paper:
        switch (groupType) {
          case 'sheets':
            return 0;
          case 'rolls':
            return 3;
          default:
            throw new Error('Unsupported groupType');
        }
      default:
        throw new Error('Unsupported productType');
    }
  }
}
