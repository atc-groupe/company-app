import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class JobPrePressService {
  constructor(private _http: HttpClient) {}

  public updateDesignNote(
    id: string,
    designNote: string | null,
  ): Observable<void> {
    const designNoteAsString = designNote ? JSON.stringify(designNote) : '';

    return this._http.patch<void>(`/company-api/jobs/${id}/pre-press`, {
      designNote: designNoteAsString,
    });
  }

  public updateGraphicDesignNote(
    id: string,
    graphicDesignNote: string | null,
  ): Observable<void> {
    const graphicDesignNoteAsString = graphicDesignNote
      ? JSON.stringify(graphicDesignNote)
      : '';

    return this._http.patch<void>(`/company-api/jobs/${id}/pre-press`, {
      graphicDesignNote: graphicDesignNoteAsString,
    });
  }

  public updateNote(id: string, note: string | null): Observable<void> {
    const noteAsString = note ? JSON.stringify(note) : '';

    return this._http.patch<void>(`/company-api/jobs/${id}/pre-press`, {
      note: noteAsString,
    });
  }

  public updateFilesCheck(
    id: string,
    filesCheck: string | null,
  ): Observable<void> {
    const filesCheckAsString = filesCheck ? JSON.stringify(filesCheck) : '';

    return this._http.patch<void>(`/company-api/jobs/${id}/pre-press`, {
      filesCheck: filesCheckAsString,
    });
  }

  public updateFilesCheckStatusIndex(
    id: string,
    index: number,
  ): Observable<void> {
    return this._http.patch<void>(`/company-api/jobs/${id}/pre-press`, {
      filesCheckStatusIndex: index,
    });
  }
}
