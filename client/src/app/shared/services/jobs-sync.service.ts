import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IActivesJobsSyncResult, IJob } from '../interfaces';

@Injectable({ providedIn: 'root' })
export class JobsSyncService {
  constructor(private _http: HttpClient) {}

  public syncOne(number: number): Observable<IJob> {
    return this._http.get<IJob>(`/company-api/jobs/sync/${number}`);
  }

  public syncActives(): Observable<IActivesJobsSyncResult> {
    return this._http.get<IActivesJobsSyncResult>(
      '/company-api/jobs/sync/actives',
    );
  }
}
