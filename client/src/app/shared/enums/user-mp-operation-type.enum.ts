export enum UserMpOperationTypeEnum {
  Manipulation = 0,
  Improductive = 1,
  Status = 2,
}
