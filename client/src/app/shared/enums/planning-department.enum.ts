export enum PlanningDepartmentEnum {
  PrePress,
  Offset,
  Digital,
  Finishing,
  Subcontractors,
}
