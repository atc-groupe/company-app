export enum JobStockReservationStatusEnum {
  Brouillon = 'Brouillon',
  ATraiter = 'A traiter',
  EnCours = 'En cours',
  AttenteLivraison = 'Attente livraison',
  Traitee = 'Traitée',
}
