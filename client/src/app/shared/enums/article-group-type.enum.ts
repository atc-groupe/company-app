export enum ArticleGroupTypeEnum {
  All = 0,
  OtherFinishes = 8,
  Lamination = 11,
  LargeFormat = 12,
  Ink = 90,
}
