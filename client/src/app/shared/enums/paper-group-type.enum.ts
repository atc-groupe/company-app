export enum PaperGroupTypeEnum {
  Sheets = 0,
  Rolls = 3,
  All = 4,
}
