export enum AuthSubjectsEnum {
  Modules = 'modules',
  Users = 'users',
  Roles = 'roles',
  Jobs = 'jobs',
  Products = 'products',
  Settings = 'settings',
}
