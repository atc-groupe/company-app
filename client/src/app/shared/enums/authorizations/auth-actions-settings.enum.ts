export enum AuthActionsSettingsEnum {
  ManageMappingParameters = 'manageMappingParameters',
  ManageJobsAutomationRules = 'manageJobsAutomationRules',
  ManageProductsGenericNames = 'manageProductsGenericNames',
  ManagePlanningViews = 'managePlanningViews',
}
