export enum AppModuleEnum {
  Account = 'account',
  Admin = 'admin',
  Dashboard = 'dashboard',
  Expeditions = 'expeditions',
  Jobs = 'jobs',
  Planning = 'planning',
  Stock = 'stock',
}
