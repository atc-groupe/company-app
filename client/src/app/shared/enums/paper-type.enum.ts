export enum PaperTypeEnum {
  Roll = 'roll',
  Sheet = 'sheet',
}
