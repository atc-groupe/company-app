export enum LogContextEnum {
  activeDirectory = 'ACTIVE DIRECTORY',
  auth = 'AUTH',
  optimaMultiPressSync = 'OPTIMA MULTIPRESS SYNC',
  mpApiCall = 'MULTIPRESS API CALL',
  mpHook = 'MP HOOK',
  planning = 'PLANNING',
  users = 'USERS',
  userSync = 'USER SYNC',
  jobSync = 'JOB SYNC',
}
