export enum PlanningPeriodEnum {
  Day = 'day',
  Week = 'week',
  All = 'all',
}
