export enum NotificationTypeEnum {
  Info = 'info',
  Warning = 'warning',
  Error = 'error',
  Success = 'success',
}
