export enum AppModuleLabelEnum {
  Account = 'Compte',
  Admin = 'Admin',
  Dashboard = 'Dashboard',
  Expeditions = 'Expeditions',
  Jobs = 'Jobs',
  Planning = 'Planning',
  Stock = 'Stock',
}
