import { TAppSettingsJobMappingMetaFilter } from '../types';

export const APP_SETTINGS_JOB_MAPPING_META_FILTERS: TAppSettingsJobMappingMetaFilter[] =
  [
    'deliveryServiceFinishes',
    'zundCuttingFinishes',
    'scoreFinishes',
    'kitFinishes',
    'materialDevices',
    'shippingDeliveryMethods',
    'skippedSubcontractors',
  ];
