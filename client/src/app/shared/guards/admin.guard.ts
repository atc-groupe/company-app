import { CanActivateFn } from '@angular/router';
import { inject } from '@angular/core';
import { Store } from '@ngrx/store';
import { UserSelectors } from '../store/user';
import { first, map } from 'rxjs';

export const adminGuard: CanActivateFn = () => {
  return inject(Store)
    .select(UserSelectors.selectUser)
    .pipe(
      first((v) => v !== null),
      map((user) => {
        return !(!user || !user.isAdmin);
      }),
    );
};
