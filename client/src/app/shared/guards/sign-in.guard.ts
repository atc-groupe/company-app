import { CanActivateFn, Router } from '@angular/router';
import { inject } from '@angular/core';
import { Store } from '@ngrx/store';
import { first, map } from 'rxjs';
import { SecuritySelectors } from '../store/security';

export const signInGuard: CanActivateFn = (route, state) => {
  const router = inject(Router);
  return inject(Store)
    .select(SecuritySelectors.selectIsAuthenticated)
    .pipe(
      first((v) => v !== null),
      map((isAuthenticated) => {
        if (isAuthenticated) {
          return router.createUrlTree(['/']);
        }

        return true;
      }),
    );
};
