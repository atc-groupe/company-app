import { CanActivateFn, Router } from '@angular/router';
import { inject } from '@angular/core';
import { Store } from '@ngrx/store';
import { SecuritySelectors } from '../store/security';
import { first, map } from 'rxjs';

export const authGuard: CanActivateFn = () => {
  const router = inject(Router);
  return inject(Store)
    .select(SecuritySelectors.selectIsAuthenticated)
    .pipe(
      first((v) => v !== null),
      map((isAuthenticated) => {
        if (!isAuthenticated) {
          return router.createUrlTree(['/sign-in']);
        }

        return true;
      }),
    );
};
