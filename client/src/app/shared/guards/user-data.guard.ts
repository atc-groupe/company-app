import { CanActivateFn } from '@angular/router';
import { inject } from '@angular/core';
import { Store } from '@ngrx/store';
import { UserSelectors, UserActions } from '../store/user';
import { first, map } from 'rxjs';

export const userDataGuard: CanActivateFn = (route, state) => {
  const store = inject(Store);

  return store.select(UserSelectors.selectUser).pipe(
    first(),
    map((user) => {
      if (!user) {
        store.dispatch(UserActions.tryFetchUserAction());
      }

      return true;
    }),
  );
};
