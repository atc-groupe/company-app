import {
  CanActivateChildFn,
  CanActivateFn,
  Router,
  UrlTree,
} from '@angular/router';
import { inject } from '@angular/core';
import { Store } from '@ngrx/store';
import { selectUser } from '../store/user/user.selectors';
import { first, map } from 'rxjs';
import { AuthSubjectsEnum } from '../enums';

export const FeatureNavGuard: CanActivateChildFn = (route, state) => {
  const router = inject(Router);

  return inject(Store)
    .select(selectUser)
    .pipe(
      first((v) => v !== null),
      map((user) => {
        const moduleUrl = state.url.split('/')[1];
        const authAsString = `${AuthSubjectsEnum.Modules}.${moduleUrl}`;

        if (!user) {
          return router.createUrlTree(['/login']);
        }

        if (user.isAdmin || state.url === '/' || state.url === '/dashboard') {
          return true;
        }

        return user.canActivate(authAsString)
          ? true
          : router.createUrlTree(['/']);
      }),
    );
};
