import { Notification } from './notification';
import { NotificationTypeEnum } from '../enums';

export class WarningNotification extends Notification {
  constructor(message: string) {
    super(message, {
      icon: 'fmd_bad',
      duration: 3,
      type: NotificationTypeEnum.Warning,
    });
  }
}
