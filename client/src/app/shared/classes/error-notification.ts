import { Notification } from './notification';
import { NotificationTypeEnum } from '../enums';

export class ErrorNotification extends Notification {
  constructor(message: string) {
    super(message, {
      icon: 'sync_problem',
      duration: 3,
      type: NotificationTypeEnum.Error,
    });
  }
}
