import { NotificationTypeEnum } from '../enums';

export class Notification {
  public readonly message: string;
  public readonly duration: number = 1; // seconds
  public readonly type: NotificationTypeEnum = NotificationTypeEnum.Info;
  public readonly position: 'left' | 'center' | 'right' = 'right';
  public readonly mustValidate: boolean = false;
  public readonly title?: string;
  public readonly icon?: string;

  constructor(
    message: string,
    options?: {
      type?: NotificationTypeEnum;
      title?: string;
      duration?: number;
      icon?: string;
      mustValidate?: boolean;
    },
  ) {
    this.message = message;

    if (!options) {
      return;
    }

    if (options.type) {
      this.type = options.type;
    }

    if (options.title) {
      this.title = options.title;
    }

    if (options.duration) {
      this.duration = options.duration;
    }

    if (options.icon) {
      this.icon = options.icon;
    }

    if (options.mustValidate) {
      this.mustValidate = options.mustValidate;
    }
  }
}
