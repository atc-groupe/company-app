import { Notification } from './notification';
import { NotificationTypeEnum } from '../enums';

export class SuccessNotification extends Notification {
  constructor(message: string) {
    super(message, {
      icon: 'done',
      duration: 3,
      type: NotificationTypeEnum.Success,
    });
  }
}
