import { Notification } from './notification';
import { NotificationTypeEnum } from '../enums';

export class InfoNotification extends Notification {
  constructor(message: string) {
    super(message, {
      icon: 'lightbulb',
      duration: 3,
      type: NotificationTypeEnum.Info,
    });
  }
}
