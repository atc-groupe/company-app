import { ISubjectAuthorization } from '../interfaces';

export type TAppAuthorizations = ISubjectAuthorization[];
