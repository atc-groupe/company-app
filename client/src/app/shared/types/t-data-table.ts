import { IDataTableColumn } from '../interfaces';

export type TDataTable = IDataTableColumn[];
