export type TAppSettingsJobMappingMetaFilter =
  | 'deliveryServiceFinishes'
  | 'zundCuttingFinishes'
  | 'scoreFinishes'
  | 'kitFinishes'
  | 'materialDevices'
  | 'shippingDeliveryMethods'
  | 'skippedSubcontractors';
