import { IPlanningSetupGroup } from '../interfaces';

export type TPlanningSetup = IPlanningSetupGroup[];
