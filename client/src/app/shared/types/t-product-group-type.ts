import { TArticleGroupType } from './t-article-group-type';
import { TPaperGroupType } from './t-paper-group-type';

export type TProductGroupType = TArticleGroupType | TPaperGroupType;
