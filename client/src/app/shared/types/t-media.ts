export type TMedia = 'xs' | 'sm' | 'md' | 'lg' | 'xl' | 'xxl';
