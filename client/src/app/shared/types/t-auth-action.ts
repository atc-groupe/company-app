import {
  AppModuleEnum,
  AuthActionsEnum,
  AuthActionsJobsEnum,
  AuthActionsProductsEnum,
  AuthActionsSettingsEnum,
} from '../enums';

export type TAuthAction =
  | AuthActionsEnum
  | AuthActionsJobsEnum
  | AuthActionsProductsEnum
  | AuthActionsSettingsEnum
  | AppModuleEnum;
