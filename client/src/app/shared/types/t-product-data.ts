import { IArticleData, IPaperData } from '../interfaces';

export type TProductData = IArticleData | IPaperData;
