import { INavItem } from '../../interfaces';
import { createReducer, on } from '@ngrx/store';
import {
  hideNavbarAction,
  pullLastNavbarItemAction,
  pushNavbarItemAction,
  setNavbarItemsAction,
  toggleNavbarAction,
} from './navbar.actions';

export interface NavbarState {
  expanded: boolean | null;
  items: INavItem[] | null;
}

const INITIAL_STATE: NavbarState = {
  expanded: false,
  items: null,
};

export const navbarReducer = createReducer(
  INITIAL_STATE,
  on(toggleNavbarAction, (state): NavbarState => {
    return {
      ...state,
      expanded: !state.expanded,
    };
  }),

  on(hideNavbarAction, (state): NavbarState => {
    return {
      ...state,
      expanded: false,
    };
  }),

  on(setNavbarItemsAction, (state, { items }): NavbarState => {
    return {
      ...state,
      items,
    };
  }),

  on(pushNavbarItemAction, (state, { item }): NavbarState => {
    return {
      ...state,
      items: state.items ? [...state.items, item] : [item],
    };
  }),

  on(pullLastNavbarItemAction, (state): NavbarState => {
    if (!state.items) {
      return state;
    }

    const items = [...state.items];
    items.pop();

    return {
      ...state,
      items,
    };
  }),
);
