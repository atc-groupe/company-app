import { STORE_KEY_NAV } from '../store.constants';
import { createAction, props } from '@ngrx/store';
import { INavItem } from '../../interfaces';

const key = `[${STORE_KEY_NAV}]`;

export const toggleNavbarAction = createAction(`${key} toggle nav`);
export const hideNavbarAction = createAction(`${key} hide nav`);
export const setNavbarItemsAction = createAction(
  `${key} set items`,
  props<{ items: INavItem[] | null }>(),
);
export const pushNavbarItemAction = createAction(
  `${key} push item`,
  props<{ item: INavItem }>(),
);

export const pullLastNavbarItemAction = createAction(`${key} remove last item`);
