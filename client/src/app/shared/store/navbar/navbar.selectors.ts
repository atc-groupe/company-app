import { createFeatureSelector, createSelector } from '@ngrx/store';
import { NavbarState } from './navbar.reducer';
import { STORE_KEY_NAV } from '../store.constants';
import { INavItem } from '../../interfaces';

export const selectNavbarState =
  createFeatureSelector<NavbarState>(STORE_KEY_NAV);

export const selectNavbarExpanded = createSelector(
  selectNavbarState,
  (state): boolean | null => state.expanded,
);
export const selectNavbarItems = createSelector(
  selectNavbarState,
  (state): INavItem[] | null => state.items,
);
