import { createFeatureSelector, createSelector } from '@ngrx/store';
import { STORE_KEY_USER } from '../store.constants';
import { UserState } from './user.reducer';
import { User } from '../../entities';

export const selectUserState = createFeatureSelector<UserState>(STORE_KEY_USER);

export const selectUser = createSelector(
  selectUserState,
  (state): User | null => state.user,
);

export const selectUserFetchError = createSelector(
  selectUserState,
  (state): string | null => state.fetchError,
);
