import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { MeService, UsersService } from '../../services';
import * as UserActions from './user.actions';
import { SecurityActions } from '../security';
import { catchError, map, of, switchMap } from 'rxjs';
import { Store } from '@ngrx/store';
import { WsMeService } from '../../services/ws-me.service';
import { tryFetchUserAction, updateUserAction } from './user.actions';
import { User } from '../../entities';
import { displayInfoNotificationAction } from '../ui';

@Injectable()
export class UserEffects {
  tryFetchUserEffect = createEffect(() =>
    this._actions.pipe(
      ofType(UserActions.tryFetchUserAction),
      switchMap(() =>
        this._meService.fetchAuthenticatedUser().pipe(
          map((user) => {
            this._store.dispatch(
              SecurityActions.setIsAuthenticatedAction({
                isAuthenticated: true,
              }),
            );
            return UserActions.fetchUserSuccessAction({ user });
          }),
          catchError((err) => {
            this._store.dispatch(
              SecurityActions.setIsAuthenticatedAction({
                isAuthenticated: false,
              }),
            );

            return of(
              UserActions.fetchUserErrorAction({ err: err.error.message }),
            );
          }),
        ),
      ),
    ),
  );

  constructor(
    private _actions: Actions,
    private _meService: MeService,
    private _wsMeService: WsMeService,
    private _store: Store,
  ) {
    this._wsMeService.onUpdate().subscribe((user) => {
      if (!user.isActive) {
        this._store.dispatch(SecurityActions.tryLogOutAction());
      } else {
        this._store.dispatch(updateUserAction({ user: new User(user) }));
        this._store.dispatch(
          displayInfoNotificationAction({
            message: 'Vos données ont été mises à jour',
          }),
        );
      }
    });

    this._wsMeService.onConnect().subscribe(() => {
      this._store.dispatch(tryFetchUserAction());
    });
  }
}
