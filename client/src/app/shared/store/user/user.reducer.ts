import { createReducer, on } from '@ngrx/store';
import * as UserActions from './user.actions';
import { User } from '../../entities';

export interface UserState {
  user: User | null;
  fetchError: string | null;
}

const INITIAL_STATE: UserState = {
  user: null,
  fetchError: null,
};

export const userReducer = createReducer(
  INITIAL_STATE,
  on(UserActions.tryFetchUserAction, (state): UserState => {
    return {
      ...state,
      fetchError: null,
    };
  }),

  on(UserActions.fetchUserSuccessAction, (state, { user }): UserState => {
    return {
      ...state,
      user,
      fetchError: null,
    };
  }),

  on(UserActions.fetchUserErrorAction, (state, { err }): UserState => {
    return {
      ...state,
      user: null,
      fetchError: err,
    };
  }),

  on(UserActions.updateUserAction, (state, { user }): UserState => {
    return {
      ...state,
      user,
    };
  }),

  on(UserActions.clearUserAction, (state): UserState => {
    return {
      ...state,
      user: null,
    };
  }),
);
