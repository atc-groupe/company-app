import { createAction, props } from '@ngrx/store';
import { STORE_KEY_USER } from '../store.constants';
import { User } from '../../entities';

const key = `[${STORE_KEY_USER}]`;

export const tryFetchUserAction = createAction(`${key} try fetch user`);

export const fetchUserSuccessAction = createAction(
  `${key} fetch user success`,
  props<{ user: User }>(),
);

export const fetchUserErrorAction = createAction(
  `${key} fetch user error`,
  props<{ err: string }>(),
);

export const updateUserAction = createAction(
  `${key} update user`,
  props<{ user: User }>(),
);

export const clearUserAction = createAction(`${key} clear user`);
