import { IAppSettings, IJobMappedStatus } from '../../interfaces';
import { createReducer, on } from '@ngrx/store';
import * as AppSettingsActions from './app-settings.actions';

export interface AppSettingsState {
  settings: IAppSettings | null;
  fetchError: string | null;
}

const INITIAL_STATE: AppSettingsState = {
  settings: null,
  fetchError: null,
};

export const appSettingsReducer = createReducer(
  INITIAL_STATE,

  on(AppSettingsActions.tryFetchSettingsAction, (state): AppSettingsState => {
    return {
      ...state,
      fetchError: null,
    };
  }),

  on(
    AppSettingsActions.fetchSettingsSuccessAction,
    (state, { settings }): AppSettingsState => {
      return {
        ...state,
        fetchError: null,
        settings,
      };
    },
  ),

  on(
    AppSettingsActions.fetchSettingsErrorAction,
    (state, { error }): AppSettingsState => {
      return {
        ...state,
        settings: null,
        fetchError: error,
      };
    },
  ),

  on(
    AppSettingsActions.updateSettingsAction,
    (state, { settings }): AppSettingsState => {
      return {
        ...state,
        settings,
        fetchError: null,
      };
    },
  ),
);
