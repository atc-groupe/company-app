import { createFeatureSelector, createSelector } from '@ngrx/store';
import { STORE_KEY_APP_SETTINGS } from '../store.constants';
import { AppSettingsState } from './app-settings.reducer';
import { IAppSettings } from '../../interfaces';

export const selectAppSettingsState = createFeatureSelector<AppSettingsState>(
  STORE_KEY_APP_SETTINGS,
);

export const selectAppSettings = createSelector(
  selectAppSettingsState,
  (state): IAppSettings | null => state.settings,
);
