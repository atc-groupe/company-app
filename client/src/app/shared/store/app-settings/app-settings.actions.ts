import { createAction, props } from '@ngrx/store';
import { STORE_KEY_APP_SETTINGS } from '../store.constants';
import { IAppSettings } from '../../interfaces';
const key = `[${STORE_KEY_APP_SETTINGS}]`;

// ---------- [fetch settings] ---------- //

export const tryFetchSettingsAction = createAction(`${key} try fetch settings`);
export const fetchSettingsSuccessAction = createAction(
  `${key} fetch settings success`,
  props<{ settings: IAppSettings }>(),
);
export const fetchSettingsErrorAction = createAction(
  `${key} fetch settings error`,
  props<{ error: string }>(),
);
export const updateSettingsAction = createAction(
  `${key} update settings from sse event`,
  props<{ settings: IAppSettings }>(),
);
