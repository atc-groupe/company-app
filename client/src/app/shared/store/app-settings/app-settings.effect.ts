import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { AppSettingsService, WsAppSettingsService } from '../../services';
import {
  fetchSettingsErrorAction,
  fetchSettingsSuccessAction,
  tryFetchSettingsAction,
  updateSettingsAction,
} from './app-settings.actions';
import { catchError, map, of, switchMap } from 'rxjs';
import { Store } from '@ngrx/store';

@Injectable()
export class AppSettingsEffect {
  tryFetchSettings = createEffect(() =>
    this._actions.pipe(
      ofType(tryFetchSettingsAction),
      switchMap(() =>
        this._appSettingsService.getAppSettings().pipe(
          map((settings) => fetchSettingsSuccessAction({ settings })),
          catchError((err) =>
            of(fetchSettingsErrorAction({ error: err.error.message })),
          ),
        ),
      ),
    ),
  );

  constructor(
    private _actions: Actions,
    private _appSettingsService: AppSettingsService,
    private _wsSettings: WsAppSettingsService,
    private _store: Store,
  ) {
    this._wsSettings.onUpdate().subscribe((settings) => {
      this._store.dispatch(updateSettingsAction({ settings }));
    });

    this._wsSettings.onConnect().subscribe(() => {
      this._store.dispatch(tryFetchSettingsAction());
    });
  }
}
