import { createFeatureSelector, createSelector } from '@ngrx/store';
import { PlanningState } from './planning.reducer';
import { STORE_KEY_PLANNING } from '../store.constants';
import {
  IPlanningFilters,
  ISortColumn,
  IPlanningMachineCards,
} from '../../interfaces';
import { PlanningSetup } from '../../entities';

export const selectPlanningState =
  createFeatureSelector<PlanningState>(STORE_KEY_PLANNING);

export const selectSetup = createSelector(
  selectPlanningState,
  (state): PlanningSetup | null => state.setup,
);

export const selectFilters = createSelector(
  selectPlanningState,
  (state): IPlanningFilters => state.filters,
);

export const selectOldFilters = createSelector(
  selectPlanningState,
  (state): IPlanningFilters | null => state.oldFilters,
);

export const selectDate = createSelector(
  selectPlanningState,
  (state): Date => state.date,
);

export const selectDayData = createSelector(
  selectPlanningState,
  (state): IPlanningMachineCards[] | null => state.computedData,
);

export const selectSearch = createSelector(
  selectPlanningState,
  (state): string | null => state.search,
);

export const selectFetchError = createSelector(
  selectPlanningState,
  (state): string | null => state.fetchError,
);

export const selectIsLoading = createSelector(
  selectPlanningState,
  (state): boolean => state.isLoading,
);

export const selectSortColumn = createSelector(
  selectPlanningState,
  (state): ISortColumn => state.sortColumn,
);

export const selectIsEmpty = createSelector(
  selectPlanningState,
  (state): boolean => !state.data?.length,
);
