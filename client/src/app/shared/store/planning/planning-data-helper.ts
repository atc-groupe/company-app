import { PlanningSetup } from '../../entities';
import {
  IPlanningCard,
  IPlanningCardDelete,
  IPlanningFilters,
  ISortColumn,
  IPlanningMachineCards,
  IPlanningSetupGroupMachine,
} from '../../interfaces';

export const getComputedData = (
  cards: IPlanningCard[],
  filters: IPlanningFilters,
  sortColumn: ISortColumn,
  setup: PlanningSetup,
): IPlanningMachineCards[] => {
  const machines = filters.view
    ? setup
        .getGroupMachinesNames(filters.view.planningGroup)
        .filter((item) => filters.view?.machineIds.includes(item.id))
    : null;

  return getSortedData(
    getMachinesData(getFilteredCards(cards, filters), machines),
    sortColumn,
    filters.period === 'day',
  );
};

export const getUpdatedCards = (
  stateCards: IPlanningCard[],
  newCards: (IPlanningCard | IPlanningCardDelete)[],
): IPlanningCard[] => {
  const updatedCards: IPlanningCard[] = [...stateCards]
    .filter(
      (card) =>
        newCards.find(
          (newCard) =>
            newCard.computedId === card.computedId &&
            newCard.syncAction === 'remove',
        ) === undefined,
    )
    .map((card) => {
      const newCard = newCards.find(
        (newCard) =>
          newCard.computedId === card.computedId &&
          newCard.syncAction === 'update',
      ) as IPlanningCard;

      return newCard ?? card;
    });

  newCards.forEach((newCard) => {
    const isInState =
      updatedCards.findIndex(
        (card) => card.computedId === newCard.computedId,
      ) !== -1;

    if (newCard.syncAction === 'add' && !isInState) {
      updatedCards.push(newCard);
    }
  });

  return updatedCards;
};

export const getSortedData = (
  dayData: IPlanningMachineCards[],
  sortColumn: ISortColumn,
  isDayView: boolean,
): IPlanningMachineCards[] => {
  return dayData
    .map((machineData) => {
      let cards = [...machineData.cards];

      if (sortColumn.index > 0 && !isDayView) {
        cards = cards
          .sort((a, b) => (a.ordering > b.ordering ? -1 : 1))
          .sort((a, b) => (a.startDate > b.startDate ? 1 : -1));
      }

      cards = cards.sort((a, b) => {
        let sortValA: number | string | Date | boolean = 0;
        let sortValB: number | string | Date | boolean = 0;

        switch (sortColumn.index) {
          case 0:
            if (!isDayView && sortColumn.dir === 'asc') {
              sortValA = b.ordering;
              sortValB = a.ordering;
            } else {
              sortValA = a.ordering;
              sortValB = b.ordering;
            }
            break;
          case 1:
            sortValA = a.jobInfo.number;
            sortValB = b.jobInfo.number;
            break;
          case 2:
            sortValA = a.jobInfo.company;
            sortValB = b.jobInfo.company;
            break;
          case 3:
            sortValA = a.jobInfo.statusNumber;
            sortValB = b.jobInfo.statusNumber;
            break;
          case 4:
            sortValA = a.jobInfo.machines.reduce(
              (acc: string, item) => (acc ? `${acc}, ${item}` : item),
              '',
            );
            sortValB = b.jobInfo.machines.reduce(
              (acc: string, item) => (acc ? `${acc}, ${item}` : item),
              '',
            );
            break;
          case 6:
            sortValA = a.operation;
            sortValB = b.operation;
            break;
          case 7:
            sortValA = a.employee ? a.employee : '';
            sortValB = b.employee ? b.employee : '';
            break;
          case 8:
            sortValA = a.productionTime;
            sortValB = b.productionTime;
            break;
          case 9:
            sortValA = a.jobInfo.sendingDate!;
            sortValB = b.jobInfo.sendingDate!;
            break;
          case 11:
            sortValA = a.completed;
            sortValB = b.completed;
            break;
        }

        if (sortValA > sortValB) {
          return sortColumn?.dir === 'asc' ? 1 : -1;
        }

        if (sortValA < sortValB) {
          return sortColumn?.dir === 'asc' ? -1 : 1;
        }

        return 0;
      });

      if (sortColumn.index === 0 && !isDayView) {
        cards = cards.reverse().sort((a, b) => {
          if (a.startDate > b.startDate) {
            return sortColumn?.dir === 'asc' ? 1 : -1;
          }

          if (a.startDate < b.startDate) {
            return sortColumn?.dir === 'asc' ? -1 : 1;
          }

          return 0;
        });
      }

      return {
        ...machineData,
        cards,
      };
    })
    .sort((a, b) => (a.name > b.name ? 1 : -1));
};

const getFilteredCards = (
  cards: IPlanningCard[],
  filters: IPlanningFilters,
): IPlanningCard[] => {
  const filteredCards = cards.filter((card) => {
    if (filters.hideDone && card.completed) {
      return false;
    }

    if (filters.hideNotPlanned && !card.isPlanned) {
      return false;
    }

    if (
      filters.mode === 'list' &&
      !filters.view?.machineIds.includes(card.machineId)
    ) {
      return false;
    }

    const employee = card.operationUserInfo
      ? card.operationUserInfo.employee
      : card.employee
      ? card.employee
      : ' Non attribué';

    if (
      (!employee && filters.employees?.length) ||
      (employee && filters.employees && !filters.employees.includes(employee))
    ) {
      return false;
    }

    return true;
  });

  if (!filters.mergeJobOperationCards) {
    return filteredCards;
  }

  return filteredCards.reduce((acc: IPlanningCard[], card) => {
    if (!acc.find((item) => item.id === card.id)) {
      acc.push(card);
    }

    return acc;
  }, []);
};

const getMachinesData = (
  cards: IPlanningCard[],
  machines: IPlanningSetupGroupMachine[] | null,
): IPlanningMachineCards[] => {
  if (machines) {
    return machines.map((machine) => {
      return {
        id: machine.id,
        name: machine.name,
        cards: cards.filter((item) => item.machineId === machine.id),
      };
    });
  }

  return cards.reduce((acc: IPlanningMachineCards[], card) => {
    const machineData = acc.find((el) => el.name === card.machineName);

    if (machineData) {
      machineData.cards.push(card);
    } else {
      acc.push({
        id: card.machineId,
        name: card.machineName,
        cards: [card],
      });
    }

    return acc;
  }, []);
};
