export const getPrevDay = (date: Date) => {
  const newDate = new Date(date);
  const inc = newDate.getDay() === 1 ? 3 : 1;
  newDate.setDate(newDate.getDate() - inc);

  return newDate;
};

export const getNextDay = (date: Date) => {
  const newDate = new Date(date);
  const inc = newDate.getDay() === 5 ? 3 : 1;
  newDate.setDate(newDate.getDate() + inc);

  return newDate;
};

export const getCurrentDay = () => {
  const newDate = new Date();
  let inc = 0;
  if (newDate.getDay() === 0) {
    inc = 2;
  }

  if (newDate.getDay() === 6) {
    inc = 1;
  }
  newDate.setDate(newDate.getDate() + inc);

  return newDate;
};

export const getPrevWeek = (date: Date) => {
  const newDate = new Date(date);
  newDate.setDate(newDate.getDate() - 7);

  return newDate;
};

export const getNextWeek = (date: Date) => {
  const newDate = new Date(date);
  newDate.setDate(newDate.getDate() + 7);

  return newDate;
};
