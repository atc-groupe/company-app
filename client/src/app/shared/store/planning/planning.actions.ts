import { STORE_KEY_PLANNING } from '../store.constants';
import { createAction, props } from '@ngrx/store';
import { PlanningSetup } from '../../entities';
import {
  IPlanningCard,
  IPlanningCardDelete,
  IPlanningCardJobInfo,
  IPlanningFilters,
  ISortColumn,
} from '../../interfaces';
import { PlanningModeEnum } from '../../enums';

const key = `[${STORE_KEY_PLANNING}]`;

export const tryFetchSetupAction = createAction(
  `${key} try fetch planning setup`,
);
export const fetchSetupSuccessAction = createAction(
  `${key} fetch planning setup success`,
  props<{ setup: PlanningSetup }>(),
);

export const initFiltersAction = createAction(
  `${key} init planning filters`,
  props<{ filters: IPlanningFilters }>(),
);
export const setFiltersAction = createAction(
  `${key} set planning filters`,
  props<{ filters: Partial<IPlanningFilters> }>(),
);

export const setPrevDayAction = createAction(`${key} set prev day`);
export const setNextDayAction = createAction(`${key} set next day`);
export const setCurrentDayAction = createAction(`${key} set current day`);
export const setPrevWeekAction = createAction(`${key} set prev week`);
export const setNextWeekAction = createAction(`${key} set next week`);

export const trySubscribeDataAction = createAction(`${key} try subscribe data`);
export const subscribeDataSuccessAction = createAction(
  `${key} subscribe data success`,
  props<{ cards: IPlanningCard[] }>(),
);

export const computeDataAction = createAction(`${key} compute data`);

export const updateDataAction = createAction(
  `${key} update data`,
  props<{ cards: (IPlanningCard | IPlanningCardDelete)[] }>(),
);

export const startSearchAction = createAction(`${key} start search`);

export const trySearchPlannedJobAction = createAction(
  `${key} try search planned job`,
  props<{ search: string }>(),
);

export const clearSearchAction = createAction(`${key} clear search`);

export const dataFetchErrorAction = createAction(
  `${key} fetch planning data error`,
  props<{ error: string }>(),
);

export const sortDataAction = createAction(
  `${key} sort data`,
  props<{ sortColumn: ISortColumn | null }>(),
);

export const clearDataAction = createAction(`${key} clear data`);
