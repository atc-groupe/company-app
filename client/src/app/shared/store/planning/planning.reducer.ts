import {
  IPlanningCard,
  IPlanningFilters,
  IPlanningMachineCards,
  ISortColumn,
} from '../../interfaces';
import { createReducer, on } from '@ngrx/store';
import { PlanningSetup } from '../../entities';
import {
  clearDataAction,
  clearSearchAction,
  computeDataAction,
  dataFetchErrorAction,
  fetchSetupSuccessAction,
  initFiltersAction,
  setCurrentDayAction,
  setFiltersAction,
  setNextDayAction,
  setNextWeekAction,
  setPrevDayAction,
  setPrevWeekAction,
  sortDataAction,
  startSearchAction,
  subscribeDataSuccessAction,
  trySearchPlannedJobAction,
  trySubscribeDataAction,
  updateDataAction,
} from './planning.actions';
import { PlanningModeEnum, PlanningPeriodEnum } from '../../enums';
import * as DateHelper from './planning-date-helper';
import * as MachinesDataHelper from './planning-data-helper';

export interface PlanningState {
  setup: PlanningSetup | null;
  oldFilters: IPlanningFilters | null;
  filters: IPlanningFilters;
  date: Date;
  sortColumn: ISortColumn;
  data: IPlanningCard[] | null;
  computedData: IPlanningMachineCards[] | null;
  search: string | null;
  fetchError: string | null;
  isLoading: boolean;
}

export const PLANNING_INITIAL_FILTERS: IPlanningFilters = {
  mode: PlanningModeEnum.List,
  period: PlanningPeriodEnum.Day,
  view: null,
  hideNotPlanned: true,
  hideDone: true,
  mergeJobOperationCards: false,
  employees: null,
};

export const PLANNING_INITIAL_SORT_COLUMN: ISortColumn = {
  index: 0,
  dir: 'asc',
};

const INITIAL_STATE: PlanningState = {
  setup: null,
  oldFilters: null,
  filters: PLANNING_INITIAL_FILTERS,
  date: new Date(),
  sortColumn: PLANNING_INITIAL_SORT_COLUMN,
  data: null,
  computedData: null,
  search: null,
  fetchError: null,
  isLoading: true,
};

export const planningReducer = createReducer(
  INITIAL_STATE,

  on(fetchSetupSuccessAction, (state, { setup }): PlanningState => {
    return {
      ...state,
      setup,
      fetchError: null,
    };
  }),

  on(initFiltersAction, (state, { filters }): PlanningState => {
    return {
      ...state,
      filters,
    };
  }),

  on(setFiltersAction, (state, { filters }): PlanningState => {
    return {
      ...state,
      oldFilters: state.filters,
      filters: {
        ...state.filters,
        ...filters,
      },
    };
  }),

  on(setPrevDayAction, (state): PlanningState => {
    return {
      ...state,
      date: DateHelper.getPrevDay(state.date),
      search: null,
      data: null,
      computedData: null,
    };
  }),

  on(setPrevWeekAction, (state): PlanningState => {
    return {
      ...state,
      date: DateHelper.getPrevWeek(state.date),
      search: null,
      data: null,
      computedData: null,
    };
  }),

  on(setNextDayAction, (state): PlanningState => {
    return {
      ...state,
      date: DateHelper.getNextDay(state.date),
      search: null,
      data: null,
      computedData: null,
    };
  }),

  on(setNextWeekAction, (state): PlanningState => {
    return {
      ...state,
      date: DateHelper.getNextWeek(state.date),
      search: null,
      data: null,
      computedData: null,
    };
  }),

  on(setCurrentDayAction, (state): PlanningState => {
    return {
      ...state,
      date: DateHelper.getCurrentDay(),
      search: null,
      data: null,
      computedData: null,
    };
  }),

  on(trySubscribeDataAction, (state): PlanningState => {
    return {
      ...state,
      data: null,
      computedData: null,
      fetchError: null,
      isLoading: true,
    };
  }),

  on(subscribeDataSuccessAction, (state, { cards }): PlanningState => {
    if (!state.setup) {
      return state;
    }

    return {
      ...state,
      data: cards,
      computedData: MachinesDataHelper.getComputedData(
        cards,
        state.filters,
        state.sortColumn,
        state.setup,
      ),
      isLoading: false,
    };
  }),

  on(computeDataAction, (state): PlanningState => {
    if (!state.data || !state.setup) {
      return state;
    }

    return {
      ...state,
      computedData: MachinesDataHelper.getComputedData(
        state.data,
        state.filters,
        state.sortColumn,
        state.setup,
      ),
      isLoading: false,
    };
  }),

  on(updateDataAction, (state, { cards }) => {
    if (!state.setup) {
      return state;
    }

    const newData = state.data
      ? MachinesDataHelper.getUpdatedCards(state.data, cards)
      : (cards.filter(
          (item) => item.syncAction !== 'remove',
        ) as IPlanningCard[]);

    return {
      ...state,
      data: newData,
      computedData: MachinesDataHelper.getComputedData(
        newData,
        state.filters,
        state.sortColumn,
        state.setup,
      ),
    };
  }),

  on(dataFetchErrorAction, (state, { error }): PlanningState => {
    return {
      ...state,
      fetchError: error,
      isLoading: false,
    };
  }),

  on(sortDataAction, (state, { sortColumn }): PlanningState => {
    if (!state.computedData) {
      return state;
    }

    const checkedSortColumn =
      sortColumn === null ? PLANNING_INITIAL_SORT_COLUMN : sortColumn;

    return {
      ...state,
      sortColumn: checkedSortColumn,
      computedData: MachinesDataHelper.getSortedData(
        state.computedData,
        checkedSortColumn,
        state.filters.period === 'day',
      ),
    };
  }),

  on(startSearchAction, (state): PlanningState => {
    return {
      ...state,
      isLoading: true,
    };
  }),

  on(trySearchPlannedJobAction, (state, { search }): PlanningState => {
    return {
      ...state,
      search: search,
      data: null,
      computedData: null,
      fetchError: null,
    };
  }),

  on(clearSearchAction, (state): PlanningState => {
    return {
      ...state,
      search: null,
    };
  }),

  on(clearDataAction, (state): PlanningState => {
    return {
      ...state,
      data: null,
      computedData: null,
      isLoading: true,
    };
  }),
);
