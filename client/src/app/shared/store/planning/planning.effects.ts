import { Injectable } from '@angular/core';
import {
  PlanningDateHelperService,
  PlanningService,
  WsPlanningService,
} from '../../services';
import { Store } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, of, switchMap, withLatestFrom } from 'rxjs';
import { selectUser } from '../user/user.selectors';
import {
  clearSearchAction,
  computeDataAction,
  dataFetchErrorAction,
  fetchSetupSuccessAction,
  initFiltersAction,
  setCurrentDayAction,
  setFiltersAction,
  setNextDayAction,
  setNextWeekAction,
  setPrevDayAction,
  setPrevWeekAction,
  subscribeDataSuccessAction,
  tryFetchSetupAction,
  trySearchPlannedJobAction,
  trySubscribeDataAction,
  updateDataAction,
} from './planning.actions';
import {
  selectDate,
  selectFilters,
  selectOldFilters,
  selectSearch,
} from './planning.selectors';
import { IPlanningDataQueryParamsDto } from '../../dto';
import { PlanningPeriodEnum } from '../../enums';
import { selectAppSettings } from '../app-settings/app-settings.selectors';
import { PLANNING_INITIAL_FILTERS } from './planning.reducer';
import * as AppSettingsActions from '../app-settings/app-settings.actions';

@Injectable()
export class PlanningEffects {
  public tryFetchPlanningSetupEffect = createEffect(() =>
    this._actions.pipe(
      ofType(tryFetchSetupAction),
      withLatestFrom(
        this._store.select(selectUser),
        this._store.select(selectAppSettings),
      ),
      switchMap(([, user, settings]) =>
        this._planningService.getSetup().pipe(
          map((setup) => {
            if (!settings) {
              throw new Error(
                "Les paramètres de l'application ne sont pas initialisés",
              );
            }

            const filters = { ...PLANNING_INITIAL_FILTERS };

            if (
              settings.planning &&
              settings.planning.views &&
              settings.planning.views.length
            ) {
              filters.view = settings.planning.views[0];
            }

            if (user) {
              filters.mode = user.settings.planningMode;
              filters.hideNotPlanned = user.settings.planningHideNotPlanned;
              filters.hideDone = user.settings.planningHideDone;
              filters.mergeJobOperationCards =
                user.settings.planningMergeJobOperationCards;

              if (user.settings.planningPeriod) {
                filters.period = user.settings.planningPeriod;
              }

              if (user.settings.planningView) {
                const view = settings.planning.views.find(
                  (item) => item._id === user.settings.planningView,
                );

                if (view) {
                  filters.view = view;
                }
              }
            }

            this._store.dispatch(initFiltersAction({ filters }));

            return fetchSetupSuccessAction({ setup });
          }),
          catchError((err) => {
            console.log(err);
            return of(dataFetchErrorAction({ error: err.error.message }));
          }),
        ),
      ),
    ),
  );

  public trySubscribeDataEffect = createEffect(() =>
    this._actions.pipe(
      ofType(trySubscribeDataAction),
      withLatestFrom(
        this._store.select(selectFilters),
        this._store.select(selectDate),
        this._store.select(selectSearch),
      ),
      switchMap(([, filters, date, search]) => {
        if (filters.view === null) {
          return of(
            dataFetchErrorAction({
              error:
                "Affichage des données impossible car les filtres n'ont pas été définis. Veuillez définir les filtres.",
            }),
          );
        }

        const queryParams: IPlanningDataQueryParamsDto = {
          department: filters.view.planningDepartment,
          group: filters.view.planningGroup,
        };

        if (search) {
          queryParams.term = search;
        } else if (filters.period === PlanningPeriodEnum.Week) {
          const firstWeekDay = this._planningDateHelper.getFirstWeekDay(date);
          const lastWeekDay = this._planningDateHelper.getLastWeekDay(date);
          queryParams.startDate = firstWeekDay.toLocaleDateString('fr-FR');
          queryParams.endDate = lastWeekDay.toLocaleDateString('fr-FR');
        } else if (filters.period === PlanningPeriodEnum.Day) {
          queryParams.startDate = date.toLocaleDateString('fr-FR');
        }

        return this._wsPlanning.onSubscribeData(queryParams).pipe(
          map((cards) => subscribeDataSuccessAction({ cards })),
          catchError((err) => of(dataFetchErrorAction({ error: err.message }))),
        );
      }),
    ),
  );

  public onChangePlanningParamsEffect = createEffect(() =>
    this._actions.pipe(
      ofType(
        setPrevDayAction,
        setPrevWeekAction,
        setNextDayAction,
        setNextWeekAction,
        setCurrentDayAction,
      ),
      map(() => trySubscribeDataAction()),
    ),
  );

  public onChangePlanningFiltersEffect = createEffect(() =>
    this._actions.pipe(
      ofType(setFiltersAction),
      withLatestFrom(this._store.select(selectOldFilters)),
      map(([{ filters }, oldFilters]) => {
        if (!filters.view && !filters.period) {
          return computeDataAction();
        }

        if (
          oldFilters &&
          filters.period === oldFilters.period &&
          filters.view?.planningDepartment ===
            oldFilters.view?.planningDepartment &&
          filters.view?.planningGroup === oldFilters.view?.planningGroup
        ) {
          return computeDataAction();
        }

        if (
          (filters.view && !oldFilters) ||
          (filters.view &&
            oldFilters?.view &&
            filters.view.planningGroup !== oldFilters.view.planningGroup)
        ) {
          this._store.dispatch(
            setFiltersAction({ filters: { employees: null } }),
          );
        }

        return trySubscribeDataAction();
      }),
    ),
  );

  public trySearchPlannedJobEffect = createEffect(() =>
    this._actions.pipe(
      ofType(trySearchPlannedJobAction),
      map(() => trySubscribeDataAction()),
    ),
  );

  public clearSearchEffect = createEffect(() =>
    this._actions.pipe(
      ofType(clearSearchAction),
      map(() => trySubscribeDataAction()),
    ),
  );

  fetchAppSettingsSuccessEffect = createEffect(() =>
    this._actions.pipe(
      ofType(AppSettingsActions.fetchSettingsSuccessAction),
      map(() => tryFetchSetupAction()),
    ),
  );

  constructor(
    private _actions: Actions,
    private _wsPlanning: WsPlanningService,
    private _planningService: PlanningService,
    private _planningDateHelper: PlanningDateHelperService,
    private _store: Store,
  ) {
    this._wsPlanning.onDataUpdate().subscribe((cards) => {
      this._store.dispatch(updateDataAction({ cards }));
    });
  }
}
