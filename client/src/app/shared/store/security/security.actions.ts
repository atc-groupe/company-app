import { createAction, props } from '@ngrx/store';
import { ICredentialsDto } from '../../dto';
import { STORE_KEY_SECURITY } from '../store.constants';

const key = `[${STORE_KEY_SECURITY}]`;

// ---------- Authentication ----------
export const trySignInAction = createAction(
  `${key} try sign in`,
  props<{ credentials: ICredentialsDto }>(),
);
export const signInSuccessAction = createAction(`${key} sign in success`);

export const signInErrorAction = createAction(
  `${key} sign in error`,
  props<{ err: string }>(),
);
export const setIsAuthenticatedAction = createAction(
  `${key} set is authenticated`,
  props<{ isAuthenticated: boolean }>(),
);

// ---------- Logout ----------
export const tryLogOutAction = createAction(`${key} try logout`);
export const logOutSuccess = createAction(`${key} logout success`);
