import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { SecurityService } from '../../services';
import * as SecurityActions from './security.actions';
import { UserActions } from '../user';
import { catchError, EMPTY, map, of, switchMap, tap } from 'rxjs';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';

@Injectable()
export class SecurityEffects {
  public trySignInEffect = createEffect(() =>
    this._actions.pipe(
      ofType(SecurityActions.trySignInAction),
      switchMap(({ credentials }) =>
        this._securityService.signIn(credentials).pipe(
          map(() => {
            this._router.navigateByUrl('/');
            return SecurityActions.signInSuccessAction();
          }),
          catchError((err) =>
            of(SecurityActions.signInErrorAction({ err: err.error.message })),
          ),
        ),
      ),
    ),
  );

  public logOutEffect = createEffect(() =>
    this._actions.pipe(
      ofType(SecurityActions.tryLogOutAction),
      map(() => {
        this._securityService.logOut();
        this._store.dispatch(UserActions.clearUserAction());
        this._router.navigateByUrl('/sign-in');
        return SecurityActions.logOutSuccess();
      }),
    ),
  );

  constructor(
    private _actions: Actions,
    private _securityService: SecurityService,
    private _router: Router,
    private _store: Store,
  ) {}
}
