import { createReducer, on } from '@ngrx/store';
import * as AuthActions from './security.actions';

export interface SecurityState {
  isAuthenticated: boolean | null;
  authError: string | null;
}

const INITIAL_STATE: SecurityState = {
  isAuthenticated: null,
  authError: null,
};

export const securityReducer = createReducer(
  INITIAL_STATE,

  on(AuthActions.trySignInAction, (state): SecurityState => {
    return {
      ...state,
      authError: null,
    };
  }),

  on(AuthActions.signInSuccessAction, (state): SecurityState => {
    return {
      ...state,
      isAuthenticated: true,
    };
  }),

  on(AuthActions.signInErrorAction, (state, { err }): SecurityState => {
    return {
      ...state,
      authError: err,
      isAuthenticated: false,
    };
  }),

  on(
    AuthActions.setIsAuthenticatedAction,
    (state, { isAuthenticated }): SecurityState => {
      return {
        ...state,
        isAuthenticated,
      };
    },
  ),

  on(AuthActions.logOutSuccess, (state): SecurityState => {
    return {
      ...state,
      isAuthenticated: null,
    };
  }),
);
