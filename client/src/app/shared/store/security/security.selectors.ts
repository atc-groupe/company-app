import { createFeatureSelector, createSelector } from '@ngrx/store';
import { SecurityState } from './security.reducer';
import { STORE_KEY_SECURITY } from '../store.constants';

export const selectSecurityState =
  createFeatureSelector<SecurityState>(STORE_KEY_SECURITY);

export const selectIsAuthenticated = createSelector(
  selectSecurityState,
  (state): boolean | null => state.isAuthenticated,
);

export const selectAuthError = createSelector(
  selectSecurityState,
  (state): string | null => state.authError,
);
