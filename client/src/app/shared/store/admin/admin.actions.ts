import { createAction } from '@ngrx/store';
import { STORE_KEY_ADMIN } from '../store.constants';

const key = `[${STORE_KEY_ADMIN}]`;

export const toggleNavAction = createAction(`${key} toggle nav`);
export const hideNavAction = createAction(`${key} hide nav`);
