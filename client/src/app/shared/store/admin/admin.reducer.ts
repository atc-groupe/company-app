import { createReducer, on } from '@ngrx/store';
import * as AdminActions from './admin.actions';

export interface AdminState {
  navExpanded: boolean | null;
}

const INITIAL_STATE: AdminState = {
  navExpanded: false,
};

export const adminReducer = createReducer(
  INITIAL_STATE,
  on(AdminActions.toggleNavAction, (state): AdminState => {
    return {
      ...state,
      navExpanded: !state.navExpanded,
    };
  }),

  on(AdminActions.hideNavAction, (state): AdminState => {
    return {
      ...state,
      navExpanded: false,
    };
  }),
);
