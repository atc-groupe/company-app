import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AdminState } from './admin.reducer';
import { STORE_KEY_ADMIN } from '../store.constants';

export const selectAdminState =
  createFeatureSelector<AdminState>(STORE_KEY_ADMIN);

export const selectNavExpanded = createSelector(
  selectAdminState,
  (state): boolean | null => state.navExpanded,
);
