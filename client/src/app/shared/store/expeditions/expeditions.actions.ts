import { STORE_KEY_EXPEDITIONS } from '../store.constants';
import { createAction, props } from '@ngrx/store';
import { IExpeditionLine, ISortColumn } from '../../interfaces';

const key = `[${STORE_KEY_EXPEDITIONS}]`;

export const setPrevDateAction = createAction(`${key} set prev date`);
export const setNextDateAction = createAction(`${key} set next date`);
export const resetDateAction = createAction(`${key} reset date`);

export const trySubscribeDataAction = createAction(`${key} try subscribe data`);

export const subscribeDataSuccessAction = createAction(
  `${key} subscribe data success`,
  props<{ data: IExpeditionLine[] }>(),
);

export const subscribeDataErrorAction = createAction(
  `${key} subscribe data error`,
  props<{ error: string }>(),
);

export const startSearchAction = createAction(`${key} start search`);

export const clearSearchAction = createAction(`${key} clear search`);

export const searchAction = createAction(
  `${key} search`,
  props<{ search: string }>(),
);

export const clearDataAction = createAction(`${key} clear data`);

export const setDataUpdatesAction = createAction(
  `${key} set data updates`,
  props<{ update: IExpeditionLine }>(),
);

export const sortDataAction = createAction(
  `${key} set sort column`,
  props<{ sortColumn: ISortColumn | null }>(),
);
