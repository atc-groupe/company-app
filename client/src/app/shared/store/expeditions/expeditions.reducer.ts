import { IExpeditionLine, ISortColumn } from '../../interfaces';
import { createReducer, on } from '@ngrx/store';
import {
  clearDataAction,
  clearSearchAction,
  resetDateAction,
  searchAction,
  setDataUpdatesAction,
  setNextDateAction,
  setPrevDateAction,
  sortDataAction,
  startSearchAction,
  subscribeDataErrorAction,
  subscribeDataSuccessAction,
  trySubscribeDataAction,
} from './expeditions.actions';
import { getSortedData, getUpdatedData } from './expeditions-data-helper';

export interface Expeditions2State {
  date: Date;
  data: IExpeditionLine[] | null;
  isLoading: boolean;
  fetchError: string | null;
  search: string | null;
  sortColumn: ISortColumn;
}

const INITIAL_SORT_COLUMN: ISortColumn = { index: 0, dir: 'asc' };

const INITIAL_STATE: Expeditions2State = {
  date: new Date(),
  data: null,
  isLoading: true,
  fetchError: null,
  search: null,
  sortColumn: INITIAL_SORT_COLUMN,
};

export const expeditionsReducer = createReducer(
  INITIAL_STATE,

  on(trySubscribeDataAction, (state): Expeditions2State => {
    return {
      ...state,
      data: null,
      isLoading: true,
      fetchError: null,
    };
  }),

  on(subscribeDataSuccessAction, (state, { data }): Expeditions2State => {
    return {
      ...state,
      data: getSortedData(data, state.sortColumn),
      isLoading: false,
    };
  }),

  on(subscribeDataErrorAction, (state, { error }): Expeditions2State => {
    return {
      ...state,
      isLoading: false,
      fetchError: error,
    };
  }),

  on(startSearchAction, (state): Expeditions2State => {
    return {
      ...state,
      isLoading: true,
    };
  }),

  on(searchAction, (state, { search }): Expeditions2State => {
    return {
      ...state,
      search,
    };
  }),

  on(clearSearchAction, (state): Expeditions2State => {
    return {
      ...state,
      search: null,
    };
  }),

  on(clearDataAction, (state): Expeditions2State => {
    return {
      ...state,
      search: null,
      data: null,
      isLoading: true,
      fetchError: null,
    };
  }),

  on(setDataUpdatesAction, (state, { update }): Expeditions2State => {
    if (!state.data && update.syncAction === 'remove') {
      return state;
    }

    const newData = state.data
      ? getSortedData(
          getUpdatedData(state.data, update, state.search),
          state.sortColumn,
        )
      : [update];

    return {
      ...state,
      data: newData,
    };
  }),

  on(sortDataAction, (state, { sortColumn }): Expeditions2State => {
    if (!state.data) {
      return state;
    }

    const newSortColumn = sortColumn ? sortColumn : INITIAL_SORT_COLUMN;

    return {
      ...state,
      sortColumn: newSortColumn,
      data: getSortedData(state.data, newSortColumn),
    };
  }),

  on(setPrevDateAction, (state): Expeditions2State => {
    const newDate = new Date(state.date);
    const inc = newDate.getDay() === 1 ? 3 : 1;
    newDate.setDate(newDate.getDate() - inc);

    return {
      ...state,
      date: newDate,
    };
  }),

  on(setNextDateAction, (state): Expeditions2State => {
    const newDate = new Date(state.date);
    const inc = newDate.getDay() === 5 ? 3 : 1;
    newDate.setDate(newDate.getDate() + inc);

    return {
      ...state,
      date: newDate,
    };
  }),

  on(resetDateAction, (state): Expeditions2State => {
    return {
      ...state,
      date: new Date(),
    };
  }),
);
