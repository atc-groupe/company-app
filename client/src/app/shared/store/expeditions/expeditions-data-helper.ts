import { IExpeditionLine, ISortColumn } from '../../interfaces';

export const getSortedData = (
  data: IExpeditionLine[],
  sortColumn: ISortColumn,
): IExpeditionLine[] => {
  return [...data].sort((a, b) => {
    let sortValA: number | string = 0;
    let sortValB: number | string = 0;

    switch (sortColumn.index) {
      case 1:
        sortValA = a.company;
        sortValB = b.company;
        break;
      case 2:
        sortValA = a.jobStatusNumber;
        sortValB = b.jobStatusNumber;
        break;
      case 3:
        sortValA = a.devices.reduce((acc: string, item) =>
          acc ? `${acc}, ${item}` : item,
        );
        sortValB = b.devices.reduce((acc: string, item) =>
          acc ? `${acc}, ${item}` : item,
        );
        break;
      case 9:
        const zipCodesA = getDisplayList(a.addressZipCodes);
        const zipCodesB = getDisplayList(b.addressZipCodes);

        sortValA = zipCodesA ? zipCodesA : '';
        sortValB = zipCodesB ? zipCodesB : '';
        break;
      case 10:
        const methodsA = getDisplayList(a.deliveryMethods);
        const methodsB = getDisplayList(b.deliveryMethods);

        sortValA = methodsA ? methodsA : '';
        sortValB = methodsB ? methodsB : '';
        break;
      default:
        sortValA = a.jobNumber;
        sortValB = b.jobNumber;
    }

    if (sortValA > sortValB) {
      return sortColumn?.dir === 'asc' ? 1 : -1;
    }

    if (sortValA < sortValB) {
      return sortColumn?.dir === 'asc' ? -1 : 1;
    }

    return 0;
  });
};

export const getUpdatedData = (
  data: IExpeditionLine[],
  update: IExpeditionLine,
  search: string | null,
): IExpeditionLine[] => {
  const newData = [...data];

  if (
    update.syncAction === 'remove' ||
    (update.jobStatusNumber > 710 && !search)
  ) {
    return newData.filter((line) => line.jobNumber !== update.jobNumber);
  }

  if (update.syncAction === 'update') {
    return newData.map((line) =>
      line.jobNumber === update.jobNumber ? update : line,
    );
  }

  if (update.syncAction === 'add') {
    const isInData =
      newData.findIndex((line) => line.jobNumber === update.jobNumber) !== -1;

    if (!isInData) {
      newData.push(update);
    }

    return newData;
  }

  return newData;
};

const getDisplayList = (list: string[]): string => {
  return list.reduce(
    (acc: string, item) => (acc ? `${acc}, ${item}` : item),
    '',
  );
};
