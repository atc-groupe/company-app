import { createFeatureSelector, createSelector } from '@ngrx/store';
import { STORE_KEY_EXPEDITIONS } from '../store.constants';
import { Expeditions2State } from './expeditions.reducer';
import { IExpeditionLine, ISortColumn } from '../../interfaces';

export const selectExpeditions2State = createFeatureSelector<Expeditions2State>(
  STORE_KEY_EXPEDITIONS,
);

export const selectDate = createSelector(
  selectExpeditions2State,
  (state): Date => state.date,
);

export const selectData = createSelector(
  selectExpeditions2State,
  (state): IExpeditionLine[] | null => state.data,
);

export const selectIsLoading = createSelector(
  selectExpeditions2State,
  (state): boolean => state.isLoading,
);

export const selectFetchError = createSelector(
  selectExpeditions2State,
  (state): string | null => state.fetchError,
);

export const selectSearch = createSelector(
  selectExpeditions2State,
  (state): string | null => state.search,
);

export const selectSortColumn = createSelector(
  selectExpeditions2State,
  (state): ISortColumn => state.sortColumn,
);
