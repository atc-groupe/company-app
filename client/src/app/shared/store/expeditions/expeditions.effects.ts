import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { WsExpeditionsService } from '../../services/ws-expeditions.service';
import { Store } from '@ngrx/store';
import {
  clearSearchAction,
  resetDateAction,
  searchAction,
  setDataUpdatesAction,
  setNextDateAction,
  setPrevDateAction,
  subscribeDataErrorAction,
  subscribeDataSuccessAction,
  trySubscribeDataAction,
} from './expeditions.actions';
import {
  catchError,
  debounceTime,
  map,
  of,
  switchMap,
  withLatestFrom,
} from 'rxjs';
import { selectDate, selectSearch } from './expeditions.selectors';
import { IExpeditionsQueryParamsDto } from '../../dto';

@Injectable()
export class ExpeditionsEffects {
  public trySubscribeDataEffect = createEffect(() =>
    this._actions.pipe(
      ofType(trySubscribeDataAction),
      debounceTime(300),
      withLatestFrom(
        this._store.select(selectSearch),
        this._store.select(selectDate),
      ),
      switchMap(([, search, date]) => {
        const dto: IExpeditionsQueryParamsDto = search
          ? {
              fullSearch: search,
            }
          : {
              sendingDate: date.toLocaleDateString('en-US'),
              endStatusNumber: 710,
            };

        return this._wsExpeditions2.onSubscribeData(dto).pipe(
          map((data) => subscribeDataSuccessAction({ data })),
          catchError((err) =>
            of(subscribeDataErrorAction({ error: err.message })),
          ),
        );
      }),
    ),
  );

  public changeDateEffect = createEffect(() =>
    this._actions.pipe(
      ofType(setPrevDateAction, setNextDateAction, resetDateAction),
      map(() => trySubscribeDataAction()),
    ),
  );

  public searchEffect = createEffect(() =>
    this._actions.pipe(
      ofType(searchAction, clearSearchAction),
      map(() => trySubscribeDataAction()),
    ),
  );

  constructor(
    private _actions: Actions,
    private _wsExpeditions2: WsExpeditionsService,
    private _store: Store,
  ) {
    this._wsExpeditions2.onDataUpdate().subscribe((update) => {
      this._store.dispatch(setDataUpdatesAction({ update }));
    });
  }
}
