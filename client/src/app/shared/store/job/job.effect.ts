import { Injectable } from '@angular/core';
import { JobsSyncService, WsJobsService } from '../../services';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import {
  setPlanningSetupAction,
  setUserOperationsAction,
  subscribeJobErrorAction,
  subscribeJobSuccessAction,
  syncJobErrorAction,
  syncJobSuccessAction,
  trySubscribeJobAction,
  trySyncJobAction,
  updateJobAction,
} from './job.actions';
import {
  catchError,
  debounceTime,
  EMPTY,
  map,
  of,
  switchMap,
  withLatestFrom,
} from 'rxjs';
import { selectMpNumber } from './job.selectors';
import * as PlanningSelectors from '../planning/planning.selectors';
import { selectUser } from '../user/user.selectors';

@Injectable()
export class JobEffect {
  trySubscribeJobEffect = createEffect(() =>
    this._actions.pipe(
      ofType(trySubscribeJobAction),
      debounceTime(100),
      withLatestFrom(
        this._store.select(selectMpNumber),
        this._store.select(PlanningSelectors.selectSetup),
        this._store.select(selectUser),
      ),
      switchMap(([, mpNumber, setup, user]) => {
        if (!mpNumber) {
          return EMPTY;
        }

        this._store.dispatch(setPlanningSetupAction({ setup }));
        if (user) {
          this._store.dispatch(
            setUserOperationsAction({ operations: user.mp.operations }),
          );
        }

        return this._wsJobs.onSubscribeJob(mpNumber).pipe(
          map((job) => subscribeJobSuccessAction({ job })),
          catchError((err) =>
            of(subscribeJobErrorAction({ error: err.message })),
          ),
        );
      }),
    ),
  );

  trySyncJobEffect = createEffect(() =>
    this._actions.pipe(
      ofType(trySyncJobAction),
      switchMap(({ mpNumber }) =>
        this._jobSyncService.syncOne(mpNumber).pipe(
          map(() => syncJobSuccessAction()),
          catchError((err) =>
            of(syncJobErrorAction({ error: err.error.message })),
          ),
        ),
      ),
    ),
  );

  constructor(
    private _wsJobs: WsJobsService,
    private _jobSyncService: JobsSyncService,
    private _actions: Actions,
    private _store: Store,
  ) {
    this._wsJobs.onJobUpdate().subscribe((job) => {
      this._store.dispatch(updateJobAction({ job }));
    });

    this._wsJobs.onConnectError().subscribe((err) => {
      this._store.dispatch(subscribeJobErrorAction({ error: err.message }));
    });

    this._wsJobs.onError().subscribe((err) => {
      this._store.dispatch(subscribeJobErrorAction({ error: err.message }));
    });
  }
}
