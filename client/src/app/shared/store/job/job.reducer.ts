import { createReducer, on } from '@ngrx/store';
import {
  clearJobAction,
  setFromAction,
  setPlanningSetupAction,
  setSubJobsSearchAction,
  setUserOperationsAction,
  subscribeJobErrorAction,
  subscribeJobSuccessAction,
  syncJobErrorAction,
  syncJobSuccessAction,
  toggleHideInvoiceSubJobs,
  toggleHideNoProdSubJobs,
  toggleMyPlanningCardsFilterAction,
  togglePlanningWorksheetAction,
  toggleSubJobsDeviceFilterAction,
  trySubscribeJobAction,
  trySyncJobAction,
  updateJobAction,
} from './job.actions';
import {
  IJob,
  IJobPlanningGroupCards,
  ISubJob,
  IUserMpOperation,
} from '../../interfaces';
import * as JobDataHelper from './job-data-helper';
import { TJobPlanningWorksheetsExpanded } from '../../types';
import { PlanningSetup } from '../../entities';

export interface SubJobsState {
  filteredList: ISubJob[] | null;
  devicesFilters: { [key: string]: boolean };
  hideNoProdSubJobs: boolean;
  hideInvoiceSubJobs: boolean;
  search: string | null;
}

export interface JobPlanningDataState {
  setup: PlanningSetup | null;
  worksheetsExpanded: TJobPlanningWorksheetsExpanded;
  data: IJobPlanningGroupCards[] | null;
  myCardsFilter: boolean;
  userOperations: IUserMpOperation[] | null;
}

export interface JobState {
  job: IJob | null;
  mpNumber: number | null;
  error: string | null;
  isLoading: boolean;
  subJobs: SubJobsState;
  planningData: JobPlanningDataState;
  from: string;
}

const INITIAL_SUB_JOBS_STATE: SubJobsState = {
  filteredList: null,
  devicesFilters: {},
  hideNoProdSubJobs: true,
  hideInvoiceSubJobs: true,
  search: null,
};

const INITIAL_PLANNING_DATA_STATE: JobPlanningDataState = {
  setup: null,
  worksheetsExpanded: {},
  data: null,
  myCardsFilter: false,
  userOperations: null,
};

const INITIAL_STATE: JobState = {
  job: null,
  mpNumber: null,
  error: null,
  isLoading: true,
  subJobs: INITIAL_SUB_JOBS_STATE,
  planningData: INITIAL_PLANNING_DATA_STATE,
  from: '/jobs',
};

export const jobReducer = createReducer(
  INITIAL_STATE,

  on(trySubscribeJobAction, (state, { mpNumber }): JobState => {
    return {
      ...state,
      job: null,
      subJobs: INITIAL_SUB_JOBS_STATE,
      mpNumber: mpNumber ? mpNumber : state.mpNumber,
      error: null,
      isLoading: true,
    };
  }),

  on(setPlanningSetupAction, (state, { setup }): JobState => {
    return {
      ...state,
      planningData: {
        ...state.planningData,
        setup,
      },
    };
  }),

  on(setUserOperationsAction, (state, { operations }): JobState => {
    return {
      ...state,
      planningData: {
        ...state.planningData,
        userOperations: operations,
      },
    };
  }),

  on(subscribeJobSuccessAction, (state, { job }): JobState => {
    const planningData = state.planningData.setup
      ? {
          ...state.planningData,
          data: JobDataHelper.getComputedPlanningData(
            job.planningData,
            state.planningData.setup,
            state.planningData.myCardsFilter,
            state.planningData.userOperations,
          ),
          worksheetsExpanded:
            JobDataHelper.getInitPlanningWorksheetExpanded(job),
        }
      : state.planningData;

    return {
      ...state,
      job,
      subJobs: JobDataHelper.getComputedSubJobs(job, {
        ...state.subJobs,
        devicesFilters: JobDataHelper.getInitialDeviceFilters(
          job.meta.productionTypes,
        ),
      }),
      planningData,
      error: null,
      isLoading: false,
    };
  }),

  on(subscribeJobErrorAction, (state, { error }): JobState => {
    return {
      ...state,
      error,
      job: null,
      subJobs: INITIAL_SUB_JOBS_STATE,
      isLoading: false,
    };
  }),

  on(clearJobAction, (state): JobState => {
    return {
      ...state,
      job: null,
      subJobs: INITIAL_SUB_JOBS_STATE,
      planningData: INITIAL_PLANNING_DATA_STATE,
      mpNumber: null,
    };
  }),

  on(updateJobAction, (state, { job }): JobState => {
    const planningData = state.planningData.setup
      ? {
          ...state.planningData,
          data: JobDataHelper.getComputedPlanningData(
            job.planningData,
            state.planningData.setup,
            state.planningData.myCardsFilter,
            state.planningData.userOperations,
          ),
          worksheetsExpanded:
            JobDataHelper.getComputedPlanningWorksheetExpanded(
              job,
              state.planningData.worksheetsExpanded,
            ),
        }
      : state.planningData;

    return {
      ...state,
      job,
      subJobs: JobDataHelper.getComputedSubJobs(job, {
        ...state.subJobs,
        devicesFilters: JobDataHelper.getComputedDeviceFilters(
          job.meta.productionTypes,
          state.subJobs.devicesFilters,
        ),
      }),
      planningData,
    };
  }),

  on(toggleSubJobsDeviceFilterAction, (state, { device }): JobState => {
    const devicesFilters = { ...state.subJobs.devicesFilters };
    devicesFilters[device] = !devicesFilters[device];

    return {
      ...state,
      subJobs: JobDataHelper.getComputedSubJobs(state.job, {
        ...state.subJobs,
        devicesFilters,
      }),
    };
  }),

  on(toggleHideNoProdSubJobs, (state): JobState => {
    return {
      ...state,
      subJobs: JobDataHelper.getComputedSubJobs(state.job, {
        ...state.subJobs,
        hideNoProdSubJobs: !state.subJobs.hideNoProdSubJobs,
      }),
    };
  }),

  on(toggleHideInvoiceSubJobs, (state): JobState => {
    return {
      ...state,
      subJobs: JobDataHelper.getComputedSubJobs(state.job, {
        ...state.subJobs,
        hideInvoiceSubJobs: !state.subJobs.hideInvoiceSubJobs,
      }),
    };
  }),

  on(setSubJobsSearchAction, (state, { term }): JobState => {
    return {
      ...state,
      subJobs: JobDataHelper.getComputedSubJobs(state.job, {
        ...state.subJobs,
        search: term,
      }),
    };
  }),

  on(setFromAction, (state, { route }): JobState => {
    return {
      ...state,
      from: route,
    };
  }),

  on(trySyncJobAction, (state): JobState => {
    return {
      ...state,
      error: null,
      isLoading: true,
    };
  }),

  on(syncJobSuccessAction, (state): JobState => {
    return {
      ...state,
      isLoading: false,
      error: null,
    };
  }),

  on(syncJobErrorAction, (state, { error }): JobState => {
    return {
      ...state,
      isLoading: false,
      error,
    };
  }),

  on(togglePlanningWorksheetAction, (state, { id }): JobState => {
    const worksheetsExpanded = { ...state.planningData.worksheetsExpanded };
    worksheetsExpanded[id] = !worksheetsExpanded[id];

    return {
      ...state,
      planningData: {
        ...state.planningData,
        worksheetsExpanded,
      },
    };
  }),

  on(toggleMyPlanningCardsFilterAction, (state): JobState => {
    const myCardsFilter = !state.planningData.myCardsFilter;
    const data =
      !state.job || !state.planningData.setup
        ? state.planningData.data
        : JobDataHelper.getComputedPlanningData(
            state.job.planningData,
            state.planningData.setup,
            myCardsFilter,
            state.planningData.userOperations,
          );

    return {
      ...state,
      planningData: {
        ...state.planningData,
        myCardsFilter: myCardsFilter,
        data,
      },
    };
  }),
);
