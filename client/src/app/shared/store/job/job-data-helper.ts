import { SubJobsState } from './job.reducer';
import {
  IJob,
  IJobPlanningCard,
  IJobPlanningGroupCards,
  IUserMpOperation,
} from '../../interfaces';
import { PlanningSetup } from '../../entities';

export const getComputedSubJobs = (
  job: IJob | null,
  state: SubJobsState,
): SubJobsState => {
  if (!job || !job.subJobs) {
    return state;
  }

  const subJobs = job.subJobs;
  const search = state.search ? state.search.toLowerCase() : null;

  const list = search
    ? subJobs.filter((subJob) => {
        if (subJob.reference.toLowerCase() === search) {
          return true;
        }

        if (search.length < 3) {
          return false;
        }

        return (
          subJob.mp.description.toLowerCase().includes(search.toLowerCase()) ||
          (subJob.mp.checklist.material &&
            subJob.mp.checklist.material
              .toLowerCase()
              .includes(search.toLowerCase()))
        );
      })
    : subJobs;

  const filteredList = list.filter((subJob) => {
    const isInvoiceSubJob = subJob.mp.description.match(/^(dev|dv).*/i);
    const hasNoProd = !subJob.prodLayers && !subJob.unlinkedFinishingLayers;

    if (isInvoiceSubJob) {
      if (state.hideInvoiceSubJobs && hasNoProd && !state.hideNoProdSubJobs) {
        return true;
      }

      return !state.hideInvoiceSubJobs;
    }

    if (hasNoProd) {
      if (
        state.hideNoProdSubJobs &&
        isInvoiceSubJob &&
        !state.hideInvoiceSubJobs
      ) {
        return true;
      }

      return !state.hideNoProdSubJobs;
    }

    return subJob.prodLayers?.some((layer) => {
      return state.devicesFilters[layer.numeric.deviceDisplayName];
    });
  });

  return {
    ...state,
    filteredList,
  };
};

export const getInitialDeviceFilters = (
  prodTypes: string[],
): {
  [key: string]: boolean;
} => {
  return prodTypes.reduce((acc: { [key: string]: boolean }, device) => {
    if (!acc[device]) {
      acc[device] = true;
    }

    return acc;
  }, {});
};

export const getComputedDeviceFilters = (
  prodTypes: string[],
  filters: { [key: string]: boolean },
): { [key: string]: boolean } => {
  return prodTypes.reduce((acc: { [key: string]: boolean }, device) => {
    acc[device] = filters[device] ? filters[device] : true;
    return acc;
  }, {});
};

export const getInitPlanningWorksheetExpanded = (
  job: IJob,
): { [key: string]: boolean } => {
  if (!job.worksheets) {
    return {};
  }

  return job.worksheets.reduce((acc: { [key: string]: boolean }, worksheet) => {
    acc[worksheet.id] = false;

    return acc;
  }, {});
};

export const getComputedPlanningWorksheetExpanded = (
  job: IJob,
  expanded: { [key: string]: boolean },
): { [key: string]: boolean } => {
  if (!job.worksheets) {
    return {};
  }

  return job.worksheets.reduce((acc: { [key: string]: boolean }, worksheet) => {
    acc[worksheet.id] =
      expanded[worksheet.id] !== undefined ? expanded[worksheet.id] : false;

    return acc;
  }, {});
};

export const getComputedPlanningData = (
  data: IJobPlanningCard[],
  setup: PlanningSetup,
  myOperations: boolean,
  userOperations: IUserMpOperation[] | null,
): IJobPlanningGroupCards[] => {
  let filteredCards = [...data];
  if (myOperations && userOperations) {
    filteredCards = filteredCards.filter(
      (el) =>
        userOperations.find((op) => op.label === el.operation) !== undefined,
    );
  }

  return filteredCards.reduce((acc: IJobPlanningGroupCards[], card) => {
    const group = setup.getGroupNameFromMachineId(card.machineId);

    if (!group) {
      return acc;
    }

    const accGroup = acc.find((el) => el.group === group);

    if (accGroup) {
      accGroup.cards.push(card);
    } else {
      acc.push({
        group,
        cards: [card],
      });
    }

    return acc;
  }, []);
};
