import { createFeatureSelector, createSelector } from '@ngrx/store';
import { STORE_KEY_JOB } from '../store.constants';
import { JobPlanningDataState, JobState, SubJobsState } from './job.reducer';
import { IJob, IJobPlanningGroupCards, ISubJob } from '../../interfaces';
import { TJobPlanningWorksheetsExpanded } from '../../types';

// --- JobState ---

export const selectJobState = createFeatureSelector<JobState>(STORE_KEY_JOB);

export const selectJob = createSelector(
  selectJobState,
  (state): IJob | null => state.job,
);

export const selectMpNumber = createSelector(
  selectJobState,
  (state): number | null => state.mpNumber,
);

export const selectError = createSelector(
  selectJobState,
  (state): string | null => state.error,
);

export const selectIsLoading = createSelector(
  selectJobState,
  (state): boolean => state.isLoading,
);

export const selectSubJobsState = createSelector(
  selectJobState,
  (state): SubJobsState => state.subJobs,
);

export const selectFrom = createSelector(
  selectJobState,
  (state): string | null => state.from,
);

// --- SubJobState ---

export const selectSubJobsFilteredList = createSelector(
  selectSubJobsState,
  (state): ISubJob[] | null => state.filteredList,
);

export const selectSubJobsDevicesFilters = createSelector(
  selectSubJobsState,
  (state): { [key: string]: boolean } => state.devicesFilters,
);

export const selectSubJobsHideInvoiceSubJobs = createSelector(
  selectSubJobsState,
  (state): boolean => state.hideInvoiceSubJobs,
);

export const selectSubJobsHideNoProdSubJobs = createSelector(
  selectSubJobsState,
  (state): boolean => state.hideNoProdSubJobs,
);

// --- PlanningDataState ---

export const selectJobPlanningDataState = createSelector(
  selectJobState,
  (state): JobPlanningDataState => state.planningData,
);

export const selectPlanningWorksheetsExpanded = createSelector(
  selectJobPlanningDataState,
  (state): TJobPlanningWorksheetsExpanded => state.worksheetsExpanded,
);

export const selectPlanningData = createSelector(
  selectJobPlanningDataState,
  (state): IJobPlanningGroupCards[] | null => state.data,
);

export const selectMyPlanningCardsFilter = createSelector(
  selectJobPlanningDataState,
  (state): boolean => state.myCardsFilter,
);
