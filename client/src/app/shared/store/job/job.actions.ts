import { STORE_KEY_JOB } from '../store.constants';
import { createAction, props } from '@ngrx/store';
import { IJob, IUserMpOperation } from '../../interfaces';
import { PlanningSetup } from '../../entities';

const key = `[${STORE_KEY_JOB}]`;

export const trySubscribeJobAction = createAction(
  `${key} try subscribe job`,
  props<{ mpNumber?: number }>(),
);

export const setPlanningSetupAction = createAction(
  `${key} set planning setup`,
  props<{ setup: PlanningSetup | null }>(),
);

export const subscribeJobSuccessAction = createAction(
  `${key} subscribe job success`,
  props<{ job: IJob }>(),
);

export const updateJobAction = createAction(
  `${key} update job`,
  props<{ job: IJob }>(),
);

export const subscribeJobErrorAction = createAction(
  `${key} subscribe job error`,
  props<{ error: string }>(),
);

export const clearJobAction = createAction(`${key} clear job`);

export const toggleSubJobsDeviceFilterAction = createAction(
  `${key} toggle sub jobs device filter`,
  props<{ device: string }>(),
);

export const toggleHideNoProdSubJobs = createAction(
  `${key} toggle hide no prod sub jobs`,
);

export const toggleHideInvoiceSubJobs = createAction(
  `${key} toggle hide invoice sub jobs`,
);

export const setSubJobsSearchAction = createAction(
  `${key} set sub jobs search`,
  props<{ term: string | null }>(),
);

export const setFromAction = createAction(
  `${key} set from`,
  props<{ route: string }>(),
);

export const trySyncJobAction = createAction(
  `${key} try sync job`,
  props<{ mpNumber: number }>(),
);

export const syncJobSuccessAction = createAction(`${key} sync job success`);

export const syncJobErrorAction = createAction(
  `${key} sync job error`,
  props<{ error: string }>(),
);

export const togglePlanningWorksheetAction = createAction(
  `${key} toggle planning worksheet`,
  props<{ id: string }>(),
);

export const toggleMyPlanningCardsFilterAction = createAction(
  `${key} toggle my planning operations filter`,
);

export const setUserOperationsAction = createAction(
  `${key} set user operations`,
  props<{ operations: IUserMpOperation[] }>(),
);
