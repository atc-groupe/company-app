import { IJobLine } from '../../interfaces/job/i-job-line';
import { IJobsQueryParamsDto } from '../../dto';
import { ISortColumn } from '../../interfaces';
import { createReducer, on } from '@ngrx/store';
import {
  clearDataAction,
  clearSearchAction,
  searchJobAction,
  setSortColumnAction,
  startSearchAction,
  subscribeDataErrorAction,
  subscribeDataSuccessAction,
  trySubscribeDataAction,
  updateLineAction,
} from './jobs.actions';
import { getSortedData, getUpdatedData } from './jobs-data-helper';

export interface JobsState {
  data: IJobLine[] | null;
  error: string | null;
  isLoading: boolean;
  search: string | null;
  filters: IJobsQueryParamsDto;
  sortColumn: ISortColumn;
}

const INITIAL_FILTERS: IJobsQueryParamsDto = {
  startStatusNumber: -1,
  endStatusNumber: 999,
};

const INITIAL_SORT_COLUMN: ISortColumn = { index: 0, dir: 'asc' };

const INITIAL_STATE: JobsState = {
  data: null,
  error: null,
  isLoading: false,
  search: null,
  filters: INITIAL_FILTERS,
  sortColumn: INITIAL_SORT_COLUMN,
};

export const jobsReducer = createReducer(
  INITIAL_STATE,

  on(trySubscribeDataAction, (state): JobsState => {
    return {
      ...state,
      data: null,
      error: null,
      isLoading: true,
    };
  }),

  on(subscribeDataSuccessAction, (state, { data }): JobsState => {
    return {
      ...state,
      data: getSortedData(data, state.sortColumn),
      isLoading: false,
    };
  }),

  on(subscribeDataErrorAction, (state, { error }): JobsState => {
    return {
      ...state,
      error,
      isLoading: false,
    };
  }),

  on(startSearchAction, (state): JobsState => {
    return {
      ...state,
      isLoading: true,
    };
  }),

  on(searchJobAction, (state, { search }): JobsState => {
    return {
      ...state,
      search,
      filters: { fullSearch: search },
    };
  }),

  on(clearSearchAction, (state): JobsState => {
    return {
      ...state,
      data: null,
      search: null,
      isLoading: false,
      filters: INITIAL_FILTERS,
    };
  }),

  on(clearSearchAction, (state): JobsState => {
    return {
      ...state,
      search: null,
      filters: INITIAL_FILTERS,
    };
  }),

  on(updateLineAction, (state, { line }): JobsState => {
    return {
      ...state,
      data: state.data
        ? getSortedData(getUpdatedData(state.data, line), state.sortColumn)
        : [line],
    };
  }),

  on(setSortColumnAction, (state, { sortColumn }): JobsState => {
    const newSortColumn = sortColumn ? sortColumn : INITIAL_SORT_COLUMN;

    const newData = state.data
      ? getSortedData(state.data, newSortColumn)
      : null;

    return {
      ...state,
      sortColumn: newSortColumn,
      data: newData,
    };
  }),

  on(clearDataAction, (state): JobsState => {
    return {
      ...state,
      data: null,
    };
  }),
);
