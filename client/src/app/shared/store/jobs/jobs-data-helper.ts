import { IJobLine } from '../../interfaces/job/i-job-line';
import { ISortColumn } from '../../interfaces';

export const getSortedData = (
  lines: IJobLine[],
  sortColumn: ISortColumn,
): IJobLine[] => {
  return [...lines].sort((a, b) => {
    let sortValA: number | string | Date = 0;
    let sortValB: number | string | Date = 0;

    switch (sortColumn.index) {
      case 1:
        sortValA = a.company;
        sortValB = b.company;
        break;
      case 2:
        sortValA = a.statusNumber;
        sortValB = b.statusNumber;
        break;
      case 3:
        sortValA = a.devices.reduce(
          (acc: string, item) => (acc ? `${acc}, ${item}` : item),
          '',
        );
        sortValB = b.devices.reduce(
          (acc: string, item) => (acc ? `${acc}, ${item}` : item),
          '',
        );
        break;
      case 4:
        sortValA = a.deliveryDate ? new Date(a.deliveryDate) : 0;
        sortValB = b.deliveryDate ? new Date(b.deliveryDate) : 0;
        break;
      default:
        sortValA = a.number;
        sortValB = b.number;
    }

    if (sortValA > sortValB) {
      return sortColumn?.dir === 'asc' ? 1 : -1;
    }

    if (sortValA < sortValB) {
      return sortColumn?.dir === 'asc' ? -1 : 1;
    }

    return 0;
  });
};

export const getUpdatedData = (
  lines: IJobLine[],
  update: IJobLine,
): IJobLine[] => {
  const newData = [...lines];

  switch (update.syncAction) {
    case 'add':
      const isInLines =
        newData.findIndex((item) => item.number === update.number) !== -1;

      if (!isInLines) {
        newData.push(update);
      }

      return newData;
    case 'update':
      return newData.map((line) =>
        line.number === update.number ? update : line,
      );
    default:
      return newData;
  }
};
