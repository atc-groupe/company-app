import { STORE_KEY_JOBS } from '../store.constants';
import { createAction, props } from '@ngrx/store';
import { ISortColumn } from '../../interfaces';
import { IJobLine } from '../../interfaces/job/i-job-line';

const key = `[${STORE_KEY_JOBS}]`;

export const trySubscribeDataAction = createAction(`${key} try subscribe data`);

export const subscribeDataSuccessAction = createAction(
  `${key} subscribe data success`,
  props<{ data: IJobLine[] }>(),
);

export const subscribeDataErrorAction = createAction(
  `${key} subscribe data error`,
  props<{ error: string }>(),
);

export const searchJobAction = createAction(
  `${key} search job`,
  props<{ search: string }>(),
);

export const updateLineAction = createAction(
  `${key} on update line`,
  props<{ line: IJobLine }>(),
);

export const setSortColumnAction = createAction(
  `${key} set sort column`,
  props<{ sortColumn: ISortColumn | null }>(),
);

export const startSearchAction = createAction(`${key} start search`);

export const clearSearchAction = createAction(`${key} clear search`);

export const clearDataAction = createAction(`${key} clear data`);
