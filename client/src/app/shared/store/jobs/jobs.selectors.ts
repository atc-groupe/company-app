import { createFeatureSelector, createSelector } from '@ngrx/store';
import { JobsState } from './jobs.reducer';
import { STORE_KEY_JOBS } from '../store.constants';
import { IJobLine } from '../../interfaces/job/i-job-line';
import { IJobsQueryParamsDto } from '../../dto';
import { ISortColumn } from '../../interfaces';

export const selectJobs2State =
  createFeatureSelector<JobsState>(STORE_KEY_JOBS);

export const selectData = createSelector(
  selectJobs2State,
  (state): IJobLine[] | null => state.data,
);

export const selectError = createSelector(
  selectJobs2State,
  (state): string | null => state.error,
);

export const selectIsLoading = createSelector(
  selectJobs2State,
  (state): boolean => state.isLoading,
);

export const selectSearch = createSelector(
  selectJobs2State,
  (state): string | null => state.search,
);

export const selectFilters = createSelector(
  selectJobs2State,
  (state): IJobsQueryParamsDto => state.filters,
);

export const selectSortColumn = createSelector(
  selectJobs2State,
  (state): ISortColumn => state.sortColumn,
);
