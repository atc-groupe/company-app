import { Injectable } from '@angular/core';
import { WsJobsService } from '../../services';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import {
  clearSearchAction,
  searchJobAction,
  subscribeDataErrorAction,
  subscribeDataSuccessAction,
  trySubscribeDataAction,
  updateLineAction,
} from './jobs.actions';
import { catchError, map, of, switchMap, withLatestFrom } from 'rxjs';
import { selectFilters } from './jobs.selectors';

@Injectable()
export class JobsEffects {
  public trySubscribeDataEffect = createEffect(() =>
    this._actions.pipe(
      ofType(trySubscribeDataAction),
      withLatestFrom(this._store.select(selectFilters)),
      switchMap(([, filters]) =>
        this._wsJobs.onSubscribeList(filters).pipe(
          map((data) => subscribeDataSuccessAction({ data })),
          catchError((err) =>
            of(subscribeDataErrorAction({ error: err.message })),
          ),
        ),
      ),
    ),
  );

  public searchJobEffect = createEffect(() =>
    this._actions.pipe(
      ofType(searchJobAction),
      map(() => trySubscribeDataAction()),
    ),
  );

  constructor(
    private _actions: Actions,
    private _wsJobs: WsJobsService,
    private _store: Store,
  ) {
    this._wsJobs.onListItemUpdate().subscribe((line) => {
      this._store.dispatch(updateLineAction({ line }));
    });
  }
}
