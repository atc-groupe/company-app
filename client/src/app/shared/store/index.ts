import { securityReducer, SecurityState, SecurityEffects } from './security';
import { ActionReducerMap } from '@ngrx/store';
import {
  STORE_KEY_ADMIN,
  STORE_KEY_APP_SETTINGS,
  STORE_KEY_EXPEDITIONS,
  STORE_KEY_JOB,
  STORE_KEY_JOB_MAPPED_STATUSES,
  STORE_KEY_JOBS,
  STORE_KEY_NAV,
  STORE_KEY_PLANNING,
  STORE_KEY_ROUTER,
  STORE_KEY_SECURITY,
  STORE_KEY_STOCK_PRODUCT,
  STORE_KEY_STOCK_PRODUCTS,
  STORE_KEY_STOCK_RESERVATIONS,
  STORE_KEY_UI,
  STORE_KEY_USER,
} from './store.constants';
import { userReducer, UserState, UserEffects } from './user';
import { adminReducer, AdminState } from './admin';
import { routerReducer, RouterState } from '@ngrx/router-store';
import { navbarReducer, NavbarState } from './navbar';
import { uiReducer, UiState } from './ui';
import {
  appSettingsReducer,
  AppSettingsState,
} from './app-settings/app-settings.reducer';
import { AppSettingsEffect } from './app-settings/app-settings.effect';
import { jobReducer, JobState } from './job/job.reducer';
import { JobEffect } from './job/job.effect';
import {
  stockProductsReducer,
  StockProductsState,
} from './stock/stock-products.reducer';
import { StockProductsEffects } from './stock/stock-products.effects';
import {
  stockProductReducer,
  StockProductState,
} from './stock/stock-product.reducer';
import { StockProductEffects } from './stock/stock-product.effects';
import { planningReducer, PlanningState } from './planning/planning.reducer';
import { PlanningEffects } from './planning/planning.effects';
import {
  expeditionsReducer,
  Expeditions2State,
} from './expeditions/expeditions.reducer';
import { ExpeditionsEffects } from './expeditions/expeditions.effects';
import { jobsReducer, JobsState } from './jobs/jobs.reducer';
import { JobsEffects } from './jobs/jobs.effects';
import {
  stockReservationsReducer,
  StockReservationsState,
} from './stock/stock-reservations.reducer';
import { StockReservationsEffects } from './stock/stock-reservations.effects';
import {
  jobMappedStatusesReducer,
  JobMappedStatusesState,
} from './job-mapped-statuses/job-mapped-statuses.reducer';
import { JobMappedStatusesEffects } from './job-mapped-statuses/job-mapped-statuses.effects';
import { UiEffects } from './ui/ui.effects';

export interface AppState {
  [STORE_KEY_ADMIN]: AdminState;
  [STORE_KEY_APP_SETTINGS]: AppSettingsState;
  [STORE_KEY_EXPEDITIONS]: Expeditions2State;
  [STORE_KEY_JOB]: JobState;
  [STORE_KEY_JOB_MAPPED_STATUSES]: JobMappedStatusesState;
  [STORE_KEY_JOBS]: JobsState;
  [STORE_KEY_NAV]: NavbarState;
  [STORE_KEY_PLANNING]: PlanningState;
  [STORE_KEY_SECURITY]: SecurityState;
  [STORE_KEY_STOCK_PRODUCTS]: StockProductsState;
  [STORE_KEY_STOCK_PRODUCT]: StockProductState;
  [STORE_KEY_STOCK_RESERVATIONS]: StockReservationsState;
  [STORE_KEY_UI]: UiState;
  [STORE_KEY_USER]: UserState;
  [STORE_KEY_ROUTER]: RouterState;
}

export const APP_REDUCERS: ActionReducerMap<AppState> = {
  [STORE_KEY_ADMIN]: adminReducer,
  [STORE_KEY_APP_SETTINGS]: appSettingsReducer,
  [STORE_KEY_EXPEDITIONS]: expeditionsReducer,
  [STORE_KEY_JOB]: jobReducer,
  [STORE_KEY_JOB_MAPPED_STATUSES]: jobMappedStatusesReducer,
  [STORE_KEY_JOBS]: jobsReducer,
  [STORE_KEY_NAV]: navbarReducer,
  [STORE_KEY_PLANNING]: planningReducer,
  [STORE_KEY_SECURITY]: securityReducer,
  [STORE_KEY_STOCK_PRODUCTS]: stockProductsReducer,
  [STORE_KEY_STOCK_PRODUCT]: stockProductReducer,
  [STORE_KEY_STOCK_RESERVATIONS]: stockReservationsReducer,
  [STORE_KEY_UI]: uiReducer,
  [STORE_KEY_USER]: userReducer,
  [STORE_KEY_ROUTER]: routerReducer,
};

export const APP_EFFECTS = [
  ExpeditionsEffects,
  JobEffect,
  JobMappedStatusesEffects,
  JobsEffects,
  PlanningEffects,
  SecurityEffects,
  UserEffects,
  AppSettingsEffect,
  StockProductsEffects,
  StockProductEffects,
  StockReservationsEffects,
  UiEffects,
];
