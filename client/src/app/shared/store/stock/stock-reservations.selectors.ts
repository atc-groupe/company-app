import { createFeatureSelector, createSelector } from '@ngrx/store';
import { StockReservationsState } from './stock-reservations.reducer';
import { STORE_KEY_STOCK_RESERVATIONS } from '../store.constants';
import { IJobStockReservationLine, ISortColumn } from '../../interfaces';

export const selectStockReservationsState =
  createFeatureSelector<StockReservationsState>(STORE_KEY_STOCK_RESERVATIONS);

export const selectData = createSelector(
  selectStockReservationsState,
  (state): IJobStockReservationLine[] | null => state.data,
);

export const selectIsLoading = createSelector(
  selectStockReservationsState,
  (state): boolean => state.isLoading,
);

export const selectError = createSelector(
  selectStockReservationsState,
  (state): string | null => state.error,
);

export const selectSortColumn = createSelector(
  selectStockReservationsState,
  (state): ISortColumn => state.sortColumn,
);
