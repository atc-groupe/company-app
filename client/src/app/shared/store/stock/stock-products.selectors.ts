import { createFeatureSelector, createSelector } from '@ngrx/store';
import { StockProductsState } from './stock-products.reducer';
import { STORE_KEY_STOCK_PRODUCTS } from '../store.constants';
import { IProduct } from '../../interfaces';
import { ProductTypeEnum } from '../../enums';

export const selectStockState = createFeatureSelector<StockProductsState>(
  STORE_KEY_STOCK_PRODUCTS,
);

export const selectProducts = createSelector(
  selectStockState,
  (state): IProduct[] | null => state.filteredProducts,
);

export const selectError = createSelector(
  selectStockState,
  (state): string | null => state.error,
);

export const selectProductFilter = createSelector(
  selectStockState,
  (state): string | null => state.productFilter,
);

export const selectIsLoading = createSelector(
  selectStockState,
  (state): boolean => state.isLoading,
);

export const selectProductType = createSelector(
  selectStockState,
  (state): ProductTypeEnum | null => state.productType,
);

export const selectProductGroupType = createSelector(
  selectStockState,
  (state): string | null => state.productGroupType,
);
