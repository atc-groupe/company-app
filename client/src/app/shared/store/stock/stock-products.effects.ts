import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { WsStockProductsService } from '../../services';
import {
  fetchProductsSuccessAction,
  onProductsErrorAction,
  tryFetchProductsAction,
  updateProductAction,
} from './stock-products.actions';
import { catchError, debounceTime, map, of, switchMap } from 'rxjs';
import { Store } from '@ngrx/store';

@Injectable()
export class StockProductsEffects {
  public tryFetchProductsEffect$ = createEffect(() =>
    this._actions.pipe(
      ofType(tryFetchProductsAction),
      debounceTime(300),
      switchMap(({ productType, groupType }) =>
        this._wsStockProducts.onSubscribeProducts(productType, groupType).pipe(
          map((products) => fetchProductsSuccessAction({ products })),
          catchError((err) =>
            of(onProductsErrorAction({ error: err.message })),
          ),
        ),
      ),
    ),
  );

  constructor(
    private _actions: Actions,
    private _wsStockProducts: WsStockProductsService,
    private _store: Store,
  ) {
    this._wsStockProducts.onProductChange().subscribe((change) => {
      this._store.dispatch(updateProductAction({ change }));
    });
  }
}
