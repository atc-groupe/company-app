import { Article, Paper } from '../../entities';
import { ProductTypeEnum } from '../../enums';
import { IProduct } from '../../interfaces';

export function getFilteredProducts(
  productType: ProductTypeEnum,
  products: IProduct[],
  search: string,
): IProduct[] {
  switch (productType) {
    case ProductTypeEnum.Article:
      return getFilteredArticles(products as Article[], search);
    case ProductTypeEnum.Paper:
      return getFilteredPapers(products as Paper[], search);
    case ProductTypeEnum.CustomerProduct:
      throw new Error('Unsupported product type');
  }
}

function getFilteredArticles(list: Article[], search: string): Article[] {
  return list.filter(
    (paper) =>
      paper.name.toLowerCase().includes(search.toLowerCase()) ||
      paper.category.toLowerCase().includes(search.toLowerCase()),
  );
}

function getFilteredPapers(list: Paper[], search: string): Paper[] {
  return list.filter(
    (paper) =>
      paper.name.toLowerCase().includes(search.toLowerCase()) ||
      paper.category.toLowerCase().includes(search.toLowerCase()) ||
      paper.width.toString().includes(search.toLowerCase()) ||
      paper.height.toString().includes(search.toLowerCase()),
  );
}
