import { STORE_KEY_STOCK_RESERVATIONS } from '../store.constants';
import { createAction, props } from '@ngrx/store';
import { IJobStockReservationLine, ISortColumn } from '../../interfaces';

const key = `[${STORE_KEY_STOCK_RESERVATIONS}]`;

export const trySubscribeDataAction = createAction(`${key} try subscribe data`);
export const subscribeDataSuccessAction = createAction(
  `${key} subscribe data success`,
  props<{ data: IJobStockReservationLine[] }>(),
);
export const subscribeDataErrorAction = createAction(
  `${key} subscribe data error`,
  props<{ error: string }>(),
);
export const updateDataItemAction = createAction(
  `${key} update data item`,
  props<{ update: IJobStockReservationLine }>(),
);
export const sortDataAction = createAction(
  `${key} sort data`,
  props<{ sortColumn: ISortColumn | null }>(),
);

export const clearDataAction = createAction(`${key} clear data`);
