import { createAction, props } from '@ngrx/store';
import { STORE_KEY_STOCK_PRODUCTS } from '../store.constants';
import { TProductGroupType } from '../../types';
import { ProductTypeEnum } from '../../enums';
import { IProduct, IProductChange } from '../../interfaces';

const key = `[${STORE_KEY_STOCK_PRODUCTS}]`;

export const tryFetchProductsAction = createAction(
  `${key} try fetch products`,
  props<{ productType: ProductTypeEnum; groupType: TProductGroupType }>(),
);

export const fetchProductsSuccessAction = createAction(
  `${key} fetch products success action`,
  props<{ products: IProduct[] }>(),
);

export const onProductsErrorAction = createAction(
  `${key} fetch product error`,
  props<{ error: string }>(),
);

export const updateProductAction = createAction(
  `${key} update product`,
  props<{ change: IProductChange }>(),
);

export const searchProductAction = createAction(
  `${key} search product`,
  props<{ term: string }>(),
);

export const clearSearchProductAction = createAction(
  `${key} clear search product`,
);

export const clearStateAction = createAction(`${key} clear state`);
