import { createReducer, on } from '@ngrx/store';
import {
  fetchProductSuccessAction,
  productErrorAction,
  setLastMutation,
  tryFetchProductAction,
  unsetLastMutation,
  updateProductAction,
} from './stock-product.actions';
import { ProductTypeEnum } from '../../enums';
import { IProduct, IStockMutationInfo } from '../../interfaces';

export interface StockProductState {
  productType: ProductTypeEnum | null;
  product: IProduct | null;
  error: string | null;
  isLoading: boolean;
  lastMutation: IStockMutationInfo | null;
}

const INITIAL_STATE: StockProductState = {
  productType: null,
  product: null,
  error: null,
  isLoading: false,
  lastMutation: null,
};

export const stockProductReducer = createReducer(
  INITIAL_STATE,

  on(tryFetchProductAction, (state, { productType }): StockProductState => {
    return {
      ...state,
      productType,
      product: null,
      isLoading: true,
      error: null,
    };
  }),

  on(fetchProductSuccessAction, (state, { product }): StockProductState => {
    return {
      ...state,
      isLoading: false,
      product,
    };
  }),

  on(productErrorAction, (state, { error }): StockProductState => {
    return {
      ...state,
      isLoading: false,
      error,
    };
  }),

  on(updateProductAction, (state, { change }): StockProductState => {
    if (change.action === 'create' || state.productType === null) {
      return state;
    }

    if (change.action === 'delete') {
      return {
        ...state,
        product: null,
        error: 'Le produit a été supprimé',
      };
    }

    return {
      ...state,
      product: change.product,
    };
  }),

  on(setLastMutation, (state, { mutationInfo }): StockProductState => {
    return {
      ...state,
      lastMutation: mutationInfo,
    };
  }),

  on(unsetLastMutation, (state): StockProductState => {
    return {
      ...state,
      lastMutation: null,
    };
  }),
);
