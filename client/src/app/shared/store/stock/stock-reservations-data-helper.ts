import { IJobStockReservationLine, ISortColumn } from '../../interfaces';

export const getUpdatedData = (
  lines: IJobStockReservationLine[],
  update: IJobStockReservationLine,
): IJobStockReservationLine[] => {
  const newLines = [...lines];
  const lineIndex = newLines.findIndex(
    (line) => line.jobNumber === update.jobNumber,
  );
  const isInLines = lineIndex !== -1;

  if (update.syncAction === 'add' && !isInLines) {
    newLines.push(update);

    return newLines;
  }

  if (update.syncAction === 'update') {
    return newLines.map((line) =>
      line.jobNumber === update.jobNumber ? update : line,
    );
  }

  if (update.syncAction === 'remove') {
    return newLines.filter((line) => line.jobNumber !== update.jobNumber);
  }

  return newLines;
};

export const getSortedData = (
  lines: IJobStockReservationLine[],
  sortColumn: ISortColumn,
): IJobStockReservationLine[] => {
  return [...lines].sort((a, b) => {
    let sortValA: number | string = 0;
    let sortValB: number | string = 0;

    switch (sortColumn.index) {
      case 1:
        sortValA = a.jobStatusNumber;
        sortValB = b.jobStatusNumber;
        break;
      case 2:
        sortValA = a.company;
        sortValB = b.company;
        break;
      case 3:
        sortValA = a.sendingDate ? a.sendingDate : '';
        sortValB = b.sendingDate ? b.sendingDate : '';
        break;
      case 4:
        sortValA = a.itemsCount;
        sortValB = b.itemsCount;
        break;
      case 5:
        sortValA = a.globalStatus;
        sortValB = b.globalStatus;
        break;
      default:
        sortValA = a.jobNumber;
        sortValB = b.jobNumber;
    }

    if (sortValA > sortValB) {
      return sortColumn?.dir === 'asc' ? 1 : -1;
    }

    if (sortValA < sortValB) {
      return sortColumn?.dir === 'asc' ? -1 : 1;
    }

    return 0;
  });
};
