import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { WsStockProductService } from '../../services';
import {
  fetchProductSuccessAction,
  productErrorAction,
  tryFetchProductAction,
  updateProductAction,
} from './stock-product.actions';
import { catchError, debounceTime, map, of, switchMap } from 'rxjs';
import { Store } from '@ngrx/store';

@Injectable()
export class StockProductEffects {
  public tryFetchProductEffect = createEffect(() =>
    this._actions.pipe(
      ofType(tryFetchProductAction),
      debounceTime(300),
      switchMap(({ productType, id }) =>
        this._wsProductService.onSubscribeProduct(productType, id).pipe(
          map((product) => fetchProductSuccessAction({ product })),
          catchError((err) => of(productErrorAction({ error: err.message }))),
        ),
      ),
    ),
  );

  constructor(
    private _actions: Actions,
    private _wsProductService: WsStockProductService,
    private _store: Store,
  ) {
    this._wsProductService.onProductChange().subscribe((change) => {
      this._store.dispatch(updateProductAction({ change }));
    });
  }
}
