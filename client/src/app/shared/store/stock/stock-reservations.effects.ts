import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { WSJobsStockReservationsService } from '../../services';
import { Store } from '@ngrx/store';
import {
  subscribeDataErrorAction,
  subscribeDataSuccessAction,
  trySubscribeDataAction,
  updateDataItemAction,
} from './stock-reservations.actions';
import { catchError, map, of, switchMap } from 'rxjs';

@Injectable()
export class StockReservationsEffects {
  public trySubscribeDataEffect = createEffect(() =>
    this._actions.pipe(
      ofType(trySubscribeDataAction),
      switchMap(() =>
        this._wsReservations.onSubscribeList().pipe(
          map((data) => subscribeDataSuccessAction({ data })),
          catchError((error) =>
            of(subscribeDataErrorAction({ error: error.message })),
          ),
        ),
      ),
    ),
  );

  constructor(
    private _actions: Actions,
    private _wsReservations: WSJobsStockReservationsService,
    private _store: Store,
  ) {
    this._wsReservations.onListItemUpdate().subscribe((update) => {
      this._store.dispatch(updateDataItemAction({ update }));
    });
  }
}
