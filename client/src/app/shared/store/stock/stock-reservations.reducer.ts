import { IJobStockReservationLine, ISortColumn } from '../../interfaces';
import { createReducer, on } from '@ngrx/store';
import {
  clearDataAction,
  sortDataAction,
  subscribeDataErrorAction,
  subscribeDataSuccessAction,
  trySubscribeDataAction,
  updateDataItemAction,
} from './stock-reservations.actions';
import {
  getSortedData,
  getUpdatedData,
} from './stock-reservations-data-helper';

export interface StockReservationsState {
  data: IJobStockReservationLine[] | null;
  isLoading: boolean;
  error: string | null;
  sortColumn: ISortColumn;
}

const INITIAL_SORT_COLUMN: ISortColumn = { index: 0, dir: 'asc' };

const INITIAL_STATE: StockReservationsState = {
  data: null,
  isLoading: false,
  error: null,
  sortColumn: INITIAL_SORT_COLUMN,
};

export const stockReservationsReducer = createReducer(
  INITIAL_STATE,

  on(trySubscribeDataAction, (state): StockReservationsState => {
    return {
      ...state,
      data: null,
      error: null,
      isLoading: true,
    };
  }),

  on(subscribeDataSuccessAction, (state, { data }): StockReservationsState => {
    return {
      ...state,
      data: getSortedData(data, state.sortColumn),
      error: null,
      isLoading: false,
    };
  }),

  on(subscribeDataErrorAction, (state, { error }): StockReservationsState => {
    return {
      ...state,
      error,
      isLoading: false,
    };
  }),

  on(updateDataItemAction, (state, { update }): StockReservationsState => {
    if (!state.data) {
      return {
        ...state,
        data: update.syncAction === 'add' ? [update] : null,
      };
    }

    return {
      ...state,
      data: getSortedData(getUpdatedData(state.data, update), state.sortColumn),
    };
  }),

  on(sortDataAction, (state, { sortColumn }): StockReservationsState => {
    if (!state.data) {
      return {
        ...state,
      };
    }

    const newSortColumn = sortColumn ? sortColumn : INITIAL_SORT_COLUMN;
    return {
      ...state,
      sortColumn: newSortColumn,
      data: getSortedData(state.data, newSortColumn),
      isLoading: false,
    };
  }),

  on(clearDataAction, (state): StockReservationsState => {
    return {
      ...state,
      data: null,
    };
  }),
);
