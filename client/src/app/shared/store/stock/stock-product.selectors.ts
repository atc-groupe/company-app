import { createFeatureSelector, createSelector } from '@ngrx/store';
import { StockProductState } from './stock-product.reducer';
import { STORE_KEY_STOCK_PRODUCT } from '../store.constants';
import { IProduct, IStockMutationInfo } from '../../interfaces';
import { ProductTypeEnum } from '../../enums';

export const selectStockState = createFeatureSelector<StockProductState>(
  STORE_KEY_STOCK_PRODUCT,
);

export const selectProductType = createSelector(
  selectStockState,
  (state: StockProductState): ProductTypeEnum | null => state.productType,
);

export const selectProduct = createSelector(
  selectStockState,
  (state: StockProductState): IProduct | null => state.product,
);

export const selectError = createSelector(
  selectStockState,
  (state: StockProductState): string | null => state.error,
);

export const selectIsLoading = createSelector(
  selectStockState,
  (state: StockProductState): boolean => state.isLoading,
);

export const selectLastMutation = createSelector(
  selectStockState,
  (state: StockProductState): IStockMutationInfo | null => state.lastMutation,
);
