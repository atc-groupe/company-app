import { STORE_KEY_STOCK_PRODUCT } from '../store.constants';
import { createAction, props } from '@ngrx/store';
import { ProductTypeEnum } from '../../enums';
import { IProduct, IProductChange, IStockMutationInfo } from '../../interfaces';

const key = `[${STORE_KEY_STOCK_PRODUCT}]`;

export const tryFetchProductAction = createAction(
  `${key} try fetch product`,
  props<{ productType: ProductTypeEnum; id: number }>(),
);

export const fetchProductSuccessAction = createAction(
  `${key} fetch product success`,
  props<{ product: IProduct }>(),
);

export const productErrorAction = createAction(
  `${key} fetch product error`,
  props<{ error: string }>(),
);

export const updateProductAction = createAction(
  `${key} update product`,
  props<{ change: IProductChange }>(),
);

export const setLastMutation = createAction(
  `${key} set product last mutation`,
  props<{ mutationInfo: IStockMutationInfo }>(),
);

export const unsetLastMutation = createAction(
  `${key} unset product last mutation`,
);
