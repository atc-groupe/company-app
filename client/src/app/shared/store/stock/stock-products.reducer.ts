import { createReducer, on } from '@ngrx/store';
import { TProductGroupType } from '../../types';
import {
  clearSearchProductAction,
  clearStateAction,
  fetchProductsSuccessAction,
  onProductsErrorAction,
  searchProductAction,
  tryFetchProductsAction,
  updateProductAction,
} from './stock-products.actions';
import { getFilteredProducts } from './stock-products-filter';
import { IProduct } from '../../interfaces';
import { ProductTypeEnum } from '../../enums';

export interface StockProductsState {
  productType: ProductTypeEnum | null;
  productGroupType: TProductGroupType | null;
  products: IProduct[] | null;
  filteredProducts: IProduct[] | null;
  productFilter: string | null;
  error: string | null;
  isLoading: boolean;
}

const INITIAL_STATE: StockProductsState = {
  productType: null,
  productGroupType: null,
  products: null,
  filteredProducts: null,
  productFilter: null,
  error: null,
  isLoading: false,
};

export const stockProductsReducer = createReducer(
  INITIAL_STATE,

  on(
    tryFetchProductsAction,
    (state, { productType, groupType }): StockProductsState => {
      const productFilter =
        state.productType !== productType ||
        state.productGroupType !== groupType
          ? null
          : state.productFilter;

      return {
        ...state,
        productType,
        productGroupType: groupType,
        products: null,
        filteredProducts: null,
        productFilter,
        error: null,
        isLoading: true,
      };
    },
  ),

  on(fetchProductsSuccessAction, (state, { products }): StockProductsState => {
    const filteredProducts = state.productFilter
      ? getFilteredProducts(state.productType!, products, state.productFilter)
      : products;

    return {
      ...state,
      products,
      filteredProducts,
      isLoading: false,
    };
  }),

  on(onProductsErrorAction, (state, { error }): StockProductsState => {
    return {
      ...state,
      error: error,
      products: null,
      filteredProducts: null,
      isLoading: false,
    };
  }),

  on(updateProductAction, (state, { change }): StockProductsState => {
    if (!state.products) {
      return state;
    }

    let products: IProduct[] = [...state.products];
    switch (change.action) {
      case 'create':
        products.push(change.product);
        break;
      case 'update':
        products = products.map((item) => {
          return item.id === change.product.id ? change.product : item;
        });
        break;
      case 'delete':
        products = products.filter((item) => item.id !== change.product.id);
    }
    return {
      ...state,
      products,
      filteredProducts: state.productFilter
        ? getFilteredProducts(state.productType!, products, state.productFilter)
        : products,
    };
  }),

  on(searchProductAction, (state, { term }): StockProductsState => {
    return {
      ...state,
      productFilter: term,
      filteredProducts: state.products
        ? getFilteredProducts(state.productType!, state.products, term)
        : null,
    };
  }),

  on(clearSearchProductAction, (state): StockProductsState => {
    return {
      ...state,
      productFilter: null,
      filteredProducts: state.products,
    };
  }),

  on(clearStateAction, (): StockProductsState => {
    return INITIAL_STATE;
  }),
);
