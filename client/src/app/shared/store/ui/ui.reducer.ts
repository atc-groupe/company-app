import { TMedia, TUiMode } from '../../types';
import { createReducer, on } from '@ngrx/store';
import {
  ErrorNotification,
  InfoNotification,
  Notification,
  SuccessNotification,
} from '../../classes';
import {
  displayErrorNotificationAction,
  displayInfoNotificationAction,
  displayNotificationAction,
  displaySuccessNotificationAction,
  displayWarningNotificationAction,
  removeNotificationAction,
  setMediaAction,
  toggleUiModeAction,
} from './ui.actions';
import { WarningNotification } from '../../classes/warning-notification';

export interface UiState {
  mode: TUiMode;
  notification: Notification | null;
  clientWidth: number;
  media: TMedia | null;
}

const INITIAL_STATE: UiState = {
  mode: 'dark',
  notification: null,
  clientWidth: 0,
  media: null,
};

export const uiReducer = createReducer(
  INITIAL_STATE,

  on(toggleUiModeAction, (state): UiState => {
    return {
      ...state,
      mode: state.mode === 'dark' ? 'light' : 'dark',
    };
  }),

  on(displayNotificationAction, (state, { notification }): UiState => {
    return {
      ...state,
      notification,
    };
  }),

  on(displaySuccessNotificationAction, (state, { message }): UiState => {
    return {
      ...state,
      notification: new SuccessNotification(message),
    };
  }),

  on(displayInfoNotificationAction, (state, { message }): UiState => {
    return {
      ...state,
      notification: new InfoNotification(message),
    };
  }),

  on(displayWarningNotificationAction, (state, { message }): UiState => {
    return {
      ...state,
      notification: new WarningNotification(message),
    };
  }),

  on(displayErrorNotificationAction, (state, { message }): UiState => {
    return {
      ...state,
      notification: new ErrorNotification(message),
    };
  }),

  on(removeNotificationAction, (state): UiState => {
    return {
      ...state,
      notification: null,
    };
  }),

  on(setMediaAction, (state, { media }): UiState => {
    return {
      ...state,
      media,
    };
  }),
);
