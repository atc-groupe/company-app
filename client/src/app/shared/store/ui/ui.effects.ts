import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { signInSuccessAction } from '../security/security.actions';
import { removeNotificationAction } from './ui.actions';
import { map } from 'rxjs';

@Injectable()
export class UiEffects {
  signInSuccessEffect = createEffect(() =>
    this._actions.pipe(
      ofType(signInSuccessAction),
      map(() => removeNotificationAction()),
    ),
  );

  constructor(private _actions: Actions) {}
}
