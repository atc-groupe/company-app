import { createAction, props } from '@ngrx/store';
import { STORE_KEY_UI } from '../store.constants';
import { TMedia, TUiMode } from '../../types';
import { Notification } from '../../classes';

const key = `[${STORE_KEY_UI}]`;

export const toggleUiModeAction = createAction(
  `${key} set ui mode`,
  props<{ uiMode: TUiMode }>(),
);

export const displayNotificationAction = createAction(
  `${key} display notification`,
  props<{ notification: Notification }>(),
);

export const displaySuccessNotificationAction = createAction(
  `${key} display success notification`,
  props<{ message: string }>(),
);

export const displayInfoNotificationAction = createAction(
  `${key} display info notification`,
  props<{ message: string }>(),
);

export const displayWarningNotificationAction = createAction(
  `${key} display warning notification`,
  props<{ message: string }>(),
);

export const displayErrorNotificationAction = createAction(
  `${key} display error notification`,
  props<{ message: string }>(),
);

export const removeNotificationAction = createAction(
  `${key} remove notification`,
);

export const setClientWidthAction = createAction(
  `${key} set client width`,
  props<{ clientWidth: number }>(),
);

export const setMediaAction = createAction(
  `${key} set client width`,
  props<{ media: TMedia }>(),
);
