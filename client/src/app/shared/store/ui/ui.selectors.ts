import { createFeatureSelector, createSelector } from '@ngrx/store';
import { UiState } from './ui.reducer';
import { STORE_KEY_UI } from '../store.constants';
import { TMedia, TUiMode } from '../../types';
import { Notification } from '../../classes';

export const selectUiState = createFeatureSelector<UiState>(STORE_KEY_UI);
export const selectUiMode = createSelector(
  selectUiState,
  (state): TUiMode => state.mode,
);

export const selectUiNotification = createSelector(
  selectUiState,
  (state): Notification | null => state.notification,
);

export const selectClientWidth = createSelector(
  selectUiState,
  (state): number => state.clientWidth,
);

export const selectMedia = createSelector(
  selectUiState,
  (state): TMedia | null => state.media,
);
