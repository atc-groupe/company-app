import { createFeatureSelector, createSelector } from '@ngrx/store';
import { STORE_KEY_JOB_MAPPED_STATUSES } from '../store.constants';
import { JobMappedStatusesState } from './job-mapped-statuses.reducer';
import { IJobMappedStatus } from '../../interfaces';

export const selectJobMappedStatusesState =
  createFeatureSelector<JobMappedStatusesState>(STORE_KEY_JOB_MAPPED_STATUSES);

export const selectMappedStatus = (number: number) =>
  createSelector(
    selectJobMappedStatusesState,
    (state): IJobMappedStatus | null => {
      const undefinedStatus = {
        statusNumber: number,
        bgColor: 'red',
        textColor: 'black',
        label: 'Undefined',
      };

      if (!state.statuses) {
        return null;
      }

      const status = state.statuses.find(
        (item) => item.statusNumber === number,
      );

      return status ? status : undefinedStatus;
    },
  );

export const selectMappedStatuses = createSelector(
  selectJobMappedStatusesState,
  (state): IJobMappedStatus[] | null => state.statuses,
);

export const selectFetchError = createSelector(
  selectJobMappedStatusesState,
  (state): string | null => state.fetchError,
);
