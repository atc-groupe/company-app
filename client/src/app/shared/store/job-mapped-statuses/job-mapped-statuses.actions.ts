import { createAction, props } from '@ngrx/store';
import { STORE_KEY_JOB_MAPPED_STATUSES } from '../store.constants';
import { IJobMappedStatus } from '../../interfaces';

const key = `[${STORE_KEY_JOB_MAPPED_STATUSES}]`;

export const tryFetchJobMappedStatuses = createAction(
  `${key} try fetch job mapped statuses`,
);

export const fetchJobMappedStatusesSuccess = createAction(
  `${key} fetch job mapped statuses success`,
  props<{ statuses: IJobMappedStatus[] }>(),
);

export const fetchJobMappedStatusesError = createAction(
  `${key} fetch job mapped statuses error`,
  props<{ error: string }>(),
);
