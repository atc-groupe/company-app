import { IJobMappedStatus } from '../../interfaces';
import { createReducer, on } from '@ngrx/store';
import {
  fetchJobMappedStatusesError,
  fetchJobMappedStatusesSuccess,
  tryFetchJobMappedStatuses,
} from './job-mapped-statuses.actions';

export interface JobMappedStatusesState {
  statuses: IJobMappedStatus[] | null;
  fetchError: string | null;
}

const INITIAL_STATE: JobMappedStatusesState = {
  statuses: null,
  fetchError: null,
};

export const jobMappedStatusesReducer = createReducer(
  INITIAL_STATE,

  on(tryFetchJobMappedStatuses, (state): JobMappedStatusesState => {
    return {
      ...state,
      statuses: null,
      fetchError: null,
    };
  }),

  on(
    fetchJobMappedStatusesSuccess,
    (state, { statuses }): JobMappedStatusesState => {
      return {
        ...state,
        statuses,
      };
    },
  ),

  on(
    fetchJobMappedStatusesError,
    (state, { error }): JobMappedStatusesState => {
      return {
        ...state,
        fetchError: error,
      };
    },
  ),
);
