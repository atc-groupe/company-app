import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { JobsService } from '../../services';
import {
  fetchJobMappedStatusesError,
  fetchJobMappedStatusesSuccess,
  tryFetchJobMappedStatuses,
} from './job-mapped-statuses.actions';
import { catchError, map, of, switchMap } from 'rxjs';
import * as AppSettingsActions from '../app-settings/app-settings.actions';

@Injectable()
export class JobMappedStatusesEffects {
  public tryFetchJobMappedStatuses = createEffect(() =>
    this._actions.pipe(
      ofType(tryFetchJobMappedStatuses),
      switchMap(() =>
        this._jobsService.getMappedStatusesList().pipe(
          map((statuses) => fetchJobMappedStatusesSuccess({ statuses })),
          catchError((err) =>
            of(fetchJobMappedStatusesError({ error: err.error.message })),
          ),
        ),
      ),
    ),
  );

  public fetchAppSettingsSuccessEffect = createEffect(() =>
    this._actions.pipe(
      ofType(AppSettingsActions.fetchSettingsSuccessAction),
      map(() => tryFetchJobMappedStatuses()),
    ),
  );

  constructor(
    private _actions: Actions,
    private _jobsService: JobsService,
  ) {}
}
