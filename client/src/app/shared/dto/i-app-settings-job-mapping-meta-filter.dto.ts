export interface IAppSettingsJobMappingMetaFilterDto {
  name: string;
  value: string;
}
