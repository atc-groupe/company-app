export interface IJobStatusChangeDto {
  statusNumber: number;
  reason: string;
}
