import { JobStockReservationStatusEnum } from '../enums';

export interface IJobStockReservationStatusDto {
  from: JobStockReservationStatusEnum;
  to: JobStockReservationStatusEnum;
}
