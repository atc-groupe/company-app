import { IUserSettings } from '../interfaces';

export interface IUserSettingsUpdateDto extends Partial<IUserSettings> {}
