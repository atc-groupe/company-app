export interface ISettingsJobStatusChangeRuleStatusDto {
  statusText: string;
  statusNumber: number;
}
