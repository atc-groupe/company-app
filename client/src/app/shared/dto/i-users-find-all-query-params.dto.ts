export class IUsersFindAllQueryParamsDto {
  roleId?: string;
  isActive?: boolean;
}
