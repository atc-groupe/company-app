import { PlanningDepartmentEnum } from '../enums';

export interface IPlanningDataQueryParamsDto {
  department: PlanningDepartmentEnum;
  group: string;
  startDate?: string;
  endDate?: string;
  term?: string;
}
