export interface IAppSettingsJobStatusDto {
  statusNumber: number;
  label: string;
  bgColor: string;
  textColor: 'white' | 'black';
}
