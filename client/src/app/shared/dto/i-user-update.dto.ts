export interface IUserUpdateDto {
  userPrincipalName?: string;
  isAdmin?: boolean;
  isActive?: boolean;
  externalAccess?: boolean;
}
