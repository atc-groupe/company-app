export interface IAppFileDto {
  name: string;
  description?: string;
  file: File;
}
