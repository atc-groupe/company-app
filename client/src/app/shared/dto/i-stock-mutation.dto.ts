export interface IStockMutationDto {
  employee_number: number;
  data: {
    job_number?: number;
    description: string;
    subtract: number;
    add: number;
  };
}
