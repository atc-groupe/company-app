export interface IUserCreateDto {
  employeeNumber: number;
  userPrincipalName: string;
  isAdmin: boolean;
  externalAccess: boolean;
}
