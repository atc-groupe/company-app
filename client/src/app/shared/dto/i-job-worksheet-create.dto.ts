export interface IJobWorksheetCreateDto {
  job_number: number;
  date: string;
  employee_number: number;
  operation_number: number;
  start_time: string;
  operation_ready: boolean;
  stop_time?: string;
  time_production?: number;
  time_customer?: number;
  total_time?: number;
  remark?: string;
  count_runs?: number;
}
