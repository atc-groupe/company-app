export interface ICredentialsDto {
  identifier: string;
  password: string;
}
