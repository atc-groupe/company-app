export interface IJobsQueryParamsDto {
  sendingDate?: string; // Format m/dd/yyyy
  startSendingDate?: string; // Format m/dd/yyyy
  endSendingDate?: string; // Format m/dd/yyyy
  statusNumber?: number;
  statusNumberList?: number[];
  startStatusNumber?: number;
  endStatusNumber?: number;
  jobEndNumber?: string;
  fullSearch?: string; // Job number, customer name, description
  reSync?: boolean; // synchronize the jobs before retrieve
  withStockReservation?: boolean;
}
