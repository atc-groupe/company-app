export interface ISettingsPlanningViewDto {
  name: string;
  planningDepartment: number;
  planningGroup: string;
}
