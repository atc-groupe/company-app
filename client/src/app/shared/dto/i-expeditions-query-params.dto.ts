export class IExpeditionsQueryParamsDto {
  sendingDate?: string;
  endStatusNumber?: number;
  fullSearch?: string;
}
