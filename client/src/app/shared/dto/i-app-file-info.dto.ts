export interface IAppFileInfoDto {
  name: string;
  description?: string | null;
}
