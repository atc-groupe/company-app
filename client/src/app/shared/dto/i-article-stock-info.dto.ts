export interface IArticleStockInfoDto {
  stockUnitNumber: number | null;
  stockUnitLabel: string | null;
}
