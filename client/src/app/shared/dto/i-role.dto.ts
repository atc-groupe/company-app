export interface IRoleDto {
  label: string;
  description: string;
}
