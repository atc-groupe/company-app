export interface IAppSettingsJobSyncDto {
  syncInterval?: number;
}
