export interface ISettingsJobStatusChangeRuleOperationDto {
  operation: string;
  operationNumber: number;
}
