import { LogContextEnum, LogLevelEnum } from '../enums';

export interface ILogsQueryParamsDto {
  context: LogContextEnum;
  level?: LogLevelEnum;
}
