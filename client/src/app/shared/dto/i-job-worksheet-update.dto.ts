export interface IJobWorksheetUpdateDto {
  operation: string;
  date?: string;
  start_time?: string;
  stop_time?: string;
  time_production?: number;
  time_customer?: number;
  operation_number?: number;
  remark?: string;
  count_runs?: number;
  operation_ready?: boolean;
}
