import { JobStatusChangeRuleTypeEnum } from '../enums';

export interface ISettingsJobStatusChangeRuleDto {
  type: JobStatusChangeRuleTypeEnum;
  fromStatusText: string;
  fromStatusNumber: number;
  canCancel: boolean;
}
