export interface IJobCommentPlanningCreateDto {
  comment: string;
  userId: string;
  userInitials: string;
  planningGroup: string;
}
