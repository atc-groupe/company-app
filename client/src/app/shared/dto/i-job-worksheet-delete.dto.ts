export interface IJobWorksheetDeleteDto {
  operation: string;
  worksheetId: string;
}
