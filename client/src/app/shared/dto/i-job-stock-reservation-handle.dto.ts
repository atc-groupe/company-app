export interface IJobStockReservationHandleDto {
  paperId: number;
  quantity: number;
}
