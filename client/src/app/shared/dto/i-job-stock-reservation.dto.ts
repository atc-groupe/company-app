import { JobStockReservationStatusEnum } from '../enums';

export interface IJobStockReservationDto {
  status: JobStockReservationStatusEnum;
  paperGenericName?: string | null;
  paperName?: string | null;
  paper?: number | null;
  width?: number;
  height?: number;
  imperativeFormat: boolean;
  quantity: number;
  printSides: string;
  destination: string;
  comment?: string;
  subtractQuantity?: number;
  mutationDate?: Date;
  handleUsername?: string;
}
