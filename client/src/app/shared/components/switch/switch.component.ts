import { Component, forwardRef, Input, ViewEncapsulation } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-switch',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './switch.component.html',
  styleUrls: ['./switch.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => SwitchComponent),
    },
  ],
})
export class SwitchComponent implements ControlValueAccessor {
  @Input() public size: 'large' | 'small' = 'small';
  public innerValue: boolean = false;
  public isDisabled = false;
  private onChange!: any;
  private onTouched!: any;

  updateValue() {
    if (this.isDisabled) {
      return;
    }
    this.innerValue = !this.innerValue;
    this.onTouched();
    this.onChange(this.innerValue);
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }

  writeValue(value: any): void {
    this.innerValue = value;
  }
}
