import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobDevicesComponent } from './job-devices.component';

describe('JobDevicesComponent', () => {
  let component: JobDevicesComponent;
  let fixture: ComponentFixture<JobDevicesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [JobDevicesComponent]
    });
    fixture = TestBed.createComponent(JobDevicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
