import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'job-devices',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './job-devices.component.html',
  styleUrls: ['./job-devices.component.scss'],
})
export class JobDevicesComponent {
  @Input() devices: string[] = [];
  @Input() isPlanned: boolean | null = null;

  get setOutlineStyle(): boolean {
    return this.isPlanned === false;
  }
}
