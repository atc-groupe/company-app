import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-cancel-button',
  standalone: true,
  imports: [],
  template: '<button class="btn" type="button">{{ label }}</button>',
})
export class CancelButtonComponent {
  @Input() label = 'Annuler';
}
