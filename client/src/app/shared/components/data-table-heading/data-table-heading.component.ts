import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IDataTableColumn, ISortColumn } from '../../interfaces';

@Component({
  selector: 'data-table-heading',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './data-table-heading.component.html',
  styleUrls: ['./data-table-heading.component.scss'],
})
export class DataTableHeadingComponent {
  @Input() columns: IDataTableColumn[] | null = null;
  @Input() sortColumn: ISortColumn | null = null;
  @Input() columnsStyle: string | null = null;

  @Output() sort = new EventEmitter<ISortColumn | null>();

  public get headingColumnSortIcon(): string | null {
    if (!this.sortColumn) {
      return null;
    }

    return this.sortColumn.dir === 'asc' ? 'arrow_upward' : 'arrow_downward';
  }

  public displaySortSymbol(index: number): boolean {
    if (!this.sortColumn) {
      return false;
    }

    return this.sortColumn.index === index;
  }

  public onSortData(index: number): void {
    if (!this.columns || !this.columns[index].sortable) {
      return;
    }

    if (!this.sortColumn) {
      this.sortColumn = { index, dir: 'asc' };
    } else {
      if (this.sortColumn.index === index) {
        switch (this.sortColumn.dir) {
          case 'asc':
            this.sortColumn.dir = 'desc';
            break;
          case 'desc':
            this.sortColumn = null;
            break;
        }
      } else {
        this.sortColumn = { index, dir: 'asc' };
      }
    }

    this.sort.emit(this.sortColumn);
  }
}
