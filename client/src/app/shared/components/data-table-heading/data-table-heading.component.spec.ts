import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataTableHeadingComponent } from './data-table-heading.component';

describe('DataTableHeadingComponent', () => {
  let component: DataTableHeadingComponent;
  let fixture: ComponentFixture<DataTableHeadingComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [DataTableHeadingComponent]
    });
    fixture = TestBed.createComponent(DataTableHeadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
