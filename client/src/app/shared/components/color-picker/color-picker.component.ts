import { Component, forwardRef, Input, ViewEncapsulation } from '@angular/core';
import { CommonModule, NgClass } from '@angular/common';
import {
  ControlValueAccessor,
  FormsModule,
  NG_VALUE_ACCESSOR,
} from '@angular/forms';
import { ClickOutsideDirective } from '../../directives/click-outside.directive';

@Component({
  selector: 'app-color-picker',
  standalone: true,
  imports: [CommonModule, NgClass, FormsModule, ClickOutsideDirective],
  templateUrl: './color-picker.component.html',
  styleUrls: ['./color-picker.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => ColorPickerComponent),
    },
  ],
})
export class ColorPickerComponent implements ControlValueAccessor {
  @Input() id: string | null = null;
  @Input() class: string | null = null;
  @Input() type: 'classic' | 'text-editor' = 'classic';
  @Input() icon: string = 'format_color_text';
  public selected = false;

  public innerValue: string = '';
  public isDisabled = false;
  private onChange!: any;
  private onTouched!: any;

  onSelect(): void {
    this.selected = true;
  }

  onClickOutside() {
    this.selected = false;
  }

  updateValue() {
    if (this.isDisabled) {
      return;
    }

    this.onChange(this.innerValue);
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }

  writeValue(value: any): void {
    this.innerValue = value;
  }
}
