import { Component, Input } from '@angular/core';
import { NgClass } from '@angular/common';

@Component({
  selector: 'app-message',
  standalone: true,
  imports: [NgClass],
  template: `
    @if (message) {
      <p [ngClass]="['message', messageClass]">
        {{ message }}<ng-content></ng-content>
      </p>
    }
  `,
})
export class MessageComponent {
  @Input() message: string | null = null;
  @Input() type: 'info' | 'warning' | 'error' = 'info';

  get messageClass(): string {
    return `message-${this.type}`;
  }
}
