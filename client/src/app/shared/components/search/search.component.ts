import {
  AfterViewInit,
  Component,
  ElementRef,
  forwardRef,
  Input,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  ControlValueAccessor,
  FormsModule,
  NG_VALUE_ACCESSOR,
} from '@angular/forms';

@Component({
  selector: 'app-search',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => SearchComponent),
    },
  ],
})
export class SearchComponent implements ControlValueAccessor, AfterViewInit {
  @Input() public class: string | string[] | null = null;
  @Input() public placeholder: string | null = null;
  @Input() public size: 'large' | 'small' = 'small';
  @Input() public autofocus = false;
  @ViewChild('input') private input?: ElementRef<HTMLInputElement>;
  public innerValue = '';
  public isDisabled = false;
  private onChange!: any;
  private onTouched!: any;

  ngAfterViewInit(): void {
    if (this.autofocus) {
      this.input?.nativeElement.focus();
    }
  }

  updateValue() {
    if (this.isDisabled) {
      return;
    }
    this.onTouched(true);
    this.onChange(this.innerValue);
  }

  public clearValue() {
    this.innerValue = '';
    this.input?.nativeElement.focus();
    this.onChange(this.innerValue);
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }

  writeValue(value: any): void {
    this.innerValue = value;
  }
}
