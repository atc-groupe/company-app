import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataHeadingComponent } from './data-heading.component';

describe('DataHeadingComponent', () => {
  let component: DataHeadingComponent;
  let fixture: ComponentFixture<DataHeadingComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [DataHeadingComponent]
    });
    fixture = TestBed.createComponent(DataHeadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
