import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IDataColumn, ISortColumn } from '../../interfaces';

@Component({
  selector: 'app-data-heading',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './data-heading.component.html',
  styleUrls: ['./data-heading.component.scss'],
})
export class DataHeadingComponent {
  @Input() columns: IDataColumn[] = [];
  @Input() public sortColumn: ISortColumn | null = null;
  @Output() sort = new EventEmitter<ISortColumn | null>();

  public get headingColumnSortIcon(): string | null {
    if (!this.sortColumn) {
      return null;
    }

    return this.sortColumn.dir === 'asc' ? 'arrow_upward' : 'arrow_downward';
  }

  public displaySortSymbol(index: number): boolean {
    if (!this.sortColumn) {
      return false;
    }

    return this.sortColumn.index === index;
  }

  public onSortData(index: number): void {
    if (this.columns[index].notSortable) {
      return;
    }

    let newSortColumn: ISortColumn | null;

    if (!this.sortColumn) {
      newSortColumn = { index, dir: 'asc' };
    } else {
      newSortColumn = { ...this.sortColumn };

      if (this.sortColumn.index === index) {
        switch (newSortColumn.dir) {
          case 'asc':
            newSortColumn.dir = 'desc';
            break;
          case 'desc':
            newSortColumn = null;
            break;
        }
      } else {
        newSortColumn = { index, dir: 'asc' };
      }
    }

    this.sortColumn = newSortColumn;
    this.sort.emit(newSortColumn);
  }
}
