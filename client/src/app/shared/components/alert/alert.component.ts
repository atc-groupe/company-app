import { Component, Input, ViewEncapsulation } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClickOutsideDirective } from '../../directives/click-outside.directive';
import { TElemStyle } from '../../types';
import { AlertService } from '../../services';

@Component({
  selector: 'app-confirm',
  standalone: true,
  imports: [CommonModule, ClickOutsideDirective],
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AlertComponent {
  @Input() message!: string;
  @Input() title!: string;
  @Input() style: TElemStyle = 'std';
  @Input() confirmLabel: string = 'Valider';
  @Input() declineLabel: string = 'Annuler';
  @Input() canClickOutside: boolean = true;
  @Input() canCancel: boolean = true;

  constructor(private _alertService: AlertService) {}

  public onClickOutside() {
    if (this.canClickOutside) {
      this._alertService.dismiss();
    }
  }

  public onConfirm(): void {
    this._alertService.confirm();
  }

  public onDecline(): void {
    this._alertService.decline();
  }

  get confirmButtonStyle() {
    return this.style === 'std' ? 'btn-primary' : `btn-${this.style}`;
  }
}
