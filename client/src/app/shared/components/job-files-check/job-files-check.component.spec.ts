import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobFilesCheckComponent } from './job-files-check.component';

describe('JobFilesCheckComponent', () => {
  let component: JobFilesCheckComponent;
  let fixture: ComponentFixture<JobFilesCheckComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [JobFilesCheckComponent]
    });
    fixture = TestBed.createComponent(JobFilesCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
