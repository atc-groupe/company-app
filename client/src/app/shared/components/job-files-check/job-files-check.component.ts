import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'job-files-check',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './job-files-check.component.html',
  styleUrls: ['./job-files-check.component.scss'],
})
export class JobFilesCheckComponent {
  @Input({ required: true }) statusIndex: 1 | 2 | 3 = 1;
}
