import { Component, Input } from '@angular/core';
import { TitleCasePipe } from '@angular/common';

@Component({
  selector: 'app-save-button',
  standalone: true,
  imports: [TitleCasePipe],
  template:
    '<button class="btn btn-primary" type="submit">{{ label }}</button>',
})
export class SaveButtonComponent {
  @Input() label: string = 'Enregistrer';
}
