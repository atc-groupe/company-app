import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-empty-message',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './empty-message.component.html',
  styleUrls: ['./empty-message.component.scss'],
})
export class EmptyMessageComponent {
  @Input() text: string | null = null;
}
