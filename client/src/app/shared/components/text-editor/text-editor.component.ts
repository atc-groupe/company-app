import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Editor } from '@tiptap/core';
import { StarterKit } from '@tiptap/starter-kit';
import { SelectComponent } from '../select/select.component';
import { Underline } from '@tiptap/extension-underline';
import { TaskList } from '@tiptap/extension-task-list';
import { TaskItem } from '@tiptap/extension-task-item';
import { TextStyle } from '@tiptap/extension-text-style';
import { Color } from '@tiptap/extension-color';
import { TextAlign } from '@tiptap/extension-text-align';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Highlight } from '@tiptap/extension-highlight';

@Component({
  selector: 'app-text-editor',
  standalone: true,
  imports: [CommonModule, SelectComponent],
  templateUrl: './text-editor.component.html',
  styleUrls: ['./text-editor.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => TextEditorComponent),
    },
  ],
})
export class TextEditorComponent
  implements ControlValueAccessor, AfterViewInit
{
  @Input() autofocus = true;
  @Input() spellCheck = false;
  @Output() valueChange = new EventEmitter<string>();
  @ViewChild('editorElem') public editorElem?: ElementRef<HTMLDivElement>;

  public disabled = false;
  private onChange!: any;
  private onTouched!: any;
  private _editor: Editor | null = null;

  ngAfterViewInit() {
    this._editor = new Editor({
      element: this.editorElem?.nativeElement,
      extensions: [
        StarterKit,
        Underline,
        TaskList.configure({
          HTMLAttributes: { class: 'task-list' },
        }),
        TaskItem.configure({
          HTMLAttributes: { class: 'task-item' },
        }),
        TextStyle,
        Color,
        TextAlign.configure({ types: ['paragraph', 'heading'] }),
        Highlight,
      ],
      content: '',
      autofocus: this.autofocus ? 'end' : false,
      editable: !this.disabled,
      injectCSS: false,
      onUpdate: ({ editor }) => {
        this.updateContent(editor.getJSON());
      },
    });
  }

  public setParagraph(): void {
    this._editor?.chain().setParagraph().focus().run();
  }

  public toggleHeading(level: 1 | 2 | 3 | 4): void {
    this._editor?.chain().toggleHeading({ level }).focus().run();
  }

  public toggleBold(): void {
    this._editor?.chain().toggleBold().focus().run();
  }

  public toggleItalic(): void {
    this._editor?.chain().toggleItalic().focus().run();
  }

  public toggleUnderlined(): void {
    this._editor?.chain().toggleUnderline().focus().run();
  }

  public toggleHighlight(): void {
    this._editor?.chain().toggleHighlight().focus().run();
  }

  public toggleBulletList(): void {
    this._editor?.chain().toggleBulletList().focus().run();
  }

  public toggleOrderedList(): void {
    this._editor?.chain().toggleOrderedList().focus().run();
  }

  public toggleTaskList(): void {
    this._editor?.chain().toggleTaskList().focus().run();
  }

  public setHorizontalRule(): void {
    this._editor?.chain().setHorizontalRule().focus().run();
  }

  public setBlockQuote(): void {
    this._editor?.chain().setBlockquote().focus().run();
  }

  public unsetBlockQuote(): void {
    this._editor?.chain().unsetBlockquote().focus().run();
  }

  public setTextAlign(align: 'left' | 'center' | 'right' | 'justify'): void {
    this._editor?.chain().setTextAlign(align).focus().run();
  }

  public undo(): void {
    this._editor?.commands.undo();
  }

  public redo(): void {
    this._editor?.commands.redo();
  }

  updateContent(content: any) {
    if (this.disabled) {
      return;
    }

    this.onChange(this._editor?.isEmpty ? null : content);
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  writeValue(data: any): void {
    setTimeout(() => {
      this._editor?.commands.setContent(data, false);
    });
  }
}
