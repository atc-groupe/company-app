import {
  Component,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { IJobMappedStatus } from '../../interfaces';
import { Store } from '@ngrx/store';
import * as StatusSelectors from '../../store/job-mapped-statuses/job-mapped-statuses.selectors';
import { first } from 'rxjs';

@Component({
  selector: 'job-status',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './job-status.component.html',
  styleUrls: ['./job-status.component.scss'],
})
export class JobStatusComponent implements OnInit, OnChanges {
  @Input({ required: true }) statusNumber!: number;
  @Input() isPlanned: boolean | null = null;
  @Input() public size: 'small' | 'medium' | 'large' = 'medium';

  public mappedStatus: IJobMappedStatus | null = null;

  constructor(private _store: Store) {}

  ngOnInit() {
    this._setMappedStatus();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['statusNumber'].isFirstChange()) {
      return;
    }

    this._setMappedStatus();
  }

  get label(): string | null {
    if (!this.mappedStatus) {
      return null;
    }

    return this.isNotPlanned ? 'Non planifié' : this.mappedStatus.label;
  }

  get color(): string {
    if (this._displayStatusColors()) {
      return this.mappedStatus
        ? this.mappedStatus.textColor
        : 'var(--color-text)';
    }

    return 'var(--color-text)';
  }

  get bgColor(): string {
    if (this._displayStatusColors()) {
      return this.mappedStatus
        ? this.mappedStatus.bgColor
        : 'var(--color-gray-30)';
    }

    return 'var(--color-bg)';
  }

  get isNotPlanned(): boolean {
    return this.isPlanned === false;
  }

  private _displayStatusColors(): boolean {
    return this.isPlanned === null || this.isPlanned;
  }

  private _setMappedStatus(): void {
    this._store
      .select(StatusSelectors.selectMappedStatus(this.statusNumber))
      .pipe(first((v) => v !== null))
      .subscribe((mappedStatus) => (this.mappedStatus = mappedStatus));
  }
}
