import {
  AfterViewInit,
  Component,
  ElementRef,
  forwardRef,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { ISelectItem } from '../../interfaces';
import {
  ControlValueAccessor,
  FormsModule,
  NG_VALUE_ACCESSOR,
} from '@angular/forms';
import { ClickOutsideDirective } from '../../directives/click-outside.directive';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-select',
  standalone: true,
  imports: [CommonModule, ClickOutsideDirective, FormsModule],
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => SelectComponent),
    },
  ],
})
export class SelectComponent
  implements ControlValueAccessor, OnInit, AfterViewInit, OnDestroy
{
  @Input() public items: ISelectItem[] | Observable<ISelectItem[]> = [];
  @Input() public searchbar = false;
  @Input() public placeholder: string | null = null;
  @Input() public canClearValue = false;
  @Input() public size: 'large' | 'small' = 'small';
  @ViewChild('searchInput') public searchInput?: ElementRef<HTMLInputElement>;
  @ViewChild('listElem') public listElem?: ElementRef<HTMLUListElement>;

  public filteredList: ISelectItem[] = [];
  public selectedItem: ISelectItem | null = null;
  public opened = false;
  public search: string | null = null;

  public selected = false;
  public isDisabled = false;

  private _subscription = new Subscription();
  private list: ISelectItem[] = [];
  private selectedValue: any;

  private onChange!: any;
  private onTouched!: any;

  ngOnInit() {
    if (this.items instanceof Observable) {
      this._subscription.add(
        this.items.subscribe((items) => {
          this.list = items;
          this.filteredList = items;
          this._setSelectedItem();
        }),
      );
    } else {
      this.list = this.items;
      this.filteredList = this.items;
      this._setSelectedItem();
    }
  }

  ngAfterViewInit() {}

  public onToggle(): void {
    if (this.isDisabled) {
      return;
    }

    this.selected = true;
    this.opened = !this.opened;

    if (this.opened && this.searchbar) {
      setTimeout(() => this.searchInput?.nativeElement.focus(), 10);
    }

    if (this.opened && this.selectedItem) {
      const index =
        this.filteredList.findIndex(
          (item) => item.value === this.selectedItem!.value,
        ) - (this.searchbar ? 3 : 4);

      setTimeout(() => {
        this.listElem!.nativeElement.scrollTop = index * 25.5;
      }, 0);
    }
  }

  updateValue(item: ISelectItem) {
    if (this.isDisabled) {
      return;
    }

    this.selectedItem = item;
    this.opened = false;
    this._resetSearch();

    this.onChange(this.selectedItem.value);
  }

  onClearValue() {
    this.selectedValue = null;
    this.selectedItem = null;
    this.onTouched();
    this.onChange(null);
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }

  writeValue(value: any): void {
    this.selectedValue = value;

    if (this.list.length) {
      this._setSelectedItem();
    }
  }

  public onClickOutside() {
    this.selected = false;

    if (!this.opened) {
      return;
    }

    this.opened = false;
    this.onTouched(true);
  }

  public onSearch() {
    if (!this.search) {
      this.filteredList = this.list;
      return;
    }

    this.filteredList = this.list.filter((item) => {
      return item.label.toLowerCase().search(this.search!.toLowerCase()) !== -1;
    });
  }

  public clearSearch($event: any) {
    $event.stopPropagation();
    this.searchInput?.nativeElement.focus();
    this._resetSearch();
  }

  get displayValue(): string | null {
    if (this.selectedItem) {
      return this.selectedItem.label;
    }

    if (!this.selectedItem && this.placeholder) {
      return this.placeholder;
    }

    return null;
  }

  get isPlaceholder(): boolean {
    return !this.selectedItem && !!this.placeholder;
  }

  get displaySearchClearIcon(): boolean {
    return !!this.search;
  }

  get displayClearValue(): boolean {
    return this.canClearValue && !!this.selectedItem;
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  private _setSelectedItem() {
    if (this.selectedValue === undefined || !this.list) {
      return;
    }

    const selectedItem = this.list.find(
      (item) => item.value === this.selectedValue,
    );
    this.selectedItem = selectedItem ? selectedItem : null;
  }

  private _resetSearch() {
    this.search = null;
    this.filteredList = this.list;
  }
}
