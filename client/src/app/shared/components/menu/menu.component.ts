import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IMenuItem } from '../../interfaces';
import { ClickOutsideDirective } from '../../directives/click-outside.directive';

@Component({
  selector: 'app-menu',
  standalone: true,
  imports: [CommonModule, ClickOutsideDirective],
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent {
  public expanded = false;

  @Input() items: IMenuItem[] | null = null;
  @Input() xDir: 'left' | 'right' = 'left';
  @Input() yDir: 'top' | 'bottom' = 'bottom';

  @Output() selected = new EventEmitter<string>();

  public toggle(): void {
    this.expanded = !this.expanded;
  }

  public close(): void {
    this.expanded = false;
  }

  public triggerMenu(item: IMenuItem): void {
    if (item.disabled) {
      return;
    }

    this.expanded = false;
    this.selected.emit(item.action);
  }
}
