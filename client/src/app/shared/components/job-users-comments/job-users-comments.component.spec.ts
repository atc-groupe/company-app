import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobUsersCommentsComponent } from './job-users-comments.component';

describe('JobUsersCommentsComponent', () => {
  let component: JobUsersCommentsComponent;
  let fixture: ComponentFixture<JobUsersCommentsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [JobUsersCommentsComponent]
    });
    fixture = TestBed.createComponent(JobUsersCommentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
