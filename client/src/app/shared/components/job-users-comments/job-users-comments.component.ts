import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IJobCommentPlanning } from '../../interfaces';

@Component({
  selector: 'job-users-comments',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './job-users-comments.component.html',
  styleUrls: ['./job-users-comments.component.scss'],
})
export class JobUsersCommentsComponent {
  @Input() comments: IJobCommentPlanning[] | null = null;

  get displayComments(): boolean {
    return this.comments ? this.comments.length > 0 : false;
  }

  get lastComment(): string | null {
    if (!this.comments || this.comments.length) {
      return null;
    }

    return this.comments[this.comments.length - 1].comment;
  }
}
