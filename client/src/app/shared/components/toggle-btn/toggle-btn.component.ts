import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-toggle-btn',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './toggle-btn.component.html',
  styleUrls: ['./toggle-btn.component.scss'],
})
export class ToggleBtnComponent {
  @Input() checked = false;
  @Input() type:
    | 'primary'
    | 'info'
    | 'success'
    | 'warning'
    | 'error'
    | 'secondary' = 'secondary';
  @Input() label: string | null = null;
  @Input() size: 'sm' | 'lg' = 'lg';
  @Input() disabled = false;
  @Output() toggle = new EventEmitter<boolean>();

  public toggleChecked(): void {
    if (!this.disabled) {
      this.checked = !this.checked;
      this.toggle.emit(this.checked);
    }
  }
}
