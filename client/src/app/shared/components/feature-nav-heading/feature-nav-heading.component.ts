import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { INavItem } from '../../interfaces';
import { first, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import {
  selectNavbarItems,
  setNavbarItemsAction,
  toggleNavbarAction,
} from '../../store/navbar';
import { RouterLink } from '@angular/router';
import { AppModuleEnum } from '../../enums';

@Component({
  selector: 'app-feature-nav-heading',
  standalone: true,
  imports: [CommonModule, RouterLink],
  templateUrl: './feature-nav-heading.component.html',
  styleUrls: ['./feature-nav-heading.component.scss'],
})
export class FeatureNavHeadingComponent implements OnInit, OnDestroy {
  @Input({ required: true }) module!: AppModuleEnum;

  public navItems: INavItem[] | null = null;
  private _subscription = new Subscription();

  constructor(private _store: Store) {}

  ngOnInit() {
    this._subscription.add(
      this._store.select(selectNavbarItems).subscribe((items) => {
        this.navItems = items;
      }),
    );
  }

  public onToggle() {
    this._store.dispatch(toggleNavbarAction());
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
    this._store.dispatch(setNavbarItemsAction({ items: [] }));
  }
}
