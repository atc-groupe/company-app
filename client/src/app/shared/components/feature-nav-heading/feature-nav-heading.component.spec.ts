import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FeatureNavHeadingComponent } from './feature-nav-heading.component';

describe('FeatureNavHeadingComponent', () => {
  let component: FeatureNavHeadingComponent;
  let fixture: ComponentFixture<FeatureNavHeadingComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FeatureNavHeadingComponent]
    });
    fixture = TestBed.createComponent(FeatureNavHeadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
