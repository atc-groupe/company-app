import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, tap } from 'rxjs';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { clearUserAction } from '../store/user/user.actions';
import { SecurityService } from '../services';

@Injectable()
export class SecurityInterceptor implements HttpInterceptor {
  constructor(
    private _router: Router,
    private _store: Store,
    private _securityService: SecurityService,
  ) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler,
  ): Observable<HttpEvent<unknown>> {
    const req = this._securityService.token
      ? request.clone({
          headers: request.headers.set(
            'authorization',
            `Bearer ${this._securityService.token}`,
          ),
        })
      : request;
    return next.handle(req).pipe(
      tap({
        error: (err) => {
          if (err.error.statusCode === 401 || err.status === 504) {
            this._store.dispatch(clearUserAction());
            this._router.navigateByUrl('/sign-in');
          }
        },
      }),
    );
  }
}
