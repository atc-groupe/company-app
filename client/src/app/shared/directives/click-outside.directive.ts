import {
  Directive,
  ElementRef,
  EventEmitter,
  HostListener,
  Output,
} from '@angular/core';

@Directive({
  selector: '[clickOutside]',
  standalone: true,
})
export class ClickOutsideDirective {
  @Output() clickOutside = new EventEmitter<void>();

  @HostListener('document:click', ['$event.target']) public onClick(
    target: HTMLElement,
  ) {
    const clickInside = this.el.nativeElement.contains(target);

    const isModalToggleButton = target.classList.contains(
      'modal-toggle-button',
    );

    const isToggleMenuButton = target.closest('.toggle-button');

    if (!clickInside && !isToggleMenuButton && !isModalToggleButton) {
      this.clickOutside.emit();
    }
  }

  constructor(private el: ElementRef) {}
}
