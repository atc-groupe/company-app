import { Directive, HostListener } from '@angular/core';
import { ModalService } from '../services';

@Directive({
  selector: '[modalClose]',
  standalone: true,
})
export class ModalCloseDirective {
  constructor(private _modalService: ModalService) {}

  @HostListener('click') onClick() {
    this._modalService.close();
  }
}
