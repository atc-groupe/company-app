import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { TMedia } from '../types';
import { Store } from '@ngrx/store';
import { selectMedia } from '../store/ui';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { filter, first, map } from 'rxjs';

@Directive({
  selector: '[appShow]',
  standalone: true,
})
export class ShowDirective {
  private _media: TMedia | null = null;
  private _medias: TMedia[] = ['xs', 'sm', 'md', 'lg', 'xl', 'xxl'];
  private _isVisible: boolean | null = null;

  @Input() set appShow(media: TMedia) {
    this._media = media;
    this._store
      .select(selectMedia)
      .pipe(first((v) => v !== null))
      .subscribe((newMedia) => {
        this._handleView(this._shouldShow(newMedia!));
      });
  }

  constructor(
    private _templateRef: TemplateRef<HTMLDivElement>,
    private _viewContainerRef: ViewContainerRef,
    private _store: Store,
  ) {
    this._store
      .select(selectMedia)
      .pipe(
        takeUntilDestroyed(),
        filter((v) => v !== null && this._media !== null),
        map((media) => this._shouldShow(media!)),
        filter((shouldShow) => shouldShow !== this._isVisible),
      )
      .subscribe((shouldShow) => this._handleView(shouldShow));
  }

  private _shouldShow(media: TMedia): boolean {
    const index = this._medias.findIndex((el) => el === this._media);
    const newIndex = this._medias.findIndex((el) => el === media);

    return newIndex >= index;
  }

  private _handleView(shouldShow: boolean): void {
    if (shouldShow) {
      this._viewContainerRef.createEmbeddedView(this._templateRef);
      this._isVisible = true;
    } else {
      this._viewContainerRef.clear();
      this._isVisible = false;
    }
  }
}
