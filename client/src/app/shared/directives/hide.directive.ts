import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { TMedia } from '../types';
import { Store } from '@ngrx/store';
import { selectMedia } from '../store/ui';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { filter, first, map } from 'rxjs';

@Directive({
  selector: '[appHide]',
  standalone: true,
})
export class HideDirective {
  private _media: TMedia | null = null;
  private _medias: TMedia[] = ['xs', 'sm', 'md', 'lg', 'xl', 'xxl'];
  private _isHidden: boolean | null = null;

  @Input() set appHide(media: TMedia) {
    this._media = media;
    this._store
      .select(selectMedia)
      .pipe(first((v) => v !== null))
      .subscribe((newMedia) => {
        this._handleView(this._shouldHide(newMedia!));
      });
  }

  constructor(
    private _templateRef: TemplateRef<HTMLDivElement>,
    private _viewContainerRef: ViewContainerRef,
    private _store: Store,
  ) {
    this._store
      .select(selectMedia)
      .pipe(
        takeUntilDestroyed(),
        filter((v) => v !== null && this._media !== null),
        map((media) => this._shouldHide(media!)),
        filter((shouldHide) => shouldHide !== this._isHidden),
      )
      .subscribe((shouldHide) => this._handleView(shouldHide));
  }

  private _shouldHide(newMedia: TMedia): boolean {
    const index = this._medias.findIndex((el) => el === this._media);
    const newIndex = this._medias.findIndex((el) => el === newMedia);

    return newIndex >= index;
  }

  private _handleView(shouldHide: boolean): void {
    if (shouldHide) {
      this._viewContainerRef.clear();
      this._isHidden = true;
    } else {
      this._viewContainerRef.createEmbeddedView(this._templateRef);
      this._isHidden = false;
    }
  }
}
