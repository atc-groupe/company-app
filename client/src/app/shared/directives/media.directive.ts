import { Directive, ElementRef, HostListener } from '@angular/core';
import { Store } from '@ngrx/store';
import { setMediaAction } from '../store/ui';
import { TMedia } from '../types';

@Directive({
  selector: '[appMedia]',
  standalone: true,
})
export class MediaDirective {
  private _activeMedia: TMedia | null = null;

  @HostListener('window:resize') onResize() {
    const newMedia = this._getMedia(this._elRef.nativeElement.clientWidth);

    if (newMedia !== this._activeMedia) {
      this._activeMedia = newMedia;
      this._store.dispatch(setMediaAction({ media: this._activeMedia }));
    }
  }

  constructor(
    private _elRef: ElementRef<HTMLDivElement>,
    private _store: Store,
  ) {
    setTimeout(() => {
      this._activeMedia = this._getMedia(this._elRef.nativeElement.clientWidth);

      this._store.dispatch(setMediaAction({ media: this._activeMedia }));
    }, 0);
  }

  private _getMedia(width: number): TMedia {
    if (width < 640) {
      return 'xs';
    }

    if (width < 768) {
      return 'sm';
    }

    if (width < 1024) {
      return 'md';
    }

    if (width < 1280) {
      return 'lg';
    }

    if (width < 1536) {
      return 'xl';
    }

    return 'xxl';
  }
}
