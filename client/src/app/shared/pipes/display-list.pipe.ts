import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'appDisplayList',
  standalone: true,
})
export class DisplayListPipe implements PipeTransform {
  transform(value: string[] | null, separator: string = ', '): string | null {
    if (!value || value.length === 0) {
      return null;
    }

    return value.reduce((acc: string, method) => {
      return acc.length ? `${acc}${separator}${method}` : method;
    }, '');
  }
}
