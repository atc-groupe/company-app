import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mpListItem',
  standalone: true,
})
export class MpListItemPipe implements PipeTransform {
  transform(value: string): string {
    return value.replaceAll(/[0-9]+\. ?/g, '').replaceAll(';', ',');
  }
}
