import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'planningTime',
  standalone: true,
})
export class PlanningTimePipe implements PipeTransform {
  transform(value: number | null): string | null {
    if (value === null) {
      return null;
    }

    const hours = Math.floor(value);
    const minutes = Math.round((value - hours) * 60);

    if (hours && minutes) {
      return `${hours}h${minutes}`;
    }

    if (hours) {
      return `${hours}h`;
    }

    return `${minutes}mn`;
  }
}
