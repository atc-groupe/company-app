import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mpEmployee',
  standalone: true,
})
export class MpEmployeePipe implements PipeTransform {
  transform(completeName: string, type: 'initials' | 'short'): string | null {
    switch (type) {
      case 'initials':
        return completeName.split(' ').reduce((acc: string, item) => {
          acc += item[0].toUpperCase();

          return acc;
        }, '');

      case 'short':
        const parsedData = completeName.split(' ').map((item, index) => {
          if (index === 0) {
            return item;
          }

          if (index === 1) {
            return `${item[0].toUpperCase()}.`;
          }

          return '';
        });

        return `${parsedData[0]} ${parsedData[1]}`;
    }
  }
}
