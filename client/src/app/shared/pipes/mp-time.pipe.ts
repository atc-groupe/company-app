import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mpTime',
  standalone: true,
})
export class MpTimePipe implements PipeTransform {
  transform(value: number): string {
    const hours = Math.floor(value);
    const minutes = Math.round((value - hours) * 60);

    if (hours && minutes) {
      return `${hours}h${minutes}`;
    }

    if (hours) {
      return `${hours}h`;
    }

    return `${minutes}mn`;
  }
}
