import { IPaperData, IProduct } from '../interfaces';
import { PaperGroupTypeEnum, PaperTypeEnum, ProductTypeEnum } from '../enums';
import { AbstractProduct } from './abstract-product';

export class Paper extends AbstractProduct<IPaperData> implements IProduct {
  constructor(data: IPaperData) {
    super(data);
  }

  get productType(): ProductTypeEnum {
    return ProductTypeEnum.Paper;
  }

  get groupType(): PaperGroupTypeEnum {
    return this._data.mp.groupType;
  }

  get type(): PaperTypeEnum {
    return this._data.mp.type;
  }

  get width(): number {
    return this._data.mp.width;
  }

  get height(): number {
    return this._data.mp.length;
  }

  get thickness(): number {
    return this._data.mp.thickness;
  }

  get widthLabel(): 'Largeur' | 'Laize' {
    return this._data.mp.type === PaperTypeEnum.Sheet ? 'Largeur' : 'Laize';
  }

  get heightLabel(): 'Hauteur' | 'Longueur (ml)' {
    return this._data.mp.type === PaperTypeEnum.Sheet
      ? 'Hauteur'
      : 'Longueur (ml)';
  }

  get displaySize(): string {
    return `${this._data.mp.width} x ${this._data.mp.length} ${
      this._data.mp.type === PaperTypeEnum.Sheet ? 'mm' : 'ml'
    }`;
  }

  get displayStockUnit(): string {
    return this._data.mp.displayUnit;
  }

  get displayFreeStock(): string {
    return `${this._data.mp.freeStock} ${
      this._data.mp.type === PaperTypeEnum.Sheet ? 'ex' : 'ml'
    }`;
  }

  get displayPrintSide(): 'Intérieure' | 'Extérieure' {
    return this._data.mp.insidePrint ? 'Intérieure' : 'Extérieure';
  }

  get freeStock(): number {
    return this._data.mp.freeStock;
  }

  get reservedStock(): number {
    return this._data.mp.reserved;
  }

  get minimumStock(): number {
    return this._data.mp.minimumStock;
  }

  get totalStock(): number {
    return this._data.mp.stock;
  }

  get packStockUnits(): number {
    return 1;
  }
}
