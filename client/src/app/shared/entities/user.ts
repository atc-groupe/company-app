import {
  IAuthorization,
  IRole,
  IUser,
  IUserAd,
  IUserMp,
  IUserSettings,
} from '../interfaces';
import { UserMpOperationTypeEnum } from '../enums';

export class User {
  private _data: IUser;
  private _authorizations = new Set<string>();
  public initials!: string;

  constructor(data: IUser) {
    this._data = data;
    this._setAuthorizations(data.roles);
    this._setName();
  }

  get _id(): string {
    return this._data._id;
  }

  get isAdmin(): boolean {
    return this._data.isAdmin;
  }

  set isAdmin(value: boolean) {
    this._data.isAdmin = value;
  }

  get isActive(): boolean {
    return this._data.isActive;
  }

  set isActive(value: boolean) {
    this._data.isActive = value;
  }

  get externalAccess(): boolean {
    return this._data.externalAccess;
  }

  set externalAccess(value: boolean) {
    this._data.externalAccess = value;
  }

  get settings(): IUserSettings {
    return this._data.settings;
  }

  get roles(): IRole[] {
    return this._data.roles;
  }

  get ad(): IUserAd {
    return this._data.ad;
  }

  get mp(): IUserMp {
    return this._data.mp;
  }

  get completeName() {
    return this._data.mp.name;
  }

  get authorizations(): Set<string> {
    return this._authorizations;
  }

  public canActivate(auth: string | IAuthorization): boolean {
    if (this.isAdmin) {
      return true;
    }

    const authAsString =
      typeof auth === 'string' ? auth : `${auth.subject}.${auth.action}`;

    return this.authorizations.has(authAsString);
  }

  public hasOperation(operation: string): boolean {
    return this._data.mp.operations.some((el) => el.label === operation);
  }

  public isGraphicDesigner(): boolean {
    return (
      this._data.mp.operations.find((item) => item.number === 11) !== undefined
    );
  }

  static getOperationTypeLabel(type: UserMpOperationTypeEnum): any {
    switch (type) {
      case UserMpOperationTypeEnum.Manipulation:
        return 'Manipulation';
      case UserMpOperationTypeEnum.Improductive:
        return 'Improductive';
      case UserMpOperationTypeEnum.Status:
        return 'Statut';
    }
  }

  private _setAuthorizations(roles: IRole[]): void {
    roles.forEach((role) => {
      role.authorizations.forEach((authorization) =>
        this._authorizations.add(authorization),
      );
    });
  }

  private _setName(): void {
    const split = this._data.mp.name.toUpperCase().split(' ');

    this.initials =
      split.length > 1 ? `${split[0][0]}${split[1][0]}` : split[0][0];
  }
}
