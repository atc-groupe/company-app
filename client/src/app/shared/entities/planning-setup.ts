import { TPlanningSetup } from '../types';
import {
  PlanningDepartmentEnum,
  PlanningModeEnum,
  PlanningPeriodEnum,
} from '../enums';
import {
  IPlanningSetupGroup,
  IPlanningSetupGroupMachine,
  ISelectItem,
} from '../interfaces';

export class PlanningSetup {
  private _groupsMap = new Map<string, IPlanningSetupGroup>();
  private _machinesMap = new Map<number, IPlanningSetupGroupMachine>();

  constructor(groups: TPlanningSetup) {
    groups.forEach((group) => {
      this._groupsMap.set(group.name, group);

      group.machines.forEach((machine) => {
        this._machinesMap.set(machine.id, machine);
      });
    });
  }

  public getDepartment(groupName: string): PlanningDepartmentEnum | null {
    const group = this._groupsMap.get(groupName);

    return group ? group.department : null;
  }

  public getMachine(machineId: number): IPlanningSetupGroupMachine | null {
    const machineSetup = this._machinesMap.get(machineId);

    return machineSetup ? machineSetup : null;
  }

  public getGroupListSelectItems(): ISelectItem[] {
    return Array.from(this._groupsMap.values()).map((group) => {
      return {
        label: group.name,
        value: group.name,
      };
    });
  }

  public getGroupMachinesNames(
    groupName: string,
  ): IPlanningSetupGroupMachine[] {
    const group = this._groupsMap.get(groupName);

    return group ? group.machines : [];
  }

  public getGroupMachinesSelectItems(groupName: string): ISelectItem[] {
    const group = this._groupsMap.get(groupName);
    const items: ISelectItem[] = [{ label: 'Toutes', value: null }];

    if (!group) {
      return items;
    }

    group.machines.forEach((machine) => {
      items.push({
        label: machine.name,
        value: machine.name === 'all' ? -1 : machine.id,
      });
    });

    return items;
  }

  public getGroupEmployees(groupName: string): string[] | null {
    const group = this._groupsMap.get(groupName);

    if (!group) {
      return null;
    }

    return group.machines
      .reduce((acc: string[], machine) => {
        machine.operations.forEach((operation) => {
          operation.employees.forEach((employee) => {
            const name =
              employee.name === '-' || employee.name === '_ _'
                ? ' Non attribué'
                : employee.name;

            if (!acc.includes(name)) {
              acc.push(name);
            }
          });
        });

        return acc;
      }, [])
      .sort((a, b) => (a > b ? 1 : -1));
  }

  public getGroupNameFromMachineId(machineId: number): string | null {
    const group = Array.from(this._groupsMap.values()).find((group) =>
      group.machines.some((el) => el.id === machineId),
    );

    return group ? group.name : null;
  }

  public static getPeriodsSelectItems(): ISelectItem[] {
    return [
      { label: 'Jour', value: PlanningPeriodEnum.Day },
      { label: 'Semaine', value: PlanningPeriodEnum.Week },
      { label: 'Tous', value: PlanningPeriodEnum.All },
    ];
  }

  public static getModesSelectItems(): ISelectItem[] {
    return [
      { label: 'Liste', value: PlanningModeEnum.List },
      { label: 'Kanban', value: PlanningModeEnum.Kanban },
    ];
  }
}
