import { TProductData } from '../types';
import { IProductGenericName } from '../interfaces/stock/i-product-generic-name';
import { IAppFile } from '../interfaces/i-app-file';

export abstract class AbstractProduct<T extends TProductData> {
  protected constructor(protected _data: T) {}

  get id(): number {
    return this._data._id;
  }

  get category(): string {
    return this._data.mp.groupDescription;
  }

  get name(): string {
    return this._data.mp.name;
  }

  get completeName(): string {
    return this._data.mp.completeName;
  }

  get code(): string {
    return this._data.mp.code;
  }

  get color(): string | null {
    return this._data.mp.color;
  }

  get supplier(): string {
    return this._data.mp.supplier;
  }

  get unitPrice(): number {
    return this._data.mp.unitPrice;
  }

  get genericName(): IProductGenericName | null {
    return this._data.genericName ? this._data.genericName : null;
  }

  get isAtStockAlert(): boolean {
    return (
      this._data.mp.minimumStock === this._data.mp.freeStock &&
      this._data.mp.minimumStock > 0
    );
  }

  get isUnderStockAlert(): boolean {
    return this._data.mp.minimumStock > this._data.mp.freeStock;
  }

  get files(): IAppFile[] | null {
    return this._data.files ?? null;
  }
}
