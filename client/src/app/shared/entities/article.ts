import { IArticleData, IProduct } from '../interfaces';
import { ArticleGroupTypeEnum, ProductTypeEnum } from '../enums';
import { AbstractProduct } from './abstract-product';

export class Article extends AbstractProduct<IArticleData> implements IProduct {
  constructor(data: IArticleData) {
    super(data);
  }

  get productType(): ProductTypeEnum {
    return ProductTypeEnum.Article;
  }

  get groupType(): ArticleGroupTypeEnum {
    return this._data.mp.groupType;
  }

  get freeStock(): number {
    return this._data.stockUnitNumber
      ? this._data.mp.freeStock / this._data.stockUnitNumber
      : this._data.mp.freeStock;
  }

  get minimumStock(): number {
    return this._data.stockUnitNumber
      ? this._data.mp.minimumStock / this._data.stockUnitNumber
      : this._data.mp.minimumStock;
  }

  get reservedStock(): number {
    return this._data.stockUnitNumber
      ? this._data.mp.reserved / this._data.stockUnitNumber
      : this._data.mp.reserved;
  }

  get totalStock(): number {
    return this._data.stockUnitNumber
      ? this._data.mp.stock / this._data.stockUnitNumber
      : this._data.mp.stock;
  }

  get displayStockUnit(): string {
    return this._data.stockUnitLabel ?? this._data.mp.displayUnit;
  }

  get mpStockUnit(): string {
    return this._data.mp.displayUnit;
  }

  get appStockUnitLabel(): string | null {
    return this._data.stockUnitLabel ?? null;
  }

  get appStockUnitNumber(): number | null {
    return this._data.stockUnitNumber ?? null;
  }

  get packStockUnits(): number {
    return this.appStockUnitNumber ? this.appStockUnitNumber : 1;
  }

  get packedPer(): number | null {
    return this._data.mp.packedPer;
  }
}
