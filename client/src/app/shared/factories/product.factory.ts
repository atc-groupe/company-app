import { Injectable } from '@angular/core';
import { ProductTypeEnum } from '../enums';
import { TProductData } from '../types';
import { IArticleData, IPaperData, IProduct } from '../interfaces';
import { Article, Paper } from '../entities';

@Injectable({ providedIn: 'root' })
export class ProductFactory {
  getProduct(productType: ProductTypeEnum, data: TProductData): IProduct {
    switch (productType) {
      case ProductTypeEnum.Article:
        return new Article(data as IArticleData);
      case ProductTypeEnum.Paper:
        return new Paper(data as IPaperData);
      case ProductTypeEnum.CustomerProduct:
        throw new Error('Unsupported product type');
    }
  }
}
