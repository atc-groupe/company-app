import { Injectable } from '@angular/core';
import {
  ErrorNotification,
  SuccessNotification,
  WarningNotification,
} from '../classes';
import { NotificationTypeEnum } from '../enums';

@Injectable({ providedIn: 'root' })
export class NotificationFactory {
  public getNotification(type: NotificationTypeEnum, message: string) {
    switch (type) {
      case NotificationTypeEnum.Info:
        return new SuccessNotification(message);
      case NotificationTypeEnum.Success:
        return new SuccessNotification(message);
      case NotificationTypeEnum.Warning:
        return new WarningNotification(message);
      case NotificationTypeEnum.Error:
        return new ErrorNotification(message);
    }
  }
}
