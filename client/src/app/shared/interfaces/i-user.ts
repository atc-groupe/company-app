import { IUserSettings } from './i-user-settings';
import { IRole } from './i-role';
import { IUserAd } from './i-user-ad';
import { IUserMp } from './i-user-mp';

export interface IUser {
  _id: string;
  isAdmin: boolean;
  isActive: boolean;
  externalAccess: boolean;
  settings: IUserSettings;
  roles: IRole[];
  ad: IUserAd;
  mp: IUserMp;
}
