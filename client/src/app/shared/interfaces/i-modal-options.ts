export interface IModalOptions {
  fullScreen?: boolean;
  yPos?: 'top' | 'center';
}
