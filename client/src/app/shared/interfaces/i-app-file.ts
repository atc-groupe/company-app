import { AppModuleEnum } from '../enums';

export interface IAppFile {
  _id: string;
  name: string;
  module: AppModuleEnum;
  path: string;
  extension: string;
  mimeType: string;
  description?: string;
  createdAt: Date;
  updatedAt: Date;
}
