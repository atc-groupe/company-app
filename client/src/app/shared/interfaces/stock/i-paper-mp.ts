import { PaperGroupTypeEnum, PaperTypeEnum } from '../../enums';

export interface IPaperMp {
  name: string;
  completeName: string;
  type: PaperTypeEnum;
  groupType: PaperGroupTypeEnum;
  groupDescription: string;
  code: string;
  color: string;
  width: number;
  length: number;
  thickness: number;
  insidePrint: boolean;
  stockManagement: boolean;
  minimumStock: number;
  stock: number;
  reserved: number;
  freeStock: number;
  supplier: string;
  displayUnit: string;
  unitPrice: number;
  stockValue: number;
}
