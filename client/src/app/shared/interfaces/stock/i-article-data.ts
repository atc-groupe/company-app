import { IArticleMp } from './i-article-mp';
import { IProductGenericName } from './i-product-generic-name';
import { IAppFile } from '../i-app-file';

export interface IArticleData {
  _id: number;
  mp: IArticleMp;
  stockUnitNumber?: number;
  stockUnitLabel?: string;
  genericName?: IProductGenericName | null;
  files?: IAppFile[];
}
