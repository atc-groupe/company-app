import { ProductTypeEnum } from '../../enums';
import { IProductGenericName } from './i-product-generic-name';
import { IAppFile } from '../i-app-file';

export interface IProduct {
  get id(): number;
  get productType(): ProductTypeEnum;
  get groupType(): any;
  get category(): string;
  get name(): string;
  get completeName(): string;
  get code(): string;
  get color(): string | null;
  get unitPrice(): number;
  get supplier(): string;
  get freeStock(): number;
  get minimumStock(): number;
  get reservedStock(): number;
  get totalStock(): number;
  get displayStockUnit(): string;
  get packStockUnits(): number;
  get isAtStockAlert(): boolean;
  get isUnderStockAlert(): boolean;
  get genericName(): IProductGenericName | null;
  get files(): IAppFile[] | null;
}
