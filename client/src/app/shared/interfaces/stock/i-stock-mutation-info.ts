import { ProductTypeEnum } from '../../enums';
import { IStockMutationDto } from '../../dto';

export interface IStockMutationInfo {
  productType: ProductTypeEnum;
  productId: number;
  action: 'decrease' | 'update';
  stockMutationDto: IStockMutationDto;
}
