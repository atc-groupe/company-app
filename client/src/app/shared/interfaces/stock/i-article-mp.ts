import { ArticleGroupTypeEnum } from '../../enums';

export interface IArticleMp {
  name: string;
  completeName: string;
  groupType: ArticleGroupTypeEnum;
  groupDescription: string;
  code: string;
  color: string;
  unit: string;
  displayUnit: string;
  packedPer: number;
  stockManagement: boolean;
  minimumStock: number;
  stock: number;
  reserved: number;
  freeStock: number;
  supplier: string;
  unitPrice: number;
  stockValue: number;
}
