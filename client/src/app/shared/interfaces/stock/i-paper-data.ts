import { IPaperMp } from './i-paper-mp';
import { IProductGenericName } from './i-product-generic-name';
import { IAppFile } from '../i-app-file';

export interface IPaperData {
  _id: number;
  mp: IPaperMp;
  genericName?: IProductGenericName | null;
  files?: IAppFile[];
}
