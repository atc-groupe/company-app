export interface IProductGenericName {
  _id: string;
  name: string;
}
