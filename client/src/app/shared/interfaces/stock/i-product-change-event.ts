import { TProductData } from '../../types';
import { ProductTypeEnum } from '../../enums';

export interface IProductChangeEvent {
  productType: ProductTypeEnum;
  action: 'create' | 'update' | 'delete';
  product: TProductData;
}
