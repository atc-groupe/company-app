import { IProduct } from './i-product';

export interface IProductChange {
  action: 'create' | 'update' | 'delete';
  product: IProduct;
}
