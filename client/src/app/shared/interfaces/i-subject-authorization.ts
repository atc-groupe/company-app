import { IActionAuthorization } from './i-action-authorization';
import { IActionAuthorizationGroup } from './i-action-authorization-group';
import { AuthSubjectsEnum } from '../enums';

export interface ISubjectAuthorization {
  subject: AuthSubjectsEnum;
  title: string;
  description?: string;
  actions?: IActionAuthorization[];
  actionGroups?: IActionAuthorizationGroup[];
}
