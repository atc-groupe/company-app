import { IAppSettingsJobMetaFilter } from './i-app-settings-job-meta-filter';

export interface IAppSettingsJobMetaFilters {
  deliveryServiceFinishes: IAppSettingsJobMetaFilter;
  zundCuttingFinishes: IAppSettingsJobMetaFilter;
  scoreFinishes: IAppSettingsJobMetaFilter;
  kitFinishes: IAppSettingsJobMetaFilter;
  materialDevices: IAppSettingsJobMetaFilter;
  shippingDeliveryMethods: IAppSettingsJobMetaFilter;
  skippedSubcontractors: IAppSettingsJobMetaFilter;
}
