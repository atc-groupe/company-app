export interface IAppSettingsJobStatus {
  _id: string;
  statusNumber: number;
  label: string;
  bgColor: string;
  textColor: 'white' | 'black';
}
