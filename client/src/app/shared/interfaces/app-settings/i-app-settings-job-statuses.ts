import { IAppSettingsJobStatus } from './i-app-settings-job-status';

export interface IAppSettingsJobStatuses {
  defaultColor: string;
  items: IAppSettingsJobStatus[];
}
