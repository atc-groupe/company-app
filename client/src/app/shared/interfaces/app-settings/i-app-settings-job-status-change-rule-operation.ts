export interface IAppSettingsJobStatusChangeRuleOperation {
  _id: string;
  operation: string;
  operationNumber: number;
}
