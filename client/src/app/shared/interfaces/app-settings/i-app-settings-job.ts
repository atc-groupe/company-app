import { IAppSettingsJobStatuses } from './i-app-settings-job-statuses';
import { IAppSettingsJobMapping } from './i-app-settings-job-mapping';
import { IAppSettingsJobSync } from './i-app-settings-job-sync';

export interface IAppSettingsJob {
  statuses: IAppSettingsJobStatuses;
  mapping: IAppSettingsJobMapping;
  sync: IAppSettingsJobSync;
}
