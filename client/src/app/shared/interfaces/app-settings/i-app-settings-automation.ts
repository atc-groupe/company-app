import { IAppSettingsJobStatusChangeRule } from './i-app-settings-job-status-change-rule';

export interface IAppSettingsAutomation {
  jobStatusChangeRules: IAppSettingsJobStatusChangeRule[];
}
