import { IAppSettingsPlanningView } from './i-app-settings-planning-view';

export interface IAppSettingsPlanning {
  views: IAppSettingsPlanningView[];
}
