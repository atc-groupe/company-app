import { IAppSettingsJobStatusChangeRuleOperation } from './i-app-settings-job-status-change-rule-operation';
import { IAppSettingsJobStatusChangeRuleStatus } from './i-app-settings-job-status-change-rule-status';
import { JobStatusChangeRuleTypeEnum } from '../../enums';

export interface IAppSettingsJobStatusChangeRule {
  _id: string;
  type: JobStatusChangeRuleTypeEnum;
  fromStatusText: string;
  fromStatusNumber: number;
  toStatuses: IAppSettingsJobStatusChangeRuleStatus[];
  operations: IAppSettingsJobStatusChangeRuleOperation[];
  canCancel: boolean;
}
