export interface IAppSettingsJobStatusChangeRuleStatus {
  _id: string;
  statusText: string;
  statusNumber: number;
}
