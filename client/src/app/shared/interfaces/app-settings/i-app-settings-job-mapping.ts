import { IAppSettingsJobMetaFilters } from './i-app-settings-job-meta-filters';

export interface IAppSettingsJobMapping {
  metaFilters: IAppSettingsJobMetaFilters;
}
