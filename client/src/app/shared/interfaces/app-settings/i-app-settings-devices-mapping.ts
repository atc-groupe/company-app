import { IAppSettingsDevicesMappingName } from './i-app-settings-devices-mapping-name';

export interface IAppSettingsDevicesMapping {
  names: IAppSettingsDevicesMappingName[];
}
