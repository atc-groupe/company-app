import { IAppSettingsJob } from './i-app-settings-job';
import { IAppSettingsDevices } from './i-app-settings-devices';
import { IAppSettingsAutomation } from './i-app-settings-automation';
import { IAppSettingsPlanning } from './i-app-settings-planning';

export interface IAppSettings {
  job: IAppSettingsJob;
  devices: IAppSettingsDevices;
  automation: IAppSettingsAutomation;
  planning: IAppSettingsPlanning;
}
