export interface IAppSettingsPlanningView {
  _id: string;
  name: string;
  planningDepartment: number;
  planningGroup: string;
  machineIds: number[];
}
