import { IAppSettingsDevicesMapping } from './i-app-settings-devices-mapping';

export interface IAppSettingsDevices {
  mapping: IAppSettingsDevicesMapping;
}
