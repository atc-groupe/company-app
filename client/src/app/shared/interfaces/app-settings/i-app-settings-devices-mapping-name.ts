export interface IAppSettingsDevicesMappingName {
  mpName: string;
  displayName: string;
}
