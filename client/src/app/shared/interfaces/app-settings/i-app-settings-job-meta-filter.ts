export interface IAppSettingsJobMetaFilter {
  title: string;
  description?: string;
  items: string[];
}
