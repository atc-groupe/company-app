export interface ISortColumn {
  index: number;
  dir: 'asc' | 'desc';
}
