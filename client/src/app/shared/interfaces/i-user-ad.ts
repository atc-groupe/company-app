export interface IUserAd {
  userPrincipalName: string;
  firstName: string;
  lastName: string;
}
