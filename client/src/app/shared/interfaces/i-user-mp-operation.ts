import { UserMpOperationTypeEnum } from '../enums';

export interface IUserMpOperation {
  number: number;
  label: string;
  type: UserMpOperationTypeEnum;
}
