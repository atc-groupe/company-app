import { Type } from '@angular/core';
import { IModalOptions } from './i-modal-options';

export interface IModal {
  component: Type<any>;
  inputs?: Record<string, unknown>;
  options?: IModalOptions;
}
