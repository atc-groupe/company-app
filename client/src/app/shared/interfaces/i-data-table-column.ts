export interface IDataTableColumn {
  label?: string;
  icon?: string;
  class: string;
  sortable: boolean;
  hide?: boolean;
}
