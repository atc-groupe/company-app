import { LogContextEnum, LogLevelEnum } from '../enums';

export interface ILog {
  context: LogContextEnum;
  level: LogLevelEnum;
  message: string;
  createdAt: Date;
}
