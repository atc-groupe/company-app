export interface IPlanningCardDelete {
  computedId: string;
  syncAction: 'remove';
}
