import { IJobCommentPlanning, IJobSync } from '../job';
import { IPlanningCardJobOperationUserInfo } from './i-planning-card-job-operation-user-info';

export interface IPlanningCardJobInfo {
  _id: string;
  number: number;
  statusNumber: number;
  company: string;
  description: string;
  machines: string[];
  filesCheckStatusIndex: 1 | 2 | 3;
  sendingDate: string | null;
  comment: IJobCommentPlanning | null;
  sync: IJobSync;
}
