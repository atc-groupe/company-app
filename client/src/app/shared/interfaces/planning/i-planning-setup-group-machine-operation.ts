import { IPlanningSetupGroupMachineOperationEmployee } from './i-planning-setup-group-machine-operation-employee';

export interface IPlanningSetupGroupMachineOperation {
  name: string;
  employees: IPlanningSetupGroupMachineOperationEmployee[];
}
