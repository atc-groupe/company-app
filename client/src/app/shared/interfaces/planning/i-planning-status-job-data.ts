import { IJobCommentPlanning, IJobSync } from '../job';
import { IPlanningStatusOperationData } from './i-planning-status-operation-data';

export interface IPlanningStatusJobData {
  number: number;
  statusNumber: number;
  company: string;
  description: string;
  machines: string[];
  filesCheckStatusIndex: 1 | 2 | 3;
  sendingDate: string | null;
  comment: IJobCommentPlanning | null;
  sync: IJobSync;
  operations: IPlanningStatusOperationData[];
}
