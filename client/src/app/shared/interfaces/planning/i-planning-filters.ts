import { PlanningModeEnum, PlanningPeriodEnum } from '../../enums';
import { IAppSettingsPlanningView } from '../app-settings/i-app-settings-planning-view';

export interface IPlanningFilters {
  mode: PlanningModeEnum;
  period: PlanningPeriodEnum;
  view: IAppSettingsPlanningView | null;
  hideNotPlanned: boolean;
  hideDone: boolean;
  mergeJobOperationCards: boolean;
  employees: string[] | null;
}
