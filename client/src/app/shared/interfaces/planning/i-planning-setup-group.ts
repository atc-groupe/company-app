import { IPlanningSetupGroupMachine } from './i-planning-setup-group-machine';
import { PlanningDepartmentEnum } from '../../enums';

export interface IPlanningSetupGroup {
  department: PlanningDepartmentEnum;
  name: string;
  machines: IPlanningSetupGroupMachine[];
}
