import { IPlanningCard } from './i-planning-card';

export interface IPlanningMachineCards {
  id: number;
  name: string;
  cards: IPlanningCard[];
}
