import { IPlanningStatusJobData } from './i-planning-status-job-data';

export interface IPlanningStatusData {
  statusNumber: number;
  data: IPlanningStatusJobData[];
}
