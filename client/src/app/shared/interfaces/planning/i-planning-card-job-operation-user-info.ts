export interface IPlanningCardJobOperationUserInfo {
  employee: string;
  active: boolean;
}
