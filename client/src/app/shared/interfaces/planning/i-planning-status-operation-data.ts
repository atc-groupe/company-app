import { IPlanningCardJobOperationUserInfo } from './i-planning-card-job-operation-user-info';

export interface IPlanningStatusOperationData {
  operation: string;
  productionTime: number;
  calculatedTime: number;
  machineId: number;
  machineName: string;
  employee: string | null;
  isPlanned: boolean;
  completed: boolean;
  ordering: number;
  userInfo: IPlanningCardJobOperationUserInfo | null;
  time: number | null;
  startDates: Date[];
}
