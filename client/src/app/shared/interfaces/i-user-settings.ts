import { TUiMode } from '../types';
import { PlanningModeEnum, PlanningPeriodEnum } from '../enums';

export interface IUserSettings {
  uiMode: TUiMode;
  planningMode: PlanningModeEnum;
  planningPeriod: PlanningPeriodEnum;
  planningView: string | null;
  planningHideNotPlanned: boolean;
  planningHideDone: boolean;
  planningMergeJobOperationCards: boolean;
}
