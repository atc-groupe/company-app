import { TElemStyle } from '../types';

export interface IAlertOptions {
  style?: TElemStyle;
  confirmLabel?: string;
  declineLabel?: string;
  canClickOutside?: boolean;
  canCancel?: boolean;
}
