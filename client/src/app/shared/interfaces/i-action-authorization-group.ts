import { IActionAuthorization } from './i-action-authorization';

export interface IActionAuthorizationGroup {
  title: string;
  actions: IActionAuthorization[];
}
