import { IJobSync } from './job';

export interface IExpeditionLine {
  _id: string;
  jobNumber: number;
  jobStatusNumber: number;
  company: string;
  description: string;
  jobManager: string;
  devices: string[];
  comment: string | null;
  printingSurface: number;
  manufacturedPiecesCount: number;
  hasSheetProduction: boolean;
  hasSubcontractingProduction: boolean;
  deliveryServiceFinishs: string[];
  deliverySendingDates: string[];
  addressZipCodes: string[];
  deliveryMethods: string[];
  sync: IJobSync;
  syncAction?: 'add' | 'update' | 'remove';
}
