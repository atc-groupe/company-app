export interface IAppWsResponse<T> {
  status: number;
  data?: T;
  error?: string;
}
