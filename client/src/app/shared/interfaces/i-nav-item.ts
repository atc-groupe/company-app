export interface INavItem {
  route?: string;
  label: string;
  disabled?: boolean;
}
