export interface IMenuItem {
  label?: string;
  title?: string;
  icon?: string;
  action?: string;
  disabled?: boolean;
  style?: 'info' | 'success' | 'warning' | 'error';
}
