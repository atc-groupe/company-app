export interface IAppWsErrorResponse {
  status: number;
  message: string;
}
