export interface IJobCommentPlanning {
  _id: string;
  comment: string;
  userId: string;
  userInitials: string;
  planningGroup: string;
  createdAt: Date;
  updatedAt: Date;
}
