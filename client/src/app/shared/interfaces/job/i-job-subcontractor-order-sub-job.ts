import { ISubJobMp } from './i-sub-job-mp';
import { IJobSubcontractorOrderSubJobItem } from './i-job-subcontractor-order-sub-job-item';

export interface IJobSubcontractorOrderSubJob {
  reference: string;
  subJobMp: ISubJobMp;
  items: IJobSubcontractorOrderSubJobItem[];
}
