import { ISubJobSubcontractorItemMp } from './i-sub-job-subcontractor-item-mp';

export interface ISubJobSubcontractorItem {
  mp: ISubJobSubcontractorItemMp;
}
