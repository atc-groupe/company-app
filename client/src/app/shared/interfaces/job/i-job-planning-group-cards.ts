import { IJobPlanningCard } from './i-job-planning-card';

export interface IJobPlanningGroupCards {
  group: string;
  cards: IJobPlanningCard[];
}
