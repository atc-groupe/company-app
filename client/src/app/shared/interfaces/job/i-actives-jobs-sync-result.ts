export interface IActivesJobsSyncResult {
  status: 'success' | 'error';
  jobsSyncCount: number;
  syncTime: string | null;
  error?: string;
}
