export interface IFinishingLayerSides {
  left: boolean;
  right: boolean;
  top: boolean;
  bottom: boolean;
}
