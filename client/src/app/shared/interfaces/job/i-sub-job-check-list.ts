export interface ISubJobCheckList {
  description: string | null;
  format: string | null;
  pao: string | null;
  print: string | null;
  material: string | null;
  finishing: string | null;
  packaging: string | null;
  delivery: string | null;
  application: string | null;
  remark: string | null;
}
