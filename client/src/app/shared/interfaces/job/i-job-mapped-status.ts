export interface IJobMappedStatus {
  statusNumber: number;
  bgColor: string;
  textColor: string;
  label: string;
}
