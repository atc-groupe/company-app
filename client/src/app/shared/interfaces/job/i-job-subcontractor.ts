import { IJobSubcontractorOrder } from './i-job-subcontractor-order';

export interface IJobSubcontractor {
  relationNumber: number;
  relation: string;
  orders: IJobSubcontractorOrder[];
}
