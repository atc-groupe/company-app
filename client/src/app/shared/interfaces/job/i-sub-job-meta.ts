export interface ISubJobMeta {
  surface: number;
  hasSheetPrinting: boolean;
  deliveryServiceFinishes: string[];
  hasNumericScoreCutting: boolean;
  hasZundCutting: boolean;
  hasKits: boolean;
}
