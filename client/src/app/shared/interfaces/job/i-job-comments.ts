import { IJobCommentPlanning } from './i-job-comment-planning';

export interface IJobComments {
  delivery?: string;
  planning: IJobCommentPlanning[];
}
