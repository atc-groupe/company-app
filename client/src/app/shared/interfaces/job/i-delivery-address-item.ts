export interface IDeliveryAddressItem {
  ordering: number;
  quantity: number;
  packedPer: number;
  packages: number;
  description: string;
  remark: string;
  extraInfo: string;
  weight: number;
  status: string;
}
