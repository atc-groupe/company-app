export interface IFinishingLayerOperation {
  name: string;
  type?: string;
  width?: number;
  height?: number;
  doubleSided?: boolean;
}
