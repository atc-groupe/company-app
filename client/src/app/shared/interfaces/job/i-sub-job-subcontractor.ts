import { ISubJobSubcontractorItem } from './i-sub-job-subcontractor-item';

export interface ISubJobSubcontractor {
  relation: string;
  relationNumber: number;
  items: ISubJobSubcontractorItem[];
}
