import { SubJobFinishingTypeEnum } from '../../enums';
import { IFinishingLayerOperation } from './i-finishing-layer-operation';
import { IFinishingLayerMaterial } from './i-finishing-layer-material';
import { IFinishingLayerSides } from './i-finishing-layer-sides';

export interface ISubJobFinishingLayer {
  finishingType: SubJobFinishingTypeEnum;
  operation: IFinishingLayerOperation;
  material: IFinishingLayerMaterial | null;
  sides?: IFinishingLayerSides;
  displayValue: string;
}
