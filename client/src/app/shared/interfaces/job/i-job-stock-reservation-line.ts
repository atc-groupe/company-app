import { IJobMappedStatus } from './i-job-mapped-status';
import { JobStockReservationStatusEnum } from '../../enums';

export interface IJobStockReservationLine {
  jobNumber: number;
  jobStatusNumber: number;
  company: string;
  sendingDate: string | null;
  globalStatus: JobStockReservationStatusEnum;
  itemsCount: number;
  syncAction?: 'add' | 'update' | 'remove';
}
