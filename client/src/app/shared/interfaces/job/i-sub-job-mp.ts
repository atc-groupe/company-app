import { ISubJobCheckList } from './i-sub-job-check-list';

export interface ISubJobMp {
  id: number;
  productNumber: number;
  productType: string;
  articleNumber: string;
  articleType: string;
  description: string;
  quantity: number;
  deliveredQuantity: number;
  weight: number;
  paoCheckRemark: string;
  paoRemark: string;
  checklist: ISubJobCheckList;
}
