export interface IJobDates {
  date: string | null;
  sending: string | null;
  delivery: string | null;
  deliveryWeek: number;
  deliveryDateLocked: boolean;
  atWork: string | null;
  proof: string | null;
  return: string | null;
}
