export interface IJobMpStatusHistory {
  date: string;
  time: string;
  status: string;
  reason: string;
}
