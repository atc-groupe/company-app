import { IJobSync } from './i-job-sync';

export interface IJobLine {
  number: number;
  statusNumber: number;
  company: string;
  description: string;
  devices: string[];
  deliveryDate: string | null;
  sync: IJobSync;
  syncAction?: 'add' | 'update';
}
