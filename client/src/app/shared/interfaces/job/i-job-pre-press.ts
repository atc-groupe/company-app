export interface IJobPrePress {
  note: string;
  graphicDesignNote: string;
  designNote: string;
  filesCheck: string;
  filesCheckStatusIndex: 1 | 2 | 3;
}
