import { IJobInfo } from './i-job-info';
import { IJobDates } from './i-job-dates';
import { IJobMpStatusHistory } from './i-job-mp-status-history';

export interface IJobMp {
  id: number;
  number: number;
  type: number;
  company: string;
  holding: string;
  relationNumber: number;
  contactName: string;
  description: string;
  reference: string;
  phone: string;
  email: string;
  creator: string;
  calculator: string;
  salesMan: string;
  jobManager: string;
  price: number;
  statusNumber: number;
  statusHistory: IJobMpStatusHistory[];
  planned: boolean;
  delivered: string;
  filesDelivered: boolean;
  info: IJobInfo;
  dates: IJobDates;
}
