export interface IJobOperation {
  operation: string;
  startDate: string;
  department: number;
  calculatedTime: number;
  productionTime: number;
  ordering: number;
  employee: string;
  information: string;
  extraInfo: string;
  machine: string;
  machineId: number;
}
