export interface IJobInfo {
  general: string;
  prePress: string;
  digital: string;
  finishing: string;
  subcontract: string;
  expeditions: string;
  application: string;
}
