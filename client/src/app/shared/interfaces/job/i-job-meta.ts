export interface IJobMeta {
  printingSurface: number;
  modelCount: number;
  manufacturedPiecesCount: number;
  productionTypes: string[];
  deliveryServiceFinishes: string[];
  hasSheetProduction: boolean;
  hasNumericScoreCutting: boolean;
  hasZundCutting: boolean;
  hasKits: boolean;
  hasSubcontractingProduction: boolean;
  addressCount: number;
  addressZipCodes: string[];
  deliveryMethods: string[];
  deliverySendingDates: string[];
}
