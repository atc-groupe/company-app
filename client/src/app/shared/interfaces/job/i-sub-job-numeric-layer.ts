export interface ISubJobNumericLayer {
  uniqueId: number;
  layerName: string;
  layerIndex: number;
  remark: string;
  device: string;
  deviceDisplayName: string;
  printMode: string;
  roll: boolean;
  width: number;
  height: number;
  quantity: number;
  paperName: string;
  paperWeight: number;
  paperThickness: number;
  paperCode: string;
  paperId: number;
}
