import { IDeliveryAddressMp } from './i-delivery-address-mp';
import { IDeliveryAddressItem } from './i-delivery-address-item';

export interface IDeliveryAddress {
  mp: IDeliveryAddressMp;
  items: IDeliveryAddressItem[];
}
