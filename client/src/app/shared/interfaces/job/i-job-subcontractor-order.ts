import { IJobSubcontractorOrderSubJob } from './i-job-subcontractor-order-sub-job';

export interface IJobSubcontractorOrder {
  contactName: string | null;
  sendingDate: string | null;
  deliveryDate: string | null;
  subJobs: IJobSubcontractorOrderSubJob[];
}
