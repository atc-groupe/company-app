export interface IJobStatus {
  number: number;
  label: string;
}
