import { JobSyncStatusEnum } from '../../enums';

export interface IJobSync {
  status: JobSyncStatusEnum;
  timestamp: number;
  errorMessage?: string;
}
