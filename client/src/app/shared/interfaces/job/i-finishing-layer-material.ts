export interface IFinishingLayerMaterial {
  name: string;
  id: number;
}
