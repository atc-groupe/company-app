export interface ISubJobSubcontractorItemMp {
  contactName: string;
  description: string;
  remark: string;
  quantity: number;
  ordered: boolean;
  sendingDate: string | null;
  deliveryDate: string | null;
}
