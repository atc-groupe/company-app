import { JobStockReservationStatusEnum } from '../../enums';
import { IProductGenericName } from '../stock/i-product-generic-name';
import { IPaperData } from '../stock';

export interface IJobStockReservation {
  _id: string;
  status: JobStockReservationStatusEnum;
  paperGenericName: IProductGenericName | null;
  paperName: string;
  paper: IPaperData | null;
  width: number | null;
  height: number | null;
  imperativeFormat: boolean;
  quantity: number;
  printSides: string;
  destination: string;
  comment: string | null;
  subtractQuantity: number | null;
  mutationDate: Date | null;
  createdAt: Date;
  updatedAt: Date;
  createUsername: string;
  updateUsername: string | null;
  handleUsername: string | null;
}
