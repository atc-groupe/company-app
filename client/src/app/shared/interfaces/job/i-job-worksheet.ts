export interface IJobWorksheet {
  id: string;
  date: string;
  employee: string;
  startTime: number;
  stopTime: number;
  timeProduction: number;
  employeeNumber: number;
  timeCustomer: number;
  operation: string;
  operationNumber: number;
  totalTime: number;
  remark: string;
  countRuns: number;
  operationReady: boolean;
}
