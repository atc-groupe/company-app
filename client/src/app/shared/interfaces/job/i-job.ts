import { IJobMp } from './i-job-mp';
import { ISubJob } from './i-sub-job';
import { IDeliveryAddress } from './i-delivery-address';
import { IJobMeta } from './i-job-meta';
import { IJobComments } from './i-job-comments';
import { IJobSync } from './i-job-sync';
import { IJobPrePress } from './i-job-pre-press';
import { IJobSubcontractor } from './i-job-subcontractor';
import { IJobOperation } from './i-job-operation';
import { IJobStockReservation } from './i-job-stock-reservation';
import { IJobWorksheet } from './i-job-worksheet';
import { IJobPlanningCard } from './i-job-planning-card';

export interface IJob {
  _id: string;
  mp: IJobMp;
  subJobs: ISubJob[] | null;
  deliveryAddresses: IDeliveryAddress[] | null;
  subcontractors: IJobSubcontractor[] | null;
  operations: IJobOperation[] | null;
  worksheets: IJobWorksheet[] | null;
  stockReservations: IJobStockReservation[] | null;
  meta: IJobMeta;
  sync: IJobSync;
  comments: IJobComments;
  prePress: IJobPrePress;
  planningData: IJobPlanningCard[];
}
