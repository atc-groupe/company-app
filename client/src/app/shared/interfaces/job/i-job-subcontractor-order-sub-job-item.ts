export interface IJobSubcontractorOrderSubJobItem {
  description: string;
  remark: string;
  quantity: number;
  ordered: boolean;
}
