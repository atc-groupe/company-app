import { ISubJobNumericLayer } from './i-sub-job-numeric-layer';
import { ISubJobFinishingLayer } from './i-sub-job-finishing-layer';

export interface ISubJobProdLayer {
  numeric: ISubJobNumericLayer;
  finishes: ISubJobFinishingLayer[] | null;
}
