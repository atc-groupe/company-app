export interface IDeliveryAddressMp {
  company: string;
  address: string;
  zipCode: string;
  city: string;
  country: string;
  countryCode: string;
  contactName: string;
  phone: string;
  email: string;
  relation: string;
  addressNumber: string;
  relationNumber: number;
  deliveryMethod: string;
  sendingDate: string;
  deliveryDate: string;
  extraAddress: string[] | null;
}
