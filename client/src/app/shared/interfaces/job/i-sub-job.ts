import { ISubJobMp } from './i-sub-job-mp';
import { ISubJobProdLayer } from './i-sub-job-prod-layer';
import { ISubJobFinishingLayer } from './i-sub-job-finishing-layer';
import { ISubJobSubcontractor } from './i-sub-job-subcontractor';
import { ISubJobMeta } from './i-sub-job-meta';

export interface ISubJob {
  referenceIndex: number;
  reference: string;
  mp: ISubJobMp;
  prodLayers: ISubJobProdLayer[] | null;
  unlinkedFinishingLayers: ISubJobFinishingLayer[] | null;
  subcontractors: ISubJobSubcontractor[] | null;
  meta: ISubJobMeta | null;
}
