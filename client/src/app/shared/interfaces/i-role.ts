export interface IRole {
  _id: string;
  label: string;
  description: string;
  authorizations: string[];
}
