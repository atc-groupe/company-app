import { AuthSubjectsEnum } from '../enums';
import { TAuthAction } from '../types';

export interface IAuthorization {
  subject: AuthSubjectsEnum;
  action: TAuthAction;
}
