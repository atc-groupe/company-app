import { IUserMpOperation } from './i-user-mp-operation';

export interface IUserMp {
  employeeNumber: number;
  name: string;
  department: string;
  operations: IUserMpOperation[];
}
