export interface IDataColumn {
  label?: string;
  icon?: string;
  notSortable?: true;
  flex: number;
  textAlign?: 'center' | 'right';
}
