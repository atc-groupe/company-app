import { TAuthAction } from '../types';

export interface IActionAuthorization {
  action: TAuthAction;
  description: string;
}
