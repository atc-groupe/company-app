export interface IAppWsConnectionError {
  message: string;
}
