import { ISubjectAuthorization } from '../interfaces';
import { AuthActionsEnum, AuthSubjectsEnum } from '../enums';

export const ROLES_AUTHORIZATIONS: ISubjectAuthorization = {
  subject: AuthSubjectsEnum.Roles,
  title: 'Roles utilisateurs',
  actions: [
    {
      action: AuthActionsEnum.Manage,
      description: 'Créer, modifier et supprimer des rôles utilisateurs',
    },
  ],
};
