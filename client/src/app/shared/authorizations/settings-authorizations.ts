import { ISubjectAuthorization } from '../interfaces';
import { AuthActionsSettingsEnum, AuthSubjectsEnum } from '../enums';

export const SETTINGS_AUTHORIZATIONS: ISubjectAuthorization = {
  subject: AuthSubjectsEnum.Settings,
  title: "Paramètres de l'application",
  actions: [
    {
      action: AuthActionsSettingsEnum.ManageMappingParameters,
      description: 'Gérer les paramètres de mapping des données MultiPress',
    },
    {
      action: AuthActionsSettingsEnum.ManageJobsAutomationRules,
      description:
        'Gérer les règles de changement automatique du statut des jobs',
    },
    {
      action: AuthActionsSettingsEnum.ManageProductsGenericNames,
      description: 'Gérer les noms génériques des matières',
    },
    {
      action: AuthActionsSettingsEnum.ManagePlanningViews,
      description: 'Gérer les vues de planning',
    },
  ],
};
