import { ISubjectAuthorization } from '../interfaces';
import { AuthActionsJobsEnum, AuthSubjectsEnum } from '../enums';

export const JOBS_AUTHORIZATIONS: ISubjectAuthorization = {
  subject: AuthSubjectsEnum.Jobs,
  title: 'Jobs',
  actions: [
    {
      action: AuthActionsJobsEnum.UpdateStatus,
      description: 'Modifier le statut du job',
    },
  ],
  actionGroups: [
    {
      title: 'Notes',
      actions: [
        {
          action: AuthActionsJobsEnum.UpdateGraphicDesignNote,
          description: 'Modifier les infos de créa graphique',
        },
        {
          action: AuthActionsJobsEnum.UpdateDesignNote,
          description: 'Modifier les infos de conception',
        },
        {
          action: AuthActionsJobsEnum.UpdatePrePressNote,
          description: 'Modifier les infos de pré-presse',
        },
        {
          action: AuthActionsJobsEnum.UpdatePlanningComment,
          description: 'Modifier la remarque de mon planning',
        },
        {
          action: AuthActionsJobsEnum.UpdateDeliveryPlanningComment,
          description: "Modifier la remarque du planning d'expéditions",
        },
      ],
    },
    {
      title: 'Pré-Presse',
      actions: [
        {
          action: AuthActionsJobsEnum.UpdateFilesCheckInfos,
          description: 'Modifier les informations de vérification fichiers',
        },
      ],
    },
    {
      title: 'Opérations',
      actions: [
        {
          action: AuthActionsJobsEnum.CreateWorksheet,
          description: "Créer une fiche d'enregistrement de temps",
        },
        {
          action: AuthActionsJobsEnum.UpdateMyWorksheets,
          description: "Modifier mes fiches d'enregistrement de temps",
        },
        {
          action: AuthActionsJobsEnum.DeleteMyWorksheets,
          description: "Supprimer mes fiches d'enregistrement de temps",
        },
        {
          action: AuthActionsJobsEnum.ManageWorksheets,
          description: "Gérer toutes les fiches d'enregistrement de temps",
        },
      ],
    },
    {
      title: 'Réservations matière',
      actions: [
        {
          action: AuthActionsJobsEnum.CreateStockReservations,
          description: 'Créer et modifier des réservations matières',
        },
        {
          action: AuthActionsJobsEnum.HandleStockReservations,
          description: 'Traiter des réservations matières',
        },
        {
          action: AuthActionsJobsEnum.ManageStockReservations,
          description: 'Créer, modifier et traiter des réservations matières',
        },
      ],
    },
    {
      title: 'Admin',
      actions: [
        {
          action: AuthActionsJobsEnum.SyncAllJobs,
          description: 'Lancer une resynchronisation de tous les jobs',
        },
      ],
    },
  ],
};
