import { ISubjectAuthorization } from '../interfaces';
import { AppModuleEnum, AuthSubjectsEnum } from '../enums';

export const MODULES_AUTHORIZATIONS: ISubjectAuthorization = {
  subject: AuthSubjectsEnum.Modules,
  title: 'Modules',
  description: "Modules de l'application",
  actions: [
    {
      action: AppModuleEnum.Jobs,
      description: 'Jobs',
    },
    {
      action: AppModuleEnum.Planning,
      description: 'Planning',
    },
    {
      action: AppModuleEnum.Stock,
      description: 'Stock',
    },
    {
      action: AppModuleEnum.Expeditions,
      description: 'Expeditions',
    },
    {
      action: AppModuleEnum.Admin,
      description: 'Admin',
    },
  ],
};
