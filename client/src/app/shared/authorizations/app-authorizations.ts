import { ISubjectAuthorization } from '../interfaces';
import { JOBS_AUTHORIZATIONS } from './jobs-authorizations';
import { MODULES_AUTHORIZATIONS } from './modules-authorizations';
import { USERS_AUTHORIZATIONS } from './users-authorizations';
import { ROLES_AUTHORIZATIONS } from './roles-authorizations';
import { PRODUCTS_AUTHORIZATIONS } from './products-authorizations';
import { SETTINGS_AUTHORIZATIONS } from './settings-authorizations';

export const APP_AUTHORIZATIONS: ISubjectAuthorization[] = [
  MODULES_AUTHORIZATIONS,
  USERS_AUTHORIZATIONS,
  ROLES_AUTHORIZATIONS,
  JOBS_AUTHORIZATIONS,
  PRODUCTS_AUTHORIZATIONS,
  SETTINGS_AUTHORIZATIONS,
];
