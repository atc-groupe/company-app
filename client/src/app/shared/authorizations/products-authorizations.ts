import { ISubjectAuthorization } from '../interfaces';
import { AuthActionsProductsEnum, AuthSubjectsEnum } from '../enums';

export const PRODUCTS_AUTHORIZATIONS: ISubjectAuthorization = {
  subject: AuthSubjectsEnum.Products,
  title: 'Produits',
  actionGroups: [
    {
      title: 'Stock',
      actions: [
        {
          action: AuthActionsProductsEnum.DecreaseStock,
          description: 'Déstocker un produit',
        },
        {
          action: AuthActionsProductsEnum.UpdateStock,
          description: "Mettre à jour le stock d'un produit",
        },
        {
          action: AuthActionsProductsEnum.ManageStock,
          description: "Déstocker et mettre à jour le stock d'un produit",
        },
      ],
    },
    {
      title: 'Informations et réglages',
      actions: [
        {
          action: AuthActionsProductsEnum.UpdateStockSettings,
          description: "Modifier les paramètres d'un produit",
        },
        {
          action: AuthActionsProductsEnum.ManageDocuments,
          description:
            'Ajouter, modifier et supprimer des docs. liés à un produit',
        },
      ],
    },
    {
      title: 'Admin',
      actions: [
        {
          action: AuthActionsProductsEnum.SyncAllProducts,
          description: 'Lancer une resynchronisation de tous les produits',
        },
      ],
    },
  ],
};
