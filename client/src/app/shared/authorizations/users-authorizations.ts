import { ISubjectAuthorization } from '../interfaces';
import { AuthActionsEnum, AuthSubjectsEnum } from '../enums';

export const USERS_AUTHORIZATIONS: ISubjectAuthorization = {
  subject: AuthSubjectsEnum.Users,
  title: 'Utilisateurs',
  actions: [
    {
      action: AuthActionsEnum.Manage,
      description: 'Ajouter, modifier et supprimer des utilisateurs',
    },
  ],
};
