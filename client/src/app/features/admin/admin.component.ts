import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavComponent } from './components/nav/nav.component';
import { RouterOutlet } from '@angular/router';
import { Store } from '@ngrx/store';
import { selectNavbarExpanded } from '../../shared/store/navbar';
import { FeatureNavHeadingComponent } from '../../shared/components/feature-nav-heading/feature-nav-heading.component';
import {
  AppModuleEnum,
  AuthActionsEnum,
  AuthActionsSettingsEnum,
  AuthSubjectsEnum,
} from '../../shared/enums';
import { selectUser } from '../../shared/store/user/user.selectors';

@Component({
  selector: 'app-admin',
  standalone: true,
  imports: [
    CommonModule,
    NavComponent,
    RouterOutlet,
    FeatureNavHeadingComponent,
  ],
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
})
export class AdminComponent {
  public expanded$ = this._store.select(selectNavbarExpanded);
  public canManageUsers = false;
  public canManageRoles = false;
  public canManageMappingParameters = false;
  public canManageJobsAutomationRules = false;
  public canManageProductGenericNames = false;
  public canManagePlanningViews = false;

  constructor(private _store: Store) {
    this._store.select(selectUser).subscribe((user) => {
      if (user) {
        this.canManageUsers = user.canActivate({
          subject: AuthSubjectsEnum.Users,
          action: AuthActionsEnum.Manage,
        });

        this.canManageRoles = user.canActivate({
          subject: AuthSubjectsEnum.Roles,
          action: AuthActionsEnum.Manage,
        });

        this.canManageMappingParameters = user.canActivate({
          subject: AuthSubjectsEnum.Settings,
          action: AuthActionsSettingsEnum.ManageMappingParameters,
        });

        this.canManageJobsAutomationRules = user.canActivate({
          subject: AuthSubjectsEnum.Settings,
          action: AuthActionsSettingsEnum.ManageJobsAutomationRules,
        });

        this.canManageProductGenericNames = user.canActivate({
          subject: AuthSubjectsEnum.Settings,
          action: AuthActionsSettingsEnum.ManageProductsGenericNames,
        });

        this.canManagePlanningViews = user.canActivate({
          subject: AuthSubjectsEnum.Settings,
          action: AuthActionsSettingsEnum.ManagePlanningViews,
        });
      }
    });
  }

  get module(): AppModuleEnum {
    return AppModuleEnum.Admin;
  }
}
