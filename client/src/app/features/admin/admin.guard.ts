import { CanActivateFn } from '@angular/router';
import { inject } from '@angular/core';
import { Store } from '@ngrx/store';
import { selectUser } from '../../shared/store/user/user.selectors';
import { map } from 'rxjs';
import {
  AuthActionsEnum,
  AuthActionsSettingsEnum,
  AuthSubjectsEnum,
} from '../../shared/enums';

export const canActivateUsers: CanActivateFn = () => {
  return inject(Store)
    .select(selectUser)
    .pipe(
      map((user) => {
        return user
          ? user.canActivate({
              subject: AuthSubjectsEnum.Users,
              action: AuthActionsEnum.Manage,
            })
          : false;
      }),
    );
};

export const canActivateRoles: CanActivateFn = () => {
  return inject(Store)
    .select(selectUser)
    .pipe(
      map((user) => {
        return user
          ? user.canActivate({
              subject: AuthSubjectsEnum.Roles,
              action: AuthActionsEnum.Manage,
            })
          : false;
      }),
    );
};

export const canActivateMappingParameters: CanActivateFn = () => {
  return inject(Store)
    .select(selectUser)
    .pipe(
      map((user) => {
        return user
          ? user.canActivate({
              subject: AuthSubjectsEnum.Settings,
              action: AuthActionsSettingsEnum.ManageMappingParameters,
            })
          : false;
      }),
    );
};

export const canActivateJobsAutomationRules: CanActivateFn = () => {
  return inject(Store)
    .select(selectUser)
    .pipe(
      map((user) => {
        return user
          ? user.canActivate({
              subject: AuthSubjectsEnum.Settings,
              action: AuthActionsSettingsEnum.ManageJobsAutomationRules,
            })
          : false;
      }),
    );
};

export const canActivateProductsParameters: CanActivateFn = () => {
  return inject(Store)
    .select(selectUser)
    .pipe(
      map((user) => {
        return user
          ? user.canActivate({
              subject: AuthSubjectsEnum.Settings,
              action: AuthActionsSettingsEnum.ManageProductsGenericNames,
            })
          : false;
      }),
    );
};

export const canActivatePlanningViews: CanActivateFn = () => {
  return inject(Store)
    .select(selectUser)
    .pipe(
      map((user) => {
        return user
          ? user.canActivate({
              subject: AuthSubjectsEnum.Settings,
              action: AuthActionsSettingsEnum.ManagePlanningViews,
            })
          : false;
      }),
    );
};
