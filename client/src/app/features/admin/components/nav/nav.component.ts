import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterLink, RouterLinkActive } from '@angular/router';
import { ClickOutsideDirective } from '../../../../shared/directives/click-outside.directive';
import { Store } from '@ngrx/store';
import { hideNavbarAction } from '../../../../shared/store/navbar';

@Component({
  selector: 'admin-nav',
  standalone: true,
  imports: [CommonModule, RouterLink, RouterLinkActive, ClickOutsideDirective],
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
})
export class NavComponent {
  @Input() public expanded: boolean | null = false;
  @Input({ required: true }) canManageUsers = false;
  @Input({ required: true }) canManageRoles = false;
  @Input({ required: true }) canManageMappingParameters = false;
  @Input({ required: true }) canManageJobsAutomationRules = false;
  @Input({ required: true }) canManageProductGenericNames = false;
  @Input({ required: true }) canManagePlanningViews = false;

  constructor(private _store: Store) {}

  public onHide() {
    this._store.dispatch(hideNavbarAction());
  }
}
