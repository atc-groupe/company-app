import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IProductGenericName } from '../../../../../../../shared/interfaces/stock/i-product-generic-name';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import {
  ModalService,
  NotificationsService,
  StockProductGenericNamesService,
} from '../../../../../../../shared/services';
import { CancelButtonComponent } from '../../../../../../../shared/components/cancel-button/cancel-button.component';
import { ModalCloseDirective } from '../../../../../../../shared/directives/modal-close.directive';
import { SaveButtonComponent } from '../../../../../../../shared/components/save-button/save-button.component';
import { MessageComponent } from '../../../../../../../shared/components/message/message.component';

@Component({
  selector: 'app-edit',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CancelButtonComponent,
    ModalCloseDirective,
    SaveButtonComponent,
    MessageComponent,
  ],
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
})
export class EditComponent implements OnInit {
  @Input() name!: IProductGenericName | null;
  @ViewChild('nameInput') nameInput!: ElementRef<HTMLInputElement>;

  public error: string | null = null;
  public form = this._fb.group({
    name: ['', Validators.required],
  });

  constructor(
    private _fb: FormBuilder,
    private _productNameService: StockProductGenericNamesService,
    private _modalService: ModalService,
    private _notificationService: NotificationsService,
  ) {}

  ngOnInit(): void {
    if (this.name) {
      this.form.get('name')?.setValue(this.name.name);
    }

    setTimeout(() => {
      this.nameInput.nativeElement.focus();
    });
  }

  public onSubmit(): void {
    this.error = null;
    const name = this.form.controls.name.value;

    if (!name) {
      this.error = 'Merci de renseigner un nom';
      return;
    }

    const action$ = this.name
      ? this._productNameService.updateOne(this.name._id, name)
      : this._productNameService.insertOne(name);

    action$.subscribe({
      next: () => {
        this._notificationService.displaySuccess(
          this.name ? 'Le nom a été modifié !' : 'Le nom a été ajouté !',
        );
        this._modalService.close({ result: 'success' });
      },
      error: (err) => {
        this.error = err.error.message;
      },
    });
  }
}
