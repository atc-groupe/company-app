import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductGenericNamesComponent } from './product-generic-names.component';

describe('ProductGenericNamesComponent', () => {
  let component: ProductGenericNamesComponent;
  let fixture: ComponentFixture<ProductGenericNamesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ProductGenericNamesComponent]
    });
    fixture = TestBed.createComponent(ProductGenericNamesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
