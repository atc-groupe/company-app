import { Component, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  AlertService,
  ModalService,
  NotificationsService,
  StockProductGenericNamesService,
} from '../../../../../shared/services';
import { IProductGenericName } from '../../../../../shared/interfaces/stock/i-product-generic-name';
import { EditComponent } from './components/edit/edit.component';
import { Store } from '@ngrx/store';
import { selectUser } from '../../../../../shared/store/user/user.selectors';
import { Subscription } from 'rxjs';
import {
  AuthActionsSettingsEnum,
  AuthSubjectsEnum,
} from '../../../../../shared/enums';
import { MessageComponent } from '../../../../../shared/components/message/message.component';

@Component({
  selector: 'admin-product-generic-names',
  standalone: true,
  imports: [CommonModule, MessageComponent],
  templateUrl: './product-generic-names.component.html',
  styleUrls: ['./product-generic-names.component.scss'],
})
export class ProductGenericNamesComponent implements OnInit, OnDestroy {
  public names: IProductGenericName[] | null = null;
  public error: string | null = null;
  public canManageProductsSettings = false;
  private _subscription = new Subscription();

  constructor(
    private _productNameService: StockProductGenericNamesService,
    private _modalService: ModalService,
    private _alertService: AlertService,
    private _store: Store,
    private _notificationService: NotificationsService,
  ) {}

  ngOnInit(): void {
    this._fetchNames();

    this._subscription.add(
      this._store.select(selectUser).subscribe((user) => {
        if (user) {
          this.canManageProductsSettings = user.canActivate({
            subject: AuthSubjectsEnum.Settings,
            action: AuthActionsSettingsEnum.ManageProductsGenericNames,
          });
        }
      }),
    );
  }

  public onAddName(): void {
    if (!this.canManageProductsSettings) {
      return;
    }

    this._modalService
      .open(EditComponent, {
        options: { yPos: 'center' },
        inputs: { name: null },
      })
      .afterClosed$.subscribe(({ result }) => {
        if (result === 'success') {
          this._fetchNames();
        }
      });
  }

  public onEditName(name: IProductGenericName): void {
    if (!this.canManageProductsSettings) {
      return;
    }

    this._modalService
      .open(EditComponent, {
        options: { fullScreen: true },
        inputs: { name },
      })
      .afterClosed$.subscribe(({ result }) => {
        if (result === 'success') {
          this._fetchNames();
        }
      });
  }

  public onRemoveName(name: IProductGenericName): void {
    if (!this.canManageProductsSettings) {
      return;
    }

    this._alertService
      .open(
        'Suppression',
        `Etes vous sure de vouloir supprimer l'élément ${name.name}`,
        {
          style: 'error',
          confirmLabel: 'Supprimer',
          canClickOutside: true,
          canCancel: true,
        },
      )
      .afterClosed$.subscribe((result) => {
        if (result === 'confirm') {
          this._productNameService.removeOne(name._id).subscribe({
            next: () => {
              this.error = null;
              this._notificationService.displaySuccess('Item supprimé !');
              this._fetchNames();
            },
            error: (err) =>
              this._notificationService.displayError(
                `Une erreur est survenue pendant la suppression. ${err.error.message}`,
              ),
          });
        }
      });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  private _fetchNames(): void {
    this._productNameService.findAll().subscribe({
      next: (names) => (this.names = names),
      error: (error) => (this.error = error.error.message),
    });
  }
}
