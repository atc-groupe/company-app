import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductGenericNamesComponent } from './product-generic-names/product-generic-names.component';
import { Store } from '@ngrx/store';
import { setNavbarItemsAction } from '../../../../shared/store/navbar';

@Component({
  selector: 'admin-products-settings',
  standalone: true,
  imports: [CommonModule, ProductGenericNamesComponent],
  templateUrl: './products-settings.component.html',
  styleUrls: ['./products-settings.component.scss'],
})
export class ProductsSettingsComponent {
  constructor(private _store: Store) {
    this._store.dispatch(
      setNavbarItemsAction({
        items: [{ label: 'Stock Paramètres produits' }],
      }),
    );
  }
}
