import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  UsersService,
  AlertService,
  ModalService,
  NotificationsService,
} from '../../../../../shared/services';
import {
  ActivatedRoute,
  Router,
  RouterLink,
  RouterLinkActive,
} from '@angular/router';
import { Store } from '@ngrx/store';
import {
  displayNotificationAction,
  displaySuccessNotificationAction,
} from '../../../../../shared/store/ui';
import { User } from '../../../../../shared/entities';
import { IRole } from '../../../../../shared/interfaces';
import { SuccessNotification } from '../../../../../shared/classes';
import { EditComponent } from '../components/edit/edit.component';
import { AddRoleComponent } from '../components/add-role/add-role.component';
import { setNavbarItemsAction } from '../../../../../shared/store/navbar';
import { selectUser } from '../../../../../shared/store/user/user.selectors';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { AuthActionsEnum, AuthSubjectsEnum } from '../../../../../shared/enums';
import { MessageComponent } from '../../../../../shared/components/message/message.component';

@Component({
  selector: 'admin-users-view',
  standalone: true,
  imports: [CommonModule, RouterLink, RouterLinkActive, MessageComponent],
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss'],
})
export class ViewComponent implements OnInit {
  public user: User | null = null;
  public error: string | null = null;
  public canManageUsers: boolean = false;
  protected readonly User = User;
  private _id: string | null = null;

  constructor(
    private _usersService: UsersService,
    private _alertService: AlertService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _store: Store,
    private _modalService: ModalService,
    private _notificationService: NotificationsService,
  ) {
    this._store
      .select(selectUser)
      .pipe(takeUntilDestroyed())
      .subscribe((user) => {
        if (user) {
          this.canManageUsers = user.canActivate({
            subject: AuthSubjectsEnum.Users,
            action: AuthActionsEnum.Manage,
          });
        }
      });
  }

  ngOnInit() {
    this._id = this._route.snapshot.paramMap.get('id')!;
    this._loadUser();
  }

  get activeButtonLabel(): string {
    return this.user?.isActive ? 'Désactiver' : 'Activer';
  }

  public onEditInfo(): void {
    if (!this.canManageUsers) {
      return;
    }

    this._modalService
      .open(EditComponent, {
        options: { yPos: 'center' },
        inputs: { user: this.user },
      })
      .afterClosed$.subscribe(({ action }) => {
        if (action === 'success') {
          this._loadUser();
        }
      });
  }

  public onToggleUserStatus(): void {
    if (!this.user || !this.canManageUsers) {
      return;
    }

    this._usersService
      .updateOne(this.user._id, {
        isActive: !this.user.isActive,
      })
      .subscribe({
        next: (user) => {
          this.user!.isActive = user.isActive;
          this._notificationService.displaySuccess(
            `L'utilisateur ${user.completeName} a été ${user.isActive ? 'activé' : 'désactivé'}.`,
          );
        },
        error: (err) => (this.error = err.error.message),
      });
  }

  public onDeleteUser(): void {
    if (!this.user || !this.canManageUsers) {
      return;
    }

    this._alertService
      .open(
        'Attention',
        `Confirmez vous la suppression de ${this.user?.completeName}`,
        {
          style: 'error',
          confirmLabel: 'Supprimer',
        },
      )
      .afterClosed$.subscribe((result) => {
        if (result === 'confirm') {
          this._usersService.deleteOne(this.user!._id).subscribe({
            next: () =>
              this._notificationService.displaySuccess(
                `L'utilisateur ${this.user!.completeName} a été supprimé`,
              ),
            error: (err) => (this.error = err.error.message),
          });

          this._router.navigateByUrl('/admin/users/list');
        }
      });
  }

  public onAddRole(): void {
    if (!this.canManageUsers) {
      return;
    }

    this._modalService
      .open(AddRoleComponent, {
        options: { yPos: 'center' },
        inputs: { user: this.user },
      })
      .afterClosed$.subscribe(({ action }) => {
        if (action === 'success') {
          this._loadUser();
        }
      });
  }

  public onRemoveRole(role: IRole): void {
    if (!this.user || !this.canManageUsers) {
      return;
    }

    this._alertService
      .open(
        'Attention',
        `Confirmez vous la suppression du role ${role.label} pour l'utilisateur ${this.user?.completeName} ?`,
        {
          style: 'error',
          confirmLabel: 'Supprimer',
        },
      )
      .afterClosed$.subscribe((result) => {
        if (result === 'confirm') {
          this._usersService.removeRole(this.user!._id, role._id).subscribe({
            next: (user) => {
              this.user = user;
              this._notificationService.displaySuccess(
                `Le rôle ${role.label} a été supprimé`,
              );
            },
            error: (err) => (this.error = err.error.message),
          });
        }
      });
  }

  private _loadUser(): void {
    this._usersService.findOne(this._id!).subscribe({
      next: (user) => {
        this.user = user;
        if (user) {
          this._setNav();
        }
      },
      error: (err) => (this.error = err.error.message),
    });
  }

  private _setNav(): void {
    this._store.dispatch(
      setNavbarItemsAction({
        items: [
          {
            route: '/admin/users/list',
            label: 'Utilisateurs',
          },
          {
            label: this.user!.completeName,
          },
        ],
      }),
    );
  }
}
