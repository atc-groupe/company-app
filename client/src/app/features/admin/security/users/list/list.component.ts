import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalService, UsersService } from '../../../../../shared/services';
import { catchError, EMPTY, Subscription } from 'rxjs';
import { RouterLink } from '@angular/router';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SearchComponent } from '../../../../../shared/components/search/search.component';
import { User } from '../../../../../shared/entities';
import { EditComponent } from '../components/edit/edit.component';
import { setNavbarItemsAction } from '../../../../../shared/store/navbar';
import { Store } from '@ngrx/store';
import { selectUser } from '../../../../../shared/store/user/user.selectors';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { AuthActionsEnum, AuthSubjectsEnum } from '../../../../../shared/enums';
import { MessageComponent } from '../../../../../shared/components/message/message.component';

@Component({
  selector: 'admin-users-list',
  standalone: true,
  imports: [
    CommonModule,
    RouterLink,
    FormsModule,
    ReactiveFormsModule,
    SearchComponent,
    MessageComponent,
  ],
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit, AfterViewInit, OnDestroy {
  public list: User[] | null = null;
  public users: User[] | null = null;
  public error: string | null = null;
  public canManageUsers: boolean = false;
  public form = this._fb.group({
    search: [''],
  });
  private _subscription = new Subscription();

  constructor(
    private _usersService: UsersService,
    private _fb: FormBuilder,
    private _modalService: ModalService,
    private _store: Store,
  ) {
    this._store.dispatch(
      setNavbarItemsAction({
        items: [{ label: 'Utilisateurs' }],
      }),
    );

    this._store
      .select(selectUser)
      .pipe(takeUntilDestroyed())
      .subscribe((user) => {
        if (user) {
          this.canManageUsers = user.canActivate({
            subject: AuthSubjectsEnum.Users,
            action: AuthActionsEnum.Manage,
          });
        }
      });
  }

  ngOnInit() {
    this._fetchUsers();
  }

  ngAfterViewInit() {
    this._subscription.add(
      this.form.controls.search.valueChanges.subscribe((value) => {
        if (!this.users) {
          return;
        }

        if (!value || value === '') {
          this.list = this.users;
          return;
        }

        this.list = this.users.filter((user) =>
          !user.completeName
            ? false
            : user.completeName.toLowerCase().search(value.toLowerCase()) !==
              -1,
        );
      }),
    );
  }

  public onAddUser(): void {
    if (!this.canManageUsers) {
      return;
    }

    this._modalService
      .open(EditComponent, { options: { yPos: 'center' } })
      .afterClosed$.subscribe(({ action }) => {
        if (action === 'success') {
          this._fetchUsers();
        }
      });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  private _fetchUsers(): void {
    this._usersService
      .findAll()
      .pipe(
        catchError((err) => {
          this.error = err.error.message;
          return EMPTY;
        }),
      )
      .subscribe((users) => {
        this.users = users;
        this.list = users;
      });
  }
}
