import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  ModalService,
  NotificationsService,
  RolesService,
  UsersService,
} from '../../../../../../shared/services';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { ISelectItem } from '../../../../../../shared/interfaces';
import { SelectComponent } from '../../../../../../shared/components/select/select.component';
import { map, Observable, tap } from 'rxjs';
import { User } from '../../../../../../shared/entities';

@Component({
  selector: 'admin-users-add-role',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, SelectComponent],
  templateUrl: './add-role.component.html',
  styleUrls: ['./add-role.component.scss'],
})
export class AddRoleComponent {
  @Input() user!: User;
  public roles$: Observable<ISelectItem[]> = this._rolesService.findAll().pipe(
    map((roles) => {
      return roles
        .filter(
          (role) =>
            this.user.roles.find((item) => item._id === role._id) === undefined,
        )
        .map((role): ISelectItem => {
          return {
            label: role.label,
            value: role._id,
          };
        });
    }),
    tap((items) => {
      if (!items.length) {
        this.hasNoSelectableRole = true;
      }
    }),
  );

  public form = this._fb.group({
    role: ['', Validators.required],
  });

  public error: string | null = null;
  public hasNoSelectableRole: boolean = false;

  constructor(
    private _rolesService: RolesService,
    private _usersService: UsersService,
    private _fb: FormBuilder,
    private _modalService: ModalService,
    private _notificationService: NotificationsService,
  ) {}

  public onAddRole() {
    const roleId = this.form.get('role')?.value;

    if (!roleId) {
      this.error = 'Merci de sélectionner un rôle';
      return;
    }

    this._usersService
      .addRole(this.user._id, roleId)
      .subscribe(() =>
        this._notificationService.displaySuccess(`Le role a été ajouté`),
      );

    this._modalService.close({ action: 'success' });
  }

  public onCancel(): void {
    this._modalService.close({ action: 'cancel' });
  }
}
