import { Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Store } from '@ngrx/store';
import {
  FormBuilder,
  FormControl,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { SelectComponent } from '../../../../../../shared/components/select/select.component';
import {
  EmployeesService,
  ModalService,
  UsersService,
} from '../../../../../../shared/services';
import { BehaviorSubject, catchError, EMPTY, map, Observable, tap } from 'rxjs';
import { ISelectItem, IUser } from '../../../../../../shared/interfaces';
import { SwitchComponent } from '../../../../../../shared/components/switch/switch.component';
import { displaySuccessNotificationAction } from '../../../../../../shared/store/ui';

@Component({
  selector: 'admin-users-edit',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SelectComponent,
    SwitchComponent,
  ],
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
})
export class EditComponent implements OnInit {
  @Input() user?: IUser;

  public error: string | null = null;
  public employeesItems$: Observable<ISelectItem[]> = new BehaviorSubject<
    ISelectItem[]
  >([]);
  public form = this._fb.group({
    employeeNumber: new FormControl<number>(-1, Validators.required),
    userPrincipalName: ['', Validators.required],
    isAdmin: [false],
    externalAccess: [false],
  });

  constructor(
    private _store: Store,
    private _fb: FormBuilder,
    private _employeesService: EmployeesService,
    private _usersService: UsersService,
    private _modalService: ModalService,
  ) {}

  ngOnInit() {
    const employees$ = this.user
      ? this._employeesService.findAll()
      : this._employeesService.findNotRegistered();

    this.employeesItems$ = employees$.pipe(
      map((employees) => {
        return employees.map((item) => {
          return {
            label: item.name,
            value: item.employeeNumber,
          };
        });
      }),
      tap(() => {
        if (this.user) {
          this.form
            .get('employeeNumber')
            ?.setValue(this.user.mp.employeeNumber);
          this.form
            .get('userPrincipalName')
            ?.setValue(this.user.ad.userPrincipalName);
          this.form.get('isAdmin')?.setValue(this.user.isAdmin);
          this.form.get('externalAccess')?.setValue(this.user.externalAccess);
        }
      }),
      catchError((err) => {
        this.error = err.error.message;
        return EMPTY;
      }),
    );
  }

  public onCancel(): void {
    this._modalService.close({ action: 'cancel' });
  }

  public onSubmit() {
    const formData = this.form.getRawValue();

    if (!formData.employeeNumber || !formData.userPrincipalName) {
      this.error = 'Merci de renseigner toutes les données obligatoires';
      return;
    }

    const data = {
      employeeNumber: formData.employeeNumber,
      userPrincipalName: formData.userPrincipalName,
      isAdmin: formData.isAdmin!,
      externalAccess: formData.externalAccess!,
    };

    const action$ = this.user
      ? this._usersService.updateOne(this.user._id, data)
      : this._usersService.createOne(data);

    action$
      .pipe(
        catchError((err) => {
          this.error = err.error.message;
          return EMPTY;
        }),
      )
      .subscribe(() => {
        this._store.dispatch(
          displaySuccessNotificationAction({
            message: `L'utilisateur ${data.userPrincipalName} a été ${
              this.user ? 'modifié' : 'créé'
            } avec succès.`,
          }),
        );
        this._modalService.close({ action: 'success' });
      });
  }

  get title(): string {
    return this.user
      ? `Modifier ${this.user.mp.name}`
      : 'Ajouter un utilisateur';
  }
}
