import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IRole } from '../../../../../shared/interfaces';
import { User } from '../../../../../shared/entities';
import { APP_AUTHORIZATIONS } from '../../../../../shared/authorizations/app-authorizations';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import {
  AlertService,
  ModalService,
  NotificationsService,
  RolesService,
  UsersService,
} from '../../../../../shared/services';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { setNavbarItemsAction } from '../../../../../shared/store/navbar';
import { TAppAuthorizations, TAuthAction } from '../../../../../shared/types';
import { EditComponent } from '../components/edit/edit.component';
import { AuthActionsEnum, AuthSubjectsEnum } from '../../../../../shared/enums';
import { SearchComponent } from '../../../../../shared/components/search/search.component';
import { selectUser } from '../../../../../shared/store/user/user.selectors';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { MessageComponent } from '../../../../../shared/components/message/message.component';

@Component({
  selector: 'admin-roles-view2',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SearchComponent,
    RouterLink,
    MessageComponent,
  ],
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss'],
})
export class ViewComponent implements OnInit, AfterViewInit, OnDestroy {
  public role: IRole | null = null;
  public users: User[] | null = null;
  public appAuthorizations = APP_AUTHORIZATIONS;
  public error: string | null = null;
  public canManageRoles: boolean = false;
  public form = this._fb.group({
    search: [''],
  });
  private _subscription = new Subscription();
  private _id: string | null = null;

  constructor(
    private _store: Store,
    private _route: ActivatedRoute,
    private _router: Router,
    private _rolesService: RolesService,
    private _usersService: UsersService,
    private _alertService: AlertService,
    private _modalService: ModalService,
    private _fb: FormBuilder,
    private _notificationService: NotificationsService,
  ) {
    this._store
      .select(selectUser)
      .pipe(takeUntilDestroyed())
      .subscribe((user) => {
        if (user) {
          this.canManageRoles = user.canActivate({
            subject: AuthSubjectsEnum.Roles,
            action: AuthActionsEnum.Manage,
          });
        }
      });
  }

  ngOnInit() {
    this._id = this._route.snapshot.paramMap.get('id');

    this._fetchRole();

    this._usersService.findAll({ roleId: this._id! }).subscribe({
      next: (users) => (this.users = users),
      error: (err) => err.error.message,
    });
  }

  ngAfterViewInit() {
    this._subscription.add(
      this.form.controls.search.valueChanges.subscribe((search) => {
        if (!search) {
          this.appAuthorizations = APP_AUTHORIZATIONS;
          return;
        }

        search = search.toLowerCase();

        this.appAuthorizations = this._getAppAuthorizations().filter(
          (subject) => {
            subject.actions = subject.actions?.filter((action) =>
              action.description.toLowerCase().includes(search!),
            );

            subject.actionGroups = subject.actionGroups?.filter((group) => {
              group.actions = group.actions?.filter((action) =>
                action.description.toLowerCase().includes(search!),
              );

              return !!group.actions.length;
            });

            return !!subject.actions?.length || !!subject.actionGroups?.length;
          },
        );
      }),
    );
  }

  public onToggleAuthorization(
    subject: AuthSubjectsEnum,
    action?: TAuthAction,
  ): void {
    if (!this.canManageRoles) {
      return;
    }

    const authName = `${subject}.${action}`;
    const isAuthorized = this.role!.authorizations.find(
      (auth) => auth === authName,
    );

    const action$ = isAuthorized
      ? this._rolesService.removeAuthorization(this.role!._id, authName)
      : this._rolesService.addAuthorization(this.role!._id, authName);

    action$.subscribe({
      next: (role) => {
        this.role = role;
        this._notificationService.displaySuccess(
          `L'autorisation ${authName} a été ${isAuthorized ? 'supprimée' : 'ajoutée'}.`,
        );
      },
      error: (err) => {
        this.error = err.error.message;
      },
    });
  }

  public isAuthorized(subject: AuthSubjectsEnum, action: TAuthAction): boolean {
    return !!this.role?.authorizations.find(
      (auth) => auth === `${subject}.${action}`,
    );
  }

  public onDeleteRole(): void {
    if (!this.role || !this.canManageRoles) {
      return;
    }

    this._alertService
      .open(
        'Attention',
        `Confirmez vous la suppression du rôle "${this.role?.label}". (Ce role sera automatiquement supprimé pour tous les utilisateurs)`,
        {
          style: 'error',
          confirmLabel: 'Supprimer',
        },
      )
      .afterClosed$.subscribe((result) => {
        if (result === 'confirm') {
          this._rolesService.deleteOne(this.role!._id).subscribe({
            next: () =>
              this._notificationService.displaySuccess(
                `Le rôle ${this.role!.label} a été supprimé`,
              ),
            error: (err) => (this.error = err.error.message),
          });

          this._router.navigateByUrl('/admin/roles/list');
        }
      });
  }

  public onEditRole(): void {
    if (!this.canManageRoles) {
      return;
    }

    this._modalService
      .open(EditComponent, {
        options: { yPos: 'center' },
        inputs: { role: this.role },
      })
      .afterClosed$.subscribe(({ result }) => {
        if (result === 'success') {
          this._fetchRole();
        }
      });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  private _getAppAuthorizations(): TAppAuthorizations {
    return JSON.parse(JSON.stringify(APP_AUTHORIZATIONS));
  }

  private _fetchRole(): void {
    this._rolesService.findOne(this._id!).subscribe({
      next: (role) => {
        this.role = role;
        this._setNav();
      },
      error: (err) => (this.error = err.error.message),
    });
  }

  private _setNav(): void {
    this._store.dispatch(
      setNavbarItemsAction({
        items: [
          {
            route: '/admin/roles',
            label: 'Roles utilisateurs',
          },
          {
            label: `${this.role?.label}`,
          },
        ],
      }),
    );
  }
}
