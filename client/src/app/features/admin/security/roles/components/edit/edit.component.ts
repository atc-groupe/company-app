import { Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { displaySuccessNotificationAction } from '../../../../../../shared/store/ui';
import {
  ModalService,
  NotificationsService,
  RolesService,
} from '../../../../../../shared/services';
import { catchError, EMPTY } from 'rxjs';
import { IRole } from '../../../../../../shared/interfaces';
import { IRoleDto } from '../../../../../../shared/dto';
import { MessageComponent } from '../../../../../../shared/components/message/message.component';
import { CancelButtonComponent } from '../../../../../../shared/components/cancel-button/cancel-button.component';
import { ModalCloseDirective } from '../../../../../../shared/directives/modal-close.directive';
import { SaveButtonComponent } from '../../../../../../shared/components/save-button/save-button.component';

@Component({
  selector: 'admin-roles-edit',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MessageComponent,
    CancelButtonComponent,
    ModalCloseDirective,
    SaveButtonComponent,
  ],
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
})
export class EditComponent implements OnInit {
  @Input() role?: IRole;
  public title: string;
  public form = this._fb.group({
    label: ['', Validators.required],
    description: ['', Validators.required],
  });
  public error: string | null = null;

  constructor(
    private _fb: FormBuilder,
    private _store: Store,
    private _rolesService: RolesService,
    private _modalService: ModalService,
    private _notificationsService: NotificationsService,
  ) {
    this.title = 'Nouveau role';
  }

  ngOnInit() {
    if (this.role) {
      this.title = `Modifier le rôle ${this.role.label}`;
      this.form.controls.label.setValue(this.role.label);
      this.form.controls.description.setValue(this.role.description);
    }
  }

  onSubmit() {
    const role = this.form.getRawValue();
    if (!role.label || !role.description) {
      this.error = 'Merci de renseigner les champs obligatoires';
      return;
    }

    const dto: IRoleDto = {
      label: role.label,
      description: role.description,
    };

    const action$ = this.role
      ? this._rolesService.updateOne(this.role._id, dto)
      : this._rolesService.createOne(dto);

    action$
      .pipe(
        catchError((err) => {
          this.error = err.error.message;
          return EMPTY;
        }),
      )
      .subscribe({
        next: (role) => {
          this._notificationsService.displaySuccess(
            `Le rôle ${role.label} a été créé avec succès.`,
          );
          this._modalService.close({ action: 'success' });
        },
        error: (err) => (this.error = err.error.message),
      });
  }
}
