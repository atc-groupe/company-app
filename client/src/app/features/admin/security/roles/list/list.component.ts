import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterLink } from '@angular/router';
import { Store } from '@ngrx/store';
import { setNavbarItemsAction } from '../../../../../shared/store/navbar';
import { IRole } from '../../../../../shared/interfaces';
import { ModalService, RolesService } from '../../../../../shared/services';
import { catchError, EMPTY, Subscription } from 'rxjs';
import { SearchComponent } from '../../../../../shared/components/search/search.component';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EditComponent } from '../components/edit/edit.component';
import { selectUser } from '../../../../../shared/store/user/user.selectors';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { AuthActionsEnum, AuthSubjectsEnum } from '../../../../../shared/enums';

@Component({
  selector: 'admin-roles-list',
  standalone: true,
  imports: [
    CommonModule,
    RouterLink,
    SearchComponent,
    FormsModule,
    ReactiveFormsModule,
  ],
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit, AfterViewInit, OnDestroy {
  public list: IRole[] | null = null;
  public error: string | null = null;
  public canManageRoles: boolean = false;
  public form = this._fb.group({
    search: [''],
  });
  private _subscription = new Subscription();
  private roles: IRole[] | null = null;

  constructor(
    private _store: Store,
    private _rolesService: RolesService,
    private _fb: FormBuilder,
    private _modalService: ModalService,
  ) {
    this._store
      .select(selectUser)
      .pipe(takeUntilDestroyed())
      .subscribe((user) => {
        if (user) {
          this.canManageRoles = user.canActivate({
            subject: AuthSubjectsEnum.Roles,
            action: AuthActionsEnum.Manage,
          });
        }
      });
  }

  ngOnInit() {
    this._fetchRoles();

    this._store.dispatch(
      setNavbarItemsAction({
        items: [{ label: 'Roles utilisateurs' }],
      }),
    );
  }

  ngAfterViewInit() {
    this._subscription.add(
      this.form.get('search')?.valueChanges.subscribe((value) => {
        if (!this.roles) {
          return;
        }

        if (!value || value === '') {
          this.list = this.roles;
          return;
        }

        this.list = this.roles.filter(
          (role) => role.label.toLowerCase().search(value.toLowerCase()) !== -1,
        );
      }),
    );
  }

  public onAddRole(): void {
    if (!this.canManageRoles) {
      return;
    }

    this._modalService
      .open(EditComponent, { options: { yPos: 'center' } })
      .afterClosed$.subscribe(({ action }) => {
        if (action === 'success') {
          this._fetchRoles();
        }
      });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  private _fetchRoles(): void {
    this._rolesService
      .findAll()
      .pipe(
        catchError((err) => {
          this.error = err.error.message;
          return EMPTY;
        }),
      )
      .subscribe((roles) => {
        this.roles = roles;
        this.list = roles;
      });
  }
}
