import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DevicesMappingAddComponent } from './devices-mapping-add.component';

describe('DevicesMappingAddComponent', () => {
  let component: DevicesMappingAddComponent;
  let fixture: ComponentFixture<DevicesMappingAddComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [DevicesMappingAddComponent]
    });
    fixture = TestBed.createComponent(DevicesMappingAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
