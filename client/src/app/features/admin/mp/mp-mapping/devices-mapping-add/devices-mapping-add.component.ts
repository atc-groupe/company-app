import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  AppSettingsDevicesService,
  ModalService,
} from '../../../../../shared/services';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { CancelButtonComponent } from '../../../../../shared/components/cancel-button/cancel-button.component';
import { ModalCloseDirective } from '../../../../../shared/directives/modal-close.directive';
import { SaveButtonComponent } from '../../../../../shared/components/save-button/save-button.component';
import { MessageComponent } from '../../../../../shared/components/message/message.component';

@Component({
  selector: 'admin-mp-mapping-devices-mapping-add',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CancelButtonComponent,
    ModalCloseDirective,
    SaveButtonComponent,
    MessageComponent,
  ],
  templateUrl: './devices-mapping-add.component.html',
  styleUrls: ['./devices-mapping-add.component.scss'],
})
export class DevicesMappingAddComponent {
  public error: string | null = null;
  public form = this._fb.group({
    mpName: ['', Validators.required],
    displayName: ['', Validators.required],
  });

  constructor(
    private _modalService: ModalService,
    private _devicesService: AppSettingsDevicesService,
    private _fb: FormBuilder,
  ) {}

  public onSubmit(): void {
    const { mpName, displayName } = this.form.getRawValue();

    if (!mpName || !displayName) {
      this.error = 'Merci de compléter tous les champs requis.';
      return;
    }

    this._devicesService.addMappingName({ mpName, displayName }).subscribe({
      next: (settings) =>
        this._modalService.close({ action: 'save', settings }),
      error: (err) => (this.error = err.error.message),
    });
  }
}
