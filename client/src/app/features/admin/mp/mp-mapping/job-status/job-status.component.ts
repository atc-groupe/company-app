import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  IAppSettingsJobStatus,
  IAppSettingsJobStatuses,
} from '../../../../../shared/interfaces';
import { RouterLink } from '@angular/router';
import {
  AlertService,
  AppSettingsJobService,
  ModalService,
  NotificationsService,
} from '../../../../../shared/services';
import { IAppSettings } from '../../../../../shared/interfaces';
import { SearchComponent } from '../../../../../shared/components/search/search.component';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { Subscription } from 'rxjs';
import { JobStatusEditComponent } from '../job-status-edit/job-status-edit.component';
import { ColorPickerComponent } from '../../../../../shared/components/color-picker/color-picker.component';
import { MessageComponent } from '../../../../../shared/components/message/message.component';

@Component({
  selector: 'admin-mp-mapping-job-status-list',
  standalone: true,
  imports: [
    CommonModule,
    RouterLink,
    SearchComponent,
    ReactiveFormsModule,
    ColorPickerComponent,
    MessageComponent,
  ],
  templateUrl: './job-status.component.html',
  styleUrls: ['./job-status.component.scss'],
})
export class JobStatusComponent implements OnInit, OnDestroy {
  @Input({ required: true }) statuses: IAppSettingsJobStatuses | null = null;
  @Input({ required: true }) canManageMappingData: boolean = false;
  @Output() settingsChange = new EventEmitter<IAppSettings>();

  public error: string | null = null;
  public form = this._fb.group({ search: [''], defaultColor: [''] });
  public filteredList: IAppSettingsJobStatus[] = [];

  private _subscription = new Subscription();

  constructor(
    private _modalService: ModalService,
    private _appSettingsJobService: AppSettingsJobService,
    private _alertService: AlertService,
    private _fb: FormBuilder,
    private _notificationsService: NotificationsService,
  ) {}

  ngOnInit() {
    const search = this.form.controls.search;
    const defaultColor = this.form.controls.defaultColor;

    if (!this.statuses || !search || !defaultColor) {
      return;
    }

    this.filteredList = this.statuses.items;
    this.form.controls.defaultColor.setValue(this.statuses.defaultColor);

    this._subscription.add(
      search.valueChanges.subscribe((value) => {
        if (!value) {
          this.filteredList = this.statuses!.items;
        }

        this.filteredList = this.statuses!.items.filter(
          (item) =>
            item.label.toLowerCase().search(value!.toLowerCase()) !== -1,
        );
      }),
    );

    this._subscription.add(
      defaultColor.valueChanges.subscribe((value) => {
        if (!value) {
          return;
        }

        this._appSettingsJobService
          .changeStatusesDefaultColor(value)
          .subscribe({
            next: (settings) => {
              this.settingsChange.next(settings);
              this._notificationsService.displaySuccess(
                `La couleur par défaut des statuts du job a été modifiée`,
              );
            },
            error: (err) => {
              this.error = err.error.message;
            },
          });
      }),
    );
  }

  public onEditJobStatus(status?: IAppSettingsJobStatus): void {
    if (!this.canManageMappingData) {
      return;
    }

    this._modalService
      .open(JobStatusEditComponent, {
        inputs: { status },
        options: { yPos: 'center' },
      })
      .afterClosed$.subscribe(
        (result: { action?: 'save'; settings?: IAppSettings }) => {
          if (result.action === 'save' && result.settings) {
            this.filteredList = result.settings.job.statuses.items;
            this.settingsChange.emit(result.settings);

            const message = status
              ? `Le statut ${status.label} a été modifié avec succès`
              : 'Le statut a été ajouté';
            this._notificationsService.displaySuccess(message);
          }
        },
      );
  }

  public onDeleteJobStatus(status: IAppSettingsJobStatus): void {
    if (!this.canManageMappingData) {
      return;
    }

    this._alertService
      .open(
        'Attention',
        `Etes vous sure de vouloir supprimer le mapping du statut ${status.label} ?`,
        {
          style: 'error',
          confirmLabel: 'Supprimer',
        },
      )
      .afterClosed$.subscribe((result) => {
        if (result === 'confirm') {
          this._appSettingsJobService.removeStatus(status._id).subscribe({
            next: (settings) => {
              this.filteredList = settings.job.statuses.items;
              this.settingsChange.emit(settings);
              this._notificationsService.displaySuccess(
                `Le statut ${status.label} a été supprimé de la liste`,
              );
            },
            error: (err) => (this.error = err.error.message),
          });
        }
      });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
