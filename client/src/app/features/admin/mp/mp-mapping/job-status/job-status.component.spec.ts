import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobStatusComponent } from './job-status.component';

describe('JobStatusListComponent', () => {
  let component: JobStatusComponent;
  let fixture: ComponentFixture<JobStatusComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [JobStatusComponent],
    });
    fixture = TestBed.createComponent(JobStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
