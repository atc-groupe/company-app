import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MpMappingComponent } from './mp-mapping.component';

describe('MpMappingComponent', () => {
  let component: MpMappingComponent;
  let fixture: ComponentFixture<MpMappingComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MpMappingComponent]
    });
    fixture = TestBed.createComponent(MpMappingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
