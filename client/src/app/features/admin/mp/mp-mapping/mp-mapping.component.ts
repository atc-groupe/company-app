import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';
import { Store } from '@ngrx/store';
import { setNavbarItemsAction } from '../../../../shared/store/navbar';

@Component({
  selector: 'admin-mp-mapping',
  standalone: true,
  imports: [CommonModule, RouterOutlet],
  templateUrl: './mp-mapping.component.html',
  styleUrls: ['./mp-mapping.component.scss'],
})
export class MpMappingComponent {
  constructor(private _store: Store) {
    this._store.dispatch(
      setNavbarItemsAction({
        items: [{ label: 'Paramètres de mapping MultiPress' }],
      }),
    );
  }
}
