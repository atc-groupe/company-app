import { Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  FormBuilder,
  FormControl,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import {
  AppSettingsJobService,
  JobsService,
  ModalService,
} from '../../../../../shared/services';
import { map, Observable } from 'rxjs';
import { SelectComponent } from '../../../../../shared/components/select/select.component';
import {
  IAppSettingsJobStatus,
  ISelectItem,
} from '../../../../../shared/interfaces';
import { ColorPickerComponent } from '../../../../../shared/components/color-picker/color-picker.component';
import { IAppSettingsJobStatusDto } from '../../../../../shared/dto';
import { MessageComponent } from '../../../../../shared/components/message/message.component';
import { CancelButtonComponent } from '../../../../../shared/components/cancel-button/cancel-button.component';
import { ModalCloseDirective } from '../../../../../shared/directives/modal-close.directive';
import { SaveButtonComponent } from '../../../../../shared/components/save-button/save-button.component';

@Component({
  selector: 'admin-mp-mapping-job-status-edit',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SelectComponent,
    ColorPickerComponent,
    MessageComponent,
    CancelButtonComponent,
    ModalCloseDirective,
    SaveButtonComponent,
  ],
  templateUrl: './job-status-edit.component.html',
  styleUrls: ['./job-status-edit.component.scss'],
})
export class JobStatusEditComponent implements OnInit {
  @Input() status: IAppSettingsJobStatus | null = null;
  public error: string | null = null;
  public statuses$: Observable<ISelectItem[]>;
  public textColors: ISelectItem[] = [
    { label: 'Blanc', value: 'white' },
    { label: 'Noir', value: 'black' },
  ];
  public form = this._fb.group({
    statusNumber: new FormControl<number | null>(null, Validators.required),
    label: ['', Validators.required],
    bgColor: ['', Validators.required],
    textColor: new FormControl<'white' | 'black'>('white', Validators.required),
  });

  constructor(
    private _fb: FormBuilder,
    private _jobsService: JobsService,
    private _appSettingsJobService: AppSettingsJobService,
    private _modalService: ModalService,
  ) {
    this.statuses$ = this._jobsService.getStatusesList().pipe(
      map((statuses) =>
        statuses.map((status): ISelectItem => {
          return {
            label: status.label,
            value: status.number,
          };
        }),
      ),
    );
  }

  ngOnInit() {
    if (this.status) {
      this.form.controls.statusNumber.setValue(this.status.statusNumber);
      this.form.controls.label.setValue(this.status.label);
      this.form.controls.bgColor.setValue(this.status.bgColor);
      this.form.controls.textColor.setValue(this.status.textColor);
    }
  }

  onSubmit(): void {
    const data = this.form.getRawValue();

    if (!data.statusNumber || !data.label || !data.bgColor || !data.textColor) {
      this.error = 'Merci de compléter tous les champs requis';

      return;
    }

    const dto: IAppSettingsJobStatusDto = {
      statusNumber: data.statusNumber,
      label: data.label,
      bgColor: data.bgColor,
      textColor: data.textColor,
    };

    const action = this.status
      ? this._appSettingsJobService.updateStatus(this.status._id, dto)
      : this._appSettingsJobService.addStatus(dto);

    action.subscribe({
      next: (settings) =>
        this._modalService.close({ action: 'save', settings }),
      error: (err) => (this.error = err.error.message),
    });
  }
}
