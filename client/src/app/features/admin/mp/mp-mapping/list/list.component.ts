import { Component, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IAppSettings } from '../../../../../shared/interfaces';
import { RouterLink } from '@angular/router';
import { JobMappingMetaFilterListComponent } from '../job-mapping-meta-filter-list/job-mapping-meta-filter-list.component';
import { JobStatusComponent } from '../job-status/job-status.component';
import { DevicesMappingListComponent } from '../devices-mapping-list/devices-mapping-list.component';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { selectAppSettings } from '../../../../../shared/store/app-settings/app-settings.selectors';
import { selectUser } from '../../../../../shared/store/user/user.selectors';
import {
  AuthActionsSettingsEnum,
  AuthSubjectsEnum,
} from '../../../../../shared/enums';

@Component({
  selector: 'admin-mp-mapping-list',
  standalone: true,
  imports: [
    CommonModule,
    RouterLink,
    JobStatusComponent,
    JobMappingMetaFilterListComponent,
    DevicesMappingListComponent,
  ],
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit, OnDestroy {
  public appSettings: IAppSettings | null = null;
  public error: string | null = null;
  public canManageMappingData: boolean = false;
  private _subscription = new Subscription();

  constructor(private _store: Store) {}

  ngOnInit() {
    this._subscription.add(
      this._store.select(selectAppSettings).subscribe((settings) => {
        this.appSettings = settings;
      }),
    );

    this._subscription.add(
      this._store.select(selectUser).subscribe((user) => {
        if (user) {
          this.canManageMappingData = user.canActivate({
            subject: AuthSubjectsEnum.Settings,
            action: AuthActionsSettingsEnum.ManageMappingParameters,
          });
        }
      }),
    );
  }

  public onSettingsChange(settings: IAppSettings): void {
    this.appSettings = settings;
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
