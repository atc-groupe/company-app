import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  IAppSettingsJobMetaFilters,
  IAppSettings,
} from '../../../../../shared/interfaces';
import { TAppSettingsJobMappingMetaFilter } from '../../../../../shared/types';
import { IAppSettingsJobMetaFilter } from '../../../../../shared/interfaces/app-settings/i-app-settings-job-meta-filter';
import { APP_SETTINGS_JOB_MAPPING_META_FILTERS } from '../../../../../shared/constants';
import {
  AlertService,
  AppSettingsJobService,
  ModalService,
  NotificationsService,
} from '../../../../../shared/services';
import { JobMappingMetaFilterAddComponent } from '../job-mapping-meta-filter-add/job-mapping-meta-filter-add.component';

@Component({
  selector: 'admin-mp-mapping-job-mapping-meta-filter-list',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './job-mapping-meta-filter-list.component.html',
  styleUrls: ['./job-mapping-meta-filter-list.component.scss'],
})
export class JobMappingMetaFilterListComponent {
  @Input({ required: true }) metaFilters: IAppSettingsJobMetaFilters | null =
    null;
  @Input({ required: true }) canManageMappingData: boolean = false;
  @Output() settingsChange = new EventEmitter<IAppSettings>();

  public META_FILTERS = APP_SETTINGS_JOB_MAPPING_META_FILTERS;
  public error: string | null = null;

  constructor(
    private _appSettingsJobService: AppSettingsJobService,
    private _modalService: ModalService,
    private _alertService: AlertService,
    private _notificationsService: NotificationsService,
  ) {}

  public getMetaFilter(
    name: TAppSettingsJobMappingMetaFilter,
  ): IAppSettingsJobMetaFilter {
    return this.metaFilters![name];
  }

  public onAddMetaFilter(title: string, name: string): void {
    if (!this.canManageMappingData) {
      return;
    }

    this._modalService
      .open(JobMappingMetaFilterAddComponent, {
        inputs: { title, name },
        options: { yPos: 'center' },
      })
      .afterClosed$.subscribe(
        (result: { action?: 'save'; settings?: IAppSettings }) => {
          if (result.action === 'save' && result.settings) {
            this.settingsChange.emit(result.settings);
            this._notificationsService.displaySuccess(
              `Le filtre a été ajouté à la liste "${title}"`,
            );
          }
        },
      );
  }

  public onDeleteJobMappingMetaFilter(name: string, value: string): void {
    if (!this.canManageMappingData) {
      return;
    }

    this._alertService
      .open(
        'Attention',
        `Etes vous sure de vouloir supprimer  "${value}" du filtre de métadonnée ${name} ?`,
        {
          style: 'error',
          confirmLabel: 'Supprimer',
        },
      )
      .afterClosed$.subscribe((result) => {
        if (result === 'confirm') {
          this._appSettingsJobService
            .removeMappingMetadataItem({ name, value })
            .subscribe({
              next: (settings) => {
                this.settingsChange.emit(settings);
                this._notificationsService.displaySuccess(
                  `${value} a été supprimée du filtre "${name}".`,
                );
              },
              error: (err) => (this.error = err.error.message),
            });
        }
      });
  }
}
