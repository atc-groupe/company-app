import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobMappingMetaFilterListComponent } from './job-mapping-meta-filter-list.component';

describe('JobMappingMetaFilterListComponent', () => {
  let component: JobMappingMetaFilterListComponent;
  let fixture: ComponentFixture<JobMappingMetaFilterListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [JobMappingMetaFilterListComponent]
    });
    fixture = TestBed.createComponent(JobMappingMetaFilterListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
