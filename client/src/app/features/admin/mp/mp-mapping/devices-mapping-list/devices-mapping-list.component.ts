import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  IAppSettingsDevices,
  IAppSettingsDevicesMappingName,
  IAppSettings,
} from '../../../../../shared/interfaces';
import {
  AlertService,
  AppSettingsDevicesService,
  ModalService,
  NotificationsService,
} from '../../../../../shared/services';
import { DevicesMappingAddComponent } from '../devices-mapping-add/devices-mapping-add.component';
import { MessageComponent } from '../../../../../shared/components/message/message.component';

@Component({
  selector: 'admin-mp-mapping-devices-mapping-list',
  standalone: true,
  imports: [CommonModule, MessageComponent],
  templateUrl: './devices-mapping-list.component.html',
  styleUrls: ['./devices-mapping-list.component.scss'],
})
export class DevicesMappingListComponent {
  @Input({ required: true }) devices: IAppSettingsDevices | null = null;
  @Input({ required: true }) canManageMappingData: boolean = false;
  @Output() settingsChange = new EventEmitter<IAppSettings>();

  public error: string | null = null;

  constructor(
    private _modalService: ModalService,
    private _alertService: AlertService,
    private _devicesService: AppSettingsDevicesService,
    private _notificationsService: NotificationsService,
  ) {}

  public addMappingName(): void {
    if (!this.canManageMappingData) {
      return;
    }

    this._modalService
      .open(DevicesMappingAddComponent, {
        options: { yPos: 'center' },
      })
      .afterClosed$.subscribe(
        (result: { action?: 'save'; settings?: IAppSettings }) => {
          if (result.action === 'save' && result.settings) {
            this.settingsChange.emit(result.settings);
          }
        },
      );
  }

  public removeMappingName(name: IAppSettingsDevicesMappingName): void {
    if (!this.canManageMappingData) {
      return;
    }

    const title = `${name.mpName} > ${name.displayName}`;
    this._alertService
      .open(
        'Attention',
        `Etes-vous sure de vouloir supprimer le mapping "${title}" ?`,
        {
          style: 'error',
          confirmLabel: 'Supprimer',
        },
      )
      .afterClosed$.subscribe((result) => {
        if (result === 'confirm') {
          this._devicesService.removeMappingName(name.mpName).subscribe({
            next: (settings) => {
              this.settingsChange.emit(settings);
              this._notificationsService.displaySuccess(
                `Le mapping "${title}" a été supprimé.`,
              );
            },
            error: (err) => (this.error = err.error.message),
          });
        }
      });
  }
}
