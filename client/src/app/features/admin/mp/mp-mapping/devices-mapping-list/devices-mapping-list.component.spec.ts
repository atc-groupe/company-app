import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DevicesMappingListComponent } from './devices-mapping-list.component';

describe('DevicesMappingListComponent', () => {
  let component: DevicesMappingListComponent;
  let fixture: ComponentFixture<DevicesMappingListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [DevicesMappingListComponent]
    });
    fixture = TestBed.createComponent(DevicesMappingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
