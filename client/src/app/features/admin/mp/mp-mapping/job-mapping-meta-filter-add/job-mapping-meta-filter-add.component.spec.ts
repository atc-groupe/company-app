import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobMappingMetaFilterAddComponent } from './job-mapping-meta-filter-add.component';

describe('JobMetaFilterAddComponent', () => {
  let component: JobMappingMetaFilterAddComponent;
  let fixture: ComponentFixture<JobMappingMetaFilterAddComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [JobMappingMetaFilterAddComponent],
    });
    fixture = TestBed.createComponent(JobMappingMetaFilterAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
