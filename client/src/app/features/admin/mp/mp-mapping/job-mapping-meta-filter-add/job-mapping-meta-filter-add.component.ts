import { Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import {
  AppSettingsJobService,
  ModalService,
} from '../../../../../shared/services';
import { CancelButtonComponent } from '../../../../../shared/components/cancel-button/cancel-button.component';
import { SaveButtonComponent } from '../../../../../shared/components/save-button/save-button.component';
import { ModalCloseDirective } from '../../../../../shared/directives/modal-close.directive';

@Component({
  selector: 'admin-mp-mapping-job-meta-filter-add',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CancelButtonComponent,
    SaveButtonComponent,
    ModalCloseDirective,
  ],
  templateUrl: './job-mapping-meta-filter-add.component.html',
  styleUrls: ['./job-mapping-meta-filter-add.component.scss'],
})
export class JobMappingMetaFilterAddComponent implements OnInit {
  @Input() name!: string;
  @Input() title!: string;
  public error: string | null = null;
  public form = this._fb.group({
    value: ['', Validators.required],
  });

  constructor(
    private _fb: FormBuilder,
    private _modalService: ModalService,
    private _jobsService: AppSettingsJobService,
  ) {}

  public onSubmit(): void {
    const value = this.form.controls.value.value;

    if (!value || !this.name) {
      this.error = "Merci d'entrer une valeur";

      return;
    }

    this._jobsService
      .addMappingMetadataItem({ name: this.name, value })
      .subscribe({
        next: (settings) =>
          this._modalService.close({ action: 'save', settings }),
        error: (err) => (this.error = err.error.message),
      });
  }

  ngOnInit() {
    console.log(this.name, this.title);
  }
}
