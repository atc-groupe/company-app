import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JobsSyncComponent } from './jobs-sync/jobs-sync.component';
import { JobsSyncLogsComponent } from './jobs-sync-logs/jobs-sync-logs.component';
import { StockSyncComponent } from './stock-sync/stock-sync.component';
import { Store } from '@ngrx/store';
import { setNavbarItemsAction } from '../../../../shared/store/navbar';

@Component({
  selector: 'admin-mp-sync',
  standalone: true,
  imports: [
    CommonModule,
    JobsSyncComponent,
    JobsSyncLogsComponent,
    StockSyncComponent,
  ],
  templateUrl: './mp-sync.component.html',
  styleUrls: ['./mp-sync.component.scss'],
})
export class MpSyncComponent {
  constructor(private _store: Store) {
    this._store.dispatch(
      setNavbarItemsAction({
        items: [{ label: 'Synchronisation MultiPress' }],
      }),
    );
  }
}
