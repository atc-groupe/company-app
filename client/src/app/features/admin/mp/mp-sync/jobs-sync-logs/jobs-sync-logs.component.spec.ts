import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobsSyncLogsComponent } from './jobs-sync-logs.component';

describe('SyncLogsComponent', () => {
  let component: JobsSyncLogsComponent;
  let fixture: ComponentFixture<JobsSyncLogsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [JobsSyncLogsComponent],
    });
    fixture = TestBed.createComponent(JobsSyncLogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
