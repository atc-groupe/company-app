import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogsService, WsLogsService } from '../../../../../shared/services';
import { LogContextEnum, LogLevelEnum } from '../../../../../shared/enums';
import { ILog } from '../../../../../shared/interfaces';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { BehaviorSubject, Subscription } from 'rxjs';
import { SearchComponent } from '../../../../../shared/components/search/search.component';

@Component({
  selector: 'admin-jobs-sync-logs',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CommonModule, ReactiveFormsModule, SearchComponent],
  templateUrl: './jobs-sync-logs.component.html',
  styleUrls: ['./jobs-sync-logs.component.scss'],
})
export class JobsSyncLogsComponent implements OnInit, OnDestroy {
  public error: string | null = null;
  public isLoading = true;
  public logLevel: LogLevelEnum = LogLevelEnum.log;
  public logs: ILog[] = [];
  public filteredLogs$ = new BehaviorSubject<ILog[]>([]);
  public form = this._fb.group({
    search: [''],
  });
  protected readonly LogLevelEnum = LogLevelEnum;
  private _subscription = new Subscription();

  constructor(
    private _logsService: LogsService,
    private _wsLogs: WsLogsService,
    private _fb: FormBuilder,
  ) {}

  ngOnInit() {
    this._fetchLogs();
    this._wsLogs.connect();

    this._subscription.add(
      this.form.get('search')?.valueChanges.subscribe(() => {
        this._updateFilteredLogs();
      }),
    );

    this._subscription.add(
      this._wsLogs.onLog().subscribe((log) => {
        if (
          (log.level === LogLevelEnum.log &&
            this.logLevel !== LogLevelEnum.log) ||
          (log.level === LogLevelEnum.warn &&
            this.logLevel === LogLevelEnum.log)
        ) {
          return;
        }

        this.logs.unshift(log);
        this._updateFilteredLogs();
      }),
    );
  }

  public setLevel(level: LogLevelEnum): void {
    this.logLevel = level;
    this._fetchLogs();
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
    this._wsLogs.disconnect();
  }

  private _fetchLogs(): void {
    this._logsService
      .find({ context: LogContextEnum.jobSync, level: this.logLevel })
      .subscribe({
        next: (logs) => {
          this.logs = logs;
          this.filteredLogs$.next(logs);
          this.isLoading = false;
        },
        error: (err) => {
          this.error = err.error.message;
        },
      });
  }

  private _updateFilteredLogs() {
    const search = this.form.get('search')?.value;
    if (!search) {
      this.filteredLogs$.next(this.logs);
      return;
    }

    this.filteredLogs$.next(
      this.logs.filter((item) =>
        `${item.message}`.toLowerCase().includes(search.toLowerCase()),
      ),
    );
  }
}
