import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MpSyncComponent } from './mp-sync.component';

describe('MpSyncComponent', () => {
  let component: MpSyncComponent;
  let fixture: ComponentFixture<MpSyncComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MpSyncComponent]
    });
    fixture = TestBed.createComponent(MpSyncComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
