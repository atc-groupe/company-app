import { Component, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  AppSettingsJobService,
  JobsSyncService,
  NotificationsService,
} from '../../../../../shared/services';
import {
  FormBuilder,
  FormControl,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { Store } from '@ngrx/store';
import { SpinnerComponent } from '../../../../../shared/components/spinner/spinner.component';
import { IActivesJobsSyncResult } from '../../../../../shared/interfaces';
import { Subscription, tap } from 'rxjs';
import { selectAppSettings } from '../../../../../shared/store/app-settings/app-settings.selectors';
import { selectUser } from '../../../../../shared/store/user/user.selectors';
import {
  AuthActionsJobsEnum,
  AuthSubjectsEnum,
} from '../../../../../shared/enums';
import { MessageComponent } from '../../../../../shared/components/message/message.component';
import { SaveButtonComponent } from '../../../../../shared/components/save-button/save-button.component';

@Component({
  selector: 'admin-jobs-sync',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SpinnerComponent,
    MessageComponent,
    SaveButtonComponent,
  ],
  templateUrl: './jobs-sync.component.html',
  styleUrls: ['./jobs-sync.component.scss'],
})
export class JobsSyncComponent implements OnInit, OnDestroy {
  public syncOneError: string | null = null;
  public syncAllError: string | null = null;
  public syncIntervalError: string | null = null;
  public syncOneActive = false;
  public syncAllActive = false;
  public syncAllResult: IActivesJobsSyncResult | null = null;
  public canSyncAllJobs: boolean = false;
  public syncOneForm = this._fb.group({
    mpNumber: new FormControl<number | null>(null, Validators.required),
  });
  public reSyncForm = this._fb.group({
    syncInterval: new FormControl<number | null>(null, Validators.required),
  });
  private _subscription = new Subscription();

  constructor(
    private _jobsSyncService: JobsSyncService,
    private _settingsJobService: AppSettingsJobService,
    private _fb: FormBuilder,
    private _store: Store,
    private _notificationsService: NotificationsService,
  ) {}

  ngOnInit() {
    this._subscription.add(
      this._store.select(selectAppSettings).subscribe((settings) => {
        if (settings) {
          this.reSyncForm.controls.syncInterval.setValue(
            settings?.job.sync.syncInterval,
          );
        }
      }),
    );

    this._subscription.add(
      this._store.select(selectUser).subscribe((user) => {
        if (user) {
          this.canSyncAllJobs = user.canActivate({
            subject: AuthSubjectsEnum.Jobs,
            action: AuthActionsJobsEnum.SyncAllJobs,
          });
        }
      }),
    );
  }

  public syncOne(): void {
    this.syncOneError = null;
    const mpNumber = this.syncOneForm.get('mpNumber')?.value;
    if (!mpNumber) {
      this.syncOneError = "L'id du job est vide";
      return;
    }
    this.syncOneActive = true;
    this._jobsSyncService
      .syncOne(mpNumber)
      .pipe(tap(() => (this.syncOneActive = false)))
      .subscribe({
        next: () => {
          this._notificationsService.displaySuccess(
            `Le job ${mpNumber} a été synchronisé avec succès`,
          );
        },
        error: (err) => {
          this.syncOneError = err.error.message;
        },
      });
  }

  public syncAllActiveJobs(): void {
    if (!this.canSyncAllJobs) {
      return;
    }

    this.syncAllActive = true;
    this.syncAllError = null;
    this.syncAllResult = null;

    this._jobsSyncService
      .syncActives()
      .pipe(tap(() => (this.syncAllActive = false)))
      .subscribe({
        next: (result) => {
          this.syncAllResult = result;

          if (result.status === 'error') {
            this._notificationsService.displayError(
              'Echec de la synchronisation',
            );
          } else {
            this._notificationsService.displaySuccess(
              `Les ${result.jobsSyncCount} jobs ont été synchronisés avec succès`,
            );
          }
        },
        error: (err) => {
          this.syncAllError = err.error.message;
        },
      });
  }

  public get syncAllMessageClass(): string | null {
    if (!this.syncAllResult) {
      return null;
    }

    return `message-${this.syncAllResult.status}`;
  }

  public saveSyncInterval(): void {
    const syncInterval = this.reSyncForm.get('syncInterval')?.value;

    if (!syncInterval) {
      this.syncIntervalError = 'Valeur obligatoire';
      return;
    }

    this._settingsJobService.updateSync({ syncInterval }).subscribe({
      next: () =>
        this._notificationsService.displaySuccess(
          'Le délai de re-synchronisation a été mis à jour',
        ),
      error: (err) => (this.syncIntervalError = err.error.message),
    });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
