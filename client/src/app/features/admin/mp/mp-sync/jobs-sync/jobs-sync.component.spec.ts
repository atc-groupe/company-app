import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobsSyncComponent } from './jobs-sync.component';

describe('JobsSyncComponent', () => {
  let component: JobsSyncComponent;
  let fixture: ComponentFixture<JobsSyncComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [JobsSyncComponent]
    });
    fixture = TestBed.createComponent(JobsSyncComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
