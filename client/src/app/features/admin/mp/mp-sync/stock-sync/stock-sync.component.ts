import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StockService } from '../../../../../shared/services';
import { SpinnerComponent } from '../../../../../shared/components/spinner/spinner.component';
import { selectUser } from '../../../../../shared/store/user/user.selectors';
import {
  AuthActionsProductsEnum,
  AuthSubjectsEnum,
} from '../../../../../shared/enums';
import { Store } from '@ngrx/store';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { MessageComponent } from '../../../../../shared/components/message/message.component';

@Component({
  selector: 'admin-stock-sync',
  standalone: true,
  imports: [CommonModule, SpinnerComponent, MessageComponent],
  templateUrl: './stock-sync.component.html',
  styleUrls: ['./stock-sync.component.scss'],
})
export class StockSyncComponent {
  public isLoading = false;
  public error: string | null = null;
  public success: boolean | null = null;
  public canSyncAllProducts: boolean = false;

  constructor(
    private _stockService: StockService,
    private _store: Store,
  ) {
    this._store
      .select(selectUser)
      .pipe(takeUntilDestroyed())
      .subscribe((user) => {
        if (user) {
          this.canSyncAllProducts = user.canActivate({
            subject: AuthSubjectsEnum.Products,
            action: AuthActionsProductsEnum.SyncAllProducts,
          });
        }
      });
  }

  public onSyncArticles(): void {
    if (!this.canSyncAllProducts) {
      return;
    }

    this.error = null;
    this.success = null;
    this.isLoading = true;

    this._stockService.syncAll().subscribe({
      next: () => {
        this.success = true;
      },
      error: (err) => {
        this.error = err.error.message;
        this.success = false;
      },
      complete: () => {
        this.isLoading = false;
      },
    });
  }
}
