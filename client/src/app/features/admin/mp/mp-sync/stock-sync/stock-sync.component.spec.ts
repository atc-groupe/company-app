import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StockSyncComponent } from './stock-sync.component';

describe('StockSyncComponent', () => {
  let component: StockSyncComponent;
  let fixture: ComponentFixture<StockSyncComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [StockSyncComponent]
    });
    fixture = TestBed.createComponent(StockSyncComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
