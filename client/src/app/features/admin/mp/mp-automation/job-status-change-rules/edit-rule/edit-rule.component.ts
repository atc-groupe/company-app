import { Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  AppSettingsAutomationService,
  JobsService,
  ModalService,
  NotificationsService,
} from '../../../../../../shared/services';
import {
  FormBuilder,
  FormControl,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { Store } from '@ngrx/store';
import { map, Observable } from 'rxjs';
import {
  IAppSettingsJobStatusChangeRule,
  IJobStatus,
  ISelectItem,
} from '../../../../../../shared/interfaces';
import { ColorPickerComponent } from '../../../../../../shared/components/color-picker/color-picker.component';
import { SelectComponent } from '../../../../../../shared/components/select/select.component';
import { ISettingsJobStatusChangeRuleDto } from '../../../../../../shared/dto';
import { SwitchComponent } from '../../../../../../shared/components/switch/switch.component';
import { displaySuccessNotificationAction } from '../../../../../../shared/store/ui';
import { JobStatusChangeRuleTypeEnum } from '../../../../../../shared/enums';
import { CancelButtonComponent } from '../../../../../../shared/components/cancel-button/cancel-button.component';
import { ModalCloseDirective } from '../../../../../../shared/directives/modal-close.directive';
import { SaveButtonComponent } from '../../../../../../shared/components/save-button/save-button.component';
import { MessageComponent } from '../../../../../../shared/components/message/message.component';

@Component({
  selector: 'app-add-rule',
  standalone: true,
  imports: [
    CommonModule,
    ColorPickerComponent,
    ReactiveFormsModule,
    SelectComponent,
    SwitchComponent,
    CancelButtonComponent,
    ModalCloseDirective,
    SaveButtonComponent,
    MessageComponent,
  ],
  templateUrl: './edit-rule.component.html',
  styleUrls: ['./edit-rule.component.scss'],
})
export class EditRuleComponent implements OnInit {
  public error: string | null = null;
  public statuses$: Observable<ISelectItem[]> = new Observable<ISelectItem[]>();
  public statuses: IJobStatus[] | null = null;
  public types: ISelectItem[] = [
    {
      label: "Démarrage de la première fiche d'enregistrement",
      value: JobStatusChangeRuleTypeEnum.onStartFirst,
    },
    {
      label: "Démarrage d'une fiche d'enregistrement",
      value: JobStatusChangeRuleTypeEnum.onStartOne,
    },
    {
      label: "Arrêt d'une fiche d'enregistrement",
      value: JobStatusChangeRuleTypeEnum.onStopOne,
    },
    {
      label: 'Fin de la dernière opération',
      value: JobStatusChangeRuleTypeEnum.onCompleteLast,
    },
    {
      label: "Suppression d'une fiche",
      value: JobStatusChangeRuleTypeEnum.onDeleteOne,
    },
    {
      label: "Suppression de la seule fiche d'enregistrement",
      value: JobStatusChangeRuleTypeEnum.onDeleteUnique,
    },
  ];
  public form = this._fb.group({
    type: new FormControl<JobStatusChangeRuleTypeEnum | null>(
      null,
      Validators.required,
    ),
    status: new FormControl<number | null>(null, Validators.required),
    canCancel: [false, Validators.required],
  });

  @Input() rule: IAppSettingsJobStatusChangeRule | null = null;

  constructor(
    private _automationService: AppSettingsAutomationService,
    private _modalService: ModalService,
    private _fb: FormBuilder,
    private _jobsService: JobsService,
    private _notificationService: NotificationsService,
  ) {}

  ngOnInit(): void {
    this.statuses$ = this._jobsService.getStatusesList().pipe(
      map((statuses) => {
        this.statuses = statuses;
        return statuses.map((status): ISelectItem => {
          return {
            label: status.label,
            value: status.number,
          };
        });
      }),
    );

    if (this.rule) {
      this.form.controls.type.setValue(this.rule.type);
      this.form.controls.status.setValue(this.rule.fromStatusNumber);
      this.form.controls.canCancel.setValue(this.rule.canCancel);
    }
  }

  public onSubmit(): void {
    if (!this.statuses) {
      return;
    }

    const type = this.form.controls.type.value;
    const statusNumber = this.form.controls.status.value;
    const canCancel = this.form.controls.canCancel.value;
    const status = this.statuses.find((item) => item.number === statusNumber);

    if (!type) {
      this.error = 'Veuillez sélectionner un type';

      return;
    }

    if (!status) {
      this.error = 'Veuillez sélectionner un statut';

      return;
    }

    if (canCancel === undefined || canCancel === null) {
      return;
    }

    const dto: ISettingsJobStatusChangeRuleDto = {
      type,
      fromStatusNumber: status.number,
      fromStatusText: status.label,
      canCancel,
    };

    const action$ = this.rule
      ? this._automationService.updateJobStatusChangeRule(this.rule._id, dto)
      : this._automationService.addJobStatusChangeRule(dto);

    action$.subscribe({
      next: () => {
        this._notificationService.displaySuccess(
          `Règle ${this.rule ? 'modifiée' : 'ajoutée'}!`,
        );
        this._modalService.close();
      },
      error: (err) => {
        this.error = err.error.message;
      },
    });
  }
}
