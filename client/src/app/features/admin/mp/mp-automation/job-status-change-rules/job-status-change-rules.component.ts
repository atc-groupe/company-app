import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Store } from '@ngrx/store';
import { IAppSettingsJobStatusChangeRule } from '../../../../../shared/interfaces';
import { selectAppSettings } from '../../../../../shared/store/app-settings/app-settings.selectors';
import { RuleComponent } from './rule/rule.component';
import {
  AlertService,
  AppSettingsAutomationService,
  ModalService,
  NotificationsService,
} from '../../../../../shared/services';
import { EditRuleComponent } from './edit-rule/edit-rule.component';
import { AddOperationComponent } from './add-operation/add-operation.component';
import { AddStatusComponent } from './add-status/add-status.component';
import { selectUser } from '../../../../../shared/store/user/user.selectors';
import {
  AuthActionsSettingsEnum,
  AuthSubjectsEnum,
} from '../../../../../shared/enums';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

@Component({
  selector: 'mp-automation-job-status-change-rules',
  standalone: true,
  imports: [CommonModule, RuleComponent],
  templateUrl: './job-status-change-rules.component.html',
  styleUrls: ['./job-status-change-rules.component.scss'],
})
export class JobStatusChangeRulesComponent {
  public expandedRuleIds: { [key: string]: boolean } = {};
  public rules: IAppSettingsJobStatusChangeRule[] | null = null;
  public canManageRules: boolean = false;

  constructor(
    private _store: Store,
    private _modalService: ModalService,
    private _alertService: AlertService,
    private _automationService: AppSettingsAutomationService,
    private _notificationsService: NotificationsService,
  ) {
    this._store
      .select(selectAppSettings)
      .pipe(takeUntilDestroyed())
      .subscribe((settings) => {
        if (settings) {
          this.rules = settings.automation.jobStatusChangeRules;
        }
      });

    this._store
      .select(selectUser)
      .pipe(takeUntilDestroyed())
      .subscribe((user) => {
        if (user) {
          this.canManageRules = user.canActivate({
            subject: AuthSubjectsEnum.Settings,
            action: AuthActionsSettingsEnum.ManageJobsAutomationRules,
          });
        }
      });
  }

  public onToggleRule(id: string): void {
    this.expandedRuleIds[id] = this.expandedRuleIds[id]
      ? !this.expandedRuleIds[id]
      : true;
  }

  public onAddRule(): void {
    if (!this.canManageRules) {
      return;
    }

    this._modalService.open(EditRuleComponent, {
      options: { yPos: 'center' },
    });
  }

  public onEditRule(rule: IAppSettingsJobStatusChangeRule): void {
    if (!this.canManageRules) {
      return;
    }

    this._modalService.open(EditRuleComponent, {
      options: { yPos: 'center' },
      inputs: { rule },
    });
  }

  public onRemoveRule(id: string): void {
    if (!this.canManageRules) {
      return;
    }

    this._alertService
      .open('Attention', `Etes vous sure de vouloir supprimer Cette règle ?`, {
        style: 'error',
        confirmLabel: 'Supprimer',
      })
      .afterClosed$.subscribe((result) => {
        if (result === 'confirm') {
          this._automationService.removeJobStatusChangeRule(id).subscribe({
            next: () =>
              this._notificationsService.displaySuccess(
                'La règle a été supprimée',
              ),
            error: () =>
              this._notificationsService.displayError(
                `Une erreur est survenue lors de la suppression`,
              ),
          });
        }
      });
  }

  public onAddRuleStatus({
    rule,
  }: {
    rule: IAppSettingsJobStatusChangeRule;
  }): void {
    if (!this.canManageRules) {
      return;
    }

    this._modalService.open(AddStatusComponent, {
      inputs: { rule },
      options: { yPos: 'center' },
    });
  }

  public onRemoveRuleStatus({
    ruleId,
    id,
  }: {
    ruleId: string;
    id: string;
  }): void {
    if (!this.canManageRules) {
      return;
    }

    this._automationService
      .removeJobStatusChangeRuleStatus(ruleId, id)
      .subscribe({
        next: () =>
          this._notificationsService.displaySuccess('Statut supprimé.'),
        error: (err) =>
          this._notificationsService.displayError(
            `Une erreur est survenue lors de la suppression. Veuillez réessayer. Erreur: ${err.error.message}`,
          ),
      });
  }

  public onAddRuleOperation(rule: IAppSettingsJobStatusChangeRule): void {
    if (!this.canManageRules) {
      return;
    }

    this._modalService.open(AddOperationComponent, {
      options: { yPos: 'center' },
      inputs: { rule },
    });
  }

  public onRemoveRuleOperation({
    ruleId,
    operationId,
  }: {
    ruleId: string;
    operationId: string;
  }) {
    if (!this.canManageRules) {
      return;
    }

    this._automationService
      .removeJobStatusChangeRuleOperation(ruleId, operationId)
      .subscribe({
        next: () =>
          this._notificationsService.displaySuccess('Opération supprimée'),
        error: () =>
          this._notificationsService.displayError(
            'Une erreur est survenue lors de la suppression.',
          ),
      });
  }
}
