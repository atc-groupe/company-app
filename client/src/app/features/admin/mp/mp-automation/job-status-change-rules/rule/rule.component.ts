import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IAppSettingsJobStatusChangeRule } from '../../../../../../shared/interfaces';
import { JobStatusChangeRuleTypeEnum } from '../../../../../../shared/enums';
import { JobStatusComponent } from '../../../../../../shared/components/job-status/job-status.component';

@Component({
  selector: 'admin-job-status-change-rule',
  standalone: true,
  imports: [CommonModule, JobStatusComponent],
  templateUrl: './rule.component.html',
  styleUrls: ['./rule.component.scss'],
})
export class RuleComponent {
  @Input() rule: IAppSettingsJobStatusChangeRule | null = null;
  @Input() expanded = false;

  @Output() remove = new EventEmitter<string>();
  @Output() edit = new EventEmitter<IAppSettingsJobStatusChangeRule>();
  @Output() addStatus = new EventEmitter<{
    rule: IAppSettingsJobStatusChangeRule;
  }>();
  @Output() removeStatus = new EventEmitter<{
    ruleId: string;
    id: string;
  }>();
  @Output() addOperation = new EventEmitter<IAppSettingsJobStatusChangeRule>();
  @Output() removeOperation = new EventEmitter<{
    ruleId: string;
    operationId: string;
  }>();
  @Output() toggle = new EventEmitter<string>();

  public onToggle(): void {
    this.expanded = !this.expanded;
    this.toggle.emit(this.rule?._id);
  }

  public onRemove(): void {
    this.expanded = !this.expanded;
    if (!this.rule) {
      return;
    }

    this.remove.emit(this.rule._id);
  }

  public onEdit(): void {
    if (!this.rule) {
      return;
    }

    this.edit.emit(this.rule);
  }

  public onAddStatus(): void {
    if (!this.rule) {
      return;
    }

    this.addStatus.emit({ rule: this.rule });
  }

  public onRemoveStatus(id: string): void {
    if (!this.rule) {
      return;
    }

    this.removeStatus.emit({ ruleId: this.rule._id, id });
  }

  public onAddOperation(): void {
    if (!this.rule) {
      return;
    }

    this.addOperation.emit(this.rule);
  }

  public onRemoveOperation(id: string): void {
    if (!this.rule) {
      return;
    }

    this.removeOperation.emit({ ruleId: this.rule._id, operationId: id });
  }

  public get typeLabel(): string | null {
    if (!this.rule) {
      return null;
    }

    switch (this.rule?.type) {
      case JobStatusChangeRuleTypeEnum.onStartFirst:
        return "Démarrage de la première fiche d'enregistrement";
      case JobStatusChangeRuleTypeEnum.onStartOne:
        return "Démarrage d'une fiche d'enregistrement";
      case JobStatusChangeRuleTypeEnum.onStopOne:
        return "Arrêt d'une fiche d'enregistrement";
      case JobStatusChangeRuleTypeEnum.onCompleteLast:
        return 'Fin de la dernière opération';
      case JobStatusChangeRuleTypeEnum.onDeleteOne:
        return "Suppression d'une fiche d'enregistrement";
      case JobStatusChangeRuleTypeEnum.onDeleteUnique:
        return "Suppression de la seule fiche d'enregistrement";
    }
  }
}
