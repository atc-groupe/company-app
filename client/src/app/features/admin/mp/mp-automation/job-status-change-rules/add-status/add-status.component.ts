import { Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  IAppSettingsJobStatusChangeRule,
  IJobStatus,
  ISelectItem,
} from '../../../../../../shared/interfaces';
import { map, Observable } from 'rxjs';
import {
  AppSettingsAutomationService,
  JobsService,
  ModalService,
  NotificationsService,
} from '../../../../../../shared/services';
import {
  FormBuilder,
  FormControl,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { ISettingsJobStatusChangeRuleStatusDto } from '../../../../../../shared/dto';
import { SelectComponent } from '../../../../../../shared/components/select/select.component';
import { CancelButtonComponent } from '../../../../../../shared/components/cancel-button/cancel-button.component';
import { ModalCloseDirective } from '../../../../../../shared/directives/modal-close.directive';
import { SaveButtonComponent } from '../../../../../../shared/components/save-button/save-button.component';
import { MessageComponent } from '../../../../../../shared/components/message/message.component';

@Component({
  selector: 'app-add-status',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SelectComponent,
    CancelButtonComponent,
    ModalCloseDirective,
    SaveButtonComponent,
    MessageComponent,
  ],
  templateUrl: './add-status.component.html',
  styleUrls: ['./add-status.component.scss'],
})
export class AddStatusComponent implements OnInit {
  @Input() rule!: IAppSettingsJobStatusChangeRule;

  public error: string | null = null;
  public statuses$: Observable<ISelectItem[]> = new Observable<ISelectItem[]>();
  public form = this._fb.group({
    status: new FormControl<IJobStatus | null>(null, Validators.required),
  });

  constructor(
    private _automationService: AppSettingsAutomationService,
    private _modalService: ModalService,
    private _fb: FormBuilder,
    private _jobsService: JobsService,
    private _notificationsService: NotificationsService,
  ) {}

  ngOnInit() {
    this.statuses$ = this._jobsService.getStatusesList().pipe(
      map((statuses) => {
        return statuses
          .filter(
            (status) =>
              !this.rule.toStatuses.some(
                (item) => item.statusNumber === status.number,
              ),
          )
          .map((status): ISelectItem => {
            return {
              label: status.label,
              value: status,
            };
          });
      }),
    );
  }

  public onSubmit(): void {
    const status = this.form.controls.status.value;

    if (!status) {
      this.error = 'Veuillez sélectionner un statut';
      return;
    }

    const dto: ISettingsJobStatusChangeRuleStatusDto = {
      statusNumber: status.number,
      statusText: status.label,
    };

    this._automationService
      .addJobStatusChangeRuleStatus(this.rule._id, dto)
      .subscribe({
        next: () => {
          this._notificationsService.displaySuccess('Statut ajouté!');
          this._modalService.close();
        },
        error: (err) => {
          this.error = err.error.message;
        },
      });
  }
}
