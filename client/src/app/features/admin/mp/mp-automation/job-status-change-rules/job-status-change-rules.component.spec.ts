import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobStatusChangeRulesComponent } from './job-status-change-rules.component';

describe('JobStatusChangeRulesComponent', () => {
  let component: JobStatusChangeRulesComponent;
  let fixture: ComponentFixture<JobStatusChangeRulesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [JobStatusChangeRulesComponent]
    });
    fixture = TestBed.createComponent(JobStatusChangeRulesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
