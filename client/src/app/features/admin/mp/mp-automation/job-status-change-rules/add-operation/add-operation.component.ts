import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  IAppSettingsJobStatusChangeRule,
  IEmployeeOperation,
  ISelectItem,
} from '../../../../../../shared/interfaces';
import {
  AppSettingsAutomationService,
  EmployeesService,
  ModalService,
  NotificationsService,
} from '../../../../../../shared/services';
import {
  FormBuilder,
  FormControl,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { map, Observable } from 'rxjs';
import { SelectComponent } from '../../../../../../shared/components/select/select.component';
import { MessageComponent } from '../../../../../../shared/components/message/message.component';
import { CancelButtonComponent } from '../../../../../../shared/components/cancel-button/cancel-button.component';
import { SaveButtonComponent } from '../../../../../../shared/components/save-button/save-button.component';
import { ModalCloseDirective } from '../../../../../../shared/directives/modal-close.directive';

@Component({
  selector: 'app-add-operation',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SelectComponent,
    MessageComponent,
    CancelButtonComponent,
    SaveButtonComponent,
    ModalCloseDirective,
  ],
  templateUrl: './add-operation.component.html',
  styleUrls: ['./add-operation.component.scss'],
})
export class AddOperationComponent {
  @Input() rule!: IAppSettingsJobStatusChangeRule;
  public error: string | null = null;
  public operationsItems$: Observable<ISelectItem[]>;
  public form = this._fb.group({
    operation: new FormControl<IEmployeeOperation | null>(
      null,
      Validators.required,
    ),
  });

  constructor(
    private _automationService: AppSettingsAutomationService,
    private _modalService: ModalService,
    private _fb: FormBuilder,
    private _employeesService: EmployeesService,
    private _notificationsService: NotificationsService,
  ) {
    this.operationsItems$ = this._employeesService.getOperations().pipe(
      map((operations) =>
        operations
          .filter(
            (operation) =>
              this.rule.operations.find(
                (item) => item.operationNumber === operation.operationNumber,
              ) === undefined,
          )
          .map((item) => {
            return {
              value: item,
              label: item.operation,
            };
          }),
      ),
    );
  }

  public onSubmit(): void {
    this.error = null;
    const operation = this.form.controls.operation.value;

    if (!operation) {
      this.error = 'Veuillez sélectionner une opération';
      return;
    }

    this._automationService
      .addJobStatusChangeRuleOperation(this.rule._id, {
        operation: operation.operation,
        operationNumber: operation.operationNumber,
      })
      .subscribe({
        next: () => {
          this._notificationsService.displaySuccess('Opération ajoutée!');
          this._modalService.close();
        },
        error: (err) => {
          this.error = err.error.message;
        },
      });
  }
}
