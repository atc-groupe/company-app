import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MpAutomationComponent } from './mp-automation.component';

describe('MpAutomationComponent', () => {
  let component: MpAutomationComponent;
  let fixture: ComponentFixture<MpAutomationComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MpAutomationComponent]
    });
    fixture = TestBed.createComponent(MpAutomationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
