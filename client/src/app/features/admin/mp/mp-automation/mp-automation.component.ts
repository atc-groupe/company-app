import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JobStatusChangeRulesComponent } from './job-status-change-rules/job-status-change-rules.component';
import { Store } from '@ngrx/store';
import { setNavbarItemsAction } from '../../../../shared/store/navbar';

@Component({
  selector: 'app-mp-automation',
  standalone: true,
  imports: [CommonModule, JobStatusChangeRulesComponent],
  templateUrl: './mp-automation.component.html',
  styleUrls: ['./mp-automation.component.scss'],
})
export class MpAutomationComponent {
  constructor(private _store: Store) {
    this._store.dispatch(
      setNavbarItemsAction({
        items: [{ label: 'Automatisation MultiPress' }],
      }),
    );
  }
}
