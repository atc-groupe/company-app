import { Routes } from '@angular/router';
import { AdminComponent } from './admin.component';
import { UsersComponent } from './security/users/users.component';
import { RolesComponent } from './security/roles/roles.component';
import { ListComponent as RoleListComponent } from './security/roles/list/list.component';
import { ViewComponent as RoleViewComponent } from './security/roles/view/view.component';
import { ListComponent as UsersListComponent } from './security/users/list/list.component';
import { ViewComponent as UsersViewComponent } from './security/users/view/view.component';
import { MpMappingComponent } from './mp/mp-mapping/mp-mapping.component';
import { ListComponent as MpMappingListComponent } from './mp/mp-mapping/list/list.component';
import { MpSyncComponent } from './mp/mp-sync/mp-sync.component';
import { MpAutomationComponent } from './mp/mp-automation/mp-automation.component';
import { ProductsSettingsComponent } from './stock/products-settings/products-settings.component';
import { PlanningViewsComponent } from './planning/planning-views/planning-views.component';
import {
  canActivateJobsAutomationRules,
  canActivateMappingParameters,
  canActivatePlanningViews,
  canActivateProductsParameters,
  canActivateRoles,
  canActivateUsers,
} from './admin.guard';

export default [
  {
    path: '',
    component: AdminComponent,
    children: [
      { path: '', redirectTo: 'roles', pathMatch: 'full' },
      {
        path: 'users',
        component: UsersComponent,
        title: 'Utilisateurs',
        canActivate: [canActivateUsers],
        children: [
          { path: '', redirectTo: 'list', pathMatch: 'full' },
          { path: 'list', component: UsersListComponent },
          { path: ':id/view', component: UsersViewComponent },
        ],
      },
      {
        path: 'roles',
        title: 'Roles utilisateurs',
        component: RolesComponent,
        canActivate: [canActivateRoles],
        children: [
          { path: '', redirectTo: 'list', pathMatch: 'full' },
          { path: 'list', component: RoleListComponent },
          { path: ':id/view', component: RoleViewComponent },
        ],
      },
      {
        path: 'multipress/mapping',
        title: 'Mapping des données MultiPress',
        component: MpMappingComponent,
        canActivate: [canActivateMappingParameters],
        children: [
          { path: '', redirectTo: 'list', pathMatch: 'full' },
          { path: 'list', component: MpMappingListComponent },
        ],
      },
      {
        path: 'multipress/sync',
        title: 'Synchronisation MultiPress',
        component: MpSyncComponent,
      },
      {
        path: 'multipress/automation',
        title: 'Automatisation',
        component: MpAutomationComponent,
        canActivate: [canActivateJobsAutomationRules],
      },
      {
        path: 'stock/products-settings',
        title: 'Paramètres produits',
        component: ProductsSettingsComponent,
        canActivate: [canActivateProductsParameters],
      },
      {
        path: 'planning/views',
        title: 'Vues planning',
        component: PlanningViewsComponent,
        canActivate: [canActivatePlanningViews],
      },
    ],
  },
] satisfies Routes;
