import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanningViewsComponent } from './planning-views.component';

describe('PlanningViewsComponent', () => {
  let component: PlanningViewsComponent;
  let fixture: ComponentFixture<PlanningViewsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [PlanningViewsComponent]
    });
    fixture = TestBed.createComponent(PlanningViewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
