import { Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IAppSettingsPlanningView } from '../../../../../../shared/interfaces/app-settings/i-app-settings-planning-view';
import { first, map, Observable } from 'rxjs';
import { ISelectItem } from '../../../../../../shared/interfaces';
import {
  FormBuilder,
  FormControl,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import {
  AppSettingsPlanningViewsService,
  ModalService,
  NotificationsService,
} from '../../../../../../shared/services';
import { Store } from '@ngrx/store';
import * as PlanningSelectors from '../../../../../../shared/store/planning/planning.selectors';
import { SelectComponent } from '../../../../../../shared/components/select/select.component';
import { MessageComponent } from '../../../../../../shared/components/message/message.component';
import { CancelButtonComponent } from '../../../../../../shared/components/cancel-button/cancel-button.component';
import { ModalCloseDirective } from '../../../../../../shared/directives/modal-close.directive';
import { SaveButtonComponent } from '../../../../../../shared/components/save-button/save-button.component';

@Component({
  selector: 'app-machine-add',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SelectComponent,
    MessageComponent,
    CancelButtonComponent,
    ModalCloseDirective,
    SaveButtonComponent,
  ],
  templateUrl: './machine-add.component.html',
  styleUrls: ['./machine-add.component.scss'],
})
export class MachineAddComponent implements OnInit {
  @Input() view!: IAppSettingsPlanningView;

  public error: string | null = null;
  public machines$: Observable<ISelectItem[]> = new Observable<ISelectItem[]>();
  public form = this._fb.group({
    machineId: new FormControl<number | null>(null, [Validators.required]),
  });

  constructor(
    private _modalService: ModalService,
    private _planningViewsService: AppSettingsPlanningViewsService,
    private _store: Store,
    private _fb: FormBuilder,
    private _notificationService: NotificationsService,
  ) {}

  ngOnInit() {
    this.machines$ = this._store.select(PlanningSelectors.selectSetup).pipe(
      first((v) => v !== null),
      map((setup) => {
        return setup!
          .getGroupMachinesSelectItems(this.view.planningGroup)
          .filter(
            (item) =>
              !this.view.machineIds.includes(item.value) && item.value !== null,
          );
      }),
    );
  }

  public onSubmit(): void {
    const machineId = this.form.controls.machineId.value;

    if (!machineId) {
      this.error = 'Aucune machine sélectionnée';
      return;
    }

    this._planningViewsService
      .addMachineId(this.view._id, machineId)
      .subscribe({
        next: () => {
          this._notificationService.displaySuccess('Machine ajoutée!');
          this._modalService.close();
        },
        error: (err) => {
          this.error = err.error.message;
        },
      });
  }
}
