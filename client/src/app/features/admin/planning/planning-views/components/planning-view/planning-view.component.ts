import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IAppSettingsPlanningView } from '../../../../../../shared/interfaces/app-settings/i-app-settings-planning-view';
import { JobStatusComponent } from '../../../../../../shared/components/job-status/job-status.component';
import { PlanningSetup } from '../../../../../../shared/entities';
import { MpListItemPipe } from '../../../../../../shared/pipes/mp-list-item.pipe';

@Component({
  selector: 'settings-planning-view',
  standalone: true,
  imports: [CommonModule, JobStatusComponent, MpListItemPipe],
  templateUrl: './planning-view.component.html',
  styleUrls: ['./planning-view.component.scss'],
})
export class PlanningViewComponent {
  @Input({ required: true }) view!: IAppSettingsPlanningView;
  @Input({ required: true }) setup: PlanningSetup | null = null;
  @Input({ required: true }) expanded!: boolean;
  @Input({ required: true }) canManagePlanningViews!: boolean;

  @Output() toggle = new EventEmitter<void>();
  @Output() remove = new EventEmitter<void>();
  @Output() edit = new EventEmitter<void>();
  @Output() addMachineId = new EventEmitter<void>();
  @Output() removeMachineId = new EventEmitter<number>();
  @Output() addStatus = new EventEmitter<void>();
  @Output() removeStatus = new EventEmitter<number>();

  public onToggle(): void {
    this.expanded = !this.expanded;
    this.toggle.emit();
  }

  public onRemove(): void {
    this.expanded = false;
    this.remove.emit();
  }

  public onEdit(): void {
    this.edit.emit();
  }

  public onAddMachineId(): void {
    this.addMachineId.emit();
  }

  public onRemoveMachineId(id: number): void {
    this.removeMachineId.emit(id);
  }

  public getMachineName(id: number): string | null {
    if (!this.setup) {
      return null;
    }

    const machine = this.setup.getMachine(id);

    return machine ? machine.name : null;
  }
}
