import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanningViewEditComponent } from './planning-view-edit.component';

describe('PlanningViewEditComponent', () => {
  let component: PlanningViewEditComponent;
  let fixture: ComponentFixture<PlanningViewEditComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [PlanningViewEditComponent]
    });
    fixture = TestBed.createComponent(PlanningViewEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
