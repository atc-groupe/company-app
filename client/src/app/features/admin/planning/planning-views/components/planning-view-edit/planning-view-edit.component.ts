import { Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IAppSettingsPlanningView } from '../../../../../../shared/interfaces/app-settings/i-app-settings-planning-view';
import {
  AppSettingsPlanningViewsService,
  ModalService,
  NotificationsService,
} from '../../../../../../shared/services';
import { SelectComponent } from '../../../../../../shared/components/select/select.component';
import { Store } from '@ngrx/store';
import { ISelectItem } from '../../../../../../shared/interfaces';
import { BehaviorSubject, first, map } from 'rxjs';
import * as PlanningSelectors from '../../../../../../shared/store/planning/planning.selectors';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { ISettingsPlanningViewDto } from '../../../../../../shared/dto';
import { PlanningSetup } from '../../../../../../shared/entities';
import { CancelButtonComponent } from '../../../../../../shared/components/cancel-button/cancel-button.component';
import { ModalCloseDirective } from '../../../../../../shared/directives/modal-close.directive';
import { SaveButtonComponent } from '../../../../../../shared/components/save-button/save-button.component';
import { MessageComponent } from '../../../../../../shared/components/message/message.component';

@Component({
  selector: 'app-planning-view-edit',
  standalone: true,
  imports: [
    CommonModule,
    SelectComponent,
    ReactiveFormsModule,
    CancelButtonComponent,
    ModalCloseDirective,
    SaveButtonComponent,
    MessageComponent,
  ],
  templateUrl: './planning-view-edit.component.html',
  styleUrls: ['./planning-view-edit.component.scss'],
})
export class PlanningViewEditComponent implements OnInit {
  @Input() setup!: PlanningSetup;
  @Input() view?: IAppSettingsPlanningView;

  public error: string | null = null;
  public groups$ = new BehaviorSubject<ISelectItem[]>([]);
  public form = this._fb.group({
    name: ['', Validators.required],
    group: ['', Validators.required],
  });

  constructor(
    private _modalService: ModalService,
    private _viewsService: AppSettingsPlanningViewsService,
    private _store: Store,
    private _fb: FormBuilder,
    private _notificationService: NotificationsService,
  ) {}

  ngOnInit() {
    this._store
      .select(PlanningSelectors.selectSetup)
      .pipe(
        first((v) => v !== null),
        map((setup) => {
          this.groups$.next(setup!.getGroupListSelectItems());
        }),
      )
      .subscribe();

    if (this.view) {
      this.form.controls.name.setValue(this.view.name);
      this.form.controls.group.setValue(this.view.planningGroup);
    }
  }

  get title(): string {
    return this.view ? `Modifier la vue` : 'Nouvelle vue';
  }

  get subtitle(): string | null {
    return this.view ? this.view.name : null;
  }

  public onSubmit(): void {
    const name = this.form.controls.name.value;
    const group = this.form.controls.group.value;

    if (!name || !group) {
      this.error = 'Merci de renseigner toutes les valeurs obligatoires';
      return;
    }

    const dto: ISettingsPlanningViewDto = {
      name,
      planningDepartment: this.setup.getDepartment(group)!,
      planningGroup: group,
    };

    const action$ = this.view
      ? this._viewsService.updateOne(this.view._id, dto)
      : this._viewsService.createOne(dto);

    action$.subscribe({
      next: () => {
        this._notificationService.displaySuccess('Vue créée!');
        this._modalService.close();
      },
      error: (err) => {
        this.error = err.error.message;
      },
    });
  }
}
