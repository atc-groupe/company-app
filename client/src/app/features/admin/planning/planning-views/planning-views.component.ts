import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  AlertService,
  AppSettingsPlanningViewsService,
  ModalService,
  NotificationsService,
} from '../../../../shared/services';
import { PlanningViewEditComponent } from './components/planning-view-edit/planning-view-edit.component';
import { IAppSettingsPlanningView } from '../../../../shared/interfaces/app-settings/i-app-settings-planning-view';
import { Store } from '@ngrx/store';
import { selectAppSettings } from '../../../../shared/store/app-settings/app-settings.selectors';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { PlanningViewComponent } from './components/planning-view/planning-view.component';
import { PlanningSetup } from '../../../../shared/entities';
import * as PlanningSelectors from '../../../../shared/store/planning/planning.selectors';
import { first } from 'rxjs';
import { IJobMappedStatus } from '../../../../shared/interfaces';
import { MachineAddComponent } from './components/machine-add/machine-add.component';
import { setNavbarItemsAction } from '../../../../shared/store/navbar';
import { selectUser } from '../../../../shared/store/user/user.selectors';
import {
  AuthActionsSettingsEnum,
  AuthSubjectsEnum,
} from '../../../../shared/enums';

@Component({
  selector: 'app-planning-views',
  standalone: true,
  imports: [CommonModule, PlanningViewComponent],
  templateUrl: './planning-views.component.html',
  styleUrls: ['./planning-views.component.scss'],
})
export class PlanningViewsComponent {
  public views: IAppSettingsPlanningView[] | null = null;
  public expandedViewsIds: { [key: string]: boolean } = {};
  public setup: PlanningSetup | null = null;
  public mappedStatuses: IJobMappedStatus[] | null = null;
  public canManagePlanningViews: boolean = false;

  constructor(
    private _modalService: ModalService,
    private _planningViewsService: AppSettingsPlanningViewsService,
    private _alertService: AlertService,
    private _store: Store,
    private _notificationService: NotificationsService,
  ) {
    this._store.dispatch(
      setNavbarItemsAction({
        items: [{ label: 'Vues de planning' }],
      }),
    );

    this._store
      .select(selectAppSettings)
      .pipe(takeUntilDestroyed())
      .subscribe((settings) => {
        if (settings != null) {
          this.views = settings.planning.views;
          this.mappedStatuses = settings.job.statuses.items;
        }
      });

    this._store
      .select(PlanningSelectors.selectSetup)
      .pipe(first((v) => v !== null))
      .subscribe((setup: PlanningSetup | null) => (this.setup = setup));

    this._store
      .select(selectUser)
      .pipe(takeUntilDestroyed())
      .subscribe((user) => {
        if (user) {
          this.canManagePlanningViews = user.canActivate({
            subject: AuthSubjectsEnum.Settings,
            action: AuthActionsSettingsEnum.ManagePlanningViews,
          });
        }
      });
  }

  public onToggleView(id: string): void {
    this.expandedViewsIds[id] === undefined
      ? (this.expandedViewsIds[id] = true)
      : (this.expandedViewsIds[id] = !this.expandedViewsIds[id]);
  }

  public onCreateView(): void {
    if (!this.canManagePlanningViews) {
      return;
    }

    this._modalService.open(PlanningViewEditComponent, {
      options: { yPos: 'center' },
      inputs: { setup: this.setup },
    });
  }

  public onUpdateView(view: IAppSettingsPlanningView): void {
    if (!this.canManagePlanningViews) {
      return;
    }

    this._modalService.open(PlanningViewEditComponent, {
      options: { yPos: 'center' },
      inputs: { view, setup: this.setup },
    });
  }

  public onRemoveView(view: IAppSettingsPlanningView): void {
    if (!this.canManagePlanningViews) {
      return;
    }

    this._alertService
      .open(
        'Supprimmer la vue',
        `Etes vous sure de vouloir supprimer la vue ${view.name}`,
        { style: 'warning', confirmLabel: 'Supprimer' },
      )
      .afterClosed$.subscribe((result) => {
        if (result === 'confirm') {
          this._planningViewsService.removeOne(view._id).subscribe({
            next: () =>
              this._notificationService.displaySuccess(
                'La vue a été supprimée.',
              ),
            error: () =>
              this._notificationService.displayError(
                'Impossible de supprimer la vue',
              ),
          });
        }
      });
  }

  public onAddMachineId(view: IAppSettingsPlanningView): void {
    if (!this.canManagePlanningViews) {
      return;
    }

    this._modalService.open(MachineAddComponent, {
      options: { yPos: 'center' },
      inputs: { view },
    });
  }

  public onRemoveMachineId(view: IAppSettingsPlanningView, id: number): void {
    if (!this.canManagePlanningViews) {
      return;
    }

    this._planningViewsService.removeMachineId(view._id, id).subscribe({
      next: () =>
        this._notificationService.displaySuccess('La machine a été supprimée.'),
      error: () =>
        this._notificationService.displayError(
          'Impossible de supprimer la machine',
        ),
    });
  }
}
