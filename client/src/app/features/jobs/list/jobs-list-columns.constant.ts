import { IDataColumn } from '../../../shared/interfaces';

export const jobsListColumns: IDataColumn[] = [
  { label: 'Job', flex: 3 },
  { label: 'Client', flex: 10 },
  { label: 'Statut', flex: 4 },
  { label: 'FAB', flex: 6 },
  { label: 'Livraison', flex: 3 },
  { icon: 'sync', flex: 1, textAlign: 'center', notSortable: true },
];
