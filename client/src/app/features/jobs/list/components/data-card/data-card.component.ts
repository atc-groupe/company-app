import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataLineComponent } from '../data-line/data-line.component';
import { JobDevicesComponent } from '../../../../../shared/components/job-devices/job-devices.component';
import { JobStatusComponent } from '../../../../../shared/components/job-status/job-status.component';

@Component({
  selector: 'jobs-card',
  standalone: true,
  imports: [CommonModule, JobDevicesComponent, JobStatusComponent],
  templateUrl: './data-card.component.html',
  styleUrls: ['./data-card.component.scss'],
})
export class DataCardComponent extends DataLineComponent {}
