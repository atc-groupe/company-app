import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JobDevicesComponent } from '../../../../../shared/components/job-devices/job-devices.component';
import { JobStatusComponent } from '../../../../../shared/components/job-status/job-status.component';
import { IJobLine } from '../../../../../shared/interfaces/job/i-job-line';
import { IDataColumn } from '../../../../../shared/interfaces';

@Component({
  selector: 'jobs-line',
  standalone: true,
  imports: [CommonModule, JobDevicesComponent, JobStatusComponent],
  templateUrl: './data-line.component.html',
  styleUrls: ['./data-line.component.scss'],
})
export class DataLineComponent {
  @Input({ required: true }) line!: IJobLine;
  @Input({ required: true }) columns!: IDataColumn[];

  @Output() viewJob = new EventEmitter();

  public get jobNumberColorClass(): string {
    return this.line.sync.status === 'error' ? 'color-error' : 'color-primary';
  }

  public onViewJob(): void {
    this.viewJob.emit();
  }
}
