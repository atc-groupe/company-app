import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchComponent } from '../../../../../shared/components/search/search.component';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { debounceTime, Subscription, tap } from 'rxjs';

@Component({
  selector: 'jobs-list-header',
  standalone: true,
  imports: [CommonModule, SearchComponent, ReactiveFormsModule],
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  @Input() searchValue: string | null = null;

  @Output() private search = new EventEmitter<{ search: string }>();
  @Output() private clearSearch = new EventEmitter<void>();
  @Output() private startSearch = new EventEmitter<void>();

  public form = this._fb.group({
    search: [''],
  });

  private _subscription = new Subscription();

  constructor(private _fb: FormBuilder) {}

  ngOnInit(): void {
    const search = this.form.get('search');

    if (!search) {
      return;
    }

    search.setValue(this.searchValue);
    this._subscription.add(
      search.valueChanges
        .pipe(
          tap((value) => {
            value ? this.startSearch.emit() : this.clearSearch.emit();
          }),
          debounceTime(300),
        )
        .subscribe((value) => {
          if (value) {
            this.search.emit({ search: value });
          }
        }),
    );
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
