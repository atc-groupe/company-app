import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpinnerComponent } from '../../../../../shared/components/spinner/spinner.component';
import { EmptyMessageComponent } from '../../../../../shared/components/empty-message/empty-message.component';

@Component({
  selector: 'jobs-list-message',
  standalone: true,
  imports: [CommonModule, SpinnerComponent, EmptyMessageComponent],
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss'],
})
export class MessageComponent {
  @Input() error: string | null = null;
  @Input() isLoading: boolean = true;
  @Input() search: string | null = null;
  @Input() jobsCount: number = 0;

  get displayErrorMessage(): boolean {
    return !this.isLoading && !!this.error;
  }

  get displayLoader(): boolean {
    return this.isLoading;
  }

  get displayEmptyPlanningMessage(): boolean {
    return !this.isLoading && !this.error && !this.search && !this.jobsCount;
  }

  get displayEmptySearchMessage(): boolean {
    return !this.isLoading && !this.error && !!this.search && !this.jobsCount;
  }
}
