import { Component, OnDestroy } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WsJobsService } from '../../../shared/services';
import { Store } from '@ngrx/store';
import * as JobActions from '../../../shared/store/job/job.actions';
import { HeaderComponent } from './components/header/header.component';
import { MessageComponent } from './components/message/message.component';
import { IDataColumn, ISortColumn } from '../../../shared/interfaces';
import { Router } from '@angular/router';

import * as JobsActions from '../../../shared/store/jobs/jobs.actions';
import * as JobsSelectors from '../../../shared/store/jobs/jobs.selectors';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { IJobLine } from '../../../shared/interfaces/job/i-job-line';
import { DataHeadingComponent } from '../../../shared/components/data-heading/data-heading.component';
import { jobsListColumns } from './jobs-list-columns.constant';
import { ShowDirective } from '../../../shared/directives/show.directive';
import { DataLineComponent } from './components/data-line/data-line.component';
import { HideDirective } from '../../../shared/directives/hide.directive';
import { DataCardComponent } from './components/data-card/data-card.component';

@Component({
  selector: 'app-list',
  standalone: true,
  imports: [
    CommonModule,
    HeaderComponent,
    MessageComponent,
    DataHeadingComponent,
    ShowDirective,
    DataLineComponent,
    HideDirective,
    DataCardComponent,
  ],
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnDestroy {
  public search: string | null = null;
  public isLoading = false;
  public error: string | null = null;
  public data: IJobLine[] | null = null;
  public columns: IDataColumn[] = jobsListColumns;
  public sortColumn$ = this._store.select(JobsSelectors.selectSortColumn);

  constructor(
    private _wsJobs: WsJobsService,
    private _store: Store,
    private _router: Router,
  ) {
    this._wsJobs.connect();

    this._wsJobs
      .onConnectError()
      .pipe(takeUntilDestroyed())
      .subscribe((err) => (this.error = err.message));

    this._wsJobs
      .onError()
      .pipe(takeUntilDestroyed())
      .subscribe((err) => (this.error = err.message));

    this._store
      .select(JobsSelectors.selectData)
      .pipe(takeUntilDestroyed())
      .subscribe((data) => (this.data = data));

    this._store
      .select(JobsSelectors.selectError)
      .pipe(takeUntilDestroyed())
      .subscribe((error) => (this.error = error));

    this._store
      .select(JobsSelectors.selectIsLoading)
      .pipe(takeUntilDestroyed())
      .subscribe((isLoading) => (this.isLoading = isLoading));

    this._store
      .select(JobsSelectors.selectSearch)
      .pipe(takeUntilDestroyed())
      .subscribe((search) => (this.search = search));
  }

  public get dataCount(): number {
    return this.data ? this.data.length : 0;
  }

  public onStartSearch(): void {
    this._store.dispatch(JobsActions.startSearchAction());
  }

  public onSearch({ search }: { search: string }): void {
    this._store.dispatch(JobsActions.searchJobAction({ search }));
  }

  public onClearSearch(): void {
    this._store.dispatch(JobsActions.clearSearchAction());
  }

  public onSortData(sortColumn: ISortColumn | null): void {
    this._store.dispatch(JobsActions.setSortColumnAction({ sortColumn }));
  }

  public get displayMessage(): boolean {
    const emptyList = this.data === null || !this.data.length;

    return !!this.error || this.isLoading || emptyList;
  }

  public get displayList(): boolean {
    return !this.displayMessage;
  }

  public async onViewJob(mpNumber: number): Promise<void> {
    this._store.dispatch(JobActions.setFromAction({ route: '/jobs' }));
    await this._router.navigateByUrl(`/jobs/${mpNumber}/view`);
  }

  ngOnDestroy() {
    this._wsJobs.disconnect();
    this._store.dispatch(JobsActions.clearDataAction());
  }
}
