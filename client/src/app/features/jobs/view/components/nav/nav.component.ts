import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterLink, RouterLinkActive } from '@angular/router';
import { ClickOutsideDirective } from '../../../../../shared/directives/click-outside.directive';
import { JobFilesCheckComponent } from '../../../../../shared/components/job-files-check/job-files-check.component';
import { IJob } from '../../../../../shared/interfaces';

@Component({
  selector: 'job-view-nav',
  standalone: true,
  imports: [
    CommonModule,
    RouterLink,
    RouterLinkActive,
    ClickOutsideDirective,
    JobFilesCheckComponent,
  ],
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
})
export class NavComponent {
  @Input() public expanded: boolean | null = false;
  @Input() public job: IJob | null = null;
  @Input() public displayWorksheets = false;

  @Output() private hide = new EventEmitter<void>();

  public onHide() {
    this.hide.emit();
  }

  public get remarksCount(): number {
    let count = 0;

    if (!this.job) {
      return count;
    }

    Object.values(this.job.mp.info).forEach((value) => {
      if (value) {
        count++;
      }
    });

    return count;
  }

  public get isPlanned(): boolean {
    return this.job ? this.job.planningData.every((el) => el.isPlanned) : true;
  }
}
