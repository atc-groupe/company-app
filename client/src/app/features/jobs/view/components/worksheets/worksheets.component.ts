import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MpListItemPipe } from '../../../../../shared/pipes/mp-list-item.pipe';
import { IJobOperation, IJobWorksheet } from '../../../../../shared/interfaces';
import { User } from '../../../../../shared/entities';
import { UserOperationsService } from '../../../../../shared/services';
import { MpEmployeePipe } from '../../../../../shared/pipes/mp-employee.pipe';
import { SpinnerComponent } from '../../../../../shared/components/spinner/spinner.component';

@Component({
  selector: 'job-worksheets',
  standalone: true,
  imports: [CommonModule, MpListItemPipe, MpEmployeePipe, SpinnerComponent],
  templateUrl: './worksheets.component.html',
  styleUrls: ['./worksheets.component.scss'],
})
export class WorksheetsComponent {
  @Input() operations: IJobOperation[] | null = null;
  @Input() worksheets: IJobWorksheet[] | null = null;
  @Input() user: User | null = null;
  @Input() startWorksheetProcessing: boolean = true;

  @Output() addWorksheet = new EventEmitter<string>();
  @Output() stopWorksheet = new EventEmitter<{ worksheet: IJobWorksheet }>();

  private index = 0;

  constructor(private _userOperationsService: UserOperationsService) {}

  public get hasMultipleOperations(): boolean {
    return this.operations ? this.operations.length > 1 : false;
  }

  public get selectedOperation(): IJobOperation | null {
    return this.operations ? this.operations[this.index] : null;
  }

  public get isLastOperation(): boolean {
    return this.operations ? this.operations.length === this.index + 1 : true;
  }

  public get isFirstOperation(): boolean {
    return this.index === 0;
  }

  public get activeWorksheet(): IJobWorksheet | null {
    if (!this.selectedOperation || !this.worksheets) {
      return null;
    }

    return this._userOperationsService.getActiveWorksheet(
      this.selectedOperation.operation,
      this.worksheets,
    );
  }

  public get isMyActiveWorksheet(): boolean {
    const worksheet = this.activeWorksheet;

    if (!worksheet || !this.user) {
      return false;
    }

    return worksheet.employeeNumber === this.user.mp.employeeNumber;
  }

  public get canAddWorksheet(): boolean {
    if (!this.selectedOperation || !this.user || !this.worksheets) {
      return false;
    }

    return this._userOperationsService.canAddWorksheet(
      this.selectedOperation.operation,
      this.worksheets,
      this.user,
    );
  }

  public setNextIndex(): void {
    if (this.isLastOperation) {
      return;
    }

    this.index++;
  }

  public setPrevIndex(): void {
    if (this.isFirstOperation) {
      return;
    }

    this.index--;
  }

  public onAddWorksheet(): void {
    if (!this.selectedOperation) {
      return;
    }

    this.addWorksheet.emit(this.selectedOperation.operation);
  }

  public onStopWorksheet(): void {
    if (!this.selectedOperation) {
      return;
    }

    const worksheet = this.activeWorksheet;

    if (!worksheet) {
      return;
    }

    this.stopWorksheet.emit({ worksheet });
  }
}
