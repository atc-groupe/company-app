import { Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { User } from '../../../../../shared/entities';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import {
  EmployeesService,
  JobsService,
  JobStatusAutomationRulesService,
  JobWorksheetsService,
  ModalService,
  NotificationsService,
  WorksheetTimeHelperService,
} from '../../../../../shared/services';
import { BehaviorSubject, delay, Subscription } from 'rxjs';
import {
  IAppSettingsJobStatusChangeRule,
  IEmployeeOperation,
  IJob,
  ISelectItem,
} from '../../../../../shared/interfaces';
import {
  IJobStatusChangeDto,
  IJobWorksheetCreateDto,
} from '../../../../../shared/dto';
import { SelectComponent } from '../../../../../shared/components/select/select.component';
import { SpinnerComponent } from '../../../../../shared/components/spinner/spinner.component';
import { SwitchComponent } from '../../../../../shared/components/switch/switch.component';
import { MpListItemPipe } from '../../../../../shared/pipes/mp-list-item.pipe';

@Component({
  selector: 'app-start-worksheet',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SelectComponent,
    SpinnerComponent,
    SwitchComponent,
    MpListItemPipe,
  ],
  templateUrl: './start-worksheet.component.html',
  styleUrls: ['./start-worksheet.component.scss'],
})
export class StartWorksheetComponent implements OnInit {
  @Input() job!: IJob;
  @Input() operation!: string;
  @Input() user!: User;

  public statuses$: BehaviorSubject<ISelectItem[]> = new BehaviorSubject<
    ISelectItem[]
  >([]);
  public error: string | null = null;
  public processing = false;
  public canCancel = false;
  public dontChangeStatus = false;
  public form: FormGroup = this._fb.group({});
  public needForm = false;

  private _subscription = new Subscription();
  private _rule: IAppSettingsJobStatusChangeRule | null = null;
  private _operations: IEmployeeOperation[] | null = null;

  constructor(
    private _fb: FormBuilder,
    private _modalService: ModalService,
    private _statusAutomationService: JobStatusAutomationRulesService,
    private _jobsService: JobsService,
    private _worksheetsService: JobWorksheetsService,
    private _worksheetTimeService: WorksheetTimeHelperService,
    private _employeesService: EmployeesService,
    private _notificationService: NotificationsService,
  ) {}

  ngOnInit() {
    const operationWorksheets = this.job.worksheets?.filter(
      (ws) => ws.operation === this.operation,
    );

    this._rule =
      operationWorksheets && operationWorksheets.length
        ? this._statusAutomationService.getOnStartOneRule(
            this.job,
            this.operation,
          )
        : this._statusAutomationService.getOnStartFirstRule(
            this.job,
            this.operation,
          );

    this._employeesService.getOperations().subscribe((operations) => {
      this._operations = operations;

      if (!this._rule) {
        this.processing = true;
        this._recordStartWorksheet();
        return;
      }

      if (this._rule.toStatuses.length === 1 && !this._rule.canCancel) {
        this._processWithStatusChange(this._rule.toStatuses[0].statusNumber);
        return;
      }

      this.needForm = true;
      this.canCancel = this._rule.canCancel;

      this.statuses$.next(
        this._rule.toStatuses.map((status) => {
          return {
            label: status.statusText,
            value: status.statusNumber,
          };
        }),
      );

      if (!this.canCancel) {
        this.form.addControl(
          'statusNumber',
          new FormControl<number | null>(null, Validators.required),
        );
      } else {
        this.form.addControl(
          'statusNumber',
          new FormControl<number | null>(null),
        );
        this.form.addControl(
          'dontChangeStatus',
          new FormControl<boolean>(false),
        );

        this._subscription.add(
          this.form.get('dontChangeStatus')?.valueChanges.subscribe((value) => {
            if (value !== null) {
              this.dontChangeStatus = value;
            }
          }),
        );
      }

      this.form
        .get('statusNumber')
        ?.setValue(this._rule.toStatuses[0].statusNumber);
    });
  }

  public get displayForm(): boolean {
    return this.needForm && !this.processing;
  }

  onCancel(): void {
    this._modalService.close({ result: 'cancel' });
  }

  onSubmit(): void {
    this.error = null;
    const statusNumber = this.form.get('statusNumber')?.value;
    const dontChangeStatus = this.form.get('dontChangeStatus')?.value;

    if (dontChangeStatus) {
      this._recordStartWorksheet();
      return;
    }

    if (!statusNumber) {
      this.error = 'Merci de renseigner un statut';
      return;
    }

    this._processWithStatusChange(statusNumber);
  }

  private _processWithStatusChange(statusNumber: number): void {
    if (!this.job) {
      return;
    }

    this.processing = true;

    const dto: IJobStatusChangeDto = {
      statusNumber,
      reason: `[API] changement par ${this.user.completeName}`,
    };

    this._jobsService.changeJobStatus(this.job.mp.number, dto).subscribe({
      next: () => {
        this._recordStartWorksheet();
      },
      error: (err) => {
        this.error = `Impossible d'effectuer l'opération. ${err.error.message}`;
        this.processing = false;
      },
    });
  }

  private _recordStartWorksheet(): void {
    if (!this.user || !this._operations || !this.job) {
      return;
    }

    const employeeOperation = this._operations.find(
      (op) => op.operation === this.operation,
    );

    if (!employeeOperation) {
      return;
    }

    const worksheet: IJobWorksheetCreateDto = {
      job_number: this.job.mp.number,
      date: new Date().toLocaleDateString('fr-FR'),
      employee_number: this.user.mp.employeeNumber,
      operation_number: employeeOperation.operationNumber,
      start_time: this._worksheetTimeService.getStartTime(),
      operation_ready: false,
    };

    this._worksheetsService
      .addOne(this.job._id, worksheet)
      .pipe(delay(800))
      .subscribe({
        next: () =>
          this._notificationService.displaySuccess(
            'Enregistrement de temps démarré!',
          ),
        error: () => {
          this._notificationService.displayError(
            'Une erreur est survenue lors de la création de la fiche',
          );
          this.processing = false;
        },
        complete: () => this._modalService.close(),
      });
  }
}
