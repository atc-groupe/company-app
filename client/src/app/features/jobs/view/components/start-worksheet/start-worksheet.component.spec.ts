import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StartWorksheetComponent } from './start-worksheet.component';

describe('StartWorksheetComponent', () => {
  let component: StartWorksheetComponent;
  let fixture: ComponentFixture<StartWorksheetComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [StartWorksheetComponent]
    });
    fixture = TestBed.createComponent(StartWorksheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
