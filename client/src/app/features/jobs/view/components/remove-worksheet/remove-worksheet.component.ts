import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { User } from '../../../../../shared/entities';
import {
  IAppSettingsJobStatusChangeRule,
  IJob,
  IJobWorksheet,
  ISelectItem,
} from '../../../../../shared/interfaces';
import {
  JobsService,
  JobStatusAutomationRulesService,
  JobWorksheetsService,
  ModalService,
  NotificationsService,
} from '../../../../../shared/services';
import { ClickOutsideDirective } from '../../../../../shared/directives/click-outside.directive';
import { IJobStatusChangeDto } from '../../../../../shared/dto';
import { SpinnerComponent } from '../../../../../shared/components/spinner/spinner.component';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  ReactiveFormsModule,
} from '@angular/forms';
import { BehaviorSubject, Subscription } from 'rxjs';
import { SelectComponent } from '../../../../../shared/components/select/select.component';
import { SwitchComponent } from '../../../../../shared/components/switch/switch.component';

@Component({
  selector: 'app-remove-worksheet',
  standalone: true,
  imports: [
    CommonModule,
    ClickOutsideDirective,
    SpinnerComponent,
    ReactiveFormsModule,
    SelectComponent,
    SwitchComponent,
  ],
  templateUrl: './remove-worksheet.component.html',
  styleUrls: ['./remove-worksheet.component.scss'],
})
export class RemoveWorksheetComponent implements OnInit, OnDestroy {
  @Input() job!: IJob;
  @Input() worksheet!: IJobWorksheet;
  @Input() user!: User;

  public error: string | null = null;
  public processing = false;
  public canCancel = false;
  public dontChangeStatus = false;
  public form: FormGroup = this._fb.group({});
  public statuses$: BehaviorSubject<ISelectItem[]> = new BehaviorSubject<
    ISelectItem[]
  >([]);

  private _subscription = new Subscription();
  private _rule: IAppSettingsJobStatusChangeRule | null = null;

  constructor(
    private _rulesService: JobStatusAutomationRulesService,
    private _modalService: ModalService,
    private _worksheetsService: JobWorksheetsService,
    private _jobsService: JobsService,
    private _fb: FormBuilder,
    private _notificationService: NotificationsService,
  ) {}

  ngOnInit() {
    this._rule = this._rulesService.getOnRemoveUniqueRule(
      this.job,
      this.worksheet.operation,
    );

    if (this._rule) {
      this.statuses$.next(
        this._rule.toStatuses.map((status) => {
          return {
            label: status.statusText,
            value: status.statusNumber,
          };
        }),
      );

      this.form.addControl(
        'statusNumber',
        new FormControl<number>(this._rule.toStatuses[0].statusNumber),
      );

      if (this._rule.canCancel) {
        this.form.addControl(
          'dontChangeStatus',
          new FormControl<boolean>(false),
        );

        this._subscription.add(
          this.form.get('dontChangeStatus')?.valueChanges.subscribe((value) => {
            this.dontChangeStatus = value;
          }),
        );
      }
    }
  }

  public closeModal(): void {
    this._modalService.close();
  }

  public get displayStatus(): boolean {
    if (
      !this._rule ||
      (this._rule && !this._rule.canCancel && this._rule.toStatuses.length < 2)
    ) {
      return false;
    }

    return !this.dontChangeStatus;
  }

  public get displayDontChangeStatus(): boolean {
    return !!this._rule && this._rule.canCancel;
  }

  public onSubmit(): void {
    if (!this._rule || this.dontChangeStatus) {
      this._processWorksheetDelete();
      return;
    }

    const statusNumber = this.form.get('statusNumber')?.value;

    if (!statusNumber) {
      this.error = 'Aucun statut sélectionné pour le job';
      return;
    }

    this.processing = true;

    const dto: IJobStatusChangeDto = {
      statusNumber,
      reason: `[API] changement par ${this.user.completeName}`,
    };

    this._jobsService.changeJobStatus(this.job.mp.number, dto).subscribe({
      next: () => {
        this._processWorksheetDelete();
      },
      error: (err) => {
        this.error = `Impossible d'effectuer l'opération. ${err.error.message}`;
        this.processing = false;
      },
    });
  }

  private _processWorksheetDelete(): void {
    this._worksheetsService
      .removeOne(this.job._id, this.worksheet.id)
      .subscribe({
        next: () => {
          this._notificationService.displaySuccess('Fiche supprimée!');
          this._modalService.close();
        },
        error: (err) => {
          this.processing = false;
          this.error = err.error.message;
        },
      });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
