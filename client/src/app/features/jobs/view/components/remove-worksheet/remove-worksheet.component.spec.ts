import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoveWorksheetComponent } from './remove-worksheet.component';

describe('RemoveWorksheetComponent', () => {
  let component: RemoveWorksheetComponent;
  let fixture: ComponentFixture<RemoveWorksheetComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RemoveWorksheetComponent]
    });
    fixture = TestBed.createComponent(RemoveWorksheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
