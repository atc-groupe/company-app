import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { User } from '../../../../../shared/entities';
import {
  IAppSettingsJobStatusChangeRule,
  IJob,
  IJobWorksheet,
  ISelectItem,
} from '../../../../../shared/interfaces';
import {
  JobsService,
  ModalService,
  UserOperationsService,
  WorksheetTimeHelperService,
  JobStatusAutomationRulesService,
  JobWorksheetsService,
  NotificationsService,
} from '../../../../../shared/services';
import { IJobWorksheetUpdateDto } from '../../../../../shared/dto/i-job-worksheet-update.dto';
import { SwitchComponent } from '../../../../../shared/components/switch/switch.component';
import { BehaviorSubject, Subscription } from 'rxjs';
import { SelectComponent } from '../../../../../shared/components/select/select.component';
import { IJobStatusChangeDto } from '../../../../../shared/dto';
import { SpinnerComponent } from '../../../../../shared/components/spinner/spinner.component';
import { MpListItemPipe } from '../../../../../shared/pipes/mp-list-item.pipe';
import { MessageComponent } from '../../../../../shared/components/message/message.component';
import { CancelButtonComponent } from '../../../../../shared/components/cancel-button/cancel-button.component';
import { ModalCloseDirective } from '../../../../../shared/directives/modal-close.directive';
import { SaveButtonComponent } from '../../../../../shared/components/save-button/save-button.component';

@Component({
  selector: 'app-edit-worksheet',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SwitchComponent,
    SelectComponent,
    SpinnerComponent,
    MpListItemPipe,
    MessageComponent,
    CancelButtonComponent,
    ModalCloseDirective,
    SaveButtonComponent,
  ],
  templateUrl: './edit-worksheet.component.html',
  styleUrls: ['./edit-worksheet.component.scss'],
})
export class EditWorksheetComponent implements OnInit, OnDestroy {
  @Input() job!: IJob;
  @Input() worksheet!: IJobWorksheet;
  @Input() user!: User;
  @Input() action!: 'stop' | 'update';

  public error: string | null = null;
  public canSetOperationReady = false;
  public processing = false;
  public statuses$ = new BehaviorSubject<ISelectItem[]>([]);
  public canCancel = false;
  public dontChangeStatus = false;
  public rule: IAppSettingsJobStatusChangeRule | null = null;
  public isNotToday = false;
  public form: FormGroup = this._fb.group({
    hours: [0, [Validators.required, Validators.min(0), Validators.max(23)]],
    minutes: [30, [Validators.required, Validators.min(0), Validators.max(59)]],
    operationReady: [false],
    remark: [''],
  });

  private _subscription = new Subscription();
  private _onStopOneRule: IAppSettingsJobStatusChangeRule | null = null;
  private _onCompleteLastRule: IAppSettingsJobStatusChangeRule | null = null;
  private _operationReady = false;

  constructor(
    private _fb: FormBuilder,
    private _modalService: ModalService,
    private _worksheetsService: JobWorksheetsService,
    private _userOperationService: UserOperationsService,
    private _statusAutomationService: JobStatusAutomationRulesService,
    private _jobsService: JobsService,
    private _worksheetTimeHelper: WorksheetTimeHelperService,
    private _notificationService: NotificationsService,
  ) {}

  ngOnInit() {
    const time = this.worksheet.totalTime
      ? this._worksheetTimeHelper.getTimeFromTotalTime(this.worksheet.totalTime)
      : this._worksheetTimeHelper.getCalculatedTime(this.worksheet);

    this.isNotToday = this._worksheetTimeHelper.isNotToday(this.worksheet);

    this.canSetOperationReady = this.job.worksheets
      ? this._userOperationService.canSetWorksheetOperationReady(
          this.user,
          this.worksheet,
          this.job.worksheets,
        )
      : false;

    this._onStopOneRule = this._statusAutomationService.getOnStopOneRule(
      this.job,
      this.worksheet.operation,
    );
    this._onCompleteLastRule =
      this._statusAutomationService.getOnCompleteLastOperationRule(
        this.job,
        this.worksheet.operation,
      );
    this.rule = this.worksheet.operationReady
      ? this._onCompleteLastRule
      : this._onStopOneRule;
    this._operationReady = this.worksheet.operationReady;

    this._subscription.add(
      this.form.controls['operationReady'].valueChanges.subscribe(
        (operationReady) => {
          this._operationReady = operationReady;
          this.rule = operationReady
            ? this._onCompleteLastRule
            : this._onStopOneRule;

          if (this.rule) {
            this.statuses$.next(
              this.rule.toStatuses.map((status) => {
                return {
                  label: status.statusText,
                  value: status.statusNumber,
                };
              }),
            );

            this.canCancel = this.rule.canCancel;

            this.form.addControl('statusNumber', new FormControl<number>(0));
            this.form
              .get('statusNumber')
              ?.setValue(this.rule.toStatuses[0].statusNumber);
          } else {
            this.canCancel = false;
            this.form.removeControl('statusNumber');
          }

          if (this.canCancel) {
            this.form.addControl(
              'dontChangeStatus',
              new FormControl<boolean>(false),
            );

            this._subscription.add(
              this.form
                .get('dontChangeStatus')
                ?.valueChanges.subscribe((value) => {
                  this.dontChangeStatus = value ? value : false;
                }),
            );
          } else {
            this.form.removeControl('dontChangeStatus');
          }
        },
      ),
    );

    this.form.get('hours')?.setValue(time.hours);
    this.form.get('minutes')?.setValue(time.minutes);
    this.form.get('operationReady')?.setValue(this.worksheet.operationReady);
    this.form.get('remark')?.setValue(this.worksheet.remark);
  }

  public get displayStatus(): boolean {
    if (this._dontProcessStatusChange() || !this.rule) {
      return false;
    }

    return (
      this.rule.canCancel ||
      (!this.rule.canCancel && this.rule.toStatuses.length > 1)
    );
  }

  public get displayDontChangeStatus(): boolean {
    if (this.action === 'update' && !this._operationReady) {
      return false;
    }

    return !!this.rule && this.rule.canCancel;
  }

  public onSubmit(): void {
    const operationReady = this.form.get('operationReady')?.value === true;
    let hours = this.form.get('hours')?.value;
    let minutes = this.form.get('minutes')?.value;
    const remark = this.form.get('remark')?.value;
    const statusNumber = this.form.get('statusNumber')?.value;

    if (!hours && !minutes) {
      this.error = 'Merci de renseigner un temps';
      return;
    }

    if (!hours) {
      hours = 0;
    }

    minutes = minutes ? minutes / 60 : 0;
    const time = hours + minutes;

    const dto: IJobWorksheetUpdateDto = {
      operation: this.worksheet.operation,
      operation_ready: operationReady,
      time_production: time,
    };

    if (remark) {
      dto.remark = remark;
    }

    if (this._dontProcessStatusChange()) {
      this._processWorksheet(dto);
      return;
    }

    if (!statusNumber) {
      this.error = 'Veuillez sélectionner un statut';
      return;
    }

    this._processStatusChangeAndWorksheet(statusNumber, dto);
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  private _processStatusChangeAndWorksheet(
    statusNumber: number,
    worksheetDto: IJobWorksheetUpdateDto,
  ): void {
    this.processing = true;

    const dto: IJobStatusChangeDto = {
      statusNumber,
      reason: `[API] changement par ${this.user.completeName}`,
    };

    this._jobsService.changeJobStatus(this.job.mp.number, dto).subscribe({
      next: () => {
        this._processWorksheet(worksheetDto);
      },
      error: (err) => {
        this.error = `Impossible d'effectuer l'opération. ${err.error.message}`;
        this.processing = false;
      },
    });
  }

  private _processWorksheet(dto: IJobWorksheetUpdateDto): void {
    this._worksheetsService
      .updateOne(this.job._id, this.worksheet.id, dto)
      .subscribe({
        next: () => {
          this._notificationService.displaySuccess('Fiche enregistrée!');
          this._modalService.close();
        },
        error: (err) => {
          this.processing = false;
          this.error = err.error.message;
        },
      });
  }

  private _dontProcessStatusChange(): boolean {
    return (
      !this.rule ||
      this.dontChangeStatus ||
      this.worksheet.operationReady ||
      (this.action === 'update' && !this._operationReady)
    );
  }
}
