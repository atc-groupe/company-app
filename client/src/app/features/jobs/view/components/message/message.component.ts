import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpinnerComponent } from '../../../../../shared/components/spinner/spinner.component';

@Component({
  selector: 'job-view-message',
  standalone: true,
  imports: [CommonModule, SpinnerComponent],
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss'],
})
export class MessageComponent {
  @Input() error: string | null = null;
  @Input() isLoading = false;

  public get displayLoader(): boolean {
    return this.isLoading;
  }

  public get displayError(): boolean {
    return !!this.error;
  }
}
