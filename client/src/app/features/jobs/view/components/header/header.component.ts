import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { User } from '../../../../../shared/entities';
import { JobStatusComponent } from '../../../../../shared/components/job-status/job-status.component';
import { RouterLink } from '@angular/router';
import { IJob } from '../../../../../shared/interfaces';

@Component({
  selector: 'job-view-header',
  standalone: true,
  imports: [CommonModule, JobStatusComponent, RouterLink],
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  @Input() job: IJob | null = null;
  @Input() from: string | null = null;
  @Input() user: User | null = null;
  @Input() canUpdateStatus = false;

  public copyCheck: true | null = null;

  @Output() private toggleNav = new EventEmitter<void>();
  @Output() private reSyncJob = new EventEmitter<void>();
  @Output() private updateStatus = new EventEmitter<void>();

  public get displayMailIcon(): boolean {
    return this.user ? this.user.isGraphicDesigner() : false;
  }

  onToggleNav(): void {
    this.toggleNav.emit();
  }

  public onReSyncJob(): void {
    this.toggleNav.emit();
    this.reSyncJob.emit();
  }

  public onUpdateStatus(): void {
    if (!this.canUpdateStatus) {
      return;
    }

    this.updateStatus.emit();
  }

  public async getMailSubject(): Promise<void> {
    if (!this.job || !this.user) {
      return;
    }

    try {
      const text = `BAT ${this.job.mp.company} [${this.job.mp.number}] // ${this.user.initials} // ${this.job.mp.description}`;
      await navigator.clipboard.writeText(text);
      this.copyCheck = true;
      setTimeout(() => {
        this.copyCheck = null;
      }, 1000);
    } catch {}
  }
}
