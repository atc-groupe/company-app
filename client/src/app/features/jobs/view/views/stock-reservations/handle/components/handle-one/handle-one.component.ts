import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IHandleStockReservationData } from '../../../../../interfaces/i-handle-stock-reservation-data';
import { SwitchComponent } from '../../../../../../../../shared/components/switch/switch.component';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { Paper } from '../../../../../../../../shared/entities';
import { ISelectItem } from '../../../../../../../../shared/interfaces';
import { SelectComponent } from '../../../../../../../../shared/components/select/select.component';
import { first, Observable, Subject } from 'rxjs';
import { SpinnerComponent } from '../../../../../../../../shared/components/spinner/spinner.component';

@Component({
  selector: 'job-stock-reservation-handle-one',
  standalone: true,
  imports: [
    CommonModule,
    SwitchComponent,
    FormsModule,
    ReactiveFormsModule,
    SelectComponent,
    SpinnerComponent,
  ],
  templateUrl: './handle-one.component.html',
  styleUrls: ['./handle-one.component.scss'],
})
export class HandleOneComponent implements OnInit {
  @Input({ required: true }) data!: IHandleStockReservationData;
  @Input({ required: true }) papers$!: Observable<Paper[] | null>;
  @Output() toggle = new EventEmitter<void>();
  @Output() changeQuantity = new EventEmitter<number>();
  @Output() selectPaper = new EventEmitter<Paper>();

  public freeStock: number | null = null;
  public items$ = new Subject<ISelectItem[]>();
  public form: FormGroup = this._fb.group({
    enabled: [true],
    paper: new FormControl<number | null>(null, Validators.required),
    quantity: new FormControl<number | null>(null, [
      Validators.required,
      Validators.min(0),
    ]),
  });
  private _papers: Paper[] | null = null;
  private _quantity = 0;

  constructor(private _fb: FormBuilder) {
    this.form.controls['enabled'].valueChanges
      .pipe(takeUntilDestroyed())
      .subscribe((value) => {
        if (value !== null) {
          this.toggle.emit();

          if (value) {
            this.form.controls['paper'].enable({ emitEvent: false });
          } else {
            this.form.controls['paper'].disable({ emitEvent: false });
          }
        }
      });

    this.form.controls['quantity'].valueChanges
      .pipe(takeUntilDestroyed())
      .subscribe((value) => {
        if (value === null) {
          return;
        }

        if (value < 0) {
          this.form.controls['quantity'].setValue(0, {
            emitEvent: false,
          });
          this.changeQuantity.emit(0);
          this._quantity = 0;
          return;
        }

        if (value > this._quantity && this.data.isAllReserved) {
          this.form.controls['quantity'].setValue(this._quantity, {
            emitEvent: false,
          });
          this.changeQuantity.emit(this._quantity);

          return;
        }

        if (value > this.data.max) {
          this.form.controls['quantity'].setValue(this.data.max, {
            emitEvent: false,
          });
          this._quantity = this.data.max;
          this.changeQuantity.emit(this.data.max);

          return;
        }

        this.changeQuantity.emit(value);
        this._quantity = value;
      });

    this.form.controls['paper'].valueChanges
      .pipe(takeUntilDestroyed())
      .subscribe((value: number | null) => {
        if (value !== null) {
          const paper = this._papers?.find((item) => item.id === value);

          if (paper) {
            this.selectPaper.emit(paper);
            this._setFreeStock(paper);
          }
        }
      });
  }

  ngOnInit(): void {
    this.form.controls['enabled'].setValue(this.data.enabled, {
      emitEvent: false,
    });

    if (this.data.reservation.paper) {
      this.freeStock = this.data.reservation.paper.mp.freeStock;
      this.form.controls['quantity'].addValidators(
        Validators.max(this.data.max),
      );
      this.form.controls['quantity'].setValue(this.data.quantity, {
        emitEvent: false,
      });
      this._quantity = this.data.quantity;
    }

    this.papers$.pipe(first((v) => v !== null)).subscribe((papers) => {
      if (!papers) {
        return;
      }

      this._papers = papers;

      if (this.data.reservation.paperGenericName) {
        const filteredPapers = papers.filter(
          (item) =>
            item.genericName &&
            item.genericName._id ===
              this.data.reservation.paperGenericName!._id,
        );
        const items = filteredPapers.map((item) => {
          return {
            value: item.id,
            label: `${item.completeName} (${item.freeStock} en stock)`,
          };
        });

        this.items$.next(items);

        if (filteredPapers.length === 1) {
          this.form.controls['paper'].setValue(filteredPapers[0].id);
          this._setFreeStock(filteredPapers[0]);
        }
      }

      if (this.data.reservation.paperName) {
        const filteredPapers = papers.filter(
          (item) => item.name === this.data.reservation.paperName,
        );
        const items = filteredPapers.map((item) => {
          return {
            value: item.id,
            label: `${item.completeName} (${item.freeStock} en stock)`,
          };
        });

        this.items$.next(items);

        if (items.length === 1) {
          this.form.controls['paper'].setValue(filteredPapers[0].id);
          this._setFreeStock(filteredPapers[0]);
        }
      }
    });
  }

  public get freeStockChipColor(): string {
    if (this.freeStock === null) {
      return '';
    }

    return this.freeStock >= this.data.reservation.quantity
      ? 'chip-success'
      : 'chip-error';
  }

  get displaySize(): string {
    return this.data.reservation.width && this.data.reservation.height
      ? `${this.data.reservation.width} x ${this.data.reservation.height}`
      : 'Plaque entière';
  }

  private _setFreeStock(paper: Paper): void {
    this.freeStock = paper.freeStock;
    this._quantity = this.data.quantity;
    this.form.controls['quantity'].setValue(this.data.quantity);
  }

  get displayPaper(): boolean {
    return (
      !!this.data.reservation.paper &&
      !this.data.reservation.paperGenericName &&
      !this.data.reservation.paperName
    );
  }
}
