import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Paper, User } from '../../../../../../../../shared/entities';
import { IJobStockReservation } from '../../../../../../../../shared/interfaces/job/i-job-stock-reservation';
import {
  JobStockReservationsService,
  ModalService,
  NotificationsService,
  StockService,
} from '../../../../../../../../shared/services';
import {
  FormBuilder,
  FormControl,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { BehaviorSubject, map, Observable, Subscription } from 'rxjs';
import { ISelectItem } from '../../../../../../../../shared/interfaces';
import {
  JobStockReservationStatusEnum,
  PaperGroupTypeEnum,
  ProductTypeEnum,
} from '../../../../../../../../shared/enums';
import { SelectComponent } from '../../../../../../../../shared/components/select/select.component';
import { SwitchComponent } from '../../../../../../../../shared/components/switch/switch.component';
import { IJobStockReservationDto } from '../../../../../../../../shared/dto';
import { MessageComponent } from '../../../../../../../../shared/components/message/message.component';
import { CancelButtonComponent } from '../../../../../../../../shared/components/cancel-button/cancel-button.component';
import { ModalCloseDirective } from '../../../../../../../../shared/directives/modal-close.directive';
import { SaveButtonComponent } from '../../../../../../../../shared/components/save-button/save-button.component';

@Component({
  selector: 'app-edit',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SelectComponent,
    SwitchComponent,
    MessageComponent,
    CancelButtonComponent,
    ModalCloseDirective,
    SaveButtonComponent,
  ],
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
})
export class EditComponent implements OnInit, OnDestroy {
  @Input() user!: User;
  @Input() jobNumber!: number;
  @Input() reservation: IJobStockReservation | null = null;

  public choiceValue: number | null = null;
  public width: number | null = null;
  public height: number | null = null;
  public error: string | null = null;
  public form = this._fb.group({
    choice: new FormControl<number | null>(null),
    paperGenericName: [''],
    paperName: [''],
    paper: new FormControl<number | null>(null),
    width: new FormControl<number | null>(null),
    height: new FormControl<number | null>(null),
    imperativeFormat: [false, Validators.required],
    quantity: new FormControl<number | null>(null, [Validators.required]),
    printSides: ['', Validators.required],
    destination: ['', Validators.required],
    comment: [''],
  });
  public choice: ISelectItem[] = [
    { label: 'Nom générique', value: 1 },
    { label: 'Matière précise avec format personnalisé', value: 2 },
    { label: 'Matière et format précis', value: 3 },
  ];
  public paperGenericNames$: Observable<ISelectItem[]>;
  public paperNames$ = new BehaviorSubject<ISelectItem[]>([]);
  public papers$ = new BehaviorSubject<ISelectItem[]>([]);
  public printSides: ISelectItem[] = [
    { label: 'SANS IMPRESSION', value: 'SANS IMPRESSION' },
    { label: 'RECTO', value: 'RECTO' },
    { label: 'RECTO/VERSO', value: 'RECTO/VERSO' },
  ];
  public destinations: ISelectItem[] = [
    { label: 'KUDU', value: 'KUDU' },
    { label: 'PLASTIF', value: 'PLASTIF' },
    { label: 'CONTRE-COLLAGE', value: 'CONTRE-COLLAGE' },
    { label: 'ZUND', value: 'ZUND' },
    { label: 'DÉCOUPE', value: 'DÉCOUPE' },
    { label: 'AUTRE', value: 'AUTRE' },
  ];

  private _subscription = new Subscription();
  private _sheets: Paper[] | null = null;

  constructor(
    private _reservationService: JobStockReservationsService,
    private _stockService: StockService,
    private _fb: FormBuilder,
    private _modalService: ModalService,
    private _notificationService: NotificationsService,
  ) {
    this.paperGenericNames$ = this._stockService.getSheetsGenericNames().pipe(
      map((data) =>
        data.map((item) => {
          return {
            value: item._id,
            label: item.name,
          };
        }),
      ),
    );
  }

  ngOnInit(): void {
    this._stockService
      .getProductsList(ProductTypeEnum.Paper, PaperGroupTypeEnum.Sheets)
      .subscribe({
        next: (sheets) => {
          this._sheets = sheets as Paper[];
          this.paperNames$.next(
            sheets
              .reduce((acc: string[], product) => {
                if (!acc.includes(product.name)) {
                  acc.push(product.name);
                }

                return acc;
              }, [])
              .map((item) => {
                return {
                  value: item,
                  label: item,
                };
              }),
          );

          this.papers$.next(
            sheets.map((product) => {
              return {
                value: product.id,
                label: `${product.completeName} [${product.freeStock} en stock]`,
              };
            }),
          );

          this._setInitValues();
        },
        error: (err) => {
          this.error = err.error.message;
        },
      });

    this._subscription.add(
      this.form.get('choice')?.valueChanges.subscribe((value) => {
        this.choiceValue = value;

        if (this.choiceValue === 1 || this.choiceValue === 2) {
          this.form.controls.width.setValidators(Validators.required);
          this.form.controls.height.setValidators(Validators.required);
        } else {
          this.form.controls.width.setValidators([]);
          this.form.controls.height.setValidators([]);
        }

        if (this.choiceValue === 1) {
          this.form.controls.paperGenericName.setValidators(
            Validators.required,
          );
          this.form.controls.paperName.setValidators([]);
          this.form.controls.paper.setValidators([]);
        }

        if (this.choiceValue === 2) {
          this.form.controls.paperGenericName.setValidators([]);
          this.form.controls.paperName.setValidators(Validators.required);
          this.form.controls.paper.setValidators([]);
        }

        if (this.choiceValue === 3) {
          this.form.controls.paperGenericName.setValidators([]);
          this.form.controls.paperName.setValidators([]);
          this.form.controls.paper.setValidators(Validators.required);
        }
      }),
    );

    this._subscription.add(
      this.form.controls.paperGenericName.valueChanges.subscribe((value) => {
        if (!value) {
          this._resetSize();
        } else {
          this._setGenericPaperSizeValidators(value);
        }
      }),
    );

    this._subscription.add(
      this.form.controls.paperName.valueChanges.subscribe((value) => {
        if (!value) {
          this._resetSize();
        } else {
          this._setPaperNameSizeValidators(value);
        }
      }),
    );
  }

  public onSubmit(): void {
    const paperGenericName = this.form.controls.paperGenericName.value;
    const paperName = this.form.controls.paperName.value;
    const paper = this.form.controls.paper.value;
    const width = this.form.controls.width.value;
    const height = this.form.controls.height.value;
    const imperativeFormat = this.form.controls.imperativeFormat.value;
    const quantity = this.form.controls.quantity.value;
    const printSides = this.form.controls.printSides.value;
    const destination = this.form.controls.destination.value;
    const comment = this.form.controls.comment.value;

    if (!this.choiceValue) {
      this.error = 'Données incomplètes';
      return;
    }

    if (this.choiceValue !== 3 && (!width || !height)) {
      this.error = 'Veuillez renseigner un format';
      return;
    }

    if (!quantity) {
      this.error = 'Veuillez renseigner une quantité';
      return;
    }

    if (!printSides) {
      this.error = 'Veuillez renseigner les faces imprimées';
      return;
    }

    if (!destination) {
      this.error = 'Veuillez renseigner une destination';
      return;
    }

    if (!this.form.valid) {
      return;
    }

    const dto: IJobStockReservationDto = {
      status: JobStockReservationStatusEnum.Brouillon,
      imperativeFormat: imperativeFormat !== null ? imperativeFormat : false,
      quantity,
      printSides,
      destination,
    };

    if (width && height) {
      dto.width = width;
      dto.height = height;
    }

    if (comment) {
      dto.comment = comment;
    }

    if (this.choiceValue === 1) {
      if (!paperGenericName) {
        this.error = 'Veuillez renseigner un nom de matière';
        return;
      }

      dto.paperGenericName = paperGenericName;
      dto.paperName = null;
      dto.paper = null;
    }

    if (this.choiceValue === 2) {
      if (!paperName) {
        this.error = 'Veuillez renseigner un nom de matière';
        return;
      }

      dto.paperGenericName = null;
      dto.paperName = paperName;
      dto.paper = null;
    }

    if (this.choiceValue === 3) {
      if (!paper) {
        this.error = 'Veuillez renseigner une matière';
        return;
      }

      dto.paperGenericName = null;
      dto.paperName = null;
      dto.paper = paper;
    }

    const action$ = this.reservation
      ? this._reservationService.updateOne(
          this.jobNumber,
          this.reservation._id,
          dto,
        )
      : this._reservationService.insertOne(this.jobNumber, dto);

    action$.subscribe({
      next: () => {
        this._notificationService.displaySuccess('Résa ajoutée !');
        this._modalService.close();
      },
      error: (err) => {
        this.error = err.error.message;
      },
    });
  }

  ngOnDestroy(): void {
    this._subscription.unsubscribe();
  }

  private _setInitValues(): void {
    if (this.reservation) {
      if (this.reservation.paperGenericName) {
        this.form.controls.choice.setValue(1);
        this.form.controls.paperGenericName.setValue(
          this.reservation.paperGenericName._id,
        );
        this.choiceValue = 1;
      }

      if (this.reservation.paperName) {
        this.form.controls.choice.setValue(2);
        this.form.controls.paperName.setValue(this.reservation.paperName);
        this.choiceValue = 2;
      }

      if (
        this.reservation.paper &&
        !this.reservation.paperGenericName &&
        !this.reservation.paperName
      ) {
        this.form.controls.choice.setValue(3);
        this.form.controls.paper.setValue(this.reservation.paper._id);
        this.choiceValue = 3;
      }

      this.form.controls.width.setValue(this.reservation.width);
      this.form.controls.height.setValue(this.reservation.height);
      this.form.controls.imperativeFormat.setValue(
        this.reservation.imperativeFormat,
      );
      this.form.controls.quantity.setValue(this.reservation.quantity);
      this.form.controls.printSides.setValue(this.reservation.printSides);
      this.form.controls.destination.setValue(this.reservation.destination);
      this.form.controls.comment.setValue(this.reservation.comment);
    }
  }

  private _resetSize(): void {
    this.width = null;
    this.height = null;
    this.form.controls.width.setValidators([Validators.required]);
    this.form.controls.height.setValidators([Validators.required]);
  }

  private _setGenericPaperSizeValidators(id: string): void {
    if (!this._sheets) {
      return;
    }

    const sheets = this._sheets.filter((item) => item.genericName?._id === id);

    return this._setSizeValidators(sheets);
  }

  private _setPaperNameSizeValidators(name: string): void {
    if (!this._sheets) {
      return;
    }

    const sheets = this._sheets.filter((item) => item.name === name);

    return this._setSizeValidators(sheets);
  }

  private _setSizeValidators(sheets: Paper[]): void {
    const maxFormat = sheets.reduce(
      (acc: [number, number], sheet) => {
        if (sheet.width > acc[0]) {
          acc[0] = sheet.width;
          acc[1] = sheet.height;
          return acc;
        }

        if (sheet.height > acc[1]) {
          acc[0] = sheet.width;
          acc[1] = sheet.height;
        }

        return acc;
      },
      [0, 0],
    );

    this.width = maxFormat[0];
    this.height = maxFormat[1];

    this.form.controls.width.addValidators([
      Validators.required,
      Validators.max(maxFormat[0]),
    ]);
    this.form.controls.height.addValidators([
      Validators.required,
      Validators.max(maxFormat[1]),
    ]);
  }
}
