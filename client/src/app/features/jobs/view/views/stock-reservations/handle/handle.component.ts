import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Store } from '@ngrx/store';
import * as JobSelectors from '../../../../../../shared/store/job/job.selectors';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { Paper, User } from '../../../../../../shared/entities';
import { ProductTypeEnum } from '../../../../../../shared/enums';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { HandleOneComponent } from './components/handle-one/handle-one.component';
import { IHandleStockReservationData } from '../../../interfaces/i-handle-stock-reservation-data';
import {
  JobStockReservationsService,
  NotificationsService,
  StockService,
} from '../../../../../../shared/services';
import {
  catchError,
  delay,
  first,
  firstValueFrom,
  Observable,
  Subject,
  tap,
} from 'rxjs';
import { selectUser } from '../../../../../../shared/store/user/user.selectors';
import {
  HandleReservationService,
  HandleReservationListService,
} from '../../../services';
import { IJob } from '../../../../../../shared/interfaces';
import { setNavbarItemsAction } from '../../../../../../shared/store/navbar';
import { MessageComponent } from '../../../../../../shared/components/message/message.component';

@Component({
  selector: 'app-handle',
  standalone: true,
  imports: [CommonModule, HandleOneComponent, RouterLink, MessageComponent],
  templateUrl: './handle.component.html',
  styleUrls: ['./handle.component.scss'],
})
export class HandleComponent {
  public error: string | null = null;
  public job: IJob | null = null;
  public listData: IHandleStockReservationData[] | null = null;
  public reservationId: string | null = null;
  public data: IHandleStockReservationData | null = null;
  public papers$ = new Subject<Paper[]>();
  private user: User | null = null;

  constructor(
    private _store: Store,
    private _activatedRoute: ActivatedRoute,
    private _stockService: StockService,
    private _reservationService: JobStockReservationsService,
    private _router: Router,
    private _handleOneService: HandleReservationService,
    private _handleListService: HandleReservationListService,
    private _notificationService: NotificationsService,
  ) {
    this.reservationId = this._activatedRoute.snapshot.paramMap.get('id');

    this._stockService.getProductsList(ProductTypeEnum.Paper, 0).subscribe({
      next: (papers) => {
        this.papers$.next(papers as Paper[]);
      },
      error: (err) => {
        this.error = err.error.message;
      },
    });

    this._store
      .select(JobSelectors.selectJob)
      .pipe(first((job) => job !== null))
      .subscribe((job) => {
        this.job = job;

        if (!job || !job.stockReservations || !job.stockReservations.length) {
          return;
        }

        this._store.dispatch(
          setNavbarItemsAction({
            items: [
              { label: `${job?.mp.number}` },
              {
                label: 'Résa matières',
                route: `/jobs/${job.mp.number}/view/stock-reservations/list`,
              },
              { label: 'traitement' },
            ],
          }),
        );

        if (this.reservationId) {
          const reservation = job.stockReservations.find(
            (item) => item._id === this.reservationId,
          );

          if (reservation) {
            this.data = this._handleOneService.getInitData(reservation);
          }
        } else {
          this.listData = this._handleListService.getInitData(
            job.stockReservations,
          );
        }
      });

    this._store
      .select(selectUser)
      .pipe(takeUntilDestroyed())
      .subscribe((user) => (this.user = user));
  }

  public get returnRoute(): string[] {
    return this.data ? ['../..', 'list'] : ['..', 'list'];
  }

  public onToggleUnique() {
    if (this.data) {
      this.data.enabled = !this.data.enabled;
    }
  }

  public onToggle(id: string): void {
    if (this.listData) {
      const itemData = this.listData.find(
        (item) => item.reservation._id === id,
      );

      if (itemData) {
        itemData.enabled = !itemData.enabled;
        this.listData = this._handleListService.getComputedData(this.listData);
      }

      return;
    }
  }

  public onChangeUniqueQuantity(quantity: number) {
    if (this.data) {
      this.data.quantity = quantity;
    }
  }

  public onChangeQuantity(id: string, quantity: number): void {
    if (this.listData) {
      const itemData = this.listData.find(
        (item) => item.reservation._id === id,
      );

      if (itemData) {
        itemData.quantity = quantity;
        this.listData = this._handleListService.getComputedData(this.listData);
      }

      return;
    }
  }

  public onSelectUniquePaper(paper: Paper): void {
    if (this.data) {
      this.data.paper = paper;
    }
  }

  public onSelectPaper(id: string, paper: Paper): void {
    if (this.listData) {
      const itemData = this.listData?.find(
        (item) => item.reservation._id === id,
      );

      if (itemData) {
        itemData.paper = paper;
        this.listData = this._handleListService.getComputedData(this.listData);
      }

      return;
    }
  }

  public async onHandle(): Promise<void> {
    this.error = null;

    if (this.listData) {
      if (this.listData.some((item) => item.paper === null && item.enabled)) {
        this.error = `Veuillez renseigner toutes les matières avant de traiter la résa.`;
        return;
      }

      const dataList: IHandleStockReservationData[] = this.listData.filter(
        (item) => item.enabled,
      );

      if (!dataList.length || !this.user || !this.job) {
        return;
      }

      for (const item of dataList) {
        item.status = 'pending';

        try {
          await firstValueFrom(this._handleOne(item));
        } catch (err: any) {
          this.error = err.error.message;
          return;
        }
      }

      this._notificationService.displaySuccess(
        'La réservation matière a été traitée !',
      );

      await this._router.navigate(this.returnRoute, {
        relativeTo: this._activatedRoute,
      });
    }

    if (this.data) {
      try {
        await firstValueFrom(this._handleOne(this.data));
      } catch (err: any) {
        this.error = err.error.message;
        return;
      }

      this._notificationService.displaySuccess(
        'La réservation matière a été traitée !',
      );

      await this._router.navigate(this.returnRoute, {
        relativeTo: this._activatedRoute,
      });
    }
  }

  private _handleOne(data: IHandleStockReservationData): Observable<void> {
    data.status = 'pending';

    return this._reservationService
      .handle(this.job!.mp.number, data.reservation._id, {
        paperId: data.paper!.id,
        quantity: data.quantity,
      })
      .pipe(
        delay(500),
        tap(() => {
          data.status = 'success';
        }),
        catchError((err) => {
          data.status = 'error';
          throw err;
        }),
      );
  }
}
