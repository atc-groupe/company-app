import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IJobStockReservation } from '../../../../../../../../shared/interfaces/job/i-job-stock-reservation';
import { MenuComponent } from '../../../../../../../../shared/components/menu/menu.component';
import { IMenuItem } from '../../../../../../../../shared/interfaces';
import { ReservationVoter } from '../../../../../services';
import { JobStockReservationStatusEnum } from '../../../../../../../../shared/enums';

@Component({
  selector: 'job-stock-reservation',
  standalone: true,
  imports: [CommonModule, MenuComponent],
  templateUrl: './stock-reservation.component.html',
  styleUrls: ['./stock-reservation.component.scss'],
})
export class StockReservationComponent implements OnInit {
  @Input({ required: true }) reservation!: IJobStockReservation;
  @Input({ required: true }) voter!: ReservationVoter;
  @Input({ required: true }) isAdmin!: boolean;

  @Output() action = new EventEmitter<{
    action: string;
    reservation: IJobStockReservation;
  }>();
  @Output() changeStatus = new EventEmitter<{
    id: string;
    status: JobStockReservationStatusEnum;
  }>();

  public actionItems: IMenuItem[] = [];

  ngOnInit() {
    if (this.reservation.status === JobStockReservationStatusEnum.Brouillon) {
      this.actionItems = [
        {
          icon: 'edit',
          label: 'Modifier',
          action: 'update',
          disabled: !this.voter.canUpdate(),
        },
        {
          icon: 'send',
          label: 'Envoyer',
          action: 'send',
          disabled: !this.voter.canSend(),
        },
        {
          icon: 'delete',
          label: 'Supprimer',
          action: 'delete',
          style: 'error',
          disabled: !this.voter.canRemove(),
        },
      ];
    } else {
      this.actionItems = [
        {
          icon: 'pan_tool',
          label: 'Traiter',
          action: 'handle',
          disabled: !this.voter.canHandle(),
        },
        {
          icon: 'chevron_left',
          label: 'Renvoyer en PAO',
          action: 'returnToDraft',
          disabled: !this.voter.canReturnToDraft(),
        },
        {
          icon: 'local_shipping',
          label: 'Passer en attente de livraison',
          action: 'setWaitingForDelivery',
          disabled: !this.voter.canSetWaitingForDelivery(),
        },
        {
          icon: 'chevron_left',
          label: 'Repasser en attente de traitement',
          action: 'resetWaitingForDelivery',
          disabled: !this.voter.canResetWaitingForDelivery(),
        },
        {
          icon: 'chevron_left',
          label: 'Annuler le traitement',
          action: 'cancel',
          disabled: !this.voter.canCancelReservation(),
        },
      ];
    }
  }

  get displayPaper(): boolean {
    if (!this.reservation.paper) {
      return false;
    }

    if (this.reservation.status === JobStockReservationStatusEnum.Traitee) {
      return true;
    }

    return !this.reservation.paperGenericName && !this.reservation.paperName;
  }

  get displaySize(): string {
    return this.reservation.width && this.reservation.height
      ? `${this.reservation.width} x ${this.reservation.height} mm`
      : 'Plaque entière';
  }

  get statusChipColor(): string {
    switch (this.reservation.status) {
      case JobStockReservationStatusEnum.ATraiter:
      case JobStockReservationStatusEnum.AttenteLivraison:
        return 'chip-info';
      case JobStockReservationStatusEnum.Traitee:
        return 'chip-success';
      default:
        return '';
    }
  }

  public onSelectAction(action: string): void {
    switch (action) {
      case 'send':
      case 'resetWaitingForDelivery':
        this.changeStatus.emit({
          id: this.reservation._id,
          status: JobStockReservationStatusEnum.ATraiter,
        });
        break;
      case 'returnToDraft':
        this.changeStatus.emit({
          id: this.reservation._id,
          status: JobStockReservationStatusEnum.Brouillon,
        });
        break;
      case 'setWaitingForDelivery':
        this.changeStatus.emit({
          id: this.reservation._id,
          status: JobStockReservationStatusEnum.AttenteLivraison,
        });
        break;
      default:
        this.action.emit({ action, reservation: this.reservation });
    }
  }
}
