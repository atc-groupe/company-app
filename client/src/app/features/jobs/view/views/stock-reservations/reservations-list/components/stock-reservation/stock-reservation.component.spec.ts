import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StockReservationComponent } from './stock-reservation.component';

describe('StockReservationComponent', () => {
  let component: StockReservationComponent;
  let fixture: ComponentFixture<StockReservationComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [StockReservationComponent]
    });
    fixture = TestBed.createComponent(StockReservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
