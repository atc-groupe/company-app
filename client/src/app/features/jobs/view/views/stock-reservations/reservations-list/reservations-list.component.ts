import { Component, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  AlertService,
  JobStockReservationsHelperService,
  JobStockReservationsService,
  ModalService,
  NotificationsService,
} from '../../../../../../shared/services';
import { User } from '../../../../../../shared/entities';
import { Subscription, withLatestFrom } from 'rxjs';
import * as JobSelectors from '../../../../../../shared/store/job/job.selectors';
import { Store } from '@ngrx/store';
import { selectUser } from '../../../../../../shared/store/user/user.selectors';
import { EditComponent } from './components/edit/edit.component';
import { StockReservationComponent } from './components/stock-reservation/stock-reservation.component';
import { ReservationsVoter, ReservationVoter } from '../../../services';
import { MenuComponent } from '../../../../../../shared/components/menu/menu.component';
import { IJob, IMenuItem } from '../../../../../../shared/interfaces';
import { IJobStockReservation } from '../../../../../../shared/interfaces/job/i-job-stock-reservation';
import {
  displayErrorNotificationAction,
  displaySuccessNotificationAction,
} from '../../../../../../shared/store/ui';
import { JobStockReservationStatusEnum } from '../../../../../../shared/enums';
import { IJobStockReservationStatusDto } from '../../../../../../shared/dto';
import { Router, RouterLink } from '@angular/router';
import { setNavbarItemsAction } from '../../../../../../shared/store/navbar';
import { EmptyMessageComponent } from '../../../../../../shared/components/empty-message/empty-message.component';

@Component({
  selector: 'app-stock-reservations',
  standalone: true,
  imports: [
    CommonModule,
    StockReservationComponent,
    MenuComponent,
    RouterLink,
    EmptyMessageComponent,
  ],
  templateUrl: './reservations-list.component.html',
  styleUrls: ['./reservations-list.component.scss'],
})
export class ReservationsListComponent implements OnInit, OnDestroy {
  public job: IJob | null = null;
  public user: User | null = null;
  public globalStatus: string | null = null;
  public voter: ReservationsVoter | null = null;
  public otherActions: IMenuItem[] = [];
  public canCreate = false;
  public canHandle = false;

  private _subscription = new Subscription();

  constructor(
    private _modalService: ModalService,
    private _reservationService: JobStockReservationsService,
    private _reservationsHelper: JobStockReservationsHelperService,
    private _alertService: AlertService,
    private _store: Store,
    private _router: Router,
    private _notificationService: NotificationsService,
  ) {}

  ngOnInit() {
    this._subscription.add(
      this._store
        .select(JobSelectors.selectJob)
        .pipe(withLatestFrom(this._store.select(selectUser)))
        .subscribe(([job, user]) => {
          this.job = job;
          this.user = user;

          if (!job) {
            return;
          }

          this._store.dispatch(
            setNavbarItemsAction({
              items: [
                { label: `${job?.mp.number}` },
                { label: 'Résa matières' },
              ],
            }),
          );

          if (job.stockReservations && user) {
            this.globalStatus = this._reservationsHelper.getGlobalStatus(
              job.stockReservations,
            );

            this.canCreate = this._reservationsHelper.canCreate;
            this.canHandle = this._reservationsHelper.canHandle;

            this.voter = new ReservationsVoter(
              this.canCreate,
              this.canHandle,
              job,
            );

            this.otherActions = [
              {
                action: 'returnToDraft',
                label: 'Renvoyer en PAO',
                icon: 'chevron_left',
                disabled: !this.voter?.canReturnToDraft(),
              },
              {
                action: 'setWaitingForDelivery',
                label: 'Passer en attente de livraison',
                icon: 'local_shipping',
                disabled: !this.voter?.canSetWaitingForDelivery(),
              },
              {
                action: 'resetWaitingForDelivery',
                label: 'Repasser en attente de traitement',
                icon: 'chevron_left',
                disabled: !this.voter?.canResetWaitingForDelivery(),
              },
              {
                action: 'deleteAll',
                label: 'Tout supprimer',
                icon: 'delete',
                disabled: !this.voter?.canRemoveAll(),
                style: 'error',
              },
            ];
          }
        }),
    );
  }

  get statusChipColor(): string {
    switch (this.globalStatus) {
      case JobStockReservationStatusEnum.ATraiter:
      case JobStockReservationStatusEnum.AttenteLivraison:
      case JobStockReservationStatusEnum.EnCours:
        return 'chip-info';
      case JobStockReservationStatusEnum.Traitee:
        return 'chip-success';
      default:
        return '';
    }
  }

  public onAddReservation(): void {
    if (!this.job || !this.user || !this.voter?.canAddReservation()) {
      return;
    }

    this._modalService.open(EditComponent, {
      inputs: {
        user: this.user,
        jobNumber: this.job.mp.number,
        reservation: null,
      },
      options: { yPos: 'center' },
    });
  }

  public getVoter(reservation: IJobStockReservation): ReservationVoter {
    return new ReservationVoter(
      this.canCreate,
      this.canHandle,
      reservation,
      this.job!,
    );
  }

  public onGlobalAction(action: string): void {
    switch (action) {
      case 'returnToDraft':
        this._changeAllStatus(
          {
            from: JobStockReservationStatusEnum.ATraiter,
            to: JobStockReservationStatusEnum.Brouillon,
          },
          'Resa renvoyées en PAO!',
        );
        break;
      case 'setWaitingForDelivery':
        this._changeAllStatus(
          {
            from: JobStockReservationStatusEnum.ATraiter,
            to: JobStockReservationStatusEnum.AttenteLivraison,
          },
          'Resa passée en attente de livraison',
        );
        break;
      case 'resetWaitingForDelivery':
        this._changeAllStatus(
          {
            from: JobStockReservationStatusEnum.AttenteLivraison,
            to: JobStockReservationStatusEnum.ATraiter,
          },
          'Resa repassée en attente de traitement',
        );
        break;
      case 'deleteAll':
        this._onDeleteAll();
        break;
    }
  }

  public onReservationAction({
    action,
    reservation,
  }: {
    action: string;
    reservation: IJobStockReservation;
  }): void {
    switch (action) {
      case 'update':
        this._onUpdateReservation(reservation);
        break;
      case 'delete':
        this._onDeleteReservation(reservation);
        break;
      case 'handle':
        if (!this.job) {
          return;
        }
        this._router.navigateByUrl(
          `/jobs/${this.job.mp.number}/view/stock-reservations/handle/${reservation._id}`,
        );
        break;
      case 'cancel':
        this._onCancelOne(reservation);
        break;
    }
  }

  public onChangeStatus({
    id,
    status,
  }: {
    id: string;
    status: JobStockReservationStatusEnum;
  }): void {
    if (!this.job) {
      return;
    }

    this._reservationService
      .updateStatus(this.job.mp.number, id, status)
      .subscribe({
        next: () =>
          this._notificationService.displaySuccess('Statut modifié !'),
        error: (err) =>
          this._notificationService.displayError(
            `Une erreur est survenue. ${err.error.message}`,
          ),
      });
  }

  public onSendAll(): void {
    this._changeAllStatus(
      {
        from: JobStockReservationStatusEnum.Brouillon,
        to: JobStockReservationStatusEnum.ATraiter,
      },
      'Resa envoyées !',
    );
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  private _changeAllStatus(
    dto: IJobStockReservationStatusDto,
    message = 'Statuts modifiés !',
  ): void {
    if (!this.job) {
      return;
    }

    this._reservationService
      .updateAllStatus(this.job.mp.number, dto)
      .subscribe({
        next: () => this._notificationService.displaySuccess(message),
        error: (err) =>
          this._notificationService.displayError(
            `Une erreur est survenue. ${err.error.message}`,
          ),
      });
  }

  private _onUpdateReservation(reservation: IJobStockReservation): void {
    if (!this.job || !this.user) {
      return;
    }

    this._modalService.open(EditComponent, {
      inputs: {
        user: this.user,
        jobNumber: this.job.mp.number,
        reservation,
      },
      options: { fullScreen: true },
    });
  }

  private _onDeleteReservation(reservation: IJobStockReservation): void {
    if (!this.job) {
      return;
    }

    this._alertService
      .open('Supprimer', 'Etes vous sûre de vouloir supprimer cette résa ?', {
        canCancel: true,
        style: 'error',
        canClickOutside: true,
        confirmLabel: 'Supprimer',
      })
      .afterClosed$.subscribe((result) => {
        if (result === 'confirm') {
          this._reservationService
            .removeOne(this.job!.mp.number, reservation._id)
            .subscribe({
              next: () =>
                this._notificationService.displaySuccess('Matière supprimée !'),
              error: (err) =>
                this._notificationService.displayError(
                  `Une erreur est survenue. ${err.error.message}`,
                ),
            });
        }
      });
  }

  private _onDeleteAll(): void {
    if (!this.job) {
      return;
    }

    this._alertService
      .open(
        'Supprimer',
        'Etes vous sûre de vouloir supprimer toutes les résa matières ?',
        {
          canCancel: true,
          style: 'error',
          canClickOutside: true,
          confirmLabel: 'Supprimer',
        },
      )
      .afterClosed$.subscribe((result) => {
        if (result === 'confirm') {
          this._reservationService.removeAll(this.job!.mp.number).subscribe({
            next: () =>
              this._notificationService.displaySuccess(
                'Réservations supprimées !',
              ),
            error: (err) =>
              this._notificationService.displayError(
                `Une erreur est survenue lors de la suppression. ${err.error.message}`,
              ),
          });
        }
      });
  }

  private _onCancelOne(reservation: IJobStockReservation): void {
    if (
      !this.job ||
      !reservation.paper ||
      reservation.subtractQuantity === null
    ) {
      return;
    }

    this._reservationService
      .cancel(this.job.mp.number, reservation._id, {
        paperId: reservation.paper._id,
        quantity: reservation.subtractQuantity,
      })
      .subscribe({
        next: () =>
          this._notificationService.displaySuccess('Traitement annulé !'),
        error: (err) => {
          this._notificationService.displayError(
            `Une erreur est survenue lors de l'annulation'. ${err.error.message}`,
          );
        },
      });
  }
}
