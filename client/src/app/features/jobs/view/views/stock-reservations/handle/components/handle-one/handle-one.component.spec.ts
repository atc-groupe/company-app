import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HandleOneComponent } from './handle-one.component';

describe('HandleOneComponent', () => {
  let component: HandleOneComponent;
  let fixture: ComponentFixture<HandleOneComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HandleOneComponent]
    });
    fixture = TestBed.createComponent(HandleOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
