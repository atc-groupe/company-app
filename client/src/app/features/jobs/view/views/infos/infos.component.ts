import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Store } from '@ngrx/store';
import * as JobSelectors from '../../../../../shared/store/job/job.selectors';
import { FeatureNavHeadingComponent } from '../../../../../shared/components/feature-nav-heading/feature-nav-heading.component';
import { JobStatusComponent } from '../../../../../shared/components/job-status/job-status.component';
import { IJob } from '../../../../../shared/interfaces';
import * as JobActions from '../../../../../shared/store/job/job.actions';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { setNavbarItemsAction } from '../../../../../shared/store/navbar';

@Component({
  selector: 'job-view-infos',
  standalone: true,
  imports: [CommonModule, FeatureNavHeadingComponent, JobStatusComponent],
  templateUrl: './infos.component.html',
  styleUrls: ['./infos.component.scss'],
})
export class InfosComponent {
  public job: IJob | null = null;

  constructor(private _store: Store) {
    this._store
      .select(JobSelectors.selectJob)
      .pipe(takeUntilDestroyed())
      .subscribe((job) => {
        this.job = job;

        this._store.dispatch(
          setNavbarItemsAction({
            items: [{ label: `${job?.mp.number}` }, { label: 'Info' }],
          }),
        );
      });
  }

  public onReSyncJob(): void {
    if (!this.job) {
      return;
    }

    this._store.dispatch(
      JobActions.trySyncJobAction({ mpNumber: this.job.mp.number }),
    );
  }
}
