import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IDeliveryAddress } from '../../../../../../../shared/interfaces';
import { DeliveryAddressItemComponent } from './delivery-address-item/delivery-address-item.component';

@Component({
  selector: 'job-delivery-address',
  standalone: true,
  imports: [CommonModule, DeliveryAddressItemComponent],
  templateUrl: './delivery-address.component.html',
  styleUrls: ['./delivery-address.component.scss'],
})
export class DeliveryAddressComponent {
  @Input({ required: true }) address!: IDeliveryAddress;

  public copyCheck = false;

  public get displayAddress(): boolean {
    return (
      !!this.address.mp.address ||
      !!this.address.mp.company ||
      !!this.address.mp.zipCode ||
      !!this.address.mp.city ||
      !!this.country
    );
  }

  public get displayContact(): boolean {
    return (
      !!this.address.mp.contactName ||
      !!this.address.mp.email ||
      !!this.address.mp.phone
    );
  }

  public get city(): string | null {
    if (!this.address.mp.city && !this.address.mp.zipCode) {
      return null;
    }

    return `${this.address.mp.zipCode || ''} ${this.address.mp.city || ''}`;
  }

  public get country(): string | null {
    if (this.address.mp.company.toLowerCase().match('a dispo')) {
      return null;
    }

    if (this.address.mp.country === 'FRANCE') {
      return null;
    }

    if (!this.address.mp.country && !this.address.mp.countryCode) {
      return null;
    }

    return `${this.address.mp.country || ''}${
      this.address.mp.countryCode ? ` [${this.address.mp.countryCode}]` : ''
    }`;
  }

  public async copyAddress(): Promise<void> {
    try {
      if (!this.displayAddress) {
        return;
      }

      let text = '';

      if (this.address.mp.company) {
        text = this.address.mp.company;
      }

      if (this.address.mp.address) {
        text = `${text}
${this.address.mp.address}`;
      }

      if (this.city) {
        text = `${text}
${this.city}`;
      }

      if (this.country) {
        text = `${text}
${this.country}`;
      }

      if (this.address.mp.contactName) {
        text = `${text}
A l'att. de ${this.address.mp.contactName}`;
      }

      if (this.address.mp.phone) {
        text = `${text}
${this.address.mp.phone}`;
      }

      await navigator.clipboard.writeText(text);

      this.copyCheck = true;
      setTimeout(() => {
        this.copyCheck = false;
      }, 1000);
    } catch (error) {
      console.log(error);
    }
  }
}
