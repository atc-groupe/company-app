import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IDeliveryAddressItem } from '../../../../../../../../shared/interfaces';

@Component({
  selector: 'job-delivery-address-item',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './delivery-address-item.component.html',
  styleUrls: ['./delivery-address-item.component.scss'],
})
export class DeliveryAddressItemComponent {
  @Input({ required: true }) item!: IDeliveryAddressItem;

  public expanded = false;

  public toggleExpanded(): void {
    this.expanded = !this.expanded;
  }
}
