import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IJob } from '../../../../../shared/interfaces';
import { Store } from '@ngrx/store';
import * as JobSelectors from '../../../../../shared/store/job/job.selectors';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { setNavbarItemsAction } from '../../../../../shared/store/navbar';
import { DeliveryAddressComponent } from './components/delivery-address/delivery-address.component';

@Component({
  selector: 'app-delivery-adresses',
  standalone: true,
  imports: [CommonModule, DeliveryAddressComponent],
  templateUrl: './delivery-adresses.component.html',
  styleUrls: ['./delivery-adresses.component.scss'],
})
export class DeliveryAdressesComponent {
  public job: IJob | null = null;

  constructor(private _store: Store) {
    this._store
      .select(JobSelectors.selectJob)
      .pipe(takeUntilDestroyed())
      .subscribe((job) => {
        this.job = job;

        if (job) {
          this._store.dispatch(
            setNavbarItemsAction({
              items: [
                { label: `${job?.mp.number}` },
                { label: 'Adresses de livraison' },
              ],
            }),
          );
        }
      });
  }
}
