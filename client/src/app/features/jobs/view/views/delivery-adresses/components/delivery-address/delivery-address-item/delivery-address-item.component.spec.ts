import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryAddressItemComponent } from './delivery-address-item.component';

describe('DeliveryAddressItemComponent', () => {
  let component: DeliveryAddressItemComponent;
  let fixture: ComponentFixture<DeliveryAddressItemComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [DeliveryAddressItemComponent]
    });
    fixture = TestBed.createComponent(DeliveryAddressItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
