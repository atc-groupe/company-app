import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryAdressesComponent } from './delivery-adresses.component';

describe('DeliveryAdressesComponent', () => {
  let component: DeliveryAdressesComponent;
  let fixture: ComponentFixture<DeliveryAdressesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [DeliveryAdressesComponent]
    });
    fixture = TestBed.createComponent(DeliveryAdressesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
