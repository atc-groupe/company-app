import { Component, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Store } from '@ngrx/store';
import * as JobSelectors from '../../../../../shared/store/job/job.selectors';
import { debounceTime, Subscription, tap } from 'rxjs';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { TextEditorComponent } from '../../../../../shared/components/text-editor/text-editor.component';
import {
  JobPrePressService,
  NotificationsService,
} from '../../../../../shared/services';
import { SpinnerComponent } from '../../../../../shared/components/spinner/spinner.component';
import {
  AuthActionsJobsEnum,
  AuthSubjectsEnum,
} from '../../../../../shared/enums';
import { ToggleBtnComponent } from '../../../../../shared/components/toggle-btn/toggle-btn.component';
import { IJob } from '../../../../../shared/interfaces';
import { selectUser } from '../../../../../shared/store/user/user.selectors';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { setNavbarItemsAction } from '../../../../../shared/store/navbar';

@Component({
  selector: 'app-files-check',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    TextEditorComponent,
    SpinnerComponent,
    ToggleBtnComponent,
  ],
  templateUrl: './files-check.component.html',
  styleUrls: ['./files-check.component.scss'],
})
export class FilesCheckComponent implements OnInit, OnDestroy {
  public job: IJob | null = null;
  public filesCheckIsLoading = false;
  public filesCheckError: string | null = null;
  public filesCheckStatusIndex = 0;
  public canUpdateFilesCheck = false;
  public form = this._fb.group({
    filesCheck: [''],
  });
  private _subscription = new Subscription();

  constructor(
    private _store: Store,
    private _fb: FormBuilder,
    private _prePressService: JobPrePressService,
    private _notificationService: NotificationsService,
  ) {
    this._store
      .select(selectUser)
      .pipe(takeUntilDestroyed())
      .subscribe((user) => {
        if (!user) {
          return;
        }

        this.canUpdateFilesCheck = user.canActivate({
          subject: AuthSubjectsEnum.Jobs,
          action: AuthActionsJobsEnum.UpdateFilesCheckInfos,
        });

        if (this.canUpdateFilesCheck) {
          this.form.controls.filesCheck.enable();
        } else {
          this.form.controls.filesCheck.disable();
        }
      });
  }

  ngOnInit() {
    this._subscription.add(
      this._store.select(JobSelectors.selectJob).subscribe((job) => {
        this.job = job;

        if (!job) {
          return;
        }

        this._store.dispatch(
          setNavbarItemsAction({
            items: [
              { label: `${job?.mp.number}` },
              { label: 'Vérif fichiers' },
            ],
          }),
        );

        this.filesCheckStatusIndex = job.prePress.filesCheckStatusIndex;

        if (job.prePress.filesCheck && !this.filesCheckIsLoading) {
          this.form.controls.filesCheck.setValue(
            JSON.parse(job.prePress.filesCheck),
            {
              emitEvent: false,
            },
          );
        }
      }),
    );

    this._subscription.add(
      this.form.controls.filesCheck.valueChanges
        .pipe(
          tap(() => {
            this.filesCheckError = null;
            this.filesCheckIsLoading = true;
          }),
          debounceTime(2000),
        )
        .subscribe((value) => {
          if (!this.job) {
            return;
          }

          this._prePressService
            .updateFilesCheck(this.job._id, value)
            .subscribe({
              next: () => {
                this.filesCheckError = null;
              },
              error: (err) => {
                this.filesCheckError = err.error.message;
              },
              complete: () => {
                this.filesCheckIsLoading = false;
              },
            });
        }),
    );
  }

  public toggleFilesCheckStatus(index: number): void {
    if (!this.job) {
      return;
    }

    this.filesCheckStatusIndex =
      this.job.prePress.filesCheckStatusIndex === index ? 1 : index;

    this._prePressService
      .updateFilesCheckStatusIndex(this.job._id, this.filesCheckStatusIndex)
      .subscribe({
        next: () =>
          this._notificationService.displaySuccess(
            'Le statut de la vérif. est modifié',
          ),
        error: () =>
          this._notificationService.displayError(
            'Une erreur est survenue lors de la mise à jour du statut',
          ),
      });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
