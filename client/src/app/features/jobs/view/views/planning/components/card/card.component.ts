import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  IJobPlanningCard,
  IJobWorksheet,
} from '../../../../../../../shared/interfaces';
import { MpListItemPipe } from '../../../../../../../shared/pipes/mp-list-item.pipe';
import { MpTimePipe } from '../../../../../../../shared/pipes/mp-time.pipe';
import { WorksheetComponent } from '../worksheet/worksheet.component';
import { User } from '../../../../../../../shared/entities';
import { TJobPlanningWorksheetsExpanded } from '../../../../../../../shared/types';

@Component({
  selector: 'job-planning-card',
  standalone: true,
  imports: [CommonModule, MpListItemPipe, MpTimePipe, WorksheetComponent],
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent {
  @Input({ required: true }) card!: IJobPlanningCard;
  @Input({ required: true }) user!: User;
  @Input({ required: true }) worksheets: IJobWorksheet[] | null = null;
  @Input() canAddWorksheet = false;
  @Input() canStopWorksheet = false;
  @Input() worksheetsExpanded: TJobPlanningWorksheetsExpanded = {};

  @Output() toggleWorksheet = new EventEmitter<string>();
  @Output() addWorksheet = new EventEmitter<void>();
  @Output() stopWorksheet = new EventEmitter<void>();

  public get isProcessing(): boolean {
    if (!this.worksheets) {
      return false;
    }

    return this.worksheets.some((ws) => !ws.totalTime);
  }

  public get isPaused(): boolean {
    if (!this.worksheets) {
      return false;
    }

    return (
      !this.isProcessing && !this.card.completed && this.worksheets.length > 0
    );
  }

  public get cardUser(): string | null {
    if (this.card.operationUserInfo) {
      return this.card.operationUserInfo.employee;
    }

    if (this.card.employee) {
      return this.card.employee;
    }

    return null;
  }

  public onToggleWorksheet(id: string): void {
    this.toggleWorksheet.emit(id);
  }

  public onAddWorksheet(): void {
    this.addWorksheet.emit();
  }

  public onStopWorksheet(): void {
    this.stopWorksheet.emit();
  }
}
