import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  IJob,
  IJobPlanningGroupCards,
  IJobWorksheet,
} from '../../../../../shared/interfaces';
import { User } from '../../../../../shared/entities';
import * as JobSelectors from '../../../../../shared/store/job/job.selectors';
import * as JobActions from '../../../../../shared/store/job/job.actions';
import { Store } from '@ngrx/store';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { JobWorksheetEventService } from '../../services';
import {
  ModalService,
  UserOperationsService,
} from '../../../../../shared/services';
import { selectUser } from '../../../../../shared/store/user/user.selectors';
import { EditWorksheetComponent } from '../../components/edit-worksheet/edit-worksheet.component';
import { RemoveWorksheetComponent } from '../../components/remove-worksheet/remove-worksheet.component';
import { CardComponent } from './components/card/card.component';
import { TJobPlanningWorksheetsExpanded } from '../../../../../shared/types';
import { MpListItemPipe } from '../../../../../shared/pipes/mp-list-item.pipe';
import { StartWorksheetComponent } from '../../components/start-worksheet/start-worksheet.component';
import { ToggleBtnComponent } from '../../../../../shared/components/toggle-btn/toggle-btn.component';
import { setNavbarItemsAction } from '../../../../../shared/store/navbar';

@Component({
  selector: 'app-planning',
  standalone: true,
  imports: [CommonModule, CardComponent, MpListItemPipe, ToggleBtnComponent],
  templateUrl: './planning.component.html',
  styleUrls: ['./planning.component.scss'],
})
export class PlanningComponent {
  public job: IJob | null = null;
  public user: User | null = null;
  public worksheetsExpanded: TJobPlanningWorksheetsExpanded = {};
  public planningData: IJobPlanningGroupCards[] | null = null;
  public filterOnMyOperations = false;

  constructor(
    private _store: Store,
    private _worksheetEventService: JobWorksheetEventService,
    private _modalService: ModalService,
    private _userOperationsService: UserOperationsService,
  ) {
    this._store
      .select(JobSelectors.selectJob)
      .pipe(takeUntilDestroyed())
      .subscribe((job) => {
        this.job = job;

        if (job) {
          this._store.dispatch(
            setNavbarItemsAction({
              items: [{ label: `${job?.mp.number}` }, { label: 'Planning' }],
            }),
          );
        }
      });

    this._store
      .select(selectUser)
      .pipe(takeUntilDestroyed())
      .subscribe((user) => (this.user = user));

    this._store
      .select(JobSelectors.selectPlanningData)
      .pipe(takeUntilDestroyed())
      .subscribe((planningData) => (this.planningData = planningData));

    this._store
      .select(JobSelectors.selectPlanningWorksheetsExpanded)
      .pipe(takeUntilDestroyed())
      .subscribe(
        (worksheetsExpanded) => (this.worksheetsExpanded = worksheetsExpanded),
      );

    this._store
      .select(JobSelectors.selectMyPlanningCardsFilter)
      .pipe(takeUntilDestroyed())
      .subscribe(
        (filterOnMyOperations) =>
          (this.filterOnMyOperations = filterOnMyOperations),
      );

    this._worksheetEventService.onStop$
      .pipe(takeUntilDestroyed())
      .subscribe((worksheet) => {
        if (worksheet && this.job && this.user) {
          this._modalService.open(EditWorksheetComponent, {
            options: { yPos: 'center' },
            inputs: {
              job: this.job,
              worksheet,
              user: this.user,
              action: 'stop',
            },
          });
        }
      });

    this._worksheetEventService.onUpdate$
      .pipe(takeUntilDestroyed())
      .subscribe((worksheet) => {
        if (worksheet && this.job && this.user) {
          this._modalService.open(EditWorksheetComponent, {
            inputs: {
              job: this.job,
              worksheet,
              user: this.user,
              action: 'update',
            },
            options: { yPos: 'center' },
          });
        }
      });

    this._worksheetEventService.onDelete$
      .pipe(takeUntilDestroyed())
      .subscribe((worksheet) => {
        if (worksheet && this.job && this.user) {
          this._modalService.open(RemoveWorksheetComponent, {
            inputs: { worksheet, job: this.job, user: this.user },
            options: { yPos: 'center' },
          });
        }
      });
  }

  public get isNotPlanned(): boolean {
    return this.job
      ? this.job.planningData.every((el) => !el.isPlanned)
      : false;
  }

  public getCardWorksheets(operation: string): IJobWorksheet[] | null {
    if (!this.job?.worksheets) {
      return null;
    }

    return this.job.worksheets.filter((el) => el.operation === operation);
  }

  public toggleFilterOnMyOperations(): void {
    this._store.dispatch(JobActions.toggleMyPlanningCardsFilterAction());
  }

  public onToggleWorksheet(id: string): void {
    this._store.dispatch(JobActions.togglePlanningWorksheetAction({ id }));
  }

  public canAddWorksheet(operation: string): boolean {
    if (!this.job || !this.job.worksheets || !this.user) {
      return false;
    }

    return this._userOperationsService.canAddWorksheet(
      operation,
      this.job.worksheets,
      this.user,
    );
  }

  public onAddWorksheet(operation: string): void {
    if (!this.user || !this.job || !this.canAddWorksheet(operation)) {
      return;
    }

    this._modalService.open(StartWorksheetComponent, {
      inputs: {
        job: this.job,
        user: this.user,
        operation,
      },
      options: { fullScreen: true },
    });
  }

  public canStopWorksheet(operation: string): boolean {
    if (!this.user || !this.job || !this.job.worksheets) {
      return false;
    }

    const activeWorksheet = this._userOperationsService.getActiveWorksheet(
      operation,
      this.job.worksheets,
    );

    if (!activeWorksheet) {
      return false;
    }

    return this._userOperationsService.canUpdateWorksheet(
      this.user,
      activeWorksheet,
      this.job.worksheets,
    );
  }

  public onStopWorksheet(operation: string): void {
    if (
      !this.user ||
      !this.job ||
      !this.job.worksheets ||
      !this.canStopWorksheet(operation)
    ) {
      return;
    }

    const activeWorksheet = this._userOperationsService.getActiveWorksheet(
      operation,
      this.job.worksheets,
    );

    if (!activeWorksheet) {
      return;
    }

    this._modalService.open(EditWorksheetComponent, {
      inputs: {
        job: this.job,
        worksheet: activeWorksheet,
        user: this.user,
        action: 'stop',
      },
      options: { fullScreen: true },
    });
  }
}
