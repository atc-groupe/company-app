import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IJobWorksheet } from '../../../../../../../shared/interfaces';
import { MpTimePipe } from '../../../../../../../shared/pipes/mp-time.pipe';
import { MpEmployeePipe } from '../../../../../../../shared/pipes/mp-employee.pipe';
import { User } from '../../../../../../../shared/entities';
import { UserOperationsService } from '../../../../../../../shared/services';
import { JobWorksheetEventService } from '../../../../services';

@Component({
  selector: 'job-worksheet',
  standalone: true,
  imports: [CommonModule, MpTimePipe, MpEmployeePipe],
  templateUrl: './worksheet.component.html',
  styleUrls: ['./worksheet.component.scss'],
})
export class WorksheetComponent {
  @Input() worksheet: IJobWorksheet | null = null;
  @Input() worksheets: IJobWorksheet[] | null = null;
  @Input() user: User | null = null;
  @Input() expanded = false;

  @Output() toggle = new EventEmitter<void>();

  constructor(
    private _userOperationsService: UserOperationsService,
    private _worksheetEvent: JobWorksheetEventService,
  ) {}

  public onToggle(): void {
    this.toggle.emit();
  }

  public get isRecording(): boolean {
    if (!this.worksheet) {
      return false;
    }

    return !!this.worksheet.startTime && !this.worksheet.totalTime;
  }

  public get startTime(): Date | null {
    if (!this.worksheet || !this.worksheet.startTime) {
      return null;
    }

    const date = new Date(this.worksheet.startTime);
    date.setHours(date.getHours() - 1);

    return date;
  }

  public get canUpdate(): boolean {
    if (!this.user || !this.worksheet || !this.worksheets) {
      return false;
    }

    const canUpdate = this._userOperationsService.canUpdateWorksheet(
      this.user,
      this.worksheet,
      this.worksheets,
    );

    const isActive = this._userOperationsService.isActiveWorksheet(
      this.worksheet,
    );

    return canUpdate && !isActive;
  }

  public get canRemove(): boolean {
    if (!this.user || !this.worksheet || !this.worksheets) {
      return false;
    }

    return this._userOperationsService.canRemoveWorksheet(
      this.user,
      this.worksheet,
      this.worksheets,
    );
  }

  public onDelete(): void {
    if (!this.worksheet) {
      return;
    }

    this._worksheetEvent.delete(this.worksheet);
  }

  public onUpdate(): void {
    if (!this.worksheet) {
      return;
    }

    this._worksheetEvent.update(this.worksheet);
  }
}
