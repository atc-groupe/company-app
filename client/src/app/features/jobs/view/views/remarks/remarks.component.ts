import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import * as JobSelectors from '../../../../../shared/store/job/job.selectors';
import { Store } from '@ngrx/store';
import { IJob } from '../../../../../shared/interfaces';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { setNavbarItemsAction } from '../../../../../shared/store/navbar';

@Component({
  selector: 'app-remarks',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './remarks.component.html',
  styleUrls: ['./remarks.component.scss'],
})
export class RemarksComponent {
  public job: IJob | null = null;

  constructor(private _store: Store) {
    this._store
      .select(JobSelectors.selectJob)
      .pipe(takeUntilDestroyed())
      .subscribe((job) => {
        this.job = job;

        if (job) {
          this._store.dispatch(
            setNavbarItemsAction({
              items: [{ label: `${job?.mp.number}` }, { label: 'Remarques' }],
            }),
          );
        }
      });
  }

  public get displayInfos(): boolean {
    if (!this.job) {
      return false;
    }

    const info = this.job.mp.info;

    return (
      !!info.general ||
      !!info.prePress ||
      !!info.digital ||
      !!info.finishing ||
      !!info.subcontract ||
      !!info.expeditions ||
      !!info.application
    );
  }
}
