import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IJob } from '../../../../../shared/interfaces';
import { debounceTime, tap } from 'rxjs';
import { Store } from '@ngrx/store';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { JobPrePressService } from '../../../../../shared/services';
import { selectUser } from '../../../../../shared/store/user/user.selectors';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import {
  AuthActionsJobsEnum,
  AuthSubjectsEnum,
} from '../../../../../shared/enums';
import { SpinnerComponent } from '../../../../../shared/components/spinner/spinner.component';
import { TextEditorComponent } from '../../../../../shared/components/text-editor/text-editor.component';
import * as JobSelectors from '../../../../../shared/store/job/job.selectors';
import { setNavbarItemsAction } from '../../../../../shared/store/navbar';

@Component({
  selector: 'app-prepress-note',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SpinnerComponent,
    TextEditorComponent,
  ],
  templateUrl: './prepress-note.component.html',
  styleUrls: ['./prepress-note.component.scss'],
})
export class PrepressNoteComponent {
  public job: IJob | null = null;
  public noteIsLoading = false;
  public noteError: string | null = null;
  public form = this._fb.group({
    note: [''],
  });

  constructor(
    private _store: Store,
    private _fb: FormBuilder,
    private _prePressService: JobPrePressService,
  ) {
    this._store
      .select(selectUser)
      .pipe(takeUntilDestroyed())
      .subscribe((user) => {
        if (!user) {
          return;
        }

        if (
          user.canActivate({
            subject: AuthSubjectsEnum.Jobs,
            action: AuthActionsJobsEnum.UpdatePrePressNote,
          })
        ) {
          this.form.controls.note.enable();
        } else {
          this.form.controls.note.disable();
        }
      });

    this._store
      .select(JobSelectors.selectJob)
      .pipe(takeUntilDestroyed())
      .subscribe((job) => {
        this.job = job;

        if (!job) {
          return;
        }

        this._store.dispatch(
          setNavbarItemsAction({
            items: [{ label: `${job?.mp.number}` }, { label: 'PAO' }],
          }),
        );

        if (job.prePress.note && !this.noteIsLoading) {
          this.form.controls.note.setValue(JSON.parse(job.prePress.note), {
            emitEvent: false,
          });
        }
      });

    this.form.controls.note.valueChanges
      .pipe(
        takeUntilDestroyed(),
        tap(() => {
          this.noteError = null;
          this.noteIsLoading = true;
        }),
        debounceTime(2000),
      )
      .subscribe((value) => {
        if (!this.job) {
          return;
        }

        this._prePressService.updateNote(this.job._id, value).subscribe({
          next: () => {
            this.noteError = null;
          },
          error: (err) => {
            this.noteError = err.error.message;
          },
          complete: () => {
            this.noteIsLoading = false;
          },
        });
      });
  }
}
