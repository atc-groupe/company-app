import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrepressNoteComponent } from './prepress-note.component';

describe('PrepressNoteComponent', () => {
  let component: PrepressNoteComponent;
  let fixture: ComponentFixture<PrepressNoteComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [PrepressNoteComponent]
    });
    fixture = TestBed.createComponent(PrepressNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
