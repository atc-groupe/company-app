import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubJobsComponent } from './sub-jobs.component';

describe('SubJobsComponent', () => {
  let component: SubJobsComponent;
  let fixture: ComponentFixture<SubJobsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SubJobsComponent]
    });
    fixture = TestBed.createComponent(SubJobsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
