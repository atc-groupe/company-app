import { Component, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import * as JobSelectors from '../../../../../shared/store/job/job.selectors';
import * as JobActions from '../../../../../shared/store/job/job.actions';
import { SubJobComponent } from './components/sub-job/sub-job.component';
import { HeaderComponent } from './components/header/header.component';
import { IJob } from '../../../../../shared/interfaces';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

@Component({
  selector: 'app-sub-jobs',
  standalone: true,
  imports: [CommonModule, SubJobComponent, HeaderComponent],
  templateUrl: './sub-jobs.component.html',
  styleUrls: ['./sub-jobs.component.scss'],
})
export class SubJobsComponent {
  public job: IJob | null = null;
  public subJobsState$ = this._store.select(JobSelectors.selectSubJobsState);
  public subJobsFilteredList$ = this._store.select(
    JobSelectors.selectSubJobsFilteredList,
  );

  constructor(private _store: Store) {
    this._store
      .select(JobSelectors.selectJob)
      .pipe(takeUntilDestroyed())
      .subscribe((job) => {
        this.job = job;
      });
  }

  public onSearchSubJobs({ term }: { term: string | null }): void {
    this._store.dispatch(JobActions.setSubJobsSearchAction({ term }));
  }

  public onToggleInvoiceSubJobs(): void {
    this._store.dispatch(JobActions.toggleHideInvoiceSubJobs());
  }

  public onToggleNoProdSubJobs(): void {
    this._store.dispatch(JobActions.toggleHideNoProdSubJobs());
  }

  public onToggleDevicesFilter(device: string): void {
    this._store.dispatch(
      JobActions.toggleSubJobsDeviceFilterAction({ device }),
    );
  }
}
