import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchComponent } from '../../../../../../../shared/components/search/search.component';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { debounceTime, Subscription } from 'rxjs';
import { SubJobsState } from '../../../../../../../shared/store/job/job.reducer';
import { IJob } from '../../../../../../../shared/interfaces';

@Component({
  selector: 'sub-jobs-header',
  standalone: true,
  imports: [CommonModule, SearchComponent, ReactiveFormsModule],
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  @Input({ required: true }) job!: IJob;
  @Input() subJobsState: SubJobsState | null = null;

  @Output() search = new EventEmitter<{ term: string | null }>();
  @Output() toggleInvoiceSubJobs = new EventEmitter<void>();
  @Output() toggleNoProdSubJobs = new EventEmitter<void>();
  @Output() toggleDeviceSubJobs = new EventEmitter<string>();

  public devices: { name: string; active: boolean }[] = [];
  public form = this._fb.group({
    search: [''],
  });
  private _subscription = new Subscription();

  constructor(private _fb: FormBuilder) {}

  ngOnInit(): void {
    this._subscription.add(
      this.form
        .get('search')
        ?.valueChanges.pipe(debounceTime(300))
        .subscribe((term) => {
          this.search.emit({ term });
        }),
    );

    if (!this.job) {
      return;
    }

    if (this.subJobsState) {
      this.form.get('search')?.setValue(this.subJobsState.search);
    }
  }

  public get hasInvoiceSubJob(): boolean {
    if (!this.job.subJobs) {
      return false;
    }

    return this.job.subJobs.some((item) =>
      item.mp.description.match(/^(dev|dv).*/i),
    );
  }

  public get hasSubJobsWithoutProduction(): boolean {
    if (!this.job.subJobs) {
      return false;
    }

    return this.job.subJobs.some(
      (item) =>
        item.prodLayers === null && item.unlinkedFinishingLayers === null,
    );
  }

  public onToggleInvoiceSubJobs(): void {
    this.toggleInvoiceSubJobs.emit();
  }

  public onToggleNoProdSubJobs(): void {
    this.toggleNoProdSubJobs.emit();
  }

  public onToggleDeviceSubJobs(device: string): void {
    this.toggleDeviceSubJobs.emit(device);
  }

  ngOnDestroy(): void {
    this._subscription.unsubscribe();
  }
}
