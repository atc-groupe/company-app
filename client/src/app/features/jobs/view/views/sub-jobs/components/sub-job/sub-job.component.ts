import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ISubJob } from '../../../../../../../shared/interfaces';
import { CommentComponent } from './comment/comment.component';
import { CheckListComponent } from './check-list/check-list.component';
import { ProdComponent } from './prod/prod.component';
import { HeadingComponent } from './heading/heading.component';
import { SubContractComponent } from './sub-contract/sub-contract.component';

@Component({
  selector: 'sub-job',
  standalone: true,
  imports: [
    CommonModule,
    CommentComponent,
    CheckListComponent,
    ProdComponent,
    HeadingComponent,
    SubContractComponent,
  ],
  templateUrl: './sub-job.component.html',
  styleUrls: ['./sub-job.component.scss'],
})
export class SubJobComponent {
  @Input() subJob: ISubJob | null = null;
}
