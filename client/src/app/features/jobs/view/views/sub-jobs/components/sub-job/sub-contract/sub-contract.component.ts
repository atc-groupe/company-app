import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ISubJobSubcontractor } from '../../../../../../../../shared/interfaces';

@Component({
  selector: 'sub-job-sub-contract',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './sub-contract.component.html',
  styleUrls: ['./sub-contract.component.scss'],
})
export class SubContractComponent {
  @Input() subcontractors: ISubJobSubcontractor[] | null = null;
}
