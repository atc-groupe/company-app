import { Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  ModalService,
  StockService,
} from '../../../../../../../../shared/services';
import { ClickOutsideDirective } from '../../../../../../../../shared/directives/click-outside.directive';
import { Paper } from '../../../../../../../../shared/entities';
import { MessageComponent } from '../../../../../../../../shared/components/message/message.component';

@Component({
  selector: 'app-material-view',
  standalone: true,
  imports: [CommonModule, ClickOutsideDirective, MessageComponent],
  templateUrl: './material-view.component.html',
  styleUrls: ['./material-view.component.scss'],
})
export class MaterialViewComponent implements OnInit {
  @Input() id!: number;

  public papers: Paper[] | null = null;
  public error: string | null = null;

  constructor(
    private _stockService: StockService,
    private _modalService: ModalService,
  ) {}

  ngOnInit() {
    this._stockService.getPapersByName(this.id).subscribe({
      next: (papers) => {
        if (!papers.length) {
          this.error = `Aucune matière correspondant à ce nom n'a été trouvée dans la basse`;
        }

        this.papers = papers;
      },
      error: () => {
        this.error =
          "Impossible de récupérer la liste des articles depuis l'API";
      },
    });
  }

  get paperName(): string | null {
    return this.papers && this.papers.length ? this.papers[0].name : null;
  }

  public onClose(): void {
    this._modalService.close();
  }
}
