import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ISubJob } from '../../../../../../../../shared/interfaces';

@Component({
  selector: 'sub-job-check-list',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './check-list.component.html',
  styleUrls: ['./check-list.component.scss'],
})
export class CheckListComponent {
  @Input() subJob: ISubJob | null = null;
  public copyCheck = false;

  public async copyChecklist(): Promise<void> {
    try {
      if (!this.subJob) {
        return;
      }
      const text = `${this.subJob.reference}${this._getFormattedData(
        this.subJob.mp.checklist.format,
      )}
${this.subJob.mp.quantity}EX${this._getFormattedData(
        this.subJob.mp.checklist.description,
      )}${this._getFormattedData(
        this.subJob.mp.checklist.print,
      )}${this._getFormattedData(
        this.subJob.mp.checklist.material,
      )}${this._getFormattedData(
        this.subJob.mp.checklist.finishing,
      )}${this._getFormattedData(
        this.subJob.mp.checklist.packaging,
      )}${this._getFormattedData(
        this.subJob.mp.checklist.delivery,
      )}${this._getFormattedData(
        this.subJob.mp.checklist.application,
      )}${this._getFormattedData(this.subJob.mp.checklist.remark)}`;

      await navigator.clipboard.writeText(text);

      this.copyCheck = true;
      setTimeout(() => {
        this.copyCheck = false;
      }, 1000);
    } catch {}
  }

  private _getFormattedData(text: string | number | null): string {
    if (!text) {
      return '';
    }

    return `
${text}`;
  }
}
