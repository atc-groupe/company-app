import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ISubJob } from '../../../../../../../../shared/interfaces';
import { ModalService } from '../../../../../../../../shared/services';
import { MaterialViewComponent } from '../material-view/material-view.component';

@Component({
  selector: 'sub-job-prod',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './prod.component.html',
  styleUrls: ['./prod.component.scss'],
})
export class ProdComponent {
  @Input() subJob: ISubJob | null = null;
  @Output() viewMaterial = new EventEmitter<number>();

  constructor(private _modalService: ModalService) {}

  public hasProductionData(): boolean {
    if (!this.subJob) {
      return false;
    }

    return (
      this.subJob.prodLayers !== null ||
      this.subJob.unlinkedFinishingLayers !== null
    );
  }

  public onViewPaper(id: number): void {
    this._modalService.open(MaterialViewComponent, {
      options: { yPos: 'center' },
      inputs: { id },
    });
  }
}
