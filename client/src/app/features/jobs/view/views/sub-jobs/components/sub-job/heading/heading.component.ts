import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ISubJob } from '../../../../../../../../shared/interfaces';

@Component({
  selector: 'sub-job-heading',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './heading.component.html',
  styleUrls: ['./heading.component.scss'],
})
export class HeadingComponent {
  @Input() subJob: ISubJob | null = null;
}
