import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubJobComponent } from './sub-job.component';

describe('SubJobComponent', () => {
  let component: SubJobComponent;
  let fixture: ComponentFixture<SubJobComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SubJobComponent]
    });
    fixture = TestBed.createComponent(SubJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
