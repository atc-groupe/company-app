import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderSubJobComponent } from './order-sub-job.component';

describe('OrderSubJobComponent', () => {
  let component: OrderSubJobComponent;
  let fixture: ComponentFixture<OrderSubJobComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [OrderSubJobComponent]
    });
    fixture = TestBed.createComponent(OrderSubJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
