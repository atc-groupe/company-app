import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IJobSubcontractorOrderSubJob } from '../../../../../../../../shared/interfaces';

@Component({
  selector: 'subcontractor-order-sub-job',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './order-sub-job.component.html',
  styleUrls: ['./order-sub-job.component.scss'],
})
export class OrderSubJobComponent {
  @Input() subJobData: IJobSubcontractorOrderSubJob | null = null;
  @Input() expanded = true;

  public toggleExpanded(): void {
    this.expanded = !this.expanded;
  }
}
