import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Store } from '@ngrx/store';
import * as JobSelectors from '../../../../../shared/store/job/job.selectors';
import { OrderComponent } from './components/order/order.component';
import { IJob } from '../../../../../shared/interfaces';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { setNavbarItemsAction } from '../../../../../shared/store/navbar';

@Component({
  selector: 'app-subcontractors',
  standalone: true,
  imports: [CommonModule, OrderComponent],
  templateUrl: './subcontractors.component.html',
  styleUrls: ['./subcontractors.component.scss'],
})
export class SubcontractorsComponent {
  public job: IJob | null = null;

  constructor(private _store: Store) {
    this._store
      .select(JobSelectors.selectJob)
      .pipe(takeUntilDestroyed())
      .subscribe((job) => {
        this.job = job;

        if (job) {
          this._store.dispatch(
            setNavbarItemsAction({
              items: [
                { label: `${job?.mp.number}` },
                { label: 'Sous-traitance' },
              ],
            }),
          );
        }
      });
  }
}
