import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IJobSubcontractorOrder } from '../../../../../../../shared/interfaces';
import { OrderInfoComponent } from './order-info/order-info.component';
import { OrderSubJobComponent } from './order-sub-job/order-sub-job.component';

@Component({
  selector: 'job-subcontractor-order',
  standalone: true,
  imports: [CommonModule, OrderInfoComponent, OrderSubJobComponent],
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss'],
})
export class OrderComponent {
  public expanded = false;

  @Input() relation: string | null = null;
  @Input() index: number | null = null;
  @Input() order: IJobSubcontractorOrder | null = null;

  get title(): string {
    const index = this.index !== null ? `#${this.index + 1}` : '';

    return `${this.relation} ${index}`;
  }

  public toggleSubJobs(): void {
    this.expanded = !this.expanded;
  }
}
