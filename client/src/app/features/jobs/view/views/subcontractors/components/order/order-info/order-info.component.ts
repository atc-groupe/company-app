import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IJobSubcontractorOrder } from '../../../../../../../../shared/interfaces';

@Component({
  selector: 'subcontractor-order-info',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './order-info.component.html',
  styleUrls: ['./order-info.component.scss'],
})
export class OrderInfoComponent {
  @Input() order: IJobSubcontractorOrder | null = null;
}
