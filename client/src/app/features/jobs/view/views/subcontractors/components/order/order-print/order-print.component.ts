import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IJobSubcontractorOrder } from '../../../../../../../../shared/interfaces';
import { ModalService } from '../../../../../../../../shared/services';
import { ClickOutsideDirective } from '../../../../../../../../shared/directives/click-outside.directive';

@Component({
  selector: 'app-order-print',
  standalone: true,
  imports: [CommonModule, ClickOutsideDirective],
  templateUrl: './order-print.component.html',
  styleUrls: ['./order-print.component.scss'],
})
export class OrderPrintComponent {
  @Input() order!: IJobSubcontractorOrder;
  @Input() relation!: string;

  constructor(private _modalService: ModalService) {}

  public hide(): void {
    this._modalService.close();
  }
}
