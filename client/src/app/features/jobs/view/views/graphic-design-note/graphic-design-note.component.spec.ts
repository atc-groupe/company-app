import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphicDesignNoteComponent } from './graphic-design-note.component';

describe('GraphicDesignNoteComponent', () => {
  let component: GraphicDesignNoteComponent;
  let fixture: ComponentFixture<GraphicDesignNoteComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [GraphicDesignNoteComponent]
    });
    fixture = TestBed.createComponent(GraphicDesignNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
