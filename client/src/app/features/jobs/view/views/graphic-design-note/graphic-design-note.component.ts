import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IJob } from '../../../../../shared/interfaces';
import { User } from '../../../../../shared/entities';
import { Store } from '@ngrx/store';
import { JobPrePressService } from '../../../../../shared/services';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import * as JobSelectors from '../../../../../shared/store/job/job.selectors';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { selectUser } from '../../../../../shared/store/user/user.selectors';
import {
  AuthActionsJobsEnum,
  AuthSubjectsEnum,
} from '../../../../../shared/enums';
import { debounceTime, tap } from 'rxjs';
import { SpinnerComponent } from '../../../../../shared/components/spinner/spinner.component';
import { TextEditorComponent } from '../../../../../shared/components/text-editor/text-editor.component';
import { setNavbarItemsAction } from '../../../../../shared/store/navbar';

@Component({
  selector: 'job-graphic-design-note',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SpinnerComponent,
    TextEditorComponent,
  ],
  templateUrl: './graphic-design-note.component.html',
  styleUrls: ['./graphic-design-note.component.scss'],
})
export class GraphicDesignNoteComponent {
  public job: IJob | null = null;
  public user: User | null = null;
  public canUpdateNote = false;
  public isLoading = false;
  public error: string | null = null;
  public form = this._fb.group({
    designNote: [''],
  });

  constructor(
    private _store: Store,
    private _prePressService: JobPrePressService,
    private _fb: FormBuilder,
  ) {
    this._store
      .select(JobSelectors.selectJob)
      .pipe(takeUntilDestroyed())
      .subscribe((job) => {
        this.job = job;

        if (!job) {
          return;
        }

        this._store.dispatch(
          setNavbarItemsAction({
            items: [
              { label: `${job?.mp.number}` },
              { label: 'Créa graphique' },
            ],
          }),
        );

        if (job.prePress.graphicDesignNote && !this.isLoading) {
          this.form.controls.designNote.setValue(
            JSON.parse(job.prePress.graphicDesignNote),
            {
              emitEvent: false,
            },
          );
        }
      });

    this._store
      .select(selectUser)
      .pipe(takeUntilDestroyed())
      .subscribe((user) => {
        if (user) {
          this.canUpdateNote = user.canActivate({
            subject: AuthSubjectsEnum.Jobs,
            action: AuthActionsJobsEnum.UpdateGraphicDesignNote,
          });

          if (this.canUpdateNote) {
            this.form.controls.designNote.enable();
          } else {
            this.form.controls.designNote.disable();
          }
        }
      });

    this.form.controls.designNote.valueChanges
      .pipe(
        takeUntilDestroyed(),
        tap(() => {
          this.error = null;
          this.isLoading = true;
        }),
        debounceTime(2000),
      )
      .subscribe((value) => {
        this._prePressService
          .updateGraphicDesignNote(this.job!._id, value)
          .subscribe({
            next: () => {
              this.error = null;
            },
            error: (err) => {
              this.error = err.error.message;
            },
            complete: () => {
              this.isLoading = false;
            },
          });
      });
  }
}
