import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignNoteComponent } from './design-note.component';

describe('DesignNoteComponent', () => {
  let component: DesignNoteComponent;
  let fixture: ComponentFixture<DesignNoteComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [DesignNoteComponent]
    });
    fixture = TestBed.createComponent(DesignNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
