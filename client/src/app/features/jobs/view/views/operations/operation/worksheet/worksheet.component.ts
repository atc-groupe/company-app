import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IJobWorksheet } from '../../../../../../../shared/interfaces';
import { MpTimePipe } from '../../../../../../../shared/pipes/mp-time.pipe';
import { MpEmployeePipe } from '../../../../../../../shared/pipes/mp-employee.pipe';

@Component({
  selector: 'job-operation-worksheet',
  standalone: true,
  imports: [CommonModule, MpTimePipe, MpEmployeePipe],
  templateUrl: './worksheet.component.html',
  styleUrls: ['./worksheet.component.scss'],
})
export class WorksheetComponent {
  @Input() worksheet: IJobWorksheet | null = null;
  @Input() worksheets: IJobWorksheet[] | null = null;

  public expanded = false;

  public toggle(): void {
    this.expanded = !this.expanded;
  }

  public get isRecording(): boolean {
    if (!this.worksheet) {
      return false;
    }

    return !!this.worksheet.startTime && !this.worksheet.totalTime;
  }

  public get startTime(): Date | null {
    if (!this.worksheet || !this.worksheet.startTime) {
      return null;
    }

    const date = new Date(this.worksheet.startTime);
    date.setHours(date.getHours() - 1);

    return date;
  }
}
