import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IJobOperation } from '../../../../../../../shared/interfaces';
import { MpTimePipe } from '../../../../../../../shared/pipes/mp-time.pipe';
import { MpListItemPipe } from '../../../../../../../shared/pipes/mp-list-item.pipe';

@Component({
  selector: 'job-operation-heading',
  standalone: true,
  imports: [CommonModule, MpTimePipe, MpListItemPipe],
  templateUrl: './heading.component.html',
  styleUrls: ['./heading.component.scss'],
})
export class HeadingComponent {
  @Input() operation: IJobOperation | null = null;
}
