import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  IJobOperation,
  IJobWorksheet,
} from '../../../../../../shared/interfaces';
import { HeadingComponent } from './heading/heading.component';
import { MpListItemPipe } from '../../../../../../shared/pipes/mp-list-item.pipe';
import { WorksheetComponent } from './worksheet/worksheet.component';
import { MpTimePipe } from '../../../../../../shared/pipes/mp-time.pipe';

@Component({
  selector: 'job-operation',
  standalone: true,
  imports: [
    CommonModule,
    HeadingComponent,
    MpListItemPipe,
    WorksheetComponent,
    MpTimePipe,
  ],
  templateUrl: './operation.component.html',
  styleUrls: ['./operation.component.scss'],
})
export class OperationComponent {
  @Input() operation: IJobOperation | null = null;
  @Input() worksheets: IJobWorksheet[] | null = null;

  public get operationReady(): boolean {
    if (!this.worksheets) {
      return false;
    }

    return this.worksheets.some((ws) => ws.operationReady);
  }

  public get isProcessing(): boolean {
    if (!this.worksheets) {
      return false;
    }

    return this.worksheets.some((ws) => !ws.totalTime);
  }

  public get isPaused(): boolean {
    if (!this.worksheets) {
      return false;
    }

    return (
      !this.isProcessing && !this.operationReady && this.worksheets.length > 0
    );
  }
}
