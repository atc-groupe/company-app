import { Component, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Store } from '@ngrx/store';
import { OperationComponent } from './operation/operation.component';
import { Subscription } from 'rxjs';
import * as JobSelectors from '../../../../../shared/store/job/job.selectors';
import { IJob, IJobWorksheet } from '../../../../../shared/interfaces';
import { setNavbarItemsAction } from '../../../../../shared/store/navbar';

@Component({
  selector: 'app-operations',
  standalone: true,
  imports: [CommonModule, OperationComponent],
  templateUrl: './operations.component.html',
  styleUrls: ['./operations.component.scss'],
})
export class OperationsComponent implements OnInit, OnDestroy {
  public job: IJob | null = null;

  private _subscription = new Subscription();

  constructor(private _store: Store) {}

  ngOnInit() {
    this._subscription.add(
      this._store.select(JobSelectors.selectJob).subscribe((job) => {
        this.job = job;

        if (job) {
          this._store.dispatch(
            setNavbarItemsAction({
              items: [{ label: `${job?.mp.number}` }, { label: 'Opérations' }],
            }),
          );
        }
      }),
    );
  }

  public getOperationWorksheets(operation: string): IJobWorksheet[] | null {
    return this.job && this.job.worksheets
      ? this.job.worksheets.filter((ws) => ws.operation === operation)
      : null;
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
