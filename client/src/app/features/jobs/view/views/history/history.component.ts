import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import * as JobSelectors from '../../../../../shared/store/job/job.selectors';
import { Store } from '@ngrx/store';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { IJob } from '../../../../../shared/interfaces';
import { setNavbarItemsAction } from '../../../../../shared/store/navbar';

@Component({
  selector: 'job-view-history',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss'],
})
export class HistoryComponent {
  public job: IJob | null = null;

  constructor(private _store: Store) {
    this._store
      .select(JobSelectors.selectJob)
      .pipe(takeUntilDestroyed())
      .subscribe((job) => {
        this.job = job;

        if (job) {
          this._store.dispatch(
            setNavbarItemsAction({
              items: [
                { label: `${job?.mp.number}` },
                { label: 'Historique statuts' },
              ],
            }),
          );
        }
      });
  }
}
