import { Component, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, RouterOutlet } from '@angular/router';
import {
  ModalService,
  UserOperationsService,
  WsJobsService,
} from '../../../shared/services';
import { debounceTime, Subscription } from 'rxjs';
import { HeaderComponent } from './components/header/header.component';
import { NavComponent } from './components/nav/nav.component';
import { Store } from '@ngrx/store';
import * as JobActions from '../../../shared/store/job/job.actions';
import { clearJobAction } from '../../../shared/store/job/job.actions';
import * as JobSelectors from '../../../shared/store/job/job.selectors';
import * as NavSelectors from '../../../shared/store/navbar/navbar.selectors';
import * as NavActions from '../../../shared/store/navbar/navbar.actions';
import { MessageComponent } from './components/message/message.component';
import { User } from '../../../shared/entities';
import { selectUser } from '../../../shared/store/user/user.selectors';
import { IJob, IJobOperation, IJobWorksheet } from '../../../shared/interfaces';
import { WorksheetsComponent } from './components/worksheets/worksheets.component';
import { EditWorksheetComponent } from './components/edit-worksheet/edit-worksheet.component';
import { StartWorksheetComponent } from './components/start-worksheet/start-worksheet.component';
import { StatusComponent } from './components/status/status.component';
import {
  AppModuleEnum,
  AuthActionsJobsEnum,
  AuthSubjectsEnum,
} from '../../../shared/enums';
import { FeatureNavHeadingComponent } from '../../../shared/components/feature-nav-heading/feature-nav-heading.component';

@Component({
  selector: 'job-view',
  standalone: true,
  imports: [
    CommonModule,
    HeaderComponent,
    NavComponent,
    MessageComponent,
    RouterOutlet,
    WorksheetsComponent,
    FeatureNavHeadingComponent,
  ],
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss'],
})
export class ViewComponent implements OnInit, OnDestroy {
  public job: IJob | null = null;
  public error: string | null = null;
  public isLoading = false;
  public user: User | null = null;
  public startWorksheetProcessing = false;
  public canUpdateStatus = false;
  public expanded$ = this._store.select(NavSelectors.selectNavbarExpanded);
  public from$ = this._store.select(JobSelectors.selectFrom);
  private _subscription = new Subscription();

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _wsJobs: WsJobsService,
    private _modalService: ModalService,
    private _userOperationsService: UserOperationsService,
    private _store: Store,
  ) {}
  ngOnInit(): void {
    const mpNumber = this._activatedRoute.snapshot.paramMap.get('mpNumber');

    if (!mpNumber) {
      return;
    }

    this._wsJobs.connect();
    this._subscription.add(
      this._wsJobs
        .onConnect()
        .pipe(debounceTime(100))
        .subscribe(() =>
          this._store.dispatch(
            JobActions.trySubscribeJobAction({ mpNumber: parseInt(mpNumber) }),
          ),
        ),
    );

    this._subscription.add(
      this._store
        .select(JobSelectors.selectJob)
        .subscribe((job) => (this.job = job)),
    );

    this._subscription.add(
      this._store.select(JobSelectors.selectError).subscribe((error) => {
        this.error = error;
      }),
    );

    this._subscription.add(
      this._store
        .select(JobSelectors.selectIsLoading)
        .subscribe((isLoading) => (this.isLoading = isLoading)),
    );

    this._subscription.add(
      this._store.select(selectUser).subscribe((user) => {
        this.user = user;

        if (this.user) {
          this.canUpdateStatus = this.user.canActivate({
            subject: AuthSubjectsEnum.Jobs,
            action: AuthActionsJobsEnum.UpdateStatus,
          });
        }
      }),
    );
  }

  public onToggleNav(): void {
    this._store.dispatch(NavActions.toggleNavbarAction());
  }

  public onHideNav(): void {
    this._store.dispatch(NavActions.hideNavbarAction());
  }

  public onReSyncJob(): void {
    if (!this.job) {
      return;
    }

    this._store.dispatch(
      JobActions.trySyncJobAction({ mpNumber: this.job.mp.number }),
    );
  }

  public onUpdateStatus(): void {
    if (!this.canUpdateStatus || !this.user || !this.job) {
      return;
    }

    this._modalService.open(StatusComponent, {
      inputs: { job: this.job, user: this.user },
      options: { fullScreen: true },
    });
  }

  public get displayJobView(): boolean {
    return !!this.job && !this.isLoading && !this.error;
  }

  public get displayMessage(): boolean {
    return !!this.error || this.isLoading;
  }

  get userOperations(): IJobOperation[] | null {
    if (!this.job || !this.user) {
      return null;
    }

    return this._userOperationsService.getUserOperations(this.job, this.user);
  }

  get displayWorksheets(): boolean {
    if (!this.job || !this.user) {
      return false;
    }

    const canCreateWorksheets = this._userOperationsService.canCreateWorksheets(
      this.user,
    );

    return canCreateWorksheets && this.userOperations !== null;
  }

  get worksheets(): IJobWorksheet[] | null {
    return this.job ? this.job.worksheets : null;
  }

  get module(): AppModuleEnum {
    return AppModuleEnum.Jobs;
  }

  public onAddWorksheet(operation: string): void {
    if (!this.user || !this.job) {
      return;
    }

    this._modalService.open(StartWorksheetComponent, {
      inputs: {
        job: this.job,
        user: this.user,
        operation,
      },
      options: { fullScreen: true },
    });
  }

  public onStopWorksheet({ worksheet }: { worksheet: IJobWorksheet }): void {
    if (!this.user) {
      return;
    }

    this._modalService.open(EditWorksheetComponent, {
      inputs: { job: this.job, worksheet, user: this.user, action: 'stop' },
      options: { fullScreen: true },
    });
  }

  ngOnDestroy(): void {
    this._subscription.unsubscribe();
    this._wsJobs.disconnect();
    this._store.dispatch(clearJobAction());
  }
}
