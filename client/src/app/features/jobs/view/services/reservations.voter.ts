import { JobStockReservationStatusEnum as Status } from '../../../../shared/enums';
import { IJobStockReservation } from '../../../../shared/interfaces/job/i-job-stock-reservation';
import { IJob } from '../../../../shared/interfaces';

export class ReservationsVoter {
  private readonly resa: IJobStockReservation[] | null;

  constructor(
    private _canCreate: boolean = false,
    private _canHandle: boolean = false,
    private _job: IJob,
  ) {
    this.resa = this._job.stockReservations;
  }

  public canSendReservations(): boolean {
    if (!this._canChange()) {
      return false;
    }

    return (
      this.resa!.some((item) => item.status === Status.Brouillon) &&
      this.resa!.every(
        (item) =>
          item.status === Status.Brouillon || item.status === Status.ATraiter,
      ) &&
      this._canCreate
    );
  }

  public canReturnToDraft(): boolean {
    if (!this._canChange()) {
      return false;
    }

    return (
      this.resa!.some((item) => item.status === Status.ATraiter) &&
      this.resa!.every(
        (item) =>
          item.status === Status.ATraiter || item.status === Status.Brouillon,
      ) &&
      this._canHandle
    );
  }

  public canSetWaitingForDelivery(): boolean {
    if (!this._canChange()) {
      return false;
    }

    return (
      this.resa!.some((item) => item.status === Status.ATraiter) &&
      this.resa!.every(
        (item) =>
          item.status === Status.AttenteLivraison ||
          item.status === Status.ATraiter,
      ) &&
      this._canHandle
    );
  }

  public canResetWaitingForDelivery(): boolean {
    if (!this._canChange()) {
      return false;
    }

    return (
      this.resa!.some((item) => item.status === Status.AttenteLivraison) &&
      this.resa!.every(
        (item) =>
          item.status === Status.AttenteLivraison ||
          item.status === Status.ATraiter,
      ) &&
      this._canHandle
    );
  }

  public canHandleReservations(): boolean {
    if (!this._canChange()) {
      return false;
    }

    return (
      this.resa!.some((item) => item.status === Status.ATraiter) &&
      this._canHandle
    );
  }

  public canRemoveAll(): boolean {
    if (!this._canChange()) {
      return false;
    }

    return (
      this.resa!.every((item) => item.status === Status.Brouillon) &&
      this._canCreate
    );
  }

  public canAddReservation(): boolean {
    return this._canCreate && this._job.mp.statusNumber !== 1000;
  }

  private _canChange(): boolean {
    if (!this.resa || !this.resa.length || this._job.mp.statusNumber === 1000) {
      return false;
    }

    return true;
  }
}
