import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { IJobWorksheet } from '../../../../shared/interfaces';

@Injectable({ providedIn: 'root' })
export class JobWorksheetEventService {
  public readonly onStop$ = new Subject<IJobWorksheet | null>();
  public readonly onDelete$ = new Subject<IJobWorksheet | null>();
  public readonly onUpdate$ = new Subject<IJobWorksheet | null>();

  public stop(worksheet: IJobWorksheet): void {
    this.onStop$.next(worksheet);
  }

  public delete(worksheet: IJobWorksheet): void {
    this.onDelete$.next(worksheet);
  }

  public update(worksheet: IJobWorksheet): void {
    this.onUpdate$.next(worksheet);
  }
}
