import { Injectable } from '@angular/core';
import { IJobStockReservation } from '../../../../shared/interfaces/job/i-job-stock-reservation';
import { IHandleStockReservationData } from '../interfaces/i-handle-stock-reservation-data';
import { JobStockReservationStatusEnum } from '../../../../shared/enums';
import { Paper } from '../../../../shared/entities';

@Injectable({ providedIn: 'root' })
export class HandleReservationListService {
  public getInitData(
    reservations: IJobStockReservation[],
  ): IHandleStockReservationData[] {
    const freeStockMap = new Map<number, number>();

    const listData: IHandleStockReservationData[] = reservations
      .filter((item) => item.status === JobStockReservationStatusEnum.ATraiter)
      .map((item) => {
        let quantity = 0;
        let paper: Paper | null = null;
        let max = 0;

        if (item.paper) {
          paper = new Paper(item.paper);

          const freeStock = freeStockMap.get(paper.id);

          if (freeStock !== undefined) {
            quantity = freeStock >= item.quantity ? item.quantity : freeStock;
            freeStockMap.set(
              paper.id,
              freeStock - item.quantity >= 0 ? freeStock - item.quantity : 0,
            );
          } else {
            quantity =
              paper.freeStock >= item.quantity
                ? item.quantity
                : paper.freeStock;
            freeStockMap.set(
              paper.id,
              paper.freeStock - item.quantity >= 0
                ? paper.freeStock - item.quantity
                : 0,
            );
          }

          max =
            paper.freeStock >= item.quantity ? item.quantity : paper.freeStock;
        }

        return {
          enabled: true,
          paper,
          max,
          isAllReserved: false,
          quantity,
          status: 'none',
          reservation: item,
        };
      });

    return this.getComputedData(listData);
  }

  public getComputedData(
    listData: IHandleStockReservationData[],
  ): IHandleStockReservationData[] {
    const reservedMap = new Map<number, number>();

    listData.forEach((item) => {
      if (!item.paper || !item.enabled) {
        return;
      }

      const reserved = reservedMap.get(item.paper.id);

      if (reserved !== undefined) {
        reservedMap.set(item.paper.id, reserved + item.quantity);
      } else {
        reservedMap.set(item.paper.id, item.quantity);
      }
    });

    return listData.map((item) => {
      if (!item.paper || !item.enabled) {
        return item;
      }

      const reserved = reservedMap.get(item.paper.id);

      item.isAllReserved =
        reserved !== undefined && reserved === item.paper.freeStock;
      item.max =
        item.paper.freeStock >= item.reservation.quantity
          ? item.reservation.quantity
          : item.paper.freeStock;

      return item;
    });
  }
}
