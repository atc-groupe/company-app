import { Injectable } from '@angular/core';
import { IJobStockReservation } from '../../../../shared/interfaces/job/i-job-stock-reservation';
import { IHandleStockReservationData } from '../interfaces/i-handle-stock-reservation-data';
import { Paper } from '../../../../shared/entities';

@Injectable({ providedIn: 'root' })
export class HandleReservationService {
  public getInitData(
    reservation: IJobStockReservation,
  ): IHandleStockReservationData {
    return {
      enabled: true,
      paper: reservation.paper ? new Paper(reservation.paper) : null,
      max: this._getUniqueMaxQuantity(reservation),
      isAllReserved: false,
      quantity: 0,
      status: 'none',
      reservation,
    };
  }
  private _getUniqueMaxQuantity(reservation: IJobStockReservation): number {
    if (!reservation.paper) {
      return 0;
    }

    return reservation.paper.mp.freeStock >= reservation.quantity
      ? reservation.quantity
      : reservation.paper.mp.freeStock;
  }
}
