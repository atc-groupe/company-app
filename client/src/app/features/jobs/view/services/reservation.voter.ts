import { IJobStockReservation } from '../../../../shared/interfaces/job/i-job-stock-reservation';
import { JobStockReservationStatusEnum as Status } from '../../../../shared/enums';
import { IJob } from '../../../../shared/interfaces';

export class ReservationVoter {
  constructor(
    private _canCreate: boolean = false,
    private _canHandle: boolean = false,
    private _resa: IJobStockReservation,
    private _job: IJob,
  ) {}

  public canUpdate(): boolean {
    return (
      this._resa.status === Status.Brouillon &&
      this._canCreate &&
      !this._isArchivedJob()
    );
  }

  public canRemove(): boolean {
    return (
      this._resa.status === Status.Brouillon &&
      this._canCreate &&
      !this._isArchivedJob()
    );
  }

  public canHandle(): boolean {
    return (
      this._resa.status === Status.ATraiter &&
      this._canHandle &&
      !this._isArchivedJob()
    );
  }

  public canSend(): boolean {
    return (
      this._resa.status === Status.Brouillon &&
      this._canCreate &&
      !this._isArchivedJob()
    );
  }

  public canReturnToDraft(): boolean {
    return (
      this._resa.status === Status.ATraiter &&
      this._canHandle &&
      !this._isArchivedJob()
    );
  }

  public canSetWaitingForDelivery(): boolean {
    return (
      this._resa.status === Status.ATraiter &&
      this._canHandle &&
      !this._isArchivedJob()
    );
  }

  public canResetWaitingForDelivery(): boolean {
    return (
      this._resa.status === Status.AttenteLivraison &&
      this._canHandle &&
      !this._isArchivedJob()
    );
  }

  public canCancelReservation(): boolean {
    return (
      this._resa.status === Status.Traitee &&
      this._canHandle &&
      !this._isArchivedJob()
    );
  }

  private _isArchivedJob(): boolean {
    return this._job.mp.statusNumber === 1000;
  }
}
