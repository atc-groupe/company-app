import { IJobStockReservation } from '../../../../shared/interfaces/job/i-job-stock-reservation';
import { Paper } from '../../../../shared/entities';

export interface IHandleStockReservationData {
  enabled: boolean;
  paper: Paper | null;
  max: number;
  isAllReserved: boolean;
  quantity: number;
  status: 'none' | 'pending' | 'success' | 'error';
  reservation: IJobStockReservation;
}
