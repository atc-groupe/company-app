import { Routes } from '@angular/router';
import { ListComponent } from './list/list.component';
import { ViewComponent } from './view/view.component';
import { InfosComponent } from './view/views/infos/infos.component';
import { HistoryComponent } from './view/views/history/history.component';
import { FilesCheckComponent } from './view/views/files-check/files-check.component';
import { GraphicDesignNoteComponent } from './view/views/graphic-design-note/graphic-design-note.component';
import { DesignNoteComponent } from './view/views/design-note/design-note.component';

import { RemarksComponent } from './view/views/remarks/remarks.component';
import { PrepressNoteComponent } from './view/views/prepress-note/prepress-note.component';
import { ReservationsListComponent } from './view/views/stock-reservations/reservations-list/reservations-list.component';
import { HandleComponent } from './view/views/stock-reservations/handle/handle.component';
import { SubJobsComponent } from './view/views/sub-jobs/sub-jobs.component';
import { SubcontractorsComponent } from './view/views/subcontractors/subcontractors.component';
import { OperationsComponent } from './view/views/operations/operations.component';
import { PlanningComponent } from './view/views/planning/planning.component';
import { DeliveryAdressesComponent } from './view/views/delivery-adresses/delivery-adresses.component';
export default [
  {
    path: '',
    children: [
      { path: '', redirectTo: 'list', pathMatch: 'full' },
      { path: 'list', component: ListComponent },
      {
        path: ':mpNumber/view',
        component: ViewComponent,
        children: [
          { path: '', redirectTo: 'infos', pathMatch: 'full' },
          { path: 'infos', component: InfosComponent },
          { path: 'history', component: HistoryComponent },
          { path: 'remarks', component: RemarksComponent },
          {
            path: 'graphic-design-note',
            component: GraphicDesignNoteComponent,
          },
          { path: 'design-note', component: DesignNoteComponent },
          { path: 'files-check', component: FilesCheckComponent },
          { path: 'pre-press-note', component: PrepressNoteComponent },
          {
            path: 'stock-reservations/list',
            component: ReservationsListComponent,
          },
          {
            path: 'stock-reservations/handle',
            component: HandleComponent,
          },
          {
            path: 'stock-reservations/handle/:id',
            component: HandleComponent,
          },
          { path: 'sub-jobs', component: SubJobsComponent },
          { path: 'subcontractors', component: SubcontractorsComponent },
          { path: 'operations', component: OperationsComponent },
          { path: 'planning', component: PlanningComponent },
          {
            path: 'delivery-addresses',
            component: DeliveryAdressesComponent,
          },
        ],
      },
    ],
  },
] satisfies Routes;
