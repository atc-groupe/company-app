import { Routes } from '@angular/router';
import { ExpeditionsComponent } from './expeditions.component';
import { PlanningComponent } from './planning/planning.component';

export default [
  {
    path: '',
    component: ExpeditionsComponent,
    children: [
      { path: '', redirectTo: 'planning', pathMatch: 'full' },
      {
        path: 'planning',
        component: PlanningComponent,
      },
    ],
  },
] satisfies Routes;
