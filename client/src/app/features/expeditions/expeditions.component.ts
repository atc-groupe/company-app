import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-expeditions',
  standalone: true,
  imports: [CommonModule, RouterOutlet],
  templateUrl: './expeditions.component.html',
  styleUrls: ['./expeditions.component.scss'],
})
export class ExpeditionsComponent {}
