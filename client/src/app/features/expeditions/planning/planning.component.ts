import { Component, OnDestroy } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeadingComponent } from './components/heading/heading.component';
import { Store } from '@ngrx/store';
import { MessageComponent } from './components/message/message.component';
import { ModalService, WsExpeditionsService } from '../../../shared/services';
import {
  IExpeditionLine,
  IJobMappedStatus,
  ISortColumn,
} from '../../../shared/interfaces';
import { PrintComponent } from './components/print/print.component';
import { selectUser } from '../../../shared/store/user/user.selectors';
import * as JobActions from '../../../shared/store/job/job.actions';
import { Router } from '@angular/router';
import { DataHeadingComponent } from '../../../shared/components/data-heading/data-heading.component';
import { planningColumns } from './planning-columns.constant';
import * as ExpeSelectors from '../../../shared/store/expeditions/expeditions.selectors';
import * as ExpeActions from '../../../shared/store/expeditions/expeditions.actions';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { ShowDirective } from '../../../shared/directives/show.directive';
import { DataLineComponent } from './components/data-line/data-line.component';
import { HideDirective } from '../../../shared/directives/hide.directive';
import { DataCardComponent } from './components/data-card/data-card.component';
import { CommentEditComponent } from './components/comment-edit/comment-edit.component';
import { JobStatusEditComponent } from './components/job-status-edit/job-status-edit.component';
import { AuthActionsJobsEnum, AuthSubjectsEnum } from '../../../shared/enums';
import * as MappedStatusesSelectors from '../../../shared/store/job-mapped-statuses/job-mapped-statuses.selectors';

@Component({
  selector: 'app-planning',
  standalone: true,
  imports: [
    CommonModule,
    HeadingComponent,
    MessageComponent,
    DataHeadingComponent,
    ShowDirective,
    DataLineComponent,
    HideDirective,
    DataCardComponent,
  ],
  templateUrl: './planning.component.html',
  styleUrls: ['./planning.component.scss'],
})
export class PlanningComponent implements OnDestroy {
  public date: Date | null = null;
  public fetchError: string | null = null;
  public isLoading = true;
  public searchValue: string | null = null;
  public canUpdateComment = false;
  public canUpdateStatus = false;
  public columns = planningColumns;
  public data: IExpeditionLine[] | null = null;
  public sortColumn$ = this._store.select(ExpeSelectors.selectSortColumn);
  public mappedStatuses: IJobMappedStatus[] | null = null;
  constructor(
    private _store: Store,
    private _wsExpeditions: WsExpeditionsService,
    private _modalService: ModalService,
    private _router: Router,
  ) {
    this._wsExpeditions.connect();

    this._wsExpeditions
      .onConnect()
      .pipe(takeUntilDestroyed())
      .subscribe(() =>
        this._store.dispatch(ExpeActions.trySubscribeDataAction()),
      );

    this._wsExpeditions
      .onConnectError()
      .pipe(takeUntilDestroyed())
      .subscribe((error) =>
        this._store.dispatch(
          ExpeActions.subscribeDataErrorAction({ error: error.message }),
        ),
      );

    this._wsExpeditions
      .onError()
      .pipe(takeUntilDestroyed())
      .subscribe((error) => {
        this._store.dispatch(
          ExpeActions.subscribeDataErrorAction({ error: error.message }),
        );
      });

    this._store
      .select(ExpeSelectors.selectData)
      .pipe(takeUntilDestroyed())
      .subscribe((data) => (this.data = data));

    this._store
      .select(ExpeSelectors.selectDate)
      .pipe(takeUntilDestroyed())
      .subscribe((date) => (this.date = date));

    this._store
      .select(ExpeSelectors.selectFetchError)
      .pipe(takeUntilDestroyed())
      .subscribe((fetchError) => (this.fetchError = fetchError));

    this._store
      .select(ExpeSelectors.selectIsLoading)
      .pipe(takeUntilDestroyed())
      .subscribe((isLoading) => (this.isLoading = isLoading));

    this._store
      .select(ExpeSelectors.selectSearch)
      .pipe(takeUntilDestroyed())
      .subscribe((searchValue) => (this.searchValue = searchValue));

    this._store
      .select(selectUser)
      .pipe(takeUntilDestroyed())
      .subscribe((user) => {
        if (user) {
          this.canUpdateStatus = user.canActivate({
            subject: AuthSubjectsEnum.Jobs,
            action: AuthActionsJobsEnum.UpdateStatus,
          });

          this.canUpdateComment = user.canActivate({
            subject: AuthSubjectsEnum.Jobs,
            action: AuthActionsJobsEnum.UpdateDeliveryPlanningComment,
          });
        }
      });

    this._store
      .select(MappedStatusesSelectors.selectMappedStatuses)
      .pipe(takeUntilDestroyed())
      .subscribe((statuses) => (this.mappedStatuses = statuses));
  }

  public onSelectPrevDate(): void {
    this._store.dispatch(ExpeActions.setPrevDateAction());
  }

  public onSelectNextDate(): void {
    this._store.dispatch(ExpeActions.setNextDateAction());
  }

  public onResetDate(): void {
    this._store.dispatch(ExpeActions.resetDateAction());
  }

  public onStartSearch(): void {
    this._store.dispatch(ExpeActions.startSearchAction());
  }

  public onSearch(event: { search: string }): void {
    this._store.dispatch(ExpeActions.searchAction({ search: event.search }));
  }

  public onClearSearch(): void {
    this._store.dispatch(ExpeActions.clearSearchAction());
  }

  public onSort(sortColumn: ISortColumn | null) {
    this._store.dispatch(ExpeActions.sortDataAction({ sortColumn }));
  }

  onUpdateComment(line: IExpeditionLine): void {
    if (!this.canUpdateComment) {
      return;
    }

    this._modalService.open(CommentEditComponent, {
      options: { yPos: 'center' },
      inputs: { line },
    });
  }

  onPrintPlanning(): void {
    this._modalService.open(PrintComponent, {
      options: { fullScreen: true },
      inputs: {
        data: this.data,
        date: this.date,
        mappedStatuses: this.mappedStatuses,
      },
    });
  }

  onUpdateJobStatus(line: IExpeditionLine): void {
    if (!this.canUpdateStatus) {
      return;
    }

    this._modalService.open(JobStatusEditComponent, {
      options: { yPos: 'center' },
      inputs: { line },
    });
  }

  public async onViewJob(mpNumber: number): Promise<void> {
    this._store.dispatch(JobActions.setFromAction({ route: '/expeditions' }));

    await this._router.navigateByUrl(`/jobs/${mpNumber}/view`);
  }

  get displayMessage(): boolean {
    return this.isLoading || this.fetchError !== null || !this.data?.length;
  }

  get displayData(): boolean {
    return !this.displayMessage;
  }

  get linesCount(): number {
    return this.data ? this.data.length : 0;
  }

  ngOnDestroy() {
    this._wsExpeditions.disconnect();
    this._store.dispatch(ExpeActions.clearDataAction());
  }
}
