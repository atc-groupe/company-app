import { IDataColumn } from '../../../shared/interfaces';

export const planningColumns: IDataColumn[] = [
  { label: 'Job', flex: 2 },
  { label: 'Client', flex: 8 },
  { label: 'Statut', flex: 4 },
  { label: 'FAB', flex: 6 },
  { label: 'M2', flex: 1, textAlign: 'center', notSortable: true },
  { label: 'EX', flex: 1, textAlign: 'center', notSortable: true },
  { label: 'PLAK', flex: 1, textAlign: 'center', notSortable: true },
  { label: 'FNSH', flex: 1, textAlign: 'center', notSortable: true },
  { label: 'D+', flex: 1, textAlign: 'center', notSortable: true },
  { label: 'CP', flex: 3 },
  { label: 'Méthodes', flex: 4 },
  { label: 'Commentaire', flex: 8, notSortable: true },
  { icon: 'sync', flex: 1, textAlign: 'center', notSortable: true },
];
