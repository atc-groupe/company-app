import { Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { User } from '../../../../../shared/entities';
import {
  FormBuilder,
  FormControl,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import {
  JobsService,
  ModalService,
  NotificationsService,
} from '../../../../../shared/services';
import { SelectComponent } from '../../../../../shared/components/select/select.component';
import { BehaviorSubject, first } from 'rxjs';
import { IExpeditionLine, ISelectItem } from '../../../../../shared/interfaces';
import { Store } from '@ngrx/store';
import { selectUser } from '../../../../../shared/store/user/user.selectors';
import { IJobStatusChangeDto } from '../../../../../shared/dto';
import { SpinnerComponent } from '../../../../../shared/components/spinner/spinner.component';
import { CancelButtonComponent } from '../../../../../shared/components/cancel-button/cancel-button.component';
import { ModalCloseDirective } from '../../../../../shared/directives/modal-close.directive';
import { SaveButtonComponent } from '../../../../../shared/components/save-button/save-button.component';
import { MessageComponent } from '../../../../../shared/components/message/message.component';

@Component({
  selector: 'exp-job-status-edit',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SelectComponent,
    SpinnerComponent,
    CancelButtonComponent,
    ModalCloseDirective,
    SaveButtonComponent,
    MessageComponent,
  ],
  templateUrl: './job-status-edit.component.html',
  styleUrls: ['./job-status-edit.component.scss'],
})
export class JobStatusEditComponent implements OnInit {
  @Input() line!: IExpeditionLine;

  public error: string | null = null;
  public processing = false;
  public statuses$ = new BehaviorSubject<ISelectItem[]>([]);
  public form = this._fb.group({
    statusNumber: new FormControl<number | null>(null, Validators.required),
  });

  private _user: User | null = null;

  constructor(
    private _fb: FormBuilder,
    private _jobsService: JobsService,
    private _modalService: ModalService,
    private _store: Store,
    private _notificationService: NotificationsService,
  ) {}

  ngOnInit() {
    this._store
      .select(selectUser)
      .pipe(first((v) => v !== null))
      .subscribe((user) => {
        if (!user) {
          this.error = 'Une erreur est survenue. Merci de re-essayer';
          return;
        }

        this._user = user;

        this._jobsService.getStatusesList().subscribe({
          next: (list) => {
            let statuses = list.map((status) => {
              return {
                label: status.label,
                value: status.number,
              };
            });

            if (!user.isAdmin) {
              statuses = statuses.filter(
                (status) =>
                  user.mp.operations.some(
                    (op) => op.label === status.label && op.type === 2,
                  ) || status.value === 1000,
              );
            }

            if (statuses.length) {
              this.statuses$.next(statuses);
              this.form.controls.statusNumber.setValue(
                user.isAdmin
                  ? this.line.jobStatusNumber
                  : this.statuses$.value[0].value,
              );
            }

            this.statuses$.next(statuses);
            this.form.controls.statusNumber.setValue(this.line.jobStatusNumber);
          },
          error: () => {
            this.error = 'Impossible de récupérer les statuts de jobs';
          },
        });
      });
  }

  public onSubmit(): void {
    if (!this._user) {
      return;
    }

    const statusNumber = this.form.controls.statusNumber.value;

    if (!statusNumber) {
      this.error = 'Veuillez sélectionner un statut';
      return;
    }

    if (statusNumber === this.line.jobStatusNumber) {
      this.error = `Le statut n'a pas été modifié`;
      return;
    }

    this.error = null;
    this.processing = true;

    const dto: IJobStatusChangeDto = {
      statusNumber,
      reason: `[API] changement par ${this._user.completeName}`,
    };

    this._jobsService.changeJobStatus(this.line.jobNumber, dto).subscribe({
      next: () => {
        this._notificationService.displaySuccess(
          `Statut du job ${this.line.jobNumber} modifié !`,
        );
        this._modalService.close();
      },
      error: (err) => {
        this.error = `Impossible d'effectuer l'opération. ${err.error.message}`;
        this.processing = false;
      },
    });
  }
}
