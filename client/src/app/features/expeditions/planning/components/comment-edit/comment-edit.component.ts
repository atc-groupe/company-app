import { Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import {
  ModalService,
  JobCommentService,
  NotificationsService,
} from '../../../../../shared/services';
import { SpinnerComponent } from '../../../../../shared/components/spinner/spinner.component';
import { IExpeditionLine } from '../../../../../shared/interfaces';
import { CancelButtonComponent } from '../../../../../shared/components/cancel-button/cancel-button.component';
import { ModalCloseDirective } from '../../../../../shared/directives/modal-close.directive';
import { SaveButtonComponent } from '../../../../../shared/components/save-button/save-button.component';

@Component({
  selector: 'exp-comment',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SpinnerComponent,
    CancelButtonComponent,
    ModalCloseDirective,
    SaveButtonComponent,
  ],
  templateUrl: './comment-edit.component.html',
  styleUrls: ['./comment-edit.component.scss'],
})
export class CommentEditComponent implements OnInit {
  @Input() line!: IExpeditionLine;

  public form = this._fb.group({
    comment: [''],
  });
  private _hasComment = false;

  constructor(
    private _fb: FormBuilder,
    private _modalService: ModalService,
    private _jobCommentService: JobCommentService,
    private _notificationService: NotificationsService,
  ) {}

  ngOnInit() {
    this._hasComment = !!this.line.comment;
    this.form
      .get('comment')
      ?.setValue(this._hasComment ? this.line.comment : null);
  }

  public onSubmit() {
    const comment = this.form.get('comment')?.value;

    const parsedComment = comment ? comment.trim() : null;
    this._jobCommentService
      .updateDeliveryComment(this.line._id, parsedComment)
      .subscribe({
        next: () => {
          let action: string | null = null;
          if (this._hasComment && !parsedComment) {
            action = 'supprimé';
          }

          if (!this._hasComment && parsedComment) {
            action = 'ajouté';
          }

          if (this._hasComment && parsedComment) {
            action = 'modifié';
          }

          if (action) {
            this._notificationService.displaySuccess(`Commentaire ${action}!`);
          }

          return this._modalService.close();
        },
        error: () =>
          this._notificationService.displayError(
            'Oups, un problème est survenu. Veuillez ré-essayer',
          ),
      });
  }
}
