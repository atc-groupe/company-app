import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentEditComponent } from './comment-edit.component';

describe('CommentComponent', () => {
  let component: CommentEditComponent;
  let fixture: ComponentFixture<CommentEditComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CommentEditComponent],
    });
    fixture = TestBed.createComponent(CommentEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
