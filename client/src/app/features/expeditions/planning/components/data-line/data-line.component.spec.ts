import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataLineComponent } from './data-line.component';

describe('CardLineComponent', () => {
  let component: DataLineComponent;
  let fixture: ComponentFixture<DataLineComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [DataLineComponent],
    });
    fixture = TestBed.createComponent(DataLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
