import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IDataColumn, IExpeditionLine } from '../../../../../shared/interfaces';
import { JobStatusComponent } from '../../../../../shared/components/job-status/job-status.component';
import { JobDevicesComponent } from '../../../../../shared/components/job-devices/job-devices.component';
import { DisplayListPipe } from '../../../../../shared/pipes/display-list.pipe';

@Component({
  selector: 'exp-planning-card-line',
  standalone: true,
  imports: [
    CommonModule,
    JobStatusComponent,
    JobDevicesComponent,
    DisplayListPipe,
  ],
  templateUrl: './data-line.component.html',
  styleUrls: ['./data-line.component.scss'],
})
export class DataLineComponent {
  @Input({ required: true }) line!: IExpeditionLine;
  @Input({ required: true }) canUpdateStatus = false;
  @Input({ required: true }) canUpdateComment = false;
  @Input({ required: true }) columns!: IDataColumn[];

  @Output() editComment = new EventEmitter<void>();
  @Output() viewJob = new EventEmitter();
  @Output() changeStatus = new EventEmitter<void>();

  public get displayAddCommentButton(): boolean {
    return !this.line.comment;
  }

  public get jobNumberColorClass(): string {
    return this.line.sync.status === 'error' ? 'color-error' : 'color-primary';
  }

  public onChangeStatus(): void {
    this.changeStatus.emit();
  }

  public onEditComment(): void {
    this.editComment.emit();
  }

  public onViewJob(): void {
    this.viewJob.emit();
  }
}
