import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { SearchComponent } from '../../../../../shared/components/search/search.component';
import { debounceTime, Subscription, tap } from 'rxjs';

@Component({
  selector: 'exp-heading',
  standalone: true,
  imports: [CommonModule, SearchComponent, ReactiveFormsModule],
  templateUrl: './heading.component.html',
  styleUrls: ['./heading.component.scss'],
})
export class HeadingComponent implements OnInit, OnDestroy {
  @Input() public date: Date | null = null;
  @Input() public searchValue: string | null = null;

  @Output() private setNextDate = new EventEmitter<void>();
  @Output() private setPrevDate = new EventEmitter<void>();
  @Output() private resetDate = new EventEmitter<void>();
  @Output() private search = new EventEmitter<{ search: string }>();
  @Output() private clearSearch = new EventEmitter<void>();
  @Output() private startSearch = new EventEmitter<void>();
  @Output() private printPlanning = new EventEmitter<void>();

  public form = this._fb.group({
    search: [''],
  });

  private _subscription = new Subscription();

  constructor(private _fb: FormBuilder) {}

  ngOnInit() {
    const search = this.form.controls.search;

    search.setValue(this.searchValue);
    this._subscription.add(
      search.valueChanges
        .pipe(
          tap(() => {
            this.startSearch.emit();
          }),
          debounceTime(300),
        )
        .subscribe((value) => {
          if (!value) {
            this.clearSearch.emit();
            return;
          }

          this.search.emit({ search: value });
        }),
    );
  }

  public onSetPrevDate() {
    this.setPrevDate.emit();
  }

  public onSetNextDate() {
    this.setNextDate.emit();
  }

  public onResetDate() {
    this.resetDate.emit();
  }

  public onPrintPlanning(): void {
    this.printPlanning.emit();
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
