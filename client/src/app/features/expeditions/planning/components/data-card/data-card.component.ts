import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataLineComponent } from '../data-line/data-line.component';
import { JobStatusComponent } from '../../../../../shared/components/job-status/job-status.component';
import { JobDevicesComponent } from '../../../../../shared/components/job-devices/job-devices.component';
import { DisplayListPipe } from '../../../../../shared/pipes/display-list.pipe';

@Component({
  selector: 'exp-planning-card',
  standalone: true,
  imports: [
    CommonModule,
    JobStatusComponent,
    JobDevicesComponent,
    DisplayListPipe,
  ],
  templateUrl: './data-card.component.html',
  styleUrls: ['./data-card.component.scss'],
})
export class DataCardComponent extends DataLineComponent {}
