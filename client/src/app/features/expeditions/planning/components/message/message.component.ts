import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpinnerComponent } from '../../../../../shared/components/spinner/spinner.component';
import { EmptyMessageComponent } from '../../../../../shared/components/empty-message/empty-message.component';

@Component({
  selector: 'exp-message',
  standalone: true,
  imports: [CommonModule, SpinnerComponent, EmptyMessageComponent],
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss'],
})
export class MessageComponent {
  @Input() public isLoading = false;
  @Input() public fetchError: string | null = null;
  @Input() public linesCount = 0;
  @Input() public searchValue: string | null = null;

  get displayErrorMessage(): boolean {
    return !!this.fetchError;
  }

  get displayLoader(): boolean {
    return this.isLoading && !this.fetchError;
  }

  get displayEmptyPlanningMessage(): boolean {
    return (
      !this.isLoading &&
      !this.fetchError &&
      !this.searchValue &&
      !this.linesCount
    );
  }

  get displayEmptySearchMessage(): boolean {
    return (
      !this.isLoading &&
      !this.fetchError &&
      !!this.searchValue &&
      !this.linesCount
    );
  }
}
