import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClickOutsideDirective } from '../../../../../shared/directives/click-outside.directive';
import { ModalService } from '../../../../../shared/services';
import { DisplayListPipe } from '../../../../../shared/pipes/display-list.pipe';
import {
  IExpeditionLine,
  IJobMappedStatus,
} from '../../../../../shared/interfaces';

@Component({
  selector: 'exp-print',
  standalone: true,
  imports: [CommonModule, ClickOutsideDirective, DisplayListPipe],
  templateUrl: './print.component.html',
  styleUrls: ['./print.component.scss'],
})
export class PrintComponent {
  @Input() date!: Date;
  @Input() data!: IExpeditionLine[];
  @Input() mappedStatuses!: IJobMappedStatus[];

  public today = new Date();

  constructor(private _modalService: ModalService) {}

  public getStatusLabel(number: number): string | null {
    return (
      this.mappedStatuses.find((status) => status.statusNumber === number)
        ?.label ?? null
    );
  }

  public onClick(): void {
    this._modalService.close();
  }
}
