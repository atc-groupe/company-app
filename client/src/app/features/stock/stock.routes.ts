import { Routes } from '@angular/router';
import { StockComponent } from './stock.component';
import { ProductsComponent } from './products/products.component';
import { ProductComponent } from './product/product.component';
import { ReservationsComponent } from './reservations/reservations.component';

export default [
  {
    path: '',
    component: StockComponent,
    children: [
      { path: '', redirectTo: 'papers/sheets', pathMatch: 'full' },
      {
        path: 'reservations/pending',
        component: ReservationsComponent,
      },
      {
        path: 'reservations/done',
        component: ReservationsComponent,
      },
      {
        path: ':productType/:groupType',
        component: ProductsComponent,
      },
      {
        path: ':productType/:groupType/:id',
        component: ProductComponent,
      },
    ],
  },
] satisfies Routes;
