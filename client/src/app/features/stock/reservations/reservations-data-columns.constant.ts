import { IDataColumn } from '../../../shared/interfaces';

export const reservationsDataColumns: IDataColumn[] = [
  { label: 'N° JOB', flex: 2 },
  { label: 'Statut du job', flex: 3 },
  { label: 'Client', flex: 7 },
  { icon: 'local_shipping', flex: 1 },
  { label: 'resa', flex: 1, textAlign: 'center' },
  { label: 'Statut resa', flex: 3, textAlign: 'center' },
];
