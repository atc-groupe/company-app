import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JobStatusComponent } from '../../../../../shared/components/job-status/job-status.component';
import {
  IDataColumn,
  IJobStockReservationLine,
} from '../../../../../shared/interfaces';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'job-stock-reservation',
  standalone: true,
  imports: [CommonModule, JobStatusComponent, RouterLink],
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.scss'],
})
export class ReservationComponent {
  @Input({ required: true }) line!: IJobStockReservationLine;
  @Input({ required: true }) columns!: IDataColumn[];

  @Output() navigateToJob = new EventEmitter<void>();

  public onNavigateToJob() {
    this.navigateToJob.emit();
  }
}
