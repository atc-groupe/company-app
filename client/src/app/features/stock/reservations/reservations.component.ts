import { Component, OnDestroy } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WSJobsStockReservationsService } from '../../../shared/services';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { Store } from '@ngrx/store';
import {
  IDataColumn,
  IJobStockReservationLine,
  ISortColumn,
} from '../../../shared/interfaces';
import * as JobActions from '../../../shared/store/job/job.actions';
import { JobStockReservationStatusEnum } from '../../../shared/enums';
import { SpinnerComponent } from '../../../shared/components/spinner/spinner.component';
import { DataHeadingComponent } from '../../../shared/components/data-heading/data-heading.component';
import { StatusComponent } from '../../jobs/view/components/status/status.component';
import { JobStatusComponent } from '../../../shared/components/job-status/job-status.component';
import { ReservationComponent } from './components/reservation/reservation.component';
import { ActivatedRoute, Router } from '@angular/router';
import * as ResaActions from '../../../shared/store/stock/stock-reservations.actions';
import * as ResaSelectors from '../../../shared/store/stock/stock-reservations.selectors';
import { reservationsDataColumns } from './reservations-data-columns.constant';
import { setNavbarItemsAction } from '../../../shared/store/navbar';
import { MessageComponent } from '../../../shared/components/message/message.component';

@Component({
  selector: 'app-reservations',
  standalone: true,
  imports: [
    CommonModule,
    SpinnerComponent,
    DataHeadingComponent,
    StatusComponent,
    JobStatusComponent,
    ReservationComponent,
    MessageComponent,
  ],
  templateUrl: './reservations.component.html',
  styleUrls: ['./reservations.component.scss'],
})
export class ReservationsComponent implements OnDestroy {
  public isLoading = true;
  public error: string | null = null;
  public data: IJobStockReservationLine[] | null = null;
  public title: string | null = null;
  public columns: IDataColumn[] = reservationsDataColumns;
  public sortColumn$ = this._store.select(ResaSelectors.selectSortColumn);

  constructor(
    private _wsReservations: WSJobsStockReservationsService,
    private _store: Store,
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
  ) {
    this._wsReservations.connect();

    this._wsReservations
      .onConnect()
      .pipe(takeUntilDestroyed())
      .subscribe(() => {
        this._store.dispatch(ResaActions.trySubscribeDataAction());
      });

    this._store
      .select(ResaSelectors.selectIsLoading)
      .pipe(takeUntilDestroyed())
      .subscribe((isLoading) => (this.isLoading = isLoading));

    this._store
      .select(ResaSelectors.selectError)
      .pipe(takeUntilDestroyed())
      .subscribe((error) => (this.error = error));

    this._store
      .select(ResaSelectors.selectData)
      .pipe(takeUntilDestroyed())
      .subscribe((lines) => {
        const route = this._activatedRoute.snapshot.url[1].path;

        this._store.dispatch(
          setNavbarItemsAction({
            items: [
              { label: 'Résa matières' },
              { label: `${route === 'pending' ? 'à traiter' : 'traitées'}` },
            ],
          }),
        );

        if (lines) {
          this.data = [];
          lines.forEach((item) => {
            const allDone =
              item.globalStatus === JobStockReservationStatusEnum.Traitee;

            if (route === 'pending' && !allDone) {
              this.data!.push(item);
            }

            if (route === 'done' && allDone) {
              this.data!.push(item);
            }
          });
        }
      });
  }

  public async onNavigateToJob(jobNumber: number): Promise<void> {
    this._store.dispatch(
      JobActions.setFromAction({ route: '/stock/reservations/pending' }),
    );

    await this._router.navigateByUrl(
      `jobs/${jobNumber}/view/stock-reservations/list`,
    );
  }

  public onSortData(sortColumn: ISortColumn | null): void {
    this._store.dispatch(ResaActions.sortDataAction({ sortColumn }));
  }

  ngOnDestroy() {
    this._wsReservations.disconnect();
    this._store.dispatch(ResaActions.clearDataAction());
  }
}
