import { Component, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IDataColumn, IProduct } from '../../../shared/interfaces';
import {
  TArticleGroupType,
  TPaperGroupType,
  TProductGroupType,
} from '../../../shared/types';
import * as ProductsSelectors from '../../../shared/store/stock/stock-products.selectors';
import { Subject, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { WsStockProductsService } from '../../../shared/services';
import { ActivatedRoute } from '@angular/router';
import * as ProductsActions from '../../../shared/store/stock/stock-products.actions';
import { ProductTypeEnum } from '../../../shared/enums';
import { Article, Paper } from '../../../shared/entities';
import { DataHeadingComponent } from '../../../shared/components/data-heading/data-heading.component';
import { PaperDataItemComponent as PaperDataItemComponent } from './components/paper-data-item/paper-data-item.component';
import { ListSearchHeadingComponent } from './components/list-search-heading/list-search-heading.component';
import { SpinnerComponent } from '../../../shared/components/spinner/spinner.component';
import { ArticleDataItemComponent as ArticleDataItemComponent } from './components/article-data-item/article-data-item.component';
import { EmptyMessageComponent } from '../../../shared/components/empty-message/empty-message.component';
import { setNavbarItemsAction } from '../../../shared/store/navbar';
import { MessageComponent } from '../../../shared/components/message/message.component';

@Component({
  selector: 'app-products',
  standalone: true,
  imports: [
    CommonModule,
    DataHeadingComponent,
    ArticleDataItemComponent,
    ListSearchHeadingComponent,
    SpinnerComponent,
    PaperDataItemComponent,
    EmptyMessageComponent,
    MessageComponent,
  ],
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
})
export class ProductsComponent implements OnInit, OnDestroy {
  public products: IProduct[] | null = null;
  public columns: IDataColumn[] = [];
  public error: string | null = null;
  public isLoading = false;
  public searchValue$ = this._store.select(
    ProductsSelectors.selectProductFilter,
  );
  public clearSearchField$ = new Subject<boolean>();

  private _subscription = new Subscription();
  private _productType: ProductTypeEnum | null = null;
  private _groupType: TProductGroupType | null = null;

  constructor(
    private _store: Store,
    private _wsStockProducts: WsStockProductsService,
    private _activatedRoute: ActivatedRoute,
  ) {}

  ngOnInit() {
    this._wsStockProducts.connect();

    this._subscription.add(
      this._activatedRoute.paramMap.subscribe((paramMap) => {
        const productType = paramMap.get('productType');
        const groupType = paramMap.get('groupType');

        if (!productType || !groupType) {
          return;
        }

        let groupTypeLabel = '';
        switch (groupType) {
          case 'rolls':
            groupTypeLabel = 'Rouleaux';
            break;
          case 'sheets':
            groupTypeLabel = 'Plaques';
            break;
          case 'inks':
            groupTypeLabel = 'Encres';
            break;
          case 'large-format':
            groupTypeLabel = 'Grand format';
            break;
          case 'other':
            groupTypeLabel = 'Divers';
            break;
        }

        this._store.dispatch(
          setNavbarItemsAction({
            items: [
              { label: productType === 'papers' ? 'Matières' : 'Articles' },
              { label: groupTypeLabel },
            ],
          }),
        );

        if (productType === 'articles') {
          this._productType = ProductTypeEnum.Article;
          this._groupType = groupType as TArticleGroupType;
          this.columns = [
            { label: 'Catégorie', flex: 3, notSortable: true },
            { label: 'Nom', flex: 4, notSortable: true },
            {
              label: 'Stock',
              flex: 1,
              textAlign: 'center',
              notSortable: true,
            },
            { icon: 'picture_as_pdf', flex: 1, textAlign: 'center' },
          ];
        } else if (productType === 'papers') {
          this._productType = ProductTypeEnum.Paper;
          this._groupType = groupType as TPaperGroupType;
          this.columns = [
            { label: 'Catégorie', flex: 4, notSortable: true },
            { label: 'Nom', flex: 6, notSortable: true },
            { label: 'Générique', flex: 2, notSortable: true },
            {
              label: 'Largeur',
              flex: 1,
              textAlign: 'center',
              notSortable: true,
            },
            {
              label: this._groupType === 'sheets' ? 'Hauteur' : 'Long (Ml)',
              flex: 1,
              textAlign: 'center',
              notSortable: true,
            },
            {
              label: this._groupType === 'sheets' ? 'Stock' : 'Stock (Ml)',
              flex: 1,
              textAlign: 'center',
              notSortable: true,
            },
            { icon: 'picture_as_pdf', flex: 1, textAlign: 'center' },
          ];
        }

        if (this._productType !== null && this._groupType !== null) {
          this._store.dispatch(
            ProductsActions.tryFetchProductsAction({
              productType: this._productType,
              groupType: this._groupType,
            }),
          );
        }
      }),
    );

    this._subscription.add(
      this._wsStockProducts.onConnect().subscribe(() => {
        if (!this._productType || !this._groupType || this.products) {
          return;
        }

        this._store.dispatch(
          ProductsActions.tryFetchProductsAction({
            productType: this._productType,
            groupType: this._groupType,
          }),
        );
      }),
    );

    this._subscription.add(
      this._wsStockProducts.onConnectError().subscribe((err) => {
        this._store.dispatch(
          ProductsActions.onProductsErrorAction({ error: err.message }),
        );
      }),
    );

    this._subscription.add(
      this._wsStockProducts.onError().subscribe((err) => {
        this._store.dispatch(
          ProductsActions.onProductsErrorAction({ error: err.message }),
        );
      }),
    );

    this._subscription.add(
      this._store
        .select(ProductsSelectors.selectProducts)
        .subscribe((products) => (this.products = products)),
    );

    this._subscription.add(
      this._store
        .select(ProductsSelectors.selectError)
        .subscribe((err) => (this.error = err)),
    );

    this._subscription.add(
      this._store
        .select(ProductsSelectors.selectIsLoading)
        .subscribe((isLoading) => (this.isLoading = isLoading)),
    );
  }

  public get articles(): Article[] | null {
    if (
      this._productType === null ||
      this._productType !== ProductTypeEnum.Article
    ) {
      return null;
    }

    return this.products as Article[];
  }

  public get papers(): Paper[] | null {
    if (
      this._productType === null ||
      this._productType !== ProductTypeEnum.Paper
    ) {
      return null;
    }

    return this.products as Paper[];
  }

  public onSearch({ term }: { term: string | null }): void {
    this._store.dispatch(
      term
        ? ProductsActions.searchProductAction({ term })
        : ProductsActions.clearSearchProductAction(),
    );
  }

  public get isEmpty(): boolean {
    return this.products?.length === 0 && !this.isLoading && !this.error;
  }

  public onScroll($event: any): void {
    console.log($event);
  }

  ngOnDestroy() {
    this._wsStockProducts.disconnect();
    this._subscription.unsubscribe();
  }
}
