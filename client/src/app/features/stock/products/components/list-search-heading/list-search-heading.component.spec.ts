import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListSearchHeadingComponent } from './list-search-heading.component';

describe('ListSearchHeadingComponent', () => {
  let component: ListSearchHeadingComponent;
  let fixture: ComponentFixture<ListSearchHeadingComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ListSearchHeadingComponent]
    });
    fixture = TestBed.createComponent(ListSearchHeadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
