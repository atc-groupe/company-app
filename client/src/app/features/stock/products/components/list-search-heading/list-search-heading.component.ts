import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { debounceTime, Observable, Subscription } from 'rxjs';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SearchComponent } from '../../../../../shared/components/search/search.component';

@Component({
  selector: 'stock-list-search-heading',
  standalone: true,
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SearchComponent],
  templateUrl: './list-search-heading.component.html',
  styleUrls: ['./list-search-heading.component.scss'],
})
export class ListSearchHeadingComponent implements OnInit, OnDestroy {
  @Input() searchValue: string | null = null;
  @Input() clearSearchField$ = new Observable<boolean>();

  @Output() search = new EventEmitter<{ term: string | null }>();

  public form = this._fb.group({
    search: [''],
  });

  private _subscription = new Subscription();

  constructor(private _fb: FormBuilder) {}

  ngOnInit() {
    this.form.controls.search.setValue(this.searchValue, { emitEvent: false });

    this._subscription.add(
      this.clearSearchField$.subscribe((value) => {
        if (value) {
          this.form.controls.search.setValue('', { emitEvent: false });
        }
      }),
    );

    this._subscription.add(
      this.form.controls.search.valueChanges
        .pipe(debounceTime(100))
        .subscribe((value) => {
          this.search.emit({ term: value });
        }),
    );
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
