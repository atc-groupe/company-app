import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaperDataItemComponent } from './paper-data-item.component';

describe('DataItemComponent', () => {
  let component: PaperDataItemComponent;
  let fixture: ComponentFixture<PaperDataItemComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [PaperDataItemComponent],
    });
    fixture = TestBed.createComponent(PaperDataItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
