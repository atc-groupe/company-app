import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IDataColumn } from '../../../../../shared/interfaces';
import { RouterLink } from '@angular/router';
import { Paper } from '../../../../../shared/entities';

@Component({
  selector: 'stock-paper-data-item',
  standalone: true,
  imports: [CommonModule, RouterLink],
  templateUrl: './paper-data-item.component.html',
  styleUrls: ['./paper-data-item.component.scss'],
})
export class PaperDataItemComponent {
  @Input() columns: IDataColumn[] | null = null;
  @Input() paper: Paper | null = null;

  get chipStockClass(): string | null {
    if (!this.paper) {
      return null;
    }

    if (this.paper.isAtStockAlert) {
      return 'chip-warning';
    }

    if (this.paper.isUnderStockAlert) {
      return 'chip-error';
    }

    if (this.paper.freeStock === 0) {
      return '';
    }

    return 'chip-info';
  }
}
