import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IDataColumn } from '../../../../../shared/interfaces';
import { RouterLink } from '@angular/router';
import { Article } from '../../../../../shared/entities';

@Component({
  selector: 'stock-article-data-item',
  standalone: true,
  imports: [CommonModule, RouterLink],
  templateUrl: './article-data-item.component.html',
  styleUrls: ['./article-data-item.component.scss'],
})
export class ArticleDataItemComponent {
  @Input() columns: IDataColumn[] = [];
  @Input() article: Article | null = null;

  get chipStockClass(): string | null {
    if (!this.article) {
      return null;
    }

    if (this.article.isUnderStockAlert) {
      return 'chip-warning';
    }

    if (this.article.isUnderStockAlert) {
      return 'chip-error';
    }

    if (this.article.freeStock === 0) {
      return '';
    }

    return 'chip-info';
  }
}
