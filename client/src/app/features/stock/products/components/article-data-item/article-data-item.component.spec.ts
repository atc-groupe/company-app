import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticleDataItemComponent } from './article-data-item.component';

describe('DataItemComponent', () => {
  let component: ArticleDataItemComponent;
  let fixture: ComponentFixture<ArticleDataItemComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ArticleDataItemComponent],
    });
    fixture = TestBed.createComponent(ArticleDataItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
