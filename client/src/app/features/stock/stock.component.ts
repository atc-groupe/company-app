import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { selectNavbarExpanded } from '../../shared/store/navbar';
import { Store } from '@ngrx/store';
import { NavComponent } from './components/nav/nav.component';
import { RouterLink, RouterLinkActive, RouterOutlet } from '@angular/router';
import { FeatureNavHeadingComponent } from '../../shared/components/feature-nav-heading/feature-nav-heading.component';
import { AppModuleEnum } from '../../shared/enums';

@Component({
  selector: 'app-stock',
  standalone: true,
  imports: [
    CommonModule,
    NavComponent,
    RouterOutlet,
    RouterLink,
    RouterLinkActive,
    FeatureNavHeadingComponent,
  ],
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.scss'],
})
export class StockComponent {
  public expanded$ = this._store.select(selectNavbarExpanded);

  constructor(private _store: Store) {}

  public get module(): AppModuleEnum {
    return AppModuleEnum.Stock;
  }
}
