import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IAppFile } from '../../../../../shared/interfaces/i-app-file';
import { MenuComponent } from '../../../../../shared/components/menu/menu.component';
import { IMenuItem } from '../../../../../shared/interfaces';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'product-files',
  standalone: true,
  imports: [CommonModule, MenuComponent, RouterLink],
  templateUrl: './files.component.html',
  styleUrls: ['./files.component.scss'],
})
export class FilesComponent {
  @Input() files: IAppFile[] | null = null;
  @Input() canManageDocuments = false;

  @Output() private addDocument = new EventEmitter<void>();
  @Output() private updateDocument = new EventEmitter<IAppFile>();
  @Output() private updateDocumentInfo = new EventEmitter<IAppFile>();
  @Output() private deleteDocument = new EventEmitter<IAppFile>();
  @Output() private downloadDocument = new EventEmitter<IAppFile>();

  public fileActions: IMenuItem[] = [
    { action: 'updateInfo', label: 'Renommer', icon: 'edit' },
    {
      action: 'update',
      label: 'Mettre à jour le fichier',
      icon: 'file_upload',
    },
    { action: 'download', label: 'Télécharger', icon: 'download' },
    { action: 'delete', label: 'Supprimer', style: 'error', icon: 'delete' },
  ];

  public getFilePath(file: IAppFile): string {
    return `/static/${file.module}/${file.path}/${file._id}${file.extension}`;
  }

  public onAddDocument(): void {
    this.addDocument.emit();
  }

  public onAction(action: string, file: IAppFile): void {
    switch (action) {
      case 'download':
        this.downloadDocument.emit(file);
        break;
      case 'update':
        this.updateDocument.emit(file);
        break;
      case 'updateInfo':
        this.updateDocumentInfo.emit(file);
        break;
      case 'delete':
        this.deleteDocument.emit(file);
    }
  }
}
