import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StockUpdateComponent } from './stock-update.component';

describe('UpdateComponent', () => {
  let component: StockUpdateComponent;
  let fixture: ComponentFixture<StockUpdateComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [StockUpdateComponent],
    });
    fixture = TestBed.createComponent(StockUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
