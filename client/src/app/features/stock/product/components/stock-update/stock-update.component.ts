import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { User } from '../../../../../shared/entities';
import {
  FormBuilder,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import {
  ModalService,
  NotificationsService,
  StockService,
} from '../../../../../shared/services';
import { IStockMutationDto } from '../../../../../shared/dto';
import { ProductTypeEnum } from '../../../../../shared/enums';
import { SpinnerComponent } from '../../../../../shared/components/spinner/spinner.component';
import { IProduct } from '../../../../../shared/interfaces';
import { Store } from '@ngrx/store';
import * as ProductActions from '../../../../../shared/store/stock/stock-product.actions';
import { CancelButtonComponent } from '../../../../../shared/components/cancel-button/cancel-button.component';
import { ModalCloseDirective } from '../../../../../shared/directives/modal-close.directive';
import { MessageComponent } from '../../../../../shared/components/message/message.component';

@Component({
  selector: 'stock-product-update',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SpinnerComponent,
    CancelButtonComponent,
    ModalCloseDirective,
    MessageComponent,
  ],
  templateUrl: './stock-update.component.html',
  styleUrls: ['./stock-update.component.scss'],
})
export class StockUpdateComponent implements OnInit {
  @Input() productType!: ProductTypeEnum;
  @Input() product!: IProduct;
  @Input() user!: User;

  @ViewChild('quantityInput') quantityInput!: ElementRef<HTMLInputElement>;

  public error: string | null = null;
  public isLoading = false;
  public form = this._fb.group({
    quantity: [0, Validators.required],
  });

  constructor(
    private _fb: FormBuilder,
    private _stockService: StockService,
    private _modalService: ModalService,
    private _store: Store,
    private _notificationService: NotificationsService,
  ) {}

  ngOnInit() {
    this.form.controls.quantity.setValue(this.product.freeStock);

    setTimeout(() => {
      this.quantityInput.nativeElement.focus();
    });
  }

  public onSubmit(): void {
    const quantity = this.form.controls.quantity.value;

    if (quantity === null || quantity === undefined) {
      this.error = 'Merci de renseigner une quantité';
      return;
    }

    if (quantity && quantity < 0) {
      this.error = 'Quantité incorrecte';
      return;
    }

    if (quantity === this.product.freeStock) {
      this.error = `Vous n'avez pas modifié la quantité`;
    }

    this.error = null;
    this.isLoading = true;
    let add = 0;
    let subtract = 0;

    if (quantity > this.product.freeStock) {
      add = (quantity - this.product.freeStock) * this.product.packStockUnits;
      subtract = 0;
    } else {
      add = 0;
      subtract =
        (this.product.freeStock - quantity) * this.product.packStockUnits;
    }

    const dto: IStockMutationDto = {
      employee_number: this.user.mp.employeeNumber,
      data: {
        add,
        subtract,
        description: `Mise à jour du stock par ${this.user.completeName}`,
      },
    };

    this._stockService
      .createMutation(this.productType, this.product.id, dto)
      .subscribe({
        next: () => {
          this._store.dispatch(
            ProductActions.setLastMutation({
              mutationInfo: {
                productType: this.productType,
                productId: this.product.id,
                action: 'update',
                stockMutationDto: dto,
              },
            }),
          );
          this._notificationService.displaySuccess(
            'Stock produit mis à jour !',
          );
          this._modalService.close({ result: 'success' });
        },
        error: (err) => {
          this.error = err.error.message;
          this.isLoading = false;
        },
      });
  }
}
