import { Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IProduct, ISelectItem } from '../../../../../shared/interfaces';
import {
  ModalService,
  NotificationsService,
  StockProductGenericNamesService,
  StockService,
} from '../../../../../shared/services';
import { map, Observable } from 'rxjs';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { SelectComponent } from '../../../../../shared/components/select/select.component';
import { ProductTypeEnum } from '../../../../../shared/enums';
import { MessageComponent } from '../../../../../shared/components/message/message.component';
import { CancelButtonComponent } from '../../../../../shared/components/cancel-button/cancel-button.component';
import { ModalCloseDirective } from '../../../../../shared/directives/modal-close.directive';
import { SaveButtonComponent } from '../../../../../shared/components/save-button/save-button.component';

@Component({
  selector: 'app-generic-name',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SelectComponent,
    MessageComponent,
    CancelButtonComponent,
    ModalCloseDirective,
    SaveButtonComponent,
  ],
  templateUrl: './generic-name.component.html',
  styleUrls: ['./generic-name.component.scss'],
})
export class GenericNameComponent implements OnInit {
  @Input() productType!: ProductTypeEnum;
  @Input() product!: IProduct;

  public error: string | null = null;
  public list$: Observable<ISelectItem[]>;
  public form = this._fb.group({
    name: [''],
  });

  constructor(
    private _productNameService: StockProductGenericNamesService,
    private _stockService: StockService,
    private _modalService: ModalService,
    private _fb: FormBuilder,
    private _notificationService: NotificationsService,
  ) {
    this.list$ = this._productNameService.findAll().pipe(
      map((list) => {
        return list.map((item) => {
          return {
            value: item._id,
            label: item.name,
          };
        });
      }),
    );
  }

  ngOnInit() {
    if (this.product.genericName) {
      this.form.controls.name.setValue(this.product.genericName._id);
    }
  }

  public onSubmit(): void {
    const name = this.form.controls.name.value;

    this._stockService
      .updateGenericName(this.productType, this.product.id, name)
      .subscribe({
        next: () => {
          this._notificationService.displaySuccess('Nom générique modifié !');
          this._modalService.close();
        },
        error: (err) => {
          this.error = err.error.message;
        },
      });
  }
}
