import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GenericNameComponent } from './generic-name.component';

describe('GenericNameComponent', () => {
  let component: GenericNameComponent;
  let fixture: ComponentFixture<GenericNameComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [GenericNameComponent]
    });
    fixture = TestBed.createComponent(GenericNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
