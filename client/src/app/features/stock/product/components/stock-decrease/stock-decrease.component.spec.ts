import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StockDecreaseComponent } from './stock-decrease.component';

describe('DecreaseComponent', () => {
  let component: StockDecreaseComponent;
  let fixture: ComponentFixture<StockDecreaseComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [StockDecreaseComponent],
    });
    fixture = TestBed.createComponent(StockDecreaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
