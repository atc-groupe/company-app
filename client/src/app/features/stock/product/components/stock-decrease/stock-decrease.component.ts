import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  FormBuilder,
  FormControl,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { SpinnerComponent } from '../../../../../shared/components/spinner/spinner.component';
import { User } from '../../../../../shared/entities';
import {
  ModalService,
  NotificationsService,
  StockService,
} from '../../../../../shared/services';
import { IStockMutationDto } from '../../../../../shared/dto';
import { ProductTypeEnum } from '../../../../../shared/enums';
import { Subscription } from 'rxjs';
import { IProduct } from '../../../../../shared/interfaces';
import { Store } from '@ngrx/store';
import * as ProductActions from '../../../../../shared/store/stock/stock-product.actions';
import { MessageComponent } from '../../../../../shared/components/message/message.component';
import { CancelButtonComponent } from '../../../../../shared/components/cancel-button/cancel-button.component';
import { ModalCloseDirective } from '../../../../../shared/directives/modal-close.directive';

@Component({
  selector: 'stock-product-decrease',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SpinnerComponent,
    MessageComponent,
    CancelButtonComponent,
    ModalCloseDirective,
  ],
  templateUrl: './stock-decrease.component.html',
  styleUrls: ['./stock-decrease.component.scss'],
})
export class StockDecreaseComponent implements OnInit {
  @Input() productType!: ProductTypeEnum;
  @Input() product!: IProduct;
  @Input() user!: User;

  @ViewChild('quantityInput') quantityInput!: ElementRef<HTMLInputElement>;

  public error: string | null = null;
  public isLoading = false;
  public form = this._fb.group({
    quantity: new FormControl<number | null>(null, [Validators.required]),
    jobNumber: new FormControl<number | null>(null),
    reason: [''],
  });

  private _subscription = new Subscription();

  constructor(
    private _fb: FormBuilder,
    private _stockService: StockService,
    private _modalService: ModalService,
    private _store: Store,
    private _notificationService: NotificationsService,
  ) {}

  ngOnInit() {
    this.form.controls.quantity.setValidators([
      Validators.required,
      Validators.max(this.product.freeStock),
    ]);

    this.form.controls.quantity.valueChanges.subscribe((value) => {
      if (value && value > this.product.freeStock) {
        this.form.get('quantity')?.setValue(this.product.freeStock);
      }
    });

    setTimeout(() => {
      this.quantityInput.nativeElement.focus();
    });
  }

  public onSubmit(): void {
    const quantity = this.form.controls.quantity.value;
    const jobNumber = this.form.controls.jobNumber.value;
    const reason = this.form.controls.reason.value;

    if (!quantity) {
      this.error = 'Merci de renseigner une quantité';
      return;
    }

    if (quantity > this.product.freeStock) {
      this.error =
        'Attention, vous tentez de déstocker plus que la quantité en stock';
      return;
    }

    this.error = null;
    this.isLoading = true;
    const description = `Déstockage par ${this.user.completeName}`;

    const dto: IStockMutationDto = {
      employee_number: this.user.mp.employeeNumber,
      data: {
        add: 0,
        subtract: quantity * this.product.packStockUnits,
        description: reason ? `${description}. ${reason}` : description,
      },
    };

    if (jobNumber) {
      dto.data.job_number = jobNumber;
    }

    this._stockService
      .createMutation(this.productType, this.product.id, dto)
      .subscribe({
        next: () => {
          this._store.dispatch(
            ProductActions.setLastMutation({
              mutationInfo: {
                productType: this.productType,
                productId: this.product.id,
                action: 'decrease',
                stockMutationDto: dto,
              },
            }),
          );
          this._notificationService.displaySuccess('Produit déstocké !');
          this._modalService.close({ result: 'success' });
        },
        error: (err) => {
          this.error = err.error.message;
          this.isLoading = false;
        },
      });
  }
}
