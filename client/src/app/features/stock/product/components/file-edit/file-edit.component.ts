import {
  Component,
  ElementRef,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { IProduct } from '../../../../../shared/interfaces';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import {
  ModalService,
  StockFilesService,
} from '../../../../../shared/services';
import { IAppFileDto, IAppFileInfoDto } from '../../../../../shared/dto';
import { SpinnerComponent } from '../../../../../shared/components/spinner/spinner.component';
import { Store } from '@ngrx/store';
import { displaySuccessNotificationAction } from '../../../../../shared/store/ui';
import { Subscription } from 'rxjs';
import { IAppFile } from '../../../../../shared/interfaces/i-app-file';
import { MessageComponent } from '../../../../../shared/components/message/message.component';
import { CancelButtonComponent } from '../../../../../shared/components/cancel-button/cancel-button.component';
import { ModalCloseDirective } from '../../../../../shared/directives/modal-close.directive';
import { SaveButtonComponent } from '../../../../../shared/components/save-button/save-button.component';

@Component({
  selector: 'app-file-edit',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SpinnerComponent,
    MessageComponent,
    CancelButtonComponent,
    ModalCloseDirective,
    SaveButtonComponent,
  ],
  templateUrl: './file-edit.component.html',
  styleUrls: ['./file-edit.component.scss'],
})
export class FileEditComponent implements OnInit, OnDestroy {
  @Input() product!: IProduct;
  @Input() action!: 'create' | 'update' | 'updateInfo';
  @Input() file: IAppFile | null = null;

  @ViewChild('file') fileRef!: ElementRef<HTMLInputElement>;

  public error: string | null = null;
  public fileName: string | null = null;
  public uploadProgress: number | null = null;
  private _file: File | null = null;
  private _subscription = new Subscription();

  public form = this._fb.group({
    name: ['', Validators.required],
  });

  constructor(
    private _fb: FormBuilder,
    private _modalService: ModalService,
    private _stockFilesService: StockFilesService,
    private _store: Store,
  ) {}

  ngOnInit() {
    if (this.action === 'updateInfo' && this.file) {
      this.form.controls.name.setValue(this.file.name);
    }
  }

  public onClose(): void {
    this._modalService.close();
  }

  public onFileChange(): void {
    this._file = this.fileRef.nativeElement.files
      ? this.fileRef.nativeElement.files[0]
      : null;

    if (this._file) {
      this.fileName = this._file.name;

      if (!this.form.controls.name.value) {
        this.form.get('name')?.setValue(this.fileName.split('.')[0]);
      }
    }
  }

  public onSubmit(): void {
    const name = this.form.controls.name.value;

    if (!name && this.action !== 'update') {
      this.error = 'Merci de renseigner un nom';
      return;
    }

    if (!this._file && this.action !== 'updateInfo') {
      this.error = 'Merci de sélectionner un fichier';
      return;
    }

    if (this.action === 'create') {
      const dto: IAppFileDto = {
        name: name!,
        file: this._file!,
      };

      this._subscription = this._stockFilesService
        .addFile(this.product.productType, this.product.id, dto)
        .subscribe({
          next: (result) => {
            if (result && result < 100) {
              this.uploadProgress = result;
            }
            if (result === 100) {
              setTimeout(() => {
                this._store.dispatch(
                  displaySuccessNotificationAction({
                    message: 'Fichier ajouté!',
                  }),
                );
                this._modalService.close();
              }, 300);
            }
          },
          error: (err) => {
            this.error = err.error.message;
            this._subscription.unsubscribe();
          },
        });
    }

    if (!this.file) {
      return;
    }

    if (this.action === 'update') {
      this._subscription = this._stockFilesService
        .updateFile(
          this.product.productType,
          this.product.id,
          this.file._id,
          this._file!,
        )
        .subscribe({
          next: (result) => {
            if (result && result < 100) {
              this.uploadProgress = result;
            }

            if (result === 100) {
              setTimeout(() => {
                this._store.dispatch(
                  displaySuccessNotificationAction({
                    message: 'Fichier modifié!',
                  }),
                );
                this._modalService.close();
              }, 300);
            }
          },
          error: (err) => {
            this.error = err.error.message;
            this._subscription.unsubscribe();
          },
        });
    }

    if (this.action === 'updateInfo') {
      const dto: IAppFileInfoDto = { name: name! };
      this._stockFilesService
        .updateFileInfo(
          this.product.productType,
          this.product.id,
          this.file._id,
          dto,
        )
        .subscribe({
          next: () => {
            this._store.dispatch(
              displaySuccessNotificationAction({
                message: 'Fichier renommé!',
              }),
            );
            this._modalService.close();
          },
          error: (err) => {
            this.error = err.error.message;
          },
        });
    }
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
