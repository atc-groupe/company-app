import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IProduct, IStockMutationInfo } from '../../../../../shared/interfaces';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'stock-product-info',
  standalone: true,
  imports: [CommonModule, RouterLink],
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss'],
})
export class InfoComponent {
  @Input() product: IProduct | null = null;
  @Input() canDecreaseStock = false;
  @Input() canUpdateStock = false;
  @Input() canUpdateSettings = false;
  @Input() lastMutation: IStockMutationInfo | null = null;

  @Output() cancelLastMutation = new EventEmitter<void>();
  @Output() decreaseStock = new EventEmitter<void>();
  @Output() updateStock = new EventEmitter<void>();
  @Output() updateGenericName = new EventEmitter<void>();

  get lastMutationLabel(): string | null {
    if (!this.lastMutation) {
      return null;
    }

    return this.lastMutation.action === 'update'
      ? 'Annuler la dernière modification'
      : 'Annuler le dernier déstockage';
  }

  get displayCancelButton(): boolean {
    if (!this.lastMutation) {
      return false;
    }

    return this.lastMutation.action === 'update'
      ? this.canUpdateStock
      : this.canUpdateStock;
  }

  get displayDecreaseButton(): boolean {
    if (!this.product || !this.product.freeStock) {
      return false;
    }

    return this.canDecreaseStock;
  }

  get displayUpdateGenericNameButton(): boolean {
    return this.canUpdateSettings;
  }

  get displayManageStockButton(): boolean {
    return this.canUpdateStock;
  }

  public onDecreaseStock(): void {
    this.decreaseStock.emit();
  }

  public onUpdateStock(): void {
    this.updateStock.emit();
  }

  public onCancelLastMutation(): void {
    this.cancelLastMutation.emit();
  }

  public onUpdateGenericName(): void {
    this.updateGenericName.emit();
  }
}
