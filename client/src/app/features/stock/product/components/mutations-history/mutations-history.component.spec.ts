import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MutationsHistoryComponent } from './mutations-history.component';

describe('MutationsHistoryComponent', () => {
  let component: MutationsHistoryComponent;
  let fixture: ComponentFixture<MutationsHistoryComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MutationsHistoryComponent]
    });
    fixture = TestBed.createComponent(MutationsHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
