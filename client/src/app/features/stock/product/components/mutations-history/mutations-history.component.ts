import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IMpStockMutation } from '../../../../../shared/interfaces';
import { MutationHistoryItemComponent } from '../mutation-history-item/mutation-history-item.component';
import { SpinnerComponent } from '../../../../../shared/components/spinner/spinner.component';

@Component({
  selector: 'stock-product-mutations-history',
  standalone: true,
  imports: [CommonModule, MutationHistoryItemComponent, SpinnerComponent],
  templateUrl: './mutations-history.component.html',
  styleUrls: ['./mutations-history.component.scss'],
})
export class MutationsHistoryComponent {
  @Input() productStockUnit: string | null = null;
  @Input() packStockUnits: number | null = null;
  @Input() mutations: IMpStockMutation[] | null = null;
  @Input() isLoading: boolean = true;
  @Input() error: string | null = null;

  get displayEmptyMessage(): boolean {
    if (this.isLoading) {
      return false;
    }

    if (this.mutations && this.mutations.length) {
      return false;
    }

    return true;
  }
}
