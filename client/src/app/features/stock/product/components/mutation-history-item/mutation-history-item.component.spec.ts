import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MutationHistoryItemComponent } from './mutation-history-item.component';

describe('MutationHistoryItemComponent', () => {
  let component: MutationHistoryItemComponent;
  let fixture: ComponentFixture<MutationHistoryItemComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MutationHistoryItemComponent]
    });
    fixture = TestBed.createComponent(MutationHistoryItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
