import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IMpStockMutation } from '../../../../../shared/interfaces';

@Component({
  selector: 'stock-product-mutation-history-item',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './mutation-history-item.component.html',
  styleUrls: ['./mutation-history-item.component.scss'],
})
export class MutationHistoryItemComponent {
  @Input() mutation: IMpStockMutation | null = null;
  @Input() productStockUnit: string | null = null;
  @Input() productPackStockUnits: number | null = null;

  get mutationLabel(): string | null {
    if (!this.mutation || !this.productPackStockUnits) {
      return null;
    }

    return this.mutation.add
      ? `+ ${this.mutation.add / this.productPackStockUnits} ${
          this.productStockUnit
        }`
      : `- ${this.mutation.subtract / this.productPackStockUnits} ${
          this.productStockUnit
        }`;
  }

  get mutationChipStyle(): string | null {
    if (!this.mutation) {
      return null;
    }

    return this.mutation.add ? 'chip-success' : 'chip-error';
  }

  get jobNumber(): number | null {
    if (!this.mutation || !this.mutation.job_number) {
      return null;
    }

    return this.mutation.job_number;
  }
}
