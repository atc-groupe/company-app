import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IProduct } from '../../../../../shared/interfaces';

@Component({
  selector: 'product-stock-info',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './stock-info.component.html',
  styleUrls: ['./stock-info.component.scss'],
})
export class StockInfoComponent {
  @Input() product: IProduct | null = null;
}
