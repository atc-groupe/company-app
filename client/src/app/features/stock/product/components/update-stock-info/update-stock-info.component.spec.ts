import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateStockInfoComponent } from './update-stock-info.component';

describe('UpdateStockInfoComponent', () => {
  let component: UpdateStockInfoComponent;
  let fixture: ComponentFixture<UpdateStockInfoComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [UpdateStockInfoComponent]
    });
    fixture = TestBed.createComponent(UpdateStockInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
