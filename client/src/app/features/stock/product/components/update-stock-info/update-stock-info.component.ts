import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilder, FormControl, ReactiveFormsModule } from '@angular/forms';
import { ModalService, StockService } from '../../../../../shared/services';
import { Article } from '../../../../../shared/entities';
import { CancelButtonComponent } from '../../../../../shared/components/cancel-button/cancel-button.component';
import { ModalCloseDirective } from '../../../../../shared/directives/modal-close.directive';
import { SaveButtonComponent } from '../../../../../shared/components/save-button/save-button.component';

@Component({
  selector: 'article-update-stock-info',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CancelButtonComponent,
    ModalCloseDirective,
    SaveButtonComponent,
  ],
  templateUrl: './update-stock-info.component.html',
  styleUrls: ['./update-stock-info.component.scss'],
})
export class UpdateStockInfoComponent implements OnInit {
  @Input() article!: Article;
  @ViewChild('quantityInput') quantityInput!: ElementRef<HTMLInputElement>;

  public error: string | null = null;
  public form = this._fb.group({
    quantity: new FormControl<number | null>(null),
    label: [''],
  });

  constructor(
    private _fb: FormBuilder,
    private _stockService: StockService,
    private _modalService: ModalService,
  ) {}

  ngOnInit(): void {
    if (this.article.appStockUnitNumber) {
      this.form.controls.quantity.setValue(this.article.appStockUnitNumber);
    }

    if (this.article.appStockUnitLabel) {
      this.form.controls.label.setValue(this.article.appStockUnitLabel);
    }

    setTimeout(() => {
      this.quantityInput.nativeElement.focus();
    });
  }

  public onSubmit(): void {
    const quantity = this.form.controls['quantity'].value;
    const label = this.form.controls['label'].value;

    this._stockService
      .updateArticleStockInfo(this.article.id, {
        stockUnitNumber: quantity,
        stockUnitLabel: label !== '' ? label : null,
      })
      .subscribe({
        next: () => this._modalService.close({ result: 'success' }),
        error: (err) => {
          this.error = err.error.message;
        },
      });
  }
}
