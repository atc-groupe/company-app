import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Article } from '../../../../../shared/entities';

@Component({
  selector: 'stock-article-info',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './article-info.component.html',
  styleUrls: ['./article-info.component.scss'],
})
export class ArticleInfoComponent {
  @Input() article: Article | null = null;
  @Input() canUpdateStockInfo: boolean = false;
  @Output() updateStockInfo = new EventEmitter<void>();

  public onUpdateStockInfo(): void {
    this.updateStockInfo.emit();
  }
}
