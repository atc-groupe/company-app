import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Paper } from '../../../../../shared/entities';

@Component({
  selector: 'stock-paper-info',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './paper-info.component.html',
  styleUrls: ['./paper-info.component.scss'],
})
export class PaperInfoComponent {
  @Input() paper: Paper | null = null;
}
