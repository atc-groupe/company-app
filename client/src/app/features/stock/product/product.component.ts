import { Component, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Article, Paper, User } from '../../../shared/entities';
import {
  IMpStockMutation,
  IProduct,
  IStockMutationInfo,
} from '../../../shared/interfaces';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import {
  AlertService,
  ModalService,
  NotificationsService,
  StockFilesService,
  StockService,
  WsStockProductService,
} from '../../../shared/services';
import { ActivatedRoute } from '@angular/router';
import * as ProductActions from '../../../shared/store/stock/stock-product.actions';
import * as ProductSelectors from '../../../shared/store/stock/stock-product.selectors';
import { selectUser } from '../../../shared/store/user/user.selectors';
import {
  AuthActionsProductsEnum,
  AuthSubjectsEnum,
  ProductTypeEnum,
} from '../../../shared/enums';
import { StockDecreaseComponent } from './components/stock-decrease/stock-decrease.component';
import { StockUpdateComponent } from './components/stock-update/stock-update.component';
import { UpdateStockInfoComponent } from './components/update-stock-info/update-stock-info.component';
import { MutationsHistoryComponent } from './components/mutations-history/mutations-history.component';
import { SpinnerComponent } from '../../../shared/components/spinner/spinner.component';
import { InfoComponent } from './components/info/info.component';
import { ArticleInfoComponent } from './components/article-info/article-info.component';
import { StockInfoComponent } from './components/stock-info/stock-info.component';
import { PaperInfoComponent } from './components/paper-info/paper-info.component';
import {
  displayErrorNotificationAction,
  displaySuccessNotificationAction,
} from '../../../shared/store/ui';
import { IStockMutationDto } from '../../../shared/dto';
import { GenericNameComponent } from './components/generic-name/generic-name.component';
import { FilesComponent } from './components/files/files.component';
import { FileEditComponent } from './components/file-edit/file-edit.component';
import { IAppFile } from '../../../shared/interfaces/i-app-file';

@Component({
  selector: 'app-product',
  standalone: true,
  imports: [
    CommonModule,
    InfoComponent,
    MutationsHistoryComponent,
    SpinnerComponent,
    InfoComponent,
    ArticleInfoComponent,
    StockInfoComponent,
    InfoComponent,
    PaperInfoComponent,
    FilesComponent,
  ],
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
})
export class ProductComponent implements OnInit, OnDestroy {
  public error: string | null = null;
  public productType: ProductTypeEnum | null = null;
  public productId: number | null = null;
  public product: IProduct | null = null;
  public isLoading = false;
  public user: User | null = null;
  public canDecreaseStock = false;
  public canUpdateStock = false;
  public canUpdateProductSettings = false;
  public canManageDocuments = false;
  public mutations: IMpStockMutation[] | null = null;
  public mutationsError: string | null = null;
  public mutationsLoading = false;
  public lastMutation: IStockMutationInfo | null = null;
  private _subscription = new Subscription();

  constructor(
    private _store: Store,
    private _wsProduct: WsStockProductService,
    private _stockService: StockService,
    private _activatedRoute: ActivatedRoute,
    private _modalService: ModalService,
    private _alertService: AlertService,
    private _stockFilesService: StockFilesService,
    private _notificationService: NotificationsService,
  ) {}

  ngOnInit() {
    this._wsProduct.connect();

    this._subscription.add(
      this._wsProduct.onConnect().subscribe(() => {
        const productTypeAsString =
          this._activatedRoute.snapshot.paramMap.get('productType');
        const idAsString = this._activatedRoute.snapshot.paramMap.get('id');

        if (!idAsString || productTypeAsString === null) {
          return;
        }

        const id = parseInt(idAsString);

        let productType: ProductTypeEnum;
        switch (productTypeAsString) {
          case 'articles':
            productType = ProductTypeEnum.Article;
            break;
          case 'papers':
            productType = ProductTypeEnum.Paper;
            break;
          default:
            throw new Error(`Unsupported product type ${productTypeAsString}`);
        }

        this.productId = id;
        this.productType = productType;

        this._store.dispatch(
          ProductActions.tryFetchProductAction({ productType, id }),
        );
      }),
    );

    this._subscription.add(
      this._wsProduct.onConnectError().subscribe((err) => {
        this._store.dispatch(
          ProductActions.productErrorAction({ error: err.message }),
        );
      }),
    );

    this._subscription.add(
      this._store
        .select(ProductSelectors.selectProduct)
        .subscribe((product) => {
          this.product = product;

          if (product) {
            this._fetchMutations();
          }
        }),
    );

    this._subscription.add(
      this._store
        .select(ProductSelectors.selectError)
        .subscribe((error) => (this.error = error)),
    );

    this._subscription.add(
      this._store
        .select(ProductSelectors.selectIsLoading)
        .subscribe((isLoading) => (this.isLoading = isLoading)),
    );

    this._subscription.add(
      this._store
        .select(ProductSelectors.selectLastMutation)
        .subscribe((info) => {
          if (!info) {
            this.lastMutation = null;
            return;
          }

          if (
            info.productType === this.productType &&
            info.productId === this.productId
          ) {
            this.lastMutation = info;
          }
        }),
    );

    this._subscription.add(
      this._store.select(selectUser).subscribe((user) => {
        this.user = user;

        if (user) {
          this.canDecreaseStock =
            user.canActivate({
              subject: AuthSubjectsEnum.Products,
              action: AuthActionsProductsEnum.DecreaseStock,
            }) ||
            user.canActivate({
              subject: AuthSubjectsEnum.Products,
              action: AuthActionsProductsEnum.ManageStock,
            });

          this.canUpdateStock =
            user.canActivate({
              subject: AuthSubjectsEnum.Products,
              action: AuthActionsProductsEnum.UpdateStock,
            }) ||
            user.canActivate({
              subject: AuthSubjectsEnum.Products,
              action: AuthActionsProductsEnum.ManageStock,
            });

          this.canUpdateProductSettings = user.canActivate({
            subject: AuthSubjectsEnum.Products,
            action: AuthActionsProductsEnum.UpdateStockSettings,
          });

          this.canManageDocuments = user.canActivate({
            subject: AuthSubjectsEnum.Products,
            action: AuthActionsProductsEnum.ManageDocuments,
          });
        }
      }),
    );
  }

  public get article(): Article | null {
    if (!this.product || this.productType !== ProductTypeEnum.Article) {
      return null;
    }

    return this.product as Article;
  }

  public get paper(): Paper | null {
    if (!this.product || this.productType !== ProductTypeEnum.Paper) {
      return null;
    }

    return this.product as Paper;
  }

  public onDecreaseStock(): void {
    if (!this.canDecreaseStock) {
      return;
    }

    this._modalService
      .open(StockDecreaseComponent, {
        inputs: {
          productType: this.productType,
          product: this.product,
          user: this.user,
        },
      })
      .afterClosed$.subscribe(({ result }: { result?: 'success' }) => {
        if (result === 'success') {
          this._fetchMutations();
        }
      });
  }

  public onCancelLastMutation(): void {
    if (
      !this.lastMutation ||
      !this.user ||
      !this.productType ||
      !this.productId
    ) {
      console.log('here');
      return;
    }

    if (this.lastMutation.action === 'update' && !this.canUpdateStock) {
      return;
    }

    if (this.lastMutation.action === 'decrease' && !this.canDecreaseStock) {
      return;
    }

    this._alertService
      .open(
        'Annulation',
        'êtes vous sure de vouloir annuler la dernière mutation de stock ?',
        {
          canCancel: true,
          canClickOutside: true,
          confirmLabel: 'Valider',
          style: 'error',
        },
      )
      .afterClosed$.subscribe((result) => {
        if (result === 'confirm') {
          const dto: IStockMutationDto = {
            ...this.lastMutation!.stockMutationDto,
            data: { ...this.lastMutation!.stockMutationDto.data },
          };

          dto.data.description = `Annulation dernière opération par ${
            this.user!.completeName
          }`;

          if (dto.data.add) {
            dto.data.subtract = dto.data.add;
            dto.data.add = 0;
          } else if (dto.data.subtract) {
            dto.data.add = dto.data.subtract;
            dto.data.subtract = 0;
          }

          this._stockService
            .createMutation(this.productType!, this.productId!, dto)
            .subscribe({
              next: () => {
                this._store.dispatch(ProductActions.unsetLastMutation());
                this._notificationService.displaySuccess('Opération annulée !');
              },
              error: (err) => {
                this._notificationService.displayError(
                  `Erreur: ${err.error.message}`,
                );
              },
            });
        }
      });
  }

  public onUpdateStock(): void {
    if (!this.canUpdateStock) {
      return;
    }

    this._modalService
      .open(StockUpdateComponent, {
        inputs: {
          productType: this.productType,
          product: this.product,
          user: this.user,
        },
      })
      .afterClosed$.subscribe(({ result }: { result?: 'success' }) => {
        if (result === 'success') {
          this._fetchMutations();
        }
      });
  }

  public onUpdateStockInfo(): void {
    if (
      this.productType !== ProductTypeEnum.Article ||
      !this.canUpdateProductSettings
    ) {
      return;
    }

    this._modalService
      .open(UpdateStockInfoComponent, {
        inputs: { article: this.product },
        options: { fullScreen: true },
      })
      .afterClosed$.subscribe(({ result }: { result?: 'success' }) => {
        if (result === 'success') {
          this._fetchMutations();
        }
      });
  }

  public onUpdateGenericName(): void {
    if (!this.canUpdateProductSettings || !this.product) {
      return;
    }

    this._modalService.open(GenericNameComponent, {
      inputs: { product: this.product, productType: this.productType },
      options: { fullScreen: true },
    });
  }

  public onAddDocument(): void {
    if (!this.product || !this.canManageDocuments) {
      return;
    }

    this._modalService.open(FileEditComponent, {
      inputs: { product: this.product, action: 'create' },
      options: { yPos: 'center' },
    });
  }

  public onUpdateDocument(file: IAppFile): void {
    if (!this.product || !this.canManageDocuments) {
      return;
    }

    this._modalService.open(FileEditComponent, {
      inputs: { product: this.product, action: 'update', file },
      options: { yPos: 'center' },
    });
  }

  public onUpdateDocumentInfo(file: IAppFile): void {
    if (!this.product || !this.canManageDocuments) {
      return;
    }

    this._modalService.open(FileEditComponent, {
      inputs: { product: this.product, action: 'updateInfo', file },
      options: { yPos: 'center' },
    });
  }

  public onDownloadDocument(file: IAppFile): void {
    this._stockFilesService.getFile(file).subscribe((blob) => {
      const a = document.createElement('a');
      a.download = `${file.name}${file.extension}`;
      a.href = URL.createObjectURL(blob);
      a.click();
      URL.revokeObjectURL(a.href);
    });
  }

  public onDeleteDocument(file: IAppFile): void {
    if (
      !this.productId ||
      this.productType === undefined ||
      !this.canManageDocuments
    ) {
      return;
    }

    this._alertService
      .open(
        'Supprimer un fichier',
        `Etes vous sûre de vouloir supprimer le fichier ${file.name}${file.extension}`,
        {
          style: 'error',
          confirmLabel: 'Supprimer',
        },
      )
      .afterClosed$.subscribe((result) => {
        if (result === 'confirm') {
          this._stockFilesService
            .removeFile(this.productType!, this.productId!, file._id)
            .subscribe({
              next: () =>
                this._notificationService.displaySuccess('Fichier supprimé'),
              error: () =>
                this._notificationService.displayError(
                  'Impossible de supprimer le fichier',
                ),
            });
        }
      });
  }

  ngOnDestroy(): void {
    this._subscription.unsubscribe();
    this._wsProduct.disconnect();
  }

  private _fetchMutations(): void {
    if (!this.productId || this.productType === null) {
      return;
    }

    this.mutationsError = null;
    this.mutationsLoading = true;
    this.mutations = null;

    this._stockService
      .getProductMutations(this.productType, this.productId)
      .subscribe({
        next: (mutations) => (this.mutations = mutations),
        error: (err) => (this.error = err.error.message),
        complete: () => (this.mutationsLoading = false),
      });
  }
}
