import { Component, OnDestroy } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, RouterOutlet } from '@angular/router';
import {
  IPlanningCardJobInfo,
  IPlanningFilters,
  IPlanningMachineCards,
  ISortColumn,
} from '../../shared/interfaces';
import { PlanningSetup, User } from '../../shared/entities';
import * as PlanningSelectors from '../../shared/store/planning/planning.selectors';
import { IAppSettingsPlanningView } from '../../shared/interfaces/app-settings/i-app-settings-planning-view';
import { filter, first, Subscription } from 'rxjs';
import { ModalService, WsPlanningService } from '../../shared/services';
import { Store } from '@ngrx/store';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import * as PlanningActions from '../../shared/store/planning/planning.actions';
import { selectUser } from '../../shared/store/user/user.selectors';
import {
  AuthActionsJobsEnum,
  AuthSubjectsEnum,
  PlanningModeEnum,
  PlanningPeriodEnum,
} from '../../shared/enums';
import { selectAppSettings } from '../../shared/store/app-settings/app-settings.selectors';
import { FiltersComponent } from './components/filters/filters.component';
import { JobCommentEditComponent } from './components/job-comment-edit/job-comment-edit.component';
import { JobStatusEditComponent } from './components/job-status-edit/job-status-edit.component';
import * as JobActions from '../../shared/store/job/job.actions';
import { DataCardComponent } from './components/data-card/data-card.component';
import { DataHeadingComponent } from '../../shared/components/data-heading/data-heading.component';
import { DataLineComponent } from './components/data-line/data-line.component';
import { HeaderComponent } from './components/header/header.component';
import { HideDirective } from '../../shared/directives/hide.directive';
import { MachineHeadingComponent } from './components/machine-heading/machine-heading.component';
import { MessageComponent } from './components/message/message.component';
import { ShowDirective } from '../../shared/directives/show.directive';
import { planningListColumns } from './planning-list-columns.constant';
import { EmployeesFilterComponent } from './components/employees-filter/employees-filter.component';

@Component({
  selector: 'app-planning',
  standalone: true,
  imports: [
    CommonModule,
    RouterOutlet,
    DataCardComponent,
    DataHeadingComponent,
    DataLineComponent,
    HeaderComponent,
    HideDirective,
    MachineHeadingComponent,
    MessageComponent,
    ShowDirective,
  ],
  templateUrl: './planning.component.html',
  styleUrls: ['./planning.component.scss'],
})
export class PlanningComponent implements OnDestroy {
  public error: string | null = null;
  public data: IPlanningMachineCards[] | null = null;
  public filters: IPlanningFilters | null = null;
  public isLoading = true;
  public setup: PlanningSetup | null = null;
  public columns = planningListColumns;
  public canUpdateComment = false;
  public canUpdateStatus = false;
  public sortColumn$ = this._store.select(PlanningSelectors.selectSortColumn);
  public date$ = this._store.select(PlanningSelectors.selectDate);
  public search$ = this._store.select(PlanningSelectors.selectSearch);
  public planningViews: IAppSettingsPlanningView[] | null = null;
  public isEmpty = false;

  private _user: User | null = null;
  private _subscription = new Subscription();

  constructor(
    private _wsPlanning: WsPlanningService,
    private _store: Store,
    private _modalService: ModalService,
    private _router: Router,
  ) {
    this._wsPlanning.connect();

    this._wsPlanning
      .onConnect()
      .pipe(takeUntilDestroyed())
      .subscribe(() => {
        this._subscription.add(
          this._store
            .select(PlanningSelectors.selectSetup)
            .pipe(first((v) => v !== null))
            .subscribe((setup) => {
              this.setup = setup;

              if (setup) {
                this._store.dispatch(PlanningActions.trySubscribeDataAction());
              }
            }),
        );
      });

    this._wsPlanning
      .onConnectError()
      .pipe(takeUntilDestroyed())
      .subscribe((err) => {
        this.error = err.message;
      });

    this._wsPlanning
      .onError()
      .pipe(takeUntilDestroyed())
      .subscribe((err) => {
        this.error = err.message;
      });

    this._store
      .select(PlanningSelectors.selectDayData)
      .pipe(
        takeUntilDestroyed(),
        filter((v) => v !== null),
      )
      .subscribe((data) => (this.data = data));

    this._store
      .select(PlanningSelectors.selectFetchError)
      .pipe(takeUntilDestroyed())
      .subscribe((error) => (this.error = error));

    this._store
      .select(PlanningSelectors.selectIsLoading)
      .pipe(takeUntilDestroyed())
      .subscribe((isLoading) => (this.isLoading = isLoading));

    this._store
      .select(PlanningSelectors.selectFilters)
      .pipe(takeUntilDestroyed())
      .subscribe((filters) => (this.filters = filters));

    this._store
      .select(PlanningSelectors.selectIsEmpty)
      .pipe(takeUntilDestroyed())
      .subscribe((isEmpty) => (this.isEmpty = isEmpty));

    this._store
      .select(selectUser)
      .pipe(takeUntilDestroyed())
      .subscribe((user) => {
        this._user = user;

        if (user) {
          this.canUpdateComment = user.canActivate({
            subject: AuthSubjectsEnum.Jobs,
            action: AuthActionsJobsEnum.UpdatePlanningComment,
          });

          this.canUpdateStatus = user.canActivate({
            subject: AuthSubjectsEnum.Jobs,
            action: AuthActionsJobsEnum.UpdateStatus,
          });
        }
      });

    this._store
      .select(selectAppSettings)
      .pipe(
        takeUntilDestroyed(),
        filter((v) => v !== null),
      )
      .subscribe((settings) => (this.planningViews = settings!.planning.views));
  }

  public get hasFilteredEmptyResult(): boolean {
    if (this.isEmpty) {
      return false;
    }

    return this.data!.every((item) => item.cards.length === 0);
  }

  public get displayMessage(): boolean {
    return (
      this.isEmpty ||
      this.hasFilteredEmptyResult ||
      this.isLoading ||
      !!this.error
    );
  }

  public get displayListData(): boolean {
    return !this.displayMessage && this.filters?.mode === 'list';
  }

  public get displayKanbanData(): boolean {
    return !this.displayMessage && this.filters?.mode === 'kanban';
  }

  public canChangeComment(jobStatusNumber: number): boolean {
    if (!this.canUpdateComment) {
      return false;
    }

    return this._user?.isAdmin || jobStatusNumber < 1000;
  }

  public onSetNextDate(): void {
    if (!this.filters) {
      return;
    }

    this._store.dispatch(
      this.filters.period === PlanningPeriodEnum.Day
        ? PlanningActions.setNextDayAction()
        : PlanningActions.setNextWeekAction(),
    );
  }

  public onSetPrevDate(): void {
    if (!this.filters) {
      return;
    }

    this._store.dispatch(
      this.filters.period === PlanningPeriodEnum.Day
        ? PlanningActions.setPrevDayAction()
        : PlanningActions.setPrevWeekAction(),
    );
  }

  public onSetCurrentDate(): void {
    this._store.dispatch(PlanningActions.setCurrentDayAction());
  }

  public onChangeFilters(): void {
    if (!this.filters || !this.setup || !this.planningViews) {
      return;
    }

    this._modalService.open(FiltersComponent, {
      options: { yPos: 'center' },
      inputs: {
        filters: this.filters,
        setup: this.setup,
        views: this.planningViews,
      },
    });
  }

  public onSortData(event: ISortColumn | null): void {
    this._store.dispatch(PlanningActions.sortDataAction({ sortColumn: event }));
  }

  public onStartSearch(): void {
    this._store.dispatch(PlanningActions.startSearchAction());
  }

  public onSearch({ search }: { search: string }): void {
    this._store.dispatch(PlanningActions.trySearchPlannedJobAction({ search }));
  }

  public onClearSearch(): void {
    this._store.dispatch(PlanningActions.clearSearchAction());
  }

  public onEditComment(jobInfo: IPlanningCardJobInfo): void {
    if (
      !this.filters?.view?.planningGroup ||
      !this._user ||
      !this.canChangeComment(jobInfo.statusNumber)
    ) {
      return;
    }

    this._modalService.open(JobCommentEditComponent, {
      options: { yPos: 'center' },
      inputs: {
        jobInfo,
        planningGroup: this.filters.view.planningGroup,
        user: this._user,
      },
    });
  }

  public onChangeStatus(jobInfo: IPlanningCardJobInfo): void {
    if (!this._user || !this.canUpdateStatus) {
      return;
    }

    this._modalService.open(JobStatusEditComponent, {
      inputs: { jobInfo, user: this._user },
      options: { yPos: 'center' },
    });
  }

  public onSetEmployeesFilter(): void {
    if (!this._user || !this.setup || !this.filters) {
      return;
    }

    this._modalService.open(EmployeesFilterComponent, {
      inputs: { user: this._user, setup: this.setup, filters: this.filters },
      options: { yPos: 'center' },
    });
  }

  public onChangeFiltersMode(mode: PlanningModeEnum): void {
    this._store.dispatch(
      PlanningActions.setFiltersAction({ filters: { mode } }),
    );
  }

  public async onViewJob(mpNumber: number): Promise<void> {
    this._store.dispatch(JobActions.setFromAction({ route: '/planning' }));

    await this._router.navigateByUrl(`/jobs/${mpNumber}/view`);
  }

  ngOnDestroy() {
    this._wsPlanning.disconnect();
    this._store.dispatch(PlanningActions.clearDataAction());
    this._subscription.unsubscribe();
  }
}
