import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MachineHeadingComponent } from './machine-heading.component';

describe('MachineHeadingComponent', () => {
  let component: MachineHeadingComponent;
  let fixture: ComponentFixture<MachineHeadingComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MachineHeadingComponent]
    });
    fixture = TestBed.createComponent(MachineHeadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
