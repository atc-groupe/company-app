import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'planning-machine-heading',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './machine-heading.component.html',
  styleUrls: ['./machine-heading.component.scss'],
})
export class MachineHeadingComponent {
  @Input({ required: true }) name!: string;
}
