import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobCommentEditComponent } from './job-comment-edit.component';

describe('JobCommentComponent', () => {
  let component: JobCommentEditComponent;
  let fixture: ComponentFixture<JobCommentEditComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [JobCommentEditComponent],
    });
    fixture = TestBed.createComponent(JobCommentEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
