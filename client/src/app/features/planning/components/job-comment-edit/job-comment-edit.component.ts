import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { User } from '../../../../shared/entities';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JobCommentService, ModalService } from '../../../../shared/services';
import { SpinnerComponent } from '../../../../shared/components/spinner/spinner.component';
import {
  IJobCommentPlanning,
  IPlanningCardJobInfo,
} from '../../../../shared/interfaces';
import { Observable } from 'rxjs';
import { MpListItemPipe } from '../../../../shared/pipes/mp-list-item.pipe';
import { MessageComponent } from '../../../../shared/components/message/message.component';
import { CancelButtonComponent } from '../../../../shared/components/cancel-button/cancel-button.component';
import { ModalCloseDirective } from '../../../../shared/directives/modal-close.directive';
import { SaveButtonComponent } from '../../../../shared/components/save-button/save-button.component';

@Component({
  selector: 'planning-job-comment',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SpinnerComponent,
    MpListItemPipe,
    MessageComponent,
    CancelButtonComponent,
    ModalCloseDirective,
    SaveButtonComponent,
  ],
  templateUrl: './job-comment-edit.component.html',
  styleUrls: ['./job-comment-edit.component.scss'],
})
export class JobCommentEditComponent implements OnInit, AfterViewInit {
  @Input() jobInfo!: IPlanningCardJobInfo;
  @Input() planningGroup!: string;
  @Input() user!: User;
  @ViewChild('comment') comment!: ElementRef<HTMLTextAreaElement>;

  public error: string | null = null;
  public processing = false;
  public form = this._fb.group({
    comment: [''],
  });

  private _comment: IJobCommentPlanning | null = null;

  constructor(
    private _fb: FormBuilder,
    private _modalService: ModalService,
    private _jobCommentService: JobCommentService,
  ) {}

  ngOnInit() {
    this._comment = this.jobInfo.comment;

    this.form
      .get('comment')
      ?.setValue(this._comment ? this._comment.comment : null);
  }

  ngAfterViewInit() {
    this.comment.nativeElement.focus();
  }

  public onSubmit() {
    const comment = this.form.controls.comment.value;

    const parsedComment = comment ? comment.trim() : null;

    if (!parsedComment && !this._comment) {
      return;
    }

    let action$: Observable<any>;

    if (!parsedComment && this._comment) {
      action$ = this._jobCommentService.removePlanningComment(
        this.jobInfo._id,
        this._comment._id,
      );
    }

    if (parsedComment && this._comment) {
      action$ = this._jobCommentService.updatePlanningComment(
        this.jobInfo._id,
        this._comment._id,
        { comment: parsedComment },
      );
    }

    if (parsedComment && !this._comment) {
      action$ = this._jobCommentService.addPlanningComment(this.jobInfo._id, {
        comment: parsedComment,
        planningGroup: this.planningGroup,
        userId: this.user._id,
        userInitials: this.user.initials,
      });
    }

    action$!.subscribe({
      next: () => {
        this.processing = true;
        setTimeout(() => {
          return this._modalService.close({
            result: 'success',
            comment: parsedComment,
          });
        }, 1000);
      },
      error: (err) => (this.error = err.error.message),
    });
  }
}
