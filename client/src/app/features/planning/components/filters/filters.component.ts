import { Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IPlanningFilters, ISelectItem } from '../../../../shared/interfaces';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { SelectComponent } from '../../../../shared/components/select/select.component';
import { BehaviorSubject } from 'rxjs';
import { PlanningSetup } from '../../../../shared/entities';
import { ClickOutsideDirective } from '../../../../shared/directives/click-outside.directive';
import {
  MeService,
  ModalService,
  NotificationsService,
} from '../../../../shared/services';
import { SwitchComponent } from '../../../../shared/components/switch/switch.component';
import { Store } from '@ngrx/store';
import { setFiltersAction } from '../../../../shared/store/planning/planning.actions';
import { PlanningModeEnum, PlanningPeriodEnum } from '../../../../shared/enums';
import { IAppSettingsPlanningView } from '../../../../shared/interfaces/app-settings/i-app-settings-planning-view';
import { MessageComponent } from '../../../../shared/components/message/message.component';
import { CancelButtonComponent } from '../../../../shared/components/cancel-button/cancel-button.component';
import { SaveButtonComponent } from '../../../../shared/components/save-button/save-button.component';

@Component({
  selector: 'planning-filters',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SelectComponent,
    ClickOutsideDirective,
    SwitchComponent,
    MessageComponent,
    CancelButtonComponent,
    SaveButtonComponent,
  ],
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss'],
})
export class FiltersComponent implements OnInit {
  @Input() public filters!: IPlanningFilters;
  @Input() public setup!: PlanningSetup;
  @Input() public views!: IAppSettingsPlanningView[];

  public modes: ISelectItem[] = PlanningSetup.getModesSelectItems();
  public periods: ISelectItem[] = PlanningSetup.getPeriodsSelectItems();
  public views$: BehaviorSubject<ISelectItem[]> = new BehaviorSubject<
    ISelectItem[]
  >([]);
  public error: string | null = null;

  public form: FormGroup = this._fb.group({
    mode: new FormControl<PlanningModeEnum>(
      PlanningModeEnum.List,
      Validators.required,
    ),
    period: new FormControl<PlanningPeriodEnum>(
      PlanningPeriodEnum.Day,
      Validators.required,
    ),
    view: ['', Validators.required],
    hideNotPlanned: [true],
    hideTreated: [true],
    mergeJobOperationCards: [false],
    saveAsDefault: [false],
  });

  constructor(
    private _fb: FormBuilder,
    private _modalService: ModalService,
    private _meService: MeService,
    private _store: Store,
    private _notificationService: NotificationsService,
  ) {}

  ngOnInit() {
    this.views$.next(
      this.views.map((view) => {
        return {
          label: view.name,
          value: view._id,
        };
      }),
    );

    this.form.controls['mode'].setValue(this.filters.mode);
    this.form.controls['period'].setValue(this.filters.period);
    this.form.controls['view'].setValue(this.filters.view?._id);
    this.form.controls['hideNotPlanned'].setValue(this.filters.hideNotPlanned);
    this.form.controls['hideTreated'].setValue(this.filters.hideDone);
    this.form.controls['mergeJobOperationCards'].setValue(
      this.filters.mergeJobOperationCards,
    );
  }

  public onSubmit(): void {
    const mode = this.form.controls['mode'].value;
    const period = this.form.controls['period'].value;
    const viewId = this.form.controls['view'].value;
    const hideNotPlanned = this.form.controls['hideNotPlanned'].value;
    const hideTreated = this.form.controls['hideTreated'].value;
    const mergeJobOperationCards =
      this.form.controls['mergeJobOperationCards'].value;
    const saveAsDefault = this.form.controls['saveAsDefault'].value;

    if (!viewId) {
      this.error = 'Merci de sélectionner une vue';
      return;
    }

    const view = this.views.find((view) => view._id === viewId);

    this._store.dispatch(
      setFiltersAction({
        filters: {
          mode: mode!,
          period: period!,
          view,
          hideNotPlanned: hideNotPlanned ?? false,
          hideDone: hideTreated ?? false,
          mergeJobOperationCards: mergeJobOperationCards ?? false,
        },
      }),
    );

    if (!saveAsDefault) {
      this._modalService.close();
      return;
    }

    this._meService
      .updateSettings({
        planningMode: mode!,
        planningPeriod: period!,
        planningView: viewId,
        planningHideNotPlanned: hideNotPlanned ?? false,
        planningHideDone: hideTreated ?? false,
        planningMergeJobOperationCards: mergeJobOperationCards ?? false,
      })
      .subscribe({
        next: () => {
          this._notificationService.displaySuccess(
            'Vos filtres de planning par défaut sont enregistrés',
          );
          this._modalService.close();
        },
        error: (err) => (this.error = err.error.message),
      });
  }

  public onClose(): void {
    this._modalService.close();
  }
}
