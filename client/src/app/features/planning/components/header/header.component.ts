import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlanningSetup } from '../../../../shared/entities';
import { IPlanningFilters } from '../../../../shared/interfaces';
import { debounceTime, Subscription, tap } from 'rxjs';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SearchComponent } from '../../../../shared/components/search/search.component';
import { PlanningModeEnum, PlanningPeriodEnum } from '../../../../shared/enums';
import { PlanningDateHelperService } from '../../../../shared/services';

@Component({
  selector: 'planning-header',
  standalone: true,
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SearchComponent],
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  @Input() public setup: PlanningSetup | null = null;
  @Input() public filters: IPlanningFilters | null = null;
  @Input() public date: Date | null = null;
  @Input() public stateSearch: string | null = null;

  @Output() private setPrevDate = new EventEmitter<void>();
  @Output() private setNextDate = new EventEmitter<void>();
  @Output() private setCurrentDate = new EventEmitter<void>();
  @Output() private changeFilters = new EventEmitter<void>();
  @Output() private setFiltersMode = new EventEmitter<PlanningModeEnum>();
  @Output() private setEmployeesFilter = new EventEmitter<void>();
  @Output() private startSearch = new EventEmitter<void>();
  @Output() private search = new EventEmitter<{ search: string }>();
  @Output() private clearSearch = new EventEmitter<void>();

  public form = this._fb.group({
    search: [''],
  });

  private _hasSearch = false;
  private _searchStarted = false;
  private _subscription = new Subscription();

  constructor(
    private _fb: FormBuilder,
    private _planningDateHelper: PlanningDateHelperService,
  ) {}

  ngOnInit() {
    this._hasSearch = !!this.stateSearch;
    const search = this.form.controls.search;

    if (!search) {
      return;
    }

    search.setValue(this.stateSearch, { emitEvent: false });

    this._subscription.add(
      this.form.controls.search.valueChanges
        .pipe(
          tap((value) => {
            if (value) {
              this._hasSearch = true;

              if (!this._searchStarted) {
                this.startSearch.emit();
              }
              this._searchStarted = true;
            } else {
              this._hasSearch = false;
              this.clearSearch.emit();
            }
          }),
          debounceTime(800),
        )
        .subscribe((value) => {
          if (value) {
            this.search.emit({ search: value });
          }

          this._searchStarted = false;
        }),
    );
  }

  public get displayDateActions(): boolean {
    return this.filters?.period !== PlanningPeriodEnum.All;
  }

  public get displayDayTitle(): boolean {
    return !this._hasSearch && this.filters?.period === PlanningPeriodEnum.Day;
  }

  public get displayWeekTitle(): boolean {
    return !this._hasSearch && this.filters?.period === PlanningPeriodEnum.Week;
  }

  public get displayAllTitle(): boolean {
    return !this._hasSearch && this.filters?.period === PlanningPeriodEnum.All;
  }

  public get firstWeekDay(): Date {
    return this._planningDateHelper.getFirstWeekDay(this.date!);
  }

  public get lastWeekDay(): Date {
    return this._planningDateHelper.getLastWeekDay(this.date!);
  }

  public get displaySearchTitle(): boolean {
    return this._hasSearch;
  }

  public get isKanbanMode(): boolean {
    return this.filters?.mode === PlanningModeEnum.Kanban;
  }

  public get isListMode(): boolean {
    return this.filters?.mode === PlanningModeEnum.List;
  }

  public onSetPrevDate(): void {
    this.setPrevDate.emit();
  }

  public onSetNextDate(): void {
    this.setNextDate.emit();
  }

  public onSetCurrentDate(): void {
    this.setCurrentDate.emit();
  }

  public onChangeFilters(): void {
    this.changeFilters.emit();
  }

  public onSetKanbanMode() {
    this.setFiltersMode.emit(PlanningModeEnum.Kanban);
  }

  public onSetListMode() {
    this.setFiltersMode.emit(PlanningModeEnum.List);
  }

  public onSetEmployeesFilter() {
    this.setEmployeesFilter.emit();
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
