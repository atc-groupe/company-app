import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JobDevicesComponent } from '../../../../shared/components/job-devices/job-devices.component';
import { JobStatusComponent } from '../../../../shared/components/job-status/job-status.component';
import { MpEmployeePipe } from '../../../../shared/pipes/mp-employee.pipe';
import { MpListItemPipe } from '../../../../shared/pipes/mp-list-item.pipe';
import { PlanningTimePipe } from '../../../../shared/pipes/planning-time.pipe';
import { JobFilesCheckComponent } from '../../../../shared/components/job-files-check/job-files-check.component';
import { MpTimePipe } from '../../../../shared/pipes/mp-time.pipe';
import {
  IDataColumn,
  IPlanningCard,
  IPlanningFilters,
  ISortColumn,
} from '../../../../shared/interfaces';

@Component({
  selector: 'planning-data-line',
  standalone: true,
  imports: [
    CommonModule,
    JobDevicesComponent,
    JobStatusComponent,
    MpEmployeePipe,
    MpListItemPipe,
    PlanningTimePipe,
    JobFilesCheckComponent,
    MpTimePipe,
  ],
  templateUrl: './data-line.component.html',
  styleUrls: ['./data-line.component.scss'],
})
export class DataLineComponent {
  @Input({ required: true }) public card!: IPlanningCard;
  @Input({ required: true }) public filters: IPlanningFilters | null = null;
  @Input({ required: true }) public canChangeComment = false;
  @Input({ required: true }) public canUpdateStatus = false;
  @Input({ required: true }) public columns!: IDataColumn[];

  @Output() sort = new EventEmitter<ISortColumn | null>();
  @Output() editComment = new EventEmitter<void>();
  @Output() viewJob = new EventEmitter();
  @Output() changeStatus = new EventEmitter<void>();

  public onViewJob(): void {
    this.viewJob.emit();
  }

  public get jobNumberColorClass(): string {
    if (!this.card.isPlanned) {
      return 'color-gray-60';
    }

    if (this.card.jobInfo.sync.status === 'success') {
      return 'color-primary';
    }

    return 'color-error';
  }

  public get displayAddCommentButton(): boolean {
    return this.canChangeComment && !this.card.jobInfo.comment;
  }

  public get isDayView(): boolean {
    return this.filters ? this.filters.period === 'day' : false;
  }

  public get displayMergedCard(): boolean {
    return (
      this.card.days.length > 1 &&
      !!this.filters &&
      this.filters?.mergeJobOperationCards &&
      this.filters.mode === 'list' &&
      this.filters.period !== 'day'
    );
  }

  public onChangeStatus(): void {
    if (this.canUpdateStatus) {
      this.changeStatus.emit();
    }
  }

  public onEditComment(): void {
    if (this.canChangeComment) {
      this.editComment.emit();
    }
  }
}
