import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpinnerComponent } from '../../../../shared/components/spinner/spinner.component';
import { EmptyMessageComponent } from '../../../../shared/components/empty-message/empty-message.component';

@Component({
  selector: 'planning2-message',
  standalone: true,
  imports: [CommonModule, SpinnerComponent, EmptyMessageComponent],
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss'],
})
export class MessageComponent {
  @Input() public isLoading = false;
  @Input() public isEmpty = false;
  @Input() public hasFilteredEmptyResult = false;
  @Input() public fetchError: string | null = null;
  @Input() public isSearchResult = false;

  get displayLoader(): boolean {
    return this.isLoading && !this.fetchError;
  }

  get displayFetchErrorMessage(): boolean {
    return !!this.fetchError;
  }

  get displayEmptyMessage(): boolean {
    return (
      this.isEmpty &&
      !this.isLoading &&
      !this.isSearchResult &&
      !this.fetchError
    );
  }

  get displayFilteredEmptyResultsMessage(): boolean {
    return (
      this.hasFilteredEmptyResult &&
      !this.isEmpty &&
      !this.isLoading &&
      !this.isSearchResult &&
      !this.fetchError
    );
  }

  get displayEmptySearchMessage(): boolean {
    return (
      this.isEmpty && !this.isLoading && this.isSearchResult && !this.fetchError
    );
  }
}
