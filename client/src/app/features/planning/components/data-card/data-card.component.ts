import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JobDevicesComponent } from '../../../../shared/components/job-devices/job-devices.component';
import { JobStatusComponent } from '../../../../shared/components/job-status/job-status.component';
import { MpEmployeePipe } from '../../../../shared/pipes/mp-employee.pipe';
import { MpListItemPipe } from '../../../../shared/pipes/mp-list-item.pipe';
import { PlanningTimePipe } from '../../../../shared/pipes/planning-time.pipe';
import { JobFilesCheckComponent } from '../../../../shared/components/job-files-check/job-files-check.component';
import { MpTimePipe } from '../../../../shared/pipes/mp-time.pipe';
import { DataLineComponent } from '../data-line/data-line.component';

@Component({
  selector: 'planning-data-card',
  standalone: true,
  imports: [
    CommonModule,
    JobDevicesComponent,
    JobStatusComponent,
    MpEmployeePipe,
    MpListItemPipe,
    PlanningTimePipe,
    JobFilesCheckComponent,
    MpTimePipe,
  ],
  templateUrl: './data-card.component.html',
  styleUrls: ['./data-card.component.scss'],
})
export class DataCardComponent extends DataLineComponent {
  public override get displayMergedCard(): boolean {
    return (
      this.card.days.length > 1 &&
      !!this.filters &&
      this.filters?.mergeJobOperationCards
    );
  }

  public isSameDate(dayDate: Date): boolean {
    return (
      new Date(dayDate).getDate() === new Date(this.card.startDate).getDate()
    );
  }
}
