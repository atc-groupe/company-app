import { Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlanningSetup, User } from '../../../../shared/entities';
import { IPlanningFilters } from '../../../../shared/interfaces';
import { Store } from '@ngrx/store';
import { FormBuilder, FormControl, ReactiveFormsModule } from '@angular/forms';
import { IAppSettingsPlanningView } from '../../../../shared/interfaces/app-settings/i-app-settings-planning-view';
import { SwitchComponent } from '../../../../shared/components/switch/switch.component';
import { MpListItemPipe } from '../../../../shared/pipes/mp-list-item.pipe';
import { ModalService } from '../../../../shared/services';
import * as PlanningActions from '../../../../shared/store/planning/planning.actions';
import { ClickOutsideDirective } from '../../../../shared/directives/click-outside.directive';
import { MessageComponent } from '../../../../shared/components/message/message.component';
import { SaveButtonComponent } from '../../../../shared/components/save-button/save-button.component';

@Component({
  selector: 'app-employees-filter',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SwitchComponent,
    MpListItemPipe,
    ClickOutsideDirective,
    MessageComponent,
    SaveButtonComponent,
  ],
  templateUrl: './employees-filter.component.html',
  styleUrls: ['./employees-filter.component.scss'],
})
export class EmployeesFilterComponent implements OnInit {
  @Input() setup!: PlanningSetup;
  @Input() user!: User;
  @Input() filters!: IPlanningFilters;

  public error: string | null = null;
  public form = this._fb.group({
    employees: this._fb.array([]),
  });
  public employees: string[] | null = null;
  public view: IAppSettingsPlanningView | null = null;
  constructor(
    private _store: Store,
    private _fb: FormBuilder,
    private _modalService: ModalService,
  ) {}

  ngOnInit() {
    if (!this.filters.view) {
      this.error = 'Aucune vue de planning sélectionnée';
      return;
    }

    this.view = this.filters.view;
    this.employees = this.setup.getGroupEmployees(this.view.planningGroup);

    if (!this.employees || this.employees.length === 0) {
      this.error = 'Aucun utilisateur connecté à ce planning';
      return;
    }

    this.employees.forEach((employee) => {
      const isActive = this.filters.employees
        ? this.filters.employees.includes(employee)
        : false;

      this.form.controls.employees.push(new FormControl(isActive));
    });
  }

  public onCancel(): void {
    this._modalService.close();
  }

  public onFilterMyJobs(): void {
    this._store.dispatch(
      PlanningActions.setFiltersAction({
        filters: { employees: [this.user.mp.name] },
      }),
    );

    this._modalService.close();
  }

  public onRemoveFilters(): void {
    this._store.dispatch(
      PlanningActions.setFiltersAction({
        filters: { employees: null },
      }),
    );

    this._modalService.close();
  }

  public onSubmit(): void {
    if (!this.employees) {
      return;
    }

    const employees = this.form.controls.employees.value as boolean[];

    if (employees.every((v) => !v)) {
      this.error = 'Veuillez sélectionner au moins un utilisateur';
      return;
    }

    const isAllSelected = employees.every((v) => v);
    const employeesFilters = isAllSelected
      ? null
      : employees.reduce((acc: string[], value, currentIndex) => {
          if (value) {
            acc.push(this.employees![currentIndex]);
          }

          return acc;
        }, []);

    this._store.dispatch(
      PlanningActions.setFiltersAction({
        filters: {
          employees: employeesFilters,
        },
      }),
    );

    this._modalService.close();
  }
}
