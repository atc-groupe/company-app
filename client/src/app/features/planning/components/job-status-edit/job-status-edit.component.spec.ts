import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobStatusEditComponent } from './job-status-edit.component';

describe('JobStatusComponent', () => {
  let component: JobStatusEditComponent;
  let fixture: ComponentFixture<JobStatusEditComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [JobStatusEditComponent],
    });
    fixture = TestBed.createComponent(JobStatusEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
