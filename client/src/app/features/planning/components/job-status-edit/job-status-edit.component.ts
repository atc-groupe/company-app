import { Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { User } from '../../../../shared/entities';
import {
  IPlanningCardJobInfo,
  ISelectItem,
} from '../../../../shared/interfaces';
import { BehaviorSubject } from 'rxjs';
import {
  JobsService,
  ModalService,
  NotificationsService,
} from '../../../../shared/services';
import {
  FormBuilder,
  FormControl,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { SelectComponent } from '../../../../shared/components/select/select.component';
import { IJobStatusChangeDto } from '../../../../shared/dto';
import { SpinnerComponent } from '../../../../shared/components/spinner/spinner.component';
import { MessageComponent } from '../../../../shared/components/message/message.component';
import { CancelButtonComponent } from '../../../../shared/components/cancel-button/cancel-button.component';
import { ModalCloseDirective } from '../../../../shared/directives/modal-close.directive';
import { SaveButtonComponent } from '../../../../shared/components/save-button/save-button.component';

@Component({
  selector: 'planning-job-status-edit',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SelectComponent,
    SpinnerComponent,
    MessageComponent,
    CancelButtonComponent,
    ModalCloseDirective,
    SaveButtonComponent,
  ],
  templateUrl: './job-status-edit.component.html',
  styleUrls: ['./job-status-edit.component.scss'],
})
export class JobStatusEditComponent implements OnInit {
  @Input() jobInfo!: IPlanningCardJobInfo;
  @Input() user!: User;

  public error: string | null = null;
  public processing = false;
  public statuses$: BehaviorSubject<ISelectItem[]> = new BehaviorSubject<
    ISelectItem[]
  >([]);
  public form = this._fb.group({
    statusNumber: new FormControl<number | null>(null, Validators.required),
  });

  constructor(
    private _jobsService: JobsService,
    private _modalService: ModalService,
    private _fb: FormBuilder,
    private _notificationService: NotificationsService,
  ) {}

  ngOnInit() {
    this._jobsService.getStatusesList().subscribe({
      next: (list) => {
        let statuses = list.map((status) => {
          return {
            label: status.label,
            value: status.number,
          };
        });

        if (!this.user.isAdmin) {
          statuses = statuses.filter((status) =>
            this.user.mp.operations.some(
              (op) => op.label === status.label && op.type === 2,
            ),
          );
        }

        if (statuses.length) {
          this.statuses$.next(statuses);
          this.form.controls.statusNumber.setValue(
            this.user.isAdmin
              ? this.jobInfo.statusNumber
              : this.statuses$.value[0].value,
          );
        }
      },
      error: () => {
        this.error = 'Impossible de récupérer les statuts de jobs';
      },
    });
  }

  public onSubmit(): void {
    const statusNumber = this.form.controls.statusNumber.value;

    if (!statusNumber) {
      this.error = 'Veuillez sélectionner un statut';
      return;
    }

    if (statusNumber === this.jobInfo.statusNumber) {
      this.error = `Le statut n'a pas été modifié`;
      return;
    }

    this.error = null;
    this.processing = true;

    const dto: IJobStatusChangeDto = {
      statusNumber,
      reason: `[API] changement par ${this.user.completeName}`,
    };

    this._jobsService.changeJobStatus(this.jobInfo.number, dto).subscribe({
      next: () => {
        this._notificationService.displaySuccess(
          `Statut du job ${this.jobInfo.number} modifié !`,
        );
        this._modalService.close();
      },
      error: (err) => {
        this.error = `Impossible d'effectuer l'opération. ${err.error.message}`;
        this.processing = false;
      },
    });
  }
}
