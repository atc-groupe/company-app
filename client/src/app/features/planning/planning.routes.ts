import { Routes } from '@angular/router';
import { PlanningComponent } from './planning.component';

export default [
  {
    path: '',
    component: PlanningComponent,
  },
] satisfies Routes;
