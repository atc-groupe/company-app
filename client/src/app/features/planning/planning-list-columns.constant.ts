import { IDataColumn } from '../../shared/interfaces';

export const planningListColumns: IDataColumn[] = [
  { icon: 'event', flex: 2 },
  { label: 'Job', flex: 2 },
  { label: 'Client', flex: 8 },
  { label: 'Statut', flex: 5 },
  { label: 'Fabrications', flex: 8 },
  { icon: 'picture_as_pdf', flex: 1, textAlign: 'center', notSortable: true },
  { label: 'Operations', flex: 4, textAlign: 'center' },
  { icon: 'person', flex: 3, textAlign: 'center' },
  { icon: 'timer', flex: 2, textAlign: 'center' },
  { icon: 'local_shipping', flex: 2, textAlign: 'center' },
  { label: 'Commentaire', flex: 9, notSortable: true },
  { icon: 'done', flex: 1, textAlign: 'center' },
  { icon: 'sync', flex: 1, textAlign: 'center', notSortable: true },
];
