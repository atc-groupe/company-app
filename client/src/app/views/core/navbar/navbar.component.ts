import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterLink, RouterLinkActive } from '@angular/router';
import { INavItem } from '../../../shared/interfaces';
import { User } from '../../../shared/entities';
import { APP_VERSION } from '../../../app.version';

@Component({
  selector: 'app-navbar',
  standalone: true,
  imports: [CommonModule, RouterLink, RouterLinkActive],
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent {
  @Input() public user: User | null = null;
  @Input() public navItems: INavItem[] | null = [];
  @Output() private toggleNav = new EventEmitter<void>();
  @Output() private logout = new EventEmitter<void>();

  public appVersion = APP_VERSION;

  public onToggleNav(): void {
    this.toggleNav.emit();
  }

  public onLogout() {
    this.logout.emit();
  }
}
