import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { INavItem, IUser } from '../../../shared/interfaces';
import { RouterLink, RouterLinkActive } from '@angular/router';
import { ClickOutsideDirective } from '../../../shared/directives/click-outside.directive';
import { User } from '../../../shared/entities';

@Component({
  selector: 'app-nav',
  standalone: true,
  imports: [CommonModule, RouterLink, RouterLinkActive, ClickOutsideDirective],
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
})
export class NavComponent {
  @Input() public user: User | null = null;
  @Input() public expanded: boolean = false;
  @Input() public navItems: INavItem[] | null = [];
  @Output() private hide = new EventEmitter<void>();
  @Output() private logout = new EventEmitter<void>();

  public onHide() {
    this.hide.emit();
  }

  public onLogout() {
    this.logout.emit();
  }
}
