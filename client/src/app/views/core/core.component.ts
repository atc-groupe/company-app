import { Component, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { RouterOutlet } from '@angular/router';
import { Store } from '@ngrx/store';
import { UserSelectors } from '../../shared/store/user';
import { MediaDirective } from '../../shared/directives/media.directive';
import { NavComponent } from './nav/nav.component';
import { INavItem } from '../../shared/interfaces';
import { SecurityActions } from '../../shared/store/security';
import {
  AuthorizationsService,
  WsAppSettingsService,
} from '../../shared/services';
import { Subscription } from 'rxjs';
import { TUiMode } from '../../shared/types';
import { selectUiMode } from '../../shared/store/ui';
import { NotificationComponent } from './notification/notification.component';
import { ModalComponent } from './modal/modal.component';
import { User } from '../../shared/entities';
import { WsMeService } from '../../shared/services/ws-me.service';

@Component({
  selector: 'app-core',
  standalone: true,
  imports: [
    CommonModule,
    NavbarComponent,
    RouterOutlet,
    MediaDirective,
    NavComponent,
    NotificationComponent,
    ModalComponent,
  ],
  templateUrl: './core.component.html',
  styleUrls: ['./core.component.scss'],
})
export class CoreComponent implements OnInit, OnDestroy {
  public user: User | null = null;
  public nav: INavItem[] = [];
  public uiMode: TUiMode | null = null;
  public navExpanded = false;
  public navItems$ = this._authorizationService.getAppFeatureNavItems();
  private _subscription = new Subscription();

  constructor(
    private _store: Store,
    private _authorizationService: AuthorizationsService,
    private _wsMe: WsMeService,
    private _wsSettings: WsAppSettingsService,
  ) {}

  ngOnInit() {
    this._subscription.add(
      this._store.select(UserSelectors.selectUser).subscribe((user) => {
        this.user = user;

        if (!user) {
          return;
        }

        this._wsMe.connect(user._id);
        this._wsSettings.connect();
      }),
    );

    this._subscription.add(
      this._store.select(selectUiMode).subscribe((uiMode) => {
        this.uiMode = uiMode;
      }),
    );
  }

  public onHideNav() {
    this.navExpanded = false;
  }

  public onToggleNav() {
    this.navExpanded = !this.navExpanded;
  }

  public onLogout() {
    this._store.dispatch(SecurityActions.tryLogOutAction());
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
    this._wsSettings.disconnect();
    this._wsMe.disconnect();
  }
}
