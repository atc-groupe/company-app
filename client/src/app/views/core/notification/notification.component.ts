import { Component, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Notification } from '../../../shared/classes';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import {
  removeNotificationAction,
  selectUiNotification,
} from '../../../shared/store/ui';

@Component({
  selector: 'app-notification',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],
})
export class NotificationComponent implements OnInit, OnDestroy {
  public notification: Notification | null = null;
  public expanded: boolean = false;
  private _subscription = new Subscription();

  constructor(private _store: Store) {}

  ngOnInit(): void {
    this._subscription.add(
      this._store.select(selectUiNotification).subscribe((notification) => {
        this.notification = notification;

        if (notification) {
          this.expanded = true;

          if (!notification.mustValidate) {
            setTimeout(() => {
              this.expanded = false;
            }, 1000 * notification.duration);
          }
        }
      }),
    );
  }

  public validate(): void {
    this.expanded = false;
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
