import {
  Component,
  OnDestroy,
  OnInit,
  Type,
  ViewEncapsulation,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalService } from '../../../shared/services';
import { Subscription } from 'rxjs';
import { IModalOptions } from '../../../shared/interfaces';

@Component({
  selector: 'app-modal',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ModalComponent implements OnInit, OnDestroy {
  public visible = true;
  public component: Type<any> | null = null;
  public inputs: Record<string, unknown> = {};
  public options: IModalOptions = { yPos: 'top', fullScreen: false };
  private _subscription = new Subscription();

  constructor(private _modalService: ModalService) {}

  ngOnInit() {
    this._modalService.modal$.subscribe((modal) => {
      if (!modal) {
        this.visible = false;
        this.component = null;
        this.inputs = {};
      } else {
        this.visible = true;
        this.component = modal.component;
        this.inputs = modal.inputs ? modal.inputs : {};
        this.options = { ...this.options, ...modal.options };
      }
    });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
