import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  FormBuilder,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { Store } from '@ngrx/store';
import {
  SecuritySelectors,
  SecurityActions,
} from '../../shared/store/security';

@Component({
  selector: 'app-signin',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule],
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss'],
})
export class SigninComponent {
  public form: FormGroup;
  public authError$ = this._store.select(SecuritySelectors.selectAuthError);

  constructor(
    _fb: FormBuilder,
    private _store: Store,
  ) {
    this.form = _fb.group({
      identifier: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  public onSignIn() {
    const { identifier, password } = this.form.getRawValue();

    if (!identifier || !password) {
      return;
    }

    this._store.dispatch(
      SecurityActions.trySignInAction({
        credentials: { identifier, password },
      }),
    );
  }
}
