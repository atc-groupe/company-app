import { INavItem } from './shared/interfaces';
import { AppModuleEnum, AppModuleLabelEnum } from './shared/enums';

export const APP_NAV: INavItem[] = [
  {
    route: AppModuleEnum.Jobs,
    label: AppModuleLabelEnum.Jobs.toUpperCase(),
    disabled: false,
  },
  {
    route: AppModuleEnum.Planning,
    label: AppModuleLabelEnum.Planning.toUpperCase(),
    disabled: false,
  },
  {
    route: AppModuleEnum.Stock,
    label: AppModuleLabelEnum.Stock.toUpperCase(),
    disabled: false,
  },
  {
    route: AppModuleEnum.Expeditions,
    label: AppModuleLabelEnum.Expeditions.toUpperCase(),
    disabled: false,
  },
  {
    route: AppModuleEnum.Admin,
    label: AppModuleLabelEnum.Admin.toUpperCase(),
    disabled: false,
  },
];
