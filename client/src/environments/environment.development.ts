import { IAppEnvironment } from '../app/shared/interfaces';

export const environment: IAppEnvironment = {
  wsUri: 'http://localhost:5010',
};
