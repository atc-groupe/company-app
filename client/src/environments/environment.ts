import { IAppEnvironment } from '../app/shared/interfaces';

export const environment: IAppEnvironment = {
  wsUri: 'https://app2.atc-groupe.com',
};
