FROM node:lts-alpine as build
WORKDIR /app
COPY client/package*.json .
RUN npm install
COPY ./client .
RUN npm run build

FROM nginx:stable-alpine
COPY --from=build /app/dist/browser /var/www/html/app
EXPOSE 443
CMD ["nginx", "-g", "daemon off;"]
